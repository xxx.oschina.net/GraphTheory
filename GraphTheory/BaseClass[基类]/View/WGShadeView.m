//
//  WGShadeView.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/3.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGShadeView.h"

@interface WGShadeView ()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *actionBtn;
@property (nonatomic, copy)   tapNext tap;

@end

@implementation WGShadeView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}
- (instancetype)initWithTitle:(NSString *)title des:(NSString *)des tap:(tapNext)tap{
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF" alpha:0.9];
        [self addlayout];
        self.titleLab.text = title;
        [self.actionBtn setTitle:des forState:UIControlStateNormal];
        _tap = tap;
    }
    return self;
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = titleStr;
}
- (void)setDesStr:(NSString *)desStr{
    _desStr = desStr;
    [self.actionBtn setTitle:desStr forState:UIControlStateNormal];
}
- (void)addlayout{
    [self addSubview:self.titleLab];
    [self addSubview:self.actionBtn];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.bottom.equalTo(self.mas_centerY).offset(-10);
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
    [self.actionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.mas_centerY).offset(-10);
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
}
- (void)turnNext{
    if (self.tap) {
        self.tap();
    }
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}
- (UIButton *)actionBtn{
    if (!_actionBtn) {
        _actionBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _actionBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_actionBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
        [_actionBtn addTarget:self action:@selector(turnNext) forControlEvents:UIControlEventTouchUpInside];
    }
    return _actionBtn;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
