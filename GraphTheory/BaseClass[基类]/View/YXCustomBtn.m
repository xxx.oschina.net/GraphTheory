//
//  YXCustomBtn.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/4/17.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "YXCustomBtn.h"

@implementation YXCustomBtn

/** 设置按钮本身相关属性 */
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.layer.borderColor = APPTintColor.CGColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 10.0f;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

/** 初始化上下两个Label */
- (YXCustomBtn *)buttonWithAboveLabelTitle:(NSString *)aStr belowLabelTitle:(NSString *)bStr {
    
    UILabel *aboveL = [[UILabel alloc] init];
    aboveL.text = aStr;
    aboveL.font = [UIFont systemFontOfSize:18.0];
    aboveL.textColor = APPTintColor;
    aboveL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:aboveL];
    self.aboveL = aboveL;
    UILabel *belowL = [[UILabel alloc] init];
    belowL.text = bStr;
    belowL.font = [UIFont systemFontOfSize:12.0];
    belowL.textColor = APPTintColor;
    belowL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:belowL];
    self.belowL = belowL;
    return self;
}

/** 初始化上下两个LabelStr */
- (YXCustomBtn *)buttonWithAboveLabelStr:(NSString *)aStr belowLabelStr:(NSString *)bStr PackageId:(NSString *)packageId {
    _aboveStr = aStr;
    _belowLStr = bStr;
    _packageId = packageId;
    return self;
}

/** 布局两个子控件 */
- (void)layoutSubviews {
    [super layoutSubviews];
    self.aboveL.frame = CGRectMake(0, 15, self.width, 20);
    self.belowL.frame = CGRectMake(0, CGRectGetMaxY(self.aboveL.frame)+ 8, self.width, 20);
}


///** 重写按钮选中状态方法 */
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.backgroundColor = selected ? APPTintColor : [UIColor whiteColor];
    self.aboveL.textColor = selected ? [UIColor whiteColor] : APPTintColor;
    self.belowL.textColor = selected ? [UIColor whiteColor] : APPTintColor;
}


@end
