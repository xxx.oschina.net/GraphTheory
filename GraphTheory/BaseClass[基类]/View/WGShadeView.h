//
//  WGShadeView.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/3.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseView.h"

NS_ASSUME_NONNULL_BEGIN

//请求成功
typedef void (^tapNext)(void);

@interface WGShadeView : DDBaseView

@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *desStr;

- (instancetype)initWithTitle:(NSString *)title des:(NSString *)des tap:(tapNext)tap;



@end

NS_ASSUME_NONNULL_END
