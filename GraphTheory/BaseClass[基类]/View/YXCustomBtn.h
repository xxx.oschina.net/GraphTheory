//
//  YXCustomBtn.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/4/17.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCustomBtn : UIButton

/** aStr上面的title bStr下面的title */
- (YXCustomBtn *)buttonWithAboveLabelTitle:(NSString *)aStr belowLabelTitle:(NSString *)bStr;

/** aStr上面的str bStr下面的str */
- (YXCustomBtn *)buttonWithAboveLabelStr:(NSString *)aStr belowLabelStr:(NSString *)bStr PackageId:(NSString *)packageId;

/** aboveLabel */
@property (nonatomic, weak) UILabel *aboveL;
/** belowLabel */
@property (nonatomic, weak) UILabel *belowL;
/** aboveStr */
@property (nonatomic, strong) NSString *aboveStr;
/** belowStr */
@property (nonatomic, strong) NSString *belowLStr;
/** packageId */
@property (nonatomic, strong) NSString *packageId;

@end

NS_ASSUME_NONNULL_END
