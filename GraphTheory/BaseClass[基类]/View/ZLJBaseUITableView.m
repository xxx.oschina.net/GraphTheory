//
//  ZLJBaseUITableView.m
//  ZanXiaoQu
//
//  Created by TomLong on 2018/10/23.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "ZLJBaseUITableView.h"

@implementation ZLJBaseUITableView

- (void)refreshTableViewPullUpAndDown{
    
    [self refreshTableViewPullUp];
    [self refreshTableViewPullDown];
}

#pragma makr -- 下拉刷新
- (void)refreshTableViewPullDown{
    __weak typeof(self) weakSelf = self;
    self.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self.mj_header endRefreshing];
        if ([weakSelf.refreshDelegate respondsToSelector:@selector(refreshTableViewPullDown:)]) {
            [weakSelf.refreshDelegate refreshTableViewPullDown:self];
        }
    }];
}
#pragma makr -- 上拉加载
- (void)refreshTableViewPullUp{
    __weak typeof(self) weakSelf = self;
    self.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.mj_footer endRefreshing];
        if ([weakSelf.refreshDelegate respondsToSelector:@selector(refreshTableViewPullUp:)]) {
            [weakSelf.refreshDelegate refreshTableViewPullUp:self];
        }
    }];
}
@end
