//
//  ZLJBaseUITableView.h
//  ZanXiaoQu
//
//  Created by TomLong on 2018/10/23.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh/MJRefresh.h>

NS_ASSUME_NONNULL_BEGIN

@protocol BaseTableViewRefreshDelegate <NSObject>
@optional
#pragma makr -- 下拉刷新
- (void)refreshTableViewPullDown:(id)refreshTableView;
#pragma makr -- 上拉加载
- (void)refreshTableViewPullUp:(id)refreshTableView;
@end

@interface ZLJBaseUITableView : UITableView

@property(nonatomic,weak) id <BaseTableViewRefreshDelegate> refreshDelegate;
/**
 同时实现上拉和刷新
 */
- (void)refreshTableViewPullUpAndDown;
#pragma makr -- 下拉刷新
- (void)refreshTableViewPullDown;
#pragma makr -- 上拉加载
- (void)refreshTableViewPullUp;
@end

NS_ASSUME_NONNULL_END
