//
//  WGHomeNavViewController.h
//  DDLife
//
//  Created by wanggang on 2020/2/10.
//  Copyright © 2020年 点都. All rights reserved.
//

#import "HWPopNavigationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGHomeNavViewController : HWPopNavigationController

@end

NS_ASSUME_NONNULL_END
