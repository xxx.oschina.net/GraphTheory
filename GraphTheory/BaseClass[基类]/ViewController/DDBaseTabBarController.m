//
//  DDBaseTabBarController.m
//  DDLife
//
//  Created by 赵越 on 2019/7/11.
//  Copyright © 2019 赵越. All rights reserved.
//

#import "DDBaseTabBarController.h"

#import "DDBaseNavigationController.h"
#import "YXHomeViewController.h"
#import "YXMediaViewController.h"
#import "YXOrderViewController.h"
#import "YXMineViewController.h"

@interface DDBaseTabBarController ()

@property (nonatomic, strong) DDBaseNavigationController *homeNav;
@property (nonatomic, strong) DDBaseNavigationController *mediaNav;
@property (nonatomic, strong) DDBaseNavigationController *orderNav;
@property (nonatomic, strong) DDBaseNavigationController *mineNav;

@end

@implementation DDBaseTabBarController

- (instancetype)init {
    if (self = [super initWithViewControllers:[self viewControllersForTabBar]
                        tabBarItemsAttributes:[self tabBarItemsAttributesForTabBar]]) {
        [self customizeTabBarAppearance];
        self.delegate = self;
        self.navigationController.navigationBar.hidden = YES;
    }
    return self;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [[UITabBar appearance] setTranslucent:NO];

    [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:YES];
    [self becomeFirstResponder];
    [self hideTabBarShadowImageView];


}
- (NSArray *)viewControllersForTabBar {
    YXHomeViewController *firstViewController = [[YXHomeViewController alloc] init];
    _homeNav = [[DDBaseNavigationController alloc]
                                                   initWithRootViewController:firstViewController];
    [firstViewController cyl_setHideNavigationBarSeparator:YES];
     [firstViewController cyl_setNavigationBarHidden:YES];
    
    YXMediaViewController *secondViewController = [[YXMediaViewController alloc] init];
    _mediaNav = [[DDBaseNavigationController alloc]
                                                    initWithRootViewController:secondViewController];
    [secondViewController cyl_setHideNavigationBarSeparator:YES];
    [secondViewController cyl_setNavigationBarHidden:YES];
    
    
    YXOrderViewController *thirdViewController = [[YXOrderViewController alloc] init];
    _orderNav = [[DDBaseNavigationController alloc]
                                                    initWithRootViewController:thirdViewController];
    [thirdViewController cyl_setHideNavigationBarSeparator:YES];
     [thirdViewController cyl_setNavigationBarHidden:YES];
    
    YXMineViewController *fourViewController = [[YXMineViewController alloc] init];
    _mineNav = [[DDBaseNavigationController alloc]
                                                   initWithRootViewController:fourViewController];
    [fourViewController cyl_setHideNavigationBarSeparator:YES];
    [fourViewController cyl_setNavigationBarHidden:YES];

    NSArray *viewControllers = @[
                                 _homeNav,
                                 _mediaNav,
                                 _orderNav,
                                 _mineNav
                                 ];
    return viewControllers;
}

- (NSArray *)tabBarItemsAttributesForTabBar {

    NSDictionary *firstTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : @"首页",
                                                 CYLTabBarItemImage : [UIImage imageNamed:@"tab_home_unselect"],  /* NSString and UIImage are supported*/
                                                 CYLTabBarItemSelectedImage : @"tab_home_select",  /* NSString and UIImage are supported*/
                                                 };
    
    NSDictionary *secondTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle : @"B&M",
                                                  CYLTabBarItemImage : [UIImage imageNamed:@"tab_media_unselect"],
                                                  CYLTabBarItemSelectedImage : @"tab_media_select",
                                                  
                                                  };
    NSDictionary *thirdTabBarItemsAttributes = @{
                                                  CYLTabBarItemTitle : @"接单",
                                                  CYLTabBarItemImage : [UIImage imageNamed:@"tab_order_unselect"],
                                                  CYLTabBarItemSelectedImage : @"tab_order_select",
                                                  };
    
    NSDictionary *fourTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : @"我的",
                                                 CYLTabBarItemImage : [UIImage imageNamed:@"tab_mine_unselect"],
                                                 CYLTabBarItemSelectedImage : @"tab_mine_select",
                                                 };


    NSArray *tabBarItemsAttributes = @[
                                       firstTabBarItemsAttributes,
                                       secondTabBarItemsAttributes,
                                       thirdTabBarItemsAttributes,
                                       fourTabBarItemsAttributes,
                                       ];
    return tabBarItemsAttributes;
}

/**
 *  更多TabBar自定义设置：比如：tabBarItem 的选中和不选中文字和背景图片属性、tabbar 背景图片属性等等
 */
- (void)customizeTabBarAppearance {
    // Customize UITabBar height
    // 自定义 TabBar 高度
    // tabBarController.tabBarHeight = CYL_IS_IPHONE_X ? 65 : 40;
    
//    [self rootWindow].backgroundColor = [UIColor colorWithHexString:@"f8f8f8"];
    [self rootWindow].backgroundColor = APPTintColor;
    
    // set the text color for unselected state
    // 普通状态下的文字属性
    NSMutableDictionary *normalAttrs = [NSMutableDictionary dictionary];
    normalAttrs[NSFontAttributeName]=[UIFont systemFontOfSize:12];
    normalAttrs[NSForegroundColorAttributeName]=ColorRGB(157, 157, 157, 1);
    // set the text color for selected state
    // 选中状态下的文字属性
    NSMutableDictionary *selectedAttrs = [NSMutableDictionary dictionary];
    selectedAttrs[NSFontAttributeName]=[UIFont systemFontOfSize:12];
    selectedAttrs[NSForegroundColorAttributeName]=APPTintColor;

    
    // set the text Attributes
    // 设置文字属性
    UITabBarItem *tabBar = [UITabBarItem appearance];
    [tabBar setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    [tabBar setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
}


@end
