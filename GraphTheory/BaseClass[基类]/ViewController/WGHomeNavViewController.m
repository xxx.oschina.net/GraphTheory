//
//  WGHomeNavViewController.m
//  DDLife
//
//  Created by wanggang on 2020/2/10.
//  Copyright © 2020年 点都. All rights reserved.
//

#import "WGHomeNavViewController.h"
#import "HWPop.h"

@interface WGHomeNavViewController ()

@end

@implementation WGHomeNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, 400);
    self.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, 400);
    self.navigationBar.translucent = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
