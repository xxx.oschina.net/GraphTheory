//
//  DDBaseViewController.h
//  DDLife
//
//  Created by 赵越 on 2019/7/11.
//  Copyright © 2019 赵越. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGShadeView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DDBaseViewController : UIViewController
///返回
- (void)back;
///用户改变
- (void)userChangeUpDate;

- (void)showAlertWithMessage:(NSString *)message
                 withConfirm:(NSString *)confirm
                      Handle:(void(^)(UIAlertAction * _Nonnull action))handle;

@end

NS_ASSUME_NONNULL_END
