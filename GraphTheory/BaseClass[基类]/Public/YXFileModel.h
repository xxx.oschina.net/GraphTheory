//
//  YXFileModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/11.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXFileModel : DDBaseModel

/** ossId */
@property (nonatomic ,copy) NSString *Id;

/** oss文件名称 */
@property (nonatomic ,copy) NSString *fileName;

/** 文件地址 */
@property (nonatomic ,copy) NSString *filepath;

/** 文件类型 1.商品 2.店铺3.广告 */
@property (nonatomic ,copy) NSString *type;

/** 链接地址 */
@property (nonatomic ,copy) NSString *linkAddress;

/** 视频截帧图片上传id */
@property (nonatomic ,copy) NSString *materielIdjt;

@end


@interface YXUploadModel : DDBaseModel


/** 上传图片*/
@property (nonatomic ,strong) UIImage *fileImg;
/** 文件地址*/
@property (nonatomic, copy) NSURL *path;
/** 文件类型*/
@property (nonatomic, copy) NSString *type;
/** 文件名称*/
@property (nonatomic, copy) NSString *name;
/** 链接地址*/
@property (nonatomic ,copy) NSString *linkAddress;
/** 广告时长*/
@property (nonatomic ,copy) NSString *duration;
/** 广告视频真实时长*/
@property (nonatomic ,copy) NSString *trueDuration;

@property (nonatomic, assign) BOOL      isUploaded;

@end


@interface YXVoiceModel : DDBaseModel


/** 语速*/
@property (nonatomic ,assign) float yusu;
/** 音调*/
@property (nonatomic ,assign) float yindiao;
/** 语速：0-15*/
@property (nonatomic ,strong) NSString *spd;
/** 语调: 0-15*/
@property (nonatomic ,strong) NSString *text;
/** 冠名内容*/
@property (nonatomic ,strong) NSString *pit;
/** 性别 0女 1男*/
@property (nonatomic ,strong) NSString *per;
/** 音频地址*/
@property (nonatomic ,strong) NSString *fileUrl;

@end


NS_ASSUME_NONNULL_END
