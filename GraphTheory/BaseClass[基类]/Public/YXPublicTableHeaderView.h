//
//  YXPublicTableHeaderView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXPublicTableHeaderView : UITableViewHeaderFooterView
@property (copy, nonatomic) NSString *title;

@end

NS_ASSUME_NONNULL_END
