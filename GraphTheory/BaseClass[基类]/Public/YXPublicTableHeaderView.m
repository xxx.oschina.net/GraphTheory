//
//  YXPublicTableHeaderView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXPublicTableHeaderView.h"

@interface YXPublicTableHeaderView ()
@property (weak, nonatomic) UILabel *titleLb;
@end


@implementation YXPublicTableHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(12, 0, KWIDTH - 12, self.height)];
    titleLb.textColor = color_TextOne;
    titleLb.font = [UIFont systemFontOfSize:14.0f];
    [self.contentView addSubview:titleLb];
    self.titleLb = titleLb;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    self.titleLb.text = title;
    if (title.length == 1) {
        self.titleLb.x = 25;
        self.titleLb.width = KWIDTH - 25;
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"#f6f6f6"];
    }else {
        self.titleLb.x = 12;
        self.titleLb.width = KWIDTH - 12;
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLb.height = self.height;
}

@end
