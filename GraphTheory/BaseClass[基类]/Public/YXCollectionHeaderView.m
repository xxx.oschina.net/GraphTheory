//
//  YXHeaderCollectionView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXCollectionHeaderView.h"



@implementation YXCollectionHeaderView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLab];
        [self addSubview:self.doubtBtn];
        [self addSubview:self.selectBtn];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text= @"播放次数统计";
        _titleLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (UIButton *)doubtBtn {
    if (!_doubtBtn) {
        _doubtBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_doubtBtn setImage:[UIImage imageNamed:@"advert_question"] forState:(UIControlStateNormal)];
        [_doubtBtn addTarget:self action:@selector(doubtBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
        _doubtBtn.hidden = YES;
    }
    return _doubtBtn;
}


- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_selectBtn setTitle:@"广告类别" forState:(UIControlStateNormal)];
        [_selectBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
        _selectBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [_selectBtn setImage:[UIImage imageNamed:@"advert_arrow_down"] forState:(UIControlStateNormal)];
        [_selectBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(12, 6)) space:10];
        [_selectBtn addTarget:self action:@selector(selectBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _selectBtn;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_doubtBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(12, 12));
    }];
    
    
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-15.0);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.selectBtn sizeToFit];
    }];
    
}

#pragma mark - Touch Even
- (void)doubtBtnAction {
    [YJProgressHUD showMessage:@"广告按次实时扣费 投放项目价格不同时显示为均价"];
}


- (void)selectBtnAction:(UIButton *)sender {
    if (self.clickSelectBtnBlock) {
        self.clickSelectBtnBlock(sender);
    }
}

#ifdef __IPHONE_11_0
+ (Class)layerClass {
    return [CustomLayer class];
}
#endif

@end


#ifdef __IPHONE_11_0
@implementation CustomLayer

- (CGFloat) zPosition {
    return 0;
}

@end
#endif
