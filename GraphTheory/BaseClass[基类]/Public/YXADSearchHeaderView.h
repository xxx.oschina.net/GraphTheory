//
//  YXADSearchHeaderView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXADSearchHeaderView : UIView

@property (nonatomic ,copy)void(^clickSearchBlock)(NSString *text);

@property (nonatomic ,strong) UISearchBar *searchBar;

@end

NS_ASSUME_NONNULL_END
