//
//  YXPublicSuccessVC.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/23.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXPublicSuccessVC : DDBaseViewController

@property (nonatomic ,strong) NSString *titleStr;

@property (nonatomic ,strong) NSString *content;

/**
 返回页面类型
 type == 1 广告物料
 type == 2 投放管理

 */
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
