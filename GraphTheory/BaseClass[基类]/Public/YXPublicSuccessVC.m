//
//  YXPublicSuccessVC.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/23.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXPublicSuccessVC.h"
#import "YXMineViewController.h"
//#import "YXADMaterialViewController.h"
//#import "YXCastPutManageViewController.h"

@interface YXPublicSuccessVC ()

@property (nonatomic ,strong) UIImageView *imgView;

@property (nonatomic ,strong) UILabel *contentLab;

@end

@implementation YXPublicSuccessVC

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:(CGRectMake((KWIDTH-150)/2, 120, 150, 125))];
        _imgView.image = [UIImage imageNamed:@"advert_sunmit_Success"];
    }
    return _imgView;
}

- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] initWithFrame:(CGRectMake(0, CGRectGetMaxY(self.imgView.frame) +15, KWIDTH, 40))];
        _contentLab.textColor = color_TextOne;
        _contentLab.font = [UIFont systemFontOfSize:14.0f];
        _contentLab.textAlignment = NSTextAlignmentCenter;
        _contentLab.numberOfLines = 2;
        _contentLab.text = @"提交成功\n广告将按设定的规则开始投放";
    }
    return _contentLab;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.titleStr?self.titleStr:@"提交成功";
    
    // 禁止侧滑返回上一页
    self.navigationController.rt_disableInteractivePop = YES;
    self.rt_disableInteractivePop = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    
    [self.view addSubview:self.imgView];
    [self.view addSubview:self.contentLab];
    
    _contentLab.text = [NSString stringWithFormat:@"%@",_content];

 
}

- (void)back {
    
    if (self.type == 1) {
        NSArray* viewArr = self.navigationController.viewControllers;
        if ([viewArr[0] isKindOfClass:[YXMineViewController class]]) {
            for (UIViewController *temp in self.navigationController.viewControllers) {
//                if ([temp isKindOfClass:[YXADMaterialViewController class]]) {
//                    [self.navigationController popToViewController:temp animated:YES];
//                }
            }
        }else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
      
    }else if (self.type == 2) {
//        for (UIViewController *temp in self.navigationController.viewControllers) {
//            if ([temp isKindOfClass:[YXCastPutManageViewController class]]) {
//                [self.navigationController popToViewController:temp animated:YES];
//            }
//        }
    }else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}



@end
