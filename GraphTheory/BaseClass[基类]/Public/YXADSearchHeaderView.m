//
//  YXADSearchHeaderView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXADSearchHeaderView.h"

@interface YXADSearchHeaderView ()<UISearchBarDelegate>

@end

@implementation YXADSearchHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.searchBar];
    }
    return self;
}

- (UISearchBar *)searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:(CGRectMake(16, 15, KWIDTH - 32, 36))];
        _searchBar.placeholder = @"搜索小区名称";
        _searchBar.searchBarStyle = UISearchBarStyleMinimal;
        _searchBar.showsCancelButton = NO;
        _searchBar.returnKeyType = UIReturnKeySearch;
        _searchBar.delegate = self;
        _searchBar.tintColor = [UIColor orangeColor];
        _searchBar.barTintColor = [UIColor whiteColor];
    }
    return _searchBar;
}

#pragma mark - UISearchBar Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if (self.clickSearchBlock) {
        self.clickSearchBlock(searchBar.text);
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar*)searchBar {
    [searchBar resignFirstResponder];
    if (searchBar.text.length==0) {
        if (self.clickSearchBlock) {
            self.clickSearchBlock(searchBar.text);
        }
    }
}


@end
