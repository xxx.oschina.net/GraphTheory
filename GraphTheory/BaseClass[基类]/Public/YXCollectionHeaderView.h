//
//  YXHeaderCollectionView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCollectionHeaderView : UICollectionReusableView

@property (nonatomic ,copy)void(^clickSelectBtnBlock)(UIButton *btn);

@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UIButton *doubtBtn;

@property (nonatomic ,strong) UIButton *selectBtn;


@end


#ifdef __IPHONE_11_0
@interface CustomLayer : CALayer

@end
#endif

NS_ASSUME_NONNULL_END
