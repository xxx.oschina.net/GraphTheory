//
//  YXHomeCYCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeCYCollectionViewCell.h"
#import "YXApplyRoleInfoModel.h"
#import "YXFocusModel.h"
#import <MAMapKit/MAMapKit.h>
#import "YXAMapManager.h"
#import "YXCityModel.h"
#import "YXDataTimeTool.h"
#import "NSMutableAttributedString+ZLJLabelString.h"

@interface YXHomeCYCollectionViewCell ()<MAMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *roleLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet MAMapView *mapBackView;
@property (weak, nonatomic) IBOutlet UIView *serviceView;
@property (weak, nonatomic) IBOutlet UIButton *ageBtn;
@property (weak, nonatomic) IBOutlet UIButton *cityBtn;
@property (weak, nonatomic) IBOutlet UIButton *collegeBtn;
@property (strong, nonatomic)  UILabel *collegeLab;

@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIButton *twoBtn;
@property (weak, nonatomic) IBOutlet UIButton *threeBtn;

@property (strong, nonatomic) MAPointAnnotation *pointAnnotation;

@end

@implementation YXHomeCYCollectionViewCell

- (void)setUserInfo:(YXUserInfoModel *)userInfo {
    _userInfo = userInfo;
}

- (void)setRoleModel:(YXApplyRoleInfoModel *)roleModel {
    _roleModel = roleModel;
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:_userInfo.wechat_headimgurl]];
    _nameLab.text = _userInfo.nike;
    _contentLab.text = _userInfo.signature;
    
    if (_userInfo.birthday.length >0) {
        NSString *endStr = [YXDataTimeTool getCurrentTimesWithFormat:@"yyyy-MM-dd"];
        NSInteger year = [YXDataTimeTool getYearsWithNowDateStr:_userInfo.birthday deadlineStr:endStr];
        [_ageBtn setTitle:[NSString stringWithFormat:@"%ld",year+1] forState:(UIControlStateNormal)];
    }else {
        [_ageBtn setTitle:@"1" forState:(UIControlStateNormal)];
    }
    
    if (_userInfo.college.length > 0) {
        _collegeBtn.hidden = NO;
        [_collegeBtn setTitle:_userInfo.college forState:(UIControlStateNormal)];
    }else {
        _collegeBtn.hidden = YES;
    }
    if (_type == 2) {
        _roleLab.text = _userInfo.show_id;
        if (_roleModel.applyInfo.serviceCities.count) {
            YXApplyRoleInfoModelApplyInfoServiceCities *city = _roleModel.applyInfo.serviceCities[0];
            [_cityBtn setTitle:city.district forState:(UIControlStateNormal)];
        }
        NSMutableAttributedString *str1 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.earnMoney] appendString:@"收益"];
        NSMutableAttributedString *str2 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.takeOrderNum] appendString:@"接单"];
        NSMutableAttributedString *str3 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.complainOrderNum] appendString:@"异议"];
        [_oneBtn setAttributedTitle:str1 forState:UIControlStateNormal];
        [_twoBtn setAttributedTitle:str2 forState:UIControlStateNormal];
        [_threeBtn setAttributedTitle:str3 forState:UIControlStateNormal];
        [self addWorkServiceView];
    }else {
        _roleLab.text = _userInfo.show_video_id;
        if (_roleModel.applyInfo.serviceCities.count) {
            YXApplyRoleInfoModelApplyInfoServiceCities *city = _roleModel.applyInfo.serviceCities[0];
            [_cityBtn setTitle:city.district forState:(UIControlStateNormal)];
        }
        NSMutableAttributedString *str1 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.collectNum] appendString:@"收藏"];
        NSMutableAttributedString *str2 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.followNum] appendString:@"关注"];
        NSMutableAttributedString *str3 =[NSMutableAttributedString attributedLabelTextWithString:[NSString stringWithFormat:@"%@",_roleModel.statisticData.fansNum] appendString:@"粉丝"];
        [_oneBtn setAttributedTitle:str1 forState:UIControlStateNormal];
        [_twoBtn setAttributedTitle:str2 forState:UIControlStateNormal];
        [_threeBtn setAttributedTitle:str3 forState:UIControlStateNormal];
        [self addVidoServiceView];
    }
 
    DDWeakSelf
    [[YXAMapManager shareLocation] getLocationInfo:^(YXCitysModel * _Nonnull cityModel) {
        CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(cityModel.lat, cityModel.lng);
//        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        weakSelf.pointAnnotation.coordinate = coor;
        //设置地图的定位中心点坐标
        weakSelf.mapBackView.centerCoordinate = coor;
        //将点添加到地图上，即所谓的大头针
        [weakSelf.mapBackView addAnnotation:weakSelf.pointAnnotation];
    }];
    
    
//    CGFloat lat = [_roleModel.applyInfo.address.lat floatValue];
//    CGFloat lng = [_roleModel.applyInfo.address.lng floatValue];
//    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lng);
//    MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
//    pointAnnotation.coordinate = coor;
//    //设置地图的定位中心点坐标
//    self.mapBackView.centerCoordinate = coor;
//    //将点添加到地图上，即所谓的大头针
//    [self.mapBackView addAnnotation:pointAnnotation];

}

- (void)addWorkServiceView {
    
    if (_userInfo.applyInfo) {
        NSArray *services = [YXApplyRoleInfoModelApplyInfoServices mj_objectArrayWithKeyValuesArray:_userInfo.applyInfo[@"worker"][@"services"]];
        if (services.count > 4) {
            services = [services subarrayWithRange:NSMakeRange(0, 4)];
        }
        NSMutableArray *viewArr = [NSMutableArray array];
        for (int i = 0 ; i < services.count; i++) {
            YXApplyRoleInfoModelApplyInfoServices *model = [services objectAtIndex:i];
            UILabel *lab = [[UILabel alloc] init];
            lab.text = model.name;
            lab.textColor = color_TextTwo;
            lab.textAlignment = NSTextAlignmentCenter;
            lab.font = [UIFont systemFontOfSize:12];
            lab.layer.masksToBounds = YES;
            lab.layer.cornerRadius = 4.0;
            lab.backgroundColor = color_BackColor;
            [self.serviceView addSubview:lab];
            [viewArr addObject:lab];
        }
        if (viewArr.count > 1) {
            [viewArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:0 tailSpacing:0];
            [viewArr mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(self.serviceView);
            }];
        }else {
            [viewArr mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.serviceView);
                make.left.equalTo(self.serviceView);
                make.size.mas_equalTo(CGSizeMake(40, 20));
            }];
        }
    }
   
    
}

- (void)addVidoServiceView {
    
    if (_userInfo.applyInfo) {
        NSArray *services = [YXFocusModelListAuthorInfoRoleInfoServices mj_objectArrayWithKeyValuesArray:_userInfo.applyInfo[@"video"][@"services"]];
        if (services.count > 4) {
            services = [services subarrayWithRange:NSMakeRange(0, 4)];
        }
        NSMutableArray *viewArr = [NSMutableArray array];
        for (int i = 0 ; i < services.count; i++) {
            YXFocusModelListAuthorInfoRoleInfoServices *model = [services objectAtIndex:i];
            UILabel *lab = [[UILabel alloc] init];
            lab.text = model.brand_name;
            lab.textColor = color_TextTwo;
            lab.textAlignment = NSTextAlignmentCenter;
            lab.font = [UIFont systemFontOfSize:12];
            lab.layer.masksToBounds = YES;
            lab.layer.cornerRadius = 4.0;
            lab.backgroundColor = color_BackColor;
            [self.serviceView addSubview:lab];
            [viewArr addObject:lab];
        }
        if (viewArr.count > 1) {
            [viewArr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:0 tailSpacing:0];
            [viewArr mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.bottom.mas_equalTo(self.serviceView);
            }];
        }else {
            [viewArr mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.serviceView);
                make.left.equalTo(self.serviceView);
                make.size.mas_equalTo(CGSizeMake(40, 20));
            }];
        }
      
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.backView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backViewAction)];
    [self.backView addGestureRecognizer:tap];
    self.userImgView.layer.masksToBounds = YES;
    self.userImgView.layer.cornerRadius = 25;
    self.ageBtn.layer.masksToBounds = YES;
    self.ageBtn.layer.cornerRadius = 4.0;
    self.ageBtn.layer.borderWidth = 1.0f;
    self.ageBtn.layer.borderColor = color_TextThree.CGColor;
    self.cityBtn.layer.masksToBounds = YES;
    self.cityBtn.layer.cornerRadius = 4.0;
    self.cityBtn.layer.borderWidth = 1.0f;
    self.cityBtn.layer.borderColor = color_TextThree.CGColor;
    self.collegeBtn.layer.masksToBounds = YES;
    self.collegeBtn.layer.cornerRadius = 4.0;
    self.collegeBtn.layer.borderWidth = 1.0f;
    self.collegeBtn.layer.borderColor = color_TextThree.CGColor;
    self.oneBtn.tag = 1000;
    self.twoBtn.tag = 1001;
    self.threeBtn.tag = 1002;
    self.mapBackView.delegate = self;
    self.mapBackView.zoomEnabled = YES;
    self.mapBackView.showsCompass = NO;
    self.mapBackView.userInteractionEnabled = NO;
}
//- (UILabel *)collegeLab {
//    if (!_collegeLab) {
//        _collegeLab = [UILabel new];
//        _collegeLab.textColor = color_TextOne;
//        _collegeLab.font = [UIFont systemFontOfSize:12];
//
//        _collegeLab.textAlignment = NSTextAlignmentCenter;
//    }
//    return _collegeLab;
//}

- (MAPointAnnotation *)pointAnnotation {
    if (!_pointAnnotation) {
        _pointAnnotation = [[MAPointAnnotation alloc] init];
    }
    return _pointAnnotation;
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
       MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.image = [UIImage imageNamed:@"map_b1"];
        return annotationView;
    }
    return nil;
}

// 点击背景
- (void)backViewAction {
    if (self.clickBackBlock) {
        self.clickBackBlock(self.type);
    }
}

- (IBAction)btnAction:(UIButton *)sender {
    if (self.clickBtnBlock) {
        self.clickBtnBlock(sender.tag-1000);
    }
    
}



@end
