//
//  YXProductCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXProductModelList;
@interface YXProductCollectionViewCell : UICollectionViewCell
@property (nonatomic ,strong) YXProductModelList *model;
@end

NS_ASSUME_NONNULL_END
