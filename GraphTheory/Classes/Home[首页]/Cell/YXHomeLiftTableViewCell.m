//
//  YXHomeLiftTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/4.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeLiftTableViewCell.h"
#import "YXShareOrdersModel.h"
@interface YXHomeLiftTableViewCell ()
/**
 图片
 */
@property (nonatomic ,strong)  UIImageView *userImgView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UILabel *timeLab;

/**
 当前金额
 */
@property (nonatomic ,strong) UILabel *curPriceLab;

/**
 状态
 */
@property (nonatomic ,strong) UILabel *statusLab;
@end

@implementation YXHomeLiftTableViewCell

- (void)setModel:(YXShareOrdersModelList *)model {
    _model = model;
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:_model.product_icon]];
    _titleLab.text = _model.product_details.name;
    _timeLab.text = _model.create_time;
    _curPriceLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
    _statusLab.text = _model.statusTitle;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.userImgView];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.timeLab];
        [self.contentView addSubview:self.curPriceLab];
        [self.contentView addSubview:self.statusLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.contentMode = UIViewContentModeScaleAspectFill;
        _userImgView.clipsToBounds = YES;
        _userImgView.layer.cornerRadius = 15.0f;
    }
    return _userImgView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}
- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.textColor = color_TextTwo;
        _timeLab.font = [UIFont systemFontOfSize:12.0];
    }
    return _timeLab;
}

- (UILabel *)curPriceLab {
    if (!_curPriceLab) {
        _curPriceLab = [[UILabel alloc] init];
        _curPriceLab.textColor = color_TextTwo;
        _curPriceLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _curPriceLab;
}

- (UILabel *)statusLab {
    if (!_statusLab) {
        _statusLab = [[UILabel alloc] init];
        _statusLab.textColor = color_TextTwo;
        _statusLab.font = [UIFont systemFontOfSize:12.0];
    }
    return _statusLab;
}


- (void)addLayout {
   
    YXWeakSelf
    [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.contentView.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.userImgView.mas_right).offset(10);
        make.top.equalTo(weakSelf.mas_top).offset(10);
    }];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(10);
    }];
    
    [self.curPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.userImgView.mas_right).offset(10);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-10);
        [weakSelf.curPriceLab sizeToFit];
    }];
    
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.contentView.mas_right).offset(-15);
        make.centerY.equalTo(weakSelf.userImgView.mas_centerY);
        [weakSelf.statusLab sizeToFit];
    }];
    
}



@end
