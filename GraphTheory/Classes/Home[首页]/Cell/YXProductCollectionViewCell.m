//
//  YXProductCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXProductCollectionViewCell.h"
#import "YXProductModel.h"

@interface YXProductCollectionViewCell ()
/** 图片*/
@property (nonatomic ,strong) UIImageView *imgView;
/** 名称*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 库存*/
@property (nonatomic ,strong) UILabel *stockLab;
/** 时间*/
@property (nonatomic ,strong) UILabel *priceLab;

@end

@implementation YXProductCollectionViewCell

- (void)setModel:(YXProductModelList *)model {
    _model = model;
    if (_model.banner.length>0) {
        NSArray *imageArr = [_model.banner componentsSeparatedByString:@","];
        [_imgView sd_setImageWithURL:[NSURL URLWithString:imageArr[0]]];
    }
    _nameLab.text = _model.title;
    _stockLab.text = [NSString stringWithFormat:@"库存：%@",_model.stock];
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.stockLab];
        [self.contentView addSubview:self.priceLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.layer.masksToBounds = YES;
        _imgView.layer.cornerRadius = 4;
    }
    return _imgView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)stockLab {
    if (!_stockLab) {
        _stockLab = [[UILabel alloc] init];
        _stockLab.textColor = color_TextTwo;
        _stockLab.text = @"已禁用";
        _stockLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _stockLab;
}

- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] init];
        _priceLab.font = [UIFont systemFontOfSize:12.0f];
        _priceLab.text = @"客服";
        _priceLab.textColor = color_TextTwo;
    }
    return _priceLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@10);
        make.size.mas_equalTo(CGSizeMake(70, 70));
    }];
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.imgView.mas_top);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_stockLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.stockLab sizeToFit];
    }];

    [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.imgView.mas_bottom);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.priceLab sizeToFit];
    }];

}




@end
