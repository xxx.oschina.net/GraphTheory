//
//  YXHomeCYCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXUserInfoModel,YXApplyRoleInfoModel;
@interface YXHomeCYCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickBackBlock)(NSInteger index);

@property (nonatomic ,copy)void(^clickBtnBlock)(NSInteger index);

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXUserInfoModel *userInfo;

@property (nonatomic ,strong) YXApplyRoleInfoModel *roleModel;

@end

NS_ASSUME_NONNULL_END
