//
//  YXVideoCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXVideoCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^editorBtnBlock)(YXVideoModel *model);

@property (nonatomic ,copy)void(^deleteBtnBlock)(YXVideoModel *model);

@property (nonatomic ,strong) YXVideoModel *model;

@end

NS_ASSUME_NONNULL_END
