//
//  YXHomeLiftCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXHomeLiftCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickOrderBtnBlock)(NSIndexPath *indexPath);

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

NS_ASSUME_NONNULL_END
