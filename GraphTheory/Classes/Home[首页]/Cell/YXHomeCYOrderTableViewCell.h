//
//  YXHomeCYOrderTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXShareOrdersModelList;
@interface YXHomeCYOrderTableViewCell : UITableViewCell

@property (nonatomic ,copy)void(^clickOrderBtnBlock)(void);

@property (nonatomic ,strong) YXShareOrdersModelList *model;
@end

NS_ASSUME_NONNULL_END
