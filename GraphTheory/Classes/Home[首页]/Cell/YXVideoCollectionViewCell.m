//
//  YXVideoCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXVideoCollectionViewCell.h"
#import "YXVideoModel.h"
@interface YXVideoCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end

@implementation YXVideoCollectionViewCell

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.v_cover_img]];
    _titleLab.text = _model.v_name;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.backView.layer.borderWidth = 1.0;
    self.backView.layer.borderColor = color_LineColor.CGColor;
    self.backView.backgroundColor = [UIColor whiteColor];
    self.imgView.layer.masksToBounds = YES;
}

- (IBAction)editorBtnAction:(UIButton *)sender {
    if (self.editorBtnBlock) {
        self.editorBtnBlock(self.model);
    }
}

- (IBAction)delectBtnAction:(UIButton *)sender {
    if (self.deleteBtnBlock) {
        self.deleteBtnBlock(self.model);
    }
}


@end
