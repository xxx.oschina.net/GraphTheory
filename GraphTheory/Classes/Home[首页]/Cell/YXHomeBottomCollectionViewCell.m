//
//  YXHomeBottomCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeBottomCollectionViewCell.h"

@implementation YXHomeBottomCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
    if (_type == 2) {
        [self.btn setTitle:@"提取佣金" forState:(UIControlStateNormal)];
        [self.btn setImage:nil forState:(UIControlStateNormal)];
    }else {
        [self.btn setTitle:@"发布新动态" forState:(UIControlStateNormal)];
        [self.btn setImage:[UIImage imageNamed:@"fabuxindongtai"] forState:(UIControlStateNormal)];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.btn.layer.masksToBounds = YES;
    self.btn.layer.cornerRadius = 4.0f;
    self.btn.userInteractionEnabled = NO;
}

@end
