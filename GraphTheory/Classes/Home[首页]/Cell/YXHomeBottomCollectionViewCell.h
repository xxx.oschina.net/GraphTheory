//
//  YXHomeBottomCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXHomeBottomCollectionViewCell : UICollectionViewCell
@property (nonatomic ,assign) NSInteger type;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

NS_ASSUME_NONNULL_END
