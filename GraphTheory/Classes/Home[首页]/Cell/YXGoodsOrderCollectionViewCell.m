//
//  YXGoodsOrderCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXGoodsOrderCollectionViewCell.h"
#import "YXUserOrderModel.h"
@interface YXGoodsOrderCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;

@end

@implementation YXGoodsOrderCollectionViewCell

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
}

- (void)setModel:(YXUserOrderModelList *)model {
    _model = model;
    
    _orderNoLab.text = [NSString stringWithFormat:@"单号：%@",_model.order_no];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.product_details.imgs]];
    _titleLab.text = _model.product_details.name;
    _subTitleLab.text = _model.create_time;
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
    [_bottomBtn setTitle:_model.statusTitle forState:(UIControlStateNormal)];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_bottomBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
    _bottomBtn.layer.masksToBounds = YES;
    _bottomBtn.layer.cornerRadius = 4.0f;
    _bottomBtn.layer.borderWidth = 1.0f;
    _bottomBtn.layer.borderColor = APPTintColor.CGColor;
    
}
- (IBAction)deleteBtnAction:(UIButton *)sender {
    if (self.clickDeleteBtn) {
        self.clickDeleteBtn(_indexPath);
    }
}

- (IBAction)bottomBtnAction:(UIButton *)sender {
    if (self.clickBottomBtnBlock) {
        self.clickBottomBtnBlock(_indexPath,sender);
    }
    
}



@end
