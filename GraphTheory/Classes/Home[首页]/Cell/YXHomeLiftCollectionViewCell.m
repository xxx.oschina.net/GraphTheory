//
//  YXHomeLiftCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeLiftCollectionViewCell.h"
#import "YXHomeLiftTableViewCell.h"
#import "YXHomeCYOrderTableViewCell.h"
#import "YXOrderDetailsViewController.h"
#import "YXShareOrdersModel.h"
@interface YXHomeLiftCollectionViewCell ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@end

@implementation YXHomeLiftCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setDataArr:(NSMutableArray *)dataArr {
    _dataArr = dataArr;
    [self.tableView reloadData];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.contentView);
        }];
    }
    return self;
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.type == 1) {
        YXHomeLiftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXHomeLiftTableViewCell"];
        if (!cell) {
            cell = [[YXHomeLiftTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXHomeLiftTableViewCell"];
        }
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }else {
        YXHomeCYOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXHomeCYOrderTableViewCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        DDWeakSelf
        [cell setClickOrderBtnBlock:^{
            if (weakSelf.clickOrderBtnBlock) {
                weakSelf.clickOrderBtnBlock(indexPath);
            }
        }];
        return cell;
    }
  
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == 2) {
        YXShareOrdersModelList *model = self.dataArr[indexPath.row];
        YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
        vc.type = 0;
        vc.Id = model.Id;
        [[UIView currentViewController].navigationController pushViewController:vc animated:YES];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerClass:[YXHomeLiftTableViewCell class] forCellReuseIdentifier:@"YXHomeCYOrderTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXHomeCYOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXHomeCYOrderTableViewCell"];
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{

        }];
      
    }
    return _tableView;
}


@end
