//
//  YXGoodsOrderCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXUserOrderModelList;
@interface YXGoodsOrderCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickDeleteBtn)(NSIndexPath *indexPath);

/**
 回调点击底部视图的按钮
 */
@property (nonatomic ,copy)void(^clickBottomBtnBlock)(NSIndexPath *indexPath,UIButton *btn);

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,strong) YXUserOrderModelList *model;

@end

NS_ASSUME_NONNULL_END
