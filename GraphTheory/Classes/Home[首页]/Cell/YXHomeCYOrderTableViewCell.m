//
//  YXHomeCYOrderTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeCYOrderTableViewCell.h"
#import "YXShareOrdersModel.h"
@interface YXHomeCYOrderTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UIButton *orderBtn;

@end

@implementation YXHomeCYOrderTableViewCell

- (void)setModel:(YXShareOrdersModelList *)model {
    _model = model;

    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.product_icon]];
    _titleLab.text = _model.content;
    _timeLab.text = _model.create_time;
    _typeLab.text = _model.product_details.name;
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
    [_orderBtn setTitle:_model.statusTitle forState:(UIControlStateNormal)];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.orderBtn.layer.masksToBounds = YES;
    self.orderBtn.layer.cornerRadius = 4.0;
    self.typeLab.layer.masksToBounds = YES;
    self.typeLab.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnAction:(UIButton *)sender {
    if (self.clickOrderBtnBlock) {
        self.clickOrderBtnBlock();
    }
}

@end
