//
//  YXHomeOrderCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXShareOrdersModelList;
@interface YXHomeOrderCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) YXShareOrdersModelList *listModel;

@end

NS_ASSUME_NONNULL_END
