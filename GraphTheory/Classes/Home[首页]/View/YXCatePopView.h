//
//  YXCatePopView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCateDetailModel;
@interface YXCatePopView : UIView

@property (nonatomic ,copy)void(^delectBlock)(NSArray *dataArr);

- (instancetype)initWithFrame:(CGRect)frame withArray:(NSMutableArray *)arr;

- (void)show;
- (void)dismiss;

@end

@interface YXCatePopViewViewCell : UITableViewCell

/** 图片*/
@property (nonatomic ,strong) UIImageView *imgView;
/** 名称*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 类型*/
@property (nonatomic ,strong) UILabel *typeLab;

@property (nonatomic ,strong) UIButton *deleBtn;

@property (nonatomic ,copy)void(^delectBlock)(void);

@end
NS_ASSUME_NONNULL_END
