//
//  YXHomeMenuCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/7.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXHomeMenuCollectionViewCell.h"
#import "YXHomeModel.h"
#import "YXVideoModel.h"
#import "YXCateModel.h"
@interface YXHomeMenuCollectionViewCell ()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UIImageView *imageView;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *titleLab;

@end

@implementation YXHomeMenuCollectionViewCell


- (void)setModel:(YXCateListModel *)model {
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:_model.img] placeholderImage:[UIImage imageNamed:@"更多"]];
    _titleLab.text = _model.name;
}

- (void)setBrandModel:(YXBrandModel *)brandModel {
    _brandModel = brandModel;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:_brandModel.brand_picpath] placeholderImage:[UIImage imageNamed:@""]];
    _titleLab.text = _brandModel.brand_name;
}
- (void)setChildModel:(YXCateChildModel *)childModel {
    _childModel = childModel;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:_childModel.img] placeholderImage:[UIImage imageNamed:@""]];
    _titleLab.text = _childModel.name;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpLayout];
    }
    return self;
}


/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.imageView];
    [self.backView addSubview:self.titleLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.backView.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
    }];
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage imageNamed:@"store_phone"];
    }
    return _imageView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"店铺";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}


@end
