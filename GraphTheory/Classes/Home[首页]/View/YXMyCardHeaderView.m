//
//  YXMyCardHeaderView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/4.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMyCardHeaderView.h"
#import <MAMapKit/MAMapKit.h>
#import "YXCardOneModel.h"
#import "YXCardTwoModel.h"
#import "YXAMapManager.h"
#import "YXCityModel.h"
#import "YXDataTimeTool.h"

@interface YXMyCardHeaderView ()<MAMapViewDelegate>
@property (weak, nonatomic) IBOutlet MAMapView *mapView;
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *ageBtn;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *cityLab;

@end

@implementation YXMyCardHeaderView

- (void)setOneModel:(YXCardOneModel *)oneModel {
    _oneModel = oneModel;
    [_userImg sd_setImageWithURL:[NSURL URLWithString:[YXUserInfoManager getUserInfo].wechat_headimgurl] placeholderImage:[UIImage imageNamed:@"头像素材"]];
    _nameLab.text = _oneModel.username;
    _typeLab.text = _oneModel.show_video_id;
    
    if (_oneModel.college.length > 0) {
        _addressLab.hidden = NO;
        _addressLab.text = _oneModel.college;
    }else {
        _addressLab.hidden = YES;
    }
    
    _contentLab.text = _oneModel.signature;
    if (_oneModel.serviceCities.count) {
        YXCardOneModelServiceCities *cityModel = _oneModel.serviceCities[0];
        _cityLab.text = cityModel.district;
    }

    if (_oneModel.birthday.length >0) {
        NSString *endStr = [YXDataTimeTool getCurrentTimesWithFormat:@"yyyy-MM-dd"];
        NSInteger year = [YXDataTimeTool getYearsWithNowDateStr:_oneModel.birthday deadlineStr:endStr];
        [_ageBtn setTitle:[NSString stringWithFormat:@"%ld",year+1] forState:(UIControlStateNormal)];
    }else {
        [_ageBtn setTitle:@"1" forState:(UIControlStateNormal)];
    }
    
    
}
- (void)setTwoModel:(YXCardTwoModel *)twoModel {
    _twoModel = twoModel;
    [_userImg sd_setImageWithURL:[NSURL URLWithString:[YXUserInfoManager getUserInfo].wechat_headimgurl] placeholderImage:[UIImage imageNamed:@"头像素材"]];
    _nameLab.text = _twoModel.username;
    _typeLab.text = _twoModel.show_id;
    
    if (_twoModel.college.length > 0) {
        _addressLab.hidden = NO;
        _addressLab.text = _twoModel.college;
    }else {
        _addressLab.hidden = YES;
    }
    
    _contentLab.text = _twoModel.signature;
    if (_twoModel.serviceCities.count) {
        YXCardOneModelServiceCities *cityModel = _twoModel.serviceCities[0];
        _cityLab.text = cityModel.district;
    }
    
    if (_twoModel.birthday.length >0) {
        NSString *endStr = [YXDataTimeTool getCurrentTimesWithFormat:@"yyyy-MM-dd"];
        NSInteger year = [YXDataTimeTool getYearsWithNowDateStr:_twoModel.birthday deadlineStr:endStr];
        [_ageBtn setTitle:[NSString stringWithFormat:@"%ld",year+1] forState:(UIControlStateNormal)];
    }else {
        [_ageBtn setTitle:@"1" forState:(UIControlStateNormal)];
    }
}

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        // 从xib中找到我们定义的view
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"YXMyCardHeaderView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
        [self setup];
    }
    return self;
    
}

- (void)setup {
    
    self.mapView.delegate = self;
    self.mapView.zoomEnabled = YES;
    self.mapView.showsCompass = NO;
    self.mapView.zoomLevel = 12;
    self.userImg.layer.masksToBounds = YES;
    self.userImg.layer.cornerRadius = 35.0;
    self.ageBtn.layer.borderColor = color_LineColor.CGColor;
    self.ageBtn.layer.borderWidth =1.0;
    self.ageBtn.layer.masksToBounds = YES;
    self.ageBtn.layer.cornerRadius= 4.0;
    self.addressLab.layer.borderColor = color_LineColor.CGColor;
    self.addressLab.layer.borderWidth =1.0;
    self.addressLab.layer.masksToBounds = YES;
    self.addressLab.layer.cornerRadius= 4.0;
    self.cityLab.layer.borderColor = color_LineColor.CGColor;
    self.cityLab.layer.borderWidth =1.0;
    self.cityLab.layer.masksToBounds = YES;
    self.cityLab.layer.cornerRadius= 4.0;
    
    DDWeakSelf
    [[YXAMapManager shareLocation] getLocationInfo:^(YXCitysModel * _Nonnull cityModel) {
        CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(cityModel.lat, cityModel.lng);
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
        pointAnnotation.coordinate = coor;
        //设置地图的定位中心点坐标
        weakSelf.mapView.centerCoordinate = coor;
        //将点添加到地图上，即所谓的大头针
        [weakSelf.mapView addAnnotation:pointAnnotation];
    }];
}


- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
       MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.image = [UIImage imageNamed:@"map_b1"];
        return annotationView;
    }
    return nil;
}

@end
