//
//  WGStroeHeadCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStroeHeadCollectionReusableView.h"

@interface WGStroeHeadCollectionReusableView()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;
/**
 右边按钮
 */
@property (nonatomic ,strong) UIButton *checkBtn;
@end

@implementation WGStroeHeadCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setType:(NSInteger)type{
    _type = type;
}

- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    if (_type == 0) {
        switch (index.section) {
            case 2:{
                _titleLab.text = @"实时数据";
                _checkBtn.hidden = YES;
            }
                break;
            case 3:{
                _titleLab.text = @"课堂小纸条";
                _checkBtn.hidden = NO;
                [_checkBtn setTitle:@"按照实际需要" forState:(UIControlStateNormal)];
                [_checkBtn setImage:[UIImage imageNamed:@"right_arrow_black"] forState:(UIControlStateNormal)];
                [_checkBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(7, 12)) space:10];
            }
                break;
            default:
                break;
        }
    }else if (_type == 1) {
        switch (index.section) {
            case 2:{
                _titleLab.text = @"实时数据";
                _checkBtn.hidden = YES;
            }
                break;
            case 3:{
                _titleLab.text = @"实时订单";
                _checkBtn.hidden = NO;
                [_checkBtn setTitle:@"换一批" forState:(UIControlStateNormal)];
                [_checkBtn setImage:[UIImage imageNamed:@"banben_e8e8e8_48"] forState:(UIControlStateNormal)];
                [_checkBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(16, 16)) space:10];
            }
                break;
            default:
                break;
        }
        
    }else if (_type == 2) {
        switch (index.section) {
            case 2:{
                _titleLab.text = @"抢单列表";
                _checkBtn.hidden = YES;
            }
                break;
            default:
                break;
        }
    }else if (_type == 3) {
        switch (index.section) {
            case 2:{
                _titleLab.text = @"视频";
                _checkBtn.hidden = YES;
            }
                break;
            default:
                break;
        }
    }
    
}

/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.checkBtn];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.top.bottom.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_checkBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView.mas_centerY);
        make.right.equalTo(self.backView.mas_right).offset(-15);
    }];

}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"我的订单";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _titleLab;
}

- (UIButton *)checkBtn {
    if (!_checkBtn) {
        _checkBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_checkBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        _checkBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_checkBtn addTarget:self action:@selector(checkBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _checkBtn;
}

- (void)checkBtnAction {
    if (self.clickCheckBtnBlock) {
        self.clickCheckBtnBlock();
    }
}

@end
