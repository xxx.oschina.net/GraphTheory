//
//  YXHomeMenuCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/7.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCateListModel,YXBrandModel,YXCateChildModel;
@interface YXHomeMenuCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,strong) YXCateListModel *model;

@property (nonatomic ,strong) YXBrandModel *brandModel;

@property (nonatomic ,strong) YXCateChildModel *childModel;

@end

NS_ASSUME_NONNULL_END
