//
//  YXTitleView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXTitleView : UICollectionReusableView

@property (nonatomic ,copy)void(^clickBtnBlock)(NSInteger index);

@property(nonatomic,strong)NSArray *titleArr;


@end

NS_ASSUME_NONNULL_END
