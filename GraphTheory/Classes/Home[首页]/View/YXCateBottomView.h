//
//  YXCateBottomView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCateBottomView : UIView

@property (nonatomic ,copy)void(^clickBtnBlock)(UIButton *btn);

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIButton *numBtn;
@property (nonatomic ,strong) UIButton *addBtn;
@property (nonatomic ,strong) UIButton *orderBtn;
@property (nonatomic ,strong) UIView *leftLine;
@property (nonatomic ,strong) UIView *rightLine;

@end

NS_ASSUME_NONNULL_END
