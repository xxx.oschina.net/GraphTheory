//
//  YXCateBottomView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateBottomView.h"

@implementation YXCateBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setup];
 
    }
    return self;
}

- (void)setup {
    [self addSubview:self.backView];
    [self.backView addSubview:self.numBtn];
    [self.backView addSubview:self.orderBtn];
    [self.backView addSubview:self.leftLine];
    [self.backView addSubview:self.rightLine];
    [self.backView addSubview:self.addBtn];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@10);
        make.bottom.right.equalTo(@-10);
    }];
    
    [self.numBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self.backView);
        make.width.equalTo(@100);
    }];
    
    [self.orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self.backView);
        make.width.equalTo(@100);
    }];
    
    [self.leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.numBtn.mas_right);
        make.top.equalTo(self.backView.mas_top).offset(10);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
        make.width.equalTo(@1);
    }];
    
    [self.rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.orderBtn.mas_left);
        make.top.equalTo(self.backView.mas_top).offset(10);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
        make.width.equalTo(@1);
    }];
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.backView);
        make.left.equalTo(self.leftLine.mas_right);
        make.right.equalTo(self.rightLine.mas_left);
    }];
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = APPTintColor;
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 4.0f;
    }
    return _backView;
}
- (UIView *)leftLine {
    if (!_leftLine) {
        _leftLine = [UIView new];
        _leftLine.backgroundColor = [UIColor whiteColor];
    }
    return _leftLine;
}
- (UIView *)rightLine {
    if (!_rightLine) {
        _rightLine = [UIView new];
        _rightLine.backgroundColor = [UIColor whiteColor];
    }
    return _rightLine;
}

- (UIButton *)numBtn {
    if (!_numBtn) {
        _numBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_numBtn setTitle:@"共0项" forState:(UIControlStateNormal)];
        [_numBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _numBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [_numBtn addTarget:self action:@selector(click:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _numBtn;
}
- (UIButton *)orderBtn {
    if (!_orderBtn) {
        _orderBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_orderBtn setTitle:@"下单" forState:(UIControlStateNormal)];
        [_orderBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _orderBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [_orderBtn addTarget:self action:@selector(click:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _orderBtn;
}
- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_addBtn setTitle:@"成为雇主" forState:(UIControlStateNormal)];
        [_addBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _addBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [_addBtn addTarget:self action:@selector(click:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _addBtn;
}

- (void)click:(UIButton *)sender {
    
    if (self.clickBtnBlock) {
        self.clickBtnBlock(sender);
    }
}

@end
