//
//  YXSearchView.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXSearchView.h"

@interface YXSearchView ();

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIImageView *imgView;
@end

@implementation YXSearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 4.0f;
        [self setup];
    }
    return self;
}

- (void)setup {
    
    [self addSubview:self.titleLab];
    [self addSubview:self.imgView];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(10);
        [self.titleLab sizeToFit];
    }];
    
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
        
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = HEX_COLOR(@"#666666");
        _titleLab.text = @"知识任务";
        _titleLab.font = [UIFont systemFontOfSize:15.0f];
    }
    return _titleLab;
}

- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.image = [UIImage imageNamed:@"search_707070"];
    }
    return _imgView;
}
- (CGSize)intrinsicContentSize{
    return UILayoutFittingExpandedSize;
}

@end
