//
//  YXNavCityView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXNavCityView.h"

@implementation YXNavCityView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor greenColor];
        [self setup];
    }
    return self;
}

- (void)setup {
    //    [self addSubview:self.cityBtn];
    [self addSubview:self.cityLab];
    [self addSubview:self.arrow];
    [self addSubview:self.weatherLab];
    [self.cityLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@5);
        make.left.equalTo(self.mas_left).offset(-10);
        make.height.equalTo(@20);
        [self.cityLab sizeToFit];
    }];
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.cityLab.mas_centerY);
        make.left.equalTo(self.cityLab.mas_right);
        make.size.mas_equalTo(CGSizeMake(12, 6));
    }];
    [self.weatherLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(-10);
        make.top.equalTo(self.cityLab.mas_bottom);
        [self.weatherLab sizeToFit];
    }];
    
}

//- (UIButton *)cityBtn {
//    if (!_cityBtn) {
//        _cityBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        _cityBtn.frame = CGRectMake(0, 0, 60, 26);
//        [_cityBtn setImage:[UIImage imageNamed:@"advert_arrow_down"] forState:UIControlStateNormal];
//        [_cityBtn setTitle:@"郑州市" forState:(UIControlStateNormal)];
//        [_cityBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
//        _cityBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
//        [_cityBtn setImageEdgeInsets:(UIEdgeInsetsMake(0, -10, 0, 10))];
//        [_cityBtn setTitleEdgeInsets:(UIEdgeInsetsMake(0, -10, 0, 10))];
//        _cityBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
////        [_cityBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(12, 6)) space:0];
//        [_cityBtn setLeftTitleAndRightImageWithSpace:0];
//    }
//    return _cityBtn;
//}

- (UILabel *)cityLab {
    if (!_cityLab) {
        _cityLab = [UILabel new];
        _cityLab.textColor = [UIColor blackColor];
        _cityLab.text = @"徐州市";
        _cityLab.font = [UIFont systemFontOfSize:15.0f];
    }
    return _cityLab;
}

- (UIImageView *)arrow {
    if (!_arrow) {
        _arrow = [UIImageView new];
        _arrow.image = [UIImage imageNamed:@"advert_arrow_down"];
    }
    return _arrow;
}

- (UILabel *)weatherLab {
    if (!_weatherLab) {
        _weatherLab = [[UILabel alloc] initWithFrame:CGRectMake(-10, 26, 60, 12)];
//        _weatherLab.backgroundColor = [UIColor whiteColor];
        _weatherLab.text = @"多云36°C";
        _weatherLab.textColor = [UIColor lightGrayColor];
        _weatherLab.font = [UIFont systemFontOfSize:12.f];
        _weatherLab.textAlignment= NSTextAlignmentLeft;
    }
    return _weatherLab;
}

@end
