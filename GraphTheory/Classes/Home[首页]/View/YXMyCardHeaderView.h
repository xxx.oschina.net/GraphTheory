//
//  YXMyCardHeaderView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/4.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCardOneModel,YXCardTwoModel;
@interface YXMyCardHeaderView : UIView
@property (nonatomic ,strong) YXCardOneModel *oneModel;
@property (nonatomic ,strong) YXCardTwoModel *twoModel;
@end

NS_ASSUME_NONNULL_END
