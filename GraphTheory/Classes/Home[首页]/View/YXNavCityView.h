//
//  YXNavCityView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXNavCityView : UIView

//@property (strong, nonatomic) UIButton *cityBtn;
@property (strong, nonatomic) UILabel *cityLab;
@property (strong, nonatomic) UIImageView *arrow;
@property (strong, nonatomic) UILabel *weatherLab;

@end

NS_ASSUME_NONNULL_END
