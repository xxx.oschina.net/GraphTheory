//
//  YXTitleView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXTitleView.h"

@interface YXTitleView ()
@property(nonatomic,strong)UIButton *selectBtn;
@property(nonatomic,strong)UIView *titleView;
@property(nonatomic,strong)UIView *moveView;

@end

@implementation YXTitleView


- (void)setTitleArr:(NSArray *)titleArr {
    _titleArr = titleArr;
    if (_titleArr.count) {
        for (int i = 0; i < _titleArr.count; i++) {
           UIButton *btn = [self viewWithTag:1000+i];
            [btn setTitle:_titleArr[i] forState:(UIControlStateNormal)];
        }
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _titleArr = @[@"视频(0)",@"商品(0)",@"订单(0)"];
        [self setupTopBtn];
    }
    return self;
}

-(void)setupTopBtn
{
    //顶部按钮容器
    self.titleView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH,40)];
    self.titleView.backgroundColor=[UIColor clearColor];
    [self addSubview:self.titleView];
    //滑动条
    self.moveView=[[UIView alloc]init];
    self.moveView.backgroundColor=APPTintColor;
    self.moveView.height=2;
    self.moveView.centerY = 40 - 1;
    //循环创建按钮
    for (int i=0; i<self.titleArr.count; i++) {
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i*KWIDTH/self.titleArr.count, 0, KWIDTH/self.titleArr.count,40 - 2);
        btn.tag=i+1000;
        [btn setTitle:self.titleArr[i] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn setTitleColor:APPTintColor forState:UIControlStateDisabled];
        btn.titleLabel.font=[UIFont systemFontOfSize:15];
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.titleView addSubview:btn];
        //默认点击第一个
        if (i==0) {
            btn.enabled=NO;
            self.selectBtn=btn;
            [btn.titleLabel sizeToFit];
            self.moveView.width=btn.titleLabel.width;
            self.moveView.centerX=btn.centerX;
        }
    }
    [self.titleView addSubview:self.moveView];
    
//    UIView *lineView = [[UIView alloc] initWithFrame:(CGRectMake(0, self.titleView.height-1, KWIDTH, 1))];
//    lineView.backgroundColor = color_LineColor;
//    [self.titleView addSubview:lineView];
}

-(void)btnClick:(UIButton *)btn
{
    self.selectBtn.enabled=YES;
    btn.enabled=NO;
    self.selectBtn=btn;
    [UIView animateWithDuration:0.25 animations:^{
        self.moveView.width=btn.titleLabel.width;
        self.moveView.centerX=btn.centerX;
    }];
    //偏移
//    CGPoint point=self.content.contentOffset;
//    point.x=btn.tag*self.content.W;
//    [self.content setContentOffset:point animated:YES];
    if (self.clickBtnBlock) {
        self.clickBtnBlock(btn.tag-1000);
    }
    
}

@end
