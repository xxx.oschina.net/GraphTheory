//
//  YXHomeOrderCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeOrderCollectionViewCell.h"
#import "YXShareOrdersModel.h"
@interface YXHomeOrderCollectionViewCell ()

@property (nonatomic ,strong)  UIView *bgView;

@property (nonatomic ,strong)  UIImageView *userImgView;

/**
 图片
 */
@property (nonatomic ,strong) UIImageView *topImgView;

/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;

/**
 当前金额
 */
@property (nonatomic ,strong) UILabel *curPriceLab;

/**
 原价金额
 */
@property (nonatomic ,strong) UILabel *originalPriceLab;



@end

@implementation YXHomeOrderCollectionViewCell

- (void)setListModel:(YXShareOrdersModelList *)listModel {
    _listModel = listModel;
    
    [_topImgView sd_setImageWithURL:[NSURL URLWithString:_listModel.product_details.imgs]];
    _titleLab.text = _listModel.content;
    _curPriceLab.text = [NSString stringWithFormat:@"¥%@",_listModel.product_details.c_studyprice];
    
    NSString *dataStr = [YXDataTimeTool getTimeStrWithString:_listModel.create_time Format:@"yyyy-MM-dd HH:mm:ss"];
    _originalPriceLab.text = [YXDataTimeTool timestampToString:[dataStr integerValue] withFormat:@"MM-dd HH:mm:ss"];
    
    
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:_listModel.userInfo.wechat_headimgurl]];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.topImgView];
        [self.bgView addSubview:self.titleLab];
        [self.bgView addSubview:self.curPriceLab];
        [self.bgView addSubview:self.originalPriceLab];
        [self.bgView addSubview:self.userImgView];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.cornerRadius = 4.0f;
        _bgView.layer.borderWidth =1.0f;
        _bgView.layer.borderColor = color_LineColor.CGColor;
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.contentMode = UIViewContentModeScaleAspectFill;
        _userImgView.clipsToBounds = YES;
        _userImgView.layer.cornerRadius = 20.0f;
    }
    return _userImgView;
}


- (UIImageView *)topImgView {
    if (!_topImgView) {
        _topImgView = [UIImageView new];
        _topImgView.image = [UIImage imageNamed:@"goods_placeholder"];
        _topImgView.contentMode = UIViewContentModeScaleAspectFill;
        _topImgView.layer.masksToBounds = YES;
    }
    return _topImgView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
        _titleLab.numberOfLines = 2;
        _titleLab.text = @"阿来得及拉的几率就发点福利拉卡机的立法江东父老安徽的咖对话框";
    }
    return _titleLab;
}


- (UILabel *)curPriceLab {
    if (!_curPriceLab) {
        _curPriceLab = [[UILabel alloc] init];
        _curPriceLab.textColor = APPTintColor;
        _curPriceLab.font = [UIFont systemFontOfSize:12.0f];
        _curPriceLab.text = @"222";
    }
    return _curPriceLab;
}

- (UILabel *)originalPriceLab {
    if (!_originalPriceLab) {
        _originalPriceLab = [[UILabel alloc] init];
        _originalPriceLab.textColor = color_TextTwo;
        _originalPriceLab.font = [UIFont systemFontOfSize:12.0];
        _originalPriceLab.text = @"333";
    }
    return _originalPriceLab;
}


- (void)addLayout {
    
    YXWeakSelf
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf);
    }];
    [self.topImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(weakSelf.bgView);
        make.height.equalTo(@120);
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bgView.mas_left).offset(10);
        make.top.equalTo(weakSelf.topImgView.mas_bottom).offset(30);
        make.right.equalTo(weakSelf.bgView.mas_right).offset(-10);
    }];
    
    
    [self.curPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLab.mas_left);
        make.bottom.equalTo(weakSelf.bgView.mas_bottom).offset(-10);
        [weakSelf.curPriceLab sizeToFit];
    }];
    
    [self.originalPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.curPriceLab.mas_right).offset(5);
        make.centerY.equalTo(weakSelf.curPriceLab.mas_centerY);
        [weakSelf.originalPriceLab sizeToFit];
    }];

    [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bgView.mas_left).offset(10);
        make.top.equalTo(weakSelf.bgView.mas_top).offset(100);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
}

@end
