//
//  WGStroeHeadCollectionReusableView.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGStroeHeadCollectionReusableView : UICollectionReusableView

@property (nonatomic, copy)void(^clickCheckBtnBlock)(void);

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, strong) NSIndexPath *index;

@end

NS_ASSUME_NONNULL_END
