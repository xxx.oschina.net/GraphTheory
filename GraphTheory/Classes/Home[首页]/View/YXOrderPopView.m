//
//  YXOrderPopView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderPopView.h"

@interface YXOrderPopView ()

/** 弹窗视图*/
@property (nonatomic ,strong) UIView *alertView;
/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIImageView *userImgView;

@property (nonatomic ,strong) UILabel *subTitleLab;

/** 关闭按钮*/
@property (nonatomic ,strong) UIButton *closeBtn;
@end

@implementation YXOrderPopView

- (instancetype)initWithFrame:(CGRect)frame Title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.alertView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 400)];
        self.alertView.backgroundColor = [UIColor whiteColor];
        self.alertView.layer.cornerRadius=10.0;
        self.alertView.layer.masksToBounds=YES;
        self.alertView.userInteractionEnabled=YES;
        [self addSubview:self.alertView];
        
        UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, self.alertView.frame.size.width, 40)];
        titleLab.backgroundColor = [UIColor whiteColor];
        titleLab.text = title;
        titleLab.font = [UIFont boldSystemFontOfSize:20];
        titleLab.textAlignment = NSTextAlignmentCenter;
        titleLab.textColor = color_TextOne;
        [self.alertView addSubview:titleLab];
        self.titleLab = titleLab;
        
            
        self.userImgView = [[UIImageView alloc] initWithFrame:(CGRectMake((self.alertView.width-80)/2, 100, 80, 80))];
        [self.userImgView sd_setImageWithURL:[NSURL URLWithString:[YXUserInfoManager getUserInfo].wechat_headimgurl] placeholderImage:[UIImage imageNamed:@"头像素材"]];
        self.userImgView.layer.masksToBounds = YES;
        self.userImgView.layer.cornerRadius = 40.0;
        [self.alertView addSubview:self.userImgView];

        UILabel *subTitleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 250, self.alertView.frame.size.width, 40)];
        subTitleLab.backgroundColor =  [UIColor whiteColor];
        subTitleLab.text = @"抢单成功";
        subTitleLab.font = [UIFont systemFontOfSize:15];
        subTitleLab.textAlignment = NSTextAlignmentCenter;
        subTitleLab.textColor = color_TextTwo;
        [self.alertView addSubview:subTitleLab];
        self.subTitleLab = subTitleLab;
        
        
        self.closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.closeBtn.frame = CGRectMake((self.alertView.width-140)/2, self.alertView.height - 80,140 , 50);
        [self.closeBtn setTitle:@"抢单成功" forState:(UIControlStateNormal)];
        [self.closeBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        self.closeBtn.backgroundColor = APPTintColor;
        self.closeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        self.closeBtn.layer.masksToBounds = YES;
        self.closeBtn.layer.cornerRadius = 4.0;
        [self.closeBtn addTarget:self action:@selector(cancelClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.alertView addSubview:self.closeBtn];
                
    }
    return self;
}

-(void)cancelClick:(UIButton *)sender {
    
//    if (self.clickBtnBlock) {
//        self.clickBtnBlock();
//    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self removeFromSuperview];
    }];
}

-(void)showAnimation{
    
    self.backgroundColor = [UIColor clearColor];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    
    self.alertView.layer.position = self.center;

    self.alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.alertView.alpha = 0;
    YXWeakSelf
    [UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        weakSelf.alertView.transform = transform;
        weakSelf.alertView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
//    if (self.clickBtnBlock) {
//        self.clickBtnBlock();
//    }
    
    if (event.allTouches.count) {
        for (NSSet *set in event.allTouches) {
            UITouch *touch = (UITouch *)set;
            if (touch.view != self.alertView) {
                [UIView animateWithDuration:0.3 animations:^{
                    [self removeFromSuperview];
                }];
            }
        }
    }
    
}

@end
