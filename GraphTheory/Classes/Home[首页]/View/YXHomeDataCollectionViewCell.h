//
//  YXHomeDataCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXHomeDataCollectionViewCell : UICollectionViewCell

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *detailLab;

@property (nonatomic, strong) NSIndexPath *index;

@end

NS_ASSUME_NONNULL_END
