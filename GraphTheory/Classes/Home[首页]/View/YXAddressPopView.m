//
//  YXAddressPopView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAddressPopView.h"
#import "YXOrderInfoModel.h"
#import <MAMapKit/MAMapKit.h>

@interface YXAddressPopView ()<MAMapViewDelegate>
/** 弹窗视图*/
@property (nonatomic ,strong) UIView *alertView;
@property (nonatomic, strong) MAMapView *mapView;

@property (nonatomic ,strong) YXOrderInfoModel *model;
@end

@implementation YXAddressPopView

- (instancetype)initWithFrame:(CGRect)frame model:(YXOrderInfoModel *)model
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.alertView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
        self.alertView.backgroundColor = [UIColor whiteColor];
        self.alertView.layer.cornerRadius=4.0;
        self.alertView.layer.masksToBounds=YES;
//        self.alertView.userInteractionEnabled=YES;
        [self addSubview:self.alertView];
    
                
        self.mapView = [[MAMapView alloc] initWithFrame:self.alertView.bounds];
         _mapView.backgroundColor = [UIColor whiteColor];
         self.mapView.delegate = self;
        //设置定位距离
         _mapView.distanceFilter = 5.0f;
          _mapView.zoomEnabled = YES;
         _mapView.showsCompass = NO;
         //普通样式
         _mapView.mapType = MAMapTypeStandard;
          //缩放等级
//          [_mapView setZoomLevel:12 animated:YES];
         [self.alertView addSubview:self.mapView];
        
        
        if (model.from_address) {
            CGFloat lat = [model.from_address.lat floatValue];
            CGFloat lng = [model.from_address.lng floatValue];
            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lng);
            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
            pointAnnotation.coordinate = coor;
            //设置地图的定位中心点坐标
            self.mapView.centerCoordinate = coor;
            //将点添加到地图上，即所谓的大头针
            [self.mapView addAnnotation:pointAnnotation];
        }
        
    }
    return self;
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
       MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.image = [UIImage imageNamed:@"map_b1"];
        return annotationView;
    }
    return nil;
}

-(void)showAnimation{
    
    self.backgroundColor = [UIColor clearColor];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    
    self.alertView.layer.position = self.center;

    self.alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.alertView.alpha = 0;
    YXWeakSelf
    [UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        weakSelf.alertView.transform = transform;
        weakSelf.alertView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if (event.allTouches.count) {
        NSSet *tempSet = event.allTouches;
        for (UITouch *touch in tempSet) {
            if (touch.view != self.alertView) {
                [UIView animateWithDuration:0.3 animations:^{
                    [self removeFromSuperview];
                }];
            }
        }
    }
    
}


@end
