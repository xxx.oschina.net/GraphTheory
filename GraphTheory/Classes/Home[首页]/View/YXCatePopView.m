//
//  YXCatePopView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCatePopView.h"
#import "YXCateModel.h"
#define kScreenWidth               [UIScreen mainScreen].bounds.size.width
#define kScreenHeight              [UIScreen mainScreen].bounds.size.height
#define kMainWindow                [UIApplication sharedApplication].keyWindow

#define kArrowWidth          15
#define kArrowHeight         10
#define kDefaultMargin       10
#define kAnimationTime       0.25
@interface YXCatePopView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIView *bgView;
@property (nonatomic ,strong) UIView *contentView;
@property (nonatomic ,strong) UIView *footerView;
@property (nonatomic ,strong) UILabel *priceLab;
@property (nonatomic ,strong) UILabel *numberLab;
@property (nonatomic ,assign) CGFloat backViewHight;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@end

@implementation YXCatePopView

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (instancetype)initWithFrame:(CGRect)frame withArray:(NSMutableArray *)arr{
    self = [super initWithFrame:frame];
    if (self) {
        [_dataArr removeAllObjects];
        _dataArr = arr;
        [self setup];
        [self changeAmount];
    }
    return self;
}

- (void)setup {
    
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.tableView];

}
- (void)changeAmount {
    
    CGFloat maxW = 50 + self.dataArr.count *70;
    _contentView.frame = CGRectMake(15, kHEIGHT - Home_Indicator_HEIGHT - 70 - maxW, KWIDTH-30, maxW);
    
    self.numberLab.text = [NSString stringWithFormat:@"共%ld项",self.dataArr.count];
    NSMutableArray *priceArr = [NSMutableArray array];
    for (YXCateDetailModel *model in self.dataArr) {
        [priceArr addObject:model.c_price];
    }
    CGFloat sum = [[priceArr valueForKeyPath:@"@sum.floatValue"] floatValue];
    _priceLab.text = [NSString stringWithFormat:@"合计：¥%.2f",sum];

}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    YXCatePopViewViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCatePopViewViewCell"];
    if (!cell) {
        cell = [[YXCatePopViewViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXCatePopViewViewCell"];
    }
    YXCateDetailModel *model = self.dataArr[indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.img]];
    cell.nameLab.text = model.name;
    cell.typeLab.text = [NSString stringWithFormat:@"%@/%@",model.c_price,model.c_unitname];
    DDWeakSelf
    [cell setDelectBlock:^{
        NSMutableArray *tempArr = weakSelf.dataArr;
        if (tempArr) {
            [tempArr removeObject:model];
        }
        weakSelf.dataArr = tempArr;
        [weakSelf changeAmount];
        if (weakSelf.delectBlock) {
            weakSelf.delectBlock(weakSelf.dataArr);
        }
        [weakSelf.tableView reloadData];
        if (weakSelf.dataArr.count == 0) {
            [weakSelf dismiss];
        }
    }];
    return cell;
}


#pragma mark - 弹出 -
- (void)show {
        
    
    self.backgroundColor = [UIColor clearColor];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.contentView.alpha = 0;
    YXWeakSelf
    [UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        weakSelf.contentView.transform = transform;
        weakSelf.contentView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    for (NSSet *set in event.allTouches) {
        UITouch *touch = (UITouch *)set;
        if (touch.view != self.contentView) {
            [UIView animateWithDuration:0.3 animations:^{
                [self removeFromSuperview];
            }];
        }
    }
}

- (void)dismiss{
    [UIView animateWithDuration:0.3 animations:^{
        [self removeFromSuperview];
    }];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.contentView.bounds style:(UITableViewStylePlain)];
        _tableView.backgroundColor = APPTintColor;
        _tableView.separatorColor = [UIColor whiteColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [self footerView];
        [_tableView registerClass:[YXCatePopViewViewCell class] forCellReuseIdentifier:@"YXCatePopViewViewCell"];
    }
    return _tableView;
}

- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
        _bgView.alpha = 0.0f;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
        [_bgView addGestureRecognizer:tap];
    }
    return _bgView;
}

- (UIView *)contentView{
    if (!_contentView) {
        CGFloat maxW = 50 + self.dataArr.count *70;
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(15, kHEIGHT - Home_Indicator_HEIGHT - 70 - maxW, KWIDTH-30, maxW)];
        _contentView.backgroundColor = APPTintColor;
        _contentView.layer.masksToBounds = YES;
        _contentView.layer.cornerRadius = 4.0;
    }
    return _contentView;
}

- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, self.contentView.width, 50))];
        _footerView.backgroundColor = APPTintColor;
        [_footerView addSubview:self.priceLab];
        [_footerView addSubview:self.numberLab];
        [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.footerView.mas_centerY);
            make.left.equalTo(self.footerView.mas_left).offset(10);
        }];
        [self.numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.footerView.mas_centerY);
            make.right.equalTo(self.footerView.mas_right).offset(-10);
        }];
    }
    return _footerView;
}
- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [UILabel new];
        _priceLab.textColor = [UIColor whiteColor];
        _priceLab.font = [UIFont systemFontOfSize:16];
        _priceLab.text = @"合计：¥1.00";
    }
    return _priceLab;
}
- (UILabel *)numberLab {
    if (!_numberLab) {
        _numberLab = [UILabel new];
        _numberLab.textColor = [UIColor whiteColor];
        _numberLab.font = [UIFont systemFontOfSize:16];
        _numberLab.text = @"共1项";
    }
    return _numberLab;
}

@end



@implementation YXCatePopViewViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = APPTintColor;
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.typeLab];
        [self.contentView addSubview:self.deleBtn];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];

    }
    return _imgView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont systemFontOfSize:14.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = [UIColor whiteColor];
    }
    return _nameLab;
}

- (UILabel *)typeLab {
    if (!_typeLab) {
        _typeLab = [[UILabel alloc] init];
        _typeLab.textColor = [UIColor whiteColor];
        _typeLab.text = @"已禁用";
        _typeLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _typeLab;
}

- (UIButton *)deleBtn {
    if (!_deleBtn) {
        _deleBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_deleBtn setImage:[UIImage imageNamed:@"del_ffffff"] forState:(UIControlStateNormal)];
        [_deleBtn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _deleBtn;
}

- (void)click {
    if (self.delectBlock) {
        self.delectBlock();
    }
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(20);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(15);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_typeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        make.top.equalTo(weakSelf.nameLab.mas_bottom).offset(5);
        [weakSelf.typeLab sizeToFit];
    }];

    [_deleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];

}
@end
