//
//  YXHomePopView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^sureBlock)(NSString *string);
typedef void(^cancelBlock)(void);

@interface YXHomePopView : UIView


- (instancetype)initWithFrame:(CGRect)frame Title:(NSString *)title dataArr:(NSArray *)dataArr;
/**
 弹出视图
 */
-(void)showAnimation;


@end

NS_ASSUME_NONNULL_END
