//
//  YXHomePopView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomePopView.h"
#import "YXCouponsTableViewCell.h"
#import "YXCouponsViewModel.h"
#import "YXCouponsModel.h"
@interface YXHomePopView ()<UITableViewDelegate,UITableViewDataSource>
/** 弹窗视图*/
@property (nonatomic ,strong) UIView *alertView;
/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UITableView *tableView;
/** 关闭按钮*/
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@end

@implementation YXHomePopView

- (instancetype)initWithFrame:(CGRect)frame Title:(NSString *)title dataArr:(NSArray *)dataArr
{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataArr = [NSMutableArray arrayWithArray:dataArr];
        
        self.alertView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 110 + (70 * dataArr.count))];
        self.alertView.backgroundColor = APPTintColor;
        self.alertView.layer.cornerRadius=10.0;
        self.alertView.layer.masksToBounds=YES;
        self.alertView.userInteractionEnabled=YES;
        [self addSubview:self.alertView];
        
        UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
        titleLab.backgroundColor = APPTintColor;
        titleLab.text = title;
        titleLab.font = [UIFont systemFontOfSize:16];
        titleLab.textAlignment = NSTextAlignmentCenter;
        titleLab.textColor = [UIColor whiteColor];
        [self.alertView addSubview:titleLab];
        self.titleLab = titleLab;
        
        
        UIView *footer = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, self.alertView.frame.size.width - 20 , 30))];
        UILabel *messageLab = [[UILabel alloc]  initWithFrame:(CGRectMake(10, 0, self.alertView.frame.size.width - 20, 30))];
        messageLab.text = @"图论社福利";
        messageLab.font = [UIFont systemFontOfSize:12.0];
        messageLab.textColor = color_TextTwo;
        [footer addSubview:messageLab];
        
        self.tableView = [[UITableView alloc] initWithFrame:(CGRectMake(10, 40, self.alertView.frame.size.width - 20 , 30 + 70 * dataArr.count))];
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = footer;
        [self.tableView registerClass:[YXCouponsTableViewCell class] forCellReuseIdentifier:@"YXCouponsTableViewCell"];
        [self.alertView addSubview: self.tableView];

        
        self.closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        self.closeBtn.frame = CGRectMake(0, self.alertView.height - 40,300 , 40);
        [self.closeBtn setTitle:@"关闭" forState:(UIControlStateNormal)];
        [self.closeBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        self.closeBtn.backgroundColor = APPTintColor;
        self.closeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [self.closeBtn addTarget:self action:@selector(cancelClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.alertView addSubview:self.closeBtn];
                
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXCouponsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCouponsTableViewCell"];
    if (!cell) {
        cell = [[YXCouponsTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXCouponsTableViewCell"];
    }
    cell.type = 1;
    YXCouponsModelList *model = self.dataArr[indexPath.row];
    cell.model = model;
    DDWeakSelf
    [cell setClickReceiveBlock:^{
        [YXCouponsViewModel queryReceiveCouponWithType_id:model.type_id Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                [YJProgressHUD showMessage:@"领取成功"];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            [UIView animateWithDuration:0.3 animations:^{
                [weakSelf removeFromSuperview];
            }];
        } failure:^(NSError * _Nonnull error) {
            
        }];
    }];
    return cell;
}

- (void)cancelClick:(UIButton *)sender{
    
//    if (self.cancelClick) {
//        self.cancelClick();
//    }
//
    [UIView animateWithDuration:0.3 animations:^{
        [self removeFromSuperview];
    }];
    
}
-(void)showAnimation{
    
    self.backgroundColor = [UIColor clearColor];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    
    self.alertView.layer.position = self.center;

    self.alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.alertView.alpha = 0;
    YXWeakSelf
    [UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        weakSelf.alertView.transform = transform;
        weakSelf.alertView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if (event.allTouches.count) {
        for (NSSet *set in event.allTouches) {
            UITouch *touch = (UITouch *)set;
            if (touch.view != self.alertView) {
                [UIView animateWithDuration:0.3 animations:^{
                    [self removeFromSuperview];
                }];
            }
        }
    }
    
}

@end
