//
//  YXHomeHeaerView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeHeaerView.h"
#import "YXHotKeyModel.h"
#import <YYLabel.h>
#import "NSAttributedString+YXExtension.h"

@interface YXHomeHeaerView ()

@end

@implementation YXHomeHeaerView

- (void)setDataArr:(NSArray *)dataArr {
    _dataArr = dataArr;
    
    if (_dataArr.count) {
        for (int i = 0; i < _dataArr.count; i ++){
            YXHotKeyModel *model = _dataArr[i];
            NSString *name;
            if (i == _dataArr.count -1) {
               name = [NSString stringWithFormat:@" %@",model.name];
            }else {
               name = [NSString stringWithFormat:@" %@ |",model.name];
            }
            static UIButton *recordBtn =nil;
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
            btn.titleLabel.font = [UIFont systemFontOfSize:12.0];

            CGRect rect = [name boundingRectWithSize:CGSizeMake(KWIDTH-55, 30) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:btn.titleLabel.font} context:nil];
            if (i == 0) {
                btn.frame =CGRectMake(45, 8, rect.size.width, rect.size.height);
            } else{
                CGFloat yuWidth = KWIDTH - 25-recordBtn.frame.origin.x -recordBtn.frame.size.width;
                if (yuWidth >= rect.size.width) {
                    btn.frame =CGRectMake(recordBtn.frame.origin.x +recordBtn.frame.size.width + 0, recordBtn.frame.origin.y, rect.size.width, rect.size.height);
                }else{
                    btn.frame =CGRectMake(0, recordBtn.frame.origin.y+recordBtn.frame.size.height+10, rect.size.width, rect.size.height);
                }
                
            }
//            btn.backgroundColor = [UIColor yellowColor];
            [btn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
            [btn setTitle:name forState:UIControlStateNormal];
            btn.tag = i;
            [self addSubview:btn];
            [btn addTarget:self action:@selector(click:) forControlEvents:(UIControlEventTouchUpInside)];
            recordBtn = btn;
        }
    }
   
}

- (void)click:(UIButton *)sender {
    NSLog(@"%@",sender.titleLabel.text);
    YXHotKeyModel *model = self.dataArr[sender.tag];
    if (self.selectHotKey) {
        self.selectHotKey(model);
    }
    
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = APPTintColor;
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout {
    
    YXWeakSelf
    [self addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.titleLab sizeToFit];
    }];

}

#pragma mark - Lazy Loding
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:12.0f];
        _titleLab.textColor = color_TextOne;
        _titleLab.text = @"热搜:";
    }
    return _titleLab;
}

@end
