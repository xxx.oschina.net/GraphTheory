//
//  YXHomeDataCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeDataCollectionViewCell.h"

@interface YXHomeDataCollectionViewCell ()


@end

@implementation YXHomeDataCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    switch (index.row) {
        case 1:{
            _titleLab.text = @"当前单数";
            _detailLab.text = @"3";
        }
            break;
        case 2:{
            _titleLab.text = @"新服务商";
            _detailLab.text = @"3";
        }
            break;
        case 0:{
            _titleLab.text = @"交易总量";
            _detailLab.text = @"3";
        }
            break;
        default:
            break;
    }
}

/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.detailLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-15);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.backView.mas_top).offset(8);
    }];
    
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"代发货";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.text = @"3";
        _detailLab.textColor = color_TextOne;
        _detailLab.font = [UIFont boldSystemFontOfSize:18.0f];
    }
    return _detailLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

@end
