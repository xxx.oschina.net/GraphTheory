//
//  YXHomeHeaerView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXHotKeyModel;
@interface YXHomeHeaerView : UIView

@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) NSArray *dataArr;

@property (nonatomic ,copy)void(^selectHotKey)(YXHotKeyModel *model);

@end

NS_ASSUME_NONNULL_END
