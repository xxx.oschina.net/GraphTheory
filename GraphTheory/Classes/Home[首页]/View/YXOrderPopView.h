//
//  YXOrderPopView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderPopView : UIView

@property (nonatomic ,copy)void(^clickBtnBlock)(void);

- (instancetype)initWithFrame:(CGRect)frame Title:(NSString *)title;

/**
 弹出视图
 */
-(void)showAnimation;
@end

NS_ASSUME_NONNULL_END
