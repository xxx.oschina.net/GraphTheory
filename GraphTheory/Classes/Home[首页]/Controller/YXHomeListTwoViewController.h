//
//  YXHomeListTwoViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"

typedef NS_ENUM(NSInteger,HomeType2) {
    HomeType2StateStudy,      // 学习
    HomeType2StateLife,       // 生活
    HomeType2StateCY,         // 创业
    HomeType2StateC2M,        // C2M

};
@class YXApplyRoleInfoModel;
@interface YXHomeListTwoViewController : DDBaseViewController<JXCategoryListContentViewDelegate>
// 页面类型
@property (nonatomic ,assign) HomeType2 type;
@property (nonatomic ,strong) NSString *city_ids;
@property (nonatomic ,strong) YXApplyRoleInfoModel *roleModel;
// 更新scrollView 高度
@property (nonatomic ,copy)void(^updateContentSize)(CGFloat height);

@end

