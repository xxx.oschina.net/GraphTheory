//
//  YXHomeListTwoViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeListTwoViewController.h"
#import "YXSelectCityViewController.h"
#import "YXCategoryViewController.h"
#import "YXCateDetailViewController.h"
#import "YXAllCateViewController.h"
#import "YXOrderDetailsViewController.h"
#import "YXAccountLogViewController.h"
#import "YXPublishViewController.h"
#import "YXInputViewController.h"
#import "YXMediaDetailsTwoViewController.h"
#import "YXCollectionViewController.h"
#import "YXFansViewController.h"
#import "YXFocusViewController.h"
#import "YXOrderViewController.h"
#import "YXMyCardViewController.h"
#import "YXAddProductViewController.h"
#import "YXOrderGoodsDetailsViewController.h"

#import "WGStroeHeadCollectionReusableView.h"
#import "YXHomeLiftCollectionViewCell.h"
#import "YXHomeCYCollectionViewCell.h"
#import "YXHomeBottomCollectionViewCell.h"
#import "YXVideoCollectionViewCell.h"
#import "YXCustomAlertActionView.h"
#import "YXTitleView.h"
#import "YXOrderPopView.h"
#import "YXProductCollectionViewCell.h"
#import "YXGoodsOrderCollectionViewCell.h"

#import <JJCollectionViewRoundFlowLayout.h>
#import "UIScrollView+DRRefresh.h"
#import "YXHomeViewModel.h"
#import "YXHomeModel.h"
#import "YXShareOrdersModel.h"
#import "YXOrderViewModel.h"
#import "YXApplyRoleInfoModel.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
#import "YXGoodsViewModel.h"
#import "YXProductModel.h"
#import "YXUserOrderModel.h"
#import "YXUserOrderViewModel.h"
@interface YXHomeListTwoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) YXCustomAlertActionView *alertView;
@property (strong, nonatomic) YXHomeModel *homeModel;
@property (strong, nonatomic) YXShareOrdersModelList *orderModel;
@property (strong, nonatomic) YXUserInfoModel *userInfo;
@property (strong, nonatomic) YXVideoModel *videoModel;
@property (strong, nonatomic) YXUserOrderModelList *goodsOrderModel;
@property (nonatomic ,assign) NSInteger status;
@property (nonatomic ,assign) NSInteger page;
@property(nonatomic,strong)NSArray *titleArr;
@property(nonatomic,assign)NSInteger videoNum;
@property(nonatomic,assign)NSInteger goodsNum;
@property(nonatomic,assign)NSInteger orderNum;
@property (strong, nonatomic) NSMutableArray *listArr;
@property (strong, nonatomic) NSMutableArray *listArr1;
@property (strong, nonatomic) NSMutableArray *listArr2;
@property (strong, nonatomic) NSMutableArray *listArr3;

@end

@implementation YXHomeListTwoViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
    [self changeSize];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.status = 0;
    self.page = 1;
    self.videoNum = 0;
    self.goodsNum = 0;
    self.orderNum = 0;
    self.listArr1 = [NSMutableArray array];
    self.listArr2 = [NSMutableArray array];
    self.listArr3 = [NSMutableArray array];
    [self loadSubViews];
}

- (void)loadData {
    
    DDWeakSelf
    if (self.type == HomeType2StateCY || self.type == HomeType2StateC2M) {
        
        // 查询用户信息
        [YXUserViewModel queryUserInfoCompletion:^(id responesObj) {
            weakSelf.userInfo = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        } failure:^(NSError *error) {
            
        }];
        
        if (self.type == HomeType2StateCY) {
                        
            // 查询抢单列表信息
            [YXHomeViewModel queryGrabOrderListCompletion:^(id  _Nonnull responesObj) {
                [weakSelf.listArr removeAllObjects];
                YXShareOrdersModel *model = [YXShareOrdersModel mj_objectWithKeyValues:responesObj[@"data"]];
                weakSelf.listArr = [YXShareOrdersModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.collectionView reloadData];
            } Failure:^(NSError * _Nonnull error) {
                
            }];
        }else {
//            if (self.status == 0) {
//                [self queryMyVideo];
//            }else if (self.status == 1) {
//                [self queryMyGoodsList];
//            }else {
//                [self queryMyVideoOrder];
//            }
            
            dispatch_group_t group = dispatch_group_create();
            dispatch_group_enter(group);
            [YXVideoViewModel queryMyVideoListWithPage:@(self.page).stringValue limit:@"10" is_delete:@"1" Completion:^(id  _Nonnull responesObj) {
                if (REQUESTDATASUCCESS) {
                    [weakSelf.listArr1 removeAllObjects];
                    weakSelf.listArr1 = [YXVideoModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
                }
                dispatch_group_leave(group);
            } failure:^(NSError * _Nonnull error) {
                dispatch_group_leave(group);
            }];
            dispatch_group_enter(group);
            [YXGoodsViewModel queryMyGoodsListWithPage:@(self.page).stringValue Completion:^(id  _Nonnull responesObj) {
                [weakSelf.listArr2 removeAllObjects];
                if (REQUESTDATASUCCESS) {
                    YXProductModel *model = [YXProductModel mj_objectWithKeyValues:responesObj[@"data"]];
                    NSArray *tem = [YXProductModelList mj_objectArrayWithKeyValuesArray:model.list];
                    weakSelf.listArr2 = [NSMutableArray arrayWithArray:tem];
                }
                dispatch_group_leave(group);
            } failure:^(NSError * _Nonnull error) {
                dispatch_group_leave(group);
            }];
            dispatch_group_enter(group);
            [YXGoodsViewModel queryMyVideoOrderListWithPage:@(self.page).stringValue is_delete:@"1" Completion:^(id  _Nonnull responesObj) {
                [weakSelf.listArr3 removeAllObjects];
                if (REQUESTDATASUCCESS) {
                    YXUserOrderModel *model = [YXUserOrderModel mj_objectWithKeyValues:responesObj[@"data"]];
                    weakSelf.listArr3 = [NSMutableArray arrayWithArray:model.list];
                }
                dispatch_group_leave(group);
            } failure:^(NSError * _Nonnull error) {
                dispatch_group_leave(group);
            }];
            
            dispatch_group_notify(group, dispatch_get_main_queue(), ^{
                weakSelf.videoNum =weakSelf.listArr1.count;
                weakSelf.goodsNum =weakSelf.listArr2.count;
                weakSelf.orderNum =weakSelf.listArr3.count;
                NSString *good = [NSString stringWithFormat:@"视频(%ld)",weakSelf.videoNum];
                NSString *middle = [NSString stringWithFormat:@"商品(%ld)",weakSelf.goodsNum];
                NSString *bad = [NSString stringWithFormat:@"订单(%ld)",weakSelf.orderNum];
                weakSelf.titleArr = @[good,middle,bad];
                [weakSelf.collectionView reloadData];
                CGFloat height = weakSelf.collectionView.collectionViewLayout.collectionViewContentSize.height;
                NSLog(@"%f",height);
                if (weakSelf.updateContentSize) {
                    weakSelf.updateContentSize(height);
                }
            });
            
        }
    }
    
}

//// 查询视频列表
//- (void)queryMyVideo {
//    DDWeakSelf
//    [YXVideoViewModel queryMyVideoListWithPage:@(self.page).stringValue limit:@"10" is_delete:@"1" Completion:^(id  _Nonnull responesObj) {
//        if (REQUESTDATASUCCESS) {
//            [weakSelf.listArr removeAllObjects];
//            weakSelf.listArr = [YXVideoModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
//            weakSelf.videoNum = weakSelf.listArr.count;
//        }
//        [weakSelf.collectionView reloadData];
//    } failure:^(NSError * _Nonnull error) {
//
//    }];
//
//}
//
//// 查询商品列表
//- (void)queryMyGoodsList {
//
//    DDWeakSelf
//    [YXGoodsViewModel queryMyGoodsListWithPage:@(self.page).stringValue Completion:^(id  _Nonnull responesObj) {
//        [weakSelf.listArr removeAllObjects];
//        if (REQUESTDATASUCCESS) {
//            YXProductModel *model = [YXProductModel mj_objectWithKeyValues:responesObj[@"data"]];
//            NSArray *tem = [YXProductModelList mj_objectArrayWithKeyValuesArray:model.list];
//            weakSelf.listArr = [NSMutableArray arrayWithArray:tem];
//            weakSelf.goodsNum =tem.count;
//        }
//        [weakSelf.collectionView reloadData];
//    } failure:^(NSError * _Nonnull error) {
//    }];
//}
//
//// 查询商品订单列表
//- (void)queryMyVideoOrder {
//
//    DDWeakSelf
//    [YXGoodsViewModel queryMyVideoOrderListWithPage:@(self.page).stringValue is_delete:@"1" Completion:^(id  _Nonnull responesObj) {
//        [weakSelf.listArr removeAllObjects];
//        if (REQUESTDATASUCCESS) {
//            YXUserOrderModel *model = [YXUserOrderModel mj_objectWithKeyValues:responesObj[@"data"]];
//            weakSelf.listArr = [NSMutableArray arrayWithArray:model.list];
//            weakSelf.orderNum =weakSelf.listArr.count;
//        }
//        [weakSelf.collectionView reloadData];
//    } failure:^(NSError * _Nonnull error) {
//
//    }];
//
//}


// 抢单
- (void)grabSubOrder {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel queryGrabSubOrderWithId:self.orderModel.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
            YXOrderPopView *popView = [[YXOrderPopView alloc] initWithFrame:[UIScreen mainScreen].bounds Title:@"抢单结果"];
            [popView showAnimation];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

// 删除视频
- (void)deleteVideo {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXVideoViewModel queryDeleteVideoWithId:self.videoModel.v_id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
    
}
// 删除订单
- (void)deleteOrder {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryDeleteSubOrderWithId:self.goodsOrderModel.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}


/**
 显示弹窗

 @param type        类型 0.删除视频 1.删除订单
 @param message     提示语
 */
- (void)showAlertViewWithType:(NSInteger)type Message:(NSString *)message{
    
    _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:message sureBtn:@"确认" cancleBtn:@"取消"];
    [_alertView showAnimation];
    YXWeakSelf
    [_alertView setSureClick:^(NSString * _Nonnull string) {
        if (type == 0) {
            [weakSelf deleteVideo];
        }else if (type == 1) {
            [weakSelf deleteOrder];
        }
    }];
    
}



#pragma mark - 设置UI
- (void)loadSubViews {
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
}

// 更新collectionView的高度
- (void)viewWillLayoutSubviews  {
    [super viewWillLayoutSubviews];
    [self changeSize];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self changeSize];
}


- (void)changeSize {
    [self.view layoutIfNeeded];
    CGFloat height = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    NSLog(@"%f",height);
    if (self.updateContentSize) {
        self.updateContentSize(height);
    }
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    if (section == 0){
        return 1;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        if (self.type == HomeType2StateCY) {
            return 1;
        }else if (self.type == HomeType2StateC2M) {
//            if (self.listArr.count) {
//                return self.listArr.count;
//            }else {
//                return 1;
//            }
            if (self.status == 0) {
                return self.listArr1.count;
            }else if (self.status == 1) {
                return self.listArr2.count;
            }else {
                return self.listArr3.count;
            }
            
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (self.type == HomeType2StateCY) {
        if (indexPath.section == 0){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }else if (indexPath.section == 1){
             return CGSizeMake((SCREEN_WIDTH - 30), 45);
         }else if (indexPath.section == 2){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }
    }else if (self.type == HomeType2StateC2M) {
        if (indexPath.section == 0){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }else if (indexPath.section == 1){
             return CGSizeMake((SCREEN_WIDTH - 30), 45);
         }else if (indexPath.section == 2){
//             if (self.listArr.count) {
//                 if (self.status ==0) {
//                    return CGSizeMake((SCREEN_WIDTH - 30 -45) / 2, 220);
//                 }else if (self.status == 1) {
//                    return CGSizeMake((SCREEN_WIDTH - 30), 100);
//                 }else {
//                     return CGSizeMake((SCREEN_WIDTH - 30), 170);
//                 }
//             }else {
//                 return CGSizeMake((SCREEN_WIDTH - 30), 220);
//             }
             if (self.status ==0) {
                return CGSizeMake((SCREEN_WIDTH - 30 -45) / 2, 220);
             }else if (self.status == 1) {
                return CGSizeMake((SCREEN_WIDTH - 30), 100);
             }else {
                 return CGSizeMake((SCREEN_WIDTH - 30), 170);
             }
         }
    }

    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.type == HomeType2StateCY) {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }else {
        if (section == 2) {
            if (self.status == 0) {
                return UIEdgeInsetsMake(15, 30, 30, 30);
            }else {
                return UIEdgeInsetsMake(0, 15, 15, 15);
            }
        }
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (self.type == HomeType2StateC2M) {
        if (section == 2) {
            if (self.status == 0) {
                return 15;
            }
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (self.type == HomeType2StateCY) {
        if (section == 2){
            return CGSizeMake(KWIDTH - 30, 40);
        }
    }else if (self.type == HomeType2StateC2M) {
        if (section == 2){
            return CGSizeMake(KWIDTH - 30, 40);
        }
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (self.type == HomeType2StateCY) {
            if (indexPath.section == 2) {
                WGStroeHeadCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGStroeHeadCollectionReusableView" forIndexPath:indexPath];
                header.type = self.type;
                header.index = indexPath;
                [header setClickCheckBtnBlock:^{
                   
                }];
                return header;
            }
        }else if (self.type == HomeType2StateC2M) {
            if (indexPath.section == 2) {
                YXTitleView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXTitleView" forIndexPath:indexPath];
                header.titleArr = self.titleArr;
                [header setClickBtnBlock:^(NSInteger index) {
                    weakSelf.status = index;
                    [weakSelf loadData];
//                    if (index == 0) {
//                        [weakSelf queryMyVideo];
//                    }else if (index == 1){
//                        [weakSelf queryMyGoodsList];
//                    }else {
//                        [weakSelf queryMyVideoOrder];
//                    }
                }];
                return header;
            }
        }
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DDWeakSelf
    if (self.type == HomeType2StateCY || self.type == HomeType2StateC2M){
        if (indexPath.section == 0) {
            YXHomeCYCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeCYCollectionViewCell" forIndexPath:indexPath];
            cell.type = self.type;
            cell.userInfo = self.userInfo;
            cell.roleModel = self.roleModel;
            [cell setClickBackBlock:^(NSInteger index) {
                YXMyCardViewController *vc = [YXMyCardViewController new];
                vc.user_id = [YXUserInfoManager getUserInfo].user_id;
                if (index == 2) {
                    vc.type = @"1";
                }else {
                    vc.type = @"2";
                }
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
            
            [cell setClickBtnBlock:^(NSInteger index) {
                if (weakSelf.type == HomeType2StateCY) {
                    if (index == 0) {
                        YXAccountLogViewController *vc = [YXAccountLogViewController new];
                        vc.type = 0;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (index == 1) {
                        weakSelf.tabBarController.selectedIndex = 2;
                    }else {
                        weakSelf.tabBarController.selectedIndex = 2;
                        RTContainerController *rtController = (RTContainerController *)[UIView currentViewController];
                        YXOrderViewController *vc = rtController.contentViewController;
                        vc.categoryView.defaultSelectedIndex = 3;
                        [vc.categoryView reloadData];
                    }
                }else {
                    if (index == 0) {
                        YXCollectionViewController *vc = [YXCollectionViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (index == 1) {
                        YXFocusViewController *vc = [YXFocusViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else {
                        YXFansViewController *vc = [YXFansViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }
                }
            }];
            return cell;
        }else if (indexPath.section == 1) {
            YXHomeBottomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell" forIndexPath:indexPath];
            cell.type = self.type;
            return cell;
        }else if (indexPath.section == 2) {
            if (self.type == HomeType2StateCY) {
                YXHomeLiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell" forIndexPath:indexPath];
                cell.type = self.type;
                cell.dataArr = self.listArr;
                [cell setClickOrderBtnBlock:^(NSIndexPath * _Nonnull indexPath) {
                    weakSelf.orderModel = weakSelf.listArr[indexPath.row];
                    [weakSelf grabSubOrder];
                }];
                return cell;
            }else {
//                if (self.listArr.count) {
                    if (self.status == 0) {
                        YXVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXVideoCollectionViewCell" forIndexPath:indexPath];
                        cell.model = self.listArr1[indexPath.row];
                        [cell setEditorBtnBlock:^(YXVideoModel * _Nonnull model) {
                            YXPublishViewController *vc = [YXPublishViewController new];
                            vc.type = 1;
                            vc.videoModel = model;
                            [weakSelf.navigationController pushViewController:vc animated:YES];
                        }];
                        [cell setDeleteBtnBlock:^(YXVideoModel * _Nonnull model) {
                            weakSelf.videoModel = model;
                            [weakSelf showAlertViewWithType:0 Message:@"是否确认删除此视频？"];
                        }];
                        return cell;
                    }else if (self.status == 1) {
                        YXProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXProductCollectionViewCell" forIndexPath:indexPath];
                        cell.model = self.listArr2[indexPath.row];
                        return cell;
                    }else {
                        YXGoodsOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXGoodsOrderCollectionViewCell" forIndexPath:indexPath];
                        cell.indexPath = indexPath;
                        cell.model = self.listArr3[indexPath.row];
                        [cell setClickDeleteBtn:^(NSIndexPath * _Nonnull indexPath) {
                            weakSelf.goodsOrderModel = weakSelf.listArr3[indexPath.row];
                            [weakSelf showAlertViewWithType:1 Message:@"是否确认删除此商品？"];
                        }];
                        [cell setClickBottomBtnBlock:^(NSIndexPath * _Nonnull indexPath, UIButton * _Nonnull btn) {
                                    
                        }];
                        return cell;
                    }

//                }else {
//                    YXHomeLiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell" forIndexPath:indexPath];
//                    cell.type = self.type;
//                    cell.dataArr = self.listArr;
//                    return cell;
//                }
            }
         
        }
    }

    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.type == HomeType2StateCY) {
        if (indexPath.section == 1) {
            YXInputViewController *vc = [YXInputViewController new];
            vc.type = 3;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (self.type == HomeType2StateC2M) {
        if (indexPath.section == 1) {
            YXPublishViewController *vc = [YXPublishViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 2) {
            if (self.status == 0) {
                YXVideoModel *model = self.listArr1[indexPath.row];
                YXMediaDetailsTwoViewController *vc = [YXMediaDetailsTwoViewController new];
                vc.Id = model.v_id;
                [self.navigationController pushViewController:vc animated:YES];
            }else if (self.status == 1) {
                YXProductModelList *model = self.listArr2[indexPath.row];
                YXAddProductViewController *vc = [YXAddProductViewController new];
                vc.type = @"1";
                vc.model = model;
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                YXUserOrderModelList *model = self.listArr3[indexPath.row];
                YXOrderGoodsDetailsViewController *vc = [YXOrderGoodsDetailsViewController new];
                vc.type = 0;
                vc.orderId = model.Id;
                [self.navigationController pushViewController:vc animated:YES];
            }
  
        }
    }

}
#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        JJCollectionViewRoundFlowLayout *layout = [[JJCollectionViewRoundFlowLayout alloc] init];
//        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.isCalculateHeader = YES;
        layout.isCalculateFooter = NO;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.scrollEnabled = NO;
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 175)];
        _collectionView.backgroundView = bgView;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = CGRectMake(0, 0, KWIDTH, 175);
        gradient.colors = [NSArray arrayWithObjects:
                           (id)APPTintColor.CGColor,
                           (id)[UIColor whiteColor].CGColor,
                           (id)[UIColor whiteColor].CGColor, nil];
        [bgView.layer addSublayer:gradient];
        
        [_collectionView registerClass:[YXHomeLiftCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXHomeCYCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXHomeCYCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXHomeBottomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell"];
        [_collectionView registerClass:[WGStroeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGStroeHeadCollectionReusableView"];
        [_collectionView registerClass:[YXTitleView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXTitleView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXVideoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXVideoCollectionViewCell"];
        [_collectionView registerClass:[YXProductCollectionViewCell class] forCellWithReuseIdentifier:@"YXProductCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXGoodsOrderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXGoodsOrderCollectionViewCell"];

    }
    return _collectionView;
}

- (NSMutableArray *)listArr {
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
- (NSArray *)titleArr {
    if (!_titleArr) {
        _titleArr = @[@"视频(0)",@"商品(0)",@"订单(0)"];
    }
    return _titleArr;
}

- (UIView *)listView {
    return self.view;
}

@end
