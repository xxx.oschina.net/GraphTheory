//
//  YXHomeListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeListViewController.h"
#import "YXSelectCityViewController.h"
#import "YXCategoryViewController.h"
#import "YXCateDetailViewController.h"
#import "YXAllCateViewController.h"
#import "YXOrderDetailsViewController.h"
#import "YXAccountLogViewController.h"
#import "YXPublishViewController.h"
#import "YXInputViewController.h"
#import "YXMediaDetailsTwoViewController.h"
#import "YXCollectionViewController.h"
#import "YXFansViewController.h"
#import "YXFocusViewController.h"
#import "YXOrderViewController.h"
#import "YXMyCardViewController.h"

#import "WGStoreOrderCollectionViewCell.h"
#import "WGStroeHeadCollectionReusableView.h"
#import "WGStoreMenuCollectionViewCell.h"
#import "WGHomeBannerCollectionViewCell.h"
#import "YXStoreActivityCollectionViewCell.h"
#import "YXHomeMenuCollectionViewCell.h"
#import "YXHomeDataCollectionViewCell.h"
#import "YXHomeOrderCollectionViewCell.h"
#import "YXHomeLiftCollectionViewCell.h"
#import "YXHomeCYCollectionViewCell.h"
#import "YXHomeBottomCollectionViewCell.h"
#import "YXVideoCollectionViewCell.h"
#import "YXCustomAlertActionView.h"
#import "YXTitleView.h"
#import "YXOrderPopView.h"

#import <JJCollectionViewRoundFlowLayout.h>
#import "UIScrollView+DRRefresh.h"
#import "YXHomeViewModel.h"
#import "YXHomeModel.h"
#import "YXShareOrdersModel.h"
#import "YXOrderViewModel.h"
#import "YXApplyRoleInfoModel.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
@interface YXHomeListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) YXCustomAlertActionView *alertView;
@property (strong, nonatomic) YXHomeModel *homeModel;
@property (strong, nonatomic) NSMutableArray *listArr;
@property (strong, nonatomic) YXShareOrdersModelList *orderModel;
@property (strong, nonatomic) YXUserInfoModel *userInfo;
@property (strong, nonatomic) YXVideoModel *videoModel;
@property (nonatomic ,strong) NSString *status;
@end

@implementation YXHomeListViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
    [self changeSize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    if (self.type == HomeTypeStateStudy) {
        self.status = @"study";
    }else if (self.type == HomeTypeStateLife) {
        self.status = @"life";
    }
    
    [self loadSubViews];
}

- (void)loadData {
    
    DDWeakSelf
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        [YXHomeViewModel queryHomeCatesWithType:self.status Completion:^(id  _Nonnull responesObj) {
            weakSelf.homeModel = [YXHomeModel mj_objectWithKeyValues:responesObj[@"data"]];
            if (weakSelf.homeModel.cateList.count) {
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:weakSelf.homeModel.cateList];
                YXCateListModel *listModel = [YXCateListModel new];
                listModel.Id = @"0";
                listModel.name = @"更多";
                listModel.img = @"更多";
                [tempArr addObject:listModel];
                weakSelf.homeModel.cateList = tempArr;
            }
            [weakSelf.collectionView reloadData];
        } failure:^(NSError * _Nonnull error) {
            
        }];
        
        if (self.type == HomeTypeStateStudy) {
            [YXHomeViewModel queryShareOrdersWithPage:@"1" service_ids:nil Completion:^(id  _Nonnull responesObj) {
                YXShareOrdersModel *model = [YXShareOrdersModel mj_objectWithKeyValues:responesObj[@"data"]];
                weakSelf.listArr = [YXShareOrdersModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.collectionView reloadData];
            } Failure:^(NSError * _Nonnull error) {
                
            }];
        }else if (self.type == HomeTypeStateLife) {
            [YXHomeViewModel queryLifeOrdersWithPage:@"1" Completion:^(id  _Nonnull responesObj) {
                YXShareOrdersModel *model = [YXShareOrdersModel mj_objectWithKeyValues:responesObj[@"data"]];
                weakSelf.listArr = [YXShareOrdersModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.collectionView reloadData];
            } Failure:^(NSError * _Nonnull error) {
                    
            }];
        }
        
    }else if (self.type == HomeTypeStateCY || self.type == HomeTypeStateC2M) {
        
        // 查询用户信息
        [YXUserViewModel queryUserInfoCompletion:^(id responesObj) {
            weakSelf.userInfo = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        } failure:^(NSError *error) {
            
        }];
        
        if (self.type == HomeTypeStateCY) {
                        
            // 查询抢单列表信息
            [YXHomeViewModel queryGrabOrderListCompletion:^(id  _Nonnull responesObj) {
                YXShareOrdersModel *model = [YXShareOrdersModel mj_objectWithKeyValues:responesObj[@"data"]];
                weakSelf.listArr = [YXShareOrdersModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.collectionView reloadData];
            } Failure:^(NSError * _Nonnull error) {
                
            }];
        }else {
            
            // 查询视频列表
            [YXVideoViewModel queryMyVideoListWithPage:@"1" limit:@"10" is_delete:@"1" Completion:^(id  _Nonnull responesObj) {
                weakSelf.listArr = [YXVideoModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
                [weakSelf.collectionView reloadData];
            } failure:^(NSError * _Nonnull error) {
                
            }];
        }
    }
    
}

// 抢单
- (void)grabSubOrder {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel queryGrabSubOrderWithId:self.orderModel.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
            YXOrderPopView *popView = [[YXOrderPopView alloc] initWithFrame:[UIScreen mainScreen].bounds Title:@"抢单结果"];
            [popView showAnimation];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

// 删除视频
- (void)deleteVideo {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXVideoViewModel queryDeleteVideoWithId:self.videoModel.v_id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];

    }];
    
}

// 弹窗
- (void)showView {

    _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:@"确认要删除该视频吗？" sureBtn:@"确认" cancleBtn:@"取消"];
    [_alertView showAnimation];
    YXWeakSelf
    [_alertView setSureClick:^(NSString * _Nonnull string) {
        [weakSelf deleteVideo];
    }];
}

#pragma mark - 设置UI
- (void)loadSubViews {
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
}

// 更新collectionView的高度
- (void)viewWillLayoutSubviews  {
    [super viewWillLayoutSubviews];
    [self changeSize];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self changeSize];
}


- (void)changeSize {
    [self.view layoutIfNeeded];
    CGFloat height = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
    NSLog(@"%f",height);
    if (self.updateContentSize) {
        self.updateContentSize(height);
    }
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        return 4;
    }else {
        return 3;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (section == 0){
            return self.homeModel.cateList.count;
        }else if (section == 1){
            return 1;
        }else if (section == 2){
            return 3;
        }else if (section == 3){
            if (self.type == HomeTypeStateStudy) {
                return self.listArr.count;
            }else if (self.type == HomeTypeStateLife) {
                return 1;
            }
        }
    }else  {
        if (section == 0){
            return 1;
        }else if (section == 1){
            return 1;
        }else if (section == 2){
            if (self.type == HomeTypeStateCY) {
                return 1;
            }else if (self.type == HomeTypeStateC2M) {
                return self.listArr.count;
            }
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (indexPath.section == 0){
             return CGSizeMake((SCREEN_WIDTH - 30) /4, 80);
         }else if (indexPath.section == 1){
             return CGSizeMake((SCREEN_WIDTH - 30), 100);
         }else if (indexPath.section == 2){
             return CGSizeMake((SCREEN_WIDTH - 30) / 3, 60);
         }else if (indexPath.section == 3){
             if (self.type == HomeTypeStateStudy) {
                 return CGSizeMake((SCREEN_WIDTH - 30 -45) / 2, 220);
             }else if (self.type == HomeTypeStateLife){
                 return CGSizeMake((SCREEN_WIDTH - 30), 250);
             }
         }
    }else if (self.type == HomeTypeStateCY) {
        if (indexPath.section == 0){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }else if (indexPath.section == 1){
             return CGSizeMake((SCREEN_WIDTH - 30), 45);
         }else if (indexPath.section == 2){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }
    }else if (self.type == HomeTypeStateC2M) {
        if (indexPath.section == 0){
             return CGSizeMake((SCREEN_WIDTH - 30), 220);
         }else if (indexPath.section == 1){
             return CGSizeMake((SCREEN_WIDTH - 30), 45);
         }else if (indexPath.section == 2){
             return CGSizeMake((SCREEN_WIDTH - 30 -45) / 2, 220);
         }
    }

    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (section == 0 || section == 1 || section == 2){
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }else if (section == 3) {
            if (self.type == HomeTypeStateStudy) {
                return UIEdgeInsetsMake(0, 30, 30, 30);
            }else if (self.type == HomeTypeStateLife) {
                return UIEdgeInsetsMake(0, 15, 15, 15);
            }
        }
    }else if (self.type == HomeTypeStateCY) {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }else {
        if (section == 2) {
            return UIEdgeInsetsMake(15, 30, 30, 30);
        }
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (self.type == HomeTypeStateStudy) {
        if (section == 3) {
            return 15;
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (section == 0||section == 1) {
            return CGSizeZero;
        }else if (section == 2|| section == 3){
            return CGSizeMake(KWIDTH - 30, 40);
        }
    }else if (self.type == HomeTypeStateCY) {
        if (section == 2){
            return CGSizeMake(KWIDTH - 30, 40);
        }
    }else if (self.type == HomeTypeStateC2M) {
        if (section == 2){
            return CGSizeMake(KWIDTH - 30, 40);
        }
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife || self.type == HomeTypeStateCY) {
            if (indexPath.section == 2||indexPath.section == 3) {
                WGStroeHeadCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGStroeHeadCollectionReusableView" forIndexPath:indexPath];
                header.type = self.type;
                header.index = indexPath;
                [header setClickCheckBtnBlock:^{
                    if (weakSelf.type == HomeTypeStateStudy) {
                        YXCategoryViewController *vc = [YXCategoryViewController new];
                        vc.type = 0;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (weakSelf.type == HomeTypeStateLife) {
                        
                    }
                }];
                return header;
            }
        }else if (self.type == HomeTypeStateC2M) {
            if (indexPath.section == 2) {
                YXTitleView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXTitleView" forIndexPath:indexPath];
                [header setClickBtnBlock:^(NSInteger index) {

                }];
                return header;
            }
        }
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DDWeakSelf
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (indexPath.section == 0){
             YXHomeMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeMenuCollectionViewCell" forIndexPath:indexPath];
            cell.model = self.homeModel.cateList[indexPath.row];
             return cell;
         }else if (indexPath.section == 1){
             WGHomeBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGHomeBannerCollectionViewCell" forIndexPath:indexPath];
             cell.model = self.homeModel;
             [cell setSelectBlock:^(YXCateBannerListModel * _Nonnull bannerModel) {
                 if ([YXUserInfoManager getUserInfo].token == nil) {
                     KPOP(@"游客不能使用此功能，请先登录");
                     return;
                 }
                 YXCateDetailViewController *vc = [YXCateDetailViewController new];
                 vc.cate_id = bannerModel.Id;
                 [weakSelf.navigationController pushViewController:vc animated:YES];
             }];
             return cell;
         }else if (indexPath.section == 2){
             WGStoreOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell" forIndexPath:indexPath];
             cell.index = indexPath;
             cell.status = self.status;
             cell.model = self.homeModel.realTimeData;
             return cell;
         }else if (indexPath.section == 3){
             if (self.type == HomeTypeStateStudy) {
                 YXHomeOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell" forIndexPath:indexPath];
                 cell.listModel = self.listArr[indexPath.row];
                 return cell;
             }else {
                 YXHomeLiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell" forIndexPath:indexPath];
                 cell.type = self.type;
                 cell.dataArr = self.listArr;
                 return cell;
             }
         
         }
    }else if (self.type == HomeTypeStateCY || self.type == HomeTypeStateC2M){
        if (indexPath.section == 0) {
            YXHomeCYCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeCYCollectionViewCell" forIndexPath:indexPath];
            cell.type = self.type;
            cell.userInfo = self.userInfo;
            cell.roleModel = self.roleModel;
            [cell setClickBackBlock:^(NSInteger index) {
                YXMyCardViewController *vc = [YXMyCardViewController new];
                vc.user_id = [YXUserInfoManager getUserInfo].user_id;
                if (index == 2) {
                    vc.type = @"1";
                }else {
                    vc.type = @"2";
                }
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
            
            [cell setClickBtnBlock:^(NSInteger index) {
                if (weakSelf.type == HomeTypeStateCY) {
                    if (index == 0) {
                        YXAccountLogViewController *vc = [YXAccountLogViewController new];
                        vc.type = 0;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (index == 1) {
                        weakSelf.tabBarController.selectedIndex = 2;
                    }else {
                        weakSelf.tabBarController.selectedIndex = 2;
                        RTContainerController *rtController = (RTContainerController *)[UIView currentViewController];
                        YXOrderViewController *vc = rtController.contentViewController;
                        vc.categoryView.defaultSelectedIndex = 3;
                        [vc.categoryView reloadData];
                    }
                }else {
                    if (index == 0) {
                        YXCollectionViewController *vc = [YXCollectionViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (index == 1) {
                        YXFocusViewController *vc = [YXFocusViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else {
                        YXFansViewController *vc = [YXFansViewController new];
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }
                }
            }];
            return cell;
        }else if (indexPath.section == 1) {
            YXHomeBottomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell" forIndexPath:indexPath];
            cell.type = self.type;
            return cell;
        }else if (indexPath.section == 2) {
            if (self.type == HomeTypeStateCY) {
                YXHomeLiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell" forIndexPath:indexPath];
                cell.type = self.type;
                cell.dataArr = self.listArr;
                [cell setClickOrderBtnBlock:^(NSIndexPath * _Nonnull indexPath) {
                    weakSelf.orderModel = self.listArr[indexPath.row];
                    [weakSelf grabSubOrder];
                }];
                return cell;
            }else {
                YXVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXVideoCollectionViewCell" forIndexPath:indexPath];
                cell.model = self.listArr[indexPath.row];
                [cell setEditorBtnBlock:^(YXVideoModel * _Nonnull model) {
//                    YXPublishViewController *vc = [YXPublishViewController new];
//                    vc.type = 1;
//                    vc.videoModel = model;
//                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }];
                [cell setDeleteBtnBlock:^(YXVideoModel * _Nonnull model) {
                    weakSelf.videoModel = model;
                    [weakSelf showView];
                }];
                return cell;
            }
         
        }
    }

    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([YXUserInfoManager getUserInfo].token == nil) {
        KPOP(@"游客不能使用此功能，请先登录");
        return;
    }
    
    if (self.type == HomeTypeStateStudy || self.type == HomeTypeStateLife) {
        if (indexPath.section == 0) {
            YXCateListModel *listModel = self.homeModel.cateList[indexPath.row];
            if ([listModel.name isEqualToString:@"更多"]) {
                YXAllCateViewController *vc = [YXAllCateViewController new];
                vc.type = self.status;
                vc.city_ids = self.city_ids;
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                YXCateDetailViewController *vc = [YXCateDetailViewController new];
                vc.cate_id = listModel.Id;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else if (indexPath.section == 3) {
            if (self.type == HomeTypeStateStudy) {
                if (self.listArr.count) {
                    YXShareOrdersModelList *model = self.listArr[indexPath.row];
                    YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
                    vc.type = 2;
                    vc.isPay = 1;
                    vc.Id = model.Id;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
        }
    }else if (self.type == HomeTypeStateCY) {
        if (indexPath.section == 1) {
            YXInputViewController *vc = [YXInputViewController new];
            vc.type = 3;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (self.type == HomeTypeStateC2M) {
        if (indexPath.section == 1) {
            YXPublishViewController *vc = [YXPublishViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 2) {
            YXVideoModel *model = self.listArr[indexPath.row];
            YXMediaDetailsTwoViewController *vc = [YXMediaDetailsTwoViewController new];
            vc.Id = model.v_id;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }

}
#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (self.type == HomeTypeStateStudy) {
        if (section == 3) {
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }
    }
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        JJCollectionViewRoundFlowLayout *layout = [[JJCollectionViewRoundFlowLayout alloc] init];
//        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.isCalculateHeader = YES;
        layout.isCalculateFooter = NO;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.scrollEnabled = NO;
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 175)];
        _collectionView.backgroundView = bgView;
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = CGRectMake(0, 0, KWIDTH, 175);
        gradient.colors = [NSArray arrayWithObjects:
                           (id)APPTintColor.CGColor,
                           (id)[UIColor whiteColor].CGColor,
                           (id)[UIColor whiteColor].CGColor, nil];
        [bgView.layer addSublayer:gradient];
        
        [_collectionView registerClass:[YXHomeMenuCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeMenuCollectionViewCell"];
        [_collectionView registerClass:[WGHomeBannerCollectionViewCell class] forCellWithReuseIdentifier:@"WGHomeBannerCollectionViewCell"];
        [_collectionView registerClass:[YXStoreActivityCollectionViewCell class] forCellWithReuseIdentifier:@"YXStoreActivityCollectionViewCell"];
        [_collectionView registerClass:[WGStoreOrderCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell"];
        [_collectionView registerClass:[YXHomeOrderCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell"];
        [_collectionView registerClass:[YXHomeLiftCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeLiftCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXHomeCYCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXHomeCYCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXHomeBottomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell"];
        [_collectionView registerClass:[WGStroeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGStroeHeadCollectionReusableView"];
        [_collectionView registerClass:[YXTitleView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXTitleView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXVideoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXVideoCollectionViewCell"];
        
    }
    return _collectionView;
}

- (NSMutableArray *)listArr {
    if (!_listArr) {
        _listArr = [NSMutableArray array];
    }
    return _listArr;
}
- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
