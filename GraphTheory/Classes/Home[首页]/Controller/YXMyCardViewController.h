//
//  YXMyCardViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXMyCardViewController : DDBaseViewController
@property (nonatomic ,strong) NSString *user_id;
@property (nonatomic ,strong) NSString *type;

@end

NS_ASSUME_NONNULL_END
