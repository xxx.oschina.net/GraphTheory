//
//  DDHomeViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/30.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXHomeViewController.h"
#import "YXCateDetailViewController.h"
#import "DDSearchViewController.h"
#import "YXAccountLogViewController.h"
#import "YXPublishViewController.h"

#import "YXSelectCityViewController.h"
#import "YXCategoryViewController.h"
#import "YXHomeListViewController.h"
#import "YXHomeListTwoViewController.h"
#import "YXEnterViewController.h"
#import "YXUserOrderViewController.h"
#import "YXCollectionViewController.h"
#import "YXSettingViewController.h"
#import "YXServiceViewController.h"
#import "YXShareViewController.h"
#import "YXMyCardViewController.h"

#import <JXCategoryView.h>
#import "JXCategoryTitleCellModel.h"
#import <JJCollectionViewRoundFlowLayout.h>
#import "UIScrollView+DRRefresh.h"
#import "YXAMapManager.h"
#import "YCMenuView.h"
#import "BDLeftMenuView.h"
#import "YXNavCityView.h"
#import "YXSearchView.h"
#import "YXHomeHeaerView.h"
#import "YXHomePopView.h"

#import "YXHomeViewModel.h"
#import "YXHotKeyModel.h"
#import "YXCityModel.h"
#import "YXCouponsViewModel.h"
#import "YXCouponsModel.h"
#import "YXApplyRoleInfoModel.h"
@interface YXHomeViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (strong, nonatomic) UIButton *leftBtn;
@property (strong, nonatomic) YXNavCityView *cityView;
@property (strong, nonatomic) YXSearchView *searchView;
@property (nonatomic ,strong) YXHomeHeaerView *headerView;
@property (nonatomic ,strong) YXHomePopView *popView;

@property (nonatomic, strong) NSArray *titles;//标题数组
@property (nonatomic, strong) NSArray *keys;//标题数组

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic, strong) JXCategoryTitleImageView *categoryView;//类型选择器
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;//类型视图选择器
@property (nonatomic, strong) NSMutableArray *hotKeyArr;
@property (strong, nonatomic) YXApplyRoleInfoModel *role1Model;
@property (strong, nonatomic) YXApplyRoleInfoModel *role2Model;

@end

@implementation YXHomeViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    
    _titles = @[@"学习",@"生活",@"创业",@"C2M"];
    _keys = @[@"0",@"1",@"2",@"3"];
    
    DDWeakSelf
    [[YXAMapManager shareLocation] getLocationInfo:^(YXCitysModel * _Nonnull cityModel) {
        weakSelf.cityView.cityLab.text = cityModel.cityName;
        weakSelf.cityView.weatherLab.text = cityModel.weather;
        [YXUserInfoManager resetUserInfoMessageWithDic:@{@"cityId":cityModel.cityId}];
    }];
    
    [self createNavUI];
          
    [self loadSubViews];
    
    [self loadData];

    [self queryApplyRoleInfo];

    // 启用屏幕边缘拖拽出左侧菜单
    [BDLeftMenuView enableScreenEdgeDraggingInView:self.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onEventLeftMenu:) name:kBDLeftMenuViewEventNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userChangeUpDate) name:DDUSERCHANGE object:nil];

}

- (void)userChangeUpDate {
    [self queryApplyRoleInfo];
    self.categoryView.defaultSelectedIndex = 0;
    [self.categoryView reloadData];
}


- (void)loadData {
    
    DDWeakSelf
    // 热门搜索
    [YXHomeViewModel queryHotRecKeywordCompletion:^(id  _Nonnull responesObj) {
        [weakSelf.hotKeyArr removeAllObjects];
        weakSelf.hotKeyArr = [YXHotKeyModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"hotKeyword"]];
        [weakSelf.headerView setDataArr:weakSelf.hotKeyArr];
    } Failure:^(NSError * _Nonnull error) {
        
    }];
    
    // 查询优惠券
    [YXCouponsViewModel queryCouponListWithPage:@"1" city_id:@"229" Completion:^(id  _Nonnull responesObj) {
        YXCouponsModel *model = [YXCouponsModel mj_objectWithKeyValues:responesObj[@"data"]];
        NSArray *data = [YXCouponsModelList mj_objectArrayWithKeyValuesArray:model.list];
        if (data.count > 0) {
            YXHomePopView *popView = [[YXHomePopView alloc] initWithFrame:[UIScreen mainScreen].bounds Title:@"优惠劵" dataArr:data];
            [popView showAnimation];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
}


// 查询身份认证
- (void)queryApplyRoleInfo {
 
    DDWeakSelf
    // 创业认证
    [YXUserViewModel queryApplyRoleInfoWithRole:@"1" Completion:^(id responesObj) {
        weakSelf.role1Model = [YXApplyRoleInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
    } failure:^(NSError *error) {
                
    }];
    
    // C2M认证
    [YXUserViewModel queryApplyRoleInfoWithRole:@"2" Completion:^(id responesObj) {
        weakSelf.role2Model = [YXApplyRoleInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
    } failure:^(NSError *error) {
                
    }];
    
}


# pragma mark -- 通知事件
- (void)onEventLeftMenu:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    BOOL isUserHeader = [userInfo[@"isUserHeader"] boolValue];
    NSDictionary *data = userInfo[@"data"];
    
    if ([YXUserInfoManager getUserInfo].token == nil) {
        KPOP(@"游客不能使用此功能，请先登录");
        return;
    }
    
    if (isUserHeader) {
        
    }else {
        NSInteger type = [data[@"type"] integerValue];
        if (type == 0) {
            YXAccountLogViewController *vc = [YXAccountLogViewController new];
            vc.type = 2;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (type == 1) {
            YXUserOrderViewController *vc = [YXUserOrderViewController new];
            vc.selectIndex = 0;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (type == 2) {
            YXCollectionViewController *vc = [YXCollectionViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (type == 3) {
            YXServiceViewController *vc = [YXServiceViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (type == 4) {
            YXSettingViewController *vc = [YXSettingViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (type == 5) {
            YXShareViewController *vc = [YXShareViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }

}


- (void)createNavUI {
    
    UIBarButtonItem *leftItem= [[UIBarButtonItem alloc] initWithCustomView:self.leftBtn];
    UIBarButtonItem *cityItem= [[UIBarButtonItem alloc] initWithCustomView:self.cityView];
    self.navigationItem.leftBarButtonItems = @[leftItem,cityItem];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32, 36));
    }];
    [self.cityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];

    DDWeakSelf
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem initLeftItems:@"" titleColor:[UIColor blackColor] titleFount:14.0f imageStr:@"home_right_add" itemClickBlock:^(UIButton * _Nonnull sender) {
        
        if ([YXUserInfoManager getUserInfo].token == nil) {
            KPOP(@"游客不能使用此功能，请先登录");
            return;
        }
        
        NSMutableArray *_sortData = [NSMutableArray new];
        YCMenuAction *all = [YCMenuAction actionWithTitle:@"接单/销劵" image:[UIImage imageNamed:@"home_tanchuang_01"] model:@"1" handler:^(YCMenuAction *action) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"创业者"]) {
                weakSelf.categoryView.defaultSelectedIndex = 2;
                [weakSelf.categoryView reloadData];
            }else {
                [YJProgressHUD showMessage:@"请先申请创业者身份"];
            }
        }];
        [_sortData addObject:all];
        YCMenuAction *get = [YCMenuAction actionWithTitle:@"创业名片" image:[UIImage imageNamed:@"home_tanchuang_02"] model:@"2" handler:^(YCMenuAction *action) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"创业者"]) {
                YXMyCardViewController *vc = [YXMyCardViewController new];
                vc.user_id = [YXUserInfoManager getUserInfo].user_id;
                vc.type = @"1";
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                [YJProgressHUD showMessage:@"您还不是创业者"];
            }
        }];
        [_sortData addObject:get];
        YCMenuAction *area = [YCMenuAction actionWithTitle:@"C2M名片" image:[UIImage imageNamed:@"home_tanchuang_03"] model:@"3" handler:^(YCMenuAction *action) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"自媒体从业者"]) {
                YXMyCardViewController *vc = [YXMyCardViewController new];
                vc.user_id = [YXUserInfoManager getUserInfo].user_id;
                vc.type = @"2";
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                [YJProgressHUD showMessage:@"您还不是自媒体从业者"];
            }
        }];
        [_sortData addObject:area];
        YCMenuAction *dynamic = [YCMenuAction actionWithTitle:@"发布动态" image:[UIImage imageNamed:@"home_tanchuang_04"] model:@"4" handler:^(YCMenuAction *action) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"自媒体从业者"]) {
                YXPublishViewController *vc = [YXPublishViewController new];
                vc.type = 0;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                [YJProgressHUD showMessage:@"您还不是自媒体从业者"];
            }
         
        }];
        [_sortData addObject:dynamic];
        YCMenuView *view = [YCMenuView menuWithActions:_sortData width:140 relyonView:sender];
        view.menuColor = [UIColor darkGrayColor];
        view.textColor = [UIColor whiteColor];

        view.separatorColor = [UIColor whiteColor];

        [view show];
    }];
    
    self.navigationItem.titleView = self.searchView;
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(240, 30));
    }];
    
    
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (BOOL)categoryView:(JXCategoryBaseView *)categoryView canClickItemAtIndex:(NSInteger)index {
    if (index == 0 || index == 1) {
        return YES;
    }else {
        
        if ([YXUserInfoManager getUserInfo].token == nil) {
            KPOP(@"游客不能使用此功能，请先登录");
            return NO;
        }
        
        if (index == 2) {
            if ([self.role1Model.status isEqualToString:@"apply"]) {
                YXEnterViewController *vc = [YXEnterViewController new];
                vc.type = 2;
                [self.navigationController pushViewController:vc animated:YES];
                return NO;
            }else if ([self.role1Model.status isEqualToString:@"waiting"]) {
                [YJProgressHUD showMessage:self.role1Model.title];
                return NO;
            }else if ([self.role1Model.status isEqualToString:@"completed"]) {
                return YES;
            }
        }else {
            if ([self.role2Model.status isEqualToString:@"apply"]) {
                YXEnterViewController *vc = [YXEnterViewController new];
                vc.type = 3;
                [self.navigationController pushViewController:vc animated:YES];
                return NO;
            }else if ([self.role2Model.status isEqualToString:@"waiting"]) {
                [YJProgressHUD showMessage:self.role2Model.title];
                return NO;
            }else if ([self.role2Model.status isEqualToString:@"completed"]) {
                return YES;
            }
            
        }

    }
    return YES;
    
}


#pragma mark - JXCategoryListContainerViewDelegate
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    if (index == 0 || index == 1) {
        YXHomeListViewController *list = [YXHomeListViewController new];
        list.type = index;
        if (index == 2) {
            list.roleModel = self.role1Model;
        }else if (index == 3) {
            list.roleModel = self.role2Model;
        }
        DDWeakSelf
        [list setUpdateContentSize:^(CGFloat height) {
            weakSelf.scrollView.contentSize = CGSizeMake(KWIDTH, height + 110);
        }];
        return list;
    }else {
        YXHomeListTwoViewController *list = [YXHomeListTwoViewController new];
        list.type = index;
        if (index == 2) {
            list.roleModel = self.role1Model;
        }else if (index == 3) {
            list.roleModel = self.role2Model;
        }
        DDWeakSelf
        [list setUpdateContentSize:^(CGFloat height) {
            weakSelf.scrollView.contentSize = CGSizeMake(KWIDTH, height + 110);
        }];
        return list;
    }

}
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

#pragma mark - Intial Methods
//添加子视图
- (void)loadSubViews {
  
    [self.view addSubview:self.backView];
    [self.backView addSubview:self.scrollView];
    [self.scrollView addSubview:self.headerView];
    [self.scrollView addSubview:self.categoryView];
    [self.scrollView addSubview:self.listContainerView];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else {
            make.edges.equalTo(self.view);
        }
    }];

    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.backView);
    }];

    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(KWIDTH, 30));
    }];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerView.mas_bottom);
        make.left.equalTo(self.scrollView);
        make.size.mas_equalTo(CGSizeMake(KWIDTH, 80));
    }];

    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.categoryView.mas_bottom);
        make.left.equalTo(self.scrollView);
        make.width.offset(KWIDTH);
        make.bottom.equalTo(self.backView);
    }];

}

#pragma mark - Lazy Loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _backView;;
}
- (YXHomeHeaerView *)headerView {
    if (!_headerView) {
        _headerView = [[YXHomeHeaerView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 30)];
        DDWeakSelf
        [_headerView setSelectHotKey:^(YXHotKeyModel * _Nonnull model) {
            if ([YXUserInfoManager getUserInfo].token == nil) {
                KPOP(@"游客不能使用此功能，请先登录");
                return;
            }
            YXCateDetailViewController *vc = [YXCateDetailViewController new];
            vc.cate_id = model.pid;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
    }
    return _headerView;
}
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = APPTintColor;
        _scrollView.contentSize = CGSizeMake(KWIDTH, 2000);
    }
    return _scrollView;
}
- (JXCategoryTitleImageView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryTitleImageView alloc]init];
        _categoryView.frame = CGRectMake(0, 30, KWIDTH, 80);
        _categoryView.delegate = self;
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleFont = [UIFont systemFontOfSize:16.0];
        _categoryView.titleColor = color_TextOne;
        _categoryView.titleSelectedColor = [UIColor whiteColor];
        _categoryView.defaultSelectedIndex = 0;
        _categoryView.backgroundColor = APPTintColor;
        _categoryView.titles = _titles;
        _categoryView.imageNames = @[@"home_xuexi_unselect",@"home_shenghuo_unselect",@"home_changye_unselect",@"home_C2M_unselect"];
        _categoryView.selectedImageNames = @[@"home_xuexi_select",@"home_shenghuo_select",@"home_changye_select",@"home_C2M_select"];
        _categoryView.imageSize = CGSizeMake(27, 30);
        NSMutableArray *types = [NSMutableArray array];
        for (int i = 0; i < self.titles.count; i++) {
            [types addObject:@(JXCategoryTitleImageType_TopImage)];
        }
        _categoryView.imageTypes = types;
        
        JXCategoryIndicatorTriangleView *triangleView = [[JXCategoryIndicatorTriangleView alloc] init];
        triangleView.indicatorWidth = JXCategoryViewAutomaticDimension;
        //可以试试宽度补偿
        triangleView.indicatorColor = [UIColor whiteColor];
        triangleView.indicatorWidth = 15;
        triangleView.indicatorHeight = 10;
        triangleView.indicatorWidthIncrement = 0;
        triangleView.verticalMargin = 0;
        _categoryView.indicators = @[triangleView];
        _categoryView.listContainer = self.listContainerView;
        _listContainerView = self.listContainerView;
    }
    return _categoryView;
}
- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_CollectionView delegate:self];
        _listContainerView.frame = CGRectMake(0, 110, KWIDTH, 890);
        _listContainerView.scrollView.scrollEnabled = NO;
    }
    return _listContainerView;
}


- (YXSearchView *)searchView {
    if (!_searchView) {
        _searchView = [[YXSearchView alloc] initWithFrame:CGRectZero];
//        if(@available(iOS 11.0, *)) {
//            [[_searchView.heightAnchor constraintEqualToConstant:44] setActive:YES];
//        }
        _searchView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchAction)];
        [_searchView addGestureRecognizer:tap];
    }
    return _searchView;
}
- (UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.frame = CGRectMake(0, 2, 30, 36);
        _leftBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_leftBtn setImage:[UIImage imageNamed:@"home_left_item"] forState:(UIControlStateNormal)];
        [_leftBtn setImageEdgeInsets:(UIEdgeInsetsMake(0, -10, 0, 10))];
        [_leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftBtn;
}
- (YXNavCityView *)cityView {
    if (!_cityView) {
        _cityView = [[YXNavCityView alloc] initWithFrame:CGRectMake(0, 0, 60, 40)];
        _cityView.cityLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cityAction)];
        [_cityView.cityLab addGestureRecognizer:tap];
    }
    return _cityView;
}
- (NSMutableArray *)hotKeyArr {
    if (!_hotKeyArr) {
        _hotKeyArr = [NSMutableArray array];
    }
    return _hotKeyArr;
}


#pragma mark - Touch Even
- (void)leftAction {
    [BDLeftMenuView show];
}
- (void)cityAction {
    YXSelectCityViewController *vc = [YXSelectCityViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    DDWeakSelf
    [vc setLocationCityBlock:^(YXCityModel * _Nonnull cityModel) {
        weakSelf.cityView.cityLab.text = cityModel.district;
        [YXUserInfoManager resetUserInfoMessageWithDic:@{@"cityId":cityModel.Id}];
        [[YXAMapManager shareLocation] queryWeatherWithCityName:cityModel.district selectCityBlock:^(YXCitysModel * _Nonnull cityModel) {
            weakSelf.cityView.weatherLab.text = cityModel.weather;
        }];
    }];
    [vc setSelectCityBlock:^(YXCitysModel * _Nonnull citysModel) {
        weakSelf.cityView.cityLab.text = citysModel.cityName;
        [YXUserInfoManager resetUserInfoMessageWithDic:@{@"cityId":citysModel.cityId}];
        [[YXAMapManager shareLocation] queryWeatherWithCityName:citysModel.cityName selectCityBlock:^(YXCitysModel * _Nonnull cityModel) {
            weakSelf.cityView.weatherLab.text = cityModel.weather;
        }];
    }];
}

- (void)searchAction {
    DDSearchViewController *vc = [DDSearchViewController new];
    vc.placeholder = @"用工";
    [self.navigationController pushViewController:vc animated:YES];
    vc.didSearchBlock = ^(NSString * _Nonnull keyWord) {
        if (keyWord.length) {
            //            YXGoodListViewController *list = [YXGoodListViewController new];
            //            list.keyWord = keyWord;
            //            [weakSelf.navigationController pushViewController:list animated:YES];
        }else{
            KPOP(@"请输入关键字");
        }
    };
}

@end
