//
//  YXHomeListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,HomeType) {
    HomeTypeStateStudy,      // 学习
    HomeTypeStateLife,       // 生活
    HomeTypeStateCY,         // 创业
    HomeTypeStateC2M,        // C2M
};
@class YXApplyRoleInfoModel;
@interface YXHomeListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>
// 页面类型
@property (nonatomic ,assign) HomeType type;

@property (nonatomic ,strong) NSString *city_ids;

@property (nonatomic ,strong) YXApplyRoleInfoModel *roleModel;

// 更新scrollView 高度
@property (nonatomic ,copy)void(^updateContentSize)(CGFloat height);


@end

NS_ASSUME_NONNULL_END
