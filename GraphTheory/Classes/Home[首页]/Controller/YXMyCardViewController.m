//
//  YXMyCardViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMyCardViewController.h"
#import "YXUserViewModel.h"
#import "DDWorkHeaderView.h"
#import "YXServiceCollectionViewCell.h"
#import "YXMyCardHeaderView.h"
#import "YXCardOneModel.h"
#import "YXCardTwoModel.h"
#import "YXUMShareManager.h"
@interface YXMyCardViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic ,strong) YXMyCardHeaderView *headerView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) YXCardOneModel *oneModel;
@property (nonatomic ,strong) YXCardTwoModel *twoModel;
@property (nonatomic ,strong) UIButton *shareBtn;
@end

@implementation YXMyCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的名片";
    self.dataArr = [NSMutableArray array];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithCustomView:self.shareBtn];
    self.navigationItem.rightBarButtonItem = right;
    
    
    [self loadData];
   
}

- (void)click {
    
    // 判断手机是否安装微信
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
        NSString *url = [NSString stringWithFormat:@"http://app.itulun.com/Home/Mingpian/index.html?user_id=%@id=%@",[YXUserInfoManager getUserInfo].user_id,self.type];
        NSString *text;
        if ([self.type isEqualToString:@"1"]) {
            text = [NSString stringWithFormat:@"工号：%@",self.twoModel.show_id];
        }else {
            text = [NSString stringWithFormat:@"工号：%@",self.oneModel.show_video_id];
        }
        [[YXUMShareManager shared] shareWebPageToPlatformType:(UMSocialPlatformType_WechatSession) thumbURL:@"" title:@"协助我开启创业之旅！" description:text webpageUrl:url];
    }else {
        [YJProgressHUD showMessage:@"您未安装微信客户端,不能分享哟~"];
    }
}

- (void)loadData {
    DDWeakSelf
    [YXUserViewModel queryBusinessCardWithtype:self.type business_user_id:self.user_id Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            if ([weakSelf.type isEqualToString:@"1"]) {
                weakSelf.twoModel = [YXCardTwoModel mj_objectWithKeyValues:responesObj[@"data"]];
            }else {
                weakSelf.oneModel = [YXCardOneModel mj_objectWithKeyValues:responesObj[@"data"]];
            }
            [weakSelf contentInsetHeaderView];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

// 设置collectionView的头部
- (void)contentInsetHeaderView {
    CGFloat header_y = 450;
    self.collectionView.delegate =self;
    self.collectionView.dataSource = self;
    self.collectionView.contentInset = UIEdgeInsetsMake(header_y, 0, 0, 0);
   self.headerView = [[YXMyCardHeaderView alloc] initWithFrame:(CGRectMake(0, -header_y, KWIDTH, header_y))];
    if ([self.type isEqualToString:@"1"]) {
        self.headerView.twoModel = self.twoModel;
    }else {
        self.headerView.oneModel = self.oneModel;
    }
    [self.collectionView addSubview: self.headerView];
    [self.collectionView setContentOffset:CGPointMake(0, -header_y)];
    [_collectionView registerClass:[DDWorkHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDWorkHeaderView"];
    [_collectionView registerNib:[UINib nibWithNibName:@"YXServiceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXServiceCollectionViewCell"];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if ([self.type isEqualToString:@"1"]) {
        return self.twoModel.services.count;
    }else {
        return self.oneModel.services.count;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([self.type isEqualToString:@"1"]) {
        YXCardTwoModelServices *brandModel = self.twoModel.services[section];
        return brandModel.child.count;
    }else {
        YXCardOneModelServices *brandModel = self.oneModel.services[section];
        return brandModel.child.count;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    return CGSizeMake((KWIDTH - 75)/4, 24);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 40);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        DDWorkHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDWorkHeaderView" forIndexPath:indexPath];
        header.leftCons = 0;
        if ([self.type isEqualToString:@"1"]) {
            YXCardTwoModelServices *brandModel = self.twoModel.services[indexPath.section];
            header.titleLab.text = brandModel.name;
        }else {
            YXCardOneModelServices *brandModel = self.oneModel.services[indexPath.section];
            header.titleLab.text = brandModel.brand_name;
        }
        header.rightBtn.hidden = YES;
        header.rightImg.hidden = YES;
        return header;
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXServiceCollectionViewCell" forIndexPath:indexPath];
    cell.selectImg.hidden = YES;
    cell.titleLab.font = [UIFont systemFontOfSize:12];
    if ([self.type isEqualToString:@"1"]) {
        YXCardTwoModelServices *brandModel = self.twoModel.services[indexPath.section];
        YXCardTwoModelServicesChild *model = brandModel.child[indexPath.row];
        cell.titleLab.text = model.name;
    }else {
        YXCardOneModelServices *brandModel = self.oneModel.services[indexPath.section];
        YXCardOneModelServicesChild *model = brandModel.child[indexPath.row];
        cell.titleLab.text = model.brand_name;
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _shareBtn.frame = CGRectMake(0, 0, 60, 26);
        _shareBtn.backgroundColor = [UIColor whiteColor];
        [_shareBtn setTitle:@"递名片" forState:(UIControlStateNormal)];
        [_shareBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        _shareBtn.layer.masksToBounds = YES;
        _shareBtn.layer.cornerRadius = 4.0;
        [_shareBtn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _shareBtn;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
