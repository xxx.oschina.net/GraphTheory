//
//  YXHomeViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXHomeViewModel : DDBaseViewModel

/**
 推荐热词
 
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryHotRecKeywordCompletion:(void(^)(id responesObj))completion
                             Failure:(void(^)(NSError *error))conError;


/**
 内容搜索

@param city_id    城市id
@param keyword    内容
@param completion  返回成功
@param conError    返回失败
*/
+ (void)querySearchWithCity_id:(NSString *)city_id
                       keyword:(NSString *)keyword
                    Completion:(void(^)(id responesObj))completion
                       Failure:(void(^)(NSError *error))conError;



/**
首页数据

@param type    学习：study 生活：life
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryHomeCatesWithType:(NSString *)type
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;

/**
首页所有分类数据

@param type    学习：study 生活：life
@param city_ids    城市id
@param c_model   0：全部模式 1：劵码模块式  2：抢单模式
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryHomeAllCatesWithType:(NSString *)type
                         city_ids:(NSString *)city_ids
                          c_model:(NSString *)c_model
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;

/**
 顶级分类下所有子集

@param cate_id    分类id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryTopCateDetailWithCate_id:(NSString *)cate_id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;
/**
 具体分类的属性

@param cate_id    分类id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryCateDetailWithCate_id:(NSString *)cate_id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;


/**
开通城市服务列表

@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryOpenServiceCityCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;


/**
所有地区列表

@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryAllDistrictsCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;


/**
 学习模块 分享订单列表

 @param page        分页数
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryShareOrdersWithPage:(NSString *)page
                     service_ids:(NSString *)service_ids
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;
/**
 学习模块 分享订单详情

 @param Id              订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryShareOrderInfoWithId:(NSString *)Id
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;

/**
 生活模块 接单列表

 @param page        分页数
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryLifeOrdersWithPage:(NSString *)page
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;


/**
 抢单列表

 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryGrabOrderListCompletion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;


@end

NS_ASSUME_NONNULL_END
