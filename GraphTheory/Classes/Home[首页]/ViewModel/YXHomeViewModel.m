//
//  YXHomeViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXHomeViewModel.h"

@implementation YXHomeViewModel

#pragma mark - 推荐热词
+ (void)queryHotRecKeywordCompletion:(void(^)(id responesObj))completion
                             Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/hotRecKeyword",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  内容搜索
+ (void)querySearchWithCity_id:(NSString *)city_id
                       keyword:(NSString *)keyword
                    Completion:(void(^)(id responesObj))completion
                       Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/search",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:city_id?city_id:@"229" forKey:@"city_id"];
    [params setValue:keyword forKey:@"keyword"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -首页数据
+ (void)queryHomeCatesWithType:(NSString *)type
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/indexCates",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 首页所有分类数据
+ (void)queryHomeAllCatesWithType:(NSString *)type
                         city_ids:(NSString *)city_ids
                          c_model:(NSString *)c_model
                    Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/allCates",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [params setValue:city_ids forKey:@"city_ids"];
    [params setValue:c_model forKey:@"c_model"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 顶级分类下所有子集
+ (void)queryTopCateDetailWithCate_id:(NSString *)cate_id
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/topCateDetail",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:cate_id forKey:@"cate_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}
#pragma mark - 具体分类的属性
+ (void)queryCateDetailWithCate_id:(NSString *)cate_id
                    Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/cateDetail",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:cate_id forKey:@"cate_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 开通服务城市列表
+ (void)queryOpenServiceCityCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/openServiceCity",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 所有地区列表
+ (void)queryAllDistrictsCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/allDistricts",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  学习模块 分享订单列表
+ (void)queryShareOrdersWithPage:(NSString *)page
                     service_ids:(NSString *)service_ids
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/shareOrders",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [params setValue:service_ids forKey:@"service_ids"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}

#pragma mark -  学习模块 分享订单详情
+ (void)queryShareOrderInfoWithId:(NSString *)Id
                        Completion:(void(^)(id responesObj))completion
                              Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/shareOrderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  生活模块 接单列表
+ (void)queryLifeOrdersWithPage:(NSString *)page
                        Completion:(void(^)(id responesObj))completion
                          Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/lifeOrders",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 抢单列表
+ (void)queryGrabOrderListCompletion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/grabOrderList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

@end
