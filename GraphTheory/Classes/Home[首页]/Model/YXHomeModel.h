//
//  YXHomeModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@class YXRealTimeDataModel;
@interface YXHomeModel : DDBaseModel

@property (nonatomic ,strong) NSArray *cateList;

@property (nonatomic ,strong) NSArray *cateBannerList;

@property (nonatomic ,strong) YXRealTimeDataModel *realTimeData;

@end

@interface YXCateListModel : DDBaseModel

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *subtitle;
@end

@interface YXCateBannerListModel : DDBaseModel

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imgbanner;
@property (nonatomic, copy) NSString *bannerurl;
@property (nonatomic, copy) NSString *sortbanner;

@end

@interface YXRealTimeDataModel : DDBaseModel

@property (nonatomic, assign) NSInteger curOrderCount;
@property (nonatomic, assign) NSInteger newSupplier;
@property (nonatomic, assign) CGFloat totalOrderCount;
@property (nonatomic, assign) NSInteger curServiceCount;
@property (nonatomic, assign) NSInteger involvedIndustryCount;
@property (nonatomic, assign) NSInteger workerCount;
@end


NS_ASSUME_NONNULL_END
