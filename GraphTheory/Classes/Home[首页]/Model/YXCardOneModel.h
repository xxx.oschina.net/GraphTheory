//  Created by li qiao robot 
#import <Foundation/Foundation.h>
@interface YXCardOneModelServicesChild : NSObject
@property (copy,nonatomic)   NSString *brand_m_id;
@property (copy,nonatomic)   NSString *brand_id;
@property (copy,nonatomic)   NSString *brand_updatetime;
@property (copy,nonatomic)   NSString *brand_pid;
@property (copy,nonatomic)   NSString *brand_picpath;
@property (copy,nonatomic)   NSString *brand_num;
@property (copy,nonatomic)   NSString *brand_rc_district;
@property (copy,nonatomic)   NSString *brand_time;
@property (copy,nonatomic)   NSString *brand_status;
@property (copy,nonatomic)   NSString *brand_name;
@end

@interface YXCardOneModelServices : NSObject
@property (copy,nonatomic)   NSString *brand_picpath;
@property (copy,nonatomic)   NSString *brand_id;
@property (copy,nonatomic)   NSString *brand_pid;
@property (copy,nonatomic)   NSString *brand_name;
@property (strong,nonatomic) NSArray *child;
@end

@interface YXCardOneModelAddress : NSObject
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXCardOneModelServiceCities : NSObject
@property (copy,nonatomic)   NSString *district;
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *is_open;
@property (copy,nonatomic)   NSString *sort_order;
@property (copy,nonatomic)   NSString *level;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXCardOneModel : NSObject
@property (copy,nonatomic)   NSString *signature;
@property (strong,nonatomic) NSArray *serviceCities;
@property (copy,nonatomic)   NSString *sex;
@property (copy,nonatomic)   NSString *fans_count;
@property (copy,nonatomic)   NSString *college;
@property (copy,nonatomic)   NSString *show_video_id;
@property (strong,nonatomic) YXCardOneModelAddress *address;
@property (copy,nonatomic)   NSString *userphone;
@property (strong,nonatomic) NSArray *service_city_ids;
@property (copy,nonatomic)   NSString *username;
@property (strong,nonatomic) NSArray *images;
@property (strong,nonatomic) NSArray *services;
@property (copy,nonatomic)   NSString *roleName;
@property (strong,nonatomic) NSArray *service_ids;
@property (copy,nonatomic)   NSString *birthday;
@end

