//  Created by li qiao robot 
#import <Foundation/Foundation.h>
@interface YXApplyRoleInfoModelApplyInfoAddress : NSObject
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXApplyRoleInfoModelApplyInfoServicesChildTypeInfo : NSObject
@property (copy,nonatomic)   NSString *ctype_id;
@property (copy,nonatomic)   NSString *ctype_name;
@property (copy,nonatomic)   NSString *ctype_rc_district;
@property (copy,nonatomic)   NSString *ctype_img;
@end

@interface YXApplyRoleInfoModelApplyInfoServicesChild : NSObject
@property (copy,nonatomic)   NSString *c_model;
@property (copy,nonatomic)   NSString *c_m_id;
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *c_contentprice;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *topid;
@property (copy,nonatomic)   NSString *subtitle;
@property (copy,nonatomic)   NSString *c_updatetime;
@property (copy,nonatomic)   NSString *c_remarka;
@property (copy,nonatomic)   NSString *c_remarktips;
@property (copy,nonatomic)   NSString *rebate;
@property (copy,nonatomic)   NSString *c_ctype_id;
@property (copy,nonatomic)   NSString *c_recommend;
@property (copy,nonatomic)   NSString *c_studyrebate;
@property (copy,nonatomic)   NSString *sort;
@property (copy,nonatomic)   NSString *type;
@property (copy,nonatomic)   NSString *bannerurl;
@property (copy,nonatomic)   NSString *c_remarktitle;
@property (copy,nonatomic)   NSString *c_process;
@property (copy,nonatomic)   NSString *c_remarkb;
@property (copy,nonatomic)   NSString *c_time;
@property (copy,nonatomic)   NSString *c_price;
@property (copy,nonatomic)   NSString *is_use;
@property (copy,nonatomic)   NSString *name;
@property (strong,nonatomic) YXApplyRoleInfoModelApplyInfoServicesChildTypeInfo *typeInfo;
@property (copy,nonatomic)   NSString *belongMode;
@property (copy,nonatomic)   NSString *c_address;
@property (copy,nonatomic)   NSString *c_remarkimg;
@property (copy,nonatomic)   NSString *c_rc_district;
@property (copy,nonatomic)   NSString *c_content;
@property (copy,nonatomic)   NSString *c_remarkc;
@property (copy,nonatomic)   NSString *sortbanner;
@property (copy,nonatomic)   NSString *c_studyprice;
@property (copy,nonatomic)   NSString *img;
@property (copy,nonatomic)   NSString *c_unitname;
@property (copy,nonatomic)   NSString *imgbanner;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXApplyRoleInfoModelApplyInfoServices : NSObject
@property (copy,nonatomic)   NSString *img;
@property (copy,nonatomic)   NSString *topid;
@property (copy,nonatomic)   NSString *c_rc_district;
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *subtitle;
@property (copy,nonatomic)   NSString *imgbanner;
@property (copy,nonatomic)   NSString *c_model;
@property (strong,nonatomic) NSArray *child;
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXApplyRoleInfoModelApplyInfoServiceCities : NSObject
@property (copy,nonatomic)   NSString *district;
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *is_open;
@property (copy,nonatomic)   NSString *sort_order;
@property (copy,nonatomic)   NSString *level;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXApplyRoleInfoModelApplyInfo : NSObject
@property (strong,nonatomic) NSArray *serviceCities;
@property (copy,nonatomic)   NSString *id_card;
@property (strong,nonatomic) NSArray *images;
@property (copy,nonatomic)   NSString *Id;
@property (strong,nonatomic) NSArray *services;
@property (strong,nonatomic) YXApplyRoleInfoModelApplyInfoAddress *address;
@property (strong,nonatomic) NSArray *service_city_ids;
@property (copy,nonatomic)   NSString *username;
@property (copy,nonatomic)   NSString *userphone;
@property (strong,nonatomic) NSArray *service_ids;
@end

@interface YXApplyRoleInfoModelStatisticData : NSObject
@property (strong,nonatomic) NSNumber *complainOrderNum;
@property (strong,nonatomic) NSNumber *takeOrderNum;
@property (strong,nonatomic) NSNumber *earnMoney;
@property (strong,nonatomic) NSNumber *collectNum;
@property (strong,nonatomic) NSNumber *fansNum;
@property (strong,nonatomic) NSNumber *followNum;
@end

@interface YXApplyRoleInfoModel : NSObject
@property (copy,nonatomic)   NSString *status;
@property (copy,nonatomic)   NSString *roleName;
@property (copy,nonatomic)   NSString *title;
@property (strong,nonatomic) YXApplyRoleInfoModelStatisticData *statisticData;
@property (copy,nonatomic)   NSString *role;
@property (strong,nonatomic) YXApplyRoleInfoModelApplyInfo *applyInfo;
@end

