//  Created by li qiao robot 
#import "YXApplyRoleInfoModel.h" 
@implementation YXApplyRoleInfoModelApplyInfoAddress
@end

@implementation YXApplyRoleInfoModelApplyInfoServicesChildTypeInfo
@end

@implementation YXApplyRoleInfoModelApplyInfoServicesChild
@end

@implementation YXApplyRoleInfoModelApplyInfoServices
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"child" : [YXApplyRoleInfoModelApplyInfoServicesChild class] }; 
}
@end

@implementation YXApplyRoleInfoModelApplyInfoServiceCities
@end

@implementation YXApplyRoleInfoModelApplyInfo
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"serviceCities" : [YXApplyRoleInfoModelApplyInfoServiceCities class] ,@"images" : [NSString class],@"services" : [YXApplyRoleInfoModelApplyInfoServices class],@"service_city_ids" : [NSString class],@"service_ids" : [NSString class]}; 
}
@end

@implementation YXApplyRoleInfoModelStatisticData
@end

@implementation YXApplyRoleInfoModel
@end

