//  Created by li qiao robot 
#import "YXCardOneModel.h" 
@implementation YXCardOneModelServicesChild
@end

@implementation YXCardOneModelServices
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"child" : [YXCardOneModelServicesChild class] }; 
}
@end

@implementation YXCardOneModelAddress
@end

@implementation YXCardOneModelServiceCities
@end

@implementation YXCardOneModel
+(NSDictionary*)mj_objectClassInArray{  
       return @{ @"serviceCities" : [YXCardOneModelServiceCities class] ,@"service_city_ids" : [NSString class],@"images" : [NSString class],@"services" : [YXCardOneModelServices class],@"service_ids" : [NSString class]}; 
}
@end

