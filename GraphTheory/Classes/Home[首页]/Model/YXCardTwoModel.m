//  Created by li qiao robot 
#import "YXCardTwoModel.h" 
@implementation YXCardTwoModelServicesChildTypeInfo
@end

@implementation YXCardTwoModelServicesChild
@end

@implementation YXCardTwoModelServices
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"child" : [YXCardTwoModelServicesChild class] }; 
}
@end

@implementation YXCardTwoModelAddress
@end

@implementation YXCardTwoModelServiceCities
@end

@implementation YXCardTwoModel
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"serviceCities" : [YXCardTwoModelServiceCities class] ,@"service_city_ids" : [NSString class],@"images" : [NSString class],@"services" : [YXCardTwoModelServices class],@"service_ids" : [NSString class]}; 
}
@end

