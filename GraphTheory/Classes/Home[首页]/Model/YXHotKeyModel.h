//
//  YXHotRecKeyModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXHotKeyModel : DDBaseModel

/**
 *  主键id
 */
@property (nonatomic, copy) NSString *Id;
/**
 *  名称
 */
@property (nonatomic, copy) NSString *name;
/**
 *  子标题
 */
@property (nonatomic, copy) NSString *subtitle;
/**
 *  pid
 */
@property (nonatomic, copy) NSString *pid;

@end

NS_ASSUME_NONNULL_END
