//
//  DDMediaViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/30.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMediaViewController.h"
#import "YXCategoryViewController.h"
#import "YXMediaDetailsViewController.h"
#import "YXMediaDetailsTwoViewController.h"

#import "UIScrollView+DRRefresh.h"
#import "DDSearchView.h"
#import "YXMediaListCollectionViewCell.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
@interface YXMediaViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDataSource>
@property (strong, nonatomic) DDSearchView *searchView;
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong ,nonatomic) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) NSString *keyword;
@end

@implementation YXMediaViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([YXUserInfoManager getUserInfo].token.length > 0) {
        self.page = 1;
        [self loadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 初始化默认值
    self.page = 1;
    self.keyword = @"";
    
    [self createNavUI];
 
    [self loadSubViews];

}

- (void)createNavUI {
    
    UIButton *backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    backBtn.frame = CGRectMake(0, 0, 60, 30);
    [backBtn setTitle:@"首页" forState:(UIControlStateNormal)];
    [backBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [backBtn setImage:[UIImage imageNamed:@"arrow_left"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:(UIControlEventTouchUpInside)];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 30, 40);
    rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [rightBtn setImage:[UIImage imageNamed:@"home_left_item"] forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32, 36));
    }];
    
    self.navigationItem.titleView = self.searchView;
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(260, 30));
    }];

  
}

- (void)backClick {
    self.tabBarController.selectedIndex = 0;
}

- (void)click {
    YXCategoryViewController *vc = [YXCategoryViewController new];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loadData {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXVideoViewModel queryVideoListWithPage:@(self.page).stringValue limit:@"10" keyword:self.keyword service_ids:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            [weakSelf.dataArr removeAllObjects];
            NSArray *temArr = [YXVideoModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
            [weakSelf.dataArr addObjectsFromArray:temArr];
            if ([temArr count] < 10) {
                [weakSelf.collectionView.mj_footer setHidden:YES];
            }else{
                [weakSelf.collectionView.mj_footer setHidden:NO];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}

- (void)loadSubViews {
    
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
    
}

#pragma mark - UICollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KWIDTH-30)/2, 270);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXMediaListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMediaListCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YXVideoModel *model = self.dataArr[indexPath.row];
    YXMediaDetailsTwoViewController *vc = [YXMediaDetailsTwoViewController new];
    vc.Id = model.v_id;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lozy Loading
- (DDSearchView *)searchView {
    if (!_searchView) {
        _searchView = [[DDSearchView alloc] initWithFrame:CGRectMake(0, 0, 240, 44)];
        _searchView.backView.backgroundColor = [UIColor whiteColor];
        _searchView.placeholder = @"知识任务";
        if(@available(iOS 11.0, *)) {
            [[_searchView.heightAnchor constraintEqualToConstant:44] setActive:YES];
        }
        DDWeakSelf
        _searchView.searchDone = ^(NSString * _Nonnull search) {
            weakSelf.keyword = search;
            weakSelf.page = 1;
            [weakSelf loadData];
        };
    }
    return _searchView;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerClass:[YXMediaListCollectionViewCell class] forCellWithReuseIdentifier:@"YXMediaListCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
            if ([YXUserInfoManager getUserInfo].token.length > 0) {
                weakSelf.page = 1;
                [weakSelf loadData];
            }
        }];
        [_collectionView setRefreshWithHeaderBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        } footerBlock:^{
            [weakSelf.collectionView footerEndRefreshing];
        }];
    }
    return _collectionView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
