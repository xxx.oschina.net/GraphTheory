//
//  YXMediaDetailsTwoViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMediaDetailsTwoViewController.h"
#import "YXPlaceOrderViewController.h"

#import "JXPageListView.h"
#import <JXPagingView/JXPagerView.h>
#import "YXMediaBottomView.h"
#import "YXCommentsView.h"
#import "YXEvaluationToolbarView.h"
#import "CDPVideoPlayer.h"
#import "YXGoodsInfoHeaderView.h"

#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
#import "UIViewController+ShouldAutorotate.h"
#import "YXPreviewManage.h"
@interface YXMediaDetailsTwoViewController ()<JXPagerViewDelegate,JXCategoryViewDelegate,CDPVideoPlayerDelegate>

@property (nonatomic ,strong) UIView *topView;
@property (nonatomic ,strong) CDPVideoPlayer *player;
@property (nonatomic ,strong) JXCategoryTitleView *categoryView;
@property (nonatomic ,strong) JXPagerView *pagingView;
@property (nonatomic ,strong) YXGoodsInfoHeaderView *headerView;
@property (nonatomic ,strong) YXEvaluationToolbarView *toolbarView; /** 底部回复评论工具条*/
@property (nonatomic ,strong) YXVideoModel *model;
@property (nonatomic ,strong) NSArray *titles;
@property (nonatomic ,strong) NSArray *listViewArr;
@property (nonatomic ,assign) NSInteger type;

@end

@implementation YXMediaDetailsTwoViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [_player pause];
    [_player close];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeTop;
    
    self.titles = @[@"简介",@"评论"];
    self.type = 0;

}

- (void)loadData {
    DDWeakSelf
    [YXVideoViewModel queryVideoInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
//            NSLog(@"%@",responesObj);
            weakSelf.model = [YXVideoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf loadSubViews];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.pagingView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

// 关注
- (void)followAuthor:(NSString *)type {
    
    DDWeakSelf
    [YXVideoViewModel queryFollowAuthorWithtype:type author_id:self.model.user_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

// 收藏
- (void)collectVideo:(NSString *)type {
    DDWeakSelf
    [YXVideoViewModel queryCollectVideoWithtype:type video_id:self.model.v_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

// 点赞
- (void)likeVideo:(NSString *)type {
    DDWeakSelf
    [YXVideoViewModel queryLikeVideoWithtype:type video_id:self.model.v_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

// 评论视频
- (void)sendText:(NSString *)content {
    
    if ([NSString isBlankString:content]) {
        return [YJProgressHUD showMessage:@"请输入评论内容"];
    }
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXVideoViewModel queryReplyVideoWithContent:content video_id:self.model.v_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:@"评论成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}


// 添加子视图
- (void)loadSubViews {
        
    _topView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, STATUS_HEIGHT+200))];
    _topView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.topView];
    
    _player=[[CDPVideoPlayer alloc] initWithFrame:CGRectMake(0,STATUS_HEIGHT,KWIDTH,200)
                                              url:self.model.v_videoroute
                                         delegate:self
                                   haveOriginalUI:YES];
    _player.title=self.model.v_name;
    [self.topView addSubview:self.player];

    _headerView = [[YXGoodsInfoHeaderView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 300))];
    _headerView.model = self.model;

    _categoryView = [[JXCategoryTitleView alloc] init];
    _categoryView.frame = CGRectMake(0, 0, KWIDTH, 50);
    _categoryView.titles = self.titles;
    _categoryView.backgroundColor = [UIColor whiteColor];
    _categoryView.delegate = self;
    _categoryView.titleSelectedColor =APPTintColor;
    _categoryView.titleColor = [UIColor blackColor];
    _categoryView.titleColorGradientEnabled = YES;
    _categoryView.titleLabelZoomEnabled = YES;
    _categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagingView.listContainerView;
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
    
    _pagingView = [[JXPagerView alloc] initWithDelegate:self];
    _pagingView.frame = CGRectMake(0, 200+STATUS_HEIGHT, KWIDTH, kHEIGHT-200-BOTTOM_SAFE_HEIGHT);
    [self.view addSubview:self.pagingView];
    
    self.categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagingView.listContainerView;

    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);

   
    [self.view addSubview:self.toolbarView];

//    if (self.type ==0) {
//
//        [self.view addSubview:self.player];
//        [self.view addSubview:self.pagingView];
//        [self.player mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (@available(iOS 11.0,*)) {
//                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
//                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
//                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
//            }else{
//                make.left.top.right.mas_equalTo(self.view);
//            }
//            make.height.equalTo(@200);
//        }];
//        [self.pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (@available(iOS 11.0,*)) {
//                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
//                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
//                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
//            }else{
//                make.left.bottom.right.mas_equalTo(self.view);
//            }
//            make.top.equalTo(self.player.mas_bottom);
//        }];
//    }else {
//        [self.view addSubview:self.toolbarView];
//        [self.view addSubview:self.pagingView];
//        [self.toolbarView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (@available(iOS 11.0,*)) {
//                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
//                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
//                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
//            }else{
//                make.left.bottom.right.mas_equalTo(self.view);
//            }
//            make.height.equalTo(@50);
//        }];
//        [self.pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
//            if (@available(iOS 11.0,*)) {
//                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
//                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
//                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
//            }else{
//                make.left.top.right.mas_equalTo(self.view);
//            }
//            make.bottom.equalTo(self.toolbarView.mas_top);
//        }];
//    }
//
    
//    //开始播放
//    [self.player play];
}

//- (void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//
//    self.pagingView.frame = self.view.bounds;
//}



#pragma mark - JXPagingViewDelegate
- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.headerView;
}
- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return 300;
}
- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return 50;
}
- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}
- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
    return self.titles.count;
}
- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
    if (index == 0) {
        YXMediaBottomView *list = [[YXMediaBottomView alloc] init];
        list.model = self.model;
        DDWeakSelf
        [list setClickGZBlock:^(UIButton *btn) {
            if (btn.selected) {
                [weakSelf followAuthor:@"1"];
            }else {
                [weakSelf followAuthor:@"2"];
            }
        }];
        [list setClickCollectionBlock:^(UIButton *btn) {
            if (btn.selected) {
                [weakSelf collectVideo:@"1"];
            }else {
                [weakSelf collectVideo:@"0"];
            }
        }];
        [list setClickPraiseBlock:^(UIButton *btn) {
            if (btn.selected) {
                [weakSelf likeVideo:@"1"];
            }else {
                [weakSelf likeVideo:@"2"];
            }
        }];
        return list;
    }else {
        YXCommentsView  *list = [[YXCommentsView alloc] init];
        list.model = self.model;
        return list;
    }

}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    if (index == 0) {
        _toolbarView.hidden = YES;
    }else {
        _toolbarView.hidden = NO;
    }
}

-(void)switchSizeClick{
    
    [_player pause];
    
    [[YXPreviewManage sharePreviewManage] showVideoWithUrl:self.model.v_videoroute withController:self];
    
    [YXVideoViewModel queryAddVideoViewCountWithId:self.model.v_id Completion:^(id  _Nonnull responesObj) {
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
}


#pragma mark - Lazy Loading
//- (YXGoodsInfoHeaderView *)headerView {
//    if (!_headerView) {
//        _headerView = [[YXGoodsInfoHeaderView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 300))];
//    }
//    return _headerView;
//}

//- (CDPVideoPlayer *)player {
//    if (!_player) {
//        _player=[[CDPVideoPlayer alloc] initWithFrame:CGRectMake(0,0,KWIDTH,200)
//                                                  url:self.model.v_videoroute
//                                             delegate:self
//                                       haveOriginalUI:YES];
//        _player.title=self.model.v_name;
//    }
//    return _player;
//}
//
//- (JXCategoryTitleView *)categoryView {
//    if (!_categoryView) {
//        _categoryView = [[JXCategoryTitleView alloc] init];
//        _categoryView.frame = CGRectMake(0, 0, KWIDTH, 50);
//        _categoryView.titles = self.titles;
//        _categoryView.backgroundColor = [UIColor whiteColor];
//        _categoryView.delegate = self;
//        _categoryView.titleSelectedColor =APPTintColor;
//        _categoryView.titleColor = [UIColor blackColor];
//        _categoryView.titleColorGradientEnabled = YES;
//        _categoryView.titleLabelZoomEnabled = YES;
//        _categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagingView.listContainerView;
//        self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
//    }
//    return _categoryView;
//}
//- (JXPagerView *)pagingView {
//    if (!_pagingView) {
//        _pagingView  =[[JXPagerView alloc] initWithDelegate:self];
//    }
//    return _pagingView;
//}

- (YXEvaluationToolbarView *)toolbarView {
    if (!_toolbarView) {
        _toolbarView = [[YXEvaluationToolbarView alloc] initWithFrame:CGRectMake(0, kHEIGHT-50 , KWIDTH, 50)];
        _toolbarView.hidden = YES;
        DDWeakSelf
        [_toolbarView setClickSendBtnBlick:^(NSString *content) {
            [weakSelf sendText:content];
        }];
    }
    return _toolbarView;
}



-(BOOL)shouldAutorotate{
    return NO;
}



//- (UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}

-(void)dealloc{
    //关闭播放器并销毁当前播放view
    //一定要在退出时使用,否则内存可能释放不了
    [_player pause];
    [_player close];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
