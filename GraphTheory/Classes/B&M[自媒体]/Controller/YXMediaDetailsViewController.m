//
//  YXMediaDetailsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMediaDetailsViewController.h"
#import "YXPlaceOrderViewController.h"

#import "YXVideoCollectionViewCell.h"
#import "YXGoodsInfoCollectionViewCell.h"
#import "YXMapCollectionViewCell.h"
#import "YXMediaBottomView.h"
#import "JXPageListView.h"
#import "CDPVideoPlayer.h"
#import "YXEvaluationToolbarView.h"

#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
#import "UIViewController+ShouldAutorotate.h"

@interface YXMediaDetailsViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,JXPageListViewDelegate,CDPVideoPlayerDelegate,JXCategoryViewDelegate>

@property (nonatomic ,strong) JXCategoryTitleView *categoryView;
@property (nonatomic ,strong) JXPageListView *pageListView;
@property (nonatomic ,strong) UIView *playerView;
@property (nonatomic ,strong) CDPVideoPlayer *player;
/** 底部回复评论工具条*/
@property (nonatomic ,strong) YXEvaluationToolbarView *toolbarView;
@property (nonatomic ,strong) YXVideoModel *model;
@property (nonatomic ,strong) NSArray *titles;
@property (nonatomic ,strong) NSMutableArray *listViewArr;
@property (nonatomic ,assign) NSInteger type;
@end

@implementation YXMediaDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self loadData];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [_player pause];
    [_player close];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.edgesForExtendedLayout = UIRectEdgeTop;

    self.titles = @[@"简介",@"评论"];
    self.type = 0;
    [self loadSubViews];

    [self.player play];

}

- (void)loadData {
    
    DDWeakSelf
    [YXVideoViewModel queryVideoInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            NSLog(@"%@",responesObj);
            weakSelf.model = [YXVideoModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.listViewArr = [NSMutableArray array];
            for (int i =0; i < weakSelf.titles.count; i++) {
                YXMediaBottomView *view = [[YXMediaBottomView alloc] initWithFrame:CGRectZero];
//                view.type = i;
                view.model = weakSelf.model;
                [weakSelf.listViewArr addObject:view];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.categoryView reloadData];
        [weakSelf.categoryView reloadDataWithoutListContainer];
        [weakSelf.pageListView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
    
}
// 添加子视图
- (void)loadSubViews {
    
    if (self.type ==0) {
        [self.view addSubview:self.pageListView];
        [self.pageListView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            }else{
                make.edges.mas_equalTo(self.view);
            }
        }];
    }else {
        
    }
  
    
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section == 0){
        return CGSizeMake(SCREEN_WIDTH, 200);
    }else if (indexPath.section == 1){
        return CGSizeMake(SCREEN_WIDTH, 100);
    }else if (indexPath.section == 2){
        return CGSizeMake(SCREEN_WIDTH, 200);
    }else if (indexPath.section == 3){
        if (self.titles.count> 0) {
            return CGSizeMake(SCREEN_WIDTH, [self.pageListView listContainerCellHeight]);
//            return CGSizeMake(SCREEN_WIDTH, 140);
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
    if (indexPath.section == 0) {
        YXVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXVideoCollectionViewCell" forIndexPath:indexPath];
        _player = [[CDPVideoPlayer alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 200) url:self.model.v_videoroute delegate:self haveOriginalUI:YES];
        _player.title = self.model.v_name;
        [cell.contentView addSubview:self.playerView];
        [self.playerView addSubview:self.player];
        return cell;
    }else if (indexPath.section == 1){
        YXGoodsInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXGoodsInfoCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 2){
        YXMapCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMapCollectionViewCell" forIndexPath:indexPath];
        return cell;
    }else if (indexPath.section == 3){
        //Tips:最后一个section（即listContainerCell所在的section）配置listContainerCell
        return [self.pageListView listContainerCellForRowAtIndexPath:indexPath];
    }
    return nil;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        YXPlaceOrderViewController *vc = [[YXPlaceOrderViewController alloc] init];
        vc.goods_id = self.model.goodsInfo.goods_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //Tips:需要传入mainTableView的scrollViewDidScroll事件
    [self.pageListView mainTableViewDidScroll:scrollView];
}
- (NSArray<UIView<JXPageListViewListDelegate> *> *)listViewsInPageListView:(JXPageListView *)pageListView {
    return self.listViewArr;
}

- (void)pinCategoryView:(JXCategoryBaseView *)pinCategoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    
}
#pragma mark - CDPVideoPlayerDelegate
//非全屏下返回点击(仅限默认UI)
-(void)back{
//    [self backClick];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 点击事件
//返回
-(void)backClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (JXPageListView *)pageListView {
    DDWeakSelf
    if (!_pageListView) {
        _pageListView = [[JXPageListView alloc] initWithDelegate:self];
//        _pageListView.frame = CGRectMake(0, STATUS_HEIGHT, KWIDTH, kHEIGHT-STATUS_HEIGHT);
        _pageListView.listViewScrollStateSaveEnabled = YES;
        _pageListView.pinCategoryViewVerticalOffset = 0;
        //Tips:pinCategoryViewHeight要赋值
        _pageListView.pinCategoryViewHeight = 50;
        //Tips:成为mainTableView dataSource和delegate的代理，像普通UITableView一样使用它
        _pageListView.mainTableView.dataSource = self;
        _pageListView.mainTableView.delegate = self;
        _pageListView.mainTableView.scrollsToTop = NO;
        _pageListView.mainTableView.backgroundColor = [UIColor whiteColor];
        [_pageListView.mainTableView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _pageListView.pinCategoryView.titles =  self.titles;
        if (@available(iOS 11.0, *)) {
            _pageListView.mainTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else{
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        [_pageListView.mainTableView registerClass:[YXVideoCollectionViewCell class] forCellWithReuseIdentifier:@"YXVideoCollectionViewCell"];
        [_pageListView.mainTableView registerNib:[UINib nibWithNibName:@"YXGoodsInfoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXGoodsInfoCollectionViewCell"];
        [_pageListView.mainTableView registerClass:[YXMapCollectionViewCell class] forCellWithReuseIdentifier:@"YXMapCollectionViewCell"];
//        [_pageListView.mainTableView setRefreshWithHeaderBlock:^{
//            [weakSelf requestHomeData];
//        } footerBlock:nil];
    }
    return _pageListView;
}

- (UIView *)playerView {
    if (!_playerView) {
        _playerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 200)];
    }
    return _playerView;
}
- (YXEvaluationToolbarView *)toolbarView {
    if (!_toolbarView) {
        _toolbarView = [[YXEvaluationToolbarView alloc] initWithFrame:CGRectZero];
    }
    return _toolbarView;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
-(BOOL)shouldAutorotate{
    return !_player.isSwitch;
}
-(void)switchSizeClick{
    DDWeakSelf
//    if (_player.isFullScreen) {
////        self.pageListView.hidden = YES;
//    }else{
////        self.pageListView.hidden = NO;
//    }
    
    
    
}
- (void)dealloc {
    [_player close];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
