//
//  YXVideoListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXVideoListViewController : DDBaseViewController

// type 0:分享课堂小纸条 1:视频
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSString *cate_id;

@property (nonatomic ,strong) NSString *brand_id;

@end

NS_ASSUME_NONNULL_END
