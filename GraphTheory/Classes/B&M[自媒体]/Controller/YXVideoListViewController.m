//
//  YXVideoListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXVideoListViewController.h"
#import "YXMediaDetailsViewController.h"
#import "YXMediaDetailsTwoViewController.h"
#import "YXOrderDetailsViewController.h"

#import "UIScrollView+DRRefresh.h"
#import "YXMediaListCollectionViewCell.h"
#import "YXHomeOrderCollectionViewCell.h"
#import "YXVideoViewModel.h"
#import "YXHomeViewModel.h"
#import "YXVideoModel.h"
#import "YXShareOrdersModel.h"
@interface YXVideoListViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDataSource>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong ,nonatomic) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;

@end

@implementation YXVideoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.type == 0) {
        self.title = @"课堂小纸条";
    }else {
        self.title = @"视频";
    }
    // 初始化默认值
    self.page = 1;
    [self loadData];
     
    [self loadSubViews];
}
- (void)loadData {
    
    DDWeakSelf
    if (self.type == 0) {
        [YJProgressHUD showLoading:@""];
        [YXHomeViewModel queryShareOrdersWithPage:@"1" service_ids:self.cate_id Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
            }
            YXShareOrdersModel *model = [YXShareOrdersModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.dataArr = [YXShareOrdersModelList mj_objectArrayWithKeyValuesArray:model.list];
            [weakSelf.collectionView reloadData];
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
        } Failure:^(NSError * _Nonnull error) {
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }else {
        [YJProgressHUD showLoading:@""];
        [YXVideoViewModel queryVideoListWithPage:@(self.page).stringValue limit:@"10" keyword:@"" service_ids:self.brand_id Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
            }
            if (REQUESTDATASUCCESS) {
                [weakSelf.dataArr removeAllObjects];
                NSArray *temArr = [YXVideoModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
                [weakSelf.dataArr addObjectsFromArray:temArr];
                if ([temArr count] < 10) {
                    [weakSelf.collectionView.mj_footer setHidden:YES];
                }else{
                    [weakSelf.collectionView.mj_footer setHidden:NO];
                }
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
            [weakSelf.collectionView reloadData];
        } failure:^(NSError * _Nonnull error) {
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }
   
    
}

- (void)loadSubViews {
    
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
    
}

#pragma mark - UICollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type == 0) {
        return CGSizeMake((SCREEN_WIDTH -45) / 2, 220);
    }else {
        return CGSizeMake((KWIDTH-30)/2, 270);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.type == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else {
        return UIEdgeInsetsMake(10, 10, 10, 10);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == 0) {
        YXHomeOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell" forIndexPath:indexPath];
        cell.listModel = self.dataArr[indexPath.row];
        return cell;
    }else {
        YXMediaListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMediaListCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }
  
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.type == 0) {
        YXShareOrdersModelList *model = self.dataArr[indexPath.row];
        YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
        vc.type = 2;
        vc.Id = model.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        YXVideoModel *model = self.dataArr[indexPath.row];
        YXMediaDetailsTwoViewController *vc = [YXMediaDetailsTwoViewController new];
        vc.Id = model.v_id;
        [self.navigationController pushViewController:vc animated:YES];
    }
  
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerClass:[YXMediaListCollectionViewCell class] forCellWithReuseIdentifier:@"YXMediaListCollectionViewCell"];
        [_collectionView registerClass:[YXHomeOrderCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        }];
        [_collectionView setRefreshWithHeaderBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        } footerBlock:^{
            [weakSelf.collectionView footerEndRefreshing];
        }];

        
    }
    return _collectionView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
