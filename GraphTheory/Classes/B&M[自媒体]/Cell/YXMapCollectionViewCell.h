//
//  YXMapCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel,YXOrderInfoModel;
@interface YXMapCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) YXVideoModel *model;

@property (strong, nonatomic) YXOrderInfoModel *orderModel;

@end

NS_ASSUME_NONNULL_END
