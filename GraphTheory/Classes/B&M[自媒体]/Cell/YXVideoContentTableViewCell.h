//
//  YXVideoContentTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXVideoContentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *playNumLab;
@property (weak, nonatomic) IBOutlet UILabel *replyNumLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (nonatomic ,strong) YXVideoModel *model;

@end

NS_ASSUME_NONNULL_END
