//
//  YXMediaListCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXMediaListCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,strong) YXVideoModel *model;

@end

NS_ASSUME_NONNULL_END
