//
//  YXVideoUserTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXVideoUserTableViewCell : UITableViewCell

@property (nonatomic ,copy)void(^clickGZBlock)(UIButton *btn);
@property (nonatomic ,copy)void(^clickCollectionBlock)(UIButton *btn);
@property (nonatomic ,copy)void(^clickPraiseBlock)(UIButton *btn);

@property (weak, nonatomic) IBOutlet UIButton *gzBtn;
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn;
@property (weak, nonatomic) IBOutlet UIButton *praiseBtn;

@property (nonatomic ,strong) YXVideoModel *model;
@end

NS_ASSUME_NONNULL_END
