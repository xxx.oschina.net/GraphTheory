//
//  YXGoodsInfoCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXGoodsInfoCollectionViewCell.h"
#import "YXVideoModel.h"
@implementation YXGoodsInfoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.backgroundColor = [UIColor whiteColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.backView.layer.borderColor = color_LineColor.CGColor;
    self.backView.layer.borderWidth = 1.0;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.layer.cornerRadius = 4.0;
}

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    if (_model.goodsInfo.banner.count) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.goodsInfo.banner[0]]];
    }
    _titleLab.text = _model.goodsInfo.title;
    _subTitleLab.text = _model.v_introduce;
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.goodsInfo.sales_price];
}

@end
