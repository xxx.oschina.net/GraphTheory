//
//  YXGoodsInfoCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXGoodsInfoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@property (weak, nonatomic) IBOutlet UILabel *subTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@property (nonatomic ,strong) YXVideoModel *model;

@end

NS_ASSUME_NONNULL_END
