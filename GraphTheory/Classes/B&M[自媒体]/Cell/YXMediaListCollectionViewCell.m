//
//  YXMediaListCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMediaListCollectionViewCell.h"
#import "YXVideoModel.h"
#import "YXDataTimeTool.h"

@interface YXMediaListCollectionViewCell ()

@property (nonatomic ,strong)  UIView *bgView;

@property (nonatomic ,strong)  UIView *topView;

/**
 图片
 */
@property (nonatomic ,strong) UIImageView *leftImgView;

/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;

/**
 当前金额
 */
@property (nonatomic ,strong) UILabel *curPriceLab;

/**
 原价金额
 */
@property (nonatomic ,strong) UILabel *originalPriceLab;
/**
 播放次数按钮
 */
@property (nonatomic ,strong) UIButton *playBtn;
/**
 回复数按钮
 */
@property (nonatomic ,strong) UIButton *replyBtn;
/**
 视频时长
 */
@property (nonatomic ,strong) UILabel *timeLab;


@end

@implementation YXMediaListCollectionViewCell

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    
    [_leftImgView sd_setImageWithURL:[NSURL URLWithString:_model.v_cover_img] placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
    _titleLab.text = _model.v_name;
    _curPriceLab.text = _model.topBrandInfo.brand_name;
    _originalPriceLab.text = _model.brand.brand_name;
    
    [_playBtn setTitle:_model.view_count forState:(UIControlStateNormal)];
    [_replyBtn setTitle:_model.reply_count forState:(UIControlStateNormal)];
    
    NSString *dataStr = [YXDataTimeTool getTimeStrWithString:_model.v_time Format:@"yyyy-MM-dd HH:mm"];
    _timeLab.text = [YXDataTimeTool timestampToString:[dataStr integerValue] withFormat:@"HH:mm"];

    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.bgView];
        [self.bgView addSubview:self.topView];
        [self.topView addSubview:self.leftImgView];
        [self.topView addSubview:self.playBtn];
        [self.topView addSubview:self.replyBtn];
        [self.topView addSubview:self.timeLab];
        [self.bgView addSubview:self.titleLab];
        [self.bgView addSubview:self.curPriceLab];
        [self.bgView addSubview:self.originalPriceLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.layer.masksToBounds = YES;
        _bgView.layer.cornerRadius = 4.0f;
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.layer.borderWidth = 1;
        _bgView.layer.borderColor = color_LineColor.CGColor;
    }
    return _bgView;
}

- (UIView *)topView {
    if (!_topView) {
        _topView = [UIView new];
    }
    return _topView;
}

- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [UIImageView new];
        _leftImgView.image = [UIImage imageNamed:@"goods_placeholder"];
        _leftImgView.contentMode = UIViewContentModeScaleAspectFill;
        _leftImgView.layer.masksToBounds = YES;
    }
    return _leftImgView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_playBtn setImage:[UIImage imageNamed:@"bofang_ffffff"] forState:UIControlStateNormal];
        [_playBtn setTitle:@"22" forState:(UIControlStateNormal)];
        [_playBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _playBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        [_playBtn setImgViewStyle:(ButtonImgViewStyleLeft) imageSize:(CGSizeMake(20, 20)) space:5];
    }
    return _playBtn;
}

- (UIButton *)replyBtn {
    if (!_replyBtn) {
        _replyBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_replyBtn setImage:[UIImage imageNamed:@"liuyan_ffffff"] forState:UIControlStateNormal];
        [_replyBtn setTitle:@"1" forState:(UIControlStateNormal)];
        [_replyBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _replyBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        [_replyBtn setImgViewStyle:(ButtonImgViewStyleLeft) imageSize:(CGSizeMake(20, 20)) space:5];
    }
    return _replyBtn;
}
- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.font = [UIFont systemFontOfSize:10.0f];
        _timeLab.textColor = [UIColor whiteColor];
        _timeLab.text = @"14:08";
    }
    return _timeLab;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16.0f];
        _titleLab.textColor = color_TextOne;
        _titleLab.numberOfLines = 2;
        _titleLab.text = @"阿来得及拉的几率就发点福利拉卡机的立法江东父老安徽的咖对话框";
    }
    return _titleLab;
}


- (UILabel *)curPriceLab {
    if (!_curPriceLab) {
        _curPriceLab = [[UILabel alloc] init];
        _curPriceLab.textColor = color_TextThree;
        _curPriceLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _curPriceLab;
}

- (UILabel *)originalPriceLab {
    if (!_originalPriceLab) {
        _originalPriceLab = [[UILabel alloc] init];
        _originalPriceLab.textColor = color_TextThree;
        _originalPriceLab.font = [UIFont systemFontOfSize:14.0];
    }
    return _originalPriceLab;
}


- (void)addLayout {
    
    YXWeakSelf
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.contentView);
    }];
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(weakSelf.bgView);
        make.height.equalTo(@200);
    }];
    
    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.topView);
    }];
    
    [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.topView.mas_left).offset(15);
        make.bottom.equalTo(weakSelf.topView.mas_bottom).offset(-15);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    [self.replyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.playBtn.mas_centerY);
        make.left.equalTo(weakSelf.playBtn.mas_right).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.replyBtn.mas_centerY);
        make.left.equalTo(weakSelf.replyBtn.mas_right).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bgView.mas_left).offset(10);
        make.top.equalTo(weakSelf.topView.mas_bottom).offset(10);
        make.right.equalTo(weakSelf.bgView.mas_right).offset(-10);
    }];
    
    
    [self.curPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLab.mas_left);
        make.bottom.equalTo(weakSelf.bgView.mas_bottom).offset(-15);
        [weakSelf.curPriceLab sizeToFit];
    }];
    
    [self.originalPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.curPriceLab.mas_right).offset(5);
        make.centerY.equalTo(weakSelf.curPriceLab.mas_centerY);
        [weakSelf.originalPriceLab sizeToFit];
    }];

}

@end
