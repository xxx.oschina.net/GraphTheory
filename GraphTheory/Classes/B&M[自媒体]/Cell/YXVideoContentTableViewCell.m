//
//  YXVideoContentTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXVideoContentTableViewCell.h"
#import "YXVideoModel.h"
@implementation YXVideoContentTableViewCell

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    
    _contentLab.text = _model.v_name;
    _playNumLab.text = _model.view_count;
    _replyNumLab.text = _model.reply_count;
    _timeLab.text = _model.v_time;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
