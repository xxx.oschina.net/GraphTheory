//
//  YXVideoUserTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXVideoUserTableViewCell.h"
#import "YXVideoModel.h"

@interface YXVideoUserTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *collectionLab;
@property (weak, nonatomic) IBOutlet UILabel *praiseLab;
@end

@implementation YXVideoUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.gzBtn setBackgroundColor:APPTintColor];
    self.gzBtn.layer.masksToBounds = YES;
    self.gzBtn.layer.cornerRadius = 4.0;
    self.userImg.contentMode = UIViewContentModeScaleAspectFill;
    self.userImg.clipsToBounds = YES;
    self.userImg.layer.cornerRadius = 20.0f;
    
}

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    
    [_userImg sd_setImageWithURL:[NSURL URLWithString:_model.ownerInfo.wechat_headimgurl]];
    _nameLab.text = [NSString stringWithFormat:@"%@:%@",_model.ownerInfo.nike,_model.ownerInfo.show_video_id];
    _numberLab.text = [NSString stringWithFormat:@"%@人",_model.ownerInfo.fans_count];
    
    _praiseLab.text = _model.like_count;
    _collectionLab.text = _model.collect_count;
    
    _gzBtn.selected = _model.is_fans;
    _praiseBtn.selected = _model.is_like;
    _collectionBtn.selected = _model.is_collect;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)gzBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.clickGZBlock) {
        self.clickGZBlock(sender);
    }
}

- (IBAction)collectionBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.clickCollectionBlock) {
        self.clickCollectionBlock(sender);
    }
}

- (IBAction)praiseBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.clickPraiseBlock) {
        self.clickPraiseBlock(sender);
    }
}

@end
