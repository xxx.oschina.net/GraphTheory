//
//  YXCategoryContentView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCategoryContentView.h"
#import "YXHomeMenuCollectionViewCell.h"
#import "YXCateModel.h"
#import "YXPublicCollectionReusableView.h"
@interface YXCategoryContentView ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong)UILabel *titleLab;

@end

@implementation YXCategoryContentView

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setDataArr:(NSMutableArray *)dataArr {
    _dataArr = dataArr;
    [self.collectionView reloadData];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpLayout];
    }
    return self;
}

- (void)setUpLayout {
    [self addSubview:self.titleLab];
    [self addSubview:self.collectionView];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@50);
    }];
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.top.equalTo(self.titleLab.mas_bottom);
    }];

}
#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.type == 0) {
        return self.dataArr.count;
    }else {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.type == 0) {
        YXCateChildModel *childModel = self.dataArr[section];
        return childModel.child.count;
    }else {
        return self.dataArr.count;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.frame.size.width - 20)/ 3, 80);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == 0) {
        YXHomeMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeMenuCollectionViewCell" forIndexPath:indexPath];
        YXCateChildModel *childModel = self.dataArr[indexPath.section];
        cell.childModel = childModel.child[indexPath.row];
        return cell;
    }else {
        YXHomeMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeMenuCollectionViewCell" forIndexPath:indexPath];
        cell.brandModel = self.dataArr[indexPath.row];
        return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (self.type == 0) {
        return CGSizeMake(KWIDTH, 40);
    }
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        YXPublicCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
        YXCateChildModel *childModel = self.dataArr[indexPath.section];
        header.titleLab.text = childModel.name;
        return header;
    }
    return nil;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.selectCategory) {
        self.selectCategory(indexPath);
    }
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.emptyDataSetSource = self;
        _collectionView.emptyDataSetDelegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXPublicCollectionReusableView"];
        [_collectionView registerClass:[YXHomeMenuCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeMenuCollectionViewCell"];
//        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
//            weakSelf.page = 1;
//            [weakSelf request];
//        }];
    }
    return _collectionView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.backgroundColor = [UIColor whiteColor];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLab.text = @"全部分类";
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}
@end
