//
//  YXCommentsView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXCommentsView : UIView <JXPagerViewListViewDelegate>
@property (nonatomic ,strong) YXVideoModel *model;
@end

NS_ASSUME_NONNULL_END
