//
//  YXCategoryContentView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseView.h"

NS_ASSUME_NONNULL_BEGIN
@interface YXCategoryContentView : DDBaseView

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSMutableArray *dataArr;


@property (nonatomic, copy) void(^selectCategory)(NSIndexPath *indexPath);

@end

NS_ASSUME_NONNULL_END
