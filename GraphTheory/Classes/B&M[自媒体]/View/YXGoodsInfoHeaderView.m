//
//  YXGoodsInfoHeaderView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXGoodsInfoHeaderView.h"
#import "YXPlaceOrderViewController.h"
#import "YXVideoCollectionViewCell.h"
#import "YXGoodsInfoCollectionViewCell.h"
#import "YXMapCollectionViewCell.h"
#import "YXVideoModel.h"
@interface YXGoodsInfoHeaderView ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@end

@implementation YXGoodsInfoHeaderView

- (void)setModel:(YXVideoModel *)model {
    _model = model;
    [self.collectionView reloadData];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
    }
    return self;
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return CGSizeMake(SCREEN_WIDTH, 100);
    }else if (indexPath.section == 1){
        return CGSizeMake(SCREEN_WIDTH, 200);
    }
    return CGSizeZero;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
   if (indexPath.section == 0){
        YXGoodsInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXGoodsInfoCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 1){
        YXMapCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMapCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }
    return [UICollectionViewCell new];
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        YXPlaceOrderViewController *vc = [[YXPlaceOrderViewController alloc] init];
        vc.goods_id = self.model.goodsInfo.goods_id;
        [[UIView currentViewController].navigationController pushViewController:vc animated:YES];
    }
 
}



- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.sectionInset = UIEdgeInsetsZero;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 300) collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.scrollEnabled = NO;
        [_collectionView registerClass:[YXVideoCollectionViewCell class] forCellWithReuseIdentifier:@"YXVideoCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXGoodsInfoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXGoodsInfoCollectionViewCell"];
        [_collectionView registerClass:[YXMapCollectionViewCell class] forCellWithReuseIdentifier:@"YXMapCollectionViewCell"];

    }
    return _collectionView;
}


@end
