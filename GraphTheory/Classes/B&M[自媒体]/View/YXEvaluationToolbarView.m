//
//  YXTopicToolbarView.m
//  CreditCard
//
//  Created by 杨旭 on 2019/3/8.
//  Copyright © 2017年 杨旭. All rights reserved.
//
#define DEFAULT_HEIGHT (50)

#import "YXEvaluationToolbarView.h"

@interface YXEvaluationToolbarView ()<UITextFieldDelegate>

{
    UIControl * _overView;
    BOOL      _isKeyboardShow;
    BOOL      _isThirdPartKeyboard;
    NSInteger _keyboardShowTime;
    CGFloat   _defaultConstraint;
    CGFloat   _keyboardAnimateDur;
    CGPoint   _oldOffset;
    CGRect    _keyboardFrame;
    
}

@end

@implementation YXEvaluationToolbarView

- (UITextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[UITextField alloc] init];
        _contentTF.delegate = self;
        _contentTF.placeholder = @"友善发言，温暖评论区";
        _contentTF.borderStyle = UITextBorderStyleRoundedRect;
        _contentTF.font = [UIFont systemFontOfSize:14];
        _contentTF.returnKeyType = UIReturnKeySend;
    }
    return _contentTF;
}

- (UIButton *)sendBtn {
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _sendBtn.backgroundColor = APPTintColor;
        [_sendBtn setTitle:@"提交" forState:(UIControlStateNormal)];
        _sendBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _sendBtn.layer.masksToBounds = YES;
        _sendBtn.layer.cornerRadius = 4.0;
        [_sendBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_sendBtn addTarget:self action:@selector(sendBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _sendBtn;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"#f2f2f2"];
     
    
        _overView=[[UIControl alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _overView.backgroundColor=[UIColor colorWithWhite:0.6 alpha:0.1];
        [_overView addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.sendBtn];
        [self addSubview:self.contentTF];
        [self layoutSubviews1];
        
        [self initKeyboardObserver];
        
        
    }
    return self;
}

// 点击回复按钮
- (void)sendBtnAction:(UIButton *)sender {
    
    if (self.clickSendBtnBlick) {
        self.clickSendBtnBlick(self.contentTF.text);
    }

    [self dismiss];

}

-(void)show
{
    UIWindow * keyWindow=[UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:_overView];
    [keyWindow addSubview:self];
    
    [self.inputView becomeFirstResponder];
    [self.contentTF becomeFirstResponder];

}

-(void)dismiss
{

    [self.inputView resignFirstResponder];
    [self.contentTF resignFirstResponder];
}

- (void)layoutSubviews1 {
//    [super layoutSubviews];
    YXWeakSelf
    [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(60, 32));
    }];
    
    [_contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.sendBtn.mas_left).offset(-10);
        make.height.offset(32);
    }];
}


#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (self.clickSendBtnBlick) {
        self.clickSendBtnBlick(self.contentTF.text);
    }
    
    [self dismiss];
    
    return YES;
}

#pragma mark - 键盘事件
- (void)initKeyboardObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    
    NSValue* keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [keyboardBoundsValue CGRectValue];
    
    NSNumber* keyboardAnimationDur = [[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    float animationDur = [keyboardAnimationDur floatValue];
    
    _keyboardShowTime++;
    
    // 第三方输入法有bug,第一次弹出没有keyboardRect
    if (animationDur > 0.0f && keyboardRect.size.height == 0)
    {
        _isThirdPartKeyboard = YES;
    }
    
    // 第三方输入法,有动画间隔时,没有高度
    if (_isThirdPartKeyboard)
    {
        // 第三次调用keyboardWillShow的时候 键盘完全展开
        if (_keyboardShowTime == 3 && keyboardRect.size.height != 0 && keyboardRect.origin.y != 0)
        {
            _keyboardFrame = keyboardRect;
            
            NSLog(@"_keyboardFrame.size.height--%f",_keyboardFrame.size.height);
            //Animate change
            [UIView animateWithDuration:_keyboardAnimateDur animations:^{
                
                self.frame=CGRectMake(0,kHEIGHT- self->_keyboardFrame.size.height-DEFAULT_HEIGHT, KWIDTH, DEFAULT_HEIGHT);
                
                [self layoutIfNeeded];
                [self layoutSubviews1];
            }];
            
        }
        if (animationDur > 0.0)
        {
            _keyboardAnimateDur = animationDur;
        }
    }
    else
    {
        if (animationDur > 0.0)
        {
            _keyboardFrame = keyboardRect;
            _keyboardAnimateDur = animationDur;
            
            //Animate change
            [UIView animateWithDuration:_keyboardAnimateDur animations:^{
                
                self.frame=CGRectMake(0,kHEIGHT- self->_keyboardFrame.size.height-DEFAULT_HEIGHT, KWIDTH, DEFAULT_HEIGHT);
                
                [self layoutIfNeeded];
                [self layoutSubviews1];
            }];
        }
    }
}

- (void)keyboardDidShow:(NSNotification*)notification
{
    _isKeyboardShow = YES;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    NSNumber* keyboardAnimationDur = [[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    float animationDur = [keyboardAnimationDur floatValue];
    
    _isThirdPartKeyboard = NO;
    _keyboardShowTime = 0;
    
    if (animationDur > 0.0)
    {
        
        [_overView removeFromSuperview];
        [UIView animateWithDuration:_keyboardAnimateDur animations:^{
            
//            self.frame=CGRectMake(0,kHEIGHT - DEFAULT_HEIGHT - self->_keyboardFrame.size.height, KWIDTH, DEFAULT_HEIGHT);
            self.frame=CGRectMake(0,kHEIGHT - DEFAULT_HEIGHT, KWIDTH, DEFAULT_HEIGHT);

            [self layoutIfNeeded];
            [self layoutSubviews1];
        } completion:^(BOOL finished) {
            
//            [self removeFromSuperview];
        }];
    }
}
- (void)keyboardDidHide:(NSNotification*)notification
{
    _isKeyboardShow = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
