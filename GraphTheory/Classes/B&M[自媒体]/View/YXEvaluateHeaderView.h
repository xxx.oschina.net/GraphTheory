//
//  YXEvaluateHeaderView.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YXEvaluateModel;
@interface YXEvaluateHeaderView : UITableViewHeaderFooterView

@property (nonatomic ,strong) YXEvaluateModel *model;

@end


