//
//  YXTopicToolbarView.h
//  CreditCard
//
//  Created by 杨旭 on 2019/3/8.
//  Copyright © 2017年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXEvaluationToolbarView : UIView
@property (nonatomic ,copy)void(^clickSendBtnBlick)(NSString *content);
@property (strong, nonatomic)  UITextField *contentTF;
@property (strong, nonatomic)  UIButton *sendBtn;

-(void)show;
-(void)dismiss;
@end
