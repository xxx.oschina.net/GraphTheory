//
//  YXMediaBottomView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXPagerView.h"

@class YXVideoModel;
@interface YXMediaBottomView : UIView <JXPagerViewListViewDelegate>

@property (nonatomic ,copy)void(^clickGZBlock)(UIButton *btn);
@property (nonatomic ,copy)void(^clickCollectionBlock)(UIButton *btn);
@property (nonatomic ,copy)void(^clickPraiseBlock)(UIButton *btn);

@property (nonatomic ,assign) NSInteger *state;

@property (nonatomic ,strong) YXVideoModel *model;

@end

