//
//  YXGoodsInfoHeaderView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXGoodsInfoHeaderView : UIView
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) YXVideoModel *model;

@end

NS_ASSUME_NONNULL_END
