//
//  YXEvaluateCell.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YXVideoModel;
@interface YXEvaluateCell : UITableViewCell


/** 顶部视图*/
@property (weak, nonatomic) IBOutlet UIView *topView;
/** 用户头像*/
@property (weak, nonatomic) IBOutlet UIImageView *userImg;
/** 用户名称*/
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
/** 时间*/
@property (weak, nonatomic) IBOutlet UILabel *timeLab;


/** 评价内容*/
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
/** 回复按钮*/
@property (weak, nonatomic) IBOutlet UIButton *replyBtn;

@property (weak, nonatomic) IBOutlet UILabel *replyLab;

@property (nonatomic ,strong) YXVideoModel *model;

@property (nonatomic ,strong) NSArray *commentsArr;

@end


