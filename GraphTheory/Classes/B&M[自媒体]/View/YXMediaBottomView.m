//
//  YXMediaBottomView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMediaBottomView.h"
#import "YXMyCardViewController.h"

#import "YXVideoUserTableViewCell.h"
#import "YXVideoContentTableViewCell.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"

@interface YXMediaBottomView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic ,strong) UITableView *tableView;
@end

@implementation YXMediaBottomView
//- (void)dealloc{
//    self.scrollCallback = nil;
//}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.tableView.frame = self.bounds;
}

#pragma mark - UITbleView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 70;
    }else {
        return 70;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        YXVideoUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXVideoUserTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        DDWeakSelf
        [cell setClickGZBlock:^(UIButton * _Nonnull btn) {
            if (weakSelf.clickGZBlock) {
                weakSelf.clickGZBlock(btn);
            }
        }];
        [cell setClickCollectionBlock:^(UIButton * _Nonnull btn) {
            if (weakSelf.clickCollectionBlock) {
                weakSelf.clickCollectionBlock(btn);
            }
        }];
        [cell setClickPraiseBlock:^(UIButton * _Nonnull btn) {
            if (weakSelf.clickPraiseBlock) {
                weakSelf.clickPraiseBlock(btn);
            }
        }];
        return cell;
    }else {
        YXVideoContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXVideoContentTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }
    return [UITableViewCell new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        YXBusinessCardViewController *vc = [YXBusinessCardViewController new];
//        vc.user_id = self.model.user_id;
//        [[UIView currentViewController].navigationController pushViewController:vc animated:YES];
//    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.scrollCallback(scrollView);
}

#pragma mark - JXPagingViewListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}
- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}
- (UIView *)listView {
    return self;
}

- (void)listViewLoadDataIfNeeded {
//    YXWeakSelf
//    if (!self.isFirstLoaded) {
//        _isFirstLoaded = YES;
//        _index = 1;
//        [self request];
//        [YJProgressHUD showLoading:@""];
//
//    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"YXVideoUserTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXVideoUserTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXVideoContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXVideoContentTableViewCell"];
    }
    return _tableView;
}


@end
