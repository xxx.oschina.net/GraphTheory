//
//  YXCommentsView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCommentsView.h"
#import "YXEvaluateCell.h"

#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
#import "YXCommentsModel.h"
#import "YXPublicSectionView.h"

@interface YXCommentsView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *commentsArr;
@end

@implementation YXCommentsView

- (void)dealloc{
    self.scrollCallback = nil;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}
- (void)layoutSubviews {
    [super layoutSubviews];
//    self.tableView.frame = self.bounds;
}

- (void)request {
    
    DDWeakSelf
    [YXVideoViewModel queryReplyVideoListWithId:self.model.v_id Completion:^(id  _Nonnull responesObj) {
        YXCommentsModel *model = [YXCommentsModel mj_objectWithKeyValues:responesObj[@"data"]];
        weakSelf.commentsArr = [YXCommentsModelList mj_objectArrayWithKeyValuesArray:model.list];
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


#pragma mark - UITbleView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXEvaluateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXEvaluateCell" forIndexPath:indexPath];
    cell.model = self.model;
    cell.commentsArr = self.commentsArr;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    YXPublicSectionView2 *headerView =[[YXPublicSectionView2 alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 40))];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.scrollCallback(scrollView);
}
#pragma mark - JXPagingViewListViewDelegate
- (UIScrollView *)listScrollView {
    return self.tableView;
}
- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}
- (UIView *)listView {
    return self;
}

- (void)listViewLoadDataIfNeeded {
//    YXWeakSelf
//    if (!self.isFirstLoaded) {
//        _isFirstLoaded = YES;
//        _index = 1;
//        [self request];
//        [YJProgressHUD showLoading:@""];
//
//    }

}

- (void)listWillAppear {
    [self request];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"YXEvaluateCell" bundle:nil] forCellReuseIdentifier:@"YXEvaluateCell"];
    }
    return _tableView;
}
- (NSMutableArray *)commentsArr {
    if (!_commentsArr) {
        _commentsArr = [NSMutableArray array];
    }
    return _commentsArr;
}

@end
