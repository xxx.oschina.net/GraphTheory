//
//  YXEvaluateHeaderView.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEvaluateHeaderView.h"
#import "YXEvaluateModel.h"
@interface YXEvaluateHeaderView ()


/** 标题*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 店铺名称*/
@property (nonatomic ,strong) UILabel *shopNameLab;

@end

@implementation YXEvaluateHeaderView

- (void)setModel:(YXEvaluateModel *)model {
    _model = model;
    
    _nameLab.text = _model.productName;
    _shopNameLab.text = [NSString stringWithFormat:@"(%@)",_model.shopsName];
    
}


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self setup];
        [self addLayout];
    }
    return self;
}


- (void)setup {
    [self.contentView setBackgroundColor:color_LineColor];
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.shopNameLab];
}


#pragma mark - Lazy loading

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.text = @"点都科技";
        _nameLab.textColor = [UIColor blackColor];
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
    }
    return _nameLab;
}

- (UILabel *)shopNameLab {
    if (!_shopNameLab) {
        _shopNameLab = [[UILabel alloc] init];
        _shopNameLab.text = @"盛邦食堂";
        _shopNameLab.textColor =  color_TextTwo;
        _shopNameLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _shopNameLab;
}

- (void)addLayout {
    
    YXWeakSelf
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_shopNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nameLab.mas_right).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.shopNameLab sizeToFit];
    }];
    

}

@end
