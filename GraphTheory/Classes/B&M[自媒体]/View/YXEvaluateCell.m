//
//  YXEvaluateCell.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEvaluateCell.h"
#import "XHStarRateView.h"
#import "YXEvaluateModel.h"
#import "YXVideoModel.h"
#import "YXCommentsModel.h"
@interface YXEvaluateCell ()

@property (nonatomic ,strong) XHStarRateView *starView;

@property (nonatomic ,strong)  UILabel *starLab;

//@property (nonatomic ,strong) YXPhotoBrowserView *photoView;;

@property (nonatomic ,strong) UIView *replyView;

@property (nonatomic ,strong) UITextView *contentTV;

@property (nonatomic ,strong) UIButton *submitBtn;
@end

@implementation YXEvaluateCell

- (XHStarRateView *)starView {
    if (!_starView) {
        _starView = [[XHStarRateView alloc] initWithFrame:(CGRectMake(CGRectGetMaxX(self.nameLab.frame)+5, 15, 80, 20))];
        _starView.rateStyle = WholeStar;
        _starView.userInteractionEnabled = NO;
    }
    return _starView;
}

- (UILabel *)starLab {
    if (!_starLab) {
        _starLab = [[UILabel alloc] initWithFrame:(CGRectMake(CGRectGetMaxX(self.starView.frame)+5, 15, 20, 20))];
        _starLab.textColor = [UIColor orangeColor];
        _starLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _starLab;
}


- (UIView *)replyView {
    if (!_replyView) {
        _replyView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 100))];
    }
    return _replyView;
}

- (UITextView *)contentTV {
    if (!_contentTV) {
        _contentTV = [[UITextView alloc] initWithFrame:(CGRectMake(40, 10, KWIDTH-55, 60))];
        _contentTV.layer.masksToBounds = YES;
        _contentTV.layer.cornerRadius = 2.0f;
        _contentTV.layer.borderWidth = 1.0f;
        _contentTV.layer.borderColor = color_LineColor.CGColor;
    }
    return _contentTV;
}


- (void)setModel:(YXVideoModel *)model {
    _model = model;
    [_userImg sd_setImageWithURL:[NSURL URLWithString:_model.ownerInfo.wechat_headimgurl]];
    _nameLab.text = [NSString stringWithFormat:@"%@:%@",_model.ownerInfo.nike,_model.ownerInfo.show_video_id];
    _timeLab.text = _model.v_time;
    _contentLab.text = _model.v_introduce;
}

- (void)setCommentsArr:(NSArray *)commentsArr {
    _commentsArr = commentsArr;
    
    if (_commentsArr.count) {
        _replyLab.hidden = NO;
        YXCommentsModelList *model = [_commentsArr firstObject];
        NSString *content = [NSString stringWithFormat:@"%@：%@",model.user_name,model.content];
        CGSize textMaxSize = CGSizeMake(KWIDTH - 80, MAXFLOAT);
        CGSize textSize = [content boundingRectWithSize:textMaxSize options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0]} context:nil].size;
        _replyLab.frame = CGRectMake(60, 100, KWIDTH - 80, textSize.height);
        _replyLab.text = [NSString stringWithFormat:@"%@：%@",model.user_name,model.content];
        [self.contentView addSubview:self.replyLab];
    }else {
        _replyLab.hidden = YES;
    }
}

//- (void)setEvaluateModel:(YXEvaluateModel *)evaluateModel {
//    _evaluateModel = evaluateModel;
//
//    _nameLab.text = [NSString userNameEncryption:_evaluateModel.createName];
//    _timeLab.text = _evaluateModel.evaluateDueTime;
//    _starLab.text = [NSString stringWithFormat:@"%.1lf",_evaluateModel.evaluateStar];
//    _contentLab.text = _evaluateModel.productEvaluate == nil ? @"暂无评价！" : _evaluateModel.productEvaluate;
//    _starView.currentScore = _evaluateModel.evaluateStar;
//
//    _replyBtn.selected = _evaluateModel.isReply;
//
//
//    if (_evaluateModel.evaluatePhoto.count > 0) {
//        _photoView.hidden = NO;
//        NSMutableArray *imageArr = [NSMutableArray array];
//        for (YXEvaluatePhotoModel *photoModel in _evaluateModel.evaluatePhoto) {
//            NSString *url = [NSString stringWithFormat:@"%@%@",orderBasePath,photoModel.photoUrl];
//            [imageArr addObject:url];
//        }
//
//        _photoView.dataArray = imageArr;
//        _photoView.frame = _evaluateModel.photoViewF;
//    }else {
//        self.photoView.hidden = YES;
//    }
    
    
//    // 是否回复
//    if (_evaluateModel.replyMessage.count > 0) {
//        _replyBtn.hidden = YES;
//        _replyLab.hidden = NO;
//        _replyLab.frame = _evaluateModel.replyLabF;
//        _replyLab.text = [NSString stringWithFormat:@"商家回复：%@",[_evaluateModel.replyMessage firstObject]];
//        [self.contentView addSubview:self.replyLab];
//
//    }else {
//        _replyBtn.hidden = NO;
//        _replyLab.hidden = YES;
//    }
    
//}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _userImg.layer.masksToBounds = YES;
    _userImg.layer.cornerRadius = 20.0f;
    _timeLab.textColor = color_TextThree;
    
//    [self.topView addSubview:self.starView];
//    [self.topView addSubview:self.starLab];
//    [self addSubview:self.photoView];


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
