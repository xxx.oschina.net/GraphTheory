//
//  YXVideoViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXVideoViewModel : DDBaseModel

/**
视频列表

@param page    当前页码
@param limit     每页的条数
@param keyword    搜索关键字
@param service_ids    服务id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryVideoListWithPage:(NSString *)page
                         limit:(NSString *)limit
                       keyword:(NSString *)keyword
                   service_ids:(NSString *)service_ids
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;

/**
视频列表

@param Id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryVideoInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError;

/**
关注视频博主

@param type    1
@param author_id    作者id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryFollowAuthorWithtype:(NSString *)type
                        author_id:(NSString *)author_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 点赞视频

@param type    1
@param video_id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryLikeVideoWithtype:(NSString *)type
                      video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
 收藏视频

@param type    1
@param video_id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryCollectVideoWithtype:(NSString *)type
                         video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
评论视频

@param content    内容
@param video_id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryReplyVideoWithContent:(NSString *)content
                          video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
评论视频列表

@param Id    主键id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryReplyVideoListWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 商品详情

@param Id    主键id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryGoodsInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError;

/**
 评论商品

@param goods_id    商品id
@param content    内容
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryReplyGoodsWithGoods_id:(NSString *)goods_id
                            Content:(NSString *)content
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

/**
 增加视频浏览量

@param Id    主键id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryAddVideoViewCountWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;

/**
 增加商品浏览量

@param Id    主键id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryAddGoodsViewCountWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;

/**
 视频博主服务分类列表【根据城市获取】

 @param city_id    城市id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryAllBrandsWithCity_id:(NSString *)city_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 视频博主服务分类下所有子集

@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryTopBrandDetailWithbrand_id:(NSString *)brand_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;

/**
 视频博主服务分类详情

@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryBrandDetailWithbrand_id:(NSString *)brand_id
                            Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;


/**
 C2M视频列表

@param page    当前页码
@param limit     每页的条数
@param is_delete    是否删除
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryMyVideoListWithPage:(NSString *)page
                         limit:(NSString *)limit
                     is_delete:(NSString *)is_delete
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;

/**
创建/修改C2M视频信息
 
@param type    0:创建 1:修改
@param completion  返回成功
@param conError    返回失败
*/
+ (void)requestCreateVideoWithType:(NSString *)type
                                Id:(NSString *)Id
                             Title:(NSString *)title
                      Top_category:(NSString *)top_category
                          category:(NSString *)category
                         cover_img:(NSString *)cover_img
                         introduce:(NSString *)introduce
                             video:(NSString *)video
                               lat:(NSString *)lat
                               lng:(NSString *)lng
                          goods_id:(NSString *)goods_id
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;


/**
 删除C2M视频信息

@param Id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryDeleteVideoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;


/**
 C2M视频详情

@param Id    视频id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryMyVideoInfoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;


@end

NS_ASSUME_NONNULL_END
