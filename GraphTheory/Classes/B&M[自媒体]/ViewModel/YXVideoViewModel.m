//
//  YXVideoViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXVideoViewModel.h"

@implementation YXVideoViewModel

#pragma mark - 视频列表
+ (void)queryVideoListWithPage:(NSString *)page
                         limit:(NSString *)limit
                       keyword:(NSString *)keyword
                   service_ids:(NSString *)service_ids
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/videoList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:limit forKey:@"limit"];
    [params setValue:keyword forKey:@"keyword"];
    [params setValue:service_ids forKey:@"service_ids"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 视频详情
+ (void)queryVideoInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/videoInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 关注视频博主
+ (void)queryFollowAuthorWithtype:(NSString *)type
                        author_id:(NSString *)author_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/followAuthor",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [params setValue:author_id forKey:@"author_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 点赞视频
+ (void)queryLikeVideoWithtype:(NSString *)type
                      video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/likeVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [params setValue:video_id forKey:@"video_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 收藏视频
+ (void)queryCollectVideoWithtype:(NSString *)type
                         video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/collectVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [params setValue:video_id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 评论视频
+ (void)queryReplyVideoWithContent:(NSString *)content
                          video_id:(NSString *)video_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@center/replyVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:content forKey:@"content"];
    [params setValue:video_id forKey:@"video_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 评论视频列表
+ (void)queryReplyVideoListWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/replyVideoList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  商品详情
+ (void)queryGoodsInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/goodsInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  评论商品
+ (void)queryReplyGoodsWithGoods_id:(NSString *)goods_id
                            Content:(NSString *)content
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/replyGoods",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:goods_id forKey:@"goods_id"];
    [params setValue:content forKey:@"content"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  增加视频浏览量
+ (void)queryAddVideoViewCountWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/addVideoViewCount",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  增加商品浏览量
+ (void)queryAddGoodsViewCountWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/addGoodsViewCount",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}




#pragma mark - 视频博主服务分类列表【根据城市获取】
+ (void)queryAllBrandsWithCity_id:(NSString *)city_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/allBrands",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:city_id forKey:@"city_id"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 视频博主服务分类下所有子集
+ (void)queryTopBrandDetailWithbrand_id:(NSString *)brand_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/topBrandDetail",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:brand_id forKey:@"brand_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 视频博主服务分类详情
+ (void)queryBrandDetailWithbrand_id:(NSString *)brand_id
                            Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@index/brandDetail",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:brand_id forKey:@"brand_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - C2M视频列表
+ (void)queryMyVideoListWithPage:(NSString *)page
                         limit:(NSString *)limit
                     is_delete:(NSString *)is_delete
                    Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myVideoList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:limit forKey:@"limit"];
    [params setValue:is_delete forKey:@"is_delete"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 创建/修改C2M视频信息
+ (void)requestCreateVideoWithType:(NSString *)type
                                Id:(NSString *)Id
                             Title:(NSString *)title
                      Top_category:(NSString *)top_category
                          category:(NSString *)category
                         cover_img:(NSString *)cover_img
                         introduce:(NSString *)introduce
                             video:(NSString *)video
                               lat:(NSString *)lat
                               lng:(NSString *)lng
                          goods_id:(NSString *)goods_id
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([type isEqualToString:@"1"]) {
        urlStr = [NSString stringWithFormat:@"%@center/updateVideo",kBaseURL];
        [params setValue:Id forKey:@"id"];
    }else {
        urlStr = [NSString stringWithFormat:@"%@center/createVideo",kBaseURL];
    }
    [params setValue:title forKey:@"title"];
    [params setValue:top_category forKey:@"top_category"];
    [params setValue:category forKey:@"category"];
    [params setValue:cover_img forKey:@"cover_img"];
    [params setValue:introduce forKey:@"introduce"];
    [params setValue:video forKey:@"video"];
    [params setValue:lat forKey:@"address[lat]"];
    [params setValue:lng forKey:@"address[lng]"];
    [params setValue:goods_id forKey:@"goods_id"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 删除C2M视频信息
+ (void)queryDeleteVideoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/deleteVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  C2M视频详情
+ (void)queryMyVideoInfoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myVideoInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}




@end
