//
//  YXVideoModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@class YXLocationModel,YXBrandModel,YXTopBrandInfoModel,YXOwnerInfoModel,YXGoodsInfoModel;
@interface YXVideoModel : DDBaseModel

@property (nonatomic, copy) NSString *v_id;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *top_category;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *v_name;
@property (nonatomic, copy) NSString *v_introduce;
@property (nonatomic, copy) NSString *v_cover_img;
@property (nonatomic, copy) NSString *v_videoroute;
@property (nonatomic, copy) NSString *v_p_id;
@property (nonatomic, copy) NSString *v_delete;
@property (nonatomic, copy) NSString *v_time;
@property (nonatomic, copy) NSString *v_m_id;
@property (nonatomic, copy) NSString *v_updatetime;
@property (nonatomic, copy) NSString *reply_count;
@property (nonatomic, copy) NSString *collect_count;
@property (nonatomic, copy) NSString *view_count;
@property (nonatomic, copy) NSString *statusTitle;
@property (nonatomic, copy) NSString *fans_count;
@property (nonatomic, copy) NSString *like_count;
@property (nonatomic, copy) NSString *follow_count;
@property (nonatomic, assign) BOOL is_fans;
@property (nonatomic, assign) BOOL is_like;
@property (nonatomic, assign) BOOL is_collect;

@property (nonatomic, strong) YXLocationModel *v_location;
@property (nonatomic, strong) YXBrandModel *brand;
@property (nonatomic, strong) YXTopBrandInfoModel *topBrandInfo;
@property (nonatomic, strong) YXOwnerInfoModel *ownerInfo;
@property (nonatomic, strong) YXGoodsInfoModel *goodsInfo;


@end


@interface YXLocationModel : DDBaseModel
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *address;
@end


@interface YXBrandModel : DDBaseModel
@property (nonatomic, copy) NSString *brand_id;
@property (nonatomic, copy) NSString *brand_pid;
@property (nonatomic, copy) NSString *brand_name;
@property (nonatomic, copy) NSString *brand_picpath;
@property (nonatomic, strong) NSArray *child;
@property (nonatomic, assign) BOOL selected;

@end

@interface YXTopBrandInfoModel : DDBaseModel
@property (nonatomic, copy) NSString *brand_id;
@property (nonatomic, copy) NSString *brand_pid;
@property (nonatomic, copy) NSString *brand_name;
@property (nonatomic, copy) NSString *brand_picpath;

@end

@interface YXOwnerInfoModel : DDBaseModel
@property (nonatomic, copy) NSString *role_video;
@property (nonatomic, copy) NSString *roleName;
@property (nonatomic, copy) NSString *fans_count;
@property (nonatomic, copy) NSString *wechat_name;
@property (nonatomic, copy) NSString *show_video_id;
@property (nonatomic, copy) NSString *wechat_headimgurl;
@property (nonatomic, copy) NSString *nike;
@property (nonatomic, copy) NSString *role_video_pass;

@end

@interface YXGoodsInfoModel : DDBaseModel
@property (nonatomic, copy) NSString *sales_price;
@property (nonatomic, copy) NSString *goods_m_id;
@property (nonatomic, copy) NSString *collect_count;
@property (nonatomic, copy) NSString *user_id;
@property (nonatomic, copy) NSString *stock;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *view_count;
@property (nonatomic, copy) NSString *is_delete;
@property (nonatomic, copy) NSString *goods_id;
@property (nonatomic, copy) NSString *create_time;
@property (nonatomic, copy) NSString *reply_count;
@property (nonatomic, copy) NSString *update_time;
@property (nonatomic, strong) NSArray *banner;

@end


NS_ASSUME_NONNULL_END
