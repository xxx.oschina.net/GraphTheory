//  Created by li qiao robot 
#import "DDBaseModel.h"
@interface YXCommentsModelList : DDBaseModel
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *is_off;
@property (copy,nonatomic)   NSString *video_id;
@property (copy,nonatomic)   NSString *m_id;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *user_face;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *user_name;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *update_time;
@property (copy,nonatomic)   NSString *content;
@end

@interface YXCommentsModel : DDBaseModel
@property (strong,nonatomic) NSNumber *page;
@property (strong,nonatomic) NSNumber *limit;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSNumber *pageCount;
@end

