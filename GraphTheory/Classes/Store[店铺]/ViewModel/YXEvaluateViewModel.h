//
//  YXEvaluateViewModel.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

@interface YXEvaluateViewModel : DDBaseViewModel




/**
 根据店铺ids查询全部评价数、好评数、中评数、差评数

 @param shopIds     店铺ids
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryShopEvaluateCountByProductId:(NSString *)shopIds
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError;


/**
 评价列表

 @param type        类型： 0.全部 1.好评 2.中评 3.差评
 @param shopIds     店铺Id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryEvaluateAllByType:(NSString *)type
                       ShopIds:(NSString *)shopIds
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;



/**
 商家回复用户评价

 @param evaluateId      评价id
 @param replyContent    回复内容
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestAddReplyWithEvaluateId:(NSString *)evaluateId
                         ReplyContent:(NSString *)replyContent
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;


@end

