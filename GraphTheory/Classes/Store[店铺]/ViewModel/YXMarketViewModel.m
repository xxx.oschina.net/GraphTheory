    //
    //  YXMarketViewModel.m
    //  BusinessFine
    //
    //  Created by 杨旭 on 2019/12/3.
    //  Copyright © 2019年 杨旭. All rights reserved.
    //

#import "YXMarketViewModel.h"
#import "YXCouponsModel.h"
@implementation YXMarketViewModel

#pragma mark - 营销活动报名/新增优惠券
+ (void)saveMarketActivityOrPreferentialWithModel:(YXCouponsModel *)couponsModel
                                       Completion:(void (^)(id responesObj))completion
                                          failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Activity/saveMarketActivityOrPreferential",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[couponsModel mj_JSONObject]];
    
    [NetWorkTools postWithUrl:urlStr body:[params mj_JSONData] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 查询优惠券类型下拉选
+ (void)querySelectCouponTypeCompletion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError * error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/selectCouponType",kBaseURL];
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  app查询优惠券列表
+ (void)queryCouponDataWithPageNum:(NSString *)pageNum
                        radioState:(NSString *)radioState
                  preferentialType:(NSString *)preferentialType
                        Completion:(void (^)(id responesObj))completion
                           failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/appQueryCouponData",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:radioState forKey:@"radioState"];
    [params setValue:preferentialType forKey:@"preferentialType"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  查询优惠券订单金额合计
+ (void)queryMarketPreferentialById:(NSString *)preferentialId
                         Completion:(void (^)(id responesObj))completion
                            failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/market/queryMarketPreferential",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:preferentialId forKey:@"preferentialId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  app优惠券查询领取信息/使用信息
+ (void)queryCouponPeopleOrUseWithType:(NSString *)type
                               PageNum:(NSString *)pageNum
                        PreferentialId:(NSString *)preferentialId
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr;
    if ([type isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/appQueryCouponPeople",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/appQueryPreferentialUse",kBaseURL];
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:preferentialId forKey:@"preferentialId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark - 查询商品优惠券适用商品列表
+ (void)queryItemSpuByShopIdWithPageNum:(NSString *)pageNum
                                 ShopId:(NSString *)shopId
                         PreferentialId:(NSString *)preferentialId
                             Completion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/ItemSpu/queryItemSpuByShopId",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:shopId forKey:@"shopId"];
    [params setValue:preferentialId forKey:@"preferentialId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 查询下过订单人员（发放优惠券使用）
+ (void)queryOrderPeopleWithPageNum:(NSString *)pageNum
                           CouponId:(NSString *)couponId
                         Completion:(void (^)(id responesObj))completion
                            failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/queryOrderPeople",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:couponId forKey:@"couponId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark - 商家手动发放优惠券
+ (void)querySaveUserCouponWithCouponId:(NSString *)couponId
                               PeopleId:(NSString *)peopleId
                             Completion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Coupon/saveUserCoupon",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:couponId forKey:@"couponId"];
    [params setValue:peopleId forKey:@"peopleId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}




#pragma mark - app优惠券增加库存
+ (void)requestUndateCouponStockWithCouponId:(NSString *)couponId
                                       Stock:(NSString *)stock
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/undateCouponStock",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:couponId forKey:@"couponId"];
    [params setValue:stock forKey:@"stock"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark - 停止优惠券发放
+ (void)requestUpdateCouponStateWithCouponId:(NSString *)couponId
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Preferential/updateCouponState",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:couponId forKey:@"couponId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 查询限时抢购获取可报名日期/报名场次
+ (void)queryActivityDateOrSceneWityType:(NSString *)type
                             Completion:(void (^)(id responesObj))completion
                                 failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr;
    if ([type isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@market/market/Activity/queryActivityDate",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@market//market/Activity/queryActivityScene",kBaseURL];
    }
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}



#pragma mark - 获取店铺spu商品
+ (void)queryitemSpuWithPageNum:(NSString *)pageNum
                           Type:(NSString *)type
                         SpuIds:(NSString *)spuIds
                     CategoryId:(NSString *)categoryId
                        GroupId:(NSString *)groupId
                  NameOrpageNum:(NSString *)nameOrpageNum
                      StartDate:(NSString *)startDate
                        EndDate:(NSString *)endDate
                   ActivityType:(NSString *)activityType
                          Scene:(NSString *)scene
                     GroupingId:(NSString *)groupingId
                  InventorySort:(NSString *)inventorySort
                      SalesSort:(NSString *)salesSort
                      PriceSort:(NSString *)priceSort
                     Completion:(void (^)(id responesObj))completion
                        failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([type isEqualToString:@"2"]) {
        urlStr = [NSString stringWithFormat:@"%@market/market/home/queryHomeitemSpuApp",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@market/market/Activity/queryitemSpuApp",kBaseURL];
    }
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:spuIds forKey:@"spuIds"];
    [params setValue:categoryId forKey:@"categoryId"];
    [params setValue:groupId forKey:@"groupId"];
    [params setValue:nameOrpageNum forKey:@"nameOrpageNum"];
    [params setValue:startDate forKey:@"startDate"];
    [params setValue:endDate forKey:@"endDate"];
    [params setValue:activityType forKey:@"activityType"];
    [params setValue:scene forKey:@"scene"];
    [params setValue:groupingId forKey:@"groupingId"];
    [params setValue:inventorySort forKey:@"inventorySort"];
    [params setValue:salesSort forKey:@"salesSort"];
    [params setValue:priceSort forKey:@"priceSort"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  拼团/限时抢购/自定义活动 查询已报名的活动
+ (void)queryActivityWithPageNum:(NSString *)pageNum
                            type:(NSString *)type
                    activityName:(NSString *)activityName
                      Completion:(void (^)(id responesObj))completion
                         failure:(void (^)(NSError *error))conError {
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([type isEqualToString:@"6"]) {
        urlStr = [NSString stringWithFormat:@"%@market/market/home/enrolmentItemData",kBaseURL];
        [params setValue:@"0" forKey:@"entrytype"];
        [params setValue:@"0" forKey:@"auditState"];
    }else {
        urlStr = [NSString stringWithFormat:@"%@market/market/Activity/queryActivity",kBaseURL];
        [params setValue:type forKey:@"type"];
        [params setValue:activityName forKey:@"activityName"];
    }
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


    ///market/Activity/queryActivitySpu
+ (void)marketActivityQueryActivitySpuWithPageNum:(NSString *)pageNum
                                         pageSize:(NSString *)pageSize
                                             type:(NSString *)type
                                       activityId:(NSString *)activityId
                                       auditState:(NSString *)auditState
                                        sortOrder:(NSString *)sortOrder
                                        sortName:(NSString *)sortName
                                       Completion:(void (^)(id responesObj))completion
                                          failure:(void (^)(NSError *error))conError{
    
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/Activity/queryActivitySpu",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:type forKey:@"type"];
    [params setValue:activityId forKey:@"activityId"];
    [params setValue:auditState forKey:@"auditState"];
    [params setValue:sortOrder forKey:@"sortOrder"];
    [params setValue:sortName forKey:@"sortName"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)deleteitemSpuWithSpuIds:(NSString *)spuIds Completion:(void (^)(id))completion failure:(void (^)(NSError *))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/Activity/deleteitemSpu",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:spuIds forKey:@"spuIds"];
        [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 查询商家自定义活动接口
+ (void)queryCustomActivityWithPageNum:(NSString *)pageNum
                              PageSize:(NSString *)pageSize
                         ActivityName:(NSString *)activityName
                           Completion:(void (^)(id responesObj))completion
                              failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/Type/queryCustomActivity",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:activityName forKey:@"activityName"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 查询城市中订购App的项目所在城市下拉选
+ (void)queryHouseCityByAppTypeCompletion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/home/queryHouseCityByAppType",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 查询城市中订购App的项目
+ (void)queryAppTypeHouseByCityWithShiCode:(NSString *)shiCode
                                 HouseName:(NSString *)houseName
                                Completion:(void (^)(id responesObj))completion
                                   failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/home/queryAppTypeHouseByCity",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:shiCode forKey:@"shiCode"];
    [params setValue:houseName forKey:@"houseName"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 商家报名查询分组下拉选
+ (void)queryItemsGroupSelectWithHouseId:(NSString *)houseId
                              Completion:(void (^)(id responesObj))completion
                                 failure:(void (^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@items/items/home/queryItemsGroupSelect",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:houseId forKey:@"houseId"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - (新)首页推荐商家报名接口
+ (void)saveMarketHomeSignUpsWithModel:(id)model
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@market/market/home/saveMarketHomeSignUps",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:[model mj_JSONObject]];
    
    [NetWorkTools postWithUrl:urlStr body:[params mj_JSONData] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 查询已报名首页推荐商品信息
+ (void)queryHomeItembyIdWithPageNum:(NSString *)pageNum
                            PageSize:(NSString *)pageSize
                     HomeRecommendId:(NSString *)homeRecommendId
                           PriceSort:(NSString *)priceSort
                       InventorySort:(NSString *)inventorySort
                           SalesSort:(NSString *)salesSort
                          Completion:(void (^)(id responesObj))completion
                             failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/home/queryHomeItembyId",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:homeRecommendId forKey:@"homeRecommendId"];
    [params setValue:priceSort forKey:@"priceSort"];
    [params setValue:inventorySort forKey:@"inventorySort"];
    [params setValue:salesSort forKey:@"salesSort"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 分组审核通过添加商品
+ (void)addHomeItemSpuWithHomeRecommendId:(NSString *)homeRecommendId
                                 ItemData:(NSString *)itemData
                               Completion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/itemSpu/addHomeItemSpu",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:homeRecommendId forKey:@"homeRecommendId"];
    [params setValue:itemData forKey:@"itemData"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 移除审核通过商品（商家）
+ (void)delItemsSpuWithHomeRecommendId:(NSString *)homeRecommendId
                                 SpuId:(NSString *)spuId
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/home/delItemsSpu",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:homeRecommendId forKey:@"homeRecommendId"];
    [params setValue:spuId forKey:@"spuId"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  App查询限时抢购活动
+ (void)queryAppFlashSaleActivityWithPageNum:(NSString *)pageNum
                                       state:(NSString *)state
                                    itemName:(NSString *)itemName
                                   startDate:(NSString *)startDate
                                     endDate:(NSString *)endDate
                                       scene:(NSString *)scene
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/newActivity/queryAppFlashSaleActivity",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:state forKey:@"state"];
    [params setValue:itemName forKey:@"itemName"];
    [params setValue:startDate forKey:@"startDate"];
    [params setValue:endDate forKey:@"endDate"];
    [params setValue:scene forKey:@"scene"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 查询app已报名的商品spu信息
+ (void)queryAppNewActivitySpuWithPageNum:(NSString *)pageNum
                                     Type:(NSString *)type
                              ActivatyIds:(NSString *)activatyIds
                                    State:(NSString *)state
                               AuditState:(NSString *)auditState
                                 ShopName:(NSString *)shopName
                                 ItemName:(NSString *)itemName
                                 MinPrice:(NSString *)minPrice
                                 MaxPrice:(NSString *)maxPrice
                                 MinMoney:(NSString *)minMoney
                                 MaxMoney:(NSString *)maxMoney
                                StartDate:(NSString *)startDate
                                  EndDate:(NSString *)endDate
                                SortOrder:(NSString *)sortOrder
                                 SortName:(NSString *)sortName
                               Completion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/newActivity/queryAppNewActivitySpu",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:type forKey:@"type"];
    [params setValue:activatyIds forKey:@"activatyIds"];
    [params setValue:shopName forKey:@"shopName"];
    [params setValue:auditState forKey:@"auditState"];
    [params setValue:state forKey:@"state"];
    [params setValue:itemName forKey:@"itemName"];
    [params setValue:minPrice forKey:@"minPrice"];
    [params setValue:maxPrice forKey:@"maxPrice"];
    [params setValue:minMoney forKey:@"minMoney"];
    [params setValue:maxMoney forKey:@"maxMoney"];
    [params setValue:startDate forKey:@"startDate"];
    [params setValue:endDate forKey:@"endDate"];
    [params setValue:sortOrder forKey:@"sortOrder"];
    [params setValue:sortName forKey:@"sortName"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 查询拼团分析
+ (void)queryCollageAnalysisWithPageNum:(NSString *)pageNum
                                  State:(NSString *)state
                                 ItemId:(NSString *)itemId
                             ActivatyId:(NSString *)activatyId
                             Completion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/market/market/newActivity/queryCollageAnalysis",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"10" forKey:@"pageSize"];
    [params setValue:state forKey:@"state"];
    [params setValue:itemId forKey:@"itemId"];
    [params setValue:activatyId forKey:@"activatyId"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

@end
