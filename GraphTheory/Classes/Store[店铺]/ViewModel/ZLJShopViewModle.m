//
//  ZLJShopViewModle.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJShopViewModle.h"
#import "WGAddGoodsModel.h"

@implementation ZLJShopViewModle


+ (void)zljQueryShopByRelationId:(NSString *)relationId 
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/storeArea/queryShopByRelationId",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:relationId forKey:@"relationId"];
   
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



+ (void)zljFindItemsGroupByShopId:(NSString *)shopId 
                       Completion:(void(^)(id responesObj))completion
                          Failure:(void(^)(NSError *error))conError{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/items/items/itemsgroup/syjAppQueryItemGroups",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:shopId forKey:@"shopId"];
    
    [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

+ (void)zljQueryItemsManageListPaygeNum:(NSString *)pageNum 
                               pageSize:(NSString *)pageSize
                              itemGroup:(NSString *)itemGroup
                         itemCategoryId:(NSString *)itemCategoryId
                             itemSource:(NSString *)itemSource
                                 shopId:(NSString *)shopId
                           itemActivity:(NSString *)itemActivity
                              itemState:(NSString *)itemState
                         searchCriteria:(NSString *)searchCriteria
                             searchType:(NSString *)searchType
                               salesMin:(NSString *)salesMin
                              priceSort:(NSString *)priceSort
                              stockSort:(NSString *)stockSort
                              salesSort:(NSString *)salesSort
                             Completion:(void(^)(id responesObj))completion
                                Failure:(void(^)(NSError *error))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@/items/items/manage/queryItemsManageList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:itemGroup forKey:@"itemGroup"];
    [params setValue:itemCategoryId forKey:@"itemCategoryId"];
    [params setValue:itemSource forKey:@"itemSource"];
    [params setValue:shopId forKey:@"shopId"];
    [params setValue:itemActivity forKey:@"itemActivity"];
    [params setValue:itemState forKey:@"itemState"];
    [params setValue:searchCriteria forKey:@"likeSearchCriteria"];
    [params setValue:searchType forKey:@"searchType"];
    [params setValue:salesMin forKey:@"salesMin"];
    [params setValue:priceSort forKey:@"priceSort"];
    [params setValue:stockSort forKey:@"stockSort"];
    [params setValue:salesSort forKey:@"salesSort"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [YJProgressHUD showLoading:@"加载中..."];
    });
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [YJProgressHUD hideHUD];
        });
        
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [YJProgressHUD hideHUD];

        });
        if (conError) {
            conError(error);
        }
    }];   
}
+ (void)wgChangeGoodGroupGroupId:(NSString *)groupId groupName:(NSString *)groupName sort:(NSString *)sort spuIds:(NSString *)spuIds Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr;
    if (groupId.length) {
        urlStr = [NSString stringWithFormat:@"%@items/items/itemsgroup/updateItems",kBaseURL];
    }else{
        urlStr = [NSString stringWithFormat:@"%@items/items/itemsgroup/insertItems",kBaseURL];
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:groupId forKey:@"id"];
    [params setValue:groupName forKey:@"groupName"];
    [params setValue:spuIds?:@"" forKey:@"spuIds"];
    [params setValue:sort forKey:@"sort"];

    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)wgQueryGroupItmeGroupId:(NSString *)groupId cateId:(NSString *)cateId keyWord:(NSString *)keyWord pageNum:(nonnull NSString *)pageNum pageSize:(nonnull NSString *)pageSize outGroup:(nonnull NSString *)outGroup Completion:(nonnull void (^)(id _Nonnull))completion Failure:(nonnull void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/items/itemsgroup/syjAppQueryItems",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:groupId forKey:@"groupId"];
    [params setValue:cateId forKey:@"cateId"];
    [params setValue:keyWord forKey:@"keyWord"];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:outGroup forKey:@"groupType"];

    [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)wgDelectGroupGroupId:(NSString *)groupId Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/items/itemsgroup/deleteItemsGroup",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:groupId forKey:@"id"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)querySpecGroupId:(NSString *)groupId Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/items/spec/selectSpec",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:groupId forKey:@"id"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)wgAddGoodsShopId:(NSString *)shopId goods:(WGAddGoodsModel *)goods Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/addItems/addItem",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:groupId forKey:@"id"];
    if (goods.spuId) {
        urlStr = [NSString stringWithFormat:@"%@items/addItems/updateItem",kBaseURL];
    }
    NSLog(@"%@", [YXUserInfoManager getUserInfo].token);
    [NetWorkTools postWithUrl:urlStr body:[[goods mj_JSONString] dataUsingEncoding:NSUTF8StringEncoding] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

+(void)zljUpdateItemsStateWithSpuid:(NSArray <ItemsManageItemList *>*)spuidModel operType:(NSString *)operType Completion:(void(^)(id responesObj))completion Failure:(void(^)(NSError *error))conError{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/items/items/manage/updateItemsState",kBaseURL];
    NSMutableArray *paramArray = [NSMutableArray array];
    for (ItemsManageItemList *itemList in spuidModel) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:itemList.spuId forKey:@"spuId"];
        NSString    *operTypeString ;
        if ([operType isEqualToString:@"下架"]) {
            operTypeString = @"0";
        }else if ([operType isEqualToString:@"上架"]){
             operTypeString = @"1";
        }else if ([operType isEqualToString:@"删除"]){
             operTypeString = @"3";
        }else if ([operType isEqualToString:@"恢复"]){//pc
             operTypeString = @"0";
        }else if ([operType isEqualToString:@"彻底删除"]){//pc
             operTypeString = @"5";
        }
        
        [params setValue:operTypeString forKey:@"operType"];
        [paramArray addObject:params];
    }
    NSLog(@"zlj--当前输出：%s\n对商品的操作：%@",__FUNCTION__,operType);
    [NetWorkTools postWithUrl:urlStr body:[paramArray mj_JSONData] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)queryItemsBySpuIdShopId:(NSString *)shopId spuId:(NSString *)spuId Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/addItems/queryItemsBySpuId",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:spuId forKey:@"spuId"];
    
    [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)queryOrderNumberShopId:(NSString *)shopId Completion:(void (^)(id _Nonnull))completion Failure:(void (^)(NSError * _Nonnull))conError{
//    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/bussiness/queryOrderNumber",kBaseURL];
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/bussiness/queryCurrentStoreData",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:shopId forKey:@"shopId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


+ (void)queryitemDetailId:(NSString *)goodsId
                   UserId:(NSString *)userId
               Completion:(void(^)(id responesObj))completion
                  Failure:(void(^)(NSError *error))conError {
    
        NSString *urlStr = [NSString stringWithFormat:@"%@itemselect/items/select/v2/itemDetail",kBaseURL];
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setValue:goodsId forKey:@"itemId"];
        [params setValue:userId forKey:@"userId"];

        [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
            if (completion) {
                completion(responseObj);
            }
        } failure:^(NSError *error) {
            if (conError) {
                conError(error);
            }
        }];
    
}

@end
