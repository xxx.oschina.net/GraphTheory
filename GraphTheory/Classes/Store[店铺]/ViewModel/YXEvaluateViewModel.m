
//
//  YXEvaluateViewModel.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEvaluateViewModel.h"

@implementation YXEvaluateViewModel


#pragma mark - 根据店铺ids查询全部评价数、好评数、中评数、差评数
+ (void)queryShopEvaluateCountByProductId:(NSString *)shopIds
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@bussiness/evaluate/queryShopEvaluateCountByProductId.dd",kBaseURL];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:shopIds == nil ? @"" : shopIds forKey:@"shopIds"];
    
    [NetWorkTools postWithUrl:urlStr parameters:param success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 评价列表
+ (void)queryEvaluateAllByType:(NSString *)type
                       ShopIds:(NSString *)shopIds
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@bussiness/evaluate/queryEvaluateMessageByType.dd",kBaseURL];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:type forKey:@"type"];
    [param setValue:shopIds == nil ? @"" : shopIds forKey:@"shopIds"];

    [NetWorkTools postWithUrl:urlStr parameters:param success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 商家回复用户评价
+ (void)requestAddReplyWithEvaluateId:(NSString *)evaluateId
                         ReplyContent:(NSString *)replyContent
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@community/evaluate/evaAppEvaluate/addReply.dd",kBaseURL];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:evaluateId forKey:@"evaluateId"];
    [param setValue:replyContent forKey:@"replyContent"];
    
    [NetWorkTools postWithUrl:urlStr parameters:param success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

@end
