//
//  YXMarketViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/3.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
@class YXCouponsModel;
@interface YXMarketViewModel : DDBaseModel



/**
 营销活动报名/新增优惠券

 @param couponsModel    优惠券信息
 @param completion      返回信息
 @param conError        返回失败
 */
+ (void)saveMarketActivityOrPreferentialWithModel:(YXCouponsModel *)couponsModel
                                       Completion:(void (^)(id responesObj))completion
                                          failure:(void (^)(NSError *error))conError;

/**
 查询优惠券类型下拉选

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)querySelectCouponTypeCompletion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError;


/**
 app查询优惠券列表

 @param pageNum             当前页码
 @param radioState          优惠券活动状态 1:未开始、0：进行中、2：已结束
 @param preferentialType    优惠券类别 0:全部优惠券 1:店铺优惠券;2:商品优惠券;3:平台优惠券
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryCouponDataWithPageNum:(NSString *)pageNum
                        radioState:(NSString *)radioState
                  preferentialType:(NSString *)preferentialType
                        Completion:(void (^)(id responesObj))completion
                           failure:(void (^)(NSError *error))conError;



/**
 查询优惠券订单金额合计

 @param preferentialId  优惠券Id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryMarketPreferentialById:(NSString *)preferentialId
                         Completion:(void (^)(id responesObj))completion
                            failure:(void (^)(NSError *error))conError;



/**
 app优惠券查询领取信息/使用信息

 @param type                类型 0:领取信息 1：使用信息
 @param pageNum             当前页码
 @param preferentialId      优惠券Id
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryCouponPeopleOrUseWithType:(NSString *)type
                               PageNum:(NSString *)pageNum
                        PreferentialId:(NSString *)preferentialId
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError;


/**
 查询商品优惠券适用商品列表

 @param pageNum             分页数
 @param shopId              店铺ID
 @param preferentialId      优惠券id
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryItemSpuByShopIdWithPageNum:(NSString *)pageNum
                                 ShopId:(NSString *)shopId
                         PreferentialId:(NSString *)preferentialId
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError;



/**
 查询下过订单人员（发放优惠券使用）

 @param pageNum         当前页码
 @param couponId        优惠券id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryOrderPeopleWithPageNum:(NSString *)pageNum
                           CouponId:(NSString *)couponId
                         Completion:(void (^)(id responesObj))completion
                            failure:(void (^)(NSError *error))conError;


/**
 商家手动发放优惠券
 
 @param couponId        优惠券ID
 @param peopleId        优惠券发放用户id（多个逗号隔开）
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)querySaveUserCouponWithCouponId:(NSString *)couponId
                               PeopleId:(NSString *)peopleId
                             Completion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError;


/**
 app优惠券增加库存

 @param couponId        优惠券id
 @param stock           增加库存数量
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUndateCouponStockWithCouponId:(NSString *)couponId
                                       Stock:(NSString *)stock
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError;




/**
 停止优惠券发放

 @param couponId        优惠券ID
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateCouponStateWithCouponId:(NSString *)couponId
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError;



/**
 查询限时抢购获取可报名日期/报名场次

 @param type            类型 0: 报名日期 1: 报名场次
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryActivityDateOrSceneWityType:(NSString *)type
                             Completion:(void (^)(id responesObj))completion
                                 failure:(void (^)(NSError *error))conError;





/**
 获取店铺spu商品

 @param pageNum         分页数
 @param type            接口类型
 @param spuIds          已选择商品spuId（多个逗号隔开）
 @param categoryId      商品类别id
 @param groupId         商品分组id
 @param nameOrpageNum   商品名称或者编号
 @param startDate       活动开始时间(限时抢购传一个活动时间)
 @param endDate         活动结束时间
 @param activityType    报名类别：1:优惠劵;2:新人专享;3:限时抢购;4:拼团;5:自定义活动;
 @param scene           场次时间
 @param groupingId      首页推荐分组id
 @param inventorySort   库存排序（0是升序 1是降序 ）
 @param salesSort       销量排序（0是升序 1是降序 ）
 @param priceSort       价格排序（0是升序 1是降序 ）
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryitemSpuWithPageNum:(NSString *)pageNum
                           Type:(NSString *)type
                         SpuIds:(NSString *)spuIds
                     CategoryId:(NSString *)categoryId
                        GroupId:(NSString *)groupId
                  NameOrpageNum:(NSString *)nameOrpageNum
                      StartDate:(NSString *)startDate
                        EndDate:(NSString *)endDate
                   ActivityType:(NSString *)activityType
                          Scene:(NSString *)scene
                     GroupingId:(NSString *)groupingId
                  InventorySort:(NSString *)inventorySort
                      SalesSort:(NSString *)salesSort
                      PriceSort:(NSString *)priceSort
                     Completion:(void (^)(id responesObj))completion
                        failure:(void (^)(NSError *error))conError;




/**
 拼团/限时抢购/自定义活动 查询已报名的活动

 @param pageNum             分页数
 @param type                活动类型 3限时抢购；4拼团；5自定义活动 6:首页推荐
 @param activityName        营销活动名称
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryActivityWithPageNum:(NSString *)pageNum
                            type:(NSString *)type
                    activityName:(NSString *)activityName
                      Completion:(void (^)(id responesObj))completion
                         failure:(void (^)(NSError *error))conError;


    /// market/Activity/queryActivitySpu
    /// @param pageNum 当前页码
    /// @param pageSize 每页的条数
    /// @param type 活动类型 2新人专享；3限时抢购； 4拼团；5自定义活动
    /// @param activityId 限时抢购/拼团/自定义活动 的活动Id
    /// @param auditState 审核状态0全部 1待审核 2通过 3拒绝
    /// @param sortOrder asc-升序    desc-降序
    /// @param sortName  0:j库存  1:浏览量。 2:销量。  3:订单金额
    /// @param completion 返回成功
    /// @param conError 返回失败
+ (void)marketActivityQueryActivitySpuWithPageNum:(NSString *)pageNum
                                         pageSize:(NSString *)pageSize
                                             type:(NSString *)type
                                     activityId:(NSString *)activityId
                                       auditState:(NSString *)auditState
                                        sortOrder:(NSString *)sortOrder
                                         sortName:(NSString *)sortName
                                       Completion:(void (^)(id responesObj))completion
                                          failure:(void (^)(NSError *error))conError;

/**
 删除活动商品

 @param spuIds 报名商品的主键id，多个以逗号隔开
 @param completion 返回成功
 @param conError 返回失败
 */
+ (void)deleteitemSpuWithSpuIds:(NSString *)spuIds
                     Completion:(void (^)(id responesObj))completion
                        failure:(void (^)(NSError *error))conError;



/**
 查询商家自定义活动接口

 @param pageNum             当前页码
 @param pageSize            当前分页个数
 @param activityName        自定义活动名称
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryCustomActivityWithPageNum:(NSString *)pageNum
                              PageSize:(NSString *)pageSize
                         ActivityName:(NSString *)activityName
                           Completion:(void (^)(id responesObj))completion
                              failure:(void (^)(NSError *error))conError;


/**
查询城市中订购App的项目所在城市下拉选
 
@param completion          返回成功
@param conError            返回失败
*/
+ (void)queryHouseCityByAppTypeCompletion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError;



/**
查询城市中订购App的项目

@param shiCode             城市编码
@param houseName           项目名称
@param completion          返回成功
@param conError            返回失败
*/
+ (void)queryAppTypeHouseByCityWithShiCode:(NSString *)shiCode
                                 HouseName:(NSString *)houseName
                                Completion:(void (^)(id responesObj))completion
                                   failure:(void (^)(NSError *error))conError;



/**
商家报名查询分组下拉选

@param houseId             项目id,项目id不传，默认查询全国分组
@param completion          返回成功
@param conError            返回失败
*/
+ (void)queryItemsGroupSelectWithHouseId:(NSString *)houseId
                              Completion:(void (^)(id responesObj))completion
                                 failure:(void (^)(NSError *error))conError;


/**
(新)首页推荐商家报名接口

@param model               首页推荐报名信息
@param completion          返回成功
@param conError            返回失败
*/
+ (void)saveMarketHomeSignUpsWithModel:(id)model
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError;


/**
查询已报名首页推荐商品信息

@param pageNum             当前页码
@param pageSize            每页的条数
@param homeRecommendId     报名表id
@param priceSort           价格排序（0是升序 1是降序 ）
@param inventorySort       库存排序（0是升序 1是降序 ）
@param salesSort           销量排序（0是升序 1是降序 ）
@param completion          返回成功
@param conError            返回失败
*/
+ (void)queryHomeItembyIdWithPageNum:(NSString *)pageNum
                            PageSize:(NSString *)pageSize
                     HomeRecommendId:(NSString *)homeRecommendId
                           PriceSort:(NSString *)priceSort
                       InventorySort:(NSString *)inventorySort
                           SalesSort:(NSString *)salesSort
                          Completion:(void (^)(id responesObj))completion
                             failure:(void (^)(NSError *error))conError;


/**
分组审核通过添加商品

@param homeRecommendId     报名表id
@param itemData            商品数据json集合
@param completion          返回成功
@param conError            返回失败
*/
+ (void)addHomeItemSpuWithHomeRecommendId:(NSString *)homeRecommendId
                                 ItemData:(NSString *)itemData
                               Completion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError;


/**
移除审核通过商品（商家）

@param homeRecommendId     报名表id
@param spuId               商品spuId
@param completion          返回成功
@param conError            返回失败
*/
+ (void)delItemsSpuWithHomeRecommendId:(NSString *)homeRecommendId
                                 SpuId:(NSString *)spuId
                            Completion:(void (^)(id responesObj))completion
                               failure:(void (^)(NSError *error))conError;


/**
 App查询限时抢购活动
 
 @param pageNum             当前页码
 @param state               状态：0:全部 1:未开始 2:进行中 3:已结束
 @param itemName            商品名称
 @param startDate           开始时间
 @param endDate             结束时间
 @param scene               场次
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryAppFlashSaleActivityWithPageNum:(NSString *)pageNum
                                       state:(NSString *)state
                                    itemName:(NSString *)itemName
                                   startDate:(NSString *)startDate
                                     endDate:(NSString *)endDate
                                       scene:(NSString *)scene
                                  Completion:(void (^)(id responesObj))completion
                                     failure:(void (^)(NSError *error))conError;


/**
 查询app已报名的商品spu信息
 
 @param pageNum             当前页码
 @param type                活动类型 2新人专享；3限时抢购； 4拼团；5自定义活动
 @param activatyIds         活动Id,多个用,隔开
 @param state               状态：0:全部 1:待审核 2:审核拒绝 3:未开始 4:进行中 5:已结束
 @param auditState          新人专享 审核状态：0:全部 1:待审核 2:通过 3:拒绝
 @param shopName            店铺名称
 @param itemName            商品名称
 @param minPrice            最小活动价
 @param maxPrice            最大活动价
 @param minMoney            最小订单金额
 @param maxMoney            最大订单金额
 @param startDate           开始时间
 @param endDate             结束时间
 @param sortOrder           排序规则（asc是升序 desc是降序 ）
 @param sortName            排序字段（0是库存 1是浏览量 2是销量 3是订单金额 ）
 @param completion          返回成功
 @param conError            返回失败
 */
+ (void)queryAppNewActivitySpuWithPageNum:(NSString *)pageNum
                                     Type:(NSString *)type
                              ActivatyIds:(NSString *)activatyIds
                                    State:(NSString *)state
                               AuditState:(NSString *)auditState
                                 ShopName:(NSString *)shopName
                                 ItemName:(NSString *)itemName
                                 MinPrice:(NSString *)minPrice
                                 MaxPrice:(NSString *)maxPrice
                                 MinMoney:(NSString *)minMoney
                                 MaxMoney:(NSString *)maxMoney
                                StartDate:(NSString *)startDate
                                  EndDate:(NSString *)endDate
                                SortOrder:(NSString *)sortOrder
                                 SortName:(NSString *)sortName
                               Completion:(void (^)(id responesObj))completion
                                  failure:(void (^)(NSError *error))conError;


/**
查询拼团分析

@param pageNum              当前页码
@param state                状态：1:待成团 2:已成团 3:拼团失败
@param itemId               商品id
@param activatyId           活动Id
@param completion           返回成功
@param conError             返回失败
*/
+ (void)queryCollageAnalysisWithPageNum:(NSString *)pageNum
                                  State:(NSString *)state
                                 ItemId:(NSString *)itemId
                             ActivatyId:(NSString *)activatyId
                             Completion:(void (^)(id responesObj))completion
                                failure:(void (^)(NSError *error))conError;

@end


