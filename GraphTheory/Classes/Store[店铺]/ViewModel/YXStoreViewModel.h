//
//  YXStoreViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/30.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN
@class WGStoreModel;
@interface YXStoreViewModel : DDBaseViewModel



/**
 查询主营类目分类接口
 
 @param categoryId      分类id
 @param level           分类级别
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryMainCategoryWithCategoryId:(NSString *)categoryId
                                  Level:(NSString *)level
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;


/**
查询店铺信息 POST /shop/store/v2/shopDetailQuery

@param completion 成功
@param conError 失败
*/
+ (void)queryShopDetailQueryCompletion:(void(^)(id responesObj))completion
                               Failure:(void(^)(NSError *error))conError;



/**
 验证店铺名字重复
 
 @param shopName 店铺名
 @param shopId 店铺id
 @param completion 成功
 @param conError 失败
 */
+ (void)validShopName:(NSString *)shopName
               shopId:(NSString *)shopId
           Completion:(void(^)(id responesObj))completion
              failure:(void(^)(NSError *error))conError;

/**
 新增/修改店铺
 
 @param type            0:创建 1:修改
 @param storeModel      店铺信息
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestInsertShopWithType:(NSInteger )type
                       StoreModel:(WGStoreModel *)storeModel
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/// 新增店铺（新）
/// @param storeModel 店铺
/// @param completion 成功
/// @param conError 失败
+ (void)requestAddStoreModel:(WGStoreModel *)storeModel
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError;

/// 修改店铺
/// @param storeModel 店铺信息
/// @param completion 成功
/// @param conError 失败
+ (void)requestUpdateStoreModel:(WGStoreModel *)storeModel
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;


/// 根据店铺类型查询配送方式
/// @param shopType 店铺类型 1:线上商家 2:本地商家
/// @param isPlat  店铺级别 1:普通店铺 2平台级店铺
/// @param completion 成功
/// @param conError 失败
+ (void)queryDistByShopType:(NSInteger)shopType
                     IsPlat:(NSInteger)isPlat
                 Completion:(void(^)(id responesObj))completion
                    failure:(void(^)(NSError *error))conError;

/// 根据店铺类型查询主营类目
/// @param type 店铺类型 1:线上商家 2:本地商家
/// @param completion 成功
/// @param conError 失败
+ (void)queryCategoryByShopType:(NSInteger)type
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 获取所有的省和直辖市
 
 @param areaId 传areaId为'0',则查询所有的省/直辖市,传其他areaId则查询下级数据,如传河南的areaId,则查询的是河南下面所有的市,依次类推
 @param completion 成功
 @param conError 失败
 */
+ (void)getAreaDataAreaId:(NSString *)areaId
               Completion:(void(^)(id responesObj))completion
                  failure:(void(^)(NSError *error))conError;



/**
 查询店铺详情
 
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryShopDetailCompletion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

///**
// 查询店铺配送方式
// 
// @param shopId 店铺id
// @param completion 成功
// @param conError 失败
// */
//+ (void)queryShopDistributionTypeShopId:(NSString *)shopId
//                             Completion:(void(^)(id responesObj))completion
//                                failure:(void(^)(NSError *error))conError;


/**
版本更新

@param version         当前版本号
@param appId           BundleIderID
@param xtType          手机系统
@param completion      返回成功
@param conError        返回失败
*/
+ (void)checkVersionWithVersion:(NSString *)version
                          AppId:(NSString *)appId
                         xtType:(NSString *)xtType
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;


@end

NS_ASSUME_NONNULL_END
