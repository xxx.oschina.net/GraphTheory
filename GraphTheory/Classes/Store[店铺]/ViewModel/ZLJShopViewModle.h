//
//  ZLJShopViewModle.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"
#import "ZLJItemsManageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLJShopViewModle : DDBaseViewModel

/**
 根据商家id查询店铺 POST /shop/store/queryShopByRelationId
 
 @param relationId 商家id
 @param completion 成功
 @param conError 失败
 */
+ (void)zljQueryShopByRelationId:(NSString *)relationId 
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;







/**
 
 查询商品所有分组信息 GET /items/itemsgroup/findItemsGroup
 
 @param shopId 店铺id
 @param completion 成功
 @param conError 失败
 */
+ (void)zljFindItemsGroupByShopId:(NSString *)shopId 
                       Completion:(void(^)(id responesObj))completion
                          Failure:(void(^)(NSError *error))conError;

/**
 商品管理查询商品列表（以spu展示） /items/items/manage/queryItemsManageList
 
 @param pageNum 当前页码
 @param pageSize 每页的条数
 @param itemGroup 商品分组
 @param itemCategoryId 商品类别
 @param itemSource 商品来源
 @param shopId 店铺id
 @param itemActivity 商品活动
 @param itemState 商品状态  1：在售中  0：仓库中   2：已售尽    3：已删除
 @param searchCriteria 商品名称/商品编号 模糊搜索
 @param searchType 模糊搜索类型 1 商品名称 2 skuId
 @param salesMin 销量最小值
 @param completion 成功
 @param conError 失败
 */
+ (void)zljQueryItemsManageListPaygeNum:(NSString *)pageNum 
                               pageSize:(NSString *)pageSize
                              itemGroup:(NSString *)itemGroup
                         itemCategoryId:(NSString *)itemCategoryId
                             itemSource:(NSString *)itemSource
                                 shopId:(NSString *)shopId
                           itemActivity:(NSString *)itemActivity
                              itemState:(NSString *)itemState
                         searchCriteria:(NSString *)searchCriteria
                             searchType:(NSString *)searchType
                               salesMin:(NSString *)salesMin
                              priceSort:(NSString *)priceSort
                              stockSort:(NSString *)stockSort
                              salesSort:(NSString *)salesSort
                             Completion:(void(^)(id responesObj))completion
                                Failure:(void(^)(NSError *error))conError;

/**
 新增或修改商品分组
 
 @param groupId 分组id
 @param groupName 分组名
 @param sort 排序
 @param spuIds 商品集合
 @param completion 成功
 @param conError 失败
 */
+ (void)wgChangeGoodGroupGroupId:(NSString *)groupId
                       groupName:(NSString *)groupName
                            sort:(NSString *)sort
                          spuIds:(NSString *)spuIds
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;
/**
 请求分组商品

 @param groupId 分组id 不传返回所有商品
 @param cateId 商品编号
 @param keyWord 搜索关键字
 @param completion 成功
 @param conError 失败
 */

/**
 请求分组商品

 @param groupId 分组id 不传返回所有商品
 @param cateId 商品编号
 @param keyWord 搜索关键字
 @param pageNum 页码
 @param pageSize 页大小
 @param outGroup 不在分组
 @param completion 成功
 @param conError 失败
 */
+ (void)wgQueryGroupItmeGroupId:(NSString *)groupId
                         cateId:(NSString *)cateId
                        keyWord:(NSString *)keyWord
                       pageNum:(NSString *)pageNum
                       pageSize:(NSString *)pageSize
                       outGroup:(NSString *)outGroup
                     Completion:(void(^)(id responesObj))completion
                        Failure:(void(^)(NSError *error))conError;

/**
 删除分组

 @param groupId 分组id
 @param completion 成功
 @param conError 失败
 */
+ (void)wgDelectGroupGroupId:(NSString *)groupId
                     Completion:(void(^)(id responesObj))completion
                        Failure:(void(^)(NSError *error))conError;

/**
 查询店铺所有规格

 @param groupId id
 @param completion 成功
 @param conError 失败
 */
+ (void)querySpecGroupId:(NSString *)groupId
                  Completion:(void(^)(id responesObj))completion
                     Failure:(void(^)(NSError *error))conError;

/**
 增加商品

 @param shopId 店铺
 @param goods 商品
 @param completion 成功
 @param conError 失败
 */
+ (void)wgAddGoodsShopId:(NSString *)shopId
                   goods:(id)goods
              Completion:(void(^)(id responesObj))completion
                 Failure:(void(^)(NSError *error))conError;



    /// 修改商品上下架状态（以spu展示）
    /// @param spuidModel  多个 ItemsManageItemList 中  spuId(spu表的主键id
    /// @param operType 操作类型（0:下架1：上架3：删除 0：恢复5：彻底删除）
    /// @param completion 成功
    /// @param conError 失败
+(void)zljUpdateItemsStateWithSpuid:(NSArray <ItemsManageItemList *>*)spuidModel
                           operType:(NSString *)operType
                         Completion:(void(^)(id responesObj))completion
                            Failure:(void(^)(NSError *error))conError;
/**
 查询商品详情根据spuId（商品修改的回显）
 
 @param shopId 店铺
 @param spuId 商品spuId
 @param completion 成功
 @param conError 失败
 */

+ (void)queryItemsBySpuIdShopId:(NSString *)shopId
                          spuId:(NSString *)spuId
                     Completion:(void(^)(id responesObj))completion
                        Failure:(void(^)(NSError *error))conError;
/**
 查询当前店铺下待发货、待服务、待付款、待售后的订单数量
 
 @param shopId 店铺
 @param completion 成功
 @param conError 失败
 */
+ (void)queryOrderNumberShopId:(NSString *)shopId
                     Completion:(void(^)(id responesObj))completion
                        Failure:(void(^)(NSError *error))conError;



+ (void)queryitemDetailId:(NSString *)goodsId
                   UserId:(NSString *)userId
               Completion:(void(^)(id responesObj))completion
                  Failure:(void(^)(NSError *error))conError;

@end

NS_ASSUME_NONNULL_END
