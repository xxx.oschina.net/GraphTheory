//
//  YXStoreViewModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/30.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXStoreViewModel.h"
#import "WGStoreModel.h"
@implementation YXStoreViewModel


#pragma mark - 查询主营类目分类接口
+ (void)queryMainCategoryWithCategoryId:(NSString *)categoryId
                                  Level:(NSString *)level
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/appQueryCateByShop",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:categoryId forKey:@"categoryId"];
    [params setValue:level forKey:@"level"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 查询店铺信息
+ (void)queryShopDetailQueryCompletion:(void(^)(id responesObj))completion
                               Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/shopDetailQuery",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 验证店铺名字重复
+ (void)validShopName:(NSString *)shopName shopId:(NSString *)shopId Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/validShopName",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:shopId forKey:@"id"];
    [params setValue:shopName forKey:@"shopName"];
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 新增店铺
+ (void)requestInsertShopWithType:(NSInteger )type
                       StoreModel:(WGStoreModel *)storeModel
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    if (type == 0) {
        urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/createShop",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@shop/shop/storeArea/updateShop",kBaseURL];
    }    
    [NetWorkTools postWithUrl:urlStr parameters:[storeModel mj_JSONObject] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 新增店铺
+ (void)requestAddStoreModel:(WGStoreModel *)storeModel Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/createShop",kBaseURL];
    NSLog(@"---%@",[storeModel mj_JSONString]);
    [NetWorkTools postWithUrl:urlStr body:[storeModel mj_JSONData] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 修改店铺
+ (void)requestUpdateStoreModel:(WGStoreModel *)storeModel
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/shopDetailUpdate",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params = [storeModel mj_JSONObject];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}



+ (void)queryDistByShopType:(NSInteger)shopType IsPlat:(NSInteger)isPlat Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/v2/queryDistByShopType",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSString stringWithFormat:@"%ld",(long)shopType] forKey:@"shopType"];
    [params setValue:[NSString stringWithFormat:@"%ld",(long)isPlat] forKey:@"isPlat"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)queryCategoryByShopType:(NSInteger)type Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@items/items/itemscategory/queryCategoryByShopType",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSString stringWithFormat:@"%ld",(long)type] forKey:@"shopType"];
//    [params setValue:[YXUserInfoManager getUserInfo].isPlatMerchant?@"2":@"1" forKey:@"isPlat"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)getAreaDataAreaId:(NSString *)areaId Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@system/area/getAreaTreeData",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    //    [params setValue:areaId forKey:@"areaId"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 查询店铺详情
+ (void)queryShopDetailCompletion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/queryShopDetail",kBaseURL];
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}
//+ (void)queryShopDistributionTypeShopId:(NSString *)shopId Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
//    NSString *urlStr = [NSString stringWithFormat:@"%@shop/shop/store/selectDistributionType",kBaseURL];
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setValue:shopId forKey:@"shopId"];
//    [NetWorkTools getWithUrl:urlStr parameters:nil success:^(id responseObj) {
//        if (completion) {
//            completion(responseObj);
//        }
//    } failure:^(NSError *error) {
//        if (conError) {
//            conError(error);
//        }
//    }];
//}

#pragma mark -  版本更新
+ (void)checkVersionWithVersion:(NSString *)version
                          AppId:(NSString *)appId
                         xtType:(NSString *)xtType
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/1_0/userPlat/app/smallProgram/checkVersion.dd",@"http://user.ddsaas.cn/userplat"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:kAppType forKey:@"appType"];
    [params setValue:version forKey:@"version"];
    [params setValue:appId forKey:@"appId"];
    [params setValue:xtType forKey:@"xtType"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


@end
