//
//  WGOrderViewModel.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderViewModel.h"

@implementation WGOrderViewModel



+ (void)getOrderShopId:(NSString *)shopId OrderStatus:(NSString *)orderState StartTime:(NSString *)startTime EndTime:(NSString *)endTime PayMethod:(NSString *)payMethod Condition:(NSString *)condition TimeType:(NSString *)timeType DistributionType:(NSString *)distributionType PayType:(NSString *)payType SearchType:(NSString *)searchType orderNo:(NSString *)orderNo pageNum:(NSString *)pageNum pageSize:(NSString *)pageSize Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/newGeneral/queryOrderData",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderState forKey:@"orderState"];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:shopId forKey:@"shopId"];
    [params setValue:startTime forKey:@"startTime"];
    [params setValue:endTime forKey:@"endTime"];
    [params setValue:payMethod forKey:@"payMethod"];
    [params setValue:condition forKey:@"likeCondition"];
    [params setValue:timeType forKey:@"timeType"];
    [params setValue:distributionType forKey:@"distributionType"];
    [params setValue:payType forKey:@"payType"];
    [params setValue:searchType forKey:@"searchType"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}

+ (void)queryOrderStateCountCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/3_0/queryOrderStateCount",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

+ (void)queryOrderInfoWithOrderNo:(NSString *)orderNo
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/3_0/appQueryOrderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}



+ (void)updateServicePriceParm:(NSDictionary *)parm Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/service/updateServicePrice",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params addEntriesFromDictionary:parm];
    
    [NetWorkTools postWithUrl:urlStr body:[params mj_JSONData] success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)queryAllDeliveryCompletion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/queryAllDelivery",kBaseURL];

    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)sendGoodsOrderNo:(NSString *)orderNo deliveryTypeId:(NSString *)deliveryTypeId deliveryTypeName:(NSString *)deliveryTypeName expressNo:(NSString *)expressNo Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/sendGoods",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:deliveryTypeId forKey:@"deliveryTypeId"];
    [params setValue:deliveryTypeName forKey:@"deliveryTypeName"];
    [params setValue:expressNo forKey:@"expressNo"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)remindPayOrderNo:(NSString *)orderNo Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/remindPay",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)addOrderRemark:(NSString *)orderRemark
               OrderNo:(NSString *)orderNo
            Completion:(void(^)(id responesObj))completion
               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/common/3_0/addOrderRemark",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderRemark forKey:@"orderRemark"];
    [params setValue:orderNo forKey:@"orderNo"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
+ (void)queryShopReceivedCompletion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/service/queryShopReceived",kBaseURL];
    
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)takeServiceOrderOrderNo:(NSString *)orderNo receivedId:(NSString *)receivedId receivedName:(NSString *)receivedName Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/service/takeServiceOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:receivedId forKey:@"receivedId"];
    [params setValue:receivedName forKey:@"receivedName"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)confirmConsumerCodeOrderNo:(NSString *)orderNo consumerCode:(NSString *)consumerCode Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/service/confirmConsumerCode",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:consumerCode forKey:@"consumerCode"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}
+ (void)confirmServiceOrderNo:(NSString *)orderNo Completion:(void (^)(id _Nonnull))completion failure:(void (^)(NSError * _Nonnull))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/service/confirmServiceOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}


#pragma mark - 查询物流信息
+ (void)queryOrderLogisticsInfoWithOrderNo:(NSString *)orderNo
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/newGeneral/queryOrderLogisticsInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  PC端 售后/退款 (查询商家退款信息详情操作)
+ (void)queryRefundDataListWithPageNum:(NSString *)pageNum
                              PageSize:(NSString *)pageSize
                                ShopId:(NSString *)shopId
                           RefundState:(NSString *)refundState
                              RefundId:(NSString *)refundId
                            OrderState:(NSString *)orderState
                               OrderNo:(NSString *)orderNo
                             StartTime:(NSString *)startTime
                               EndTime:(NSString *)endTime
                             PayMethod:(NSString *)payMethod
                             Condition:(NSString *)condition
                         LikeCondition:(NSString *)likeCondition
                              TimeType:(NSString *)timeType
                      DistributionType:(NSString *)distributionType
                               PayType:(NSString *)payType
                            SearchType:(NSString *)searchType
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/queryRefundDataList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:pageSize forKey:@"pageSize"];
    [params setValue:shopId forKey:@"shopId"];
    [params setValue:refundState forKey:@"refundState"];
    [params setValue:refundId forKey:@"refundId"];
    [params setValue:orderState forKey:@"orderState"];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:startTime forKey:@"startTime"];
    [params setValue:endTime forKey:@"endTime"];
    [params setValue:payMethod forKey:@"payMethod"];
    [params setValue:condition forKey:@"condition"];
    [params setValue:likeCondition forKey:@"likeCondition"];
    [params setValue:timeType forKey:@"timeType"];
    [params setValue:distributionType forKey:@"distributionType"];
    [params setValue:payType forKey:@"payType"];
    [params setValue:searchType forKey:@"searchType"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}

+ (void)requestAgreedToByRefundId:(NSString *)refundId
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/agreedToRefund",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:refundId forKey:@"refundId"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

+ (void)requestRefuseToByRefundId:(NSString *)refundId
                     refundReason:(NSString *)refundReason
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/refuseToRefund",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:refundId forKey:@"refundId"];
    [params setValue:refundReason==nil?@"":refundReason forKey:@"refundReason"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


+ (void)queryRefundOrderLogisticsInfoById:(NSString *)refundId
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/queryRefundOrderLogisticsInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:refundId forKey:@"refundId"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



+ (void)requestOrderRefundWithOrderNo:(NSString *)orderNo
                         refundReason:(NSString *)refundReason
                         refundAmount:(NSString *)refundAmount
                        refundExplain:(NSString *)refundExplain
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/businessOrderRefund",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:orderNo forKey:@"orderNo"];
    [params setValue:refundReason forKey:@"refundReason"];
    [params setValue:refundAmount forKey:@"refundAmount"];
    [params setValue:refundExplain forKey:@"refundExplain"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


+ (void)queryRefundReasonCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/order/refund/businessQueryRefundReason",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
    
}

@end
