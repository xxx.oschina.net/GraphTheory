//
//  WGOrderViewModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"
#import "WGRequestModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGOrderViewModel : DDBaseModel


/**
 获取订单记录

 @param shopId 店铺id
 @param orderState 订单状态
 @param startTime 订单创建时间区间开始
 @param endTime 订单创建时间区间结束
 @param payMethod 支付方式
 @param condition 模糊查询的条件
 @param timeType 区分下单时间和支付时间，0：下单时间，1：支付时间
 @param distributionType 配送方式:1快递,2到店消费,3上门服务,选择多个是以逗号隔开
 @param payType 交易类型：01商品分销,02.本地商家
 @param searchType 区分模糊查询字段：-1：订单号,0:手机号,1:支付单号,2:快递单号,3:客户,4:商品名称,5:商品sku
 @param orderNo 订单号
 @param pageNum 当前页码
 @param pageSize 每页的条数
 @param completion 成功
 @param conError 失败
 */
+ (void)getOrderShopId:(NSString *)shopId
           OrderStatus:(NSString *)orderState
             StartTime:(NSString *)startTime
               EndTime:(NSString *)endTime
             PayMethod:(NSString *)payMethod
             Condition:(NSString *)condition
              TimeType:(NSString *)timeType
      DistributionType:(NSString *)distributionType
               PayType:(NSString *)payType
            SearchType:(NSString *)searchType
               orderNo:(NSString *)orderNo
               pageNum:(NSString *)pageNum
              pageSize:(NSString *)pageSize
            Completion:(void(^)(id responesObj))completion
               failure:(void(^)(NSError *error))conError;



/**
查询订单角标数量

@param completion 成功
@param conError 失败
*/
+ (void)queryOrderStateCountCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;



/**
app查询订单详情

@param orderNo      订单号
@param completion   成功
@param conError     失败
*/
+ (void)queryOrderInfoWithOrderNo:(NSString *)orderNo
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/*
"orderNo": "2019102114225205352743", 订单号
"itemPrices": [
               {
                   "itemId": "5566464", 商品id
                   "itemPrice": "1", 商品单价
                   "itemNum": "4" 商品数量
               }
               ]
}
 */
+ (void)updateServicePriceParm:(NSDictionary *)parm
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 查询所有的快递类型

 @param completion 成功
 @param conError 失败
 */
+ (void)queryAllDeliveryCompletion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;

/**
 发货

 @param orderNo 订单号
 @param deliveryTypeId 快递类型
 @param deliveryTypeName 类型名称
 @param expressNo 快递单号
 @param completion 成功
 @param conError 失败
 */
+ (void)sendGoodsOrderNo:(NSString *)orderNo
          deliveryTypeId:(NSString *)deliveryTypeId
        deliveryTypeName:(NSString *)deliveryTypeName
               expressNo:(NSString *)expressNo
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError;

/**
 提醒支付

 @param orderNo 订单号
 @param completion 成功
 @param conError 失败
 */
+ (void)remindPayOrderNo:(NSString *)orderNo
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError;


/**
添加订单备注

@param orderRemark 订单备注
@param orderNo 订单号
@param completion 成功
@param conError 失败
*/
+ (void)addOrderRemark:(NSString *)orderRemark
               OrderNo:(NSString *)orderNo
            Completion:(void(^)(id responesObj))completion
               failure:(void(^)(NSError *error))conError;



/**
 查询店铺下接单人员

 @param completion 成功
 @param conError 失败
 */
+ (void)queryShopReceivedCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

/**
 待接单下的接单操作

 @param orderNo 订单号
 @param receivedId 接单人id
 @param receivedName 接单人姓名
 @param completion 成功
 @param conError 失败
 */
+ (void)takeServiceOrderOrderNo:(NSString *)orderNo
                 receivedId:(NSString *)receivedId
                      receivedName:(NSString *)receivedName
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 待服务下的确认消费码操作

 @param orderNo 订单号
 @param consumerCode 消费码
 @param completion 成功
 @param conError 失败
 */
+ (void)confirmConsumerCodeOrderNo:(NSString *)orderNo
                      consumerCode:(NSString *)consumerCode
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;

/**
 确认订单

 @param orderNo 订单号
 @param completion 成功
 @param conError 失败
 */
+ (void)confirmServiceOrderNo:(NSString *)orderNo
                   Completion:(void(^)(id responesObj))completion
                      failure:(void(^)(NSError *error))conError;


/**
查询物流信息

@param orderNo 订单号
@param completion 成功
@param conError 失败
*/
+ (void)queryOrderLogisticsInfoWithOrderNo:(NSString *)orderNo
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError;



/**
 PC端 售后/退款 (查询商家退款信息详情操作)

 @param pageNum         当前页码
 @param pageSize        每页的条数
 @param shopId          店铺id
 @param refundState     售后状态：0退款中,1退款成功,2退款关闭
 @param refundId        退款记录id
 @param orderState      订单状态
 @param orderNo         订单号
 @param startTime       订单创建时间区间开始
 @param endTime         订单创建时间区间结束
 @param payMethod       支付方式
 @param condition       模糊查询的条件
 @param likeCondition   生意经app模糊查询条件
 @param timeType        区分下单时间和支付时间，0：下单时间，1：支付时间
 @param distributionType 配送方式:1快递,2到店消费,3上门服务,选择多个是以逗号隔开
 @param payType         交易类型：01商品分销,02.本地商家
 @param searchType      区分模糊查询字段：-1：订单号,0:手机号,1:支付单号,2:快递单号,3:客户,4:商品名称,5:商品sku
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryRefundDataListWithPageNum:(NSString *)pageNum
                              PageSize:(NSString *)pageSize
                                ShopId:(NSString *)shopId
                           RefundState:(NSString *)refundState
                              RefundId:(NSString *)refundId
                            OrderState:(NSString *)orderState
                               OrderNo:(NSString *)orderNo
                             StartTime:(NSString *)startTime
                               EndTime:(NSString *)endTime
                             PayMethod:(NSString *)payMethod
                             Condition:(NSString *)condition
                         LikeCondition:(NSString *)likeCondition
                              TimeType:(NSString *)timeType
                      DistributionType:(NSString *)distributionType
                               PayType:(NSString *)payType
                            SearchType:(NSString *)searchType
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;



/**
 售后/退款 (同意退款操作)

 @param refundId        退款记录id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestAgreedToByRefundId:(NSString *)refundId
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
 售后/退款 (拒绝退款操作)

 @param refundId        退款记录id
 @param refundReason    拒绝退款原因
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestRefuseToByRefundId:(NSString *)refundId
                     refundReason:(NSString *)refundReason
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;




/**
 查询退款物流的接口(买家已发货)

 @param refundId        退款记录id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryRefundOrderLogisticsInfoById:(NSString *)refundId
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError;


/**
 商家发起退款

 @param orderNo         订单号
 @param refundReason    退款原因
 @param refundAmount    退款金额
 @param refundExplain   退款说明
 @param completion      返回成功
 @param conError        返回失败
*/
+ (void)requestOrderRefundWithOrderNo:(NSString *)orderNo
                         refundReason:(NSString *)refundReason
                         refundAmount:(NSString *)refundAmount
                        refundExplain:(NSString *)refundExplain
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;

/**
 商家查询退款原因
 
 @param completion      返回成功
 @param conError        返回失败
*/
+ (void)queryRefundReasonCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

@end

NS_ASSUME_NONNULL_END
