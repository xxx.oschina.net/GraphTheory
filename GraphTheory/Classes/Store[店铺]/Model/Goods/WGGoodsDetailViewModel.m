//
//  WGGoodsDetailViewModel.m
//  DDLife
//
//  Created by wanggang on 2019/11/27.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "WGGoodsDetailViewModel.h"
#import "WGGoodsInfoModel.h"
@implementation WGGoodsBottomActionModel

- (NSMutableArray *)leftButtons{
    if (!_leftButtons) {
        _leftButtons = [NSMutableArray new];
        if (self.goodsType == WGGoodsTypeJD) {
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }else if (self.goodsType == WGGoodsTypeNormal){
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }else if (self.goodsType == WGGoodsTypeNewMenber){
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }else if (self.goodsType == WGGoodsTypeFlashSale){
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }else if (self.goodsType == WGGoodsTypeGroupon){
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }else if (self.goodsType == WGGoodsTypeGrouponIng){
            
        }else if (self.goodsType == WGGoodsTypeGrouponEnd){
            
        }else if (self.goodsType == WGGoodsTypeThirdShare){
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeThirdShare)]];
        }else{
            [_leftButtons addObjectsFromArray:@[@(WGGoodsButtonTypeShop),@(WGGoodsButtonTypeServie),@(WGGoodsButtonTypeCart)]];
        }
    }
    return _leftButtons;
}
- (NSMutableArray *)rightButtons{
    if (!_rightButtons) {
        _rightButtons = [NSMutableArray new];
        if (self.goodsType == WGGoodsTypeJD) {
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeAddCart),@(WGGoodsButtonTypeBuyNow)]];
        }else if (self.goodsType == WGGoodsTypeNormal){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeAddCart),@(WGGoodsButtonTypeBuyNow)]];
        }else if (self.goodsType == WGGoodsTypeNewMenber){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeBuyNow)]];
        }else if (self.goodsType == WGGoodsTypeFlashSale){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeAddCart),@(WGGoodsButtonTypeBuyNow)]];
        }else if (self.goodsType == WGGoodsTypeGroupon){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeOneBuy),@(WGGoodsButtonTypeGrouponBuy)]];
        }else if (self.goodsType == WGGoodsTypeGrouponIng){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeGrouponIng)]];
        }else if (self.goodsType == WGGoodsTypeGrouponEnd){
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeGrouponBegin)]];
        }else if (self.goodsType == WGGoodsTypeThirdShare){
//            if (self.thirdGoods.couponState == 1) {
//                [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeThirdBuy)]];
//
//            }else{
//                [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeThirdBuyNow)]];
//            }
        }else{
            [_rightButtons addObjectsFromArray:@[@(WGGoodsButtonTypeAddCart),@(WGGoodsButtonTypeBuyNow)]];
        }
    }
    return _rightButtons;
}
@end

@implementation WGGoodsViewRowModel

@end

@implementation WGGoodsViewSetionModel
- (NSMutableArray *)goodsViewRows{
    if (!_goodsViewRows) {
        _goodsViewRows = [NSMutableArray new];
        if (_setionType == WGGoodsViewSetionTypeInventGroupon){
            if (_goodsType == WGGoodsTypeGrouponIng) {
                WGGoodsViewRowModel *groupIng = [WGGoodsViewRowModel new];
                groupIng.setionType = _setionType;
                groupIng.goodsInfo = _goodsInfo;
                groupIng.goodsType = self.goodsType;;
                groupIng.detailType = WGGoodsViewDetailTypeGrouponIng;
                [_goodsViewRows addObject:groupIng];
                WGGoodsViewRowModel *groupIng1 = [WGGoodsViewRowModel new];
                groupIng1.setionType = _setionType;
                groupIng1.goodsInfo = _goodsInfo;
                groupIng1.goodsType = self.goodsType;;
                groupIng1.detailType = WGGoodsViewDetailTypeGrouponIng;
                [_goodsViewRows addObject:groupIng1];
                WGGoodsViewRowModel *groupIng2 = [WGGoodsViewRowModel new];
                groupIng2.setionType = _setionType;
                groupIng2.goodsInfo = _goodsInfo;
                groupIng2.goodsType = self.goodsType;;
                groupIng2.detailType = WGGoodsViewDetailTypeGrouponIng;
                [_goodsViewRows addObject:groupIng2];
                
            }
            
        }else if (_setionType == WGGoodsViewSetionTypeGoodsInfo) {
            WGGoodsViewRowModel *banner = [WGGoodsViewRowModel new];
            banner.setionType = _setionType;
            banner.goodsInfo = _goodsInfo;
            banner.goodsType = self.goodsType;;
            banner.detailType = WGGoodsViewDetailTypeBanner;
            [_goodsViewRows addObject:banner];
            if (_goodsType == WGGoodsTypeFlashSale) {
                if (_goodsInfo.isStart == 1) {
                    WGGoodsViewRowModel *flashSaleDetial = [WGGoodsViewRowModel new];
                    flashSaleDetial.setionType = _setionType;
                    flashSaleDetial.goodsInfo = _goodsInfo;
                    flashSaleDetial.goodsType = self.goodsType;;
                    flashSaleDetial.detailType = WGGoodsViewDetailTypeFlashSaleDetial;
                    [_goodsViewRows addObject:flashSaleDetial];
                }else{
                    WGGoodsViewRowModel *flashSaleDetial = [WGGoodsViewRowModel new];
                    flashSaleDetial.setionType = _setionType;
                    flashSaleDetial.goodsInfo = _goodsInfo;
                    flashSaleDetial.goodsType = self.goodsType;;
                    flashSaleDetial.detailType = WGGoodsViewDetailTypeFlashSaleRemind;
                    [_goodsViewRows addObject:flashSaleDetial];
                    WGGoodsViewRowModel *normalPrice = [WGGoodsViewRowModel new];
                    normalPrice.setionType = _setionType;
                    normalPrice.goodsInfo = _goodsInfo;
                    normalPrice.goodsType = self.goodsType;;
                    normalPrice.detailType = WGGoodsViewDetailTypeNormalPrice;
                    [_goodsViewRows addObject:normalPrice];
                }
                
            }else{
                WGGoodsViewRowModel *normalPrice = [WGGoodsViewRowModel new];
                normalPrice.setionType = _setionType;
                normalPrice.goodsInfo = _goodsInfo;
                normalPrice.goodsType = self.goodsType;;
                normalPrice.detailType = WGGoodsViewDetailTypeNormalPrice;
                [_goodsViewRows addObject:normalPrice];
                
            }
            WGGoodsViewRowModel *goodsTitle = [WGGoodsViewRowModel new];
            goodsTitle.setionType = _setionType;
            goodsTitle.goodsInfo = _goodsInfo;
            goodsTitle.goodsType = self.goodsType;;
            goodsTitle.detailType = WGGoodsViewDetailTypeTitle;
            [_goodsViewRows addObject:goodsTitle];
            
            
            
        }else if (_setionType == WGGoodsViewSetionTypeDiscounts){
            WGGoodsViewRowModel *discounts = [WGGoodsViewRowModel new];
            discounts.setionType = _setionType;
            discounts.goodsInfo = _goodsInfo;
            discounts.goodsType = self.goodsType;;
            discounts.detailType = WGGoodsViewDetailTypeDiscounts;
            [_goodsViewRows addObject:discounts];
        }else if (_setionType == WGGoodsViewSetionTypeOnGroupon){
            WGGoodsViewRowModel *onGroupon = [WGGoodsViewRowModel new];
            onGroupon.setionType = _setionType;
            onGroupon.goodsInfo = _goodsInfo;
            onGroupon.goodsType = self.goodsType;;
            onGroupon.detailType = WGGoodsViewDetailTypeOnGroupon;
            [_goodsViewRows addObject:onGroupon];
        }else if (_setionType == WGGoodsViewSetionTypeSelectSome){
            WGGoodsViewRowModel *sku = [WGGoodsViewRowModel new];
            sku.setionType = _setionType;
            sku.goodsInfo = _goodsInfo;
            sku.goodsType = self.goodsType;;
            sku.detailType = WGGoodsViewDetailTypeSku;
            [_goodsViewRows addObject:sku];
            WGGoodsViewRowModel *address = [WGGoodsViewRowModel new];
            address.setionType = _setionType;
            address.goodsInfo = _goodsInfo;
            address.goodsType = self.goodsType;;
            address.detailType = WGGoodsViewDetailTypeAddress;
            [_goodsViewRows addObject:address];
            //            WGGoodsViewRowModel *freight = [WGGoodsViewRowModel new];
            //            freight.setionType = _setionType;
            //            freight.goodsInfo = _goodsInfo;
            //            freight.goodsType = self.goodsType;;
            //            freight.detailType = WGGoodsViewDetailTypeFreight;
            //            [_goodsViewRows addObject:freight];
        }else if (_setionType == WGGoodsViewSetionTypeParm){
            WGGoodsViewRowModel *parm = [WGGoodsViewRowModel new];
            parm.setionType = _setionType;
            parm.goodsInfo = _goodsInfo;
            parm.goodsType = self.goodsType;;
            parm.detailType = WGGoodsViewDetailTypeParm;
            [_goodsViewRows addObject:parm];
            
        }else if (_setionType == WGGoodsViewSetionTypeDetail){
            WGGoodsViewRowModel *detail = [WGGoodsViewRowModel new];
            detail.setionType = _setionType;
            detail.goodsInfo = _goodsInfo;
            detail.goodsType = self.goodsType;;
            detail.detailType = WGGoodsViewDetailTypeDetail;
            [_goodsViewRows addObject:detail];
        }else if (_setionType == WGGoodsViewSetionTypeThirdTicket){
            WGGoodsViewRowModel *thirdTiket = [WGGoodsViewRowModel new];
            thirdTiket.setionType = _setionType;
            thirdTiket.goodsInfo = _goodsInfo;
            thirdTiket.goodsType = self.goodsType;;
            thirdTiket.detailType = WGGoodsViewDetailTypeThirdTiket;
            [_goodsViewRows addObject:thirdTiket];
        }
        //        else if (_setionType == WGGoodsViewSetionTypeThirdShop){
        //            WGGoodsViewRowModel *thirdShop = [WGGoodsViewRowModel new];
        //            thirdShop.setionType = _setionType;
        //            thirdShop.goodsInfo = _goodsInfo;
        //            thirdShop.goodsType = self.goodsType;;
        //            thirdShop.detailType = WGGoodsViewDetailTypeThirdShop;
        //            [_goodsViewRows addObject:thirdShop];
        //        }
    }
    return _goodsViewRows;
}

@end

@interface WGGoodsDetailViewModel ()

@property (nonatomic, assign) NSInteger grouponTag;///<拼团中位置
@property (nonatomic, assign) NSInteger discountsTag;///<优惠券位置


@end

@implementation WGGoodsDetailViewModel

- (WGGoodsType)goodsType{
//    if (self.thirdGoods) {
//        return WGGoodsTypeThirdShare;
//    }
    if (_goodsType == WGGoodsTypeSoldOut) {
        return WGGoodsTypeSoldOut;
    }
    if (_goodsInfo.activityType == 4) {
        return WGGoodsTypeGroupon;
    }else if(_goodsInfo.activityType == 3){
        return WGGoodsTypeFlashSale;
    }else if(_goodsInfo.activityType == 2){
        return WGGoodsTypeNewMenber;
    }else if(_goodsInfo.activityType == 6){
        return WGGoodsTypePoint;
    }
    else{
        if (_goodsInfo.source.integerValue == 1) {
            return WGGoodsTypeJD;
        }else{
            return WGGoodsTypeNormal;
        }
    }
    return WGGoodsTypeNormal;
}
- (WGGoodsBottomActionModel *)goodsBottomAction{
    if (!_goodsBottomAction) {
        _goodsBottomAction = [WGGoodsBottomActionModel new];
        _goodsBottomAction.goodsType = self.goodsType;
        _goodsBottomAction.goodsInfo = self.goodsInfo;
//        _goodsBottomAction.thirdGoods = self.thirdGoods;
    }
    return _goodsBottomAction;
}
- (void)setGoodsInfo:(WGGoodsInfoModel *)goodsInfo{
    _goodsInfo = goodsInfo;
}
- (NSMutableArray *)goodsViewSections{
    if (!_goodsViewSections) {
        _goodsViewSections = [NSMutableArray new];
        NSInteger tag = 0;
        if (self.goodsType == WGGoodsTypeGrouponIng) {
            WGGoodsViewSetionModel *grouponIng = [WGGoodsViewSetionModel new];
            grouponIng.goodsType = self.goodsType;
            grouponIng.setionType = WGGoodsViewSetionTypeInventGroupon;
            grouponIng.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:grouponIng];
            tag ++;
        }
        if (self.goodsType == WGGoodsTypeGrouponEnd) {
            WGGoodsViewSetionModel *grouponEnd = [WGGoodsViewSetionModel new];
            grouponEnd.goodsType = self.goodsType;
            grouponEnd.setionType = WGGoodsViewSetionTypeInventGrouponEnd;
            grouponEnd.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:grouponEnd];
            tag ++;
        }
        
        WGGoodsViewSetionModel *goodsInfo = [WGGoodsViewSetionModel new];
        goodsInfo.goodsType = self.goodsType;
        goodsInfo.setionType = WGGoodsViewSetionTypeGoodsInfo;
        goodsInfo.goodsInfo = _goodsInfo;
        [_goodsViewSections addObject:goodsInfo];
        tag ++;
        //        WGGoodsViewSetionModel *discounts = [WGGoodsViewSetionModel new];
        //        discounts.goodsType = self.goodsType;;
        //        discounts.setionType = WGGoodsViewSetionTypeDiscounts;
        //        discounts.goodsInfo = _goodsInfo;
        //        [_goodsViewSections addObject:discounts];
        //        tag ++;
        _discountsTag = tag;
        _grouponTag = tag;
        //        if (self.goodsType == WGGoodsTypeGroupon) {
        //            WGGoodsViewSetionModel *onFlashSale = [WGGoodsViewSetionModel new];
        //            onFlashSale.goodsType = self.goodsType;;
        //            onFlashSale.setionType = WGGoodsViewSetionTypeOnGroupon;
        //            [_goodsViewSections addObject:onFlashSale];
        //        }
        if (self.goodsType != WGGoodsTypeThirdShare) {
            WGGoodsViewSetionModel *selectSome = [WGGoodsViewSetionModel new];
            selectSome.goodsType = self.goodsType;;
            selectSome.setionType = WGGoodsViewSetionTypeSelectSome;
            selectSome.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:selectSome];
            tag ++;
        }else{
//            if (_thirdGoods.couponState == 1) {
//                WGGoodsViewSetionModel *thirdTicket = [WGGoodsViewSetionModel new];
//                thirdTicket.goodsType = self.goodsType;;
//                thirdTicket.setionType = WGGoodsViewSetionTypeThirdTicket;
//                thirdTicket.goodsInfo = _goodsInfo;
//                [_goodsViewSections addObject:thirdTicket];
//            }

            
            WGGoodsViewSetionModel *thirdShop = [WGGoodsViewSetionModel new];
            thirdShop.goodsType = self.goodsType;;
            thirdShop.setionType = WGGoodsViewSetionTypeThirdShop;
            thirdShop.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:thirdShop];
        }
        
        if (_goodsInfo.param.length) {
            WGGoodsViewSetionModel *parm = [WGGoodsViewSetionModel new];
            parm.goodsType = self.goodsType;;
            parm.setionType = WGGoodsViewSetionTypeParm;
            parm.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:parm];
            tag ++;
            
        }
        if (_goodsInfo.nappintroduction.length) {
            WGGoodsViewSetionModel *detail = [WGGoodsViewSetionModel new];
            detail.goodsType = self.goodsType;;
            detail.setionType = WGGoodsViewSetionTypeDetail;
            detail.goodsInfo = _goodsInfo;
            [_goodsViewSections addObject:detail];
            tag ++;
        }
    }
    return _goodsViewSections;
}
- (void)addGrouping{
    if (self.goodsType == WGGoodsTypeGroupon) {
        WGGoodsViewSetionModel *onFlashSale = [WGGoodsViewSetionModel new];
        onFlashSale.goodsType = self.goodsType;;
        onFlashSale.setionType = WGGoodsViewSetionTypeOnGroupon;
        if (self.goodsViewSections.count > self.grouponTag) {
            [self.goodsViewSections insertObject:onFlashSale atIndex:self.grouponTag];
        }else{
            [self.goodsViewSections addObject:onFlashSale];
        }
    }
}
- (void)addDiscounts{
    if (self.goodsType != WGGoodsTypeThirdShare) {
        WGGoodsViewSetionModel *discounts = [WGGoodsViewSetionModel new];
        discounts.goodsType = self.goodsType;;
        discounts.setionType = WGGoodsViewSetionTypeDiscounts;
        discounts.goodsInfo = _goodsInfo;
        if (self.goodsViewSections.count > self.discountsTag) {
            [self.goodsViewSections insertObject:discounts atIndex:self.discountsTag];
        }else{
            [self.goodsViewSections addObject:discounts];
        }
        self.grouponTag ++;
    }
    
}
@end
