//
//  ZLJFindItemsGroupModel.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJFindItemsGroupModel.h"
#import "ZLJItemsManageModel.h"

@implementation FindItemsGroupDataItem

- (id)copyWithZone:(NSZone *)zone{
   return  [self lxz_copyWithZone:zone];
}

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"kId":@"id"};
}
- (NSString *)spuIds{
    if (_spuIdArr) {
        _spuIds = [NSString stringWithFormat:@"[%@]",[[_spuIdArr allObjects] componentsJoinedByString:@","]];
    }
    return _spuIds;
}
- (NSMutableSet *)spuIdArr{
    if (!_spuIdArr) {
        _spuIdArr = [NSMutableSet new];
    }
    return _spuIdArr;
}
- (NSMutableArray *)selectArr{
    if (!_selectArr) {
        _selectArr = [NSMutableArray new];
    }
    return _selectArr;
}
@end


@implementation ZLJFindItemsGroupModel

+(NSDictionary *)mj_objectClassInArray{
    return @{@"data":[FindItemsGroupDataItem class]};
}
@end
