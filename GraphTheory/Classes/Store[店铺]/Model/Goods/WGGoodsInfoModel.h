//
//  WGGoodsInfoModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/9.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class Paths,SpecSku,SpecList;
@interface WGGoodsInfoModel : NSObject

@property (nonatomic , copy) NSString              * itemId;///<商品id
@property (nonatomic , copy) NSString              * wareQD;
@property (nonatomic , copy) NSString              * imagePath;///<主图
@property (nonatomic , copy) NSString              * monthSales;///<月销
@property (nonatomic , copy) NSString              * param;///<参数
@property (nonatomic , copy) NSString              * itemStock;///<库存
@property (nonatomic , copy) NSString              * nappintroduction;///<商品详情
@property (nonatomic , copy) NSArray<Paths *>              * paths;///<轮播图
@property (nonatomic , copy) NSString              * distributionType;///<配送方式
@property (nonatomic , copy) NSString              * source;///<来源 01京东 02自定义商品
@property (nonatomic , copy) NSArray<SpecSku *>              * specSku;///<sku
@property (nonatomic , copy) NSArray<SpecSku *>              * specSkuSort;///<本地sku的数据,按照spec排序

@property (nonatomic , copy) NSArray<SpecList *>              * specList;///<sku
@property (nonatomic , strong) NSArray<SpecList *>              * specListSort;///本地排序，去重复和没有规格的

@property (nonatomic , copy) NSString              * itemName;///<商品名
@property (nonatomic , copy) NSString              * price;///<价格
@property (nonatomic , copy) NSString              * exchangeIntegral;///<积分
@property (nonatomic , copy) NSString              * oriPrice;///<原价
@property (nonatomic , copy) NSString              * activityPrice;///<活动价
@property (nonatomic , copy) NSString              * specialPeoplePrice;///<新人专享
@property (nonatomic , copy) NSString              * singlePrice;

@property (nonatomic , copy) NSString              * activityTypeName;///<活动名
@property (nonatomic , copy) NSString              * shopId;///<店铺id
@property (nonatomic , copy) NSString              * shopName;///<店铺名
@property (nonatomic , copy) NSString              * brandName;
@property (nonatomic , copy) NSString              * startDate;///<活动开始时间
@property (nonatomic , copy) NSString              * endDate;///<活动结束时间
@property (nonatomic , copy) NSString              * acvtivityId;///<活动id
@property (nonatomic , copy) NSString              * spuId;
@property (nonatomic , copy) NSString              * activityId;
@property (nonatomic , copy) NSString              * buyNum;///<已买件数
@property (nonatomic , assign) NSInteger              followNum;///<已关注

@property (nonatomic , assign) NSInteger              isRemind;///<是否提醒
@property (nonatomic , assign) NSInteger              groupNumber;///<拼团人数
@property (nonatomic , assign) NSInteger              activityType;///<活动类型 5:自定义活动 4:拼团 3:抢购 2:新人 1:优惠券 6:积分兑换
@property (nonatomic , assign) NSInteger              specialPeople;///<是否新人专享 1:是2:否
@property (nonatomic , assign) NSInteger              isStart;///<活动是否开始
/* 本地用的属性 */
@property (nonatomic , copy) NSString              * noSelect;///<规格选中的格式化内容
@property (nonatomic , assign) NSInteger              goodsCount;///<数量
@property (nonatomic , assign) CGFloat                particularsWebViewHeight;
@property (nonatomic , assign) NSInteger              goodStatus;
@property (nonatomic , strong) SpecSku              * selectSku;///<选中的sku


@end

@interface Paths :NSObject
@property (nonatomic , copy) NSString              * path;
@end



@class SpecList;
@interface SpecSku :NSObject
@property (nonatomic , copy) NSArray              * spec;
@property (nonatomic , copy) NSString             * price;
@property (nonatomic , copy) NSString             * exchangeIntegral;///<积分
@property (nonatomic , copy) NSString             * singlePrice;
@property (nonatomic , copy) NSString             * activityPrice;
@property (nonatomic , copy) NSString             * specialPeoplePrice;
@property (nonatomic , copy) NSString             * imagePath;
@property (nonatomic , copy) NSString             * skuInfo;
@property (nonatomic , copy) NSString             * stock;
@property (nonatomic , copy) NSArray<SpecList *>              * specList;
@property (nonatomic , strong) NSArray              * specQC;///<sku去重
@property (nonatomic , strong) NSMutableArray              * specSort;///<本地的排好序的sku

@end



@class SpecValueList;
@interface SpecList :NSObject
@property (nonatomic , copy) NSArray<SpecValueList *>              * specValueList;
@property (nonatomic , copy) NSString              * specName;
@property (nonatomic , strong) NSArray              * specValue;


@end

@interface SpecValueList :NSObject
@property (nonatomic , copy) NSString              * specValue;

@end



NS_ASSUME_NONNULL_END
