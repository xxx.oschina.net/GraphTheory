//
//  ZLJFindItemsGroupModel.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FindItemsGroupDataItem :NSObject<NSCopying>
@property (nonatomic , copy) NSString              * kId;//分组id
@property (nonatomic , assign) NSInteger              active;//1:正常;0:锁定;-1:删除
@property (nonatomic , copy) NSString              * createPerson;//创建人
@property (nonatomic , copy) NSString              * url;//商品分组链接地址
@property (nonatomic , copy) NSString              * createTime;//创建时间
@property (nonatomic , copy) NSString              * remark;//备注
@property (nonatomic , copy) NSString              * num;//商品数量
@property (nonatomic , copy) NSString              * shopId;//店铺id
@property (nonatomic , copy) NSString              * updatePerson;//最后更新人
@property (nonatomic , copy) NSString              * groupName;//分组名称
@property (nonatomic , copy) NSString              * spuIds;//分组下spuId集合
@property (nonatomic , copy) NSString              * sort;//排序
@property (nonatomic , copy) NSString              * updateTime;//最后更新时间

@property (nonatomic , strong) NSMutableSet              * spuIdArr;//分组商品
@property (nonatomic , strong) NSMutableArray              * selectArr;//选中商品商品





@end


@interface ZLJFindItemsGroupModel :NSObject
@property (nonatomic , assign) NSInteger              pageSize;//每页多少数据
@property (nonatomic , copy) NSString              * time;//时间
@property (nonatomic , strong) NSArray <FindItemsGroupDataItem *>              * data;//数据源
@property (nonatomic , assign) NSInteger              code;//请求吗
@property (nonatomic , assign) NSInteger              pageCount;//
@property (nonatomic , copy) NSString              * msg;//请求信息 
@property (nonatomic , assign) NSInteger              total;//数据个数
@property (nonatomic , assign) BOOL              state;//状态
@property (nonatomic , assign) NSInteger              pageNum;// 

@end

NS_ASSUME_NONNULL_END
