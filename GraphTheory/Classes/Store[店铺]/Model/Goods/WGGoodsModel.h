//
//  WGGoodsModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/28.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"
#import "ZLJFindItemsGroupModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface Spu :NSObject
@property (nonatomic , copy) NSString              * images;
@property (nonatomic , copy) NSString              * shopId;
@property (nonatomic , copy) NSString              * category_id;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * distributionType;
@property (nonatomic , copy) NSString              * brand_id;
@property (nonatomic , copy) NSString              * categoryName;

@end

@interface SpecMsg :NSObject
@property (nonatomic , copy) NSString              * spec_value;
@property (nonatomic , copy) NSString              * spec_id;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * spec_name;

@end

@interface SkuList :NSObject
@property (nonatomic , copy) NSString              * itemId;
@property (nonatomic , copy) NSString              * itemStock;
@property (nonatomic , copy) NSString              * skuName;
@property (nonatomic , assign) NSInteger              itemsState;
@property (nonatomic , copy) NSString              * specValue;
@property (nonatomic , copy) NSString              * source;
@property (nonatomic , copy) NSString              * specName;
@property (nonatomic , copy) NSString              * skuInfoId;
@property (nonatomic , assign) NSInteger              costPrice;
@property (nonatomic , strong) NSArray<SpecMsg *>              * specMsg;
@property (nonatomic , strong) NSString            * price;
@property (nonatomic , copy) NSString              * spuId;
@property (nonatomic , copy) NSString              * shopId;
@property (nonatomic , assign) NSInteger              oriPrice;
@property (nonatomic , copy) NSString              * skuId;
@property (nonatomic , copy) NSString              * imagePath;

@end

@interface SpuDetail :NSObject
@property (nonatomic , copy) NSString              * updatePerson;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , assign) NSInteger              active;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * updateTime;
@property (nonatomic , copy) NSString              * createPerson;
@property (nonatomic , copy) NSString              * itemsDetails;
@property (nonatomic , copy) NSString              * createTime;

@end

@interface WGGoodsModel :NSObject

@property (nonatomic , strong) FindItemsGroupDataItem              * itemGroupMsg;
@property (nonatomic , strong) Spu              * spu;
@property (nonatomic , copy) NSArray<SkuList *>              * skuList;
@property (nonatomic , strong) SpuDetail              * spuDetail;

@end

NS_ASSUME_NONNULL_END
