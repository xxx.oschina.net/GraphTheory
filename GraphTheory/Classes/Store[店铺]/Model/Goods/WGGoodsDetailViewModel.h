//
//  WGGoodsDetailViewModel.h
//  DDLife
//
//  Created by wanggang on 2019/11/27.
//  Copyright © 2019年 点都. All rights reserved.
//

#import <Foundation/Foundation.h>
//

NS_ASSUME_NONNULL_BEGIN

/**
 商品类型
 */
typedef NS_ENUM(NSUInteger, WGGoodsType) {
    WGGoodsTypeUnKnow,  ///<未知
    WGGoodsTypeJD,  ///<京东商品
    WGGoodsTypeNormal,  ///<自营普通商品
    WGGoodsTypeFlashSale,  ///<抢购
    WGGoodsTypeGroupon,  ///<拼团
    WGGoodsTypeNewMenber,  ///<新人专享
    WGGoodsTypePoint,  ///<积分兑换
    WGGoodsTypeGrouponIng,  ///<拼团中
    WGGoodsTypeGrouponEnd,  ///<拼团结束
    WGGoodsTypeSoldOut,  ///<下架
    WGGoodsTypeThirdShare,  ///<第三方分享
};
/**
 商品详情底部类型
 */
typedef NS_ENUM(NSUInteger, WGGoodsButtonType) {
    WGGoodsButtonTypeNone,  ///<无操作
    WGGoodsButtonTypeShop,  ///<店铺
    WGGoodsButtonTypeServie,  ///<客服
    WGGoodsButtonTypeCart,  ///<购物车
    WGGoodsButtonTypeAddCart,  ///<加入购物车
    WGGoodsButtonTypeBuyNow,  ///<立即购买
    WGGoodsButtonTypeOneBuy,  ///<单独购买
    WGGoodsButtonTypeGrouponBuy,  ///<拼团购买
    WGGoodsButtonTypeGrouponIng,  ///<拼团中
    WGGoodsButtonTypeGrouponBegin,  ///<发起拼团
    WGGoodsButtonTypeGrouponJoin,  ///<参与拼团
    WGGoodsButtonTypeThirdShare,  ///<三方分享
    WGGoodsButtonTypeThirdBuy,  ///<三方领券购买
    WGGoodsButtonTypeThirdBuyNow,  ///<三方立即购买
    WGGoodsButtonTypePointExchange,  ///<积分兑换
};

/**
 商品详情分组显示类型
 */
typedef NS_ENUM(NSUInteger, WGGoodsViewSetionType) {
    WGGoodsViewSetionTypeInventGroupon,  ///<邀请拼团信息
    WGGoodsViewSetionTypeInventGrouponEnd,  ///<邀请拼团信信息，结束
    WGGoodsViewSetionTypeGoodsInfo,  ///<商品信息分组(图片,标题,价格等)
    WGGoodsViewSetionTypeDiscounts,  ///<商品优惠信息
    WGGoodsViewSetionTypeOnGroupon,  ///<商品拼团中
    WGGoodsViewSetionTypeSelectSome,  ///<商品选择信息(地址、规格、运费)
    WGGoodsViewSetionTypeParm,  ///<商品参数
    WGGoodsViewSetionTypeDetail,  ///<商品详情
    WGGoodsViewSetionTypeThirdTicket,  ///<第三方优惠券
    WGGoodsViewSetionTypeThirdShop,  ///<第三方店铺头
};

/**
 商品详情具体显示类型
 */
typedef NS_ENUM(NSUInteger, WGGoodsViewDetailType) {
    WGGoodsViewDetailTypeGrouponIng,  ///<拼团中,当前拼团人数
    WGGoodsViewDetailTypeGrouponEnd,  ///<拼团中,拼团结束
    WGGoodsViewDetailTypeGrouponCountDown,  ///<拼团倒计时
    WGGoodsViewDetailTypeBanner,  ///<商品轮播图
    WGGoodsViewDetailTypeFlashSaleDetial,  ///<商品抢购详情(价格,倒计时等)
    WGGoodsViewDetailTypeFlashSaleRemind,  ///<商品抢购提醒
    WGGoodsViewDetailTypeNormalPrice,  ///<商品价格、销量正常显示
    WGGoodsViewDetailTypeTitle,  ///<商品标题
    WGGoodsViewDetailTypeDiscounts,  ///<商品优惠
    WGGoodsViewDetailTypeOnGroupon,  ///<拼团中
    WGGoodsViewDetailTypeSku,  ///<商品规格选中
    WGGoodsViewDetailTypeAddress,  ///<配送地址选中
    WGGoodsViewDetailTypeFreight,  ///<运费
    WGGoodsViewDetailTypeParm,  ///<参数
    WGGoodsViewDetailTypeDetail,  ///<详情
    WGGoodsViewDetailTypeThirdTiket,  ///<第三方优惠券
    WGGoodsViewDetailTypeThirdShop,  ///<第三方店铺
};
/**
 底部按钮
 */
@class WGGoodsInfoModel;
@interface WGGoodsBottomActionModel : NSObject
@property (nonatomic, assign) WGGoodsType goodsType;///<商品类型
@property (nonatomic, strong) WGGoodsInfoModel *goodsInfo;///<商品
//@property (nonatomic ,strong) ZLJTicketCenterInfoModel *thirdGoods;///淘宝商品

@property (nonatomic, strong) NSMutableArray *leftButtons;///<左边按钮数组
@property (nonatomic, strong) NSMutableArray *rightButtons;///<右边按钮数组

@end

/**
 商品显示行
 */
@interface WGGoodsViewRowModel : NSObject

@property (nonatomic, strong) WGGoodsInfoModel *goodsInfo;///<商品
@property (nonatomic, assign) WGGoodsType goodsType;///<商品类型
@property (nonatomic, assign) WGGoodsViewSetionType setionType;///<分组类型
@property (nonatomic, assign) WGGoodsViewDetailType detailType;///<分组类型

@end

/**
 商品显示分组
 */
@interface WGGoodsViewSetionModel : NSObject

@property (nonatomic, strong) WGGoodsInfoModel *goodsInfo;///<商品
@property (nonatomic, assign) WGGoodsType goodsType;///<商品类型
@property (nonatomic, assign) WGGoodsViewSetionType setionType;///<分组类型
@property (nonatomic, strong) NSMutableArray <WGGoodsViewRowModel *>*goodsViewRows;///<商品分类行展示
@end

/**
 商品详情页数据驱动
 */
@interface WGGoodsDetailViewModel : NSObject

@property (nonatomic, strong) WGGoodsInfoModel *goodsInfo;///<商品
//@property (nonatomic ,strong) ZLJTicketCenterInfoModel *thirdGoods;///淘宝商品

@property (nonatomic, strong) WGGoodsBottomActionModel *goodsBottomAction;///<底部按钮
@property (nonatomic, assign) WGGoodsType goodsType;///<商品类型
@property (nonatomic, strong) NSMutableArray <WGGoodsViewSetionModel *>*goodsViewSections;///<商品显示分组

/**
 添加拼团中的(拼团和优惠券都有的话，先添加优惠券)
 */
- (void)addGrouping;
/**
 添加优惠券(拼团和优惠券都有的话，先添加优惠券)
 */
- (void)addDiscounts;


@end

NS_ASSUME_NONNULL_END
