//
//  WGGoodsInfoModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/9.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGGoodsInfoModel.h"


@implementation WGGoodsInfoModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"specialPeople": @"newPeople",@"specialPeoplePrice":@"newPeoplePrice",@"monthSales":@[@"monthSales",@"sales"]};
}
+(NSDictionary *)mj_objectClassInArray{
    
    return @{@"specSku":[SpecSku class],@"specList":[SpecList class],@"paths":[Paths class]};
}
- (NSInteger)goodsCount{
    if (_goodsCount == 0) {
        return 1;
    }
    return _goodsCount;
}
- (NSArray *)specSkuSort{
    if (!_specSkuSort) {
        NSMutableArray *temp = [NSMutableArray new];
        ///遍历原始sku数据
        for (SpecSku *tempSku in _specSku) {
            ///遍历原始规格数组
            for (int i = 0; i < self.specListSort.count; i ++) {
                SpecList *temSpec = _specListSort[i];
                for (NSString *temStr in tempSku.specQC) {
                    if ([temSpec.specValue containsObject:temStr]) {
                        [tempSku.specSort addObject:temStr];
                        continue;
                    }
                }
            }
            [temp addObject:tempSku];
        }
        _specSkuSort = temp;
    }
    return _specSkuSort;
}
- (NSArray *)specListSort{
    if (!_specListSort) {
        NSMutableArray *temp = [NSMutableArray new];
        for (int i = 0; i < _specList.count; i ++) {
            SpecList *temSpec = _specList[i];
            if (temSpec.specValueList.count > 0) {
                [temp addObject:temSpec];
            }
        }
        _specListSort = temp;
    }
    return _specListSort;
}
@end

@implementation Paths

@end

@implementation SpecSku

- (NSMutableArray *)specSort{
    if (!_specSort) {
        _specSort = [NSMutableArray new];
        
    }
    return _specSort;
}

- (NSArray *)specQC{
    if (!_specQC) {
        NSMutableDictionary *tem = [NSMutableDictionary new];
        for (NSString *va in _spec) {
            [tem setValue:va forKey:va];
        }
        _specQC = [[tem allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        
    }
    return _specQC;
}
@end

@implementation SpecList

+(NSDictionary *)mj_objectClassInArray{
    
    return @{@"specValueList":[SpecValueList class]};
}
- (NSArray *)specValue{
    if (!_specValue) {
        NSMutableDictionary *tem = [NSMutableDictionary new];
        for (SpecValueList *va in _specValueList) {
            [tem setValue:va.specValue forKey:va.specValue];
        }
        _specValue = [[tem allValues] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    return _specValue;
}

@end

@implementation SpecValueList

@end
