//
//  ZLJItemsManageModel.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJItemsManageModel.h"

@implementation ItemsManageItemList
@end


@implementation ZLJItemsManageModel

+(NSDictionary *)mj_objectClassInArray{
    return @{@"data":[ItemsManageItemList class]};
}
@end
