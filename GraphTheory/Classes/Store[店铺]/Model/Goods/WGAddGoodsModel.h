//
//  WGAddGoodsModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTTextInputLimitManager.h"
#import "ZLJFindItemsGroupModel.h"
#import "WGGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

/**
 图片
 */
@interface WGGoodsImageModel : DDBaseModel
@property (nonatomic , strong) UIImage              * thumbnail;///<缩略图
@property (nonatomic , strong) UIImage              * original;///<原图
@property (nonatomic , strong) NSString              * imagePath;///<图片地址
@property (nonatomic , assign) NSInteger              type;///<图片类型
@property (nonatomic , strong) NSString              * title;///<图片标题
@property (nonatomic , strong) NSString              * des;///<图片描述
@property (nonatomic , strong) UIImage              * defalutImage;///<默认图

@end

/**
 请求到的规格
 */
@interface WGGoodsSpecModel : DDBaseModel
@property (nonatomic , strong) NSString              * Id;///<规格id
@property (nonatomic , copy) NSString              * spec_name;///<规格名
@property (nonatomic , copy) NSString              * text;///<规格名
@end

/**
 规格详细
 */
@interface WGAddGoodsSpecDetailModel : NSObject

@property (nonatomic , strong) NSString              * key;///<上传时用的key
@property (nonatomic , strong) NSString              * spes;///</<值
@property (nonatomic , strong) NSString              * name;///</<规格名
@property (nonatomic , strong) NSString              * placeholder;///</<规格名
@property (nonatomic , strong) NSString              * tip;///<提示

@property (nonatomic , strong) WGGoodsImageModel               * image;///<商品图片
@property (nonatomic , assign) BOOL               canEditor;///<是否可以编辑
@property (nonatomic , assign) BOOL               isMust;///<是否必填

@property (nonatomic , assign) LTTextLimitInputType              inputType;///<输入框限制类型
@property (nonatomic, assign) LTTextLimitType textLimitType;       // 输入框大小限制类型
@property (nonatomic, assign) NSInteger textLimitSize;          // 字符长度
@property (nonatomic , assign) UIKeyboardType              keyBoardType;///<键盘


@end

/**
 规格
 */
@interface WGAddGoodsSpecModel : NSObject

@property (nonatomic , strong) NSMutableArray             <WGAddGoodsSpecDetailModel *>* specDetails;///<详细规格
- (void)addDetaiSpes:(NSArray *)details;

@end
/**
 添加商品
 */
@interface WGAddGoodsModel : NSObject

@property (nonatomic , strong) NSString              * dataform;///<
@property (nonatomic , strong) NSString              * categoryId;///<分类id
@property (nonatomic , strong) NSString              * itemsName;///<商品名称
@property (nonatomic , strong) NSString              * itemsImages;///<商品图片,多个以,拼接
@property (nonatomic , strong) NSString              * itemsGroup;///<商品分组
@property (nonatomic , strong) NSMutableArray              * specId;///<规格名id,list的形式传递
@property (nonatomic , strong) NSString              * priceMin;///<取规格价的最低价
@property (nonatomic , strong) NSString              * oriPrice;///<划线价格
@property (nonatomic , strong) NSMutableArray              * spec;///<商品规格,传递过的是list形式[{"spec1":"用户填写的商品规格名","spec2":"222","specImage":"www.hhhh.com","inventory":"100","price":"50","skuCode":"用户自己填写"}]
@property (nonatomic , strong) NSString              * itemsInfo;///<商品详情
@property (nonatomic , strong) NSString              * spuId;///<spuId

@property (nonatomic , strong) NSString              * distributionType;///<配送方式,多个配送方式以,分割拼接
@property (nonatomic , assign) NSInteger              states;///<上架状态,还是仓库中状态0:保存1：上架

/*********本地用***********/
@property (nonatomic , strong) NSMutableArray         <WGAddGoodsSpecModel *> *spesModelArr;///<规格数组
@property (nonatomic , strong) NSMutableArray         <WGGoodsImageModel *> *goodsImageArr;///<图片数组
@property (nonatomic , strong) WGStoreCategoryModel         *categoryModel;///<分类
@property (nonatomic , strong) FindItemsGroupDataItem         *groupModel;///<商品分组
@property (nonatomic , strong) NSMutableArray         <WGGoodsSpecModel *> *spesArr;///<选中的规格
@property (nonatomic , strong) NSString         *specStr;///<选中的规格格式化字符串
@property (nonatomic , strong) NSArray *shioModels;///<配送方式

///商品详情转添加商品
+ (WGAddGoodsModel *)formattingGoodsModelFromDetail:(WGGoodsModel *)goodsDetail;

@end

NS_ASSUME_NONNULL_END
