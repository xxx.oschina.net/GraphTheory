
//
//  WGAddGoodsModel.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGAddGoodsModel.h"

@implementation WGGoodsImageModel

@end

@implementation WGGoodsSpecModel

//+(NSDictionary *)mj_replacedKeyFromPropertyName{
//    return @{@"kId":@"id"};
//}

@end

@implementation WGAddGoodsSpecDetailModel
- (BOOL)isMust{
    if ([_name containsString:@"*"]) {
        return YES;
    }
    return NO;
}
- (WGGoodsImageModel *)image{
    if (!_image) {
        _image = [WGGoodsImageModel new];
    }
    return _image;
}
@end

@implementation WGAddGoodsSpecModel

- (NSMutableArray *)specDetails{
    if (!_specDetails) {
        _specDetails = [NSMutableArray new];
        WGAddGoodsSpecDetailModel *price = [WGAddGoodsSpecDetailModel new];
        price.name = @"价格*";
        price.key = @"price";
        price.placeholder = @"¥0.00";
        price.tip = @"请输入价格";
        price.inputType = LTTextLimitInputTypeFloatNumber;
        price.canEditor = YES;
        price.keyBoardType = UIKeyboardTypeDecimalPad;
        [_specDetails addObject:price];
        
        WGAddGoodsSpecDetailModel *inventory = [WGAddGoodsSpecDetailModel new];
        inventory.name = @"库存*";
        inventory.key = @"inventory";
        inventory.placeholder = @"请输入库存数量";
        inventory.inputType = LTTextLimitInputTypeNumber;
        inventory.textLimitType = LTTextLimitTypeLength;
        inventory.textLimitSize = 4;
        inventory.canEditor = YES;
        inventory.keyBoardType = UIKeyboardTypePhonePad;
        [_specDetails addObject:inventory];
        
        WGAddGoodsSpecDetailModel *specImage = [WGAddGoodsSpecDetailModel new];
        specImage.name = @"图片";
        specImage.key = @"specImage";
        specImage.placeholder = @"上传一张图片";
        specImage.inputType = LTTextLimitInputTypeFloatNumber;
        specImage.canEditor = NO;
        specImage.keyBoardType = UIKeyboardTypePhonePad;
        [_specDetails addObject:specImage];
    }
    return _specDetails;
}
- (void)addDetaiSpes:(NSArray *)details{
    for (int i = 0 ; i < details.count ; i ++) {
        WGGoodsSpecModel *title = details[i];
        WGAddGoodsSpecDetailModel *spes = [WGAddGoodsSpecDetailModel new];
        spes.name = [NSString stringWithFormat:@"%@*",title.spec_name];
        spes.key = [NSString stringWithFormat:@"spec%d",i + 1];
        spes.placeholder = [NSString stringWithFormat:@"请输入%@",title.spec_name];
        spes.inputType = LTTextLimitInputTypeNone;
        spes.textLimitType = LTTextLimitTypeLength;
        spes.textLimitSize = 18;

        spes.keyBoardType = UIKeyboardTypeDefault;
        spes.canEditor = YES;
        [self.specDetails insertObject:spes atIndex:i];
    }
}
@end

@implementation WGAddGoodsModel

+ (NSArray *)mj_ignoredPropertyNames{
    return @[@"spesModelArr",@"goodsImageArr",@"categoryModel",@"groupModel",@"spesArr",@"specStr"];
}
//- (NSString *)itemsInfo{
//    return @"测试";
//}
- (NSString *)oriPrice{
    if (!_oriPrice) {
        _oriPrice = @"";
    }
    return _oriPrice;
}
- (NSString *)itemsImages{
    NSMutableArray *tem = [NSMutableArray new];
    for (WGGoodsImageModel *model in self.goodsImageArr) {
        if (model.imagePath) {
            [tem addObject:model.imagePath];
        }
    }
    return [tem componentsJoinedByString:@","];
}
- (NSMutableArray *)spec{
    CGFloat minPrice = MAXFLOAT;
    NSMutableArray *temArr = [NSMutableArray new];
    for (WGAddGoodsSpecModel *temSpes in self.spesModelArr) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        for (WGAddGoodsSpecDetailModel *temDetail in temSpes.specDetails) {
            if (temDetail.image.imagePath) {
                [dic setValue:temDetail.image.imagePath forKey:temDetail.key];
            }else{
                [dic setValue:temDetail.spes forKey:temDetail.key];
            }
            if ([temDetail.key isEqualToString:@"price"]) {
                if (temDetail.spes.floatValue < minPrice) {
                    minPrice = temDetail.spes.floatValue;
                }
            }
        }
        [temArr addObject:dic];
    }
    _priceMin = [NSString stringWithFormat:@"%.2f",minPrice];
    return temArr;
}
- (NSMutableArray *)spesModelArr{
    if (!_spesModelArr) {
        _spesModelArr = [NSMutableArray new];
    }
    return _spesModelArr;
}
- (NSMutableArray *)goodsImageArr{
    if (!_goodsImageArr) {
        _goodsImageArr = [NSMutableArray new];
    }
    return _goodsImageArr;
}
- (NSMutableArray *)spesArr{
    if (!_spesArr) {
        _spesArr = [NSMutableArray new];
    }
    return _spesArr;
}

- (NSString *)categoryId{
    return _categoryModel.category_id;
}
- (NSString *)itemsGroup{
    return _groupModel.kId;
}
- (NSString *)specStr{
    NSMutableArray *temStr = [NSMutableArray new];
    NSMutableArray *temID = [NSMutableArray new];

    for (WGGoodsSpecModel *spe in _spesArr) {
        if (spe.Id) {
            [temID addObject:spe.Id];
        }
        if (spe.spec_name) {
            [temStr addObject:spe.spec_name];
        }
    }
    _specId = temID;
    return [temStr componentsJoinedByString:@"+"];
}

+ (WGAddGoodsModel *)formattingGoodsModelFromDetail:(WGGoodsModel *)goodsDetail{
    WGAddGoodsModel *addModel = [WGAddGoodsModel new];
    addModel.spuId = goodsDetail.spuDetail.Id;
    NSArray *headImags = [goodsDetail.spu.images componentsSeparatedByString:@","];
    //图片
    for (NSString *str in headImags) {
        WGGoodsImageModel *temImage = [WGGoodsImageModel new];
        temImage.imagePath = str;
        [addModel.goodsImageArr addObject:temImage];
    }
    if (goodsDetail.skuList.count) {
        //商品名
        addModel.itemsName = goodsDetail.skuList[0].skuName;
        //商品分类
        addModel.categoryId = goodsDetail.spu.category_id;

    }
    WGStoreCategoryModel *category = [WGStoreCategoryModel new];
    category.category_id = goodsDetail.spu.category_id;
    category.category_name = goodsDetail.spu.categoryName;
    addModel.categoryModel = category;
    addModel.distributionType = goodsDetail.spu.distributionType;
    addModel.groupModel = goodsDetail.itemGroupMsg;
    addModel.itemsInfo = goodsDetail.spuDetail.itemsDetails;
    NSMutableArray *skuArr = [NSMutableArray new];

    for (int i = 0; i < goodsDetail.skuList.count; i++) {
        SkuList *skuList = goodsDetail.skuList[i];
        WGAddGoodsSpecModel *temSpesDetai = [WGAddGoodsSpecModel new];
        temSpesDetai.specDetails = [NSMutableArray new];
        for (int j = 0; j < skuList.specMsg.count; j++) {
            SpecMsg *specMsg = skuList.specMsg[j];
            WGGoodsSpecModel *spec = [WGGoodsSpecModel new];
            spec.Id = specMsg.spec_id;
            spec.spec_name = specMsg.spec_name;
            if (i == 0) {
                [addModel.spesArr addObject:spec];
            }
            WGAddGoodsSpecDetailModel *spesDetai
            = [WGAddGoodsSpecDetailModel new];
            spesDetai.name = [NSString stringWithFormat:@"%@*",spec.spec_name];
            spesDetai.key = [NSString stringWithFormat:@"spec%d",j + 1];
            spesDetai.spes = specMsg.spec_value;
            spesDetai.placeholder = [NSString stringWithFormat:@"请输入%@",spec.spec_name];
            spesDetai.inputType = LTTextLimitInputTypeNone;
            spesDetai.textLimitType = LTTextLimitTypeLength;
            spesDetai.textLimitSize = 18;
            
            spesDetai.keyBoardType = UIKeyboardTypeDefault;
            spesDetai.canEditor = YES;
            [temSpesDetai.specDetails addObject:spesDetai];
        }
        WGAddGoodsSpecDetailModel *price = [WGAddGoodsSpecDetailModel new];
        price.name = @"价格*";
        price.key = @"price";
        price.spes = skuList.price;
        price.placeholder = @"¥0.00";
        price.tip = @"请输入价格";
        price.inputType = LTTextLimitInputTypeFloatNumber;
        price.canEditor = YES;
        price.keyBoardType = UIKeyboardTypeDecimalPad;
        [temSpesDetai.specDetails addObject:price];
        
        WGAddGoodsSpecDetailModel *inventory = [WGAddGoodsSpecDetailModel new];
        inventory.name = @"库存*";
        inventory.key = @"inventory";
        inventory.spes = skuList.itemStock;
        inventory.placeholder = @"请输入库存数量";
        inventory.inputType = LTTextLimitInputTypeNumber;
        inventory.textLimitType = LTTextLimitTypeLength;
        inventory.textLimitSize = 4;
        inventory.canEditor = YES;
        inventory.keyBoardType = UIKeyboardTypePhonePad;
        [temSpesDetai.specDetails addObject:inventory];
        
        WGAddGoodsSpecDetailModel *specImage = [WGAddGoodsSpecDetailModel new];
        specImage.name = @"图片";
        specImage.key = @"specImage";
        specImage.placeholder = @"上传一张图片";
        specImage.inputType = LTTextLimitInputTypeFloatNumber;
        specImage.canEditor = NO;
        specImage.keyBoardType = UIKeyboardTypePhonePad;
        specImage.image = [WGGoodsImageModel new];
        if (skuList.imagePath) {
            specImage.image.imagePath = skuList.imagePath;
        }
        [temSpesDetai.specDetails addObject:specImage];
        [skuArr addObject:temSpesDetai];
    }
    addModel.spesModelArr = skuArr;
    return addModel;
}

@end
