//
//  ZLJItemsManageModel.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ItemsManageItemList :NSObject
@property (nonatomic , copy) NSString              * distributionType;//配送方式:1快递,2到店消费,3上门服务,以逗号分隔
@property (nonatomic , copy) NSString              * itemId;//商品id
@property (nonatomic , assign) NSInteger              state;//商品状态 0:仓库中   1：在售中   2：已售尽   3：已删除
@property (nonatomic , assign) NSInteger              monthSales;//
@property (nonatomic , assign) NSInteger              browseNum;//
@property (nonatomic , assign) NSInteger              visitorNum;//
@property (nonatomic , copy) NSString              * itemName;//商品名
@property (nonatomic , copy) NSString              * stateName;//商品状态
@property (nonatomic , copy) NSString             * totalSales;//销量
@property (nonatomic , copy) NSString              * createTime;//创建时间
@property (nonatomic , copy) NSString              * skuSource;//sku来源
@property (nonatomic , copy) NSString              * price;//售价
@property (nonatomic , copy) NSString              * spuId;//spu_id
@property (nonatomic , copy) NSString              * shopName;//店铺名
@property (nonatomic , copy) NSString              * imagePath;//图片地址
@property (nonatomic , copy) NSString              * itemInfo;//图片地址
@property (nonatomic , copy) NSString              * itemStock;//库存

@property (nonatomic , copy) NSString              * source;//


@property (nonatomic , assign) BOOL              selected;///选中状态

@property (nonatomic , assign) NSInteger               marketIsLock;//1：未锁定 2：锁定

@end


@interface ZLJItemsManageModel :NSObject
@property (nonatomic , assign) NSInteger              pageSize;//<#class#>
@property (nonatomic , copy) NSString              * time;//<#class#>
@property (nonatomic , strong) NSArray <ItemsManageItemList *>              * data;//<#class#>
@property (nonatomic , assign) NSInteger              code;//<#class#>
@property (nonatomic , assign) NSInteger              pageCount;//<#class#>
@property (nonatomic , copy) NSString              * msg;//<#class#>
@property (nonatomic , assign) NSInteger              total;//<#class#>
@property (nonatomic , assign) BOOL              state;//<#class#>
@property (nonatomic , assign) NSInteger              pageNum;//<#class#>

@end

NS_ASSUME_NONNULL_END
