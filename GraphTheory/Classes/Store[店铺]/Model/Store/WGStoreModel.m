//
//  WGStoreModel.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreModel.h"
#import "WGAddGoodsModel.h"
#import "YXMerchantInfoModel.h"

@implementation WGStoreDetailNumModel

@end

@implementation WGStoreCategoryModel

MJCodingImplementation

+(NSDictionary *)mj_objectClassInArray{
    return @{@"children":[WGStoreCategoryModel class]};
}

@end

@implementation WGStoreShipModel


+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"Id":@"id"};
}
@end

@implementation WGStoreOpenModel

MJCodingImplementation

@end

@implementation WGStoreTimeModel
- (id)copyWithZone:(NSZone *)zone{
    return  [self lxz_copyWithZone:zone];
}

MJCodingImplementation
- (NSMutableArray *)shipWeek{
    if (!_shipWeek) {
        _shipWeek = [NSMutableArray new];
    }
    return _shipWeek;
}
- (NSMutableArray *)shipTime{
    if (!_shipTime) {
        _shipTime = [NSMutableArray new];
    }
    return _shipTime;
}
@end

@implementation WGStoreModel

- (id)copyWithZone:(NSZone *)zone{
    return  [self lxz_copyWithZone:zone];
}

MJExtensionCodingImplementation

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"kId":@"id"};
}
+ (NSArray *)mj_ignoredPropertyNames{
    return @[@"storeDoorTime",@"storeTime",@"storeImage",@"cardFrontImage",@"cardReverseImage",@"otherPermitImage",@"businessLicenseImage",@"healthPermitImage",@"shioModels",@"categorys",@"picArr"];
}
//- (NSString *)distributionType{
//    NSMutableArray *temShip = [NSMutableArray new];
//    for (WGStoreShipModel *ship in _shioModels) {
//        [temShip addObject:ship.Id];
//    }
//    return [temShip componentsJoinedByString:@","];
//}
//- (NSString *)mainCategory{
//    NSMutableArray *tem = [NSMutableArray new];
//    for (WGStoreCategoryModel *model in _categorys) {
//        [tem addObject:model.serviceId];
//    }
//    return [tem componentsJoinedByString:@";"];
//}
//- (NSString *)mainCategoryName{
//    NSMutableArray *tem = [NSMutableArray new];
//    for (WGStoreCategoryModel *model in _categorys) {
//        [tem addObject:model.serviceName];
//    }
//    return [tem componentsJoinedByString:@";"];
//}
- (NSString *)cardFront{
    WGGoodsImageModel *tem = _picArr[0][0];
    _cardFront = tem.imagePath?:_cardFront;
    return _cardFront;
}
- (NSString *)cardReverse{
    WGGoodsImageModel *tem = _picArr[0][1];
    _cardReverse = tem.imagePath?:_cardReverse;
    return _cardReverse;
}
- (NSString *)businessLicense{
    WGGoodsImageModel *tem = _picArr[1][0];
    _businessLicense = tem.imagePath?:_businessLicense;
    return _businessLicense;
}
//- (NSString *)healthPermit{
//    WGGoodsImageModel *tem = _picArr[2][0];
//    _healthPermit = tem.imagePath?:_healthPermit;
//    return _healthPermit;
//}
//- (NSString *)otherPermit{
//    WGGoodsImageModel *tem = _picArr[3][0];
//    _otherPermit = tem.imagePath?:_otherPermit;
//    return _otherPermit;
//}
- (NSMutableArray *)picArr{
    if (!_picArr) {
        _picArr = [NSMutableArray new];
        WGGoodsImageModel *image1 = [WGGoodsImageModel new];
        image1.title = @"上传身份证正面照片";
        image1.type = 0;
        image1.defalutImage = [UIImage imageNamed:@"shop_icon_sfzFront"];
        WGGoodsImageModel *image2 = [WGGoodsImageModel new];
        image2.title = @"上传身份证反面照片";
        image2.type = 1;
        image2.defalutImage = [UIImage imageNamed:@"shop_icon_sfzback"];
        
        [_picArr addObject:@[image1,image2]];
        WGGoodsImageModel *image3 = [WGGoodsImageModel new];
        image3.title = @"上传营业执照";
        image3.type = 2;
        image3.defalutImage = [UIImage imageNamed:@"shop_icon_yyzz"];
        [_picArr addObject:@[image3]];
//        WGGoodsImageModel *image4 = [WGGoodsImageModel new];
//        image4.title = @"上传特殊资质证明";
//        image4.des = @"请提供《食品流通许可证》或《食品经营许可证》\r或《食品生产许可证》";
//        image4.type = 3;
//        image4.defalutImage = [UIImage imageNamed:@"shop_icon_tszz"];
//        [_picArr addObject:@[image4]];
//        WGGoodsImageModel *image5 = [WGGoodsImageModel new];
//        image5.title = @"上传特殊资质证明";
//        image5.des = @"请提供《特种行业经营许可证》";
//        image5.type = 4;
//        image5.defalutImage = [UIImage imageNamed:@"shop_icon_tzhy"];
//        [_picArr addObject:@[image5]];
    }
    return _picArr;
}
- (WGStoreTimeModel *)storeTime{
    if (!_storeTime) {
        _storeTime = [WGStoreTimeModel new];
        if (_businessWeek) {
            if ([_businessWeek isEqualToString:@"周一到周日"]) {
                _storeTime.shipTime = [[NSMutableArray alloc]initWithArray:@[@"周一",@"周二",@"周三",@"周四",@"周五",@"周六",@"周日"]];
            }else{
                _storeTime.shipWeek = [[NSMutableArray alloc]initWithArray:[_businessWeek componentsSeparatedByString:@","]];
            }
        }
        if (_businessHours) {
            NSArray *temHours = [_businessHours componentsSeparatedByString:@","];
            NSMutableArray *temh = [NSMutableArray new];
            for (NSString *temStr in temHours) {
                NSArray *hours = [temStr componentsSeparatedByString:@"-"];
                WGStoreOpenModel *open = [WGStoreOpenModel new];
                if (hours.count > 0) {
                    open.beginTime = hours[0];
                }
                if (hours.count > 1) {
                    open.endTime = hours[1];
                }
                [temh addObject:open];
            }
            _storeTime.shipTime = temh;
        }
    }
    return _storeTime;
}
- (NSString *)businessWeek{
    if (_storeTime) {
        _businessWeek = [_storeTime.shipWeek componentsJoinedByString:@","];
    }
    return _businessWeek;
}
- (NSString *)businessHours{
    if (_storeTime) {
        NSMutableArray *tem = [NSMutableArray new];
        for (WGStoreOpenModel *model in _storeTime.shipTime) {
            if (model.beginTime.length > 0 && model.endTime > 0) {
                NSString *str = [NSString stringWithFormat:@"%@-%@",model.beginTime,model.endTime];
                [tem addObject:str];
            }
        }
        _businessHours = [tem componentsJoinedByString:@","];
    }
    return _businessHours;
}
- (NSString *)yingyeTime{
    NSMutableString *time = [NSMutableString new];
    if (self.businessWeek.length) {
        [time appendString:self.businessWeek];
    }
    if (self.businessHours) {
        NSString *str = [self.businessHours stringByReplacingOccurrencesOfString:@"," withString:@"\n"];
        [time appendString:[NSString stringWithFormat:@"\n%@",str]];
    }
    return time;
    
}

@end



@implementation DDStoreDataModel

- (void)setMerchant:(NSDictionary *)merchant {
    _merchant = merchant;
    if (_merchant) {
        _merchantInfoModel = [YXMerchantInfoModel mj_objectWithKeyValues:_merchant];
    }
}

- (void)setShop:(NSDictionary *)shop {
    _shop = shop;
    if (_shop) {
        _storeModel = [WGStoreModel mj_objectWithKeyValues:_shop];
    }
}

- (void)setShopVice:(NSDictionary *)shopVice {
    _shopVice = shopVice;
    if (_shopVice) {
        _shopViceModel = [DDShopViceModel mj_objectWithKeyValues:_shopVice];
    }
}

@end


@implementation DDShopViceModel

@end
