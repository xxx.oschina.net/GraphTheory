//
//  WGStoreModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@interface WGStoreDetailNumModel : NSObject
@property (nonatomic ,copy) NSString *dfkNumber;///<待付款
@property (nonatomic ,copy) NSString *dfhNumber;///<待发货
@property (nonatomic ,copy) NSString *dfwNumber;///<待服务
@property (nonatomic ,copy) NSString *dshNumber;///<待售后
@property (nonatomic ,copy) NSString *payMoney;///<待服务
@property (nonatomic ,copy) NSString *nowOrderNumber;///<订单数量
@property (nonatomic ,copy) NSString *customerPrice;///<客单价
@property (nonatomic ,copy) NSString *buyersNumber;///<买家数量
@property (nonatomic ,copy) NSString *visitorsNumber;///<访客数量
@property (nonatomic ,copy) NSString *conversionRate;///<转化率
@property (nonatomic ,copy) NSString *quantityOrderNumber;///<下单件数
@property (nonatomic ,copy) NSString *payPackNumber;///<支付件数

@end


@interface WGStoreCategoryModel : NSObject

@property (nonatomic ,copy) NSString *level;///<分类级别
@property (nonatomic ,copy) NSString *category_id;///<分类编号
@property (nonatomic ,copy) NSString *category_name;///<分类级别
@property (nonatomic ,copy) NSArray *children;///<下级
@property (nonatomic ,copy) NSString *value;///<分类级别
@property (nonatomic ,copy) NSString *key;///<分类编号
@property (nonatomic ,copy) NSString *title;///<分类级别

@property (nonatomic ,copy) NSString *serviceId;///<服务器用的id
@property (nonatomic ,copy) NSString *serviceName;///<服务器用的名字


@property (nonatomic ,assign) BOOL isSelect;///<分类级别

@end

@interface WGStoreShipModel : NSObject

@property (nonatomic ,copy) NSString *Id;///<配送方式id
@property (nonatomic ,copy) NSString *text;///<配送方式

@end

@interface WGStoreOpenModel : NSObject

@property (nonatomic ,copy) NSString *beginTime;///<开始时间
@property (nonatomic ,copy) NSString *endTime;///<结束时间
@end


@interface WGStoreTimeModel : NSObject<NSCopying>

@property (nonatomic ,copy) NSMutableArray *shipWeek;///<上门时间星期
@property (nonatomic ,copy) NSMutableArray *shipTime;///<上门时间

@end

@interface WGStoreModel : NSObject<NSCopying>

@property (nonatomic ,strong) NSString *relationId;///<商家id
@property (nonatomic ,strong) NSString *shopId; ///店铺id
@property (nonatomic ,strong) NSString *shopName;///<店铺名称
@property (nonatomic ,strong) NSString *mainCategory;///<主营类(两级选择),分类表下一级类目id和二级类目id,以逗号进行隔开
@property (nonatomic ,strong) NSString *mainCategoryName;///<主营类(两级选择)名称,以斜线隔开
@property (nonatomic ,strong) NSString *distributionType;///<<配送方式:1快递,2到店消费,3上门服务,以逗号分隔
@property (nonatomic ,strong) NSString *shopAddress;///<店铺地址
@property (nonatomic ,strong) NSString *longitude;///<店铺经纬度
@property (nonatomic ,strong) NSString *serviceMobile;///<服务电话
@property (nonatomic ,strong) NSString *businessWeek;///<店铺营业周期
@property (nonatomic ,strong) NSString *businessHours;///<店铺营业时间
@property (nonatomic ,strong) NSString *serviceScope;///<上门服务范围(公里/km)
@property (nonatomic ,strong) NSString *storeDeliveryArea;///<快递配送区域id
@property (nonatomic ,strong) NSString *shopIntroduce;///<店铺简介
@property (nonatomic ,assign) NSInteger whetherNationalArea;///<0:全国配送;1:不是全国配送
@property (nonatomic ,strong) NSString *areaData;///<配送区域id和名称的集合(areaId:选中的所在地区最后一级的主键;areaName:选中的所在地区最后一级的fullName的值;全国配送时areaData不用传)的集合 ( [{"areaId":"12-972","areaName":"江苏-镇江市"},{"areaId":"12-972","areaName":"江苏-镇江市"}])
/************* 图片 ****************/
@property (nonatomic ,strong) UIImage *storeImage;///<店铺图片
@property (nonatomic ,strong) NSString *shopLogo;///<店铺图片地址
@property (nonatomic ,strong) NSString *ossId;///<店铺图片附件id

@property (nonatomic , copy) NSString              * cardFront;///<身份证正面附件地址
@property (nonatomic , copy) NSString              * cardReverse;///<身份证反面附件地址
@property (nonatomic , copy) NSString              * businessLicense;///<营业执照附件地址
@property (nonatomic , copy) NSString              * otherPermit;///<其他许可证附件地址
@property (nonatomic , copy) NSString              * healthPermit;///<卫生许可证附件地址



/************* 本地用 ****************/
@property (nonatomic ,strong) WGStoreTimeModel *storeDoorTime;///<上门时间
@property (nonatomic ,strong) WGStoreTimeModel *storeTime;///<营业时间
@property (nonatomic ,strong) NSArray *shioModels;///<配送方式
@property (nonatomic ,strong) NSArray *categorys;///<主营类目

@property (nonatomic ,strong) NSString *yingyeTime;///<营业时间
@property (nonatomic ,strong) NSMutableArray *picArr;///<营业时间

/************* 本地用 ****************/

@property (nonatomic , copy) NSString              * createPerson;//创建人
//@property (nonatomic , assign) NSInteger              active;//1:正常;0:锁定;-1:删除
//@property (nonatomic , copy) NSString              * updatePerson;//最后更新人
@property (nonatomic , copy) NSString              * deliveryArea;//快递配送区域id,取地址表下的areaId,多个以逗号隔开
//@property (nonatomic , copy) NSString              * updateTime;//最后更新时间
@property (nonatomic , copy) NSString              * kId;//主键id -- 店铺id
@property (nonatomic , copy) NSString              * createTime;//创建时间
@property (nonatomic , copy) NSString              * remark;//备注


@property (nonatomic , assign) NSInteger              subjectType;///<主体类型 1:个人认证 2:企业认证
@property (nonatomic , copy) NSString              * province;///<店铺地址:省
@property (nonatomic , copy) NSString              * city;///<店铺地址:市
@property (nonatomic , copy) NSString              * county;///<店铺地址:县

@property (nonatomic , assign) NSInteger              shopType;///<店铺类型 1:线上商场 2:本地商家
@property (nonatomic , assign) NSInteger              distPlatCommon;///<店铺级别 1:普通店铺 2平台级店铺


@end



@class YXMerchantInfoModel, DDShopViceModel;
@interface DDStoreDataModel :  NSObject

@property (nonatomic, strong) NSDictionary *merchant;
@property (nonatomic, strong) NSDictionary *shop;
@property (nonatomic, strong) NSDictionary *shopVice;

@property (nonatomic, strong) YXMerchantInfoModel *merchantInfoModel; //商家信息
@property (nonatomic, strong) WGStoreModel *storeModel; //店铺信息
@property (nonatomic, strong) DDShopViceModel *shopViceModel; //认证信息

@end


@interface DDShopViceModel :  NSObject

@property (nonatomic, copy) NSString *Id;                    //主键id
@property (nonatomic, copy) NSString *businessLicense;       //营业执照附件地址
@property (nonatomic, copy) NSString *cardFront;             //身份证正面
@property (nonatomic, copy) NSString *cardReverse;           //身份证反面
@property (nonatomic, copy) NSString *healthPermit;          //卫生许可证附件地址
@property (nonatomic, copy) NSString *otherPermit;           //其他许可证附件地址

@end


NS_ASSUME_NONNULL_END
