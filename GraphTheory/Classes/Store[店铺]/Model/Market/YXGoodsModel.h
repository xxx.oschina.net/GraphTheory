//
//  YXGoodsModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/7.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@class YXSkuModel;
@interface YXGoodsModel : DDBaseModel

/** 商品id*/
@property (nonatomic ,copy) NSString *itemId;
/** 商品名称*/
@property (nonatomic ,copy) NSString *itemInfo;
/** 商品信息json字符串*/
@property (nonatomic ,copy) NSString *items_info;
/** 商品库存*/
@property (nonatomic ,assign) NSInteger itemStock;
/** 商品图片地址*/
@property (nonatomic ,copy) NSString *imagePath;
/** 原价*/
@property (nonatomic ,assign) CGFloat oriPrice;
/** 价格*/
@property (nonatomic ,assign) CGFloat price;
/** 销售*/
@property (nonatomic ,assign) NSInteger sales;
/** 活动价格*/
@property (nonatomic ,assign) CGFloat activityPrice;
/** 店铺名称*/
@property (nonatomic ,copy) NSString *shopName;
/** 商品来源 01：京东 02：自定义商品*/
@property (nonatomic ,copy) NSString *source;
/** spuID*/
@property (nonatomic ,copy) NSString *spuId;
/** 商品是否报名*/
@property (nonatomic ,assign) BOOL state;
/** 活动名称*/
@property (nonatomic ,copy) NSString *activityName;
/** 消费类型*/
@property (nonatomic ,copy) NSString *distributionType;
/** auditState:1待审核 2通过 3拒绝*/
@property (nonatomic ,assign) NSInteger auditState;
/** 是否选中*/
@property (nonatomic ,assign) BOOL isSelected;
/** 是否编辑价格*/
@property (nonatomic ,assign) BOOL isEditor;
/** sku数组*/
@property (nonatomic ,copy) NSArray <YXSkuModel *> *skuList;



@end


@interface YXSkuModel : DDBaseModel

/** 商品来源 01：京东 02：自定义商品*/
@property (nonatomic ,copy) NSString *source;
/** spuID*/
@property (nonatomic ,copy) NSString *spuId;
/** skuId*/
@property (nonatomic ,copy) NSString *skuId;
/** sku名称*/
@property (nonatomic ,copy) NSString *skuName;
/** 商品库存*/
@property (nonatomic ,copy) NSString *specValue;
/** 价格*/
@property (nonatomic ,copy) NSString *price;
/** 原价*/
@property (nonatomic ,copy) NSString *oriPrice;
/** 进货价*/
@property (nonatomic ,copy) NSString *costPrice;
/** 编辑的现价*/
@property (nonatomic ,copy) NSString *temPrice;
/** 设置编辑的现价*/
@property (nonatomic ,copy) NSString *marketPrice;
/** 活动价格*/
@property (nonatomic ,assign) CGFloat activityPrice;
/** 新人专享sku现价*/
//@property (nonatomic ,copy) NSString *newPeoplePrice;
/** 商品库存*/
@property (nonatomic ,assign) NSInteger itemStock;
/** 商品库存*/
@property (nonatomic ,copy) NSString *itemVentes;
/** 是否编辑价格*/
@property (nonatomic ,assign) BOOL isEditor;
@end



NS_ASSUME_NONNULL_END
