//
//  YXActivityModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/9.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXActivityModel : DDBaseModel

/** 主键id*/
@property (nonatomic ,copy) NSString *Id;
/** 活动id*/
@property (nonatomic ,copy) NSString *activityId;
/** 活动名称*/
@property (nonatomic ,copy) NSString *activityName;
/** 活动类型*/
@property (nonatomic ,assign) NSInteger activityType;
/** 状态*/
@property (nonatomic ,assign) NSInteger state;
/** 日期*/
@property (nonatomic ,copy) NSString *qgday;
/** 星期*/
@property (nonatomic ,copy) NSString *xingqi;
/** 场次*/
@property (nonatomic ,copy) NSString *scene;
/** 活动开始时间*/
@property (nonatomic ,copy) NSString *startDate;
/** 活动结束时间*/
@property (nonatomic ,copy) NSString *endDate;
/** 报名开始时间*/
@property (nonatomic ,copy) NSString *signUpStartDate;
/** 报名结束时间*/
@property (nonatomic ,copy) NSString *signUpEndDate;
/** 活动详情*/
@property (nonatomic ,copy) NSString *hdxq;
/** 名称*/
@property (nonatomic ,copy) NSString *name;
/** 报名商品数量*/
@property (nonatomic ,assign) NSInteger num;
/** auditState:1待审核 2通过 3拒绝*/
@property (nonatomic ,copy) NSString *auditState;
/** 报名类型 1商品,2分组*/
@property (nonatomic ,copy) NSString *entrytype;
/** 是否全国 1全国,2指定小区*/
@property (nonatomic ,assign) NSInteger isAll;
/** 是否长期显示 1长期显示 2指定日期显示*/
@property (nonatomic ,assign) NSInteger longDisplay;
/** 分组id*/
@property (nonatomic ,copy) NSString *groupId;
/** 分组名称*/
@property (nonatomic ,copy) NSString *groupName;
/** 项目id*/
@property (nonatomic ,copy) NSString *houseId;
/** 项目名称*/
@property (nonatomic ,copy) NSString *houseName;

@property (nonatomic ,assign) NSInteger spuNum;

@end

NS_ASSUME_NONNULL_END
