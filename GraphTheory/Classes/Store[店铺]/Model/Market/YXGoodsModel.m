//
//  YXGoodsModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/7.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXGoodsModel.h"

@implementation YXGoodsModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"skuList" : @"YXSkuModel"};
}

@end

@implementation YXSkuModel

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"marketPrice":@"newPeoplePrice"};
}
@end
