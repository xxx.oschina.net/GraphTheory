//
//  WGAreaModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/12/11.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGAreaModel : DDBaseModel

@property (nonatomic , copy) NSString              * areaId;
@property (nonatomic , copy) NSString              * areaName;

@property (nonatomic , assign) BOOL              isSelect;
@property (nonatomic , assign) BOOL              isOpen;///<是否展开
@property (nonatomic , assign) BOOL              isZXS;///<是直辖市

@property (nonatomic , strong) NSMutableArray              * areaData;

@end

NS_ASSUME_NONNULL_END
