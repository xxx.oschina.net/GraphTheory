//
//  WGExpressModel.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/16.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WaybillCode :NSObject
@property (nonatomic , copy) NSString              * deliveryOrderId;
@property (nonatomic , assign) NSInteger              orderId;
@property (nonatomic , assign) NSInteger              parentId;
@property (nonatomic , copy) NSString              * carrier;

@end

@interface OrderTrack :NSObject
@property (nonatomic , copy) NSString              * time;
@property (nonatomic , copy) NSString              * status;
@property (nonatomic , assign) CGFloat height;
@end

@interface WGExpressModel :NSObject
@property (nonatomic , copy) NSArray<OrderTrack *>              * list;
@property (nonatomic , copy) NSArray<WGOderDetailModel *>              * item;
@property (nonatomic , strong) NSArray<OrderTrack *>              * listSort;
@property (nonatomic , copy) NSArray<WaybillCode *>              * waybillCode;

@property (nonatomic , copy) NSString              * deliveryTypeName;
@property (nonatomic , copy) NSString              * deliveryTypeId;
@property (nonatomic , copy) NSString              * expressNo;


@end

NS_ASSUME_NONNULL_END
