//
//  WGOrderModel.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  strongright © 2019年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Coding.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger ,OrderActionType){
    OrderActionTypeDeliver,///<发货
    OrderActionTypeNote,///<备注
    OrderActionTypeSelctClerk,///<选择接单人
    OrderActionTypeVerifyPayNO,///<确认消费码
    OrderActionTypeContinueDeliver,///<继续发货
    OrderActionTypeCancle,///<取消订单
    OrderActionTypeDistribution,///<查看物流
    OrderActionTypeRemindPay,///<提醒付款
    OrderActionTypeDone,///<完成订单
};

@interface WGOderActionModel :NSObject
@property (nonatomic , strong) NSString              * actionTitle;
@property (nonatomic , assign) OrderActionType         actionType;

@end

@interface WGOderDetailInfoModel :NSObject
@property (nonatomic , strong) NSString              * keyStr;
@property (nonatomic , strong) NSString              * valueStr;
@property (nonatomic , assign) BOOL              isCanCopy;

@end

@interface WGOderDetailModel :NSObject<NSCopying,NSMutableCopying>

@property (nonatomic , strong) NSString              * distributionType;///<1.快递 2.到店 3.上门
@property (nonatomic , strong) NSString              * itemPath;
@property (nonatomic , strong) NSString              * itemType;
@property (nonatomic , assign) NSInteger              itemSubtotal;///<商品小计
@property (nonatomic , strong) NSString              * itemPrice;
@property (nonatomic , strong) NSString              * itemDiscount;
@property (nonatomic , strong) NSString              * source;///<//来源 01京东 02自定义商品
@property (nonatomic , strong) NSString              * gid;
@property (nonatomic , strong) NSString              * itemNum;
@property (nonatomic , strong) NSString              * itemInfo;
@property (nonatomic , strong) NSString              * shopName;
@property (nonatomic , strong) NSString              * skuId;
@property (nonatomic , strong) NSString              * itemId;
@property (nonatomic , assign) BOOL               isEditing;




@end

@interface WGShopModel : NSObject<NSCopying,NSMutableCopying>

@property (nonatomic , copy) NSString              * userId;///<用户id
@property (nonatomic , strong) NSString              * remark;///<备注
@property (nonatomic , strong) NSString              * mainId;///<订单主表主键id
@property (nonatomic , strong) NSString              * mainDiscountPrice;///<订单主表店铺下的优惠金额
@property (nonatomic , assign) NSInteger              orderState;///< 订单状态 0待付款1待发货 2待服务3已发货4已完成5已关闭6售后、退款
@property (nonatomic , strong) NSMutableArray<WGOderDetailModel *>              * items;///<
@property (nonatomic , copy) NSString              * deliveryTypeName;///<快递名称
@property (nonatomic , copy) NSString              * deliveryTypeId;///<快递id
@property (nonatomic , copy) NSString              * receivedId;///<接单人id
@property (nonatomic , copy) NSString              * receivedName;///<接单人
@property (nonatomic , copy) NSString              * createTime;///<记录创建时间

@property (nonatomic , strong) NSString              * threeOrderId;///<第三方订单号
@property (nonatomic , strong) NSString              * shopId;///<商铺号
@property (nonatomic , strong) NSString              * mainItemTotal;///<订单主表店铺下商品总金额
@property (nonatomic , strong) NSString              * payMethod;///<支付方式:1余额 2微信 3支付宝
@property (nonatomic , strong) NSString              * shopName;///<店铺名
@property (nonatomic , strong) NSString              * mainSource;///<订单来源
@property (nonatomic , assign) NSInteger              state;///<支付状态 0失败1成功
@property (nonatomic , strong) NSString              * expressNo;///<快递单号
@property (nonatomic , strong) NSString              * orderNo;///<订单号
@property (nonatomic , strong) NSString              * tradeNo;///<交易单号
@property (nonatomic , strong) NSString              * systemNo;///<交易流水号
@property (nonatomic , strong) NSString              * dealTime;///<支付时间
@property (nonatomic , strong) NSString              * mobile;///<手机号
@property (nonatomic , strong) NSString              * addressId;///<收货地址id
@property (nonatomic , strong) NSString              * mainPayPrice;///<实付金额
@property (nonatomic , strong) NSString              * consumerCode;///<消费码
@property (nonatomic , strong) NSString              * mainDistributionType;///<配送方式1.快递 2.到店 3.上门;
@property (nonatomic , strong) NSString              * mainDistributionName;///<配送方式1.快递 2.到店 3.上门;

@property (nonatomic , strong) NSString              * userName;///<用户名
@property (nonatomic , strong) NSString              * orderTime;///<下单时间
@property (nonatomic , assign) NSInteger              evaluateState;
@property (nonatomic , strong) NSString              * whetherToOrder;///<订单是否接单: 0：未接单;1:已接单2:待消费
@property (nonatomic , strong) NSString              * address;///<收货地址
@property (nonatomic , strong) NSString              * mainFreight;///<运费
@property (nonatomic , strong) NSString              * itemNum;///<商品个数

@property (nonatomic , assign) NSInteger               operationState;///<是否可以发货0: 启用 1:禁用
@property (nonatomic , assign) NSInteger               orderType;///<订单类型0: 未拼团 1: 拼团中 2: 拼团成功 3: 拼团失败
@property (nonatomic , assign) NSInteger               exchangeIntegral;///<积分

/************本地用*********/
@property (nonatomic , strong) NSMutableArray              * priceArr;///<计算钱
@property (nonatomic , strong) NSMutableArray              * infoArr;///展示订单信息
@property (nonatomic , strong) NSMutableArray              <WGOderActionModel *>* actionArr;///<操作类型

@end

@interface WGOderBadgeModel :NSObject
@property (nonatomic , assign) NSInteger               state0; //待付款
@property (nonatomic , assign) NSInteger               state1; //待发货
@property (nonatomic , assign) NSInteger               state2; //待服务
@property (nonatomic , assign) NSInteger               state3;
@property (nonatomic , assign) NSInteger               state4;
@property (nonatomic , assign) NSInteger               state5;
@property (nonatomic , assign) NSInteger               state6;

@end


NS_ASSUME_NONNULL_END
