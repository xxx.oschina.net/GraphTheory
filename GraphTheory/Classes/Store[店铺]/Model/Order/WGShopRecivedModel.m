//
//  WGShopRecivedModel.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGShopRecivedModel.h"

@implementation WGShopRecivedModel

- (NSString *)locatName{
    if (!_locatName) {
        if (_name.length) {
            _locatName = _name;
        }else if (_nickName.length){
            _locatName = _nickName;
        }else if (_mobile.length){
            _locatName = _mobile;
        }else{
            _locatName = _createName;
        }
    }
    return _locatName;
}
@end
