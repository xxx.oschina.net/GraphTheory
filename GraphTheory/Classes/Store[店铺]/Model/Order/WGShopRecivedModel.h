//
//  WGShopRecivedModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGShopRecivedModel : DDBaseModel

@property (nonatomic , copy) NSString              * createPerson;
@property (nonatomic , assign) NSInteger              active;
@property (nonatomic , copy) NSString              * nickName;
@property (nonatomic , copy) NSString              * appType;
@property (nonatomic , copy) NSString              * updatePerson;
@property (nonatomic , copy) NSString              * updateTime;
@property (nonatomic , assign) NSInteger              sex;
@property (nonatomic , copy) NSString              * vendor;
@property (nonatomic , copy) NSString              * systemType;
@property (nonatomic , copy) NSString              * name;
@property (nonatomic , assign) NSInteger              state;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * merchantId;
@property (nonatomic , copy) NSString              * mobile;
@property (nonatomic , copy) NSString              * headImgUrl;
@property (nonatomic , copy) NSString              * model;
@property (nonatomic , copy) NSString              * mobileToken;
@property (nonatomic , copy) NSString              * umToken;
@property (nonatomic , copy) NSString              * role;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * password;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * createName;
@property (nonatomic , copy) NSString              * locatName;



@end

NS_ASSUME_NONNULL_END
