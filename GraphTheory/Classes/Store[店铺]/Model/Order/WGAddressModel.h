//
//  WGAddressModel.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/27.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGAddressModel : NSObject

@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * createPerson;
@property (nonatomic , copy) NSString              * updateTime;
@property (nonatomic , copy) NSString              * updatePerson;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * active;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * name;///<收货人姓名
@property (nonatomic , copy) NSString              * mobile;///<收货人手机号
@property (nonatomic , copy) NSString              * areaId;///<传的是选中的所在地区最后一级的主键,看不明白交流
@property (nonatomic , copy) NSString              * areaName;///<传的是选中的所在地区最后一级的fullName的值,看不明白交流
@property (nonatomic , copy) NSString              * detialAddress;///<详细地址
@property (nonatomic , strong) NSString               * addressTag;///<标签0:家,1:公司,2:学校,3:其他
@property (nonatomic , assign) NSInteger               defaultAddress;///<是否为默认地址:1:是默认地址,2为非默认地址
@property (nonatomic , strong) NSString               * address;///<标签0:家,1:公司,2:学校,3:其他


@end

NS_ASSUME_NONNULL_END
