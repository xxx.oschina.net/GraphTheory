//
//  WGDeliveryModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGDeliveryModel : DDBaseModel

@property (nonatomic , strong) NSString              * Id;
@property (nonatomic , strong) NSString              * createTime;
@property (nonatomic , strong) NSString              * createPerson;
@property (nonatomic , strong) NSString              * updateTime;
@property (nonatomic , strong) NSString              * updatePerson;
@property (nonatomic , strong) NSString              * remark;
@property (nonatomic , strong) NSString              * active;
@property (nonatomic , strong) NSString              * deliveryTypeName;
@property (nonatomic , strong) NSString              * customTypeName;
@property (nonatomic , strong) NSString              * expressNo;



@end

NS_ASSUME_NONNULL_END
