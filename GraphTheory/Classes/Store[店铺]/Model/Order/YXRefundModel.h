//
//  YXRefundModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/5.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@class YXRefundsModel,YXRefundItemsModel,YXRefundActionModel;
@interface YXRefundModel : DDBaseModel

@property (nonatomic , copy) NSString              * address;
@property (nonatomic , copy) NSString              * addressId;
@property (nonatomic , copy) NSString              * assembleId;
@property (nonatomic , copy) NSString              * consumerCode;
@property (nonatomic , copy) NSString              * costPrice;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * dealTime;
@property (nonatomic , copy) NSString              * deliverTime;
@property (nonatomic , copy) NSString              * deliveryTypeId;
@property (nonatomic , copy) NSString              * deliveryTypeName;
@property (nonatomic , copy) NSString              * evaluateState;
@property (nonatomic , copy) NSString              * expressNo;
@property (nonatomic , copy) NSString              * isAllRefund;
@property (nonatomic , copy) NSString              * itemIds;
@property (nonatomic , copy) NSString              * mainDiscountPrice;
/** 配送方式:1快递,2到店,3上门 */
@property (nonatomic , assign) NSInteger             mainDistributionType;
@property (nonatomic , copy) NSString              * mainFreight;
@property (nonatomic , copy) NSString              * mainId;
@property (nonatomic , copy) NSString              * mainItemTotal;
@property (nonatomic , copy) NSString              * mainPayPrice;
@property (nonatomic , copy) NSString              * mainSource;
@property (nonatomic , copy) NSString              * mobile;
@property (nonatomic , copy) NSString              * orderNo;
@property (nonatomic , copy) NSString              * orderState;
@property (nonatomic , copy) NSString              * orderTime;
/** 订单类型0: 未拼团 1: 拼团中 2: 拼团成功 3: 拼团失败 */
@property (nonatomic , assign) NSString              * orderType;
@property (nonatomic , copy) NSString              * payMethod;
@property (nonatomic , copy) NSString              * receivedId;
@property (nonatomic , copy) NSString              * receivedName;
@property (nonatomic , copy) NSString              * refunDeliveryTypeName;
/** 退款金额*/
@property (nonatomic , copy) NSString              * refundAmount;
@property (nonatomic , copy) NSString              * refundDeliveryTypeId;
@property (nonatomic , copy) NSString              * refundExpressNo;
@property (nonatomic , copy) NSString              * refundId;
/** 退款说明*/
@property (nonatomic , copy) NSString              * refundInstructions;
/** 退款失败原因*/
@property (nonatomic , copy) NSString              * refundFailReason;
/** 商户退款单号*/
@property (nonatomic , copy) NSString              * refundNo;
/** 退款原因*/
@property (nonatomic , copy) NSString              * refundRemark;
/** 0:退款中; 1:退款成功; 2:退款失败;3:退款中(等待商家处理); 4:退款中(等待买家发货); 5:退款中(买家已发货)*/
@property (nonatomic , assign) NSInteger             refundState;
/** 退款类型 0:仅退款 1:退货退款*/
@property (nonatomic , assign) NSInteger             refundType;
/** 退款凭证信息*/
@property (nonatomic , copy) NSString              * refundCredentialsInfo;
@property (nonatomic , copy) NSString              * remark;
@property (nonatomic , copy) NSString              * shopId;
@property (nonatomic , copy) NSString              * shopName;
@property (nonatomic , copy) NSString              * threeOrderId;
/** 退款预计到账时间*/
@property (nonatomic , copy) NSString              * refundSuccessTime;
/** 退款签收地址*/
@property (nonatomic , copy) NSString              * signStatus;
/** 退款签收时间*/
@property (nonatomic , copy) NSString              * signTime;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * userName;
@property (nonatomic , copy) NSString              * whetherToOrder;
@property (nonatomic , copy) NSString              * refundsTitle;

/** 退款进度*/
@property (nonatomic , strong) NSArray  <YXRefundsModel *> * refunds;
/** 退款进度排序*/
@property (nonatomic , strong) NSArray  <YXRefundsModel *> * refundsSort;

/** 商品信息*/
@property (nonatomic , strong) NSArray  <YXRefundItemsModel *> * items;

@property (nonatomic , strong) NSMutableArray              <YXRefundActionModel *>* actionArr;///<操作类型
@end


@interface YXRefundsModel : DDBaseModel

/** 退款进度时间*/
@property (nonatomic , strong) NSString              * refundScheduleTime;
/** 退款进度内容*/
@property (nonatomic , strong) NSString              * refundScheduleText;
/** 退款进度内容高度*/
@property (nonatomic , assign) CGFloat height;

@end

@interface YXRefundItemsModel : DDBaseModel

@property (nonatomic , strong) NSString              * distributionType;///<1.快递 2.到店 3.上门
@property (nonatomic , strong) NSString              * itemPath;
@property (nonatomic , strong) NSString              * itemType;
@property (nonatomic , assign) NSInteger              itemSubtotal;///<商品小计
@property (nonatomic , strong) NSString              * itemPrice;
@property (nonatomic , strong) NSString              * itemDiscount;
@property (nonatomic , strong) NSString              * source;///<//来源 01京东 02自定义商品
@property (nonatomic , strong) NSString              * gid;
@property (nonatomic , strong) NSString              * itemNum;
@property (nonatomic , strong) NSString              * itemInfo;
@property (nonatomic , strong) NSString              * shopName;
@property (nonatomic , strong) NSString              * skuId;
@property (nonatomic , strong) NSString              * itemId;
@property (nonatomic , assign) BOOL              * isEditing;

@end


@interface YXRefundCredentialsModel : DDBaseModel
@property (nonatomic , copy) NSString              * credentialsId;
@property (nonatomic , copy) NSString              * credentialsPath;
@end


typedef NS_ENUM(NSInteger ,RefundActionType){
    RefundActionTypeAgreed,///<同意退款
    RefundActionTypeRefused,///<拒绝退款
};

@interface YXRefundActionModel :NSObject
@property (nonatomic , strong) NSString              * actionTitle;
@property (nonatomic , assign) RefundActionType         actionType;

@end

@interface YXRefundReasonModel :NSObject
@property (nonatomic , copy) NSString              * busRefundReason;
@end


NS_ASSUME_NONNULL_END
