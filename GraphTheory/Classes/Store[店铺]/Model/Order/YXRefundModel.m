//
//  YXRefundModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/5.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXRefundModel.h"

@implementation YXRefundModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"refunds" : @"YXRefundsModel",
             @"items"   : @"YXRefundItemsModel"};
}
- (NSString *)refundsTitle {
    if (!_refundsTitle) {
        if (_refundState ==3) {
            if (_refundType ==0){
                _refundsTitle = @"退款进度";
            }else {
                _refundsTitle = @"退货退款进度";
            }
        }else if (_refundState == 1) {
            if (_refundType == 0) {
                _refundsTitle = @"退款进度";
            }else {
                _refundsTitle = @"退货退款";
            }
        }else if (_refundState == 2) {
            if (_refundType == 0) {
                _refundsTitle = @"退款流程";
            }else {
                _refundsTitle = @"退货退款流程";
            }
        }else if (_refundState == 4 || _refundState == 5) {
            _refundsTitle = @"退货退款进度";
        }
    }
    return _refundsTitle;
}

- (NSArray *)refundsSort{
    if (!_refundsSort) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"refundScheduleTime" ascending:NO];
        // 排序结果
        _refundsSort = [_refunds sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
        
    }
    return _refundsSort;
}

- (NSMutableArray *)actionArr{
    if (!_actionArr) {
        _actionArr = [NSMutableArray new];
        if ((_refundState != 1) || (_refundState != 2)) {
            if (_refundState == 4) {
                YXRefundActionModel *action = [YXRefundActionModel new];
                action.actionTitle = @"拒绝退款";
                action.actionType = RefundActionTypeRefused;
                [_actionArr addObject:action];
            }else {
                YXRefundActionModel *action = [YXRefundActionModel new];
                action.actionTitle = @"同意退款";
                action.actionType = RefundActionTypeAgreed;
                [_actionArr addObject:action];
                YXRefundActionModel *action1 = [YXRefundActionModel new];
                action1.actionTitle = @"拒绝退款";
                action1.actionType = RefundActionTypeRefused;
                [_actionArr addObject:action1];
            }

        }
    }
    return _actionArr;
}

@end

@implementation YXRefundsModel

- (CGFloat)height{
    if (_height == 0) {
        NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGRect rect = [_refundScheduleText boundingRectWithSize:CGSizeMake(KWIDTH - 55, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:dic context:nil];
        _height = rect.size.height + 35;
        
    }
    return _height;
}

@end


@implementation YXRefundItemsModel

@end

@implementation YXRefundCredentialsModel

@end

@implementation YXRefundActionModel

@end

@implementation YXRefundReasonModel

@end
