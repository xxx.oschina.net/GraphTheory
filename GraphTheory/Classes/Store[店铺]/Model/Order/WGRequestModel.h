//
//  WGRequestModel.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGRequestModel : DDBaseModel
@property (nonatomic , assign) NSInteger              pageSize;
@property (nonatomic , copy) NSString              * time;
@property (nonatomic , assign) NSInteger              code;
@property (nonatomic , assign) NSInteger              pageCount;
@property (nonatomic , copy) NSString              * msg;
@property (nonatomic , assign) NSInteger              total;
@property (nonatomic , assign) BOOL              state;
@property (nonatomic , assign) NSInteger              pageNum;
@property (nonatomic , copy) id              data;
@end

NS_ASSUME_NONNULL_END
