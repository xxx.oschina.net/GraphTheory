//
//  WGOrderModel.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderModel.h"

@implementation WGOderActionModel

@end

@implementation WGOderDetailInfoModel

@end

@implementation WGOderDetailModel

- (id)copyWithZone:(NSZone *)zone{
    return [self lxz_copyWithZone:zone];
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"carID": @"id",@"itemInfo":@[@"itemInfo",@"itemName"]};
}
- (NSInteger)productStocks{
    return 1000;
}

@end

@implementation WGShopModel

- (id)copyWithZone:(NSZone *)zone{
    return [self lxz_copyWithZone:zone];
}

+(NSDictionary *)mj_objectClassInArray{
    
    return @{@"items":[WGOderDetailModel class]};
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"items": @[@"items",@"Items"],@"mainFreight":@[@"mainFreight",@"freight"],@"mainItemTotal":@[@"mainItemTotal",@"mianDiscountPrice"]};
}
- (NSString *)mainDistributionName{
    if (!_mainDistributionName) {
        if (_mainDistributionType.integerValue == 1) {
            _mainDistributionName = @"快递";
        }else if (_mainDistributionType.integerValue == 2){
            _mainDistributionName = @"到店";
            
        }else if (_mainDistributionType.integerValue == 3){
            if (_whetherToOrder.integerValue == 0) {
                _mainDistributionName = @"上门(待接单)";
            }else if (_whetherToOrder.integerValue == 1){
                _mainDistributionName = @"上门(已接单)";
            }else{
                _mainDistributionName = @"上门";
            }
        }
    }
    return _mainDistributionName;
}
- (NSString *)itemNum{
    if (!_itemNum) {
        NSInteger num = 0;
        for (WGOderDetailModel *detail in _items) {
            num += detail.itemNum.integerValue;
        }
        _itemNum = [NSString stringWithFormat:@"%ld",(long)num];
    }
    return _itemNum;
}

- (NSMutableArray *)priceArr{
    if (!_priceArr) {
        _priceArr = [NSMutableArray new];
        if (_mainItemTotal.length) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"商品总额";
            tem.valueStr = [NSString stringWithFormat:@"￥%@",[NSString formatPriceString:_mainItemTotal]];
            [_priceArr addObject:tem];
        }
        //        if (_recordFreight.length && _recordFreight.floatValue != 0) {
        WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
        tem.keyStr = @"运费";
        tem.valueStr = [NSString stringWithFormat:@"+￥%@",[NSString formatPriceString:_mainFreight]];
        [_priceArr addObject:tem];
        
        //        }
        if (_mainDiscountPrice.length && _mainDiscountPrice.floatValue != 0) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"优惠";
            tem.valueStr = [NSString stringWithFormat:@"-￥%@",[NSString formatPriceString:_mainDiscountPrice]];
            [_priceArr addObject:tem];
        }
        
    }
    return _priceArr;
}

- (NSMutableArray *)infoArr{
    if (!_infoArr) {
        _infoArr = [NSMutableArray new];
        [_infoArr removeAllObjects];
        if (_payMethod.integerValue > 0&&(_orderState != 0&&_orderState != 5)) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"支付方式";
            if ([_payMethod isEqualToString:@"2"]) {
                tem.valueStr = @"微信";
            }else if ([_payMethod isEqualToString:@"3"]){
                tem.valueStr = @"支付宝";
            }else if ([_payMethod isEqualToString:@"1"]){
                tem.valueStr = @"余额";
            }
            tem.isCanCopy = NO;
            [_infoArr addObject:tem];
        }
        if (self.orderNo.length) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"订单号";
            tem.valueStr = self.orderNo;
            tem.isCanCopy = YES;
            [_infoArr addObject:tem];
        }
        
        if (self.tradeNo.length) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"交易单号";
            tem.valueStr = self.tradeNo;
            tem.isCanCopy = YES;
            [_infoArr addObject:tem];
        }
        
        if (self.systemNo.length) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"交易流水号";
            tem.valueStr = self.systemNo;
            tem.isCanCopy = YES;
            [_infoArr addObject:tem];
        }
        
        if (_orderTime.length && (_orderState != 0&&_orderState != 5)) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"下单时间";
            tem.valueStr = _orderTime;
            tem.isCanCopy = NO;
            [_infoArr addObject:tem];
        }
        
        if (self.dealTime.length) {
            WGOderDetailInfoModel *tem = [WGOderDetailInfoModel new];
            tem.keyStr = @"付款时间";
            tem.valueStr = self.dealTime;
            tem.isCanCopy = NO;
            [_infoArr addObject:tem];
        }
      
    }
    return _infoArr;
}
- (NSMutableArray *)items{
    if (!_items) {
        _items = [NSMutableArray new];
    }
    return _items;
}
- (NSMutableArray *)actionArr{
    if (!_actionArr) {
        _actionArr = [NSMutableArray new];
        
        //        if (_orderState == 1 || _orderState == 2) {
        //            WGOderActionModel *action = [WGOderActionModel new];
        //            action.actionTitle = @"取消订单";
        //            action.actionType = OrderActionTypeCancle;
        //            [_actionArr addObject:action];
        //        }
        if (_orderState == 1 && _mainSource.integerValue != 1 && _mainSource.integerValue != 3) {
            WGOderActionModel *action = [WGOderActionModel new];
            action.actionTitle = @"备注";
            action.actionType = OrderActionTypeNote;
            [_actionArr addObject:action];

            
            WGOderActionModel *action1 = [WGOderActionModel new];
            action1.actionTitle = @"发货";
            action1.actionType = OrderActionTypeDeliver;
            [_actionArr addObject:action1];
            
        }
        
        if (_orderState == 2) {
            if (_mainDistributionType.integerValue == 3) {
                if (_whetherToOrder.integerValue == 0) {
                    WGOderActionModel *action = [WGOderActionModel new];
                    action.actionTitle = @"接单";
                    action.actionType = OrderActionTypeSelctClerk;
                    [_actionArr addObject:action];
                }else if (_whetherToOrder.integerValue == 1){
                    WGOderActionModel *action = [WGOderActionModel new];
                    action.actionTitle = @"更换接单人";
                    action.actionType = OrderActionTypeSelctClerk;
                    [_actionArr addObject:action];
                }else{
                    WGOderActionModel *action = [WGOderActionModel new];
                    action.actionTitle = @"接单";
                    action.actionType = OrderActionTypeSelctClerk;
                    [_actionArr addObject:action];
                }
            }else if (_mainDistributionType.integerValue == 2){
                WGOderActionModel *action = [WGOderActionModel new];
                action.actionTitle = @"确认消费码";
                action.actionType = OrderActionTypeVerifyPayNO;
                [_actionArr addObject:action];
                
//                WGOderActionModel *action1 = [WGOderActionModel new];
//                action1.actionTitle = @"确认订单";
//                action1.actionType = OrderActionTypeDone;
//                [_actionArr addObject:action1];
            }
        }
        
        if (_orderState == 3) {
            WGOderActionModel *action = [WGOderActionModel new];
             action.actionTitle = @"继续发货";
             action.actionType = OrderActionTypeContinueDeliver;
             [_actionArr addObject:action];
            
            WGOderActionModel *action1 = [WGOderActionModel new];
            action1.actionTitle = @"查看物流";
            action1.actionType = OrderActionTypeDistribution;
            [_actionArr addObject:action1];

        }
        if (_orderState == 4 && _mainDistributionType.integerValue == 1){
            WGOderActionModel *action = [WGOderActionModel new];
            action.actionTitle = @"查看物流";
            action.actionType = OrderActionTypeDistribution;
            [_actionArr addObject:action];
        }
        if (_orderState == 0) {
            WGOderActionModel *action = [WGOderActionModel new];
            action.actionTitle = @"提醒付款";
            action.actionType = OrderActionTypeRemindPay;
            [_actionArr addObject:action];
            
        }
    }
    return _actionArr;
}
@end

@implementation WGOderBadgeModel


@end
