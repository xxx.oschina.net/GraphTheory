//
//  WGExpressModel.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/16.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressModel.h"
#import "YXDataTimeTool.h"
@implementation WaybillCode : NSObject 

@end

@implementation OrderTrack

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"operatorer": @[@"operator",@"zone"],@"time":@[@"msgTime",@"time"],@"status":@[@"content",@"status"]};
}

- (CGFloat)height{
    if (_height == 0) {
        NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
        CGRect rect = [_status boundingRectWithSize:CGSizeMake(KWIDTH - 55, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:dic context:nil];
        _height = rect.size.height + 35;
        
    }
    return _height;
}

@end
@implementation WGExpressModel

+(NSDictionary *)mj_objectClassInArray{
    
    return @{@"list":[OrderTrack class],@"item":[WGOderDetailModel class],@"waybillCode":[WaybillCode class]};
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"list": @[@"list",@"orderTrack"]};
}
- (NSArray *)listSort{
    if (!_listSort) {
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO];
        // 排序结果
        _listSort = [_list sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];

    }
    return _listSort;
}
- (NSArray *)list{
    if (!_list) {
        OrderTrack *detail = [OrderTrack new];
        detail.status = @"暂无物流数据";
        detail.time = [YXDataTimeTool getCurrentTimes];
        _list = @[detail];
    }
    return _list;
}
- (NSString *)expressNo{
    if (!_expressNo) {
        if (_waybillCode.count) {
            _expressNo = _waybillCode[0].deliveryOrderId;
        }
    }
    return _expressNo;
}
- (NSString *)deliveryTypeName{
    if (!_deliveryTypeName) {
        if (_waybillCode.count) {
            _deliveryTypeName = _waybillCode[0].carrier;
        }
    }
    return _deliveryTypeName;
}

@end

