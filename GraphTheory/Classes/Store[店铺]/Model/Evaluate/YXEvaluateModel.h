//
//  YXEvaluateModel.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

@interface YXEvaluateModel : DDBaseModel

/** 评价id*/
@property (nonatomic ,copy) NSString *evaluateId;
/** 用户名*/
@property (nonatomic ,copy) NSString *createName;
/** 店铺名称*/
@property (nonatomic ,copy) NSString *shopsName;
/** 产品名称*/
@property (nonatomic ,copy) NSString *productName;
/** 评价时间*/
@property (nonatomic ,copy) NSString *evaluateDueTime;
/** 评价等级*/
@property (nonatomic ,assign) CGFloat evaluateStar;
/** 评价内容*/
@property (nonatomic ,copy) NSString *productEvaluate;
/** 是否回复*/
@property (nonatomic ,assign) BOOL isReply;
/** 商家回复内容*/
@property (nonatomic ,strong) NSArray *replyMessage;
/** 图片数组*/
@property (nonatomic ,strong) NSArray *evaluatePhoto;


/** 返回模型cell的高度*/
@property (nonatomic ,assign) CGFloat cellHeight;
/** 返回商品评价模型cell的高度*/
@property (nonatomic ,assign) CGFloat productCellHeight;
/** 图片frame*/
@property (nonatomic ,assign) CGRect photoViewF;
/** 商家回复内容frame*/
@property (nonatomic ,assign) CGRect replyLabF;

@end




@interface YXEvaluatePhotoModel : DDBaseModel

/** 图片地址标识 1.user 2.order */
@property (nonatomic ,copy) NSString *orderOrUser;

/** 图片地址*/
@property (nonatomic ,copy) NSString *photoUrl;

@end


@interface YXEvaluateCountModel : DDBaseModel


/** 全部个数*/
@property (nonatomic ,assign) NSInteger allCounts;
/** 差评个数*/
@property (nonatomic ,assign) NSInteger badCounts;
/** 好评个数*/
@property (nonatomic ,assign) NSInteger goodCounts;
/** 中评个数*/
@property (nonatomic ,assign) NSInteger middleCounts;

@end
