
//
//  YXEvaluateModel.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/2/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEvaluateModel.h"

@implementation YXEvaluateModel

+ (NSDictionary *)objectClassInArray{
    return @{
             @"evaluatePhoto" : @"YXEvaluatePhotoModel",
             };
}

- (void)setProductName:(NSString *)productName {
    _productName = productName;
    if (_productName.length > 6) {
       _productName = [NSString stringWithFormat:@"%@...",[_productName substringToIndex:6]];
    }
}


- (CGFloat)cellHeight {
    
    CGFloat margin = 10;

    // 1.默认高度
    _cellHeight = 50+ margin;
    
    // 评价内容
    CGSize textMaxSize = CGSizeMake(KWIDTH - 40, MAXFLOAT);
    CGSize textSize = [self.productEvaluate boundingRectWithSize:textMaxSize options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size;
    _cellHeight += textSize.height + 10;
    
    if (self.evaluatePhoto.count > 0) {
        self.photoViewF = CGRectMake(20, _cellHeight, KWIDTH - 40, 80);
        _cellHeight += 80 + 10;
    }else {
        _cellHeight += 10;
    }
    
    
    // 判断是否回复信息
    if (self.replyMessage.count > 0) {
        //  回复内容
        NSString *content = [NSString stringWithFormat:@"商家回复：%@",[self.replyMessage firstObject]];
        CGSize textMaxSize = CGSizeMake(KWIDTH - 40, MAXFLOAT);
        CGSize textSize = [content boundingRectWithSize:textMaxSize options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0]} context:nil].size;
        _cellHeight += textSize.height + 10;
        
    }else {
        _cellHeight += 30 + 10;
        
    }
    
    return _cellHeight;
}

- (CGFloat)productCellHeight {

    CGFloat margin = 10;

    // 1.默认高度
    _productCellHeight = 40+ margin;

    // 标题
    CGSize textMaxSize = CGSizeMake(KWIDTH - 30, MAXFLOAT);
    CGSize textSize = [self.productEvaluate boundingRectWithSize:textMaxSize options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15]} context:nil].size;
    _productCellHeight += textSize.height ;

    if (self.evaluatePhoto.count > 0) {
        self.photoViewF = CGRectMake(0, _productCellHeight, KWIDTH, 80);
        _productCellHeight += 80 + 10;
    }else {
        _productCellHeight += 10;
    }


    return _productCellHeight;
}



@end



@implementation YXEvaluatePhotoModel


@end


@implementation YXEvaluateCountModel


@end
