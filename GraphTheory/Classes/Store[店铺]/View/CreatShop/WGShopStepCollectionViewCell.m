//
//  WGShopStepCollectionViewCell.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGShopStepCollectionViewCell.h"

@interface WGShopStepCollectionViewCell ()

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UIView *lineView;

@property (strong, nonatomic) UIImageView *baseImageView;
@property (strong, nonatomic) UIImageView *ceeImageView;
@property (strong, nonatomic) UILabel *baseTitleLab;
@property (strong, nonatomic) UILabel *ceTitleLab;

@end

@implementation WGShopStepCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setStep:(NSInteger)step{
    _step = step;
    if (step == 0) {
        _baseImageView.image = [UIImage imageNamed:@"shop_icon_baseInfo"];
        _ceeImageView.image = [UIImage imageNamed:@"shop_icon_ceInfo"];
        [self drawLineOfDashByCAShapeLayer:self.lineView lineLength:5 lineSpacing:5 lineColor:HEX_COLOR(@"#BBBBBB") lineDirection:YES];

    }else{
        _baseImageView.image = [UIImage imageNamed:@"shop_icon_baseInfo"];
        _ceeImageView.image = [UIImage imageNamed:@"shop_icon_baseInfo"];
        [self drawLineOfDashByCAShapeLayer:self.lineView lineLength:5 lineSpacing:5 lineColor:APPTintColor lineDirection:YES];

    }
}
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.baseTitleLab];
    [self.backView addSubview:self.baseImageView];
    [self.backView addSubview:self.ceeImageView];
    [self.backView addSubview:self.ceTitleLab];
    self.lineView.frame = CGRectMake(0, 0, 124, 2);

    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.centerY.equalTo(self.baseImageView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(124, 2));
    }];
    [_baseImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(35, 35));
        make.top.equalTo(self.backView.mas_top).offset(15);
        make.right.equalTo(self.lineView.mas_left).offset(-15);
    }];
    [_baseTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.baseImageView.mas_bottom).offset(10);
        make.centerX.equalTo(self.baseImageView.mas_centerX);
    }];
    [_ceeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(35, 35));
        make.top.equalTo(self.backView.mas_top).offset(15);
        make.left.equalTo(self.lineView.mas_right).offset(15);
    }];
    [_ceTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.ceeImageView.mas_bottom).offset(10);
        make.centerX.equalTo(self.ceeImageView.mas_centerX);
    }];
}
/**
 *  通过 CAShapeLayer 方式绘制虚线
 *
 *  param lineView:       需要绘制成虚线的view
 *  param lineLength:     虚线的宽度
 *  param lineSpacing:    虚线的间距
 *  param lineColor:      虚线的颜色
 *  param lineDirection   虚线的方向  YES 为水平方向， NO 为垂直方向
 **/
- (void)drawLineOfDashByCAShapeLayer:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor lineDirection:(BOOL)isHorizonal {
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:lineView.bounds];
    if (isHorizonal) {
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame))];
    } else{
        [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.frame) / 2, CGRectGetHeight(lineView.frame)/2)];
    }
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    //  设置虚线宽度
    if (isHorizonal) {
        [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)];
    } else {
        [shapeLayer setLineWidth:CGRectGetWidth(lineView.frame)];
    }
    [shapeLayer setLineJoin:kCALineJoinRound];
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    if (isHorizonal) {
        CGPathAddLineToPoint(path, NULL,CGRectGetWidth(lineView.frame), 0);
    } else {
        CGPathAddLineToPoint(path, NULL, 0, CGRectGetHeight(lineView.frame));
    }
    [shapeLayer setPath:path];
    CGPathRelease(path);
    //  把绘制好的虚线添加上来
    [lineView.layer addSublayer:shapeLayer];
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
    }
    return _lineView;
}
- (UILabel *)baseTitleLab{
    if (!_baseTitleLab) {
        _baseTitleLab = [UILabel new];
        _baseTitleLab.textColor = color_TextOne;
        _baseTitleLab.font = [UIFont systemFontOfSize:12];
        _baseTitleLab.text = @"基本信息";
    }
    return _baseTitleLab;
}
- (UILabel *)ceTitleLab{
    if (!_ceTitleLab) {
        _ceTitleLab = [UILabel new];
        _ceTitleLab.textColor = color_TextOne;
        _ceTitleLab.font = [UIFont systemFontOfSize:12];
        _ceTitleLab.text = @"认证信息";
    }
    return _ceTitleLab;
}
- (UIImageView *)baseImageView{
    if (!_baseImageView) {
        _baseImageView = [UIImageView new];
        _baseImageView.image = [UIImage imageNamed:@"shop_icon_baseInfo"];
    }
    return _baseImageView;
}
- (UIImageView *)ceeImageView{
    if (!_ceeImageView) {
        _ceeImageView = [UIImageView new];
        _ceeImageView.image = [UIImage imageNamed:@"shop_icon_ceInfo"];
    }
    return _ceeImageView;
}
@end
