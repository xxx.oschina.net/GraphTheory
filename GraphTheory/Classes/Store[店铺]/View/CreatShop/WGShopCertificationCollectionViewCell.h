//
//  WGShopCertificationCollectionViewCell.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/5.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAddGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN
@class DDStoreDataModel;
@interface WGShopCertificationCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGGoodsImageModel *goodsModel;

@property (nonatomic, strong) NSIndexPath *indexPath;

@property (nonatomic, strong) DDStoreDataModel *storeDataModel;

@end

NS_ASSUME_NONNULL_END
