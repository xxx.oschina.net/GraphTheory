//
//  WGShopCertificationCollectionViewCell.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/5.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGShopCertificationCollectionViewCell.h"
#import "WGStoreModel.h"
@interface WGShopCertificationCollectionViewCell ()

/**
 边框
 */
@property (nonatomic ,strong) UIImageView *borderImageView;

/**
 详细图片
 */
@property (nonatomic ,strong) UIImageView *detailImageView;

/**
 描述
 */
@property (nonatomic ,strong) UILabel *detailLab;
/**
 描述
 */
@property (nonatomic ,strong) UILabel *desLab;


@end


@implementation WGShopCertificationCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}

- (void)setGoodsModel:(WGGoodsImageModel *)goodsModel{
    _goodsModel = goodsModel;
    if (_goodsModel.thumbnail) {
        _detailImageView.image = _goodsModel.thumbnail;
    }else if (_goodsModel.imagePath){
        [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_goodsModel.imagePath] placeholderImage:_goodsModel.defalutImage];
    }else{
        _detailImageView.image = _goodsModel.defalutImage;
    }
    _detailLab.text = goodsModel.title;
    _desLab.text = goodsModel.des;
}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
}

- (void)setStoreDataModel:(DDStoreDataModel *)storeDataModel {
    _storeDataModel = storeDataModel;
    
    if (_indexPath.section == 2) {
        switch (_indexPath.row) {
            case 0: {
                _detailLab.text = @"上传身份证正面";
                _desLab.text = @"";
                [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeDataModel.shopViceModel.cardFront] placeholderImage:[UIImage imageNamed:@"shop_icon_sfzFront"]];
            }
                break;
            case 1: {
                _detailLab.text = @"上传身份证反面";
                _desLab.text = @"";
                    [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeDataModel.shopViceModel.cardReverse] placeholderImage:[UIImage imageNamed:@"shop_icon_sfzback"]];
            }
                break;
            case 2: {
                _detailLab.text = @"上传营业执照";
                _desLab.text = @"";
                    [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeDataModel.shopViceModel.businessLicense] placeholderImage:[UIImage imageNamed:@"shop_icon_yyzz"]];
            }
                break;
            case 3: {
                _detailLab.text = @"上传特殊资质证明";
                _desLab.text = @"请提供《食品流通许可证》或《食品经营许可证》\r或《食品生产许可证》";
                    [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeDataModel.shopViceModel.healthPermit] placeholderImage:[UIImage imageNamed:@"shop_icon_tszz"]];
            }
                break;
            case 4: {
                _detailLab.text = @"上传特殊资质证明";
                _desLab.text = @"请提供《特种行业经营许可证》";
                    [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeDataModel.shopViceModel.otherPermit] placeholderImage:[UIImage imageNamed:@"shop_icon_tzhy"]];
            }
                break;
            default:
                break;
        }
    }
}


/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.borderImageView];
    [self addSubview:self.detailImageView];
    [self addSubview:self.detailLab];
    [self addSubview:self.desLab];

    [_borderImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(198.0, 128.0));
        make.centerX.equalTo(self.mas_centerX);
    }];
    [_detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(1);
        make.size.mas_equalTo(CGSizeMake(196.0, 126.0));
        make.centerX.equalTo(self.mas_centerX);    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailImageView.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    [_desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailLab.mas_bottom).offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
}
- (UIImageView *)borderImageView{
    if (!_borderImageView) {
        _borderImageView = [UIImageView new];
        _borderImageView.image = [UIImage imageNamed:@"shop_icon_border"];
    }
    return _borderImageView;
}
- (UIImageView *)detailImageView{
    if (!_detailImageView) {
        _detailImageView = [UIImageView new];
        _detailImageView.contentMode = UIViewContentModeScaleToFill;
        
    }
    return _detailImageView;
}
- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [UILabel new];
        _detailLab.textColor = color_TextThree;
        _detailLab.font = [UIFont systemFontOfSize:14];
        _detailLab.textAlignment = NSTextAlignmentCenter;
    }
    return _detailLab;
}
- (UILabel *)desLab{
    if (!_desLab) {
        _desLab = [UILabel new];
        _desLab.textColor = color_TextThree;
        _desLab.font = [UIFont systemFontOfSize:12];
        _desLab.textAlignment = NSTextAlignmentCenter;
        _desLab.numberOfLines = 2;
    }
    return _desLab;
}
@end
