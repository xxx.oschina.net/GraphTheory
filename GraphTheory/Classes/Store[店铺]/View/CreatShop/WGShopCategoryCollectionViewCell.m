//
//  WGShopCategoryCollectionViewCell.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGShopCategoryCollectionViewCell.h"
#import "YXWebViewController.h"
@interface WGShopCategoryCollectionViewCell ()

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UILabel *titleLab;

@property (strong, nonatomic) UIButton *introduceBtn;



@end

@implementation WGShopCategoryCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)introduceBtnAction{
    
//    YXWebViewController *vc = [YXWebViewController new];
//    vc.titleStr = @"主营类目明细";
//    vc.url = DD_Store_GoodsCategoryWebUrl;
//    [[UIView currentViewController].navigationController pushViewController:vc animated:YES];
    
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.introduceBtn];

    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_introduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLab.mas_right).offset(8);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];

}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextThree;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"主体类目明细";
    }
    return _titleLab;
}
- (UIButton *)introduceBtn {
    if (!_introduceBtn) {
        _introduceBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];

        [_introduceBtn setTitle:@"查看详情" forState:(UIControlStateNormal)];
        [_introduceBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
        _introduceBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_introduceBtn addTarget:self action:@selector(introduceBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _introduceBtn;
}
@end
