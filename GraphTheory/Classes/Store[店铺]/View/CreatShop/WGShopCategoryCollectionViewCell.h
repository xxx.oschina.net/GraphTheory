//
//  WGShopCategoryCollectionViewCell.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGShopCategoryCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
