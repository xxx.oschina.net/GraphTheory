//
//  WGShopSubjectSelectCollectionViewCell.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGShopSubjectSelectCollectionViewCell.h"
#import "MMButton.h"
#import "YXWebViewController.h"

@interface WGShopSubjectSelectCollectionViewCell ()

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UIView *lineView;

@property (strong, nonatomic) MMButton *introduceBtn;
@property (strong, nonatomic) MMButton *personalBtn;
@property (strong, nonatomic) MMButton *companyBtn;


@end

@implementation WGShopSubjectSelectCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setStoreModel:(WGStoreModel *)storeModel{
    _storeModel = storeModel;
    _personalBtn.selected = storeModel.subjectType == 1;
    _companyBtn.selected = storeModel.subjectType == 2;

}
- (void)introduceBtnAction{
    
//    YXWebViewController *vc = [YXWebViewController new];
//    vc.titleStr = @"主体类型介绍";
//    vc.url = DD_Store_AccountSubjectWebUrl;
//    [[UIView currentViewController].navigationController pushViewController:vc animated:YES];
}
- (void)personalBtnAction{
    _personalBtn.selected = YES;
    _companyBtn.selected = NO;
    if (self.clickTypeBtnBlock) {
        self.clickTypeBtnBlock(1);
    }
}
- (void)companyBtnAction{
    _personalBtn.selected = NO;
    _companyBtn.selected = YES;
    if (self.clickTypeBtnBlock) {
        self.clickTypeBtnBlock(2);
    }
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.introduceBtn];
    [self.backView addSubview:self.personalBtn];
    [self.backView addSubview:self.companyBtn];
    [self.backView addSubview:self.lineView];
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_introduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLab.mas_right).offset(0);
        make.bottom.equalTo(self.titleLab.mas_top).offset(0);
    }];
    [_companyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_personalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.companyBtn.mas_left).offset(-40);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.backView);
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.height.mas_equalTo(@1);
    }];

}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"F4F4F4"];
    }
    return _lineView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"主体类型";
    }
    return _titleLab;
}
- (MMButton *)personalBtn {
    if (!_personalBtn) {
        _personalBtn = [MMButton buttonWithType:(UIButtonTypeCustom)];
        _personalBtn.spaceBetweenTitleAndImage = 10;
//        _personalBtn.imageAlignment = MMImageAlignmentRight;

        [_personalBtn setTitle:@"个人认证" forState:(UIControlStateNormal)];
        [_personalBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        [_personalBtn setImage:[UIImage imageNamed:@"advert_unSelect"] forState:(UIControlStateNormal)];
        [_personalBtn setImage:[UIImage imageNamed:@"advert_Select"] forState:(UIControlStateSelected)];
        _personalBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_personalBtn addTarget:self action:@selector(personalBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _personalBtn;
}
- (MMButton *)companyBtn {
    if (!_companyBtn) {
        _companyBtn = [MMButton buttonWithType:(UIButtonTypeCustom)];
        _companyBtn.spaceBetweenTitleAndImage = 10;
//        _companyBtn.imageAlignment = MMImageAlignmentRight;
        [_companyBtn setTitle:@"企业认证" forState:(UIControlStateNormal)];
        [_companyBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        [_companyBtn setImage:[UIImage imageNamed:@"advert_unSelect"] forState:(UIControlStateNormal)];
        [_companyBtn setImage:[UIImage imageNamed:@"advert_Select"] forState:(UIControlStateSelected)];
        _companyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_companyBtn addTarget:self action:@selector(companyBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _companyBtn;
}
- (MMButton *)introduceBtn {
    if (!_introduceBtn) {
        _introduceBtn = [MMButton buttonWithType:(UIButtonTypeCustom)];
        _introduceBtn.spaceBetweenTitleAndImage = 5;
        _introduceBtn.imageAlignment = MMImageAlignmentRight;

        [_introduceBtn setTitle:@"介绍" forState:(UIControlStateNormal)];
        [_introduceBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        [_introduceBtn setImage:[UIImage imageNamed:@"shop_icon_tip"] forState:(UIControlStateNormal)];
        _introduceBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_introduceBtn addTarget:self action:@selector(introduceBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _introduceBtn;
}
@end
