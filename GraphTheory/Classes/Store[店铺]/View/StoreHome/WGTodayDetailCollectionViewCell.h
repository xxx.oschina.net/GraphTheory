//
//  WGTodayDetailCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGTodayDetailCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSIndexPath *index;
@property (nonatomic, strong) WGStoreDetailNumModel *detail;

@end

NS_ASSUME_NONNULL_END
