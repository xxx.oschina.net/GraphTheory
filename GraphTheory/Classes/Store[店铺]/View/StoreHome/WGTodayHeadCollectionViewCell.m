//
//  WGTodayHeadCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGTodayHeadCollectionViewCell.h"

@interface WGTodayHeadCollectionViewCell ()

/**
 今日概况
 */
@property (nonatomic ,strong) UILabel *todayLab;

/**
 编辑按钮
 */
@property (nonatomic ,strong) UIButton *editorBtn;



@end

@implementation WGTodayHeadCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.todayLab];
    [self.contentView addSubview:self.editorBtn];
    [_todayLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [_editorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.mas_bottom);
        make.width.equalTo(self.editorBtn.mas_height);
    }];
}

- (UILabel *)todayLab {
    if (!_todayLab) {
        _todayLab = [[UILabel alloc] init];
        _todayLab.text = @"今日概况";
        _todayLab.textColor = [UIColor whiteColor];
        _todayLab.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _todayLab;
}

- (UIButton *)editorBtn {
    if (!_editorBtn) {
        _editorBtn = [UIButton  buttonWithType:UIButtonTypeCustom];
        [_editorBtn setImage:[UIImage imageNamed:@"store_home_editor"] forState:UIControlStateNormal];
        _editorBtn.hidden = YES;
//        [_editorBtn setTitle:@"编辑" forState:UIControlStateNormal];
//        [_editorBtn addTarget:self action:@selector(msgAction) forControlEvents:UIControlEventTouchUpInside];
    }
    return _editorBtn;
}
@end
