//
//  WGMineInfoHeadBackCollectionReusableView.m
//  DDLife
//
//  Created by wanggang on 2019/7/18.
//  Copyright © 2019年 赵越. All rights reserved.
//

#import "WGMineInfoHeadBackCollectionReusableView.h"
#import "WGCollectionViewLayoutAttributes.h"

@interface WGMineInfoHeadBackCollectionReusableView ()

@property (nonatomic, strong) UIView *backView;

@end

@implementation WGMineInfoHeadBackCollectionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _backView = [UIView new];
        [self addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        _backView.layer.cornerRadius = 4;
        _backView.layer.masksToBounds = YES;
    }
    return self;
}

- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    if ([layoutAttributes isKindOfClass:[WGCollectionViewLayoutAttributes class]]) {
        WGCollectionViewLayoutAttributes *attr = (WGCollectionViewLayoutAttributes *)layoutAttributes;
//        self.backgroundColor = attr.backgroundColor;
        if (attr.type == 0) {
            _backView.backgroundColor = [UIColor clearColor];
        }else if (attr.type == 1){
            _backView.backgroundColor = [UIColor colorWithHexString:@"#FF6F24"];
        }else if (attr.type == 2){
            _backView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        }
    }
}

@end

