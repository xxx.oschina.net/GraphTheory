//
//  WGStoreMenuCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreMenuCollectionViewCell.h"

@interface WGStoreMenuCollectionViewCell()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UIImageView *iconImageView;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *detailLab;


@end

@implementation WGStoreMenuCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    switch (index.row) {
        case 0:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_store"];
            _detailLab.text = @"店铺";
        }
            break;
        case 1:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_goods"];
            _detailLab.text = @"商品";
        }
            break;
        case 2:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_order"];
            _detailLab.text = @"订单";
        }
            break;
        case 3:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_marking"];
            _detailLab.text = @"营销";
        }
            break;
        case 4:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_clientele"];
            _detailLab.text = @"客户";
        }
            break;
        case 5:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_evaluate"];
            _detailLab.text = @"评价";
        }
            break;
        case 6:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_analyze"];
            _detailLab.text = @"分析";
        }
            break;
        case 7:{
            _iconImageView.image = [UIImage imageNamed:@"store_home_analyze"];
            _detailLab.text = @"员工";
        }
            break;
        default:
            break;
    }
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.iconImageView];
    [self.backView addSubview:self.detailLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.backView.mas_top).offset(5);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
    }];
    
}
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.image = [UIImage imageNamed:@"store_phone"];
    }
    return _iconImageView;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.text = @"店铺";
        _detailLab.textColor = color_TextTwo;
        _detailLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _detailLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
//        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

@end
