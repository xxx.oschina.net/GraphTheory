//
//  WGTodayDetailCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGTodayDetailCollectionViewCell.h"

@interface WGTodayDetailCollectionViewCell()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *detailLab;


@end

@implementation WGTodayDetailCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}

- (void)setDetail:(WGStoreDetailNumModel *)detail{
    _detail = detail;
    switch (_index.row) {
        case 1:{
            _titleLab.text = @"交易金额";
            _detailLab.text = [NSString formatPriceString:_detail.payMoney];
        }
            break;
        case 2:{
            _titleLab.text = @"订单数量";
            _detailLab.text = _detail.nowOrderNumber?:@"0";
        }
            break;
        case 3:{
            _titleLab.text = @"客单价";
            _detailLab.text = _detail.customerPrice?[NSString stringWithFormat:@"%.02f",[_detail.customerPrice floatValue]]:@"0";
        }
            break;
        case 4:{
            _titleLab.text = @"买家数量";
            _detailLab.text = _detail.buyersNumber?:@"0";
        }
            break;
        case 5:{
            _titleLab.text = @"下单件数";
            _detailLab.text = _detail.quantityOrderNumber?:@"0";
        }
            break;
        case 6:{
            _titleLab.text = @"支付件数";
            _detailLab.text = _detail.payPackNumber?:@"0";
        }
            break;
            
            
        default:
            break;
    }

}

- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    }
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.detailLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(30);
        make.top.equalTo(self.backView.mas_top).offset(5);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLab.mas_left);
        make.top.equalTo(self.titleLab.mas_bottom).offset(10);
    }];
    
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"客单价";
        _titleLab.textColor = [UIColor whiteColor];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.text = @"122";
        _detailLab.textColor = [UIColor whiteColor];
        _detailLab.font = [UIFont boldSystemFontOfSize:18.0f];
    }
    return _detailLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}

@end
