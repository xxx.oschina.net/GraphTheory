//
//  WGStoreOrderCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreOrderCollectionViewCell.h"
#import "YXHomeModel.h"
@interface WGStoreOrderCollectionViewCell()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *detailLab;


@end

@implementation WGStoreOrderCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    _titleLab.hidden = NO;
}

- (void)setStatus:(NSString *)status {
    _status = status;
}

- (void)setModel:(YXRealTimeDataModel *)model {
    _model = model;
    if ([_status isEqualToString:@"study"]) {
        switch (_index.row) {
            case 0:{
                _titleLab.text = @"当前单数";
                _detailLab.text = [NSString stringWithFormat:@"%ld个",_model.curOrderCount];
            }
                break;
            case 1:{
                _titleLab.text = @"新服务商";
                _detailLab.text = [NSString stringWithFormat:@"%ld家",_model.newSupplier];
            }
                break;
            case 2:{
                _titleLab.text = @"交易总额";
                _detailLab.text = [NSString stringWithFormat:@"%.02f元",_model.totalOrderCount];
            }
                break;
            default:
                break;
        }
    }else {
        switch (_index.row) {
            case 0:{
                _titleLab.text = @"当前服务";
                _detailLab.text = [NSString stringWithFormat:@"%ld个",_model.curServiceCount];
            }
                break;
            case 1:{
                _titleLab.text = @"涉及行业";
                _detailLab.text = [NSString stringWithFormat:@"%ld家",_model.involvedIndustryCount];
            }
                break;
            case 2:{
                _titleLab.text = @"工人入驻";
                _detailLab.text = [NSString stringWithFormat:@"%.ld名",_model.workerCount];
            }
                break;
            default:
                break;
        }
    }
    

    
}

/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.detailLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.backView.mas_top).offset(8);
    }];
    
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"当前单数";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.text = @"0个";
        _detailLab.textColor = APPTintColor;
        _detailLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _detailLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
//        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}
@end
