//
//  WGMineSetionBackLayout.h
//  DDLife
//
//  Created by wanggang on 2019/7/18.
//  Copyright © 2019年 赵越. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static NSString * const DDSetSectionBack = @"DDSetSectionBack";

@protocol WGCollectionViewDelegateFlowLayout <UICollectionViewDelegateFlowLayout>

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section;

@end
/**
 设置setion整体背景
 */
@interface WGMineSetionBackLayout : UICollectionViewFlowLayout


@end

NS_ASSUME_NONNULL_END
