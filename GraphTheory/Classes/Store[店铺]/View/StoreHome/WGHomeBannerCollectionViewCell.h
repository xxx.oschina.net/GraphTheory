//
//  WGHomeBannerCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/2.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXHomeModel,YXCateBannerListModel;
@interface WGHomeBannerCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong)void(^selectBlock)(YXCateBannerListModel *bannerModel);

@property (nonatomic,strong) NSIndexPath *index;

@property (nonatomic ,strong) YXHomeModel *model;


@end

NS_ASSUME_NONNULL_END
