//
//  WGHomeBannerCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/2.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGHomeBannerCollectionViewCell.h"
#import "SDCycleScrollView.h"
#import "YXHomeModel.h"
@interface WGHomeBannerCollectionViewCell ()<SDCycleScrollViewDelegate>

/** 商品轮播图*/
@property (nonatomic ,strong) SDCycleScrollView *goodImgScrollView;

@end

@implementation WGHomeBannerCollectionViewCell

- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    _goodImgScrollView.localizationImageNamesGroup = @[@"home_banner"];
}

- (void)setModel:(YXHomeModel *)model {
    _model = model;
    if (_model.cateBannerList.count) {
        NSMutableArray *imgArr = [NSMutableArray array];
        for (YXCateBannerListModel *listModel in _model.cateBannerList) {
            [imgArr addObject:listModel.imgbanner];
            [imgArr addObject:listModel.imgbanner];
        }
        _goodImgScrollView.imageURLStringsGroup = imgArr;
    }else {
        _goodImgScrollView.localizationImageNamesGroup = @[@"home_banner"];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.goodImgScrollView];;
        [self addLayout];
        
    }
    return self;
}

- (void)addLayout {
    [self.goodImgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
//    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.equalTo(self);
//        make.bottom.equalTo(self.mas_bottom).offset(-10);
//        make.height.equalTo(@20);
//    }];

    
}
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    if (_model.cateBannerList.count) {
        YXCateBannerListModel *listModel = _model.cateBannerList[0];
        if (self.selectBlock) {
            self.selectBlock(listModel);
        }
    }
    
}
- (SDCycleScrollView *)goodImgScrollView {
    if (!_goodImgScrollView) {
        _goodImgScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:nil];
        _goodImgScrollView.backgroundColor = [UIColor whiteColor];
        _goodImgScrollView.autoScrollTimeInterval = 5.f;
        _goodImgScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        _goodImgScrollView.localizationImageNamesGroup = @[@"home_banner"];
        _goodImgScrollView.layer.masksToBounds = YES;
        _goodImgScrollView.layer.cornerRadius = 4.0f;
        _goodImgScrollView.delegate = self;
        
    }
    return _goodImgScrollView;
}

@end
