//
//  YXStoreActivityCollectionViewCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/1/11.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXActivityModel;
@interface YXStoreActivityCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) YXActivityModel *acitvityModel;

@end

NS_ASSUME_NONNULL_END
