//
//  YXStoreActivityCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/1/11.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXStoreActivityCollectionViewCell.h"
#import "YXActivityModel.h"
@interface YXStoreActivityCollectionViewCell ()
/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UILabel *titleLab;

@end

@implementation YXStoreActivityCollectionViewCell

- (void)setAcitvityModel:(YXActivityModel *)acitvityModel {
    _acitvityModel = acitvityModel;
    _titleLab.text = [NSString stringWithFormat:@"%@",acitvityModel.activityName];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}

/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView.mas_centerY);
        make.left.equalTo(self.backView.mas_left).offset(15);
    }];
    
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"活动名称";
        _titleLab.textColor = color_TextTwo;
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}


- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        //        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}
@end
