//
//  WGCollectionViewLayoutAttributes.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes

@property (nonatomic, assign) NSInteger type;
@property (nonatomic, assign) NSInteger selectSeciton;

@property (nonatomic ,copy) void(^delectIndex)(NSInteger seciton);



@end

NS_ASSUME_NONNULL_END
