//
//  WGStoreOrderCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXRealTimeDataModel;
@interface WGStoreOrderCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSIndexPath *index;
@property (nonatomic, strong) WGStoreDetailNumModel *detail;
@property (nonatomic, strong) NSString *status;
@property (nonatomic ,strong) YXRealTimeDataModel *model;

@end

NS_ASSUME_NONNULL_END
