//
//  WGMineInfoHeadBackCollectionReusableView.h
//  DDLife
//
//  Created by wanggang on 2019/7/18.
//  Copyright © 2019年 赵越. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/**
 店铺-头视图背景
 */
@interface WGMineInfoHeadBackCollectionReusableView : UICollectionReusableView



@end

NS_ASSUME_NONNULL_END
