//
//  WGMineSetionBackLayout.m
//  DDLife
//
//  Created by wanggang on 2019/7/18.
//  Copyright © 2019年 赵越. All rights reserved.
//

#import "WGMineSetionBackLayout.h"
#import "WGMineInfoHeadBackCollectionReusableView.h"
#import "WGCollectionViewLayoutAttributes.h"

@interface WGMineSetionBackLayout ()
/**
 存储 布局的 att
 */
@property (nonatomic,strong) NSMutableArray *layoutInfoArr;

/**
 collection 滑动区域大小
 */
@property (nonatomic,assign) CGSize contentSize;
@end
@implementation WGMineSetionBackLayout

/**
 布局 布局配置数据  布局前的准备会调用这个方法
 */
- (void)prepareLayout{
    [super prepareLayout];
    [self registerClass:[WGMineInfoHeadBackCollectionReusableView class] forDecorationViewOfKind:DDSetSectionBack];

    //获取布局信息
    id delegate = self.collectionView.delegate;
    if (![delegate conformsToProtocol:@protocol(WGCollectionViewDelegateFlowLayout)]) {
        return;
    }
    [self.layoutInfoArr removeAllObjects];
//    CGRect tmpsectionFrame = CGRectZero;
    NSInteger numberOfSections = [self.collectionView numberOfSections];
    
    for (NSInteger section = 0; section < numberOfSections; section++){

        NSInteger num = [self.collectionView numberOfItemsInSection:section];
        UICollectionViewLayoutAttributes *first = nil;//组第一个 item att
        UICollectionViewLayoutAttributes *last = nil;//组最后一个 item att
        if (num>0) {
            first = [self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
            last = [self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:num-1 inSection:section]];
        }
        
        UIEdgeInsets sectionInset = self.sectionInset;
        if ([delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)]) {
            UIEdgeInsets inset = [delegate collectionView:self.collectionView layout:self insetForSectionAtIndex:section];
            if (!UIEdgeInsetsEqualToEdgeInsets(inset, UIEdgeInsetsZero)) {
                sectionInset = inset;
            }
        }
        CGSize headerSize = CGSizeZero;
        if ([delegate respondsToSelector:@selector(collectionView:layout:referenceSizeForHeaderInSection:)]) {
            CGSize size = [delegate collectionView:self.collectionView layout:self referenceSizeForHeaderInSection:section];
            if (!CGSizeEqualToSize(size, CGSizeZero)) {
                headerSize = size;
            }
        }
        CGRect sectionFrame = CGRectZero;
        if (self.scrollDirection == UICollectionViewScrollDirectionHorizontal) {
            sectionFrame = CGRectMake(CGRectGetMinX(first.frame)-headerSize.width, CGRectGetMinY(first.frame), CGRectGetMaxX(last.frame), CGRectGetMaxY(last.frame));
            sectionFrame.origin.y = sectionInset.top;
            
            sectionFrame.size.width = sectionFrame.size.width-sectionFrame.origin.x;
            sectionFrame.size.height = CGRectGetHeight(self.collectionView.frame)-sectionInset.top-sectionInset.bottom;
        } else {
            
            sectionFrame = CGRectMake(CGRectGetMinX(first.frame), CGRectGetMinY(first.frame)-headerSize.height, CGRectGetMaxX(last.frame), CGRectGetMaxY(last.frame));
            sectionFrame.origin.x = sectionInset.left;
            
            sectionFrame.size.width = CGRectGetWidth(self.collectionView.frame)-sectionInset.left-sectionInset.right;
            sectionFrame.size.height = sectionFrame.size.height-sectionFrame.origin.y;
            
        }
        WGCollectionViewLayoutAttributes *att = [WGCollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:DDSetSectionBack withIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
        att.frame = sectionFrame;
        att.zIndex = -1;
        att.hidden = NO;
        if ([delegate respondsToSelector:@selector(collectionView:layout:backgroundColorForSection:)]) {
            att.type = [delegate collectionView:self.collectionView layout:self backgroundColorForSection:section];
        }
        
        [self.layoutInfoArr addObject:att];
        
//        tmpsectionFrame = sectionFrame;
    }
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    NSArray *arr = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray *rt = [NSMutableArray array];
    [rt addObjectsFromArray:arr];
    for (UICollectionViewLayoutAttributes *att in self.layoutInfoArr) {
        if (CGRectIntersectsRect(att.frame, rect)) {
            [rt addObject:att];
        }
    }
    return rt;
}



-(NSMutableArray *)layoutInfoArr{
    if (_layoutInfoArr==nil) {
        _layoutInfoArr = [NSMutableArray array];
    }
    return _layoutInfoArr;
}

-(UICollectionViewLayoutAttributes*)layoutAttributesForDecorationViewOfKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath{
    if ([elementKind isEqualToString:DDSetSectionBack]) {
        return [self.layoutInfoArr objectAtIndex:indexPath.section];
    }
    
    UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:elementKind withIndexPath:indexPath];
    return attrs;
}
@end
