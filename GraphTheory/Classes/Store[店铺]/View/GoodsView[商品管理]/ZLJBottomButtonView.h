//
//  ZLJBottomButtonView.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLJBottomButtonView : UIView
- (instancetype)initWithButtonTitle:(NSArray <NSString *>*)titleArray buttonSize:(CGSize )buttonSize clickButton:(void(^)(UIButton *button))clickButton;

@end

NS_ASSUME_NONNULL_END
