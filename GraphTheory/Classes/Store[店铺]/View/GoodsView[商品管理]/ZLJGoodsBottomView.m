//
//  ZLJGoodsBottomView.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJGoodsBottomView.h"
#import "UIView+frameAdjust.h"

@implementation ZLJGoodsBottomEditView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self addSubview:self.selectedAllBtn];
        [self addSubview:self.xiaJiaBtn];
        [self addSubview:self.numberLab];
        [self addSubview:self.deleteBtn];
    }
    return self;
}

- (void)addNormalViewConstraints{
    [self.selectedAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(49);
        make.left.mas_equalTo(self);
        make.width.mas_equalTo(self.mas_width).multipliedBy(0.2);
        make.centerY.mas_equalTo(self);
    }];
    [self.xiaJiaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(self.selectedAllBtn.mas_height);
        make.width.mas_equalTo(self.mas_width).multipliedBy(0.25);
         make.centerY.mas_equalTo(self);
    }];
    [self.numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.selectedAllBtn.mas_height);
        make.left.mas_equalTo(self.selectedAllBtn.mas_right);
        make.right.mas_equalTo(self.xiaJiaBtn.mas_left);
         make.centerY.mas_equalTo(self);
    }];
    
}

- (void)addViewConstraintsWithDeleteBtn{
    [self.selectedAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(49);
        make.left.mas_equalTo(self);
        make.width.mas_equalTo(self.mas_width).multipliedBy(0.2);
         make.centerY.mas_equalTo(self);
    }];
    [self.xiaJiaBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self);
        make.height.mas_equalTo(self.selectedAllBtn.mas_height);
        make.width.mas_equalTo(self.mas_width).multipliedBy(0.25);
        make.left.mas_equalTo(self.deleteBtn.mas_right);
         make.centerY.mas_equalTo(self);
    }];
    [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.xiaJiaBtn.mas_left);
        make.height.mas_equalTo(self.selectedAllBtn.mas_height);
        make.width.mas_equalTo(self.mas_width).multipliedBy(0.25);
         make.centerY.mas_equalTo(self);
    }];
    
    [self.numberLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.selectedAllBtn.mas_height);
        make.left.mas_equalTo(self.selectedAllBtn.mas_right);
        make.right.mas_equalTo(self.deleteBtn.mas_left);
        make.centerY.mas_equalTo(self);
    }];
}


-(ZLJBlockButton *)selectedAllBtn{
    if (!_selectedAllBtn) {
        _selectedAllBtn = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
        [_selectedAllBtn setTitle:@"全选" forState:UIControlStateNormal];
        [_selectedAllBtn setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        _selectedAllBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [_selectedAllBtn setImage:[UIImage imageNamed:@"cell_normal"] forState:UIControlStateNormal];
        [_selectedAllBtn setImage:[UIImage imageNamed:@"cell_selected"] forState:UIControlStateSelected];
        _selectedAllBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -2, 0, 0);
        _selectedAllBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    }
    return _selectedAllBtn;
}

-(ZLJBlockButton *)xiaJiaBtn{
    if (!_xiaJiaBtn) {
        _xiaJiaBtn = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
        [_xiaJiaBtn setTitle:@"下架" forState:UIControlStateNormal];
        [_xiaJiaBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        _xiaJiaBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _xiaJiaBtn.backgroundColor = [UIColor colorWithHexString:@"#FF6F24"];
    }
    return _xiaJiaBtn;
}

-(ZLJBlockButton *)deleteBtn{
    if (!_deleteBtn) {
        _deleteBtn = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _deleteBtn.backgroundColor = [UIColor colorWithHexString:@"#E41818"];
    }
    return _deleteBtn;
}

-(UILabel *)numberLab{
    if (!_numberLab) {
        _numberLab = [[UILabel alloc] init];
        _numberLab.font = [UIFont systemFontOfSize:16.0f];
        _numberLab.textAlignment = NSTextAlignmentCenter;
        _numberLab.textColor = [UIColor colorWithHexString:@"333333"];
        [_numberLab setAdjustsFontSizeToFitWidth:YES];
        _numberLab.text  = @"已选择0个";
    }
    return _numberLab;
}

@end


@interface ZLJGoodsBottomView ()

@property (nonatomic,strong) UIView *normalView;//正常状态view


@end


@implementation ZLJGoodsBottomView


- (instancetype)initShowDeleteBtn:(BOOL )showDeleteBtn
{
    self = [super init];
    if (self) {
        self.layer.zPosition = 1.0f;  
        [self addEditView];
        if (showDeleteBtn) {
            [self.editView addViewConstraintsWithDeleteBtn];
        }else{
            [self.editView addNormalViewConstraints];
        }
        
        [self addNormalView];
    }
    return self;
}

- (void)showSubviewWithEditType:(BOOL )editType{
    if (editType) {//编辑状态
        self.editView.selectedAllBtn.selected = NO;
        self.editView.numberLab.text = [NSString stringWithFormat:@"已选择0个"];
        [self bringSubviewToFront:self.editView];
    }else{//正常转态
        [self bringSubviewToFront:self.normalView];
    }
}

//正常状态
- (void)addNormalView{
    [self addSubview:self.normalView];
    [self.normalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.normalView addSubview:self.goodsGroupBtn];
    [self.normalView addSubview:self.addGoodsBtn];
    
    [@[self.goodsGroupBtn,self.addGoodsBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(self);
        make.height.mas_equalTo(49);
    }];
    [self.goodsGroupBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self);
        make.right.mas_equalTo(self.addGoodsBtn.mas_left);
    }];
    
    [self.addGoodsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self);
        make.left.mas_equalTo(self.goodsGroupBtn.mas_right);
        make.width.mas_equalTo(self.goodsGroupBtn.mas_width).multipliedBy(2);
    }];
}
-(UIView *)normalView{
    if (!_normalView) {
        _normalView = [[UIView alloc] init];
        _normalView.backgroundColor = UIColor.whiteColor;
    }
    return _normalView;
}
-(ZLJBlockButton *)goodsGroupBtn{
    if (!_goodsGroupBtn) {
//        DDWeakSelf
        _goodsGroupBtn = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
        [_goodsGroupBtn setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        [_goodsGroupBtn setTitle:@"商品分组" forState:UIControlStateNormal];
        _goodsGroupBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _goodsGroupBtn.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    }
    return _goodsGroupBtn;
}

-(ZLJBlockButton *)addGoodsBtn{
    if (!_addGoodsBtn) {
        _addGoodsBtn = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
        [_addGoodsBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        [_addGoodsBtn setTitle:@"新增商品" forState:UIControlStateNormal];
        _addGoodsBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        _addGoodsBtn.backgroundColor = [UIColor colorWithHexString:@"FF6F24"];
    }
    return _addGoodsBtn;
}
//编辑状态
- (void)addEditView{
    [self addSubview:self.editView];
    [self.editView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(ZLJGoodsBottomEditView *)editView{
    if (!_editView) {
        _editView = [[ZLJGoodsBottomEditView alloc] init];

    }
    return _editView;
}
@end


