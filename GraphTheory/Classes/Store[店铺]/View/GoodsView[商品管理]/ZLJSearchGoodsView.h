//
//  ZLJSearchGoodsView.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLJSearchGoodsView : UIView

/**
 添加带有placeholder的搜索框
 */
- (instancetype)initWithPlaceHoder:(NSString *)placeholder;
@property (nonatomic ,strong) UIColor *textBackColor;///<输入框背景颜色

@property (nonatomic ,copy) void(^search)(NSString *keyWord);///<搜索回调
@property (nonatomic,copy) void(^characterChange)(NSString *keyWord);//实时输出
- (void)searchEndEditing;
@end

NS_ASSUME_NONNULL_END
