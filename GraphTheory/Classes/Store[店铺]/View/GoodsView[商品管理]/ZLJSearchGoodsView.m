//
//  ZLJSearchGoodsView.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//商品管理顶部搜索框

#import "ZLJSearchGoodsView.h"

@interface ZLJSearchGoodsView ()<UITextFieldDelegate>


@property (nonatomic,strong) LTLimitTextField *searchTF;//搜索框
@end

@implementation ZLJSearchGoodsView
- (void)searchEndEditing{
    [self endEditing:YES];
    if (self.searchTF.text.length > 0) {
        if (self.search) {
            self.search(self.searchTF.text);
        }
    }
//    self.searchTF.text = @"";
}

- (instancetype)initWithPlaceHoder:(NSString *)placeholder
{
    self = [super init];
    if (self) {
        
        [self addSubview:self.searchTF];
//        [self.searchTF  becomeFirstResponder];
        [self searchTFPlaceholder:placeholder];
        [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
        }];
    }
    return self;
}
- (void)setTextBackColor:(UIColor *)textBackColor{
    _textBackColor = textBackColor;
    _searchTF.backgroundColor = textBackColor;
}
- (LTLimitTextField *)searchTF{
    if (!_searchTF) {
        _searchTF = [LTLimitTextField new];
        _searchTF.backgroundColor = [UIColor colorWithHexString:@"ffffff"];
        _searchTF.layer.cornerRadius = 10.0f;
        _searchTF.layer.masksToBounds  = YES;
        _searchTF.textAlignment = NSTextAlignmentCenter;
        _searchTF.delegate = self;
        _searchTF.returnKeyType = UIReturnKeySearch;
        [_searchTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _searchTF;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (self.search) {
        [textField resignFirstResponder];
        self.search(textField.text);
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    if (self.search) {
//        [textField resignFirstResponder];
//        self.search(textField.text);
//    }
    [textField resignFirstResponder];
    return YES;
}
- (void)textFieldChanged:(UITextField *)textField{
     NSLog(@"zlj--当前输出%@：%s",textField.text,__FUNCTION__);
    if (self.characterChange) {
        self.characterChange(textField.text);
    }
}
- (void)searchTFPlaceholder:(NSString *)placeholder{
    
    if (placeholder.length == 0) {
        return;
    }
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.alignment = NSTextAlignmentCenter;
    NSDictionary    *dic = @{NSForegroundColorAttributeName:[UIColor colorWithHexString:@"8E8E93"],
                             NSFontAttributeName:[UIFont systemFontOfSize:16], 
                             NSParagraphStyleAttributeName:style};
    NSAttributedString *attri = [[NSAttributedString alloc] initWithString:placeholder attributes:dic];
    self.searchTF.attributedPlaceholder = attri;
}
- (CGSize)intrinsicContentSize {
    return CGSizeMake(SCREEN_WIDTH * 3 / 4.0f, 36);
}
@end
