//
//  ZLJGoodsSortView.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/23.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJGoodsSortView.h"


@interface ZLJGoodsSortView ()

@property (nonatomic,strong) NSMutableArray *viewArray;//视图array

@property (nonatomic,copy) SortButtonClickBlock sortButtonBlock;
@end

@implementation ZLJGoodsSortView

- (void)setTitle:(NSString *)title {
    _title = title;
    if (self.viewArray.count > 0) {
        ZLJGoodsSortButton *cellView =  [self.viewArray lastObject];
        [cellView setTitle:title forState:UIControlStateNormal];
        [cellView setImage:[UIImage imageNamed:@"button_down"] forState:UIControlStateNormal];
        [cellView setImage:[UIImage imageNamed:@"button_down"] forState:UIControlStateHighlighted];
        [cellView setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(12, 6)) space:5];
    }
}


- (instancetype)initWithFrame:(CGRect)frame withTitleArray:(NSArray <NSString *>*)titleArray clickButton:(nonnull SortButtonClickBlock)clickButton{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        self.sortButtonBlock =  clickButton;
        UIView  *lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHexString:@"f4f4f4"];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(15);
            make.right.bottom.mas_equalTo(self);
            make.height.mas_equalTo(@1);
        }];
        
        self.viewArray = [NSMutableArray array];
        for (int i = 0; i < titleArray.count; i++ ) {
            
            NSString    *titleString = titleArray[i];
            ZLJGoodsSortButton    *cellView = [ZLJGoodsSortButton buttonWithType:UIButtonTypeCustom];
            [cellView buttonImage:cellView buttonTitle:titleString];
            cellView.tag = i;
            cellView.highlighted = YES;
//            cellView.selected = YES;
            [self.viewArray addObject:cellView];
            [cellView addTarget:self action:@selector(cellViewClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:cellView];
        }
        [self.viewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:5 tailSpacing:5];
        [self.viewArray mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.mas_equalTo(44);
        }];
        [self layoutIfNeeded];
        for (ZLJGoodsSortButton *button in self.viewArray) { // 此处 #import "ZLJGoodsSaleingVC.h" 同样有设置 -- ZLJ
            CGSize imageSize = button.imageView.frame.size;
            CGSize titleSize = button.titleLabel.frame.size;
            // titleLabel的宽度不一定正确的时候，需要进行判断
            CGFloat labelWidth = button.titleLabel.intrinsicContentSize.width;
            if (titleSize.width < labelWidth) {
                titleSize.width = labelWidth;
            }
            // 文字距左边框的距离减少imageView的宽度-间距，右侧增加距离imageView的宽度
            [button setTitleEdgeInsets:UIEdgeInsetsMake(-1.0, -imageSize.width - 8, 0.0, imageSize.width)];
            // 图片距左边框的距离增加titleLable的宽度,距右边框的距离减少titleLable的宽度
            [button setImageEdgeInsets:UIEdgeInsetsMake(0.0, titleSize.width,0.0,-titleSize.width)];
        }
    }
    return self;
}
- (void)cellViewClick:(UIButton *)sender{

    BOOL senderSelected = sender.selected;
    for (ZLJGoodsSortButton    *cellView in self.viewArray) {
        cellView.selected = NO;
       cellView.highlighted = YES;
    }
    if (sender.titleLabel.text.length == 0 || sender.titleLabel.text == nil) {
        return;
    }
    sender.selected = !senderSelected;
    if (self.sortButtonBlock) {
        self.sortButtonBlock(sender);
    }
}
//
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    UITouch *touch = [touches anyObject];
//    CGPoint point = [touch locationInView:self];
//    for (ZLJGoodsSortButton *button in self.viewArray) {
//        if (CGRectContainsPoint(button.frame, point)) {
//           button.selected = !button.selected;
//        }
//    }
//
//}
@end


@implementation ZLJGoodsSortButton

+ (instancetype)buttonWithType:(UIButtonType)buttonType{
    ZLJGoodsSortButton  *sortButton = [super buttonWithType:buttonType];
    if (sortButton) {
        [sortButton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        sortButton.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    }
    return sortButton;
}

- (void)buttonImage:(ZLJGoodsSortButton *)sortButton  buttonTitle:(NSString *)title{
    
    if (sortButton == nil || title.length == 0) {
        return;
    }
    [sortButton setTitle:title forState:UIControlStateNormal];
    if ([title isEqualToString:@"审核状态"]) {
        return;
    }
    if ([title isEqualToString:@"全部分组"]) {
        [sortButton setImage:[UIImage imageNamed:@"button_down"] forState:UIControlStateNormal];
        [sortButton setImage:[UIImage imageNamed:@"button_down"] forState:UIControlStateHighlighted];
        
    }else{
        
        [sortButton setImage:[UIImage imageNamed:@"selected_down"] forState:UIControlStateNormal];
        [sortButton setImage:[UIImage imageNamed:@"selected_up"] forState:UIControlStateSelected];
        [sortButton setImage:[UIImage imageNamed:@"selected_no"] forState:UIControlStateHighlighted];
    }
    
}

@end

