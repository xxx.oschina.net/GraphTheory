//
//  ZLJGoodsBottomView.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
/*
 商品管理底部视图
 
 |__________商品分组_______新增商品___________|
 |                                         |
 */

#import <UIKit/UIKit.h>
#import "ZLJBlockButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLJGoodsBottomEditView : UIView

@property (nonatomic,strong) ZLJBlockButton *selectedAllBtn;//全选

@property (nonatomic,strong) ZLJBlockButton *xiaJiaBtn;//下架

@property (nonatomic,strong) ZLJBlockButton *deleteBtn;//删除

@property (nonatomic,strong) UILabel *numberLab;//选择个数

//添加 |全选 -- 已选择 -- 下架|  UI
- (void)addNormalViewConstraints;


//添加 |全选 -- 已选择 -- 删除 -- 下架|  UI
- (void)addViewConstraintsWithDeleteBtn;
@end



@interface ZLJGoodsBottomView : UIView


@property (nonatomic,strong) ZLJBlockButton *goodsGroupBtn;//商品分组

@property (nonatomic,strong) ZLJBlockButton *addGoodsBtn;//新增商品

@property (nonatomic,strong) ZLJGoodsBottomEditView *editView;//编辑转态view

/**
 初始化方法

 @param showDeleteBtn 是否显示删除按钮
 @return 当前类
 */
- (instancetype)initShowDeleteBtn:(BOOL )showDeleteBtn;
/**
 编辑 、非编辑状态

 @param editType 编辑状态
 */
- (void)showSubviewWithEditType:(BOOL )editType;
@end

NS_ASSUME_NONNULL_END
