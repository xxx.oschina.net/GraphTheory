//
//  WGGoodGroupCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLJFindItemsGroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGGoodGroupCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) FindItemsGroupDataItem *model;///<分组信息

@property (nonatomic ,copy) void(^goodGroupAction)(NSInteger type);///<1:修改 2:删除

@end

NS_ASSUME_NONNULL_END
