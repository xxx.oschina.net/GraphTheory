//
//  WGGoodGroupAddGoodCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGGoodGroupAddGoodCollectionViewCell.h"

@interface WGGoodGroupAddGoodCollectionViewCell ()

@property (nonatomic,strong) UILabel *nameLab;//名称

@property (nonatomic,strong) UIButton *addBtn;//值

@end


@implementation WGGoodGroupAddGoodCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setModel:(FindItemsGroupDataItem *)model{
    _model = model;
    _nameLab.text = [NSString stringWithFormat:@"分组商品(%@)",model.num?:@"0"];
}
- (void)setupSubviews{
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.addBtn];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.height.mas_equalTo(40);
    }];
}
- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab= [UILabel new];
        _nameLab.textColor = [UIColor colorWithHexString:@"333333"];
        _nameLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _nameLab;
}
- (UIButton *)addBtn{
    if (!_addBtn) {
        DDWeakSelf
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn setTitle:@"添加商品" forState:UIControlStateNormal];
        [_addBtn setTitleColor:APPTintColor forState:UIControlStateNormal];
        _addBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_addBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.addGoodGroup) {
                weakSelf.addGoodGroup();
            }
        }];
    }
    return _addBtn;
}
@end
