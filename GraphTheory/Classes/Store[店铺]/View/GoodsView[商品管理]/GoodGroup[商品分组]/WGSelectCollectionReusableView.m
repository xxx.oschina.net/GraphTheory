//
//  WGSelectCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGSelectCollectionReusableView.h"

@interface WGSelectCollectionReusableView ()

@property (nonatomic,strong) UILabel *nameLab;//名称

@property (nonatomic,strong) UIButton *selectedAllBtn;//值

@end


@implementation WGSelectCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setIsAllSelcet:(BOOL)isAllSelcet{
    _isAllSelcet = isAllSelcet;
    _selectedAllBtn.selected = _isAllSelcet;
}
- (void)setupSubviews{
    [self addSubview:self.nameLab];
    [self addSubview:self.selectedAllBtn];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.selectedAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
}
- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab= [UILabel new];
        _nameLab.textColor = [UIColor colorWithHexString:@"333333"];
        _nameLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _nameLab.text = @"全选";
    }
    return _nameLab;
}


-(UIButton *)selectedAllBtn{
    if (!_selectedAllBtn) {
        DDWeakSelf;
        _selectedAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectedAllBtn setImage:[UIImage imageNamed:@"cell_normal"] forState:UIControlStateNormal];
        [_selectedAllBtn setImage:[UIImage imageNamed:@"cell_selected"] forState:UIControlStateSelected];
        [_selectedAllBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.selectAll) {
                btn.selected = !btn.selected;
                weakSelf.isAllSelcet = btn.selected;
                weakSelf.selectAll(weakSelf.selectedAllBtn.selected);
            }
        }];
    }
    return _selectedAllBtn;
}


@end
