//
//  WGGoodCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class ItemsManageItemList;

@interface WGGoodCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) ItemsManageItemList *model;

@property (nonatomic ,copy) void(^selectStatus)(BOOL isSelect);///<全选回调


@end

NS_ASSUME_NONNULL_END
