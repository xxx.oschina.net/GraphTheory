//
//  WGGoodCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGGoodCollectionViewCell.h"
#import "ZLJItemsManageModel.h"

@interface WGGoodCollectionViewCell ()

@property (nonatomic,strong) UIView *editBackView;//点击编辑时候使用 -- 父视图（除编辑选择框）

@property (nonatomic,strong) UIImageView *goodsImgView;//商品图片

@property (nonatomic,strong) UILabel *moneyLab;//金额

@property (nonatomic,strong) UILabel *kucunLab;//库存

@property (nonatomic,strong) UILabel *xiaoliangLab;//销量

@property (nonatomic,strong) UILabel *titleLab;//标题

@property (nonatomic,strong) UIButton *cellSelectedBtn;//cell选择转态Button
@end


@implementation WGGoodCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setModel:(ItemsManageItemList *)model{
    _model = model;
    NSString *imgString = _model.imagePath;
    
    DD_STRING_NN(imgString);
    NSURL *imgUrl = [NSURL URLWithString:[imgString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    [self.goodsImgView sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
    DD_STRING_NN(_model.price);
    NSString    *priceString = [NSString stringWithFormat:@"￥%@",_model.price];
    self.moneyLab.text = priceString;
    self.kucunLab.text = [NSString stringWithFormat:@"库存:%@",_model.itemStock?:@"0"];
    self.xiaoliangLab.text = [NSString stringWithFormat:@"销量:%@",_model.totalSales?:@"0"];
    DD_STRING_NN(_model.itemName);
    self.titleLab.attributedText = [self titleLabelString:_model.itemInfo];
    self.cellSelectedBtn.selected = _model.selected;
}
- (NSAttributedString *)titleLabelString:(NSString *)string{
    
    //NSKernAttributeName:@1.5f 字间距
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineSpacing = 10.0f;
    paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;//省略号位置
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                          NSParagraphStyleAttributeName:paraStyle};
    NSAttributedString  *attributeStr = [[NSAttributedString alloc] initWithString:string attributes:dic];
    return attributeStr;
}

- (void)setupSubviews{
    
    [self.contentView addSubview:self.editBackView];
    [self.editBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    //添加视图
    [self.editBackView addSubview:self.goodsImgView];
    [self.editBackView addSubview:self.moneyLab];
    [self.editBackView addSubview:self.kucunLab];
    [self.editBackView addSubview:self.xiaoliangLab];
    [self.editBackView addSubview:self.titleLab];
    
    [self.editBackView addSubview:self.cellSelectedBtn];
    
    CGFloat imgViewWidth = 70.0f;
    CGFloat moneyLabToLeft = 95.0f; // 图片到右侧距离
    CGFloat titleLabLineHeight = 24.0f;//titlelLable单行高度
    
    self.moneyLab.font = [UIFont systemFontOfSize:16.0f];
    self.kucunLab.font = [UIFont systemFontOfSize:12.0f];
    self.xiaoliangLab.font = [UIFont systemFontOfSize:12.0f];
    
    //布局
    [self.goodsImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.size.mas_equalTo(CGSizeMake(imgViewWidth, imgViewWidth));
        make.centerY.equalTo(self.editBackView);
    }];
    NSArray *viewArray = @[self.moneyLab,self.kucunLab,self.xiaoliangLab];
    [viewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:moneyLabToLeft tailSpacing:40];
    [viewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodsImgView);
        make.height.mas_equalTo(@16);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsImgView);
        make.left.mas_equalTo(self.goodsImgView.mas_right).offset(10);
        make.right.mas_equalTo(self.cellSelectedBtn.mas_left).offset(-10);
        make.height.mas_greaterThanOrEqualTo(titleLabLineHeight);
    }];
    [self.cellSelectedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.editBackView).offset(-15);
        make.centerY.equalTo(self.editBackView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
}
//TODO:视图布局
- (UIButton *)cellSelectedBtn{
    if (!_cellSelectedBtn) {
        DDWeakSelf;
        _cellSelectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cellSelectedBtn setImage:[UIImage imageNamed:@"cell_normal"] forState:UIControlStateNormal];
        [_cellSelectedBtn setImage:[UIImage imageNamed:@"cell_selected"] forState:UIControlStateSelected];
        [_cellSelectedBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.selectStatus) {
                btn.selected = !btn.selected;
                weakSelf.model.selected = btn.selected;
                weakSelf.selectStatus(weakSelf.cellSelectedBtn.selected);
            }
        }];
    }
    return _cellSelectedBtn;
}

- (UIView *)editBackView{
    if (!_editBackView) {
        _editBackView = [[UIView alloc] init];
        _editBackView.backgroundColor = UIColor.whiteColor;
    }
    return _editBackView;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor colorWithHexString:@"333333"];
        _titleLab.numberOfLines = 2;
    }
    return _titleLab;
}

-(UILabel *)moneyLab{//金额
    if (!_moneyLab) {
        _moneyLab = [[UILabel alloc] init];
        _moneyLab.textColor = [UIColor colorWithHexString:@"E41818"];
        
    }
    return _moneyLab;
}

-(UILabel *)kucunLab{//库存
    if (!_kucunLab) {
        _kucunLab = [[UILabel alloc] init];
        _kucunLab.textColor = [UIColor colorWithHexString:@"666666"];
        _kucunLab.textAlignment = NSTextAlignmentCenter;
    }
    return _kucunLab;
}

-(UILabel *)xiaoliangLab{//销量
    if (!_xiaoliangLab) {
        _xiaoliangLab = [[UILabel alloc] init];
        _xiaoliangLab.textColor = [UIColor colorWithHexString:@"666666"];
        _xiaoliangLab.textAlignment = NSTextAlignmentCenter;
        
    }
    return _xiaoliangLab;
}

- (UIImageView *)goodsImgView{//图片视图
    if (!_goodsImgView) {
        _goodsImgView = [[UIImageView alloc] init];
    }
    return _goodsImgView;
}

@end
