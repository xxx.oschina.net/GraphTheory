//
//  WGGoodGroupCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGGoodGroupCollectionViewCell.h"

@interface WGGoodGroupCollectionViewCell ()
@property (nonatomic,strong) UIView *backView;///<背景
@property (nonatomic,strong) UIView *topView;///<背景
@property (nonatomic,strong) UIView *bottomView;///<背景


@property (nonatomic,strong) UILabel *name;///<分组名
@property (nonatomic,strong) UILabel *goodCount;///<商品数量
@property (nonatomic,strong) UIButton *deletBtn;///<删除
@property (nonatomic,strong) UIButton *changeBtnBtn;///<修改

@end

@implementation WGGoodGroupCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 4;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self setupSubviews];
    }
    return self;
}
- (void)setModel:(FindItemsGroupDataItem *)model{
    _model = model;
    _name.text = model.groupName;
    _goodCount.text = [NSString stringWithFormat:@"商品%@件",model.num];
}
- (void)setupSubviews{
    [self.contentView addSubview:self.backView];
    [self.contentView addSubview:self.topView];
    [self.contentView addSubview:self.bottomView];
    [self.topView addSubview:self.name];
    [self.topView addSubview:self.goodCount];
    [self.bottomView addSubview:self.deletBtn];
    [self.bottomView addSubview:self.changeBtnBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.backView);
        make.height.mas_equalTo(44);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.backView);
        make.top.equalTo(self.topView.mas_bottom).offset(1);
    }];
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.topView).offset(15);
        make.centerY.equalTo(self.topView);
    }];
    [self.goodCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topView).offset(-15);
        make.centerY.equalTo(self.topView);
    }];
    [self.changeBtnBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.bottomView);
        make.width.equalTo(self.deletBtn.mas_width);
        make.right.equalTo(self.deletBtn.mas_left).offset(-1);
    }];
    [self.deletBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self.bottomView);
        make.width.equalTo(self.deletBtn.mas_width);
        make.left.equalTo(self.changeBtnBtn.mas_right).offset(1);
    }];

}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = color_LineColor;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)topView{
    if (!_topView) {
        _topView = [UIView new];
        _topView.backgroundColor = [UIColor whiteColor];
    }
    return _topView;
}
- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor clearColor];
    }
    return _bottomView;
}
- (UILabel *)name{
    if (!_name) {
        _name = [UILabel new];
        _name.textColor = color_TextOne;
        _name.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _name;
}
- (UILabel *)goodCount{
    if (!_goodCount) {
        _goodCount = [UILabel new];
        _goodCount.textColor = color_TextOne;
        _goodCount.font = [UIFont systemFontOfSize:14.0f];
    }
    return _goodCount;
}
- (UIButton *)changeBtnBtn{
    if (!_changeBtnBtn) {
        DDWeakSelf
        _changeBtnBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _changeBtnBtn.backgroundColor = [UIColor whiteColor];
        [_changeBtnBtn setTitle:@"修改" forState:UIControlStateNormal];
        [_changeBtnBtn setTitleColor:color_TextThree forState:UIControlStateNormal];
        _changeBtnBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_changeBtnBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.goodGroupAction) {
                weakSelf.goodGroupAction(1);
            }
        }];
    }
    return _changeBtnBtn;
}
- (UIButton *)deletBtn{
    if (!_deletBtn) {
        DDWeakSelf
        _deletBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deletBtn.backgroundColor = [UIColor whiteColor];

        [_deletBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deletBtn setTitleColor:color_TextThree forState:UIControlStateNormal];
        _deletBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_deletBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.goodGroupAction) {
                weakSelf.goodGroupAction(2);
            }

        }];
    }
    return _deletBtn;
}

@end
