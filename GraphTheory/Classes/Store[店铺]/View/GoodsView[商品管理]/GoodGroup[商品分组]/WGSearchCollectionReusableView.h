//
//  WGSearchCollectionReusableView.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGSearchCollectionReusableView : UICollectionReusableView

@property (nonatomic ,copy) void(^searchGood)(NSString *text);///<搜索回调


@end

NS_ASSUME_NONNULL_END
