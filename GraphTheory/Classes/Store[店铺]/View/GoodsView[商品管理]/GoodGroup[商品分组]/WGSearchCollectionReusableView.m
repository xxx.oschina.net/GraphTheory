//
//  WGSearchCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGSearchCollectionReusableView.h"
#import "ZLJSearchGoodsView.h"

@interface WGSearchCollectionReusableView ()

@property (nonatomic,strong) ZLJSearchGoodsView *searchGoodsView;//搜索商品

@end


@implementation WGSearchCollectionReusableView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addLayout];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}
- (void)addLayout{
    [self addSubview:self.searchGoodsView];
    [self.searchGoodsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(15, 15, 15, 15));
    }];
}
- (ZLJSearchGoodsView *)searchGoodsView{
    if (!_searchGoodsView) {
        DDWeakSelf
        _searchGoodsView = [[ZLJSearchGoodsView alloc] initWithPlaceHoder:@"搜索"];
        _searchGoodsView.search = ^(NSString * _Nonnull keyWord) {
            if (weakSelf.searchGood) {
                weakSelf.searchGood(keyWord);
            }
        };
        _searchGoodsView.textBackColor = [UIColor colorWithHexString:@"#F8F8F8"];
    }
    return _searchGoodsView;
}

@end
