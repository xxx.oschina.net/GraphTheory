//
//  WGGoodGroupAddGoodCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLJFindItemsGroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGGoodGroupAddGoodCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy) void(^addGoodGroup)(void);///<添加商品分组
@property (nonatomic ,strong) FindItemsGroupDataItem *model;///<分组信息


@end

NS_ASSUME_NONNULL_END
