//
//  WGGoodGroupView.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseView.h"
#import "ZLJFindItemsGroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGGoodGroupView : DDBaseView

@property (nonatomic ,copy) void(^deletGood)(void);///<删除商品
@property (nonatomic, assign) NSInteger type;///<0:删除 1:保存

@property (nonatomic, strong) FindItemsGroupDataItem *groupModel;
@property (nonatomic, strong) NSMutableArray *arr;


@end

NS_ASSUME_NONNULL_END
