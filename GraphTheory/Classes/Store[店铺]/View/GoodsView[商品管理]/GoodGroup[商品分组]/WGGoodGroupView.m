//
//  WGGoodGroupView.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGGoodGroupView.h"

@interface WGGoodGroupView ()

@property (nonatomic,strong) UILabel *countLab;//数量

@property (nonatomic,strong) UIButton *deletBtn;//删除

@end


@implementation WGGoodGroupView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setGroupModel:(FindItemsGroupDataItem *)groupModel{
    _groupModel = groupModel;
    _countLab.text = [NSString stringWithFormat:@"已选择%lu个",(unsigned long)groupModel.selectArr.count];
    [_deletBtn setTitle:_groupModel.kId.length?@"删除":@"删除" forState:UIControlStateNormal];
}
- (void)setArr:(NSMutableArray *)arr{
    _arr = arr;
    _countLab.text = [NSString stringWithFormat:@"已选择%lu个",(unsigned long)arr.count];
    [_deletBtn setTitle:@"保存" forState:UIControlStateNormal];
}
- (void)setupSubviews{
    [self addSubview:self.countLab];
    [self addSubview:self.deletBtn];
    [self.countLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.deletBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.top.bottom.mas_equalTo(self);
        make.width.mas_equalTo(125);
    }];
}
- (UILabel *)countLab{
    if (!_countLab) {
        _countLab= [UILabel new];
        _countLab.textColor = [UIColor colorWithHexString:@"333333"];
        _countLab.font = [UIFont systemFontOfSize:16.0f];
    }
    return _countLab;
}


-(UIButton *)deletBtn{
    if (!_deletBtn) {
        DDWeakSelf;
        _deletBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deletBtn.backgroundColor = color_RedColor;
        [_deletBtn setTitle:@"删除" forState:UIControlStateNormal];
        [_deletBtn setTitleColor:[UIColor colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
        _deletBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [_deletBtn addAction:^(UIButton * _Nonnull btn) {
            if (weakSelf.deletGood) {
                weakSelf.deletGood();
            }
        }];
    }
    return _deletBtn;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
