//
//  WGSelectCollectionReusableView.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGSelectCollectionReusableView : UICollectionReusableView

@property (nonatomic ,copy) void(^selectAll)(BOOL isSelctAll);///<全选回调
@property (nonatomic ,assign) BOOL isAllSelcet;///<是否全部选择


@end

NS_ASSUME_NONNULL_END
