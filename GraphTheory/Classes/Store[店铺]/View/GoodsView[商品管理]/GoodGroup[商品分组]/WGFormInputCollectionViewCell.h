//
//  WGFormInputCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 通用cell输入框样式

 - WGFormInputType: 样式
 */
typedef NS_ENUM(NSInteger ,WGFormInputType){
    WGFormInputTypeGroupName,///<分组名称
    WGFormInputTypeGroupSort,///<分组排序
};

/**
 通用cell 格式: 名称————————————————————————输入框
 */
@interface WGFormInputCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) NSString *valueStr;///<值
@property (nonatomic,assign) WGFormInputType type;
@property (nonatomic ,copy) void(^formInputTextChange)(WGFormInputType type,NSString *text);///<表单修改回调


@end

NS_ASSUME_NONNULL_END
