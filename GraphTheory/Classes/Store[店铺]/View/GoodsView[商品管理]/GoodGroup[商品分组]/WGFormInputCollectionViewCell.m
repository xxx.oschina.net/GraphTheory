//
//  WGFormInputCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/18.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGFormInputCollectionViewCell.h"
#import "LTLimitTextField.h"


@interface WGFormInputCollectionViewCell ()

@property (nonatomic,strong) UILabel *nameLab;//名称

@property (nonatomic,strong) LTLimitTextField *valueTextTF;//值
@end


@implementation WGFormInputCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setType:(WGFormInputType)type{
    _type = type;
    switch (type) {
        case WGFormInputTypeGroupName:
        {
            _nameLab.text = @"分组名称";
            _valueTextTF.placeholder = @"请输入";
            _valueTextTF.keyboardType = UIKeyboardTypeDefault;
            _valueTextTF.textLimitInputType = LTTextLimitInputTypeNone;
            _valueTextTF.textLimitType = LTTextLimitTypeNone;

        }
            break;
        case WGFormInputTypeGroupSort:
        {
            _nameLab.text = @"排序";
            _valueTextTF.placeholder = @"请输入";
            _valueTextTF.keyboardType = UIKeyboardTypeNumberPad;
            _valueTextTF.textLimitInputType = LTTextLimitInputTypeNumber;
            _valueTextTF.textLimitType = LTTextLimitTypeLength;
            _valueTextTF.textLimitSize = 3;
        }
            break;

            
        default:
            break;
    }
}
- (void)setValueStr:(NSString *)valueStr{
    _valueStr = valueStr;
    _valueTextTF.text = _valueStr;
}
- (void)textFieldChanged:(UITextField *)text{
    if (self.formInputTextChange) {
        self.formInputTextChange(_type, text.text);
    }
}
- (void)setupSubviews{
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.valueTextTF];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.valueTextTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.left.mas_equalTo(self.nameLab.mas_right).offset(15);
        make.height.mas_equalTo(40);
    }];
}
- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab= [UILabel new];
        _nameLab.textColor = [UIColor colorWithHexString:@"333333"];
        _nameLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _nameLab;
}
- (LTLimitTextField *)valueTextTF{
    if (!_valueTextTF) {
        _valueTextTF= [LTLimitTextField new];
        _valueTextTF.textAlignment = NSTextAlignmentRight;
        _valueTextTF.font = [UIFont systemFontOfSize:14.0f];
        _valueTextTF.textColor = [UIColor colorWithHexString:@"333333"];
        [_valueTextTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

    }
    return _valueTextTF;
}
@end
