//
//  WGAddGoodsSpesCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAddGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGAddGoodsSpesCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGAddGoodsSpecDetailModel *model;


@end

NS_ASSUME_NONNULL_END
