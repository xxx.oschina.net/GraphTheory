//
//  ZLJAddCollectSelectedCell.h
//  BusinessFine
//
//  Created by TomLong on 2019/10/14.
//  Copyright © 2019 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLJAddCollectSelectedCell : UICollectionViewCell

@property (nonatomic,strong) UILabel *leftNameLab;//左边名称
@property (nonatomic,strong) UILabel *rightNameLab;//右边名称

@end

NS_ASSUME_NONNULL_END
