//
//  WGAddGoodsAddImageCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGAddGoodsAddImageCollectionViewCell.h"
//goods_add_addImage
@interface WGAddGoodsAddImageCollectionViewCell ()
@property (nonatomic,strong) UIImageView *imageView;//图片
@end

@implementation WGAddGoodsAddImageCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setImage:(UIImage *)image{
    _image = image;
    _imageView.image = image?:[UIImage imageNamed:@"goods_add_addImage"];
}
- (void)setupSubviews{
    [self addSubview:self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).mas_offset(UIEdgeInsetsMake(10, 0, 10, 0));
    }];
}

#pragma mark -- lazy load
-(UIImageView *)imageView{
    if (nil == _imageView) {
        _imageView = [UIImageView new];
        _imageView.image = [UIImage imageNamed:@"goods_add_addImage"];
    }
    return _imageView;
}
@end
