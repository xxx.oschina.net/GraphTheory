//
//  ZLJAddGoodsNameCell.h
//  BusinessFine
//
//  Created by TomLong on 2019/10/14.
//  Copyright © 2019 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGAddGoodsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZLJAddGoodsNameCell : UICollectionViewCell

@property (nonatomic, strong) WGAddGoodsModel *model;

@end

NS_ASSUME_NONNULL_END
