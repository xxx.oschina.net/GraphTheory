//
//  WGAddGoodBackCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGAddGoodBackCollectionReusableView.h"

#import "WGCollectionViewLayoutAttributes.h"

@interface WGAddGoodBackCollectionReusableView ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIButton *deletBtn;
@property (nonatomic, strong) WGCollectionViewLayoutAttributes *att;

@end

@implementation WGAddGoodBackCollectionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _backView = [UIView new];
        [self addSubview:_backView];
        [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        _deletBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deletBtn setImage:[UIImage imageNamed:@"goods_add_deletSpes"] forState:UIControlStateNormal];
        [_deletBtn addTarget:self action:@selector(deletSpes) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deletBtn];
        [_deletBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(40, 40));
            make.centerX.equalTo(self.backView.mas_right);
            make.centerY.equalTo(self.backView.mas_centerY);
        }];
    }
    return self;
}
- (void)deletSpes{
    NSLog(@"-----");
    if (_att.delectIndex) {
        _att.delectIndex(_att.selectSeciton);
    }
}
- (void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    if ([layoutAttributes isKindOfClass:[WGCollectionViewLayoutAttributes class]]) {
        WGCollectionViewLayoutAttributes *attr = (WGCollectionViewLayoutAttributes *)layoutAttributes;
        _att = attr;
        //        self.backgroundColor = attr.backgroundColor;
        if (attr.type == 0) {
            [_backView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self);
            }];

            _backView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
            _deletBtn.hidden = YES;
        }else if (attr.type == 1){
            [_backView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self);
            }];

            _backView.backgroundColor = [UIColor whiteColor];
            _deletBtn.hidden = YES;
        }else if (attr.type == 2){
            _backView.backgroundColor = [UIColor whiteColor];
            [_backView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self).mas_offset(UIEdgeInsetsMake(0, 15, 0, 15));
            }];
            _deletBtn.hidden = NO;

        }
    }
}
@end
