//
//  WGAddGoodBackLayout.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static NSString * const DDSetSectionBack = @"DDSetSectionBack";

@protocol WGAddGoodCollectionViewDelegateFlowLayout <UICollectionViewDelegateFlowLayout>

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section;
- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout deleteSection:(NSInteger)section;
@end
/**
 设置setion整体背景
 */
@interface WGAddGoodBackLayout : UICollectionViewFlowLayout


@end
NS_ASSUME_NONNULL_END
