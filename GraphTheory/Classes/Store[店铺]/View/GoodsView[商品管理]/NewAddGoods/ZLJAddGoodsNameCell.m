//
//  ZLJAddGoodsNameCell.m
//  BusinessFine
//
//  Created by TomLong on 2019/10/14.
//  Copyright © 2019 杨旭. All rights reserved.
//

#import "ZLJAddGoodsNameCell.h"


@interface ZLJAddGoodsNameCell ()

@property (nonatomic,strong) UILabel *leftNameLab;//商品名称

@property (nonatomic,strong) UITextField *nameTF;//输入商品名称
@end

@implementation ZLJAddGoodsNameCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setModel:(WGAddGoodsModel *)model{
    _model = model;
    _nameTF.text = model.itemsName;
}
- (void)textFieldChanged:(UITextField*)textField{
    _model.itemsName = textField.text;
}

- (void)setupSubviews{
    self.leftNameLab = [UILabel new];
    self.leftNameLab.text = @"*商品名称";
    self.leftNameLab.textColor = [UIColor colorWithHexString:@"333333"];
    self.leftNameLab.font = [UIFont systemFontOfSize:14.0f];
    [self addSubview:self.leftNameLab];
    
    self.nameTF = [UITextField new];
    self.nameTF.textAlignment = NSTextAlignmentRight;
    self.nameTF.font = [UIFont systemFontOfSize:14.0f];
    self.nameTF.textColor = [UIColor colorWithHexString:@"333333"];
    self.nameTF.placeholder = @"请输入商品名称（60字以内）";
    [self.nameTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

    [self addSubview:self.nameTF];
    [self.leftNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.left.mas_equalTo(self.leftNameLab.mas_right).offset(15);
        make.height.mas_equalTo(40);
    }];
}

@end
