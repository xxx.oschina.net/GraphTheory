//
//  ZLJAddCollectSelectedCell.m
//  BusinessFine
//
//  Created by TomLong on 2019/10/14.
//  Copyright © 2019 杨旭. All rights reserved.
//

#import "ZLJAddCollectSelectedCell.h"


@interface ZLJAddCollectSelectedCell ()
@property (nonatomic,strong) UILabel *lineLab;//底部线
@property (nonatomic,strong) UIImageView *righImgView;//向右箭头
@end
@implementation ZLJAddCollectSelectedCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        [self setupSubviews];
    }
    return self;
}
- (void)setupSubviews{
    [self addSubview:self.leftNameLab];
    [self addSubview:self.rightNameLab];
    [self addSubview:self.lineLab];
    [self addSubview:self.righImgView];
    [self.leftNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.righImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(7, 12));
    }];

    [self.rightNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.left.mas_equalTo(self.leftNameLab.mas_right).offset(10);
        make.right.mas_equalTo(self.righImgView.mas_left).offset(-10);
    }];
    
    [self.lineLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.mas_equalTo(self);
        make.height.mas_equalTo(1);
        make.left.mas_equalTo(15);
    }];
}

#pragma mark -- lazy load
- (UILabel *)leftNameLab{
    if (nil == _leftNameLab) {
        _leftNameLab = [UILabel new];
        _leftNameLab.textColor = [UIColor colorWithHexString:@"333333"];
        _leftNameLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _leftNameLab;
}

- (UILabel *)rightNameLab{
    if (nil == _rightNameLab) {
        _rightNameLab = [UILabel new];
        _rightNameLab.textColor = [UIColor colorWithHexString:@"999999"];
        _rightNameLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _rightNameLab;
}

-(UIImageView *)righImgView{
    if (nil == _righImgView) {
        _righImgView = [UIImageView new];
        _righImgView.image = [UIImage imageNamed:@"right_arrow_black"];
    }
    return _righImgView;
}

-(UILabel *)lineLab{
    if (nil == _lineLab) {
        _lineLab = [UILabel new];
        _lineLab.backgroundColor = [UIColor colorWithHexString:@"f4f4f4"];
    }
    return _lineLab;
}

@end

