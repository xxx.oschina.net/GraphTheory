//
//  WGAddGoodsAddImageCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGAddGoodsAddImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;

@end

NS_ASSUME_NONNULL_END
