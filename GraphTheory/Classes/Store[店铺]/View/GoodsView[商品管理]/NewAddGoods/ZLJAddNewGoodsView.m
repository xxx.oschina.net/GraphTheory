//
//  ZLJAddNewGoodsView.m
//  BusinessFine
//
//  Created by TomLong on 2019/10/14.
//  Copyright © 2019 杨旭. All rights reserved.
//
#import "ZLJAddNewGoodsView.h"
#import "ZLJAddGoodsNameCell.h"// section == 1商品名称
#import "ZLJAddCollectSelectedCell.h"// section == 2
#import "WGAddGoodsSpesCollectionViewCell.h"
#import "WGStoreSelctTimeWeekCollectionViewCell.h"
#import "WGAddGoodsModel.h"
#import "WGAddGoodsAddImageCollectionViewCell.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import "WGStoreSelectCategoryViewController.h"
#import "WGStoreSelectShipViewController.h"
#import "YXStoreViewModel.h"
#import "ZLJShopViewModle.h"
#import "WGSelectSpesViewController.h"
#import "WGPublicBottomView.h"
#import "WGAddGoodBackLayout.h"
@interface ZLJAddNewGoodsView ()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate,WGAddGoodCollectionViewDelegateFlowLayout>
{
    NSArray *_sectionTwoArray;//section == 2 左侧数据显示
}
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (strong, nonatomic) WGAddGoodsModel *addGoodsModel;
@property (strong, nonatomic) NSArray *shipMethod;///<配送方式
@property (strong, nonatomic) NSArray *goodsGroup;///<商品分组
@property (strong, nonatomic) NSArray *spesAll;///<所有规格
@property (strong, nonatomic) NSIndexPath *selectIndex;///<x图片选中行
@end
@implementation ZLJAddNewGoodsView
#pragma mark - Intial Methods
- (void)sectionTwoDataSourceInit{
    _addGoodsModel = [WGAddGoodsModel new];
    NSArray *leftArray = @[@"*商品分类",@"*配送方式",
                           @"商品分组",@"商品描述"];
    NSArray *rightArray = @[@"请选择",@"请选择",
                            @"请选择",@""];
    _sectionTwoArray = @[leftArray,rightArray];
}
#pragma mark - Override Methods
- (instancetype)init{
    self = [super init];
    if (self) {
        [self sectionTwoDataSourceInit];
        [self addSubview:self.collectionView];
        [self addSubview:self.bottomView];
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.bottom.and.right.equalTo(self);
            make.height.equalTo(@60);
        }];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top);
            make.bottom.equalTo(self.bottomView.mas_top);
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
        }];
    }
    return self;
}
#pragma mark -- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat itemWidth = KWIDTH;
    CGFloat itemHeight = 44.0f;
    if (indexPath.section == 0) {
        itemWidth = (KWIDTH - 30 - 15*4)/ 5.0f;
        itemHeight = itemWidth + 20;
    }else if (indexPath.section == 1){
        itemHeight = 44;
    }else if (indexPath.section == 2){
        itemHeight = 44;
    }else if (indexPath.section == 3){
        itemHeight = 44;
    }else if (indexPath.section == (4 + _addGoodsModel.spesModelArr.count)){
        return CGSizeMake(80, 28);
    }
    else if (3 < indexPath.section && indexPath.section < 4 + _addGoodsModel.spesModelArr.count){
        return CGSizeMake(KWIDTH - 30, 30);
    }
    return CGSizeMake(itemWidth, itemHeight);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }else if (section == 1){
        return UIEdgeInsetsMake(0, 0, 0, 0);
    } else if (section == (4 + _addGoodsModel.spesModelArr.count)){
        return UIEdgeInsetsMake(10, 15, 0, 15);
    }
    else if (3 < section && section < 4 + _addGoodsModel.spesModelArr.count){
        return UIEdgeInsetsMake(10, 15, 0, 40);
    }
    return UIEdgeInsetsMake(10, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return 15;
    }
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return 0;
    }else if (section == 2){
        return 1;
    }
    return 0;
}
#pragma mark --UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (_addGoodsModel.spesArr.count > 1) {
        return 5 + _addGoodsModel.spesModelArr.count;
    }
    return 4 + _addGoodsModel.spesModelArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        if (_addGoodsModel.goodsImageArr.count < 5) {
            return _addGoodsModel.goodsImageArr.count + 1;
        }
        return _addGoodsModel.goodsImageArr.count;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return 4;
    }else if (section == 3){
        return 1;
    }else if (section == (4 + _addGoodsModel.spesModelArr.count)){
        return 1;
    }
    else if (3 < section && section < 4 + _addGoodsModel.spesModelArr.count){
        return _addGoodsModel.spesModelArr[section - 4].specDetails.count;
    }
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell    *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"addCollectionCell" forIndexPath:indexPath];
    if (indexPath.section == 0) {//添加图片
        WGAddGoodsAddImageCollectionViewCell    *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGAddGoodsAddImageCollectionViewCell" forIndexPath:indexPath];
        if (indexPath.row < _addGoodsModel.goodsImageArr.count) {
            cell.image = _addGoodsModel.goodsImageArr[indexPath.row];
        }else{
            cell.image = nil;
        }
        return cell;
    }else if (indexPath.section == 1){
        ZLJAddGoodsNameCell    *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SectionOneCell" forIndexPath:indexPath];
        cell.model = _addGoodsModel;
        return cell;
    }else if (indexPath.section == 2){
        ZLJAddCollectSelectedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SectionTwoCell" forIndexPath:indexPath];
        cell.leftNameLab.text = _sectionTwoArray[0][indexPath.row];
        cell.rightNameLab.text = _sectionTwoArray[1][indexPath.row];
        if (indexPath.row == 0&&_addGoodsModel.categoryModel.category_name) {
            cell.rightNameLab.text = _addGoodsModel.categoryModel.category_name;
        }else if (indexPath.row == 1 && _addGoodsModel.distributionType.length){
            NSString *tem = [_addGoodsModel.distributionType copy];
            tem = [tem stringByReplacingOccurrencesOfString:@"1" withString:@"快递"];
            tem = [tem stringByReplacingOccurrencesOfString:@"2" withString:@"到店"];
            tem = [tem stringByReplacingOccurrencesOfString:@"3" withString:@"上门"];
            cell.rightNameLab.text = tem;
        }else if (indexPath.row == 2&&_addGoodsModel.groupModel){
            cell.rightNameLab.text = _addGoodsModel.groupModel.groupName;
        }
        return cell;
    }else if (indexPath.section == 3){
        ZLJAddCollectSelectedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SectionTwoCell" forIndexPath:indexPath];
        cell.leftNameLab.text = @"*选择规格";
        cell.rightNameLab.text = _addGoodsModel.specStr.length?_addGoodsModel.specStr:@"请选择";
        return cell;
    }else if (indexPath.section == 4 + _addGoodsModel.spesModelArr.count){
        WGStoreSelctTimeWeekCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell" forIndexPath:indexPath];
        cell.titleStr = @"添加多规格";
        cell.type = 1;
        return cell;
    }
    else if (3 < indexPath.section && indexPath.section < 4 + _addGoodsModel.spesModelArr.count){
        WGAddGoodsSpesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGAddGoodsSpesCollectionViewCell" forIndexPath:indexPath];
        cell.model = _addGoodsModel.spesModelArr[indexPath.section - 4].specDetails[indexPath.row];
        return cell;
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    _selectIndex = indexPath;
    if (indexPath.section == 0) {
        if (indexPath.row == _addGoodsModel.goodsImageArr.count) {
            TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1
                                                                                                  delegate:self];
            imagePicker.allowCrop = YES;
            imagePicker.cropRect = CGRectMake(0, (kHEIGHT - SCREEN_WIDTH)/2, KWIDTH, KWIDTH);
            [imagePicker setSortAscendingByModificationDate:NO];
            imagePicker.isSelectOriginalPhoto = YES;
            imagePicker.allowPickingVideo = NO;
            [[UIView currentViewController]  presentViewController:imagePicker
                                                          animated:YES
                                                        completion:nil];
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            WGStoreSelectCategoryViewController *ship = [WGStoreSelectCategoryViewController new];
            [[UIView currentViewController].navigationController pushViewController:ship animated:YES];
            ship.clickSelectCategoryModelBlock = ^(WGStoreCategoryModel * _Nonnull model) {
                weakSelf.addGoodsModel.categoryModel = model;
                [weakSelf.collectionView reloadData];
            };
        }else if (indexPath.row == 1){
            if (_shipMethod.count) {
                WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
                if (_addGoodsModel.distributionType.length) {
                    ship.selectArr = [[NSMutableArray alloc]initWithArray:[_addGoodsModel.distributionType componentsSeparatedByString:@","]];
                }
                ship.selectStoreShipMethod = ^(NSArray * _Nonnull arr) {
                    weakSelf.addGoodsModel.distributionType = [arr componentsJoinedByString:@","];
                    [weakSelf.collectionView reloadData];
                };
                ship.titleArr = _shipMethod;
                [[UIView currentViewController].navigationController pushViewController:ship animated:YES];
                return;
            }
            [YJProgressHUD showLoading:@""];
            [YXStoreViewModel queryShopDistributionTypeShopId:nil Completion:^(id  _Nonnull responesObj) {
                [YJProgressHUD hideHUD];
                if ([responesObj[@"code"] integerValue] == 200) {
                    WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
                    if (weakSelf.addGoodsModel.distributionType.length) {
                        ship.selectArr = [[NSMutableArray alloc]initWithArray:[weakSelf.addGoodsModel.distributionType componentsSeparatedByString:@","]];
                    }
                    ship.selectStoreShipMethod = ^(NSArray * _Nonnull arr) {
                        weakSelf.addGoodsModel.distributionType = [arr componentsJoinedByString:@","];
                        [weakSelf.collectionView reloadData];
                    };
                    NSMutableArray *tem = [NSMutableArray new];
                    for (NSDictionary *dic in responesObj[@"data"]) {
                        [tem addObject:dic[@"id"]];
                    }
                    weakSelf.shipMethod = tem;
                    ship.titleArr = tem;
                    [[UIView currentViewController].navigationController pushViewController:ship animated:YES];
                }else{
                    KPOP(responesObj[@"msg"]);
                }
            } failure:^(NSError * _Nonnull error) {
                [YJProgressHUD hideHUD];
                KPOP(REQUESTERR)
            }];
        }else if (indexPath.row == 2){
            if (_goodsGroup.count) {
                WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
                ship.selectGoodsGroup = ^(FindItemsGroupDataItem * _Nonnull model) {
                    weakSelf.addGoodsModel.groupModel = model;
                    [weakSelf.collectionView reloadData];
                };
                ship.type = 1;
                ship.titleArr = _goodsGroup;
                [[UIView currentViewController].navigationController pushViewController:ship animated:YES];
                return;
            }
            [YJProgressHUD showLoading:@""];
            [ZLJShopViewModle zljFindItemsGroupByShopId:@"1dpee8dbm000102h09j5139ku3dt"
                                             Completion:^(id  _Nonnull responesObj) {
                                                 [YJProgressHUD hideHUD];
                                                 if ([responesObj[@"code"] integerValue] == 200) {
                                                     ZLJFindItemsGroupModel *model = [ZLJFindItemsGroupModel mj_objectWithKeyValues:responesObj];
                                                     NSMutableArray *tem = [NSMutableArray new];
                                                     for (FindItemsGroupDataItem *itemModel in model.data) {
                                                         [tem addObject:itemModel];
                                                     }
                                                     WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
                                                     ship.selectGoodsGroup = ^(FindItemsGroupDataItem * _Nonnull model) {
                                                         weakSelf.addGoodsModel.groupModel = model;
                                                         [weakSelf.collectionView reloadData];
                                                     };
                                                     ship.type = 1;
                                                     ship.titleArr = tem;
                                                     [[UIView currentViewController].navigationController pushViewController:ship animated:YES];
                                                 }else{
                                                     KPOP(responesObj[@"msg"]);
                                                 }
                                             } Failure:^(NSError * _Nonnull error) {
                                                 [YJProgressHUD hideHUD];
                                                 KPOP(REQUESTERR)
                                             }];
        }
    }else if (indexPath.section == 3){
        if (_spesAll.count) {
            WGSelectSpesViewController *select = [WGSelectSpesViewController new];
            select.titleArr = _spesAll;
            select.selectArr = _addGoodsModel.spesArr;
            select.selectGoodsSpes = ^(NSMutableArray * _Nonnull arr) {
                [weakSelf.addGoodsModel.spesModelArr removeAllObjects];
                weakSelf.addGoodsModel.spesArr = arr;
                WGAddGoodsSpecModel *tem = [WGAddGoodsSpecModel new];
                [tem addDetaiSpes:arr];
                [weakSelf.addGoodsModel.spesModelArr addObject:tem];
                [weakSelf.collectionView reloadData];
            };
            [[UIView currentViewController].navigationController pushViewController:select animated:YES];
            return;
        }else{
            [YJProgressHUD showLoading:@""];
            [ZLJShopViewModle querySpecGroupId:nil Completion:^(id  _Nonnull responesObj) {
                [YJProgressHUD hideHUD];
                if ([responesObj[@"code"] integerValue] == 200) {
                    NSArray *allSpec = [WGGoodsSpecModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                    weakSelf.spesAll = allSpec;
                    WGSelectSpesViewController *select = [WGSelectSpesViewController new];
                    select.titleArr = allSpec;
                    select.selectArr = weakSelf.addGoodsModel.spesArr;
                    select.selectGoodsSpes = ^(NSMutableArray * _Nonnull arr) {
                        [weakSelf.addGoodsModel.spesModelArr removeAllObjects];
                        weakSelf.addGoodsModel.spesArr = arr;
                        WGAddGoodsSpecModel *tem = [WGAddGoodsSpecModel new];
                        [tem addDetaiSpes:arr];
                        [weakSelf.addGoodsModel.spesModelArr addObject:tem];                        [weakSelf.collectionView reloadData];
                    };
                    [[UIView currentViewController].navigationController pushViewController:select animated:YES];
                }else{
                    KPOP(responesObj[@"msg"]);
                }
            } Failure:^(NSError * _Nonnull error) {
                [YJProgressHUD hideHUD];
                KPOP(REQUESTERR)
            }];
        }
    }else if (indexPath.section == 4 + _addGoodsModel.spesModelArr.count){
        WGAddGoodsSpecModel *tem = [WGAddGoodsSpecModel new];
        [tem addDetaiSpes:_addGoodsModel.spesArr];
        [_addGoodsModel.spesModelArr addObject:tem];
        [weakSelf.collectionView reloadData];
    }else if (3 < indexPath.section && indexPath.section < 4 + _addGoodsModel.spesModelArr.count){
        if (indexPath.row == _addGoodsModel.spesModelArr[indexPath.section - 4].specDetails.count) {
            TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1
                                                                                                  delegate:self];
            imagePicker.allowCrop = YES;
            imagePicker.cropRect = CGRectMake(0, (kHEIGHT - SCREEN_WIDTH)/2, KWIDTH, KWIDTH);
            [imagePicker setSortAscendingByModificationDate:NO];
            imagePicker.isSelectOriginalPhoto = YES;
            imagePicker.allowPickingVideo = NO;
            [[UIView currentViewController]  presentViewController:imagePicker
                                                          animated:YES
                                                        completion:nil];
        }
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    else if (3 < section && section < 4 + _addGoodsModel.spesModelArr.count){
        return 2;
    }
    return 0;
}
- (void)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout deleteSection:(NSInteger)section{
    [_addGoodsModel.spesModelArr removeObjectAtIndex:section - 4];
    [_collectionView reloadData];
}
#pragma mark - Lazy Loads
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        WGAddGoodBackLayout * layout = [[WGAddGoodBackLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"addCollectionCell"];
        [_collectionView registerClass:[ZLJAddGoodsNameCell class] forCellWithReuseIdentifier:@"SectionOneCell"];
        [_collectionView registerClass:[ZLJAddCollectSelectedCell class] forCellWithReuseIdentifier:@"SectionTwoCell"];
        [_collectionView registerClass:[WGAddGoodsSpesCollectionViewCell class] forCellWithReuseIdentifier:@"WGAddGoodsSpesCollectionViewCell"];
        [_collectionView registerClass:[WGAddGoodsAddImageCollectionViewCell class] forCellWithReuseIdentifier:@"WGAddGoodsAddImageCollectionViewCell"];
        [_collectionView registerClass:[WGStoreSelctTimeWeekCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@[@"放入仓库",@"上架"]];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
        }];
    }
    return _bottomView;
}
#pragma mark - Target Methods
#pragma mark - Public Methods
#pragma mark - Private Methods
#pragma mark - Private TZImagePickerControllerDelegate
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    UIImage *seleImage = photos[0];
    if (_selectIndex.section == 0) {
        [_addGoodsModel.goodsImageArr addObject:seleImage];
    }else if (_selectIndex.section > 3){
        WGAddGoodsSpecDetailModel *model = _addGoodsModel.spesModelArr[_selectIndex.section - 4].specDetails[_selectIndex.row];
        model.image = seleImage;
    }
    [self.collectionView reloadData];
}
#if TARGET_IPHONE_SIMULATOR
-(void)injected {
    self.backgroundColor = UIColor.cyanColor;
}
#endif
@end
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
//
//}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
//
//}
//        [_collectionView registerClass:[WGStoreOrderCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell"];
//        [_collectionView registerClass:[WGStoreMenuCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreMenuCollectionViewCell"];
//        [_collectionView registerClass:[WGStroeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGStroeHeadCollectionReusableView"];
