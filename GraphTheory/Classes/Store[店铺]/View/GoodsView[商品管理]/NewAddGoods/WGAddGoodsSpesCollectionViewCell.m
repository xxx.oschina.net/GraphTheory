//
//  WGAddGoodsSpesCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGAddGoodsSpesCollectionViewCell.h"
#import "LTLimitTextField.h"

@interface WGAddGoodsSpesCollectionViewCell ()

@property (nonatomic,strong) UILabel *leftNameLab;//规格名
@property (nonatomic,strong) LTLimitTextField *nameTF;//规格
@property (nonatomic,strong) UIImageView *imageView;//规格图片

@end

@implementation WGAddGoodsSpesCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.clearColor;
        [self setupSubviews];
    }
    return self;
}
- (void)textFieldChanged:(UITextField*)textField{
    _model.spes = textField.text;
}

- (void)setModel:(WGAddGoodsSpecDetailModel *)model{
    _model = model;
    _leftNameLab.text = model.name;
    _nameTF.text = model.spes;
    _nameTF.placeholder = model.placeholder;
    _nameTF.textLimitInputType = model.inputType;
    _nameTF.keyboardType = model.keyBoardType;
    _nameTF.enabled = model.canEditor;
    
}
- (void)setupSubviews{
    self.leftNameLab = [UILabel new];
    self.leftNameLab.text = @"*商品名称";
    self.leftNameLab.textColor = [UIColor colorWithHexString:@"333333"];
    self.leftNameLab.font = [UIFont systemFontOfSize:12.0f];
    [self addSubview:self.leftNameLab];
    
    self.nameTF = [LTLimitTextField new];
    self.nameTF.textAlignment = NSTextAlignmentLeft;
    self.nameTF.font = [UIFont systemFontOfSize:12.0f];
    self.nameTF.textColor = [UIColor colorWithHexString:@"333333"];
    self.nameTF.placeholder = @"请输入商品名称（60字以内）";
    [self.nameTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

    [self addSubview:self.nameTF];
    self.imageView = [UIImageView new];
    self.imageView.hidden = YES;
    [self addSubview:self.imageView];
    [self.leftNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15.0f);
        make.centerY.mas_equalTo(self);
    }];
    
    [self.nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.left.mas_equalTo(self.leftNameLab.mas_right).offset(15);
        make.height.mas_equalTo(40);
    }];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self);
        make.right.mas_equalTo(-15.0f);
        make.left.mas_equalTo(self.leftNameLab.mas_right).offset(8);
        make.height.mas_equalTo(40);
    }];

}
@end
