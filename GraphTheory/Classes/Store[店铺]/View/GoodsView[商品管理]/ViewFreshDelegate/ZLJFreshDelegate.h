//
//  ZLJFreshDelegate.h
//  BusinessFine
//
//  Created by TomLong on 2019/11/11.
//  Copyright © 2019 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZLJFreshDelegate <NSObject>
//下拉刷新
- (void)refreshGoodsViewPullDown;
// 上拉加载
- (void)refreshGoodsViewPullUp;
@end

NS_ASSUME_NONNULL_END
