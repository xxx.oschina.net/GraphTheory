//
//  ZLJBottomButtonView.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJBottomButtonView.h"

@implementation ZLJBottomButtonView

- (instancetype)initWithButtonTitle:(NSArray <NSString *>*)titleArray   buttonSize:(CGSize )buttonSize clickButton:(void(^)(UIButton *button))clickButton
{
    self = [super init];
    if (self) {
        UIView  *lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHexString:@"F4F4F4"];
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5);
            make.left.mas_equalTo(15);
            make.right.mas_equalTo(self);
            make.top.mas_equalTo(self);
        }];
        CGFloat buttonWidth = buttonSize.width;
        CGFloat buttonHeight = buttonSize.height;   
        for (NSInteger i = 0; i < titleArray.count; i++ ) {
            UIButton    *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = i;
            button.titleLabel.font = [UIFont systemFontOfSize:12.0f];
            [button setTitle:titleArray[i] forState:UIControlStateNormal];
            UIColor *color = [UIColor colorWithHexString:@"999999"];
            [button setTitleColor:color forState:UIControlStateNormal];
            button.layer.borderColor = color.CGColor;
            button.layer.borderWidth = 1.0f;
            button.layer.cornerRadius = buttonHeight / 2.0f;
            button.layer.masksToBounds = YES;
            [self addSubview:button];
            CGFloat rightValue = 0 - (15  + (buttonWidth + 15)* i );
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self);
                make.right.mas_equalTo(rightValue);
                make.size.mas_equalTo(buttonSize);
            }];
            [button addAction:^(UIButton * _Nonnull btn) {
                 NSLog(@"zlj--当前输出：%s",__FUNCTION__);
                NSString    *btnTitle = btn.titleLabel.text;
                BOOL shanChu = [btnTitle isEqualToString:@"删除"];
                if ([btnTitle isEqualToString:@"上架"] || [btnTitle isEqualToString:@"下架"] ||shanChu ) {
                    NSString    *messageString = [NSString stringWithFormat:@"确认%@吗?",btnTitle];
                    YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:messageString sureBtn:@"确认" cancleBtn:@"取消"];
                    [alertView showAnimation];
                    [alertView setSureClick:^(NSString * _Nonnull string) {
                        if (clickButton) {
                           clickButton(btn);
                        }
                    }];
                }else{
                    if (clickButton) {
                        clickButton(btn);
                     }
                }
            }];
        }
    }
    return self;
}

@end
