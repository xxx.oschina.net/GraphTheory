//
//  ZLJGoodsSortView.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/23.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SortButtonClickBlock)(UIButton *clickBtn);
@interface ZLJGoodsSortView : UIView

@property (nonatomic ,strong) NSString *title;

- (instancetype)initWithFrame:(CGRect)frame withTitleArray:(NSArray <NSString *>*)titleArray clickButton:(SortButtonClickBlock)clickButton;
@end


@interface ZLJGoodsSortButton : UIButton

- (void)buttonImage:(ZLJGoodsSortButton *)sortButton  buttonTitle:(NSString *)title;

@end


NS_ASSUME_NONNULL_END
