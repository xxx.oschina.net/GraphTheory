//
//  ZLJGoodsMoreCell.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemsManageItemList;

NS_ASSUME_NONNULL_BEGIN
typedef void(^CellButtonClickBlock)(ItemsManageItemList *cellModel,UIButton *button);

@interface ZLJGoodsMoreCell : UITableViewCell
/**
 titleArray ： 底部按钮显示。下架、修改、删除
 ZLJMoreGroupView 加载的cell方法
 */
- (instancetype)initMoreWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier titleArray:(NSArray <NSString *>*)titleArray;
/**
 cell编辑转态

 @param editType cell状态
 */
- (void)editCell:(BOOL )editType;



/**
 加载cell数据

 @param cellModel cell数据
 */
- (void)cellModel:(ItemsManageItemList *)cellModel  cellButtonClick:(CellButtonClickBlock)cellButtonClick;
@end

NS_ASSUME_NONNULL_END
