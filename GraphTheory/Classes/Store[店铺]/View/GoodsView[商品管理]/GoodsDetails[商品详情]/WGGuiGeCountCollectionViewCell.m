//
//  WGGuiGeCountCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/27.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGuiGeCountCollectionViewCell.h"
#import "LTLimitTextField.h"
//#import <PPNumberButton.h>
#import "WGGoodsInfoModel.h"
@interface WGGuiGeCountCollectionViewCell ()

@property(nonatomic, strong)UILabel *lb;
@property(nonatomic, strong)UIButton *bt_reduce;
@property(nonatomic, strong)LTLimitTextField *tf_count;
@property(nonatomic, strong)UIButton *bt_add;
//@property(nonatomic, strong)PPNumberButton *numberBtn;


@end

@implementation WGGuiGeCountCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout{
    [self.contentView addSubview:self.lb];
//    [self addSubview:self.numberBtn];
//    [self addSubview:self.bt_add];
//    [self addSubview:self.bt_reduce];
//    [self addSubview:self.tf_count];
    
    [self.lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.top.and.bottom.equalTo(self);
    }];
//    [self.numberBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(110, 30));
//        make.centerY.equalTo(self.mas_centerY);
//        make.right.equalTo(self.mas_right).offset(-15);
//    }];
//    [self.bt_add mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(40, 30));
//        make.centerY.equalTo(self.mas_centerY);
//        make.right.equalTo(self.mas_right).offset(-15);
//    }];
//    [self.tf_count mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(40, 30));
//        make.centerY.equalTo(self.mas_centerY);
//        make.right.equalTo(self.bt_add.mas_left).offset(-4);
//    }];
//    [self.bt_reduce mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(40, 30));
//        make.centerY.equalTo(self.mas_centerY);
//        make.right.equalTo(self.tf_count.mas_left).offset(-4);
//    }];
}
//- (PPNumberButton *)numberBtn{
//    if (!_numberBtn) {
//        _numberBtn = [[PPNumberButton alloc]init];
//        _numberBtn.shakeAnimation = YES;
//        // 设置最小值
//        _numberBtn.minValue = 1;
//        // 设置输入框中的字体大小
//        _numberBtn.inputFieldFont = 15;
//        _numberBtn.borderColor = color_TextThree;
//        _numberBtn.increaseTitle = @"＋";
//        _numberBtn.decreaseTitle = @"－";
//        YXWeakSelf;
//        _numberBtn.resultBlock = ^(PPNumberButton *ppBtn, CGFloat number, BOOL increaseStatus) {
//            weakSelf.model.goodsCount = number;
//            if (weakSelf.upDataModel) {
//                weakSelf.upDataModel(weakSelf.model);
//            }
//
//        };
//    }
//    return _numberBtn;
//}
- (UILabel *)lb{
    if (!_lb) {
        _lb = [UILabel new];
        _lb.textColor = [UIColor blackColor];
        _lb.font = [UIFont systemFontOfSize:14];
        _lb.text = @"购买数量";
    }
    return _lb;
}
- (UIButton *)bt_add{
    if (!_bt_add) {
        _bt_add = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bt_add setBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]];
        
        [_bt_add setTitleColor:[UIColor blackColor] forState:0];
        _bt_add.titleLabel.font = [UIFont systemFontOfSize:20];
        [_bt_add setTitle:@"+" forState:0];
        [_bt_add addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bt_add;
}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    _tf_count.text = [NSString stringWithFormat:@"%ld",(long)_model.goodsCount];
//    _numberBtn.currentNumber = model.goodsCount;
}
- (void)add{
    _model.goodsCount ++ ;
    _tf_count.text = [NSString stringWithFormat:@"%ld",(long)_model.goodsCount];
    if (self.upDataModel) {
        self.upDataModel(_model);
    }
}
- (void)reduce{
    _model.goodsCount --;
    if (_model.goodsCount <= 0) {
        _model.goodsCount = 1;
    }
    _tf_count.text = [NSString stringWithFormat:@"%ld",(long)_model.goodsCount];
    if (self.upDataModel) {
        self.upDataModel(_model);
    }
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (self.upDataModel) {
        self.upDataModel(_model);
    }
}
- (UIButton *)bt_reduce{
    if (!_bt_reduce) {
        _bt_reduce = [UIButton buttonWithType:UIButtonTypeCustom];
        [_bt_reduce setBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]];
        
        [_bt_reduce setTitleColor:[UIColor blackColor] forState:0];
        _bt_reduce.titleLabel.font = [UIFont systemFontOfSize:20];
        [_bt_reduce setTitle:@"-" forState:0];
        [_bt_reduce addTarget:self action:@selector(reduce) forControlEvents:UIControlEventTouchUpInside];
    }
    return _bt_reduce;
}
- (LTLimitTextField *)tf_count{
    if (!_tf_count) {
        _tf_count = [[LTLimitTextField alloc]init];
        _tf_count.text = @"1";
        _tf_count.textLimitInputType = LTTextLimitInputTypeNumber;
        _tf_count.keyboardType = UIKeyboardTypeNumberPad;
        _tf_count.textAlignment = NSTextAlignmentCenter;
        _tf_count.font = [UIFont systemFontOfSize:15];
        _tf_count.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
        [_tf_count addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return _tf_count;
}
@end
