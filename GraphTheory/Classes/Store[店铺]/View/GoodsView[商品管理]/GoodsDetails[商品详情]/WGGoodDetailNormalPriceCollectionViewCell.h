//
//  WGGoodDetailNormalPriceCollectionViewCell.h
//  DDLife
//
//  Created by wanggang on 2019/11/27.
//  Copyright © 2019年 点都. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class WGGoodsInfoModel;
@interface WGGoodDetailNormalPriceCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGGoodsInfoModel *model;


@end

NS_ASSUME_NONNULL_END
