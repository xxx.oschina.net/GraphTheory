//
//  WGGoodDetailBannerCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGoodDetailBannerCollectionViewCell.h"
#import "SDCycleScrollView.h"
#import "YXPreviewManage.h"
//#import "XZMRefresh.h"
#import "WGGoodsInfoModel.h"
@interface WGGoodDetailBannerCollectionViewCell ()<SDCycleScrollViewDelegate>

/** 商品轮播图*/
@property (nonatomic ,strong) SDCycleScrollView *goodImgScrollView;
/** 商品图标分页*/
@property (nonatomic ,strong) UILabel *pageLab;
@property (nonatomic ,assign) NSInteger select;
@property (nonatomic ,strong) NSArray *imgArr;
@property (nonatomic ,strong) UICollectionView *scrollview;


@end

@implementation WGGoodDetailBannerCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _select = 1;
        [self addSubview:self.goodImgScrollView];
        for (UIView *view in self.goodImgScrollView.subviews) {
            if ([view isKindOfClass:[UICollectionView class]]) {
                _scrollview = view;
                if (@available(iOS 11.0, *)) {
                    _scrollview.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
                } else {
                    // Fallback on earlier versions
                }

            }
        }
//        if (_scrollview) {
//            // 设置footer文字
//            [self.scrollview xzm_addNormalFooterWithTarget:self action:@selector(loadMoreDataWithFooter)];
//
//            [self.scrollview.xzm_footer setTitle:@"显示商品大图" forState:XZMRefreshStateNormal];
//            [self.scrollview.xzm_footer setTitle:@"松开显示大图" forState:XZMRefreshStatePulling];
//            [self.scrollview.xzm_footer setTitle:@"显示商品大图" forState:XZMRefreshStateRefreshing];
//            // 设置字体
//            self.scrollview.xzm_footer.font = [UIFont systemFontOfSize:14];
//            // 设置颜色
//            self.scrollview.xzm_footer.textColor = color_TextOne;
//
//        }
        [self addSubview:self.pageLab];
        [self addLayout];
    }
    return self;
}
//- (void)loadMoreDataWithFooter{
//    [self.scrollview.xzm_footer endRefreshing];
//    [[YXPreviewManage sharePreviewManage]showPhotoWithImgArr:self.imgArr currentIndex:0];

//}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    NSMutableArray *imagePaths = [NSMutableArray new];
    for (Paths *p in model.paths) {
        [imagePaths addObject:p.path];
    }
    _imgArr = imagePaths;
    _goodImgScrollView.imageURLStringsGroup = imagePaths;
    _pageLab.text = [NSString stringWithFormat:@"%ld/%ld",(long)_select,imagePaths.count];
}
#pragma -  SDCycleScrollView Delegate
/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    _select = index + 1;
    _pageLab.text = [NSString stringWithFormat:@"%ld/%ld",index + 1,_imgArr.count];
}
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
    [[YXPreviewManage sharePreviewManage]showPhotoWithImgArr:self.imgArr currentIndex:index];
}

- (void)addLayout {
    [self.goodImgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.pageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(40, 16));
        make.bottom.equalTo(self.mas_bottom).offset(-15);
    }];
}
#pragma mark - Lazy loading
- (SDCycleScrollView *)goodImgScrollView {
    if (!_goodImgScrollView) {
        _goodImgScrollView = [SDCycleScrollView cycleScrollViewWithFrame:(CGRectMake(0, 0, KWIDTH, KWIDTH*608/750)) delegate:self placeholderImage:nil];
        _goodImgScrollView.backgroundColor = [UIColor whiteColor];
        _goodImgScrollView.delegate = self;
        _goodImgScrollView.infiniteLoop = NO;
        _goodImgScrollView.autoScroll = NO;
        _goodImgScrollView.showPageControl = NO;
        _goodImgScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFit;
    }
    return _goodImgScrollView;
}

- (UILabel *)pageLab {
    if (!_pageLab) {
        _pageLab = [[UILabel alloc] initWithFrame:(CGRectMake(KWIDTH - 60, 300, 40, 16))];
        _pageLab.backgroundColor = [UIColor colorWithHexString:@"#333333" alpha:0.7];
        _pageLab.textColor = [UIColor whiteColor];
        _pageLab.textAlignment = NSTextAlignmentCenter;
        _pageLab.font = [UIFont systemFontOfSize:12.0f];
        _pageLab.layer.cornerRadius = 8.0f;;
        _pageLab.layer.masksToBounds = YES;
        _pageLab.text = [NSString stringWithFormat:@"%@/%lu",@"1",(unsigned long)self.imgArr.count];
        
    }
    return _pageLab;
}

@end
