//
//  WGEmptyCollectionReusableView.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGEmptyCollectionReusableView : UICollectionReusableView

@property (nonatomic, strong) UIColor *backColor;

@end

NS_ASSUME_NONNULL_END
