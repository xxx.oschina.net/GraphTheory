//
//  WGGoodDetailMoreCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGoodDetailMoreCollectionViewCell.h"

@interface WGGoodDetailMoreCollectionViewCell ()

/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 内容*/
@property (nonatomic ,strong) UILabel *contentLab;
/** 箭头*/
@property (nonatomic, strong) UIImageView *arrowImage;
/** 线*/
@property (nonatomic ,strong) UIView *lineView;

@end

@implementation WGGoodDetailMoreCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.contentLab];
        [self.contentView addSubview:self.lineView];
        [self.contentView addSubview:self.arrowImage];
        [self addLayout];
    }
    return self;
}
- (void)setType:(GoodsDetailMoreType)type{
    _type = type;
    switch (type) {
        case GoodsDetailMoreTypeGuiGe:
            _titleLab.text = @"已选";
            break;
        case GoodsDetailMoreTypeFee:{
            _titleLab.text = @"运费";
            
        }
            break;
        case GoodsDetailMoreTypeDetail:
            _titleLab.text = @"参数";
            break;
        case GoodsDetailMoreTypeAddress:
        {
            _titleLab.text = @"送至";
        }
            
            break;
            
        default:
            break;
    }

    [_titleLab setContentHuggingPriority:UILayoutPriorityRequired
                              forAxis:UILayoutConstraintAxisHorizontal];
    [_contentLab setContentHuggingPriority:UILayoutPriorityDefaultLow
                              forAxis:UILayoutConstraintAxisHorizontal];
}
- (void)setContent:(NSString *)content{
    _content= content;
    _contentLab.text = content;
    if (_type == GoodsDetailMoreTypeAddress) {
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        //使用一张图片作为Attachment数据
        attachment.image = [UIImage imageNamed:@"dingwei"];
        //这里bounds的x值并不会产生影响
        attachment.bounds = CGRectMake(0, 0, 10, 13);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:content];
        [attributedString insertAttributedString:[NSAttributedString attributedStringWithAttachment:attachment] atIndex:0];
        _contentLab.attributedText = content.length?attributedString:nil;
    }
}
- (void)setIsEnd:(BOOL)isEnd{
    _isEnd = isEnd;
}
- (void)addLayout {
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.titleLab sizeToFit];
    }];
    [_arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-10);
    }];
    [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.arrowImage.mas_right).offset(-10);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.and.right.equalTo(self);
        make.height.equalTo(@1);
        make.left.equalTo(self.mas_left).offset(15);
    }];
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:15];
        _titleLab.textColor = [UIColor colorWithHexString:@"#999999"];
        
    }
    return _titleLab;
}
- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:15];
        _contentLab.textColor = [UIColor colorWithHexString:@"#333333"];
    }
    return _contentLab;
}

- (UIImageView *)arrowImage{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"advert_arrow_right"]];
    }
    return _arrowImage;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}
@end
