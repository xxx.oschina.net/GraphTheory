//
//  WGGoodDetailBannerCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN


/**
 商品详情-滚动图
 */
@class WGGoodsInfoModel;
@interface WGGoodDetailBannerCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGGoodsInfoModel *model;

@end

NS_ASSUME_NONNULL_END
