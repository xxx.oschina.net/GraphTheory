//
//  WGGuiGeHeadCollectionReusableView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/27.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGuiGeHeadCollectionReusableView.h"

@interface WGGuiGeHeadCollectionReusableView ()

@property (nonatomic ,strong) UILabel *titleLab;

@end

@implementation WGGuiGeHeadCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setTitle:(NSString *)title{
    _title = title;
    _titleLab.text = title;
}
- (void)setType:(NSInteger)type{
    self.backgroundColor = [UIColor whiteColor];

    [self.titleLab mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.centerY.equalTo(self.mas_centerY);
    }];
}
- (void)setUpLayout{
    [self addSubview:self.titleLab];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self).insets(UIEdgeInsetsMake(15, 15, 0, 15));
    }];
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = [UIColor blackColor];
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}

@end
