//
//  WGGoodDetailMoreCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,GoodsDetailMoreType) {
    GoodsDetailMoreTypeGuiGe,           //规格
    GoodsDetailMoreTypeAddress,             //地址
    GoodsDetailMoreTypeFee,           //运费
    GoodsDetailMoreTypeDetail,      //参数
    
};

/**
 商品详情,其他选项
 */
@interface WGGoodDetailMoreCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) GoodsDetailMoreType type;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) BOOL isEnd;


@end

NS_ASSUME_NONNULL_END
