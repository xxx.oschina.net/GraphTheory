//
//  WGGuiGeView.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGGoodsDetailViewModel.h"

NS_ASSUME_NONNULL_BEGIN
@class WGGoodsInfoModel;
@interface WGGuiGeView : UIView

@property (nonatomic, strong) WGGoodsInfoModel *model;
@property (nonatomic, strong) WGGoodsDetailViewModel *viewsModel;
//@property (nonatomic, assign) WGGoodsButtonType type;
@property (nonatomic ,assign) NSInteger goodStatus;
@property (strong, nonatomic) UIViewController *vc;
@property (nonatomic, strong) NSString *cannotStr;

//@property (nonatomic ,copy) void(^upDataModel)(WGGoodsInfoModel *model);
//@property (nonatomic ,copy) void(^actionType)(WGGoodsButtonType type);

@end

NS_ASSUME_NONNULL_END
