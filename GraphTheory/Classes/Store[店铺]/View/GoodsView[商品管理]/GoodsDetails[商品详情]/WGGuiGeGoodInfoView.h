//
//  WGGuiGeGoodInfoCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGGoodsDetailViewModel.h"

NS_ASSUME_NONNULL_BEGIN
@class SpecSku;
@interface WGGuiGeGoodInfoView : UIView

@property (strong, nonatomic) WGGoodsInfoModel *model;

@property (nonatomic, assign) WGGoodsButtonType type;


@property (strong, nonatomic) SpecSku *sku;

@property (strong, nonatomic) NSString *noSelct;

@property (strong, nonatomic) UIViewController *vc;

@end

NS_ASSUME_NONNULL_END
