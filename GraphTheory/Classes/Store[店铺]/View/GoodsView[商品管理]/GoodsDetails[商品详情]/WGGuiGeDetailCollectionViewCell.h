//
//  WGGuiGeDetailCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGGuiGeDetailCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) NSString *model;
@property (assign, nonatomic) NSInteger type;///<0:正常 1:被选中 2:无法选中

@end

NS_ASSUME_NONNULL_END
