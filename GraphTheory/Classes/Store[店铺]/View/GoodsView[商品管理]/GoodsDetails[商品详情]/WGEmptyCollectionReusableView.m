//
//  WGEmptyCollectionReusableView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGEmptyCollectionReusableView.h"

@implementation WGEmptyCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = HEX_COLOR(@"#F4F4F4");
    }
    return self;
}

- (void)setBackColor:(UIColor *)backColor{
    _backColor = backColor;
    self.backgroundColor = _backColor;
}
@end
