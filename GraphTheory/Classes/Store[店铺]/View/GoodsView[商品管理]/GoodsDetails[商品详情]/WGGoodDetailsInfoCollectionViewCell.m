//
//  WGGoodDetailsInfoCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGoodDetailsInfoCollectionViewCell.h"
#import "UILabel+Extension.h"
#import "WGGoodsInfoModel.h"
@interface WGGoodDetailsInfoCollectionViewCell ()

/** 运营商*/
@property (nonatomic ,strong) UILabel *operatorLab;
/** 商品名称*/
@property (nonatomic ,strong) UILabel *goodTitleLab;

@end

@implementation WGGoodDetailsInfoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.goodTitleLab];
        [self addLayout];
    }
    return self;
}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    [UILabel setLabelSpace:self.goodTitleLab withValue:model.itemName withFont:[UIFont boldSystemFontOfSize:16.0] withSpacing:3.0f ImageName:_model.source.integerValue == 1?@"jingdong":nil];
}

- (UILabel *)operatorLab {
    if (!_operatorLab) {
        _operatorLab = [[UILabel alloc] init];
        _operatorLab.font = [UIFont systemFontOfSize:10.0f];
        _operatorLab.textColor = [UIColor whiteColor];
        _operatorLab.backgroundColor = color_RedColor;
        _operatorLab.textAlignment = NSTextAlignmentCenter;
        _operatorLab.layer.masksToBounds = YES;
        _operatorLab.layer.cornerRadius = 2.0f;
        _operatorLab.text = @"自营";
    }
    return _operatorLab;
}

- (UILabel *)goodTitleLab {
    if (!_goodTitleLab) {
        _goodTitleLab = [[UILabel alloc] init];
        _goodTitleLab.textColor = color_TextOne;
        _goodTitleLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _goodTitleLab.numberOfLines = 2;
    
        //        _goodTitleLab.text = @"骄傲了大姐夫拉到就发啦时代峻峰拉三等奖法拉第缴费拉伸的减肥啦";
    }
    return _goodTitleLab;
}
- (void)addLayout {
    
    YXWeakSelf
    [_goodTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(5);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        [weakSelf.goodTitleLab sizeToFit];
    }];
}
@end
