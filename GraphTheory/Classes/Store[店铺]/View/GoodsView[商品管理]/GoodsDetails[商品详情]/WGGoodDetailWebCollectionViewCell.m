//
//  WGGoodDetailWebCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGoodDetailWebCollectionViewCell.h"
#import <WebKit/WebKit.h>
#import "WGGoodsInfoModel.h"

@interface WGGoodDetailWebCollectionViewCell ()<WKNavigationDelegate,UIScrollViewDelegate>
@property(strong,nonatomic) WKWebView *wkWebView;


@end

@implementation WGGoodDetailWebCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.wkWebView];
        [self addLayout];
    }
    return self;
}
- (void)setHtml:(WGGoodsInfoModel *)html{
    _html = html;
    NSString *htmls = [NSString stringWithFormat:@"<html> \n"
                       "<head> \n"
//                       "<style type=\"text/css\"> \n"
//                       "body {font-size:14px;}\n"
//                       "</style> \n"
                       "<meta name='viewport' content='user-scalable=no'> \n"
                       "</head> \n"
                       "<body>%@"
//                       "<style>\n"
//                       "table,p,tbody,span,tr,td,img,dd,img,dl,form{width:100% !important;}\n"
//                       "</style> \n"
                       "</body>"
                       "</html>",html.nappintroduction];
    [_wkWebView loadHTMLString:htmls baseURL:nil];
}
- (void)addLayout {
    
    [_wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}
- (WKWebView *)wkWebView{
    if (!_wkWebView) {
        NSMutableString *jScript = [NSMutableString string];
//        NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        [jScript appendString:@"document.documentElement.style.webkitUserSelect='none';"];//禁止选择
        
        [jScript appendString:@"document.documentElement.style.webkitTouchCallout='none';"];//禁止长按
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];

        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;

        _wkWebView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:wkWebConfig];
        _wkWebView.navigationDelegate = self;
        _wkWebView.scrollView.scrollEnabled = NO;
        //
        _wkWebView.backgroundColor = [UIColor whiteColor];
        _wkWebView.opaque = NO;
        if (@available(iOS 11.0, *)) {
            _wkWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else {
        }

        _wkWebView.scrollView.showsVerticalScrollIndicator = NO;
        [_wkWebView.scrollView addObserver:self forKeyPath:@"contentSize"
                               options:NSKeyValueObservingOptionNew context:nil];
    }
    return _wkWebView;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    CGFloat newHeight = self.wkWebView.scrollView.contentSize.height;
    if (newHeight != self.html.particularsWebViewHeight) {
        self.webViewFinishLoadBlock ? self.webViewFinishLoadBlock(newHeight) : nil;
    }
}
- (void)removeKVO{
    @try {
        if (_wkWebView) {
            [_wkWebView.scrollView removeObserver:self forKeyPath:@"contentSize"];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"多次删除kvo 报错了");
    }
}
@end
