//
//  WGGoodDetailImAndTCollectionReusableView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGoodDetailImAndTCollectionReusableView.h"

@interface WGGoodDetailImAndTCollectionReusableView ()

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIView *leftLineView;
@property (nonatomic ,strong) UIView *rightLineView;


@end

@implementation WGGoodDetailImAndTCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.titleLab];
        [self addSubview:self.leftLineView];
        [self addSubview:self.rightLineView];
        [self addLayout];
    }
    return self;
}
- (void)addLayout {
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    [_leftLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLab.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(35, 1));
        make.right.equalTo(self.titleLab.mas_left).offset(-15);
    }];
    [_rightLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLab.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(35, 1));
        make.left.equalTo(self.titleLab.mas_right).offset(15);
    }];

}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.textColor = color_TextTwo;
        _titleLab.text = @"产品详情";
    }
    return _titleLab;
}
- (UIView *)leftLineView{
    if (!_leftLineView) {
        _leftLineView = [UIView new];
        _leftLineView.backgroundColor = color_LineColor;
    }
    return _leftLineView;
}
- (UIView *)rightLineView{
    if (!_rightLineView) {
        _rightLineView = [UIView new];
        _rightLineView.backgroundColor = color_LineColor;
    }
    return _rightLineView;
}
@end
