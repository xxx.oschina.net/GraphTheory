//
//  WGLineCollectionReusableView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/27.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGLineCollectionReusableView.h"

@interface WGLineCollectionReusableView ()

@property (nonatomic ,strong) UIView *lineView;


@end

@implementation WGLineCollectionReusableView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout{
    [self addSubview:self.lineView];

    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self).insets(UIEdgeInsetsMake(0, 15, 0, 0));
    }];
}

- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

@end
