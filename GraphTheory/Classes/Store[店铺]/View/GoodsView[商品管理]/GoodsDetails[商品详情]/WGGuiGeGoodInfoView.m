//
//  WGGuiGeGoodInfoCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGuiGeGoodInfoView.h"
#import "UIViewController+KNSemiModal.h"
#import "WGGoodsInfoModel.h"
@interface WGGuiGeGoodInfoView ()

@property (nonatomic ,strong) UIButton *closeBtn;

@property (nonatomic ,strong) UIImageView *leftImgView;

@property (nonatomic ,strong) UILabel *priceLab;

@property (nonatomic ,strong) UILabel *inventoryLab;

@property (nonatomic ,strong) UILabel *noSelectLab;

@property (nonatomic ,strong) UIView *lineView;

@end

@implementation WGGuiGeGoodInfoView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    [_leftImgView sd_setImageWithURL:[NSURL URLWithString:model.imagePath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [self setPriceLabStr:model.price exchangeIntegral:model.exchangeIntegral];
    _inventoryLab.text = @"";
}
- (void)setType:(WGGoodsButtonType)type{
    _type = type;
    if (type == WGGoodsButtonTypeOneBuy) {
        if (_sku) {
            [self setPriceLabStr:_sku.singlePrice exchangeIntegral:_sku.exchangeIntegral];
        }else{
            [self setPriceLabStr:_model.singlePrice exchangeIntegral:_model.exchangeIntegral];
        }
    }else if (type == WGGoodsButtonTypeGrouponBuy){
        if (_sku) {
            [self setPriceLabStr:_sku.activityPrice exchangeIntegral:_sku.exchangeIntegral];
        }else{
            [self setPriceLabStr:_model.activityPrice exchangeIntegral:_model.exchangeIntegral];
        }
    }else{
        if (_sku) {
            [self setPriceLabStr:_sku.price exchangeIntegral:_sku.exchangeIntegral];
        }else{
            [self setPriceLabStr:_model.price exchangeIntegral:_model.exchangeIntegral];
        }

    }
}

- (void)setSku:(SpecSku *)sku{
    _sku = sku;
    _inventoryLab.hidden = YES;
    if (sku) {
        [_leftImgView sd_setImageWithURL:[NSURL URLWithString:_sku.imagePath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        if (_type == WGGoodsButtonTypeOneBuy) {
            [self setPriceLabStr:_sku.singlePrice exchangeIntegral:_sku.exchangeIntegral];
        }else if (_type == WGGoodsButtonTypeGrouponBuy){
            [self setPriceLabStr:_sku.activityPrice exchangeIntegral:_sku.exchangeIntegral];
        }else{
            [self setPriceLabStr:_sku.price exchangeIntegral:_sku.exchangeIntegral];
        }
        _inventoryLab.text = [NSString stringWithFormat:@"库存:%@",_sku.skuInfo];

    }else{
        [_leftImgView sd_setImageWithURL:[NSURL URLWithString:_model.imagePath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        if (_type == WGGoodsButtonTypeOneBuy) {
            [self setPriceLabStr:_sku.singlePrice exchangeIntegral:_sku.exchangeIntegral];
        }else if (_type == WGGoodsButtonTypeGrouponBuy){
            [self setPriceLabStr:_sku.activityPrice exchangeIntegral:_sku.exchangeIntegral];
        }else{
            [self setPriceLabStr:_sku.price exchangeIntegral:_sku.exchangeIntegral];
        }
        _inventoryLab.text = @"";
    }
}
- (void)setPriceLabStr:(NSString *)priceStr exchangeIntegral:(NSString *)exchangeIntegral{
    NSMutableString *price = [NSMutableString new];
    if (exchangeIntegral.floatValue > 0) {
        [price appendString:[NSString stringWithFormat:@"%@积分",exchangeIntegral]];
        if (priceStr.floatValue > 0) {
            [price appendString:@"+"];
        }
    }
    if (price.length > 0) {
        if (priceStr.floatValue > 0) {
            [price appendString:[NSString stringWithFormat:@"%@元",[NSString formatPriceString:priceStr]]];
            _priceLab.text = price;
        }
    }else{
        _priceLab.text = [NSString stringWithFormat:@"¥%@",[NSString formatPriceString:priceStr]];
    }
}
- (void)setNoSelct:(NSString *)noSelct{
    _noSelct = noSelct;
    _noSelectLab.text = noSelct;
}
- (void)setVc:(UIViewController *)vc{
    _vc = vc;
}
- (void)close{
    if ([_vc respondsToSelector:@selector(dismissSemiModalView)]) {
        [_vc dismissSemiModalView];
    }
}
- (void)setUpLayout{
    [self addSubview:self.closeBtn];
    [self addSubview:self.leftImgView];
    [self addSubview:self.priceLab];
    [self addSubview:self.inventoryLab];
    [self addSubview:self.noSelectLab];
    [self addSubview:self.lineView];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(40);
        make.right.equalTo(self.mas_right);
        make.top.equalTo(self.mas_top);
    }];
    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(120, 120));
    }];
    [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.leftImgView.mas_right).offset(10);
        make.bottom.equalTo(self.leftImgView.mas_centerY).offset(-20);
    }];
    [self.inventoryLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).offset(10);
        make.bottom.equalTo(self.leftImgView.mas_centerY).offset(5);
    }];
    [self.noSelectLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).offset(10);
        make.right.equalTo(self.mas_right).offset(-15);
        make.top.equalTo(self.priceLab.mas_bottom).offset(15);
    }];

    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(self.mas_right);
        make.height.equalTo(@1);
    }];
}
- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [[UIImageView alloc] init];
        _leftImgView.image = [UIImage imageNamed:@"图层1拷贝"];
        _leftImgView.layer.masksToBounds = YES;
        _leftImgView.layer.cornerRadius = 4.0f;
    }
    return _leftImgView;
}
- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_closeBtn setImage:[UIImage imageNamed:@"good_close"] forState:(UIControlStateNormal)];
        [_closeBtn addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}
- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] init];
        _priceLab.textColor = color_RedColor;
        _priceLab.font = [UIFont systemFontOfSize:20.0f];
        _priceLab.text = @"¥109.9";
    }
    return _priceLab;
}

- (UILabel *)inventoryLab {
    if (!_inventoryLab) {
        _inventoryLab = [[UILabel alloc] init];
        _inventoryLab.textColor = color_TextOne;
        _inventoryLab.font = [UIFont systemFontOfSize:14.0];
        _inventoryLab.text = @"库存273件";
    }
    return _inventoryLab;
}
- (UILabel *)noSelectLab {
    if (!_noSelectLab) {
        _noSelectLab = [[UILabel alloc] init];
        _noSelectLab.textColor = color_TextOne;
        _noSelectLab.font = [UIFont systemFontOfSize:14.0];
        _noSelectLab.numberOfLines = 0;
    }
    return _noSelectLab;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}
@end
