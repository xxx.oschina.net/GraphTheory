//
//  WGGoodDetailsInfoCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 商品详情-商品信息
 */
@class WGGoodsInfoModel;
@interface WGGoodDetailsInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGGoodsInfoModel *model;

@end

NS_ASSUME_NONNULL_END
