//
//  WGGoodDetailNormalPriceCollectionViewCell.m
//  DDLife
//
//  Created by wanggang on 2019/11/27.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "WGGoodDetailNormalPriceCollectionViewCell.h"
//#import <WMZTags.h>
#import "WGGoodsInfoModel.h"
@interface WGGoodDetailNormalPriceCollectionViewCell ()

/** 商品当前价格*/
@property (nonatomic ,strong) UILabel *curPriceLab;
/** 商品原价*/
@property (nonatomic ,strong) UILabel *originalPriceLab;
/** 原价栅格线*/
@property (nonatomic ,strong) UIView *gridLineView;
/** 月销量*/
@property (nonatomic ,strong) UILabel *salesLab;
//@property (strong, nonatomic) WMZTagParam *tagsModel;
//@property (strong, nonatomic) WMZTags *tagsView;


@end

@implementation WGGoodDetailNormalPriceCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.curPriceLab];
        [self.contentView addSubview:self.originalPriceLab];
        [self.contentView addSubview:self.gridLineView];
        [self.contentView addSubview:self.salesLab];
        [self addLayout];
    }
    return self;
}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    
    NSMutableString *price = [NSMutableString new];
    if (model.exchangeIntegral.floatValue > 0) {
        [price appendString:[NSString stringWithFormat:@"%@积分",model.exchangeIntegral]];
        if (model.price.floatValue > 0) {
            [price appendString:@"+"];
        }
    }
    if (price.length > 0) {
        if (model.price.floatValue > 0) {
            [price appendString:[NSString stringWithFormat:@"%@元",[NSString formatPriceString:model.price]]];
            _curPriceLab.text = price;
        }
    }else{
        _curPriceLab.text = [NSString stringWithFormat:@"¥%@",[NSString formatPriceString:model.price]];
    }
    if (model.oriPrice.length&&model.oriPrice.floatValue != 0) {
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle],NSBaselineOffsetAttributeName : @(0)};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString: [NSString stringWithFormat:@"¥%@",[NSString formatPriceString:model.oriPrice]] attributes:attribtDic];
        _originalPriceLab.attributedText = attribtStr;
        
    }else{
        _originalPriceLab.text = @"";
    }
    if (model.activityType == 3 && !model.isStart) {
        _originalPriceLab.text = @"";
    }
//    NSArray *temTagArr;
//    if (_model.activityType == 4) {
//        temTagArr =  @[[NSString stringWithFormat:@"%ld人团",(long)model.groupNumber]];
//    }else if (_model.activityType == 2){
//        temTagArr =  @[@"新人专享"];
//    }
//    else if (_model.activityTypeName.length&&_model.activityType > 0&&_model.isStart == 1){
//        temTagArr =  @[_model.activityTypeName];
//    }
//    self.tagsModel.wDataSet(temTagArr);
//    [self.tagsView updateUI];
    _salesLab.text = [NSString stringWithFormat:@"月销 %@",_model.monthSales?:@"0"];
}


- (UILabel *)curPriceLab {
    if (!_curPriceLab) {
        _curPriceLab = [[UILabel alloc] init];
        _curPriceLab.textColor = color_RedColor;
        _curPriceLab.font = [UIFont boldSystemFontOfSize:23.0f];
        //        _curPriceLab.text = @"¥109.9";
    }
    return _curPriceLab;
}

- (UILabel *)originalPriceLab {
    if (!_originalPriceLab) {
        _originalPriceLab = [[UILabel alloc] init];
        _originalPriceLab.textColor = color_TextThree;
        _originalPriceLab.font = [UIFont systemFontOfSize:14.0f];
        //        _originalPriceLab.text = @"¥190";
        
    }
    return _originalPriceLab;
}

- (UIView *)gridLineView {
    if (!_gridLineView) {
        _gridLineView = [[UIView alloc] init];
        _gridLineView.backgroundColor = color_TextThree;
    }
    return _gridLineView;
}

- (UILabel *)salesLab {
    if (!_salesLab) {
        _salesLab = [[UILabel alloc] init];
        _salesLab.textColor = color_TextThree;
        _salesLab.font = [UIFont systemFontOfSize:12.0f];
        //        _salesLab.text = @"月销量：126";
    }
    return _salesLab;
}
//- (WMZTagParam *)tagsModel{
//    if (!_tagsModel) {
//        _tagsModel = TagParam()
//        .wRadiusSet(2)
//        .wInnerColorSet([UIColor colorWithHexString:@"#FF8A09"])
//        .marginTopSet(0)
//        .marginLeftSet(0)
//        .marginRightSet(0)
//        .marginBottomSet(0)
//        .paddingTopSet(1.5)
//        .paddingLeftSet(5)
//        .btnTopSet(3)
//        .btnLeftSet(10)
//        .wBackGroundColorSet([UIColor clearColor]).wFontSet(12).wColorSet([UIColor whiteColor]).wMasonrySet(^(MASConstraintMaker * _Nonnull make) {
//            make.bottom.equalTo(self.originalPriceLab.mas_baseline);
//            make.left.equalTo(self.originalPriceLab.mas_right).offset(15);
//            make.right.equalTo(self.salesLab.mas_left).offset(-10);
//        });
//        
//    }
//    return _tagsModel;
//}
//- (WMZTags *)tagsView{
//    if (!_tagsView) {
//        _tagsView = [[WMZTags alloc]initConfigureWithModel:self.tagsModel withView:self];
//    }
//    return _tagsView;
//}
- (void)addLayout {
    
    YXWeakSelf
    [_curPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        [weakSelf.curPriceLab sizeToFit];
    }];
    
    [_originalPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.curPriceLab.mas_right).offset(5);
        make.baseline.equalTo(weakSelf.curPriceLab.mas_baseline).offset(0);
        [weakSelf.originalPriceLab sizeToFit];
        
    }];
    [_salesLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.curPriceLab.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        [weakSelf.salesLab sizeToFit];
    }];
}
@end
