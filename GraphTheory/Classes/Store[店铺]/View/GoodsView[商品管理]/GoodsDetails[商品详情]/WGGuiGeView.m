//
//  WGGuiGeView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGuiGeView.h"
#import "WGGuiGeCountCollectionViewCell.h"
#import "WGGuiGeDetailCollectionViewCell.h"
#import "WGGuiGeGoodInfoView.h"
#import "WGGuiGeHeadCollectionReusableView.h"
#import "WGLineCollectionReusableView.h"
#import "ORSKUDataFilter.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "UIViewController+KNSemiModal.h"
#import "WGGoodsInfoModel.h"
@interface WGGuiGeView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ORSKUDataFilterDataSource>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) WGGuiGeGoodInfoView *headView;
@property (strong, nonatomic) UIView           *rightView;
@property (strong, nonatomic) UIButton         *bottomBtn;
@property (nonatomic, strong) ORSKUDataFilter *filter;
@property (nonatomic, strong) NSString *noSelect;
@property (strong, nonatomic) UILabel          *cannotLab;//购物车数量

@end

@implementation WGGuiGeView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
        _filter = [[ORSKUDataFilter alloc] initWithDataSource:self];
    }
    return self;
}
- (void)setModel:(WGGoodsInfoModel *)model{
    _model = model;
    if (model.specList.count != 0) {
        if (model.specSku.count == 1) {
            _filter.needDefaultValue = YES;
        }
    }
    //    _filter.needDefaultValue = YES;
    [_filter reloadData];
    [self action_complete:nil];
    
    _headView.model = model;
    
    [self.collectionView reloadData];
}
- (void)setViewsModel:(WGGoodsDetailViewModel *)viewsModel{
    _viewsModel = viewsModel;
//    [self setLayouButtons];
}

//- (void)setGoodStatus:(NSInteger)goodStatus{
//    _goodStatus = goodStatus;
//    _cannotLab.hidden = YES;
//    if (_goodStatus == 0) {
//        for (UIButton *btn in _rightView.subviews) {
//            if (_viewsModel.goodsInfo.activityType == 4 && btn.tag == WGGoodsButtonTypeOneBuy) {
//                [_bottomBtn setBackgroundColor:APPTintColor];
//                _bottomBtn.enabled = YES;
//            }else{
//                [btn setBackgroundColor:[UIColor colorWithHexString:@"#999999"]];
//                btn.enabled = NO;
//            }
//        }
//        if (_viewsModel.goodsInfo.activityType == 4 && _type == WGGoodsButtonTypeOneBuy) {
//            [_bottomBtn setBackgroundColor:APPTintColor];
//            _bottomBtn.enabled = YES;
//            _cannotLab.hidden = YES;
//
//        }else{
//            [_bottomBtn setBackgroundColor:[UIColor colorWithHexString:@"#999999"]];
//            _bottomBtn.enabled = NO;
//            _cannotLab.hidden = NO;
//        }
//
//    }else{
//        [self updataBottomNomal];
//        _cannotLab.hidden = YES;
//        [_bottomBtn setBackgroundColor:APPTintColor];
//        _bottomBtn.enabled = YES;
//    }
//}
//- (void)updataBottomNomal{
//    for (UIButton *btn in _rightView.subviews) {
//        WGGoodsButtonType type = btn.tag;
//        if (type == WGGoodsButtonTypeAddCart){
//            NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"加入购物车" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            [self setRightBtn:btn title:title backColor:color_OrangeColor enable:YES];
//
//        }else if (type == WGGoodsButtonTypeBuyNow){
//            NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"立即购买" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            [self setRightBtn:btn title:title backColor:color_GreenColor enable:YES];
//        }else if (type == WGGoodsButtonTypeOneBuy){
//            NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",[NSString formatPriceString:_model.selectSku?_model.selectSku.singlePrice:_model.singlePrice]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            NSAttributedString *buy = [[NSAttributedString alloc] initWithString:@"\r单独购买" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            [title appendAttributedString:buy];
//            [self setRightBtn:btn title:title backColor:[UIColor colorWithHexString:@"#E41818" alpha:0.5] enable:YES];
//        }else if (type == WGGoodsButtonTypeGrouponBuy){
//            NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",[NSString formatPriceString:_model.selectSku?_model.selectSku.activityPrice:_model.activityPrice]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            NSAttributedString *buy = [[NSAttributedString alloc] initWithString:@"\r发起拼团" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor whiteColor]}];
//            [title appendAttributedString:buy];
//            [self setRightBtn:btn title:title backColor:[UIColor colorWithHexString:@"#E41818"] enable:YES];
//
//        }
//    }
//}
//- (void)setLayouButtons{
//    UIView *rightLastView;
//    for (UIView *view in rightLastView.subviews) {
//        [view removeFromSuperview];
//    }
//
//    if (_viewsModel.goodsType == WGGoodsTypePoint) {
//        UIButton *applyBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        applyBtn.backgroundColor = APPTintColor;
//        applyBtn.layer.masksToBounds = YES;
//        applyBtn.layer.cornerRadius = 3.5;
//        applyBtn.titleLabel.font = [UIFont systemFontOfSize:16.0];
//        [applyBtn setTitle:@"确认兑换" forState:UIControlStateNormal];
//        [applyBtn addTarget:self action:@selector(refundAction) forControlEvents:(UIControlEventTouchUpInside)];
//        [self addSubview:applyBtn];
//        [applyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.bottom.equalTo(self.mas_bottom).offset(-Home_Indicator_HEIGHT);
//            make.width.equalTo(self.mas_width);
//            make.height.equalTo(@50);
//            make.centerX.equalTo(self.mas_centerX);
//        }];
//        return;
//    }
//
//    for (int i = 0 ; i < _viewsModel.goodsBottomAction.rightButtons.count; i ++) {
//        WGGoodsButtonType type = [_viewsModel.goodsBottomAction.rightButtons[i] integerValue];
//        UIView *temView = [self creatActionView:type];
//        [self.rightView addSubview:temView];
//        [temView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.bottom.equalTo(self.rightView);
//            if (rightLastView) {
//                make.left.equalTo(rightLastView.mas_right).offset(1);
//                make.width.equalTo(rightLastView.mas_width);
//            }else{
//                make.left.equalTo(self.rightView.mas_left);
//            }
//            if (i == self.viewsModel.goodsBottomAction.rightButtons.count - 1) {
//                make.right.equalTo(self.rightView.mas_right);
//            }
//        }];
//        rightLastView = temView;
//    }
//}
//- (UIView *)creatActionView:(WGGoodsButtonType)type{
//    UIButton *actionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [actionBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
//    actionBtn.tag = type;
//    if (type == WGGoodsButtonTypeAddCart){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"加入购物车" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [self setRightBtn:actionBtn title:title backColor:color_OrangeColor enable:YES];
//
//    }else if (type == WGGoodsButtonTypeBuyNow){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"立即购买" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [self setRightBtn:actionBtn title:title backColor:color_GreenColor enable:YES];
//    }else if (type == WGGoodsButtonTypeGrouponIng){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"立即购买" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [self setRightBtn:actionBtn title:title backColor:color_GreenColor enable:YES];
//    }else if (type == WGGoodsButtonTypeGrouponBegin){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:@"发起拼团" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [self setRightBtn:actionBtn title:title backColor:color_GreenColor enable:YES];
//    }else if (type == WGGoodsButtonTypeOneBuy){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",[NSString formatPriceString:_model.selectSku?_model.selectSku.singlePrice:_model.singlePrice]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        NSAttributedString *buy = [[NSAttributedString alloc] initWithString:@"\r单独购买" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [title appendAttributedString:buy];
//        [self setRightBtn:actionBtn title:title backColor:[UIColor colorWithHexString:@"#E41818" alpha:0.5] enable:YES];
//    }else if (type == WGGoodsButtonTypeGrouponBuy){
//        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"¥%@",[NSString formatPriceString:_model.selectSku?_model.selectSku.activityPrice:_model.activityPrice]] attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        NSAttributedString *buy = [[NSAttributedString alloc] initWithString:@"\r发起拼团" attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [title appendAttributedString:buy];
//        [self setRightBtn:actionBtn title:title backColor:[UIColor colorWithHexString:@"#E41818"] enable:YES];
//
//    }
//    return actionBtn;
//}

//- (void)setRightBtn:(UIButton *)actionBtn title:(NSMutableAttributedString *)title backColor:(UIColor *)backColor enable:(BOOL)enable{
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//    paragraphStyle.lineSpacing = 2;
//    paragraphStyle.alignment = NSTextAlignmentCenter;
//    [title addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, title.length)];
//    actionBtn.backgroundColor = backColor;
//    actionBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        actionBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
//    actionBtn.titleLabel.numberOfLines = 0;
//    [actionBtn setAttributedTitle:title forState:UIControlStateNormal];
//    actionBtn.enabled = enable;
//}


//- (void)setType:(WGGoodsButtonType)type{
//    _type = type;
//    if (type != WGGoodsButtonTypeNone) {
//        _bottomBtn.hidden = NO;
//        _rightView.hidden = YES;
//    }else{
//        _bottomBtn.hidden = YES;
//        _rightView.hidden = NO;
//    }
//    if (_viewsModel.goodsInfo.activityType == 4 && _type == WGGoodsButtonTypeOneBuy) {
//        [_bottomBtn setBackgroundColor:APPTintColor];
//        _bottomBtn.enabled = YES;
//        _cannotLab.hidden = YES;
//        _cannotLab.hidden = YES;
//    }
//    _headView.type = type;
//}
- (void)setVc:(UIViewController *)vc{
    _vc = vc;
    _headView.vc = vc;
}
#pragma mark -- collectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return _model.specListSort.count + (_viewsModel.goodsType == WGGoodsTypePoint?0:1);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section == _model.specListSort.count) {
        return 1;
    }
    return _model.specListSort[section].specValue.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == _model.specListSort.count){
        return CGSizeMake(KWIDTH, 40);
    }else{
        NSString *guige = _model.specListSort[indexPath.section].specValue[indexPath.row];
        CGSize guiGeSize = [guige sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0f]}];
        return CGSizeMake(guiGeSize.width + 30, 35);
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section == _model.specListSort.count ) {
        return 0;
    }else{
        return 15;
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section == _model.specListSort.count) {
        return 0;
    }else{
        return 15;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == _model.specListSort.count) {
        return CGSizeZero;
    }else{
        return CGSizeMake(KWIDTH, 40);
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 1);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == _model.specListSort.count ) {
        return UIEdgeInsetsZero;
    }else{
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == _model.specListSort.count){
        YXWeakSelf;
        WGGuiGeCountCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGuiGeCountCollectionViewCell" forIndexPath:indexPath];
        cell.model = _model;
        [cell setUpDataModel:^(WGGoodsInfoModel * _Nonnull model) {
//            if (weakSelf.upDataModel) {
//                weakSelf.upDataModel(model);
//            }
        }];
        return cell;
    }else{
        WGGuiGeDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGuiGeDetailCollectionViewCell" forIndexPath:indexPath];
        cell.model = _model.specListSort[indexPath.section].specValue[indexPath.row];    if ([_filter.availableIndexPathsSet containsObject:indexPath]) {
            cell.type = 0;
        }else {
            cell.type = 2;
        }
        
        if ([_filter.selectedIndexPaths containsObject:indexPath]) {
            cell.type = 1;
        }
        return cell;
    }
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section < _model.specListSort.count ) {
            WGGuiGeHeadCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGGuiGeHeadCollectionReusableView" forIndexPath:indexPath];
            view.title = _model.specListSort[indexPath.section].specName;
            return view;
        }
        
        
    }else {
        WGLineCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGLineCollectionReusableView" forIndexPath:indexPath];
        return view;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([_filter.availableIndexPathsSet containsObject:indexPath]) {
        [_filter didSelectedPropertyWithIndexPath:indexPath];
        
        [collectionView reloadData];
        [self action_complete:nil];
    }
    
}

#pragma mark -- ORSKUDataFilterDataSource

- (NSInteger)numberOfSectionsForPropertiesInFilter:(ORSKUDataFilter *)filter {
//    NSLog(@"%@",_model.specListSort);
    return _model.specListSort.count;
}

- (NSArray *)filter:(ORSKUDataFilter *)filter propertiesInSection:(NSInteger)section {
//    NSLog(@"%@",_model.specListSort);
    return  _model.specListSort[section].specValue;
}

- (NSInteger)numberOfConditionsInFilter:(ORSKUDataFilter *)filter {
//    NSLog(@"%@",_model.specSkuSort);
    return _model.specSkuSort.count;
}

- (NSArray *)filter:(ORSKUDataFilter *)filter conditionForRow:(NSInteger)row {
//    NSLog(@"%@",_model.specSkuSort);
    return _model.specSkuSort[row].specSort;
}

- (id)filter:(ORSKUDataFilter *)filter resultOfConditionForRow:(NSInteger)row {
    SpecSku *tem = _model.specSkuSort[row];
    return tem;
}
- (IBAction)action_complete:(id)sender {
    _noSelect = @"";
    SpecSku *dic = _filter.currentResult;
    
    if (_filter.selectedIndexPaths.count == 0) {
        _headView.model = _model;
    }
    NSMutableArray *arr = [NSMutableArray new];
    for (SpecList *list in _model.specListSort) {
        [arr addObject:list.specName];
    }
    if ((_filter.selectedIndexPaths.count == _model.specListSort.count)&&_filter.selectedIndexPaths.count != 0) {
        _headView.sku = dic;
        _noSelect = [NSString stringWithFormat:@"已选:%@",[dic.spec componentsJoinedByString:@","]];
    }
    if (_filter.selectedIndexPaths.count < _model.specListSort.count) {
//                for (NSIndexPath *index in _filter.selectedIndexPaths) {
//                    [arr removeObject:_model.specListSort[index.section].specName];
//                }
//                _noSelect = [NSString stringWithFormat:@"请选择:%@",[arr componentsJoinedByString:@" "]];
        _noSelect = [NSString stringWithFormat:@"请选择:%@",@"规格"];
    }
    _headView.noSelct = _noSelect;
    _model.selectSku = dic;
    _model.noSelect = _noSelect;
//    [self updataBottomNomal];
//    if (self.upDataModel) {
//        self.upDataModel(_model);
//    }
    NSLog(@"----%@",arr);
}
- (void)refundAction{
    if (_model.specSkuSort.count == 0||_model.selectSku) {
        if ([_vc respondsToSelector:@selector(dismissSemiModalView)]) {
            [_vc dismissSemiModalView];
        }
//        if (self.actionType) {
//            self.actionType(WGGoodsButtonTypePointExchange);
//        }
    }else{
        KPOP(_noSelect);
    }
}
- (void)setCannotStr:(NSString *)cannotStr{
    _cannotStr = cannotStr;
    _cannotLab.text = cannotStr;
}
- (void)sureAction:(UIButton *)btn{
    if (_model.specSkuSort.count == 0||_model.selectSku) {
        if ([_vc respondsToSelector:@selector(dismissSemiModalView)]) {
            [_vc dismissSemiModalView];
        }
//        if (self.actionType) {
//            if (btn.tag == WGGoodsButtonTypeNone) {
//                self.actionType(_type);
//            }else{
//                self.actionType(btn.tag);
//            }
//        }
    }else{
        KPOP(_noSelect);
    }
}

- (void)setUpLayout{
    [self addSubview:self.headView];
    [self addSubview:self.collectionView];
    [self addSubview:self.bottomBtn];
    [self addSubview:self.cannotLab];
    [self addSubview:self.rightView];
    
    CGFloat bottom = Home_Indicator_HEIGHT;
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.right.equalTo(self);
        make.height.equalTo(@150);
    }];
    [self.rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.bottom.and.right.equalTo(self);
        make.height.equalTo(@50);
    }];
    [self.bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).offset(-bottom);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(@50);
        make.centerX.equalTo(self.mas_centerX);
    }];
    [_cannotLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.bottom.equalTo(self.bottomBtn.mas_top);
        make.height.mas_equalTo(20);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.equalTo(self.headView.mas_bottom);
        make.bottom.equalTo(self.bottomBtn.mas_top);
    }];
}
- (UILabel *)cannotLab{
    if (!_cannotLab) {
        _cannotLab = [UILabel new];
        _cannotLab.textColor = [UIColor colorWithHexString:@"ff8a09"];
        _cannotLab.font = [UIFont systemFontOfSize:14];
        _cannotLab.text = @"此商品不在该区域销售,请更改送货地址";
        _cannotLab.backgroundColor = [UIColor colorWithHexString:@"ff8a09" alpha:0.2];
        _cannotLab.textAlignment = NSTextAlignmentCenter;
        _cannotLab.hidden = YES;
    }
    return _cannotLab;
}

- (UIButton *)bottomBtn{
    if (!_bottomBtn) {
        _bottomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _bottomBtn.tag = WGGoodsButtonTypeNone;
        [_bottomBtn setBackgroundColor:APPTintColor];
        [_bottomBtn setTitle:@"确定" forState:(UIControlStateNormal)];
        [_bottomBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_bottomBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
        _bottomBtn.hidden = YES;
    }
    return _bottomBtn;
}
- (WGGuiGeGoodInfoView *)headView{
    if (!_headView) {
        _headView = [[WGGuiGeGoodInfoView alloc]initWithFrame:CGRectZero];
    }
    return _headView;
}
- (UIView *)rightView{
    if (!_rightView) {
        _rightView = [UIView new];
    }
    return _rightView;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewLeftAlignedLayout * flowLayout = [[UICollectionViewLeftAlignedLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[WGGuiGeDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGGuiGeDetailCollectionViewCell"];
        [_collectionView registerClass:[WGGuiGeCountCollectionViewCell class] forCellWithReuseIdentifier:@"WGGuiGeCountCollectionViewCell"];
        [_collectionView registerClass:[WGGuiGeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGuiGeHeadCollectionReusableView"];
        [_collectionView registerClass:[WGLineCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"WGLineCollectionReusableView"];
        
    }
    return _collectionView;
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */



@end
