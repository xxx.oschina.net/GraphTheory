//
//  WGLineCollectionReusableView.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/27.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGLineCollectionReusableView : UICollectionReusableView



@end

NS_ASSUME_NONNULL_END
