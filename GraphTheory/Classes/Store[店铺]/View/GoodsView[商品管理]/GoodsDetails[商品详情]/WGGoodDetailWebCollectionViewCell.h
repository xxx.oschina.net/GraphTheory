//
//  WGGoodDetailWebCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/30.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 商品详情-图文详情
 */
@class WGGoodsInfoModel;
@interface WGGoodDetailWebCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGGoodsInfoModel *html;
@property (nonatomic,strong) void (^webViewFinishLoadBlock)(CGFloat webViewHeight);

- (void)removeKVO;

@end

NS_ASSUME_NONNULL_END
