//
//  WGGuiGeDetailCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/26.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGGuiGeDetailCollectionViewCell.h"

@interface WGGuiGeDetailCollectionViewCell ()


@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *guigeLab;

@end

@implementation WGGuiGeDetailCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout{
    [self.contentView addSubview:self.bgView];
    [self.bgView addSubview:self.guigeLab];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    [self.guigeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.bgView);
    }];
}
- (void)setModel:(NSString *)model{
    _model = model;
    _guigeLab.text = model;
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 0) {
        _bgView.layer.borderColor = APPTintColor.CGColor;
        _bgView.backgroundColor = [UIColor whiteColor];
        _guigeLab.textColor = APPTintColor;
    }else if (_type == 1){
        _bgView.layer.borderColor = APPTintColor.CGColor;
        _bgView.backgroundColor = APPTintColor;
        _guigeLab.textColor = [UIColor whiteColor];
    }else{
        _bgView.layer.borderColor = color_TextThree.CGColor;
        _bgView.backgroundColor = [UIColor whiteColor];
        _guigeLab.textColor = color_TextThree;
    }
}
- (UIView *)bgView{
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.layer.cornerRadius = 3;
        _bgView.layer.borderWidth = 1;
        _bgView.layer.borderColor = APPTintColor.CGColor;
    }
    return _bgView;
}
- (UILabel *)guigeLab{
    if (!_guigeLab) {
        _guigeLab = [UILabel new];
        _guigeLab.textColor = APPTintColor;
        _guigeLab.font = [UIFont systemFontOfSize:14];
        _guigeLab.textAlignment = NSTextAlignmentCenter;
    }
    return _guigeLab;
}
@end
