//
//  ZLJGoodsMoreCell.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJGoodsMoreCell.h"
#import "ZLJBottomButtonView.h"
#import "ZLJItemsManageModel.h"


@interface ZLJGoodsMoreCell ()
{
    NSArray <NSString *>*_titleArray;
}
@property (nonatomic,strong) UIView *editBackView;//点击编辑时候使用 -- 父视图（除编辑选择框） 

@property (nonatomic,strong) UIImageView *goodsImgView;//商品图片

@property (nonatomic,strong) UILabel *moneyLab;//金额

@property (nonatomic,strong) UILabel *kucunLab;//库存

@property (nonatomic,strong) UILabel *xiaoliangLab;//销量

@property (nonatomic,strong) UILabel *titleLab;//标题

@property (nonatomic,strong) ZLJBottomButtonView *bottomView;//底部按钮视图

@property (nonatomic,strong) UIButton *cellSelectedBtn;//cell选择转态Button

@property (nonatomic,copy) CellButtonClickBlock cellButtonBlock;//cell上面点击按钮

@property (nonatomic,strong) ItemsManageItemList *cellClickModel;//cell上点击按钮传值model
@end


@implementation ZLJGoodsMoreCell

- (void)cellModel:(ItemsManageItemList *)cellModel cellButtonClick:(CellButtonClickBlock)cellButtonClick{
    
    self.cellClickModel = cellModel;
    self.cellButtonBlock = cellButtonClick;
    
    NSString *imgString = cellModel.imagePath;
    
    DD_STRING_NN(imgString);
    NSURL *imgUrl = [NSURL URLWithString:[imgString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    [self.goodsImgView sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
    DD_STRING_NN(cellModel.price);
    NSString    *priceString = [NSString stringWithFormat:@"￥%@",cellModel.price];
    self.moneyLab.text = priceString;

    NSString    *zljKuCun = cellModel.itemStock?:@"0";//zlj -- 京东不现实库存
    if ([zljKuCun isEqualToString:@"0"] && [cellModel.source isEqualToString:@"01"]) {
        self.kucunLab.text = @"";
    }else{

        self.kucunLab.text = [NSString stringWithFormat:@"库存:%@",cellModel.itemStock?:@"0"];
    }
    self.xiaoliangLab.text = [NSString stringWithFormat:@"销量:%@",cellModel.totalSales?:@"0"];
    DD_STRING_NN(cellModel.itemName);
    
    self.titleLab.attributedText = [self titleLabelString:cellModel.itemName];
}

- (void)editCell:(BOOL )editType{
    
    if (editType) {
        [self.editBackView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 40, 0, 0));
        }];
        self.bottomView.alpha = 0;
        [@[self.bottomView] mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0);
        }];
        
    }else{
        [self.editBackView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        self.bottomView.alpha = 1;
        [@[self.bottomView] mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.mas_equalTo(self.editBackView);
            make.top.mas_equalTo(self.goodsImgView.mas_bottom).offset(15);
        }];
    }
}
/*
 ZLJMoreGroupView 加载的cell方法
 */
- (instancetype)initMoreWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier titleArray:(NSArray <NSString *>*)titleArray{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleArray = titleArray;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviewsTo];
    }
    return self;
}

- (void)addSubviewsTo{
    self.cellSelectedBtn.selected = YES;
    [self.contentView addSubview:self.cellSelectedBtn];
    [self.cellSelectedBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(44);
        make.size.mas_equalTo(CGSizeMake(12, 12));
    }];
    
    [self.contentView addSubview:self.editBackView];
    [self.editBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    //添加视图
    [self.editBackView addSubview:self.goodsImgView];
    [self.editBackView addSubview:self.moneyLab];
    [self.editBackView addSubview:self.kucunLab];
    [self.editBackView addSubview:self.xiaoliangLab];
    [self.editBackView addSubview:self.titleLab];
    
    [self.editBackView addSubview:self.bottomView];
    
    CGFloat imgViewWidth = 60.0f;
    CGFloat moneyLabToLeft = 80.0f; // 图片到右侧距离
    CGFloat titleLabLineHeight = 18.0f;//titlelLable单行高度
    self.moneyLab.font = [UIFont systemFontOfSize:14.0f];
    self.kucunLab.font = [UIFont systemFontOfSize:10.0f];
    self.xiaoliangLab.font = [UIFont systemFontOfSize:10.0f];
    //布局
    [self.goodsImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(10);
        make.size.mas_equalTo(CGSizeMake(imgViewWidth, imgViewWidth));
    }];
    NSArray *viewArray = @[self.moneyLab,self.kucunLab,self.xiaoliangLab];
    [viewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:moneyLabToLeft tailSpacing:0];
    [viewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodsImgView);
        make.height.mas_equalTo(@16);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.goodsImgView);
        make.left.mas_equalTo(self.goodsImgView.mas_right).offset(10);
        make.right.mas_equalTo(-5);
        make.height.mas_greaterThanOrEqualTo(titleLabLineHeight);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.mas_equalTo(self);
        make.top.mas_equalTo(self.goodsImgView.mas_bottom);
    }];
    
    
#if TARGET_IPHONE_SIMULATOR 
    self.goodsImgView.backgroundColor = [UIColor colorWithHexString:@"f8f8f8"];
    self.moneyLab.text = @"￥123.20";
    self.kucunLab.text = @"库存:100";
    self.xiaoliangLab.text = @"销量:100";
    self.titleLab.backgroundColor = [UIColor colorWithHexString:@"f8f8f8"];
#endif 
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    self.cellSelectedBtn.selected = selected;
}

- (NSAttributedString *)titleLabelString:(NSString *)string{
    
    //NSKernAttributeName:@1.5f 字间距 
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineSpacing = 5.0f;
    paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;//省略号位置
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                          NSParagraphStyleAttributeName:paraStyle};
    NSAttributedString  *attributeStr = [[NSAttributedString alloc] initWithString:string attributes:dic];
    return attributeStr;
}

//TODO:视图布局
- (UIButton *)cellSelectedBtn{
    if (!_cellSelectedBtn) {
        _cellSelectedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cellSelectedBtn setImage:[UIImage imageNamed:@"cell_normal"] forState:UIControlStateNormal];
        [_cellSelectedBtn setImage:[UIImage imageNamed:@"cell_selected"] forState:UIControlStateSelected];
        _cellSelectedBtn.userInteractionEnabled = NO;
    }
    return _cellSelectedBtn;
}

- (UIView *)editBackView{
    if (!_editBackView) {
        _editBackView = [[UIView alloc] init];
        _editBackView.backgroundColor = UIColor.whiteColor;
    }
    return _editBackView;
}


- (ZLJBottomButtonView *)bottomView{
    if (!_bottomView) {
         __weak typeof(self) weakSelf = self;
        _bottomView = [[ZLJBottomButtonView alloc] initWithButtonTitle:_titleArray buttonSize:CGSizeMake(40.0f, 22.0f) clickButton:^(UIButton * _Nonnull button) {
            NSLog(@"zlj--当前输出：%s",__FUNCTION__);
            if (weakSelf.cellButtonBlock) {
                weakSelf.cellButtonBlock(weakSelf.cellClickModel,button);
            }
        }];
    }
    return _bottomView;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor colorWithHexString:@"333333"];
        _titleLab.numberOfLines = 2;
    }
    return _titleLab;
}

-(UILabel *)moneyLab{//金额
    if (!_moneyLab) {
        _moneyLab = [[UILabel alloc] init];
        _moneyLab.textColor = [UIColor colorWithHexString:@"E41818"];
        
    }
    return _moneyLab;
}

-(UILabel *)kucunLab{//库存
    if (!_kucunLab) {
        _kucunLab = [[UILabel alloc] init];
        _kucunLab.textColor = [UIColor colorWithHexString:@"666666"];
        _kucunLab.textAlignment = NSTextAlignmentCenter;
    }
    return _kucunLab;
}

-(UILabel *)xiaoliangLab{//销量
    if (!_xiaoliangLab) {
        _xiaoliangLab = [[UILabel alloc] init];
        _xiaoliangLab.textColor = [UIColor colorWithHexString:@"666666"];
        _xiaoliangLab.textAlignment = NSTextAlignmentCenter;
        
    }
    return _xiaoliangLab;
}

- (UIImageView *)goodsImgView{//图片视图
    if (!_goodsImgView) {
        _goodsImgView = [[UIImageView alloc] init];
    }
    return _goodsImgView;
}

@end

