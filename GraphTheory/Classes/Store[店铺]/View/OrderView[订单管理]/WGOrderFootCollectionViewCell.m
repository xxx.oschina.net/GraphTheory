//
//  WGOrderFootCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderFootCollectionViewCell.h"
#import "YXRefundModel.h"
@interface WGOrderFootCollectionViewCell ()

@property (strong, nonatomic)  UIView *backView;

@end

@implementation WGOrderFootCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)addLayout{
    [self.contentView addSubview:self.backView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}

- (void)setModel:(WGShopModel *)model{
    _model = model;
    for(UIView *view in [self.backView subviews])
    {
        [view removeFromSuperview];
    }

    UIButton *lastBtn;
    
    NSArray *arr = [[_model.actionArr reverseObjectEnumerator]allObjects];
    for (int i = 0; i < arr.count; i ++) {
        WGOderActionModel *tem = arr[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 1000 + i;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitleColor:APPTintColor forState:UIControlStateNormal];
        btn.layer.borderColor = APPTintColor.CGColor;
        [btn addTarget:self action:@selector(actionBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 4;
        [btn setTitle:tem.actionTitle forState:UIControlStateNormal];
        [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
        [_backView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (lastBtn) {
                make.right.equalTo(lastBtn.mas_left).offset(-15);
            }else{
                make.right.equalTo(self.backView.mas_right).offset(-15);
            }
            make.height.equalTo(@28);
            make.centerY.equalTo(self.backView.mas_centerY);
        }];
        lastBtn = btn;
    }
}

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    for(UIView *view in [self.backView subviews])
    {
        [view removeFromSuperview];
    }
    
    UIButton *lastBtn;
    
    NSArray *arr = [[_refundModel.actionArr reverseObjectEnumerator]allObjects];
    for (int i = 0; i < arr.count; i ++) {
        YXRefundActionModel *tem = arr[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.tag = 1000 + i;
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitleColor:color_TextThree forState:UIControlStateNormal];
        btn.layer.borderColor = color_TextThree.CGColor;
        [btn addTarget:self action:@selector(actionBtn:) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.borderWidth = 1;
        btn.layer.cornerRadius = 4;
        [btn setTitle:tem.actionTitle forState:UIControlStateNormal];
        [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
        [_backView addSubview:btn];
        
        [btn mas_makeConstraints:^(MASConstraintMaker *make) {
            if (lastBtn) {
                make.right.equalTo(lastBtn.mas_left).offset(-15);
            }else{
                make.right.equalTo(self.backView.mas_right).offset(-15);
            }
            make.height.equalTo(@28);
            make.centerY.equalTo(self.backView.mas_centerY);
        }];
        lastBtn = btn;
    }
    
}


- (void)actionBtn:(UIButton *)btn{
    
    if (_model) {
        if (self.orderAction) {
            NSArray *arr = [[_model.actionArr reverseObjectEnumerator]allObjects];
            WGOderActionModel *tem = arr[btn.tag - 1000];
            self.orderAction(tem.actionType);
            
            //        if (btn.tag - 1000 == 0) {
            //            if (_model.orderState == 1) {
            //                self.orderAction(OrderActionTypeCancle);
            //            }else if (_model.orderState == 2){
            //                if (_model.mainDistributionType.integerValue == 3) {
            //                    self.orderAction(OrderActionTypeCancle);
            //                }else if (_model.mainDistributionType.integerValue == 2){
            //                    self.orderAction(OrderActionTypeCancle);
            //                }
            //            }else if (_model.orderState == 3){
            //                self.orderAction(OrderActionTypeDistribution);
            //            }else if (_model.orderState == 4 && _model.mainDistributionType.integerValue == 1){
            //                self.orderAction(OrderActionTypeDistribution);
            //            }else if (_type == 0){
            //                self.orderAction(OrderActionTypeRemindPay);
            //            }
            //        }else if (btn.tag - 1000 == 1){
            //            if (_model.orderState == 1) {
            //                self.orderAction(OrderActionTypeDeliver);
            //            }else if (_model.orderState == 2){
            //                if (_model.mainDistributionType.integerValue == 3) {
            //                    self.orderAction(OrderActionTypeSelctClerk);
            //                }else if (_model.mainDistributionType.integerValue == 2){
            //                    self.orderAction(OrderActionTypeVerifyPayNO);
            //                }
            //            }
            //        }else if (btn.tag - 1000 == 2){
            //            if (_model.orderState == 2){
            //                if (_model.mainDistributionType.integerValue == 2){
            //                    self.orderAction(OrderActionTypeDone);
            //                }
            //            }
            //        }
        }
    }
 
    if (_refundModel) {
        if (_refundAction) {
            NSArray *arr = [[_refundModel.actionArr reverseObjectEnumerator]allObjects];
            YXRefundActionModel *tem = arr[btn.tag - 1000];
            self.refundAction(tem);
        }
    }
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}

@end
