//
//  WGOrderGoodCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单商品
 */
@class YXRefundModel,YXRefundItemsModel;
@interface WGOrderGoodCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGOderDetailModel *detailModel;
@property (nonatomic, strong) WGShopModel *orderModel;
@property (nonatomic, assign) NSInteger type;///<0:订单列表 1:订单详情
@property (nonatomic ,assign) BOOL isHideTotal;
@property (nonatomic ,assign) BOOL isHideChange;


// 售后
@property (nonatomic, strong) YXRefundModel *refundModel;
@property (nonatomic, strong) YXRefundItemsModel *refundItemsModel;



/**
 选择配送方式回调
 */
@property (nonatomic ,copy)void(^changePriceAction)(NSString *price);


@end

NS_ASSUME_NONNULL_END
