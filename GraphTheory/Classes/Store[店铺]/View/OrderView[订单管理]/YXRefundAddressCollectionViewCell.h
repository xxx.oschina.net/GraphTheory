//
//  YXRefundAddressCollectionViewCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/9.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXRefundModel;
@interface YXRefundAddressCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) YXRefundModel *refundModel;

@end

NS_ASSUME_NONNULL_END
