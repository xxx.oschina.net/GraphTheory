//
//  WGOrderHeadCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderHeadCollectionViewCell.h"
#import "UIImage+ColorImage.h"
#import "YXRefundModel.h"
@interface WGOrderHeadCollectionViewCell ()

@property (strong, nonatomic)  UILabel *nameLab;
@property (strong, nonatomic)  UILabel *timeLab;
@property (strong, nonatomic)  UIButton *messegeBtn;
@property (strong, nonatomic)  UIButton *phoneBtn;
@property (strong, nonatomic)  UILabel *groupLab;
@property (strong, nonatomic)  UIImageView *distributionImg;


@end

@implementation WGOrderHeadCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setType:(NSInteger)type{
    _type = type;
    _timeLab.hidden = _type == 1;
}
- (void)setModel:(WGShopModel *)model{
    _model = model;
    _nameLab.text = model.userName;
    _timeLab.text = model.orderTime;
    
    if (_model.orderType == 1) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团中";
    }else if (_model.orderType == 2) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团成功";
    }else if (_model.orderType == 3) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团失败";
    }else {
        _groupLab.hidden = YES;
    }
}
- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    _nameLab.text = _refundModel.userName;
    _timeLab.hidden = YES;
    _distributionImg.hidden = NO;
    if (_refundModel.mainDistributionType == 1) {
        _distributionImg.image = [UIImage imageNamed:@"refund_courier"];
    }else if (_refundModel.mainDistributionType == 2) {
        _distributionImg.image = [UIImage imageNamed:@"refund_shop"];
    }else if (_refundModel.mainDistributionType == 3) {
        _distributionImg.image = [UIImage imageNamed:@"refund_door"];
    }
 
    if (_model.orderType == 1) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团中";
    }else if (_model.orderType == 2) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团成功";
    }else if (_model.orderType == 3) {
        _groupLab.hidden = NO;
        _groupLab.text = @"拼团失败";
    }else {
        _groupLab.hidden = YES;
    }
}

- (void)addLayout{
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.timeLab];
    [self.contentView addSubview:self.messegeBtn];
    [self.contentView addSubview:self.phoneBtn];
    [self.contentView addSubview:self.groupLab];
    [self.contentView addSubview:self.distributionImg];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLab.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
    [self.messegeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLab.mas_centerY);
        make.left.equalTo(self.nameLab.mas_right).offset(3);
        make.size.mas_equalTo(CGSizeMake(20, 30));
    }];
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLab.mas_centerY);
        make.left.equalTo(self.messegeBtn.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(20, 30));
    }];
    [self.groupLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLab.mas_centerY);
        make.left.equalTo(self.phoneBtn.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(50, 20));
    }];
    [self.distributionImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLab.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(14, 14));
    }];

}
- (void)messgeClick{
    
    if (self.clickMsgBlock) {
        self.clickMsgBlock();
    }
}
- (void)phoneClick{
    if (_refundModel.mobile) {
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",_refundModel.mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];

    }else{
        KPOP(@"暂无联系方式");
    }
}

- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.textColor = color_TextOne;
        _nameLab.font = [UIFont systemFontOfSize:14];
    }
    return _nameLab;
}
- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [UILabel new];
        _timeLab.textColor = color_TextTwo;
        _timeLab.font = [UIFont systemFontOfSize:14];
    }
    return _timeLab;
}
- (UIButton *)messegeBtn{
    if (!_messegeBtn) {
        _messegeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_messegeBtn setImage:[UIImage imageNamed:@"order_list_messge"] forState:UIControlStateNormal];
        [_messegeBtn addTarget:self action:@selector(messgeClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _messegeBtn;
}
- (UIButton *)phoneBtn{
    if (!_phoneBtn) {
        _phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_phoneBtn setImage:[UIImage imageNamed:@"order_list_phone"] forState:UIControlStateNormal];
        [_phoneBtn addTarget:self action:@selector(phoneClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _phoneBtn;
}
- (UILabel *)groupLab{
    if (!_groupLab) {
        _groupLab = [UILabel new];
        _groupLab.backgroundColor = APPTintColor;
        _groupLab.textColor = [UIColor whiteColor];
        _groupLab.font = [UIFont systemFontOfSize:10];
        _groupLab.layer.masksToBounds = YES;
        _groupLab.layer.cornerRadius = 2.0f;
        _groupLab.textAlignment = NSTextAlignmentCenter;
        _groupLab.hidden = YES;
    }
    return _groupLab;
}
- (UIImageView *)distributionImg {
    if (!_distributionImg) {
        _distributionImg = [UIImageView new];
        _distributionImg.hidden = YES;
    }
    return _distributionImg;
}
@end
