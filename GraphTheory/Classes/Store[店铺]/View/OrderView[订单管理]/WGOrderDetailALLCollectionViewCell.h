//
//  WGOrderDetailALLCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情总价
 */
@interface WGOrderDetailALLCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) WGShopModel *model;
@end

NS_ASSUME_NONNULL_END
