//
//  WGOrderFootCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单列表底部按钮
 */
@class YXRefundModel,YXRefundActionModel;
@interface WGOrderFootCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGShopModel *model;
// 售后
@property (nonatomic, strong) YXRefundModel *refundModel;

@property (nonatomic ,copy) void(^orderAction)(OrderActionType type);

@property (nonatomic ,copy) void(^refundAction)(YXRefundActionModel *actionModel);

@end

NS_ASSUME_NONNULL_END
