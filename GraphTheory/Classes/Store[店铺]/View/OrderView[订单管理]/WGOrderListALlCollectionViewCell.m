//
//  WGOrderListALlCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/20.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderListALlCollectionViewCell.h"
#import "YXRefundModel.h"
@interface WGOrderListALlCollectionViewCell ()

@property (strong, nonatomic)  UILabel *goodTotal;
@property (strong, nonatomic)  UILabel *typeLab;



@end

@implementation WGOrderListALlCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
        
    }
    return self;
}
- (void)setModel:(WGShopModel *)model{
    _model = model;
    NSMutableAttributedString *temAttString = [NSMutableAttributedString new];
    NSAttributedString *count = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"共%@件 ",model.itemNum] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],NSForegroundColorAttributeName: color_TextOne}];
    [temAttString appendAttributedString:count];
    
    if (_model.exchangeIntegral > 0 && [_model.mainPayPrice floatValue] > 0) {
        NSAttributedString *price = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"积分%ld + ￥%@",model.exchangeIntegral,[NSString formatPriceString:model.mainPayPrice]] attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16.0f],NSForegroundColorAttributeName: color_RedColor}];
        [temAttString appendAttributedString:price];
    }else if (_model.exchangeIntegral > 0) {
        NSAttributedString *price = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"积分%ld",model.exchangeIntegral] attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16.0f],NSForegroundColorAttributeName: color_RedColor}];
        [temAttString appendAttributedString:price];
    }else if ([_model.mainPayPrice floatValue] > 0) {
        NSAttributedString *price = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",[NSString formatPriceString:model.mainPayPrice]] attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16.0f],NSForegroundColorAttributeName: color_RedColor}];
        [temAttString appendAttributedString:price];
    }

    _goodTotal.attributedText = temAttString;
    _typeLab.text = model.mainDistributionName;

}

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    
    NSMutableAttributedString *temAttString = [NSMutableAttributedString new];
//    NSAttributedString *count = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"共%@件 ",_refundModel.itemNum] attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0f],NSForegroundColorAttributeName: color_TextOne}];
//    [temAttString appendAttributedString:count];
    NSAttributedString *price = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",[NSString formatPriceString:_refundModel.mainPayPrice]] attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:16.0f],NSForegroundColorAttributeName: color_RedColor}];
    [temAttString appendAttributedString:price];
    _goodTotal.attributedText = temAttString;
    if (_refundModel.refundState == 0||
        _refundModel.refundState == 3||
        _refundModel.refundState == 4||
        _refundModel.refundState == 5) {
        if (_refundModel.refundType == 0) {
            _typeLab.text = @"仅退款";
        }else {
            if (_refundModel.refundState == 3) {
                _typeLab.text = @"退货退款";
            }else if (_refundModel.refundState == 4) {
                _typeLab.text = @"退货退款(等待买家发货)";
            }else if (_refundModel.refundState == 5) {
                _typeLab.text = @"退货退款(买家已发货)";
            }
        }
    }else if (_refundModel.refundState == 1){
        _typeLab.text = @"退款成功";
    }else if (_refundModel.refundState == 2) {
        _typeLab.text = @"退款关闭";
    }
}

- (void)addLayout{

    [self.contentView addSubview:self.goodTotal];
    [self.goodTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY).offset(3);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
    [self.contentView addSubview:self.typeLab];
    
    [self.typeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
    }];
}
- (UILabel *)typeLab{
    if (!_typeLab) {
        _typeLab = [UILabel new];
        _typeLab.textColor = color_TextTwo;
        _typeLab.font = [UIFont systemFontOfSize:14];
    }
    return _typeLab;
}

- (UILabel *)goodTotal{
    if (!_goodTotal) {
        _goodTotal = [UILabel new];
        _goodTotal.textColor = color_TextOne;
        _goodTotal.font = [UIFont systemFontOfSize:14];
    }
    return _goodTotal;
}

@end
