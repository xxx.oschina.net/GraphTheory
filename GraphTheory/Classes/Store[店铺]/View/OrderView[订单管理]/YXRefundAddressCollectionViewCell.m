//
//  YXRefundAddressCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/9.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXRefundAddressCollectionViewCell.h"
#import "YXRefundModel.h"

@interface YXRefundAddressCollectionViewCell ()

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic ,strong) UIImageView *rightImg;
@end

@implementation YXRefundAddressCollectionViewCell

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    _titleLab.text = [NSString stringWithFormat:@"%@",_refundModel.signStatus];
    _timeLab.text = _refundModel.signTime;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.timeLab];
        [self.contentView addSubview:self.rightImg];
        [self addLayout];
    }
    return self;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"已签收";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0];
        _titleLab.numberOfLines = 2;
    }
    return _titleLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.text = @"2019-12-12";
        _timeLab.textColor = color_TextOne;
        _timeLab.font = [UIFont systemFontOfSize:12.0];
    }
    return _timeLab;
}
- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:@"right_arrow_black"];
    }
    return _rightImg;
}


#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.top.equalTo(@16.0);
        make.right.equalTo(@-15.0);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(5);
        make.right.equalTo(@-15.0);
        make.height.equalTo(@12.0);
        [weakSelf.timeLab sizeToFit];
    }];
    
    [self.rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weakSelf);
        make.right.mas_equalTo(-15);
        make.size.mas_equalTo(CGSizeMake(7, 12));
    }];
}

@end
