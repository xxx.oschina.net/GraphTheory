//
//  WGExpressGoodsCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/19.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressGoodsCollectionViewCell.h"

@interface WGExpressGoodsCollectionViewCell ()


@property(nonatomic, strong) UIImageView *goodImageView;

@end

@implementation WGExpressGoodsCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setModel:(WGOderDetailModel *)model{
    if (model == nil) {
        _goodImageView.image = nil;
        return;
    }
    _model = model;
    [_goodImageView sd_setImageWithURL:[NSURL URLWithString:model.itemPath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}
- (void)setUpLayout{
    [self.contentView addSubview:self.goodImageView];
    [_goodImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}
- (UIImageView *)goodImageView{
    if (!_goodImageView) {
        _goodImageView = [UIImageView new];
        _goodImageView.layer.cornerRadius  = 5;
    }
    return _goodImageView;
}
@end
