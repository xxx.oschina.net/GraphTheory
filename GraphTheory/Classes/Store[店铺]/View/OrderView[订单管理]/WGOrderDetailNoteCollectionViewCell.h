//
//  WGOrderDetailNoteCollectionViewCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/10/16.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGOrderDetailNoteCollectionViewCell : UICollectionViewCell

// 订单备注
@property (nonatomic ,strong) NSString *noteStr;

@end

NS_ASSUME_NONNULL_END
