//
//  WGExpressDetailCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class OrderTrack,YXRefundsModel;
@interface WGExpressDetailCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, assign) BOOL isEnd;

@property (nonatomic, strong) OrderTrack *model;
@property (nonatomic, strong) YXRefundsModel *refundsModel;

@end

NS_ASSUME_NONNULL_END
