//
//  WGOrderDisTypeCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderDisTypeCollectionViewCell.h"
#import "YXRefundModel.h"
@interface WGOrderDisTypeCollectionViewCell ()

@property (strong, nonatomic)  UILabel *typeLab;
@property (strong, nonatomic)  UILabel *goodTotal;
@property (strong, nonatomic)  UIImageView *rightImgView;

@end


@implementation WGOrderDisTypeCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setDetailModel:(WGShopModel *)detailModel{
    _detailModel = detailModel;
    _typeLab.text = detailModel.mainDistributionName;
    if (detailModel.receivedName.length) {
        _goodTotal.text = [NSString stringWithFormat:@"接单人:%@",detailModel.receivedName];
    }else{
        _goodTotal.text = @"";
    }
}

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    _rightImgView.hidden = NO;
    _typeLab.text = @"退款信息";
    if (_refundModel.mainDistributionType == 1) {
        _rightImgView.image = [UIImage imageNamed:@"refund_courier"];
    }else if (_refundModel.mainDistributionType == 2) {
        _rightImgView.image = [UIImage imageNamed:@"refund_shop"];
    }else if (_refundModel.mainDistributionType == 3) {
        _rightImgView.image = [UIImage imageNamed:@"refund_door"];
    }
}

- (void)addLayout{
    [self.contentView addSubview:self.goodTotal];
    [self.goodTotal mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY).offset(3);
        make.right.equalTo(self.mas_right).offset(-15);
    }];

    [self.contentView addSubview:self.typeLab];
    [self.typeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    
    [self.contentView addSubview:self.rightImgView];
    [self.rightImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(14, 14));
    }];
    
}
- (UILabel *)typeLab{
    if (!_typeLab) {
        _typeLab = [UILabel new];
        _typeLab.textColor = color_TextTwo;
        _typeLab.font = [UIFont systemFontOfSize:14];
    }
    return _typeLab;
}
- (UILabel *)goodTotal{
    if (!_goodTotal) {
        _goodTotal = [UILabel new];
        _goodTotal.textColor = color_TextOne;
        _goodTotal.font = [UIFont systemFontOfSize:14];
    }
    return _goodTotal;
}
- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = [UIImageView new];
        _rightImgView.hidden = YES;
    }
    return _rightImgView;
}

@end
