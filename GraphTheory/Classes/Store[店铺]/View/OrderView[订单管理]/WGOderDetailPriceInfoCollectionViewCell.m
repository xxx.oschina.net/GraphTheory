//
//  WGOderDetailPriceInfoCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOderDetailPriceInfoCollectionViewCell.h"

@interface WGOderDetailPriceInfoCollectionViewCell ()


@property (nonatomic ,strong) UILabel *leftLab;

@property (nonatomic ,strong) UILabel *rightLab;

@end

@implementation WGOderDetailPriceInfoCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setModel:(WGOderDetailInfoModel *)model{
    _model = model;
    _leftLab.text = model.keyStr;
    _rightLab.text = model.valueStr;
}
- (void)setRightLabColor:(UIColor *)rightLabColor {
    _rightLabColor = rightLabColor;
    _rightLab.textColor = _rightLabColor;
}

- (void)addLayout{
    [self.contentView addSubview:self.leftLab];
    [self.contentView addSubview:self.rightLab];
    [self.leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    [self.rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
}

- (UILabel *)leftLab {
    if (!_leftLab) {
        _leftLab = [[UILabel alloc] init];
        _leftLab.textColor = color_TextOne;
        _leftLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _leftLab;
}
- (UILabel *)rightLab {
    if (!_rightLab) {
        _rightLab = [[UILabel alloc] init];
        _rightLab.textColor = color_RedColor;
        _rightLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _rightLab;
}
@end
