//
//  WGOrderDetailStatusCollectionReusableView.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderDetailStatusCollectionReusableView.h"
#import "YXRefundModel.h"
@interface WGOrderDetailStatusCollectionReusableView ()

@property (strong, nonatomic)  UILabel *statusLab;
@property (strong, nonatomic)  UIImageView *statusImage;

@end

@implementation WGOrderDetailStatusCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = APPTintColor;
        [self addLayout];
    }
    return self;
}
- (void)setOrderModel:(WGShopModel *)orderModel{
    _orderModel = orderModel;
    switch (_orderModel.orderState) {
        case 0:{
            _statusLab.text = @"待付款";
            _statusImage.image = [UIImage imageNamed:@"order_status_waitPay"];
        }
            break;
        case 1:{
            _statusLab.text = @"待发货";
            _statusImage.image = [UIImage imageNamed:@"order_status_waitSend"];
        }
            break;
        case 2:{
            _statusLab.text = @"待服务";
            _statusImage.image = [UIImage imageNamed:@"order_status_service"];
        }
            break;
        case 3:{
            _statusLab.text = @"已发货";
            _statusImage.image = [UIImage imageNamed:@"order_status_Send"];
        }
            break;
        case 4:{
            _statusLab.text = @"交易成功";
            _statusImage.image = [UIImage imageNamed:@"order_status_done"];
        }
            break;
        case 5:{
            _statusLab.text = @"已关闭";
            _statusImage.image = [UIImage imageNamed:@"order_status_close"];
        }
            break;
        case 6:{
            _statusLab.text = @"售后/退款";
            _statusImage.image = [UIImage imageNamed:@"order_status_close"];
        }
            break;
            
            
        default:
        {
            _statusLab.text = @"已关闭";
            _statusImage.image = [UIImage imageNamed:@"order_status_close"];
        }
            break;
    }

}

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    
    if (_refundModel.refundState == 0 || _refundModel.refundState == 3) {
        _statusLab.text = @"等待商家处理";        
    }else if (_refundModel.refundState == 1){
        _statusLab.text = @"退款成功";
    }else if (_refundModel.refundState == 2) {
        _statusLab.text = @"退款关闭";
    }else if (_refundModel.refundState == 4) {
        _statusLab.text = @"商家已同意 等待买家发货";
    }else if (_refundModel.refundState == 5) {
        _statusLab.text = @"买家已发货 等待商家处理";
    }
    
}


- (void)addLayout{
    [self addSubview:self.statusLab];
    [self addSubview:self.statusImage];
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    [self.statusImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
}
- (UILabel *)statusLab{
    if (!_statusLab) {
        _statusLab = [UILabel new];
        _statusLab.textColor = [UIColor whiteColor];
        _statusLab.font = [UIFont systemFontOfSize:17];
    }
    return _statusLab;
}
- (UIImageView *)statusImage{
    if (!_statusImage) {
        _statusImage = [UIImageView new];
    }
    return _statusImage;
}
@end
