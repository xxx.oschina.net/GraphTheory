//
//  WGOrderListALlCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/20.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单列表-总共件数
 */
@class YXRefundModel;
@interface WGOrderListALlCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGShopModel *model;
// 售后
@property (nonatomic, strong) YXRefundModel *refundModel;
@end

NS_ASSUME_NONNULL_END
