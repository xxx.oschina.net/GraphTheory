//
//  WGOrderDetailNoteCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/10/16.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGOrderDetailNoteCollectionViewCell.h"

@interface WGOrderDetailNoteCollectionViewCell ()

@property (nonatomic ,strong) UILabel *leftLab;

@end

@implementation WGOrderDetailNoteCollectionViewCell

- (void)setNoteStr:(NSString *)noteStr {
    _noteStr = noteStr;
    _leftLab.text = noteStr;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}

- (void)addLayout{
    [self.contentView addSubview:self.leftLab];
    [self.leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@10);
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        [self.leftLab sizeToFit];
    }];

}

- (UILabel *)leftLab {
    if (!_leftLab) {
        _leftLab = [[UILabel alloc] init];
        _leftLab.textColor = color_TextOne;
        _leftLab.font = [UIFont systemFontOfSize:14.0f];
        _leftLab.numberOfLines = 0;
    }
    return _leftLab;
}

@end
