//
//  WGOrderDisTypeCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 配送方式
 */
@class YXRefundModel;
@interface WGOrderDisTypeCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGShopModel *detailModel;

@property (nonatomic, strong) YXRefundModel *refundModel;

@end

NS_ASSUME_NONNULL_END
