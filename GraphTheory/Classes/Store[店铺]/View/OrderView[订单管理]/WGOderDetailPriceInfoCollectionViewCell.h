//
//  WGOderDetailPriceInfoCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情-有关价格展示
 */
@interface WGOderDetailPriceInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGOderDetailInfoModel *model;

@property (nonatomic, strong) UIColor *rightLabColor;
@end

NS_ASSUME_NONNULL_END
