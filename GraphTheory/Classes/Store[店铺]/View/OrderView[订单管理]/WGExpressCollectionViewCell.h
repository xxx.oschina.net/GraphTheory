//
//  WGExpressCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class WGExpressModel;
@interface WGExpressCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) WGExpressModel *orderModel;
@property (nonatomic ,copy)void(^clickSelectCourierNoBtn)(UIButton *btn);
@end

NS_ASSUME_NONNULL_END
