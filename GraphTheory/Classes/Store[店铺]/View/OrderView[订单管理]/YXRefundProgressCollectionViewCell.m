//
//  YXRefundProgressCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/6.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXRefundProgressCollectionViewCell.h"
#import "YXRefundModel.h"
@interface YXRefundProgressCollectionViewCell ()

@property (nonatomic ,strong) UIView *topLineView;
@property (nonatomic ,strong) UIView *pointView;
@property (nonatomic ,strong) UIView *bottomLineView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *timeLab;

@end

@implementation YXRefundProgressCollectionViewCell

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    if (_indexPath.row == 0) {
        _pointView.backgroundColor = APPTintColor;
        _topLineView.hidden = YES;
    }else {
        _pointView.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
        _topLineView.hidden = NO;
    }
}

- (void)setRefundsModel:(YXRefundsModel *)refundsModel {
    _refundsModel = refundsModel;
    _titleLab.text = _refundsModel.refundScheduleText;
    _timeLab.text = _refundsModel.refundScheduleTime;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.pointView];
        [self.contentView addSubview:self.topLineView];
        [self.contentView addSubview:self.bottomLineView];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.timeLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)pointView {
    if (!_pointView) {
        _pointView = [[UIView alloc] init];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
        _pointView.layer.masksToBounds = YES;
        _pointView.layer.cornerRadius = 5;
    }
    return _pointView;
}
- (UIView *)topLineView {
    if (!_topLineView) {
        _topLineView = [[UIView alloc] init];
        _topLineView.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    }
    return _topLineView;
}
- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = [UIColor colorWithHexString:@"#EEEEEE"];
    }
    return _bottomLineView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"等待商家付款";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0];
    }
    return _titleLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.text = @"2019-12-12";
        _timeLab.textColor = color_TextThree;
        _timeLab.font = [UIFont systemFontOfSize:12.0];
    }
    return _timeLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.top.equalTo(@5.0);
        make.size.mas_equalTo(CGSizeMake(10, 10));
    }];
    
    [_topLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.pointView.mas_centerX);
        make.top.equalTo(weakSelf.mas_top);
        make.bottom.equalTo(weakSelf.pointView.mas_top);
        make.width.equalTo(@1.0);
    }];
    
    [_bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.pointView.mas_centerX);
        make.top.equalTo(weakSelf.pointView.mas_bottom);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.width.equalTo(@1.0);
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.pointView.mas_centerY);
        make.left.equalTo(weakSelf.pointView.mas_right).offset(15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.height.equalTo(@14.0);
        [weakSelf.titleLab sizeToFit];
    }];

    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(10);
        make.left.equalTo(weakSelf.pointView.mas_right).offset(15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.height.equalTo(@12.0);
        [weakSelf.timeLab sizeToFit];
    }];
    
}

@end
