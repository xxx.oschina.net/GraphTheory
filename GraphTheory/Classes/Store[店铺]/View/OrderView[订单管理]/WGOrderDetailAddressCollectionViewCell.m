//
//  WGOrderDetailAddressCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderDetailAddressCollectionViewCell.h"

@interface WGOrderDetailAddressCollectionViewCell ()
/** 姓名*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 手机号*/
@property (nonatomic ,strong) UILabel *phoneLab;
/** 详细地址*/
@property (nonatomic ,strong) UILabel *addressDetailsLab;

@end

@implementation WGOrderDetailAddressCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setType:(NSInteger)type{
    _type = type;
}
- (void)setModel:(WGShopModel *)model{
    _model = model;
    _nameLab.text = model.userName;
    _phoneLab.text = model.mobile;
    _addressDetailsLab.text = [NSString stringWithFormat:@"%@",model.address];
}

- (void)addLayout{
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.phoneLab];
    [self.contentView addSubview:self.addressDetailsLab];

    
    YXWeakSelf
    
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nameLab.mas_right).offset(10);
        make.bottom.equalTo(weakSelf.nameLab.mas_bottom).offset(0);
        [weakSelf.phoneLab sizeToFit];
    }];
    [self.addressDetailsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.nameLab.mas_left);
        make.top.equalTo(weakSelf.nameLab.mas_bottom).offset(5);
        make.right.equalTo(weakSelf.mas_right).offset(-65);
        [weakSelf.addressDetailsLab sizeToFit];
    }];
    
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.textColor = color_TextOne;
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
        _nameLab.text = @"张三";
    }
    return _nameLab;
}
- (UILabel *)phoneLab {
    if (!_phoneLab) {
        _phoneLab = [[UILabel alloc] init];
        _phoneLab.textColor = color_TextTwo;
        _phoneLab.font = [UIFont systemFontOfSize:14.0f];
        _phoneLab.text = @"188888888888";
    }
    return _phoneLab;
}

- (UILabel *)addressDetailsLab {
    if (!_addressDetailsLab) {
        _addressDetailsLab = [[UILabel alloc] init];
        _addressDetailsLab.textColor = color_TextOne;
        _addressDetailsLab.font = [UIFont systemFontOfSize:14.0f];
        _addressDetailsLab.text = @"郑州市郑东新区";
        _addressDetailsLab.numberOfLines = 0;
        _addressDetailsLab.userInteractionEnabled = YES;
        [_addressDetailsLab addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pasteBoard:)]];
    }
    return _addressDetailsLab;
}

- (void)pasteBoard:(UILongPressGestureRecognizer*)longPress {

    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@",_model.address];
        [YJProgressHUD showSuccess:@"复制成功"];
    }

}


@end
