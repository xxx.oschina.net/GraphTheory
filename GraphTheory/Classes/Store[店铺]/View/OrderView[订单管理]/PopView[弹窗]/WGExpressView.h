//
//  WGExpressView.h
//  DDLife
//
//  Created by wanggang on 2019/10/24.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "DDBaseView.h"

NS_ASSUME_NONNULL_BEGIN

/**
 物流信息弹窗
 */
@interface WGExpressView : DDBaseView

@property (nonatomic, strong) WGShopModel *model;
@property (strong, nonatomic) UIViewController *vc;


@end

NS_ASSUME_NONNULL_END
