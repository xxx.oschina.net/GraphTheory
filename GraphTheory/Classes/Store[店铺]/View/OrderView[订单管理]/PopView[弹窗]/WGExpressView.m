//
//  WGExpressView.m
//  DDLife
//
//  Created by wanggang on 2019/10/24.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "WGExpressView.h"
#import "UIViewController+KNSemiModal.h"


@interface WGExpressView ()

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic ,strong) UIView *line;
@property (nonatomic ,strong) UILabel *kdNameLab;
@property (nonatomic ,strong) UIView *kddhView;
@property (nonatomic ,strong) UILabel *kddhLab;
@property (nonatomic ,strong) UIButton *dhBtn;

@end

@implementation WGExpressView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        [self setUpLayout];
    }
    return self;
}
- (void)setModel:(WGShopModel *)model{
    _model = model;
    _kdNameLab.text = model.deliveryTypeName;;
    _kddhLab.text = model.expressNo;
    [self layoutIfNeeded];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.backView.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)].CGPath;
    self.backView.layer.mask = shapeLayer;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.closeBtn];
    [self.backView addSubview:self.line];
    [self.backView addSubview:self.kdNameLab];
    [self.backView addSubview:self.kddhView];
    [self.kddhView addSubview:self.kddhLab];
    [self.kddhView addSubview:self.dhBtn];

    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.right.equalTo(self.backView);
        make.height.mas_equalTo(44);
    }];
    [self.line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView);
        make.top.equalTo(self.titleLab.mas_bottom);
        make.height.mas_equalTo(1);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.top.equalTo(self.backView);
        make.width.equalTo(self.closeBtn.mas_height);
        make.bottom.equalTo(self.titleLab.mas_bottom);
    }];
    [self.kdNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.line.mas_bottom).offset(15);
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.kddhView.mas_top).offset(-15);
    }];
    [self.kddhView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.kdNameLab.mas_bottom).offset(15);
        make.centerX.equalTo(self.backView.mas_centerX);
    }];
    [self.kddhLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.and.bottom.equalTo(self.kddhView);
    }];
    [self.dhBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.kddhLab.mas_right);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        make.right.equalTo(self.kddhView.mas_right);
        make.bottom.equalTo(self.kddhLab);
    }];
}
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}
- (UIView *)kddhView {
    if (!_kddhView) {
        _kddhView = [UIView new];
        _kddhView.backgroundColor = [UIColor whiteColor];
    }
    return _kddhView;
}
- (UIView *)line {
    if (!_line) {
        _line = [UIView new];
        _line.backgroundColor = HEX_COLOR(@"#F4F4F4");
    }
    return _line;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.text = @"快递单号";
        _titleLab.textColor = color_TextOne;
        _titleLab.backgroundColor = HEX_COLOR(@"ffffff");
        _titleLab.font = [UIFont systemFontOfSize:18];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}
- (UILabel *)kdNameLab {
    if (!_kdNameLab) {
        _kdNameLab = [UILabel new];
        _kdNameLab.text = @"";
        _kdNameLab.textColor = color_TextOne;
        _kdNameLab.backgroundColor = HEX_COLOR(@"ffffff");
        _kdNameLab.font = [UIFont systemFontOfSize:14];
        _kdNameLab.textAlignment = NSTextAlignmentCenter;
    }
    return _kdNameLab;
}
- (UILabel *)kddhLab {
    if (!_kddhLab) {
        _kddhLab = [UILabel new];
        _kddhLab.text = @"";
        _kddhLab.textColor = color_TextOne;
        _kddhLab.backgroundColor = HEX_COLOR(@"ffffff");
        _kddhLab.font = [UIFont systemFontOfSize:14];
        _kddhLab.textAlignment = NSTextAlignmentCenter;
    }
    return _kddhLab;
}
- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];;
        [_closeBtn setImage:[UIImage imageNamed:@"order_peisong_close"] forState:(UIControlStateNormal)];
        [_closeBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
        [_closeBtn addTarget:self action:@selector(closeBtnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}
- (UIButton *)dhBtn {
    if (!_dhBtn) {
        _dhBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];;
        [_dhBtn setImage:[UIImage imageNamed:@"cos_copy_dh"] forState:(UIControlStateNormal)];
        [_dhBtn setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:(UIControlStateNormal)];
        [_dhBtn addTarget:self action:@selector(copyClicked:) forControlEvents:(UIControlEventTouchUpInside)];
        _dhBtn.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;
    }
    return _dhBtn;
}
- (void)copyClicked:(UIButton *)sender{
    UIPasteboard*pasteboard = [UIPasteboard generalPasteboard];
    
    pasteboard.string= _model.expressNo;
    KPOP(@"复制成功");

}
- (void)closeBtnClicked:(UIButton *)sender {
    [self disMiss];
}
-(void)disMiss{
    if ([_vc respondsToSelector:@selector(dismissSemiModalView)]) {
        [_vc dismissSemiModalView];
    }
    //    [UIView animateWithDuration:0.3 animations:^{
    //        self.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
    //    }completion:^(BOOL finished) {
    //        [self removeFromSuperview];
    //    }];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
