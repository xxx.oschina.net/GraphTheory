//
//  WGOrderActionView.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseView.h"
#import "WGPopUpTool.h"
#import "WGDeliveryModel.h"
#import "WGShopRecivedModel.h"

NS_ASSUME_NONNULL_BEGIN

/**
 发货页面
 */
@interface WGOderDeliver : DDBaseView
@property (nonatomic ,strong) WGDeliveryModel *model;
@property (nonatomic ,strong) NSString  *expressType;
@property (nonatomic ,strong) NSString  *expressNo;
@property (nonatomic ,copy) void(^deliverAction)(WGDeliveryModel *model,NSString  *expressNo);
@end
/**
 备注页面
 */
@interface WGOderNote : DDBaseView

@property (nonatomic ,strong) NSString  *note;
@property (nonatomic ,copy) void(^noteAction)(NSString  *note);


@end
/**
 选择接单人
 */
@interface WGOderSelctClerk : DDBaseView

@property (nonatomic ,copy) void(^receviedAction)(WGShopRecivedModel *receivedMode);

@end
/**
 确认消费码
 */
@interface WGOderVerifyPayNO : DDBaseView
@property (nonatomic ,copy) void(^verifyPayNo)(NSString  *payNo, NSString *orderNo);

@end


/**
 订单页操作
 */
@interface WGOrderActionView : DDBaseView

@property (nonatomic,strong) NSString  *note;
@property (nonatomic,assign) OrderActionType actionType;
@property (nonatomic,strong) UIView *subView;

@property (nonatomic ,copy) void(^deliverAction)(WGDeliveryModel *model,NSString  *expressNo);
@property (nonatomic ,copy) void(^noteAction)(NSString  *note);
@property (nonatomic ,copy) void(^receviedAction)(WGShopRecivedModel *receivedMode);
@property (nonatomic ,copy) void(^verifyPayNo)(NSString  *payNo, NSString *orderNo);





- (void)show;
- (void)confirmBtnClick;//确认按钮

@end

NS_ASSUME_NONNULL_END
