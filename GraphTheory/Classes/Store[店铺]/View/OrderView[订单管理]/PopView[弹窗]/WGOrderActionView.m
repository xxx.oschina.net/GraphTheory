//
//  WGOrderActionView.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderActionView.h"
#import "DDScanViewController.h"
#import "DDBaseNavigationController.h"
#import "LTLimitTextField.h"
#import "WGStoreSelctTimeWeekCollectionViewCell.h"
#import "YCMenuView.h"
#import "WGDeliveryModel.h"
#import "WGOrderViewModel.h"
#import <IQTextView.h>


@interface WGOderDeliver ()
@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIView *expTypeView;
@property (nonatomic ,strong) LTLimitTextField *expTypeFielf;
@property (nonatomic ,strong) UIButton *expTypeBtn;
@property (nonatomic ,strong) UIView *expNOView;
@property (nonatomic ,strong) LTLimitTextField *expNOFielf;
@property (nonatomic ,strong) UIButton *scanBtn;

@end

@implementation WGOderDeliver
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)scanNO{
    DDWeakSelf
    DDScanViewController *scan = [DDScanViewController new];
    scan.type = DDSCANTYPEExpressNO;

    scan.scanSuccessReslut = ^(NSString * _Nonnull reslut) {
        NSLog(@"----%@",reslut)
        weakSelf.expNOFielf.text = reslut;
        weakSelf.expressNo = reslut;
        if (weakSelf.deliverAction) {
            weakSelf.deliverAction(weakSelf.model, reslut);
        }
    };
    DDBaseNavigationController *nav = [[DDBaseNavigationController alloc]initWithRootViewController:scan];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;

    [[UIView currentViewController] presentViewController:nav animated:YES completion:nil];
}
- (void)selecType{
    DDWeakSelf
    if ([YXUserInfoManager shareUserInfoManager].allDelivery.count) {
        NSMutableArray *temSort = [NSMutableArray new];
        for (int i = 0; i < [YXUserInfoManager shareUserInfoManager].allDelivery.count; i ++) {
            WGDeliveryModel *de = [YXUserInfoManager shareUserInfoManager].allDelivery[i];
            YCMenuAction *action = [YCMenuAction actionWithTitle:de.deliveryTypeName image:nil handler:^(YCMenuAction *action) {
                if ([action.title isEqualToString:@"自定义快递公司"]) {
                    weakSelf.expTypeFielf.enabled = YES;
                    weakSelf.expTypeFielf.text = nil;
                    weakSelf.expTypeFielf.placeholder = @"请输入快递公司";
                }else {
                    weakSelf.expTypeFielf.enabled = NO;
                    weakSelf.expTypeFielf.text = action.title;
                }
                weakSelf.model = [YXUserInfoManager shareUserInfoManager].allDelivery[i];
                if (weakSelf.deliverAction) {
                    weakSelf.deliverAction(weakSelf.model, weakSelf.expressNo);
                }
                
            }];
            [temSort addObject:action];
        }
        YCMenuView *view = [YCMenuView menuWithActions:temSort width:_expTypeView.width relyonView:_expTypeView];
        [view show];
    }else{
        [YJProgressHUD showLoading:@""];
        [WGOrderViewModel queryAllDeliveryCompletion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            NSMutableArray *tem = [NSMutableArray array];
            [tem removeAllObjects];
            WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
            if (model.state) {
                WGDeliveryModel *deModel = [WGDeliveryModel new];
                [deModel setDeliveryTypeName:@"自定义快递公司"];
                tem = [WGDeliveryModel mj_objectArrayWithKeyValuesArray:model.data];
                [tem addObject:deModel];
                [YXUserInfoManager shareUserInfoManager].allDelivery = tem;
                NSMutableArray *temSort = [NSMutableArray new];
                for (int i = 0; i < [YXUserInfoManager shareUserInfoManager].allDelivery.count; i ++) {
                    WGDeliveryModel *de = [YXUserInfoManager shareUserInfoManager].allDelivery[i];
                    YCMenuAction *action = [YCMenuAction actionWithTitle:de.deliveryTypeName image:nil handler:^(YCMenuAction *action) {
                        if ([action.title isEqualToString:@"自定义快递公司"]) {
                            weakSelf.expTypeFielf.enabled = YES;
                            weakSelf.expTypeFielf.text = nil;
                            weakSelf.expTypeFielf.placeholder = @"请输入快递公司";
                        }else {
                            weakSelf.expTypeFielf.enabled = NO;
                            weakSelf.expTypeFielf.text = action.title;
                        }
                        weakSelf.model = [YXUserInfoManager shareUserInfoManager].allDelivery[i];
                        if (weakSelf.deliverAction) {
                            weakSelf.deliverAction(weakSelf.model, weakSelf.expressNo);
                        }
                        
                    }];
                    [temSort addObject:action];
                }
                YCMenuView *view = [YCMenuView menuWithActions:temSort width:weakSelf.expTypeFielf.width relyonView:weakSelf.expTypeView];
                [view show];
            }else{
                [YJProgressHUD showError:REQUESTERR];
            }
            
        } failure:^(NSError * _Nonnull error) {
            [YJProgressHUD hideHUD];
            [YJProgressHUD showError:REQUESTERR];
        }];
        
    }
}
- (void)textFieldChanged:(UITextField *)text{
    if (text == self.expTypeFielf) {
        _expressType = text.text;
        _model = [WGDeliveryModel new];
        _model.deliveryTypeName = @"自定义快递公司";
        _model.customTypeName = _expressType;
    }else if (text == self.expNOFielf) {
        _expressNo = text.text;
    }
    
    if (self.deliverAction) {
        self.deliverAction(_model, _expressNo);
    }
}
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.expTypeView];
    [self.expTypeView addSubview:self.expTypeFielf];
    [self.expTypeView addSubview:self.expTypeBtn];
    [self.backView addSubview:self.expNOView];
    [self.expNOView addSubview:self.expNOFielf];
    [self.expNOView addSubview:self.scanBtn];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(21, 25, 21, 25));
    }];
    [self.expTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.backView);
        make.height.mas_equalTo(32);
    }];
    [self.expTypeFielf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(self.expTypeView);
        make.left.equalTo(self.expTypeView).offset(5);
        make.right.equalTo(self.expTypeBtn.mas_left).offset(-5);
    }];
    [self.expTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32, 32));
        make.right.equalTo(self.expTypeView);
        make.centerY.equalTo(self.expTypeView);
    }];
    [self.expNOView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.expTypeView.mas_bottom).offset(10);
        make.left.right.bottom.equalTo(self.backView);
        make.height.mas_equalTo(32);
    }];
    [self.expNOFielf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(self.expNOView);
        make.left.equalTo(self.expNOView).offset(5);
        make.right.equalTo(self.scanBtn.mas_left).offset(-5);
    }];
    [self.scanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32, 32));
        make.right.equalTo(self.expNOView);
        make.centerY.equalTo(self.expNOView);
    }];
    
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}
- (UIView *)expTypeView{
    if (!_expTypeView) {
        _expTypeView = [UIView new];
        _expTypeView.backgroundColor = [UIColor whiteColor];
        _expTypeView.layer.cornerRadius = 4;
        _expTypeView.layer.borderWidth = 1;
        _expTypeView.layer.borderColor = color_TextFour.CGColor;
    }
    return _expTypeView;
}
- (LTLimitTextField *)expTypeFielf{
    if (!_expTypeFielf) {
        _expTypeFielf = [LTLimitTextField new];
        _expTypeFielf.textColor = color_TextTwo;
        _expTypeFielf.font = [UIFont systemFontOfSize:14];
        _expTypeFielf.enabled = NO;
        _expTypeFielf.placeholder = @"快递类型";
        [_expTypeFielf addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _expTypeFielf;
}
- (UIButton *)expTypeBtn{
    if (!_expTypeBtn) {
        _expTypeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_expTypeBtn setImage:[UIImage imageNamed:@"order_list_action_down"] forState:UIControlStateNormal];
        [_expTypeBtn addTarget:self action:@selector(selecType) forControlEvents:UIControlEventTouchUpInside];
    }
    return _expTypeBtn;
}
- (UIView *)expNOView{
    if (!_expNOView) {
        _expNOView = [UIView new];
        _expNOView.backgroundColor = [UIColor whiteColor];
        _expNOView.layer.cornerRadius = 4;
        _expNOView.layer.borderWidth = 1;
        _expNOView.layer.borderColor = color_TextFour.CGColor;
    }
    return _expNOView;
}
- (LTLimitTextField *)expNOFielf{
    if (!_expNOFielf) {
        _expNOFielf = [LTLimitTextField new];
        _expNOFielf.textColor = color_TextTwo;
        _expNOFielf.font = [UIFont systemFontOfSize:14];
        _expNOFielf.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
        _expNOFielf.keyboardType = UIKeyboardTypeDefault;
        _expNOFielf.placeholder = @"快递单号";
        [_expNOFielf addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

    }
    return _expNOFielf;
}
- (UIButton *)scanBtn{
    if (!_scanBtn) {
        _scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_scanBtn setImage:[UIImage imageNamed:@"order_list_action_scan"] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scanNO) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scanBtn;
}

@end


@interface WGOderNote ()<UITextViewDelegate>

@property (nonatomic ,strong) UIView *backView;

@property (nonatomic ,strong) IQTextView *textView;

@end

@implementation WGOderNote

- (void)setNote:(NSString *)note {
    _note = note;
    _textView.text = _note;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}

- (void)setUpLayout {
    
    [self addSubview:self.backView];
    [self.backView addSubview:self.textView];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(21, 30, 21, 30));
        make.height.mas_equalTo(50);
    }];
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];

}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 4;
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = color_TextFour.CGColor;
    }
    return _backView;
}

- (IQTextView *)textView {
    if (!_textView) {
        _textView = [IQTextView new];
        _textView.textColor = color_TextTwo;
        _textView.font = [UIFont systemFontOfSize:14];
        _textView.placeholder = @"请输入内容";
        _textView.delegate = self;
    }
    return _textView;
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.noteAction) {
        self.noteAction(textView.text);
    }
}


@end

@interface WGOderSelctClerk ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) NSIndexPath *selectIndex;


@end

@implementation WGOderSelctClerk
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self);
            make.height.mas_equalTo(50);
        }];
        [self request];
    }
    return self;
}
- (void)request{
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel  queryShopReceivedCompletion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            weakSelf.titleArr = [WGShopRecivedModel mj_objectArrayWithKeyValuesArray:model.data];
            NSInteger lineNum = ceil(weakSelf.titleArr.count / 2.0);
            
            CGFloat h = 21 * 2 + 32 * lineNum + (lineNum - 1) * 10;
            [weakSelf.collectionView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(h);
            }];
            [weakSelf.collectionView reloadData];
        }else{
            [YJProgressHUD showError:REQUESTERR];
        }

    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];

    }];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return _titleArr.count;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(125, 32);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        CGFloat left = (self.frame.size.width - 125 * 2 - 10) / 2;
        return UIEdgeInsetsMake(21, left, 21, left);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf;
    if (indexPath.section == 0) {
        
        WGStoreSelctTimeWeekCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell" forIndexPath:indexPath];
        WGShopRecivedModel *model = _titleArr[indexPath.row];
        cell.titleStr = model.locatName;
        cell.isSelect = _selectIndex == indexPath;
        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    _selectIndex = indexPath;
    if (self.receviedAction) {
        self.receviedAction(_titleArr[indexPath.row]);
    }
    [self.collectionView reloadData];
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[WGStoreSelctTimeWeekCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell"];
    }
    return _collectionView;
}
@end

@interface WGOderVerifyPayNO ()

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) LTLimitTextField *xfmFielf;
@property (nonatomic ,strong) UIButton *scanBtn;

@end

@implementation WGOderVerifyPayNO

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
        
    }
    return self;
}
- (void)textFieldChanged:(UITextField *)text{
    if (self.verifyPayNo) {
        self.verifyPayNo(text.text,nil);
    }
}
- (void)scanNO{
    DDWeakSelf
    DDScanViewController *scan = [DDScanViewController new];
    scan.type = DDSCANTYPECoustomNo;
        scan.scanCoustomSuccessReslut = ^(NSString * _Nonnull coustomNo, NSString * _Nonnull orderNO) {
        weakSelf.xfmFielf.text = coustomNo;
        if (weakSelf.verifyPayNo) {
            weakSelf.verifyPayNo(coustomNo,orderNO);
        }

    };
    DDBaseNavigationController *nav = [[DDBaseNavigationController alloc]initWithRootViewController:scan];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [[UIView currentViewController] presentViewController:nav animated:YES completion:nil];
}

- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.xfmFielf];
    [self.backView addSubview:self.scanBtn];

    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(21, 30, 21, 30));
        make.height.mas_equalTo(32);
    }];
    [self.xfmFielf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.equalTo(self.backView);
        make.left.equalTo(self.backView).offset(5);
        make.right.equalTo(self.scanBtn.mas_left).offset(-5);
    }];
    [self.scanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32, 32));
        make.right.equalTo(self.backView);
        make.centerY.equalTo(self.backView);
    }];
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 4;
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = color_TextFour.CGColor;
    }
    return _backView;
}
- (LTLimitTextField *)xfmFielf{
    if (!_xfmFielf) {
        _xfmFielf = [LTLimitTextField new];
        _xfmFielf.textColor = color_TextTwo;
        _xfmFielf.font = [UIFont systemFontOfSize:14];
        _xfmFielf.placeholder = @"请输入8位消费码";
        _xfmFielf.textLimitInputType = LTTextLimitInputTypeNumber;
        _xfmFielf.textLimitType = LTTextLimitTypeLength;
        _xfmFielf.textLimitSize = 8;
        _xfmFielf.keyboardType = UIKeyboardTypeNumberPad;
        [_xfmFielf addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

    }
    return _xfmFielf;
}
- (UIButton *)scanBtn{
    if (!_scanBtn) {
        _scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_scanBtn setImage:[UIImage imageNamed:@"order_list_action_scan"] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scanNO) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scanBtn;
}

@end

@interface WGOrderActionView ()

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIView *topView;
@property (nonatomic ,strong) UIView *contentView;
@property (nonatomic ,strong) UIView *lineView;

@property (nonatomic ,strong) UIButton *sureBtn;
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic ,strong) UILabel *titleLab;


@property (nonatomic ,strong) WGDeliveryModel *model;
@property (nonatomic ,strong) NSString  *expressNo;
@property (nonatomic ,strong) WGShopRecivedModel *receivedMode;
@property (nonatomic ,strong) NSString  *payNo;
@property (nonatomic ,strong) NSString  *orderNo;

@end

@implementation WGOrderActionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
        
    }
    return self;
}
- (void)show{
    [[WGPopUpTool sharedInstance] popUpWithView:self subView:_subView withAnimation:YES withCanTapBgCancel:YES];;
}
- (void)closeView{
    [[WGPopUpTool sharedInstance] removePopUpView];
}
- (void)confirmBtnClick{
    switch (_actionType) {
        case OrderActionTypeDeliver :{
            if (_model.deliveryTypeName.length == 0) {
                KPOP(@"请选择或输入快递类型");
            }else if ([_model.deliveryTypeName isEqualToString:@"自定义快递公司"] && [NSString isBlankString:_model.customTypeName]) {
               KPOP(@"请输入快递公司");
            }else if (_expressNo.length == 0){
                KPOP(@"请输入或扫描快递单号");
            }else if (self.deliverAction){
                self.deliverAction(_model, _expressNo);
                [[WGPopUpTool sharedInstance] removePopUpView];
            }
        }
            break;
        case OrderActionTypeNote:{
            if (_note.length == 0) {
                KPOP(@"请输入备注内容");
            }else {
                self.noteAction(_note);
                [[WGPopUpTool sharedInstance] removePopUpView];
            }
        }
            break;
        case OrderActionTypeSelctClerk:
        {
            if (!_receivedMode.Id) {
                KPOP(@"请选择接单人");
            }else if (self.receviedAction){
                self.receviedAction(_receivedMode);
                [[WGPopUpTool sharedInstance] removePopUpView];
            }
        }
            break;
        case OrderActionTypeVerifyPayNO:{
            if (_payNo.length == 0) {
                KPOP(@"请输入消费码");
            }else if (self.verifyPayNo){
                self.verifyPayNo(_payNo,_orderNo);
                [[WGPopUpTool sharedInstance] removePopUpView];

            }

        }
            break;
            
        case OrderActionTypeContinueDeliver :{
            if (_model.deliveryTypeName.length == 0) {
                KPOP(@"请选择或输入快递类型");
            }else if ([_model.deliveryTypeName isEqualToString:@"自定义快递公司"] && [NSString isBlankString:_model.customTypeName]) {
                KPOP(@"请输入快递公司");
            }else if (_expressNo.length == 0){
                KPOP(@"请输入或扫描快递单号");
            }else if (self.deliverAction){
                self.deliverAction(_model, _expressNo);
                [[WGPopUpTool sharedInstance] removePopUpView];
            }
        }
            break;
        default:
            break;
    }
    
}

- (void)setNote:(NSString *)note {
    _note = note;
}
- (void)setActionType:(OrderActionType)actionType{
    _actionType = actionType;
    DDWeakSelf
    switch (_actionType) {
        case OrderActionTypeDeliver:{
            _titleLab.text = @"输入快递单号";
            WGOderDeliver *deliver = [WGOderDeliver new];
            deliver.deliverAction = ^(WGDeliveryModel * _Nonnull model, NSString * _Nonnull expressNo) {
                weakSelf.model = model;
                weakSelf.expressNo = expressNo;
            };
            [_contentView addSubview:deliver];
            [deliver mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.contentView);
            }];
        }
            break;
        case OrderActionTypeNote:{
            _titleLab.text = @"添加备注";
            WGOderNote *note = [WGOderNote new];
            note.note = self.note;
            note.noteAction = ^(NSString * _Nonnull note) {
                weakSelf.note = note;
            };
            [_contentView addSubview:note];
            [note mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.contentView);
            }];
        }
            break;
        case OrderActionTypeSelctClerk:
        {
            _titleLab.text = @"选择接单人";
            WGOderSelctClerk *selct = [WGOderSelctClerk new];
            selct.receviedAction = ^(WGShopRecivedModel * _Nonnull receivedMode) {
                weakSelf.receivedMode = receivedMode;
            };
            [_contentView addSubview:selct];
            [selct mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.contentView);
            }];
            
        }
            break;
        case OrderActionTypeVerifyPayNO:{
            _titleLab.text = @"确认消费码";
            WGOderVerifyPayNO *ver = [WGOderVerifyPayNO new];
            ver.verifyPayNo = ^(NSString * _Nonnull payNo, NSString * _Nonnull orderNo) {
                weakSelf.payNo = payNo;
                weakSelf.orderNo = orderNo;
                if (orderNo.length) {
                    [weakSelf confirmBtnClick];
                }
            };
            [_contentView addSubview:ver];
            [ver mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.contentView);
            }];
        }
            break;
        case OrderActionTypeContinueDeliver:{
            _titleLab.text = @"输入快递单号";
            WGOderDeliver *deliver = [WGOderDeliver new];
            deliver.deliverAction = ^(WGDeliveryModel * _Nonnull model, NSString * _Nonnull expressNo) {
                weakSelf.model = model;
                weakSelf.expressNo = expressNo;
            };
            [_contentView addSubview:deliver];
            [deliver mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.contentView);
            }];
        }
            break;
                    
        default:
            break;
    }
}

- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.topView];
    [self.topView addSubview:self.titleLab];
    [self.topView addSubview:self.closeBtn];
    [self.backView addSubview:self.contentView];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.sureBtn];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.backView);
        make.height.mas_equalTo(44);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.topView.mas_centerX);
        make.centerY.equalTo(self.topView.mas_centerY);
    }];
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.titleLab.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.right.equalTo(self.topView.mas_right);
    }];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.backView);
        make.top.equalTo(self.topView.mas_bottom);
        make.bottom.equalTo(self.lineView.mas_top);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.backView);
        make.bottom.equalTo(self.sureBtn.mas_top);
        make.height.mas_equalTo(1);
    }];
    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.backView);
        make.height.mas_equalTo(44);
    }];
    
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.cornerRadius = 4;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)topView{
    if (!_topView) {
        _topView = [UIView new];
        _topView.backgroundColor = color_BackColor;
        _topView.clipsToBounds = YES;
        
    }
    return _topView;
}
- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = color_BackColor;
    }
    return _contentView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_BackColor;
    }
    return _lineView;
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:16];
    }
    return _titleLab;
}
- (UIButton *)closeBtn{
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"order_list_action_close"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeBtn;
}
- (UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setTitle:@"确认" forState:UIControlStateNormal];
        [_sureBtn setTitleColor:color_TextTwo forState:UIControlStateNormal];
        _sureBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_sureBtn addTarget:self action:@selector(confirmBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sureBtn;
    
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
