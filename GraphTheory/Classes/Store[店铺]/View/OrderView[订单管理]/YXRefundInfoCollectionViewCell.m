//
//  YXRefundInfoCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/6.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXRefundInfoCollectionViewCell.h"
#import "YXRefundModel.h"
#import "YXPreviewManage.h"
@interface YXRefundInfoCollectionViewCell ()

@property (nonatomic ,strong) UILabel *label;

/**
 退款凭证图片数组
 */
@property (nonatomic ,strong) NSMutableArray *imgArr;

@end

@implementation YXRefundInfoCollectionViewCell

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    [self setUpListView];
}
- (void)setUpListView {
    
    for (int i = 0; i < 5; i ++) {
        _label = [[UILabel alloc] initWithFrame:(CGRectMake(15, 30 *i + 10, KWIDTH - 30, 30))];
        _label.textColor = color_TextThree;
        _label.font = [UIFont systemFontOfSize:12.0];
        if (i == 0) {
            _label.text = [NSString stringWithFormat:@"退款编号：%@",_refundModel.refundNo];
        }else if (i == 1) {
            _label.text = [NSString stringWithFormat:@"申请时间：%@",_refundModel.createTime];
        }else if (i == 2) {
            _label.text = [NSString stringWithFormat:@"退款类型：%@",_refundModel.refundType == 0?@"仅退款":@"退货退款"];
        }else if (i == 3) {
            _label.text = [NSString stringWithFormat:@"退款原因：%@",_refundModel.refundRemark];
        }else if (i == 4) {
            _label.text = [NSString stringWithFormat:@"退款说明：%@",_refundModel.refundInstructions];
        }
        [self.contentView addSubview:self.label];
    }
    
    if (_refundModel.refundCredentialsInfo.length > 0) {
        NSArray *jsonArr = [_refundModel.refundCredentialsInfo mj_JSONObject];
        NSArray *dataArr = [YXRefundCredentialsModel mj_objectArrayWithKeyValuesArray:jsonArr];
        if ([dataArr count] > 0) {
            _imgArr = [NSMutableArray array];
            for (YXRefundCredentialsModel *model in dataArr) {
                if (model.credentialsPath.length > 0) {
                    [_imgArr addObject:model.credentialsPath];
                }
            }
            if ([_imgArr count] > 0) {
                for (int i = 0; i < _imgArr.count; i ++ ) {
                    UIButton *imgBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
                    imgBtn.frame = CGRectMake(15+ (i*(44 + 10)), 170, 44, 44);
                    imgBtn.backgroundColor = [UIColor whiteColor];
                    NSString *url = [_imgArr objectAtIndex:i];
                    [imgBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:url] forState:(UIControlStateNormal)];
                    imgBtn.tag = i;
                    [imgBtn addTarget:self action:@selector(imgBtnAciton:) forControlEvents:(UIControlEventTouchUpInside)];
                    [self.contentView addSubview:imgBtn];
                    
                }
            }
        }
    }
}

- (void)imgBtnAciton:(UIButton *)sender {
    if ([_imgArr count] > 0) {
        [[YXPreviewManage sharePreviewManage] showPhotoWithImgArr:_imgArr currentIndex:sender.tag];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}



@end
