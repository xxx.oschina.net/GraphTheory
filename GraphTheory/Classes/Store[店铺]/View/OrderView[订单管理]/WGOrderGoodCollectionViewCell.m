//
//  WGOrderGoodCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderGoodCollectionViewCell.h"
#import "LBXScanNative.h"
#import "LTLimitTextField.h"
#import "YXRefundModel.h"
@interface WGOrderGoodCollectionViewCell ()

@property (strong, nonatomic)  UILabel *goodName;
@property (strong, nonatomic)  UILabel *goodPrice;
@property (strong, nonatomic)  UILabel *goodCount;
@property (strong, nonatomic)  UILabel *goodSku;
@property (strong, nonatomic)  UILabel *goodTotal;
@property (strong, nonatomic)  UILabel *msgLab;
@property (strong, nonatomic)  UILabel *statusLab;


@property (strong, nonatomic)  UIImageView *goodImage;
@property (strong, nonatomic)  UIView *goodView;
@property (strong, nonatomic)  UIButton *changePrice;

@property (strong, nonatomic)  UIView *changeView;
@property (strong, nonatomic)  UIButton *saveBtn;
@property (strong, nonatomic)  UIButton *cancleBtn;
@property (strong, nonatomic)  LTLimitTextField *changePriceField;




@property (assign, nonatomic)  BOOL isLoad;


@end

@implementation WGOrderGoodCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        _isLoad = NO;
    }
    return self;
}
- (void)setOrderModel:(WGShopModel *)orderModel{
    _orderModel = orderModel;
    switch (_orderModel.orderState) {
        case 0:{
            _statusLab.text = @"待付款";
        }
            break;
        case 1:{
            _statusLab.text = @"待发货";
        }
            break;
        case 2:{
            _statusLab.text = @"待服务";
        }
            break;
        case 3:{
            _statusLab.text = @"已发货";
        }
            break;
        case 4:{
            _statusLab.text = @"交易成功";
        }
            break;
        case 5:{
            _statusLab.text = @"已关闭";
        }
            break;
        case 6:{
            _statusLab.text = @"售后/退款";
        }
            break;
            
            
        default:
        {
            _statusLab.text = @"已关闭";
        }
            break;
    }
    
}
- (void)setDetailModel:(WGOderDetailModel *)detailModel{
    _detailModel = detailModel;
    _goodName.text = detailModel.itemInfo;
    _goodPrice.text = [NSString stringWithFormat:@"￥%@",[NSString formatPriceString:detailModel.itemPrice]];
    _changePriceField.text = detailModel.itemPrice;
    _goodSku.text = detailModel.itemType;
    _goodCount.text = [NSString stringWithFormat:@"X%@",detailModel.itemNum];
    [_goodImage sd_setImageWithURL:[NSURL URLWithString:detailModel.itemPath] placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
    if (_detailModel.isEditing) {
        _changeView.hidden = NO;
        _changePrice.hidden = YES;
        _goodPrice.hidden = YES;
    }else{
        _changeView.hidden = YES;
        _changePrice.hidden = NO;
        _goodPrice.hidden = NO;
    }
}
- (void)changePriceALL{
    self.changeView.hidden = NO;
    self.changePrice.hidden = YES;
    self.goodPrice.hidden = YES;
}
- (void)cancleClick{
    self.changeView.hidden = YES;
    self.changePrice.hidden = NO;
    self.goodPrice.hidden = NO;
}
- (void)saveClick{
    if (_changePriceField.text.floatValue == 0) {
        KPOP(@"修改金额不能为0,请重新输入");
        return;
    }
    self.changeView.hidden = YES;
    self.changePrice.hidden = NO;
    self.goodPrice.hidden = NO;
    _goodPrice.text = [NSString stringWithFormat:@"￥%@",[NSString formatPriceString:_changePriceField.text]];
    if (self.changePriceAction) {
        self.changePriceAction(_changePriceField.text);
    }
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (!_isLoad) {
        if (_type == 0) {
            [self addLayout];
        }else if (_type == 1){
            [self addLayoutDetail];
        }
        _isLoad = YES;
    }
    
}
- (void)setIsHideTotal:(BOOL)isHideTotal{
    _isHideTotal = isHideTotal;
    _goodTotal.hidden = _isHideTotal;
}
- (void)setIsHideChange:(BOOL)isHideChange{
    _isHideChange = isHideChange;
    _changePrice.hidden = _isHideChange;
//    _changeView.hidden = _isHideChange;
}

- (void)setRefundModel:(YXRefundModel *)refundModel {
    _refundModel = refundModel;
    if (_refundModel.refundState == 1) {
        _statusLab.text = @"已完成";
    }else if (_refundModel.refundState == 2) {
        _statusLab.text = @"已关闭";
    }else {
        _statusLab.text = @"待处理";
    }
}

- (void)setRefundItemsModel:(YXRefundItemsModel *)refundItemsModel {
    _refundItemsModel = refundItemsModel;
 
    _goodName.text = refundItemsModel.itemInfo;
    _goodPrice.text = [NSString stringWithFormat:@"￥%@",[NSString formatPriceString:refundItemsModel.itemPrice]];
    _changePriceField.text = refundItemsModel.itemPrice;
    _goodSku.text = refundItemsModel.itemType;
    _goodCount.text = [NSString stringWithFormat:@"X%@",refundItemsModel.itemNum];
    [_goodImage sd_setImageWithURL:[NSURL URLWithString:refundItemsModel.itemPath] placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
    if (_detailModel.isEditing) {
        _changeView.hidden = NO;
        _changePrice.hidden = YES;
        _goodPrice.hidden = YES;
    }else{
        _changeView.hidden = YES;
        _changePrice.hidden = NO;
        _goodPrice.hidden = NO;
    }
}


- (void)addLayoutDetail{
    
    [self.contentView addSubview:self.goodView];
    [self.goodView addSubview:self.goodImage];
    [self.goodView addSubview:self.goodName];
    [self.goodView addSubview:self.goodPrice];
    [self.goodView addSubview:self.goodCount];
    [self.goodView addSubview:self.goodSku];
    [self.goodView addSubview:self.msgLab];
    [self.goodView addSubview:self.changePrice];

    
    [self.goodView addSubview:self.changeView];
    [self.changeView addSubview:self.cancleBtn];
    [self.changeView addSubview:self.saveBtn];
    [self.changeView addSubview:self.changePriceField];
    
    _goodPrice.font = [UIFont boldSystemFontOfSize:16];
    _goodPrice.textColor = color_RedColor;
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.left.equalTo(self.goodView).offset(15);
        make.top.equalTo(self.goodView.mas_top).offset(15);
        make.bottom.equalTo(self.goodView.mas_bottom).offset(-15);
        make.height.equalTo(self.goodImage.mas_width);
    }];
    [self.goodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
    }];
    [self.goodPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodImage.mas_bottom);
        make.left.equalTo(self.goodName.mas_left);
    }];
    [self.goodSku mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodName.mas_bottom);
        make.bottom.equalTo(self.goodPrice.mas_top);
        make.left.equalTo(self.goodName.mas_left);
        make.right.equalTo(self.goodView.mas_right).offset(-10);

    }];
    [self.msgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodSku.mas_bottom).offset(3);
        make.left.equalTo(self.goodName.mas_left);
    }];
    [self.goodCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodSku.mas_centerY);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
    }];
    [self.changePrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodPrice.mas_centerY);
        make.right.equalTo(self.goodView.mas_right).offset(-15);
    }];
    [self.changeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodImage.mas_bottom);
        make.left.equalTo(self.goodName.mas_left);
        make.right.equalTo(self.goodView.mas_right);
        make.height.mas_equalTo(20);
    }];
     [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
         make.centerY.equalTo(self.changeView.mas_centerY);
         make.right.equalTo(self.changeView.mas_right).offset(-15);
     }];
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.changeView.mas_centerY);
        make.right.equalTo(self.cancleBtn.mas_left).offset(-20);
    }];
    [self.changePriceField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.changeView.mas_centerY);
        make.left.equalTo(self.changeView.mas_left);
        make.top.bottom.equalTo(self.changeView);
    }];


    [_goodPrice setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];//不可以被压缩，尽量显示完整
    [_goodName setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];//宽度不够时，可以被压缩
}

- (void)addLayout{


    [self.contentView addSubview:self.goodView];
    [self.goodView addSubview:self.goodImage];
    [self.goodView addSubview:self.goodName];
    [self.goodView addSubview:self.goodPrice];
    [self.goodView addSubview:self.goodCount];
    [self.goodView addSubview:self.goodSku];
    [self.goodView addSubview:self.msgLab];
    [self.goodView addSubview:self.statusLab];

    _goodPrice.font = [UIFont systemFontOfSize:14];
    _goodPrice.textColor = color_TextOne;
    
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.left.equalTo(self.goodView).offset(15);
        make.top.equalTo(self.goodView.mas_top).offset(15);
        make.bottom.equalTo(self.goodView.mas_bottom).offset(-15);
        make.height.equalTo(self.goodImage.mas_width);
    }];
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.goodImage.mas_left);
        make.right.equalTo(self.goodImage.mas_right);
        make.bottom.equalTo(self.goodImage.mas_bottom);
        make.height.equalTo(@20);
    }];

    [self.goodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
    }];
    [self.goodPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.right.equalTo(self.goodView.mas_right).offset(-15);
        make.left.equalTo(self.goodName.mas_right).offset(10);
    }];
    
    [self.goodSku mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodName.mas_bottom).offset(10);
        make.left.equalTo(self.goodName.mas_left);
        make.right.equalTo(self.goodView.mas_right).offset(-10);

    }];
    [self.goodCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodSku.mas_centerY);
        make.right.equalTo(self.goodView.mas_right).offset(-15);
    }];
    [self.msgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodSku.mas_bottom).offset(3);
        make.left.equalTo(self.goodName.mas_left);
    }];
    [_goodPrice setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];//不可以被压缩，尽量显示完整
    [_goodName setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];//宽度不够时，可以被压缩
}
- (UIImageView *)goodImage{
    if (!_goodImage) {
        _goodImage = [UIImageView new];
        _goodImage.layer.cornerRadius = 5;
    }
    return _goodImage;
}
- (UILabel *)goodName{
    if (!_goodName) {
        _goodName = [UILabel new];
        _goodName.textColor = color_TextOne;
        _goodName.font = [UIFont systemFontOfSize:14];
        _goodName.numberOfLines = 2;
    }
    return _goodName;
}
- (UILabel *)goodTotal{
    if (!_goodTotal) {
        _goodTotal = [UILabel new];
        _goodTotal.textColor = color_TextOne;
        _goodTotal.font = [UIFont systemFontOfSize:14];
    }
    return _goodTotal;
}
- (UILabel *)goodPrice{
    if (!_goodPrice) {
        _goodPrice = [UILabel new];
        _goodPrice.textColor = color_TextOne;
        _goodPrice.font = [UIFont systemFontOfSize:14];
        _goodPrice.textAlignment = NSTextAlignmentRight;
    }
    return _goodPrice;
}
- (UILabel *)goodSku{
    if (!_goodSku) {
        _goodSku = [UILabel new];
        _goodSku.textColor = color_TextTwo;
        _goodSku.font = [UIFont systemFontOfSize:14];
        _goodSku.numberOfLines = 0;
    }
    return _goodSku;
}
- (UILabel *)msgLab{
    if (!_msgLab) {
        _msgLab = [UILabel new];
        _msgLab.textColor = color_RedColor;
        _msgLab.font = [UIFont systemFontOfSize:14];
    }
    return _msgLab;
}
- (UILabel *)statusLab{
    if (!_statusLab) {
        _statusLab = [UILabel new];
        _statusLab.textColor = [UIColor whiteColor];
        _statusLab.backgroundColor = [UIColor colorWithHexString:@"333333" alpha:0.5];
        _statusLab.font = [UIFont systemFontOfSize:12];
        _statusLab.textAlignment = NSTextAlignmentCenter;
    }
    return _statusLab;
}
- (UILabel *)goodCount{
    if (!_goodCount) {
        _goodCount = [UILabel new];
        _goodCount.textColor = color_TextTwo;
        _goodCount.font = [UIFont systemFontOfSize:14];
        _goodPrice.textAlignment = NSTextAlignmentRight;
        
    }
    return _goodCount;
}
- (UIButton *)changePrice{
    if (!_changePrice) {
        _changePrice = [UIButton buttonWithType:UIButtonTypeCustom];
        [_changePrice setTitle:@"修改金额" forState:UIControlStateNormal];
        [_changePrice setTitleColor:APPTintColor forState:UIControlStateNormal];
        _changePrice.titleLabel.font = [UIFont systemFontOfSize:14];
        [_changePrice addTarget:self action:@selector(changePriceALL) forControlEvents:UIControlEventTouchUpInside];
    }
    return _changePrice;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:APPTintColor forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_saveBtn addTarget:self action:@selector(saveClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBtn;
}
- (UIButton *)cancleBtn{
    if (!_cancleBtn) {
        _cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_cancleBtn setTitleColor:color_RedColor forState:UIControlStateNormal];
        _cancleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_cancleBtn addTarget:self action:@selector(cancleClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancleBtn;
}
- (LTLimitTextField *)changePriceField{
    if (!_changePriceField) {
        _changePriceField = [LTLimitTextField new];
        _changePriceField.textColor = color_TextTwo;
        _changePriceField.font = [UIFont systemFontOfSize:14];
        _changePriceField.borderStyle = UITextBorderStyleRoundedRect;
        _changePriceField.textLimitInputType = LTTextLimitInputTypeFloatNumber;
        _changePriceField.keyboardType = UIKeyboardTypeNumberPad;
        _changePriceField.placeholder = @"请输入";
    }
    return _changePriceField;
}

- (UIView *)goodView{
    if (!_goodView) {
        _goodView = [UIView new];
    }
    return _goodView;
}
- (UIView *)changeView{
    if (!_changeView) {
        _changeView = [UIView new];
        _changeView.clipsToBounds = YES;
        _changeView.hidden = YES;
    }
    return _changeView;
}

@end
