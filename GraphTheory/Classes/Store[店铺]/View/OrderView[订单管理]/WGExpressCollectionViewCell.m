//
//  WGExpressCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressCollectionViewCell.h"
#import "WGExpressModel.h"
@interface WGExpressCollectionViewCell ()

@property (nonatomic, strong) UIImageView *leftImgView;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UILabel *subTitleLab;
@property (nonatomic, strong) UIButton *courierNoBtn;
@property (nonatomic, strong) UILabel *companyLab;

@end

@implementation WGExpressCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}

- (void)setDataArr:(NSArray *)dataArr {
    _dataArr = dataArr;
}

- (void)setOrderModel:(WGExpressModel *)orderModel{
    _orderModel = orderModel;
    
    [self.courierNoBtn setTitle:orderModel.expressNo forState:(UIControlStateNormal)];
    self.companyLab.text = orderModel.deliveryTypeName;
    
    // 判断是否多物流
    if (_dataArr.count > 1) {
        [_courierNoBtn setImage:[UIImage imageNamed:@"order_list_action_down"] forState:(UIControlStateNormal)];
        [_courierNoBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(12, 6)) space:5];
    }else {
        [_courierNoBtn setImage:[UIImage imageNamed:@""] forState:(UIControlStateNormal)];
    }
    
}
- (void)addLayout{
    [self.contentView addSubview:self.leftImgView];
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.subTitleLab];
    [self.contentView addSubview:self.courierNoBtn];
    [self.contentView addSubview:self.companyLab];

    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(45, 45));
    }];
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).offset(28);
        make.top.equalTo(self.mas_top).offset(18);
        [self.titleLab sizeToFit];
    }];
    
    [self.subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).offset(28);
        make.top.equalTo(self.titleLab.mas_bottom).offset(16);
        [self.subTitleLab sizeToFit];
    }];
    
    [self.courierNoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLab.mas_right).offset(10);
        make.centerY.equalTo(self.titleLab.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(120, 20));
    }];
    
    [self.companyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.subTitleLab.mas_right).offset(10);
        make.centerY.equalTo(self.subTitleLab.mas_centerY);
        [self.companyLab sizeToFit];
    }];
    
}

#pragma mark - Lozy Loading
- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [UIImageView new];
        _leftImgView.image = [UIImage imageNamed:@"order_logistics"];
    }
    return _leftImgView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextThree;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"快递单号：";
    }
    return _titleLab;
}
- (UILabel *)subTitleLab{
    if (!_subTitleLab) {
        _subTitleLab = [UILabel new];
        _subTitleLab.textColor = color_TextThree;
        _subTitleLab.font = [UIFont systemFontOfSize:14];
        _subTitleLab.text = @"快递公司：";
    }
    return _subTitleLab;
}
- (UIButton *)courierNoBtn{
    if (!_courierNoBtn) {
        _courierNoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_courierNoBtn setTitle:@"" forState:(UIControlStateNormal)];
        [_courierNoBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _courierNoBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _courierNoBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_courierNoBtn addTarget:self action:@selector(courierNoBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _courierNoBtn;
}
- (UILabel *)companyLab{
    if (!_companyLab) {
        _companyLab = [UILabel new];
        _companyLab.textColor = color_TextOne;
        _companyLab.font = [UIFont systemFontOfSize:14];
    }
    return _companyLab;
}

#pragma mark - Touch Even
- (void)courierNoBtnAction:(UIButton *)sender {
    
    // 判断多物流
    if (self.dataArr.count > 1) {
        if (self.clickSelectCourierNoBtn) {
            self.clickSelectCourierNoBtn(sender);
        }
    }

}

@end
