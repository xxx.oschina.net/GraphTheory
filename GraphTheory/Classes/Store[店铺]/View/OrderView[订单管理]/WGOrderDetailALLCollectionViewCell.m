//
//  WGOrderDetailALLCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderDetailALLCollectionViewCell.h"

@interface WGOrderDetailALLCollectionViewCell ()


@property (nonatomic ,strong) UILabel *leftLab;

@property (nonatomic ,strong) UILabel *rightLab;

@end


@implementation WGOrderDetailALLCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setModel:(WGShopModel *)model{
    _model = model;
    if (model.orderState == 0) {
        _leftLab.text = @"待付款:";
        _leftLab.textColor = color_RedColor;
    }else{
        _leftLab.text = @"实付款:";
        _leftLab.textColor = color_TextOne;
    }
    
    if (_model.exchangeIntegral > 0 && [_model.mainPayPrice floatValue] > 0) {
        _rightLab.text = [NSString stringWithFormat:@"积分%ld + ￥%@",_model.exchangeIntegral,[NSString formatPriceString:model.mainPayPrice]];
    }else if (_model.exchangeIntegral > 0) {
        _rightLab.text = [NSString stringWithFormat:@"积分%ld",_model.exchangeIntegral];
    }else if ([_model.mainPayPrice floatValue] > 0) {
        _rightLab.text = [NSString stringWithFormat:@"￥%@",[NSString formatPriceString:model.mainPayPrice]];
    }
    
}
- (void)addLayout{
    [self.contentView addSubview:self.leftLab];
    [self.contentView addSubview:self.rightLab];
    [self.rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
    }];
    [self.leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.rightLab.mas_left).offset(2);
    }];

}

- (UILabel *)leftLab {
    if (!_leftLab) {
        _leftLab = [[UILabel alloc] init];
        _leftLab.textColor = color_TextOne;
        _leftLab.font = [UIFont systemFontOfSize:14.0f];
        _leftLab.text = @"实付款:";
    }
    return _leftLab;
}
- (UILabel *)rightLab {
    if (!_rightLab) {
        _rightLab = [[UILabel alloc] init];
        _rightLab.textColor = color_RedColor;
        _rightLab.font = [UIFont systemFontOfSize:16.0f];
    }
    return _rightLab;
}


@end
