//
//  WGOrderDetailAddressCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情地址
 */
@interface WGOrderDetailAddressCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) WGShopModel *model;
@property (nonatomic, assign) NSInteger type;///<2:商品详情确认订单 1:订单详情 3:购物车确认订单

@end

NS_ASSUME_NONNULL_END
