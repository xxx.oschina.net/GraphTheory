//
//  YXRefundProgressCollectionViewCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/6.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXRefundsModel;
@interface YXRefundProgressCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) NSIndexPath *indexPath;
@property (nonatomic ,strong) YXRefundsModel *refundsModel;


@end

NS_ASSUME_NONNULL_END
