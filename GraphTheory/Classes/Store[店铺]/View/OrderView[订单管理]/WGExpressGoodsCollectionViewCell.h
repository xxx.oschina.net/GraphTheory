//
//  WGExpressGoodsCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/19.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGExpressGoodsCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) WGOderDetailModel *model;

@end

NS_ASSUME_NONNULL_END
