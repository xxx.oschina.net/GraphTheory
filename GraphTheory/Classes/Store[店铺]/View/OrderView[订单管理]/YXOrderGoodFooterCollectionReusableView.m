//
//  YXOrderGoodFooterCollectionReusableView.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderGoodFooterCollectionReusableView.h"

@interface YXOrderGoodFooterCollectionReusableView ()

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UIButton *refundBtn;

@end

@implementation YXOrderGoodFooterCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         self.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        [self setup];
    }
    return self;
}

- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [UIView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;;
}

- (UIButton *)refundBtn {
    if (!_refundBtn) {
        _refundBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_refundBtn setTitle:@"退款" forState:(UIControlStateNormal)];
        [_refundBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
        _refundBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _refundBtn.layer.masksToBounds = YES;
        _refundBtn.layer.cornerRadius = 4;
        _refundBtn.layer.borderColor = color_TextThree.CGColor;
        _refundBtn.layer.borderWidth = 1;
        [_refundBtn addTarget:self action:@selector(refundBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _refundBtn;
}


- (void)setup {
    
    YXWeakSelf
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.refundBtn];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(weakSelf);
        make.height.equalTo(@44);
    }];
    
    [_refundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.right.equalTo(weakSelf.bgView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(60, 24));
    }];

}

- (void)refundBtnAction {
    
    if (self.clickRefundBtnBlock) {
        self.clickRefundBtnBlock();
    }
}

@end
