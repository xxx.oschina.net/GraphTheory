//
//  WGOrderDetailInfoCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGOrderDetailInfoCollectionViewCell.h"
@interface WGOrderDetailInfoCollectionViewCell ()

//@property (nonatomic ,strong) UILabel *leftLab;
@end

@implementation WGOrderDetailInfoCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
//        [self addLayout];
    }
    return self;
}

- (void)setArr:(NSArray *)arr{
    _arr = arr;
    
    for (UIView *subView in self.contentView.subviews) {
        [subView removeFromSuperview];
    }
    
    if (arr.count > 0) {
        for (int i = 0; i < arr.count ;i ++ ) {
            WGOderDetailInfoModel *model = [arr objectAtIndex:i];
            UILabel *lab = [[UILabel alloc] initWithFrame:(CGRectMake(15, 10 + i * 30, KWIDTH-30, 30))];
            lab.textColor = color_TextOne;
            lab.font = [UIFont systemFontOfSize:14.0f];
            lab.text = [NSString stringWithFormat:@"%@:%@",model.keyStr,model.valueStr];
            if ([model.keyStr isEqualToString:@"订单号"]) {
                lab.userInteractionEnabled = YES;
                [lab addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(orderNoPasteBoard:)]];
            }
            if ([model.keyStr isEqualToString:@"交易单号"]) {
                lab.userInteractionEnabled = YES;
                [lab addGestureRecognizer:[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(tradingPasteBoard:)]];
            }
            [self.contentView addSubview:lab];
        }
    }
    
}

#pragma mark - 复制订单号
- (void)orderNoPasteBoard:(UILongPressGestureRecognizer*)longPress {

    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        if (_arr.count > 0) {
            for (WGOderDetailInfoModel *model in _arr) {
                if ([model.keyStr isEqualToString:@"订单号"]) {
                    pasteboard.string = [NSString stringWithFormat:@"%@",model.valueStr];
                    [YJProgressHUD showSuccess:@"复制成功"];
                }
            }
         }
    }

}
#pragma mark - 复制交易单号
- (void)tradingPasteBoard:(UILongPressGestureRecognizer*)longPress {
    
    if (longPress.state == UIGestureRecognizerStateBegan) {
         UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
         if (_arr.count > 0) {
             for (WGOderDetailInfoModel *model in _arr) {
                 if ([model.keyStr isEqualToString:@"交易单号"]) {
                     pasteboard.string = [NSString stringWithFormat:@"%@",model.valueStr];
                     [YJProgressHUD showSuccess:@"复制成功"];
                 }
             }
          }
     }
}


@end
