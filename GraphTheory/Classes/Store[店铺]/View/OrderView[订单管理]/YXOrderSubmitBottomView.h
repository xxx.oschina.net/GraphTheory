//
//  YXOrderSubmitBottomView.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/10.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情底部按钮
 */
@interface YXOrderSubmitBottomView : UIView

@property (nonatomic ,strong) UIButton *submitBtn;
@property (nonatomic ,strong) UILabel *priceLab;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *numLab;


@property (nonatomic ,copy) void(^orderAction)(void);


@end

NS_ASSUME_NONNULL_END
