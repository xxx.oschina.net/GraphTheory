//
//  WGOrderDetailStatusCollectionReusableView.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/1.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情-顶部订单状态
 */
@class YXRefundModel;
@interface WGOrderDetailStatusCollectionReusableView : UICollectionReusableView
@property (nonatomic, strong) WGShopModel *orderModel;

// 退款详情
@property (nonatomic, strong) YXRefundModel *refundModel;

@end

NS_ASSUME_NONNULL_END
