//
//  WGOrderHeadCollectionViewCell.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/7/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 订单列表顶部-联系收货人
 */
@class YXRefundModel;
@interface WGOrderHeadCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) NSInteger type;///<0:订单列表 1:订单详情

@property (nonatomic, strong) WGShopModel *model;
// 售后
@property (nonatomic ,strong) YXRefundModel *refundModel;

@property (nonatomic ,copy)void(^clickMsgBlock)(void);

@end

NS_ASSUME_NONNULL_END
