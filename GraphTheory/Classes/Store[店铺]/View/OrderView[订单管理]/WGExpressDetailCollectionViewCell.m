//
//  WGExpressDetailCollectionViewCell.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressDetailCollectionViewCell.h"
#import "WGExpressModel.h"
#import "YXRefundModel.h"
@interface WGExpressDetailCollectionViewCell ()

@property (nonatomic, strong) UILabel *detailLab;
@property (nonatomic, strong) UILabel *timeLab;
@property (nonatomic, strong) UIView *topViewLine;
@property (nonatomic, strong) UIView *bottomViewLine;
@property (nonatomic, strong) UIView *circleViewLine;

@end

@implementation WGExpressDetailCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setIsFirst:(BOOL)isFirst{
    _isFirst = isFirst;
    _topViewLine.hidden = NO;
    if (isFirst) {
        _topViewLine.hidden = YES;
        _circleViewLine.backgroundColor = APPTintColor;
    }else{
        _circleViewLine.backgroundColor = color_LineColor;
    }
}
- (void)setIsEnd:(BOOL)isEnd{
    _isEnd = isEnd;
    _bottomViewLine.hidden = NO;
    if (_isEnd) {
        _bottomViewLine.hidden = YES;
    }
}
- (void)setModel:(OrderTrack *)model{
    _model = model;
    _detailLab.text = model.status;
    _timeLab.text = model.time;
}
//- (void)setFlowModel:(WGRefundFlowModel *)flowModel{
//    _flowModel = flowModel;
//    _detailLab.text = flowModel.refundScheduleText;
//    _timeLab.text = flowModel.refundScheduleTime;
//}
- (void)setRefundsModel:(YXRefundsModel *)refundsModel {
    _refundsModel = refundsModel;
    _detailLab.text = _refundsModel.refundScheduleText;
    _timeLab.text = _refundsModel.refundScheduleTime;
}

- (void)addLayout{
    [self.contentView addSubview:self.circleViewLine];
    [self.contentView addSubview:self.topViewLine];
    [self.contentView addSubview:self.bottomViewLine];
    [self.contentView addSubview:self.detailLab];
    [self.contentView addSubview:self.timeLab];
    [_circleViewLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(10, 10));
        make.top.equalTo(self.mas_top).offset(15);
        make.left.equalTo(self.mas_left).offset(15);
    }];
    [_topViewLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.top.equalTo(self.mas_top);
        make.bottom.equalTo(self.circleViewLine.mas_top);
        make.centerX.equalTo(self.circleViewLine.mas_centerX);
    }];
    [_bottomViewLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(1);
        make.top.equalTo(self.circleViewLine.mas_bottom);
        make.bottom.equalTo(self.mas_bottom);
        make.centerX.equalTo(self.circleViewLine.mas_centerX);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.circleViewLine.mas_top).offset(-5);
        make.right.equalTo(self.mas_right).offset(-10);
        make.left.equalTo(self.circleViewLine.mas_right).offset(15);
    }];
    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15);
        make.left.equalTo(self.circleViewLine.mas_right).offset(15);
        make.top.equalTo(self.detailLab.mas_bottom).offset(5);
    }];
    
}
- (UIView *)circleViewLine{
    if (!_circleViewLine) {
        _circleViewLine = [UIView new];
        _circleViewLine.layer.cornerRadius = 5;
        _circleViewLine.backgroundColor = color_LineColor;
    }
    return _circleViewLine;
}
- (UIView *)topViewLine{
    if (!_topViewLine) {
        _topViewLine = [UIView new];
        _topViewLine.backgroundColor = color_LineColor;
    }
    return _topViewLine;
}
- (UIView *)bottomViewLine{
    if (!_bottomViewLine) {
        _bottomViewLine = [UIView new];
        _bottomViewLine.backgroundColor = color_LineColor;
    }
    return _bottomViewLine;
}
- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [UILabel new];
        _detailLab.textColor = color_TextOne;
        _detailLab.numberOfLines = 0;
        _detailLab.font = [UIFont systemFontOfSize:14];
    }
    return _detailLab;
}
- (UILabel *)timeLab{
    if (!_timeLab) {
        _timeLab = [UILabel new];
        _timeLab.textColor = color_TextThree;
        _timeLab.numberOfLines = 0;
        _timeLab.font = [UIFont systemFontOfSize:12];
    }
    return _timeLab;
}
@end
