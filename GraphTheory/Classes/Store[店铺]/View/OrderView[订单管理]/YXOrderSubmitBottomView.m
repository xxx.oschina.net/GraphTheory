//
//  YXOrderSubmitBottomView.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/10.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "YXOrderSubmitBottomView.h"

@interface YXOrderSubmitBottomView ()


@end

@implementation YXOrderSubmitBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}

- (void)setup {
    [self addSubview:self.submitBtn];
    [self addSubview:self.priceLab];
    [self addSubview:self.titleLab];
    [self addSubview:self.numLab];
    
    [self.submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.submitBtn.mas_left).offset(-10);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.priceLab.mas_left);
    }];
    [self.numLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.titleLab.mas_left).offset(-10);
    }];
}

#pragma mark - Lazy loading
- (UIButton *)submitBtn {
    if (!_submitBtn) {
        _submitBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_submitBtn setTitle:@"提交订单" forState:(UIControlStateNormal)];
        [_submitBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _submitBtn.backgroundColor = APPTintColor;
        _submitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
        _submitBtn.layer.masksToBounds = YES;
        _submitBtn.layer.cornerRadius =4.0;
        [_submitBtn addTarget:self action:@selector(payBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _submitBtn;
}
- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] init];
        _priceLab.textColor = APPTintColor;
        _priceLab.font = [UIFont systemFontOfSize:12.0f];
        _priceLab.text = @"¥0.10";
    }
    return _priceLab;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor blackColor];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.text = @"合计：";
    }
    return _titleLab;
}
- (UILabel *)numLab {
    if (!_numLab) {
        _numLab = [[UILabel alloc] init];
        _numLab.textColor = color_TextTwo;
        _numLab.font = [UIFont systemFontOfSize:14.0f];
        _numLab.text = @"共0项";
    }
    return _numLab;
}

#pragma make -  Touch Even
- (void)payBtnAction:(UIButton *)sender{
    if (self.orderAction) {
        self.orderAction();
    }
}

@end
