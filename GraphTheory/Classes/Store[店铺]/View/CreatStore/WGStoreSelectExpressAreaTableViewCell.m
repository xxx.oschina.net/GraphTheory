//
//  WGStoreSelectExpressAreaTableViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectExpressAreaTableViewCell.h"

@interface WGStoreSelectExpressAreaTableViewCell ()

@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UIImageView *selectImageView;
@property (strong, nonatomic) UIButton *selectBtn;


@end

@implementation WGStoreSelectExpressAreaTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style
                    reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIsFirst:(BOOL)isFirst{
    _isFirst = isFirst;
    [_selectBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(self.isFirst?3:36);
    }];
}
- (void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    _selectBtn.selected = isSelect;
//    _selectImageView.image = [UIImage imageNamed:_isSelect?@"store_creat_mutSelect": @"store_creat_mutNoSelect"];
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = _titleStr;
}
- (void)selectAction{
    if (self.select) {
        self.select();
    }
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.selectBtn];
    
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(3);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(36, 36));
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.selectBtn.mas_right);
        make.centerY.equalTo(self.mas_centerY);
    }];
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}
- (UIImageView *)selectImageView{
    if (!_selectImageView) {
        _selectImageView = [UIImageView new];
        _selectImageView.image = [UIImage imageNamed:@"store_creat_mutSelect"];
    }
    return _selectImageView;
}
- (UIButton *)selectBtn{
    if (!_selectBtn) {
        _selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_selectBtn setImage:[UIImage imageNamed:@"store_creat_mutNoSelect"] forState:UIControlStateNormal];
        [_selectBtn setImage:[UIImage imageNamed:@"store_creat_mutSelect"] forState:UIControlStateSelected];
        [_selectBtn addTarget:self action:@selector(selectAction) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _selectBtn;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
