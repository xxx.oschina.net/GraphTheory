//
//  WGCreatStoreCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGStoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGCreatStoreCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSIndexPath *index;
@property (nonatomic, strong) WGStoreModel *storeModel;
@property (nonatomic, strong) NSIndexPath *shopIndex;

@property (nonatomic ,copy) void(^textFieldChangeBlock)(NSString *content);
@property (nonatomic ,copy) void(^textViewChange)(CGFloat height);


@end

NS_ASSUME_NONNULL_END
