//
//  WGStoreSelctTimeWeekCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 星期
 */
@interface WGStoreSelctTimeWeekCollectionViewCell : UICollectionViewCell
@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, assign) NSInteger type;///<1:绿色

@property (nonatomic, strong) NSString *titleStr;

@end

NS_ASSUME_NONNULL_END
