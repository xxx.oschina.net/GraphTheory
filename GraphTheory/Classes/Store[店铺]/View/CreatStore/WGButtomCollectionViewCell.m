//
//  WGButtomCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGButtomCollectionViewCell.h"

@interface WGButtomCollectionViewCell ()

@property (strong, nonatomic) UIButton *saveBtn;

@end

@implementation WGButtomCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setEnabled:(BOOL)enabled{
    _enabled = enabled;
    _saveBtn.alpha = _enabled?1:0.5;
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    [_saveBtn setTitle:titleStr forState:UIControlStateNormal];
}
- (void)setBackColor:(UIColor *)backColor{
    _backColor = backColor;
    _saveBtn.backgroundColor = backColor;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.saveBtn];
    [_saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _saveBtn.layer.cornerRadius = 4;
        _saveBtn.userInteractionEnabled = NO;
        _saveBtn.alpha = 0.5;
    }
    return _saveBtn;
}
@end
