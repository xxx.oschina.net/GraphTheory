//
//  WGStoreSelctTimeCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelctTimeCollectionViewCell.h"
#import <BRPickerView/BRDatePickerView.h>

@interface WGStoreSelctTimeCollectionViewCell ()

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UIView *lineView;
@property (strong, nonatomic) UIButton *begainBtn;
@property (strong, nonatomic) UIButton *endBtn;
@property (strong, nonatomic) UIButton *addBtn;


@end

@implementation WGStoreSelctTimeCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    if (index.row == 0) {
        [_addBtn setImage:[UIImage imageNamed:@"store_creat_selectTime_add"] forState:UIControlStateNormal];
    }else{
        [_addBtn setImage:[UIImage imageNamed:@"store_creat_selectTime_reduce"] forState:UIControlStateNormal];
    }
}
- (void)setLastmodel:(WGStoreOpenModel *)lastmodel{
    _lastmodel = lastmodel;
}
- (void)setModel:(WGStoreOpenModel *)model{
    _model = model;
    [_begainBtn setTitle:model.beginTime.length > 0?model.beginTime:@"开始时间" forState:UIControlStateNormal];
    _begainBtn.selected = model.beginTime.length == 0;
    [_endBtn setTitle:model.endTime.length > 0?model.endTime:@"结束时间" forState:UIControlStateNormal];
    _endBtn.selected = model.endTime.length == 0;
}
- (void)selectTime:(UIButton *)btn{
    if (btn == _addBtn) {
        if (self.selectStoreTime) {
            self.selectStoreTime(_model, _index, 2);
        }
        return;
    }
    DDWeakSelf;
    NSDate *minDate;
    NSDate *maxDate;
    if (btn == _begainBtn) {
        if (_model.endTime) {
            NSArray *tem = [_model.endTime componentsSeparatedByString:@":"];
            maxDate = [NSDate br_setHour:[tem[0] integerValue] minute:[tem[1] integerValue]];
        }
        if (_lastmodel.endTime) {
            NSArray *tem = [_lastmodel.endTime componentsSeparatedByString:@":"];
            minDate = [NSDate br_setHour:[tem[0] integerValue] minute:[tem[1] integerValue]];
        }
    }else{
        if (_lastmodel.endTime) {
            NSArray *tem = [_lastmodel.endTime componentsSeparatedByString:@":"];
            minDate = [NSDate br_setHour:[tem[0] integerValue] minute:[tem[1] integerValue]];
        }

        if (_model.beginTime.length > 0) {
            NSArray *tem = [_model.beginTime componentsSeparatedByString:@":"];
            minDate = [NSDate br_setHour:[tem[0] integerValue] minute:[tem[1] integerValue]];
        }
    }
    [BRDatePickerView showDatePickerWithMode:BRDatePickerModeHM title:btn == _begainBtn?@"开始时间":@"结束时间" selectValue:_begainBtn?_model.beginTime:_model.endTime minDate:minDate maxDate:maxDate isAutoSelect:NO resultBlock:^(NSDate * _Nullable selectDate, NSString * _Nullable selectValue) {
        if (btn == weakSelf.begainBtn) {
            weakSelf.model.beginTime = selectValue;
        }else{
            weakSelf.model.endTime = selectValue;
        }
        if (weakSelf.selectStoreTime) {
            weakSelf.selectStoreTime(weakSelf.model, weakSelf.index, 0);
        }
    }];
    
    
//    [BRDatePickerView showDatePickerWithTitle:btn == _begainBtn?@"开始时间":@"结束时间" dateType:BRDatePickerModeHM defaultSelValue:btn == _begainBtn?_model.beginTime:_model.endTime minDate:minDate maxDate:maxDate isAutoSelect:NO themeColor:color_TextOne resultBlock:^(NSString *selectValue) {
//        if (btn == weakSelf.begainBtn) {
//            weakSelf.model.beginTime = selectValue;
//        }else{
//            weakSelf.model.endTime = selectValue;
//        }
//        if (weakSelf.selectStoreTime) {
//            weakSelf.selectStoreTime(weakSelf.model, weakSelf.index, 0);
//        }
//    } cancelBlock:^{
//        
//    }];
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.begainBtn];
    [self.backView addSubview:self.endBtn];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.addBtn];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(46);
        make.right.top.bottom.equalTo(self.backView);
    }];
    [_begainBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.top.and.bottom.equalTo(self.backView);
        make.right.equalTo(self.lineView.mas_left).offset(-10);
        make.width.equalTo(self.endBtn.mas_width);
    }];
    [_endBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lineView.mas_right).offset(10);
        make.top.and.bottom.equalTo(self.backView);
        make.right.equalTo(self.addBtn.mas_left);
        make.width.equalTo(self.endBtn.mas_width);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(34, 1));
        make.centerY.equalTo(self.backView.mas_centerY);
    }];

}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_TextThree;
    }
    return _lineView;
}
- (UIButton *)begainBtn{
    if (!_begainBtn) {
        _begainBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _begainBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_begainBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        [_begainBtn setTitleColor:color_TextTwo forState:UIControlStateSelected];

        [_begainBtn addTarget:self action:@selector(selectTime:) forControlEvents:UIControlEventTouchUpInside];
        _begainBtn.layer.borderColor = color_TextThree.CGColor;
        _begainBtn.layer.borderWidth = 1;
        _begainBtn.layer.cornerRadius = 4;
    }
    return _begainBtn;
}
- (UIButton *)endBtn{
    if (!_endBtn) {
        _endBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _endBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_endBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        [_endBtn setTitleColor:color_TextTwo forState:UIControlStateSelected];
        [_endBtn addTarget:self action:@selector(selectTime:) forControlEvents:UIControlEventTouchUpInside];
        _endBtn.layer.borderColor = color_TextThree.CGColor;
        _endBtn.layer.borderWidth = 1;
        _endBtn.layer.cornerRadius = 4;

    }
    return _endBtn;
}
- (UIButton *)addBtn{
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addBtn addTarget:self action:@selector(selectTime:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addBtn;

}
@end
