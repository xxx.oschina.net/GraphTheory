//
//  WGStoreSelctShipTableViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelctShipTableViewCell.h"

@interface WGStoreSelctShipTableViewCell ()

@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UIImageView *selectImageView;


@end

@implementation WGStoreSelctShipTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style
                    reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    _selectImageView.image = [UIImage imageNamed:_isSelect?@"store_creat_mutSelect": @"store_creat_mutNoSelect"];

}
- (void)setType:(NSInteger)type{
    _type = type;
    _selectImageView.hidden = type == 1;
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = titleStr;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.selectImageView];

    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.centerY.equalTo(self.mas_centerY);
    }];
    [_selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-15);
        make.centerY.equalTo(self.mas_centerY);
    }];
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}
- (UIImageView *)selectImageView{
    if (!_selectImageView) {
        _selectImageView = [UIImageView new];
        _selectImageView.image = [UIImage imageNamed:@"store_creat_mutSelect"];
    }
    return _selectImageView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
