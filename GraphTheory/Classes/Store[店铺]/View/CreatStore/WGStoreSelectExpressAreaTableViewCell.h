//
//  WGStoreSelectExpressAreaTableViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelectExpressAreaTableViewCell : DDBaseTableViewCell

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, assign) BOOL isFirst;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic ,copy) void(^select)(void);

@end

NS_ASSUME_NONNULL_END
