//
//  YXStoreTypeCollectionViewCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/8/11.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXStoreTypeCollectionViewCell.h"

@implementation YXStoreTypeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}

/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.detailLab];
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.top.equalTo(self.backView.mas_top).offset(13);
    }];
  
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.left.equalTo(self.titleLab.mas_right).offset(15);
        //        make.centerY.equalTo(self.backView.mas_centerY);
        make.top.equalTo(self.titleLab.mas_top);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.backView);
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.height.mas_equalTo(@1);
    }];

}


- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"F4F4F4"];
    }
    return _lineView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}
- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [UILabel new];
        _detailLab.textColor = color_TextOne;
        _detailLab.font = [UIFont systemFontOfSize:14];
        _detailLab.textAlignment = NSTextAlignmentRight;
    }
    return _detailLab;
}

@end
