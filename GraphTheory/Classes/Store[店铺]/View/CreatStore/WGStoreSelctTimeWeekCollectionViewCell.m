//
//  WGStoreSelctTimeWeekCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelctTimeWeekCollectionViewCell.h"

@interface WGStoreSelctTimeWeekCollectionViewCell ()

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UILabel *titleLab;

@end

@implementation WGStoreSelctTimeWeekCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    _backView.layer.borderWidth = _isSelect?0:1;
    _backView.backgroundColor = _isSelect?APPTintColor:[UIColor whiteColor];
    _titleLab.textColor = _isSelect?[UIColor whiteColor]:color_TextOne;

}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = _titleStr;
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 1) {
        _backView.layer.borderWidth = 1;
        _backView.layer.borderColor = color_GreenColor.CGColor;
        _backView.backgroundColor = [UIColor whiteColor];
        _titleLab.textColor = color_GreenColor;
    }else{
        _backView.layer.borderWidth = _isSelect?0:1;
        _backView.layer.borderColor = color_TextThree.CGColor;
        _backView.backgroundColor = _isSelect?APPTintColor:[UIColor whiteColor];
        _titleLab.textColor = _isSelect?[UIColor whiteColor]:color_TextOne;
    }
    
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.backView);
    }];
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.borderColor = color_TextThree.CGColor;
        _backView.layer.borderWidth = 1;
        _backView.layer.cornerRadius = 4;
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}


@end
