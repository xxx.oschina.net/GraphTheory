//
//  WGCreatStoreCollectionViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGCreatStoreCollectionViewCell.h"
#import "LTPlaceholderTextView.h"

@interface WGCreatStoreCollectionViewCell ()<UITextFieldDelegate,UITextViewDelegate>

@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UIView *lineView;

@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UILabel *detailLab;
@property (strong, nonatomic) LTLimitTextField *detailField;
@property (strong, nonatomic) LTPlaceholderTextView *textView;

@property (strong, nonatomic) UIImageView *detailImageView;
@property (strong, nonatomic) UIImageView *arrowImageView;

@end

@implementation WGCreatStoreCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setStoreModel:(WGStoreModel *)storeModel{
    _storeModel = storeModel;
}
- (void)setShopIndex:(NSIndexPath *)shopIndex{
    _shopIndex = shopIndex;
    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(17);
    }];

    [_titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    _detailField.keyboardType = UIKeyboardTypeDefault;
    _detailField.textLimitInputType = 0;
    _detailField.textLimitType = 0;
    _detailImageView.hidden = YES;
    _detailField.hidden = NO;
    _detailLab.hidden = YES;
    _textView.hidden = YES;
    switch (shopIndex.row) {
        case 1:{
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];

            _titleLab.text = @"店铺图片";
            if (_storeModel.storeImage) {
                _detailImageView.image = _storeModel.storeImage;
                _detailImageView.hidden = NO;
                _detailField.hidden = YES;
            }else if (_storeModel.shopLogo){
                _detailImageView.hidden = NO;
                _detailField.hidden = YES;
                [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeModel.shopLogo] placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
            }else{
                _detailImageView.image = [UIImage imageNamed:@"shop_icon_add"];
                _detailImageView.hidden = NO;
                _detailField.hidden = YES;
                
            }
        }
            break;
        case 2:
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];
            _detailField.textLimitInputType = LTTextLimitInputTypeNone;
            _detailField.textLimitType = LTTextLimitTypeLength;
            _detailField.textLimitSize = 16;
            _titleLab.text = @"店铺名称";
            _detailField.placeholder = @"请输入";
            _detailField.enabled = YES;
            _detailField.text = _storeModel.shopName;
            break;
        case 3:
            _titleLab.text = @"主营类目";
            _detailField.placeholder = @"请选择";
            _detailField.enabled = NO;
            _detailField.text = _storeModel.mainCategoryName;
            break;
        case 4:
        {
            _titleLab.text = @"配送方式";
            _detailField.placeholder = @"请选择";
            _detailField.enabled = NO;
            NSString *tem = [_storeModel.distributionType copy];
            tem = [tem stringByReplacingOccurrencesOfString:@"1" withString:@"快递"];
            tem = [tem stringByReplacingOccurrencesOfString:@"2" withString:@"到店"];
            tem = [tem stringByReplacingOccurrencesOfString:@"3" withString:@"上门"];
            _detailField.text = tem;
        }
            break;
        case 5:
            _titleLab.text = @"选择地区";
            _detailField.placeholder = @"请选择";
            _detailField.enabled = NO;
            _detailField.text = [NSString stringWithFormat:@"%@%@%@",_storeModel.province?:@"",_storeModel.city?:@"",_storeModel.county?:@""];
            
            break;
        case 6:{
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];
            [_titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.backView.mas_left).offset(15);
                make.top.equalTo(self.backView.mas_top).offset(13);
            }];
            _titleLab.text = @"详细地址";
            _detailField.hidden = YES;
            _textView.hidden = NO;
            _textView.placeholder = _storeModel.shopAddress?@"":@"请输入";
            _detailField.enabled = NO;
            _textView.text = _storeModel.shopAddress;
        }
            break;
        case 7:
            _detailField.keyboardType = UIKeyboardTypeDecimalPad;
            _detailField.textLimitInputType = LTTextLimitInputTypeNumber;
            _detailField.textLimitType = LTTextLimitTypeLength;
            _detailField.textLimitSize = 2;
            _titleLab.text = @"上门服务范围(公里/km)";
            _detailField.placeholder = @"公里";
            _detailField.enabled = YES;
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];
            _detailField.text = _storeModel.serviceScope;
            
            break;
        case 8:{
            _titleLab.text = @"营业时间";
            if (_storeModel.storeTime) {
                _detailLab.hidden = NO;
                _detailField.hidden = YES;
                
                _detailLab.numberOfLines = 0;
                _detailLab.text = _storeModel.yingyeTime;
//                [_detailLab mas_updateConstraints:^(MASConstraintMaker *make) {
//                    make.top.equalTo(self.backView.mas_top).offset(5);
//                }];
            }else{
                _detailField.placeholder = @"请选择";
                _detailField.enabled = YES;
                _detailField.hidden = NO;
            }
        }
            
            break;
        case 9:
            _titleLab.text = @"快递配送区域";
            _detailField.placeholder = @"";
            if (!_storeModel.whetherNationalArea) {
                _detailField.text = @"全国";
            }else{
                if (_storeModel.areaData.length) {
                    _detailField.text = @"已选择";
                }else{
                    _detailField.text = @"";
                }
            }
            _detailField.enabled = NO;
            break;
        case 10:
            _titleLab.text = @"服务电话";
            _detailField.textLimitType = LTTextLimitTypeLength;
            _detailField.textLimitSize = 18;
            _detailField.keyboardType = UIKeyboardTypePhonePad;
            _detailField.textLimitInputType = LTTextLimitInputTypeNumber;
            _detailField.text = _storeModel.serviceMobile;
            _detailField.placeholder = @"请输入";

            _detailField.enabled = YES;
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];
            //                    _detailField.text = _storeModel.storePhone;
            
            break;
        case 11:
        {
            [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(0);
            }];
            [_titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.backView.mas_left).offset(15);
                make.top.equalTo(self.backView.mas_top).offset(13);
            }];
            _titleLab.text = @"店铺简介";
            _detailField.hidden = YES;
            _textView.hidden = NO;
            _textView.placeholder = _storeModel.shopIntroduce?@"":@"请输入";
            _detailField.enabled = NO;
            _textView.text = _storeModel.shopIntroduce;
        }
            break;
            
        default:
            break;
    }
    
    
    
}
- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(17);
    }];
    _detailField.keyboardType = UIKeyboardTypeDefault;
    _detailField.textLimitInputType = 0;
    _detailField.textLimitType = 0;
    _detailImageView.hidden = YES;
    _detailField.hidden = NO;
    _detailLab.hidden = YES;
    _textView.hidden = YES;
    switch (index.section) {
        case 0:
        {
            switch (index.row) {
                case 0:
                    _titleLab.text = @"店铺图片";
                    if (_storeModel.storeImage) {
                        _detailImageView.image = _storeModel.storeImage;
                        _detailImageView.hidden = NO;
                        _detailField.hidden = YES;
                    }else if (_storeModel.shopLogo){
                        _detailImageView.hidden = NO;
                        _detailField.hidden = YES;
                        [_detailImageView sd_setImageWithURL:[NSURL URLWithString:_storeModel.shopLogo] placeholderImage:[UIImage imageNamed:@"goods_placeholder"]];
                    }else{
                        _detailImageView.hidden = YES;
                        
                        _detailField.hidden = NO;
                        _detailField.placeholder = @"请选择";
                        _detailField.enabled = NO;
                        _detailField.text = nil;
                        
                    }
                    break;
                case 1:
                    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.width.mas_equalTo(0);
                    }];
                    
                    _titleLab.text = @"店铺名称";
                    _detailField.placeholder = @"请输入";
                    _detailField.enabled = YES;
                    _detailField.text = _storeModel.shopName;
                    break;
                case 2:
                    _titleLab.text = @"主营类目";
                    _detailField.placeholder = @"请选择";
                    _detailField.enabled = NO;
                    _detailField.text = _storeModel.mainCategoryName;
                    break;
                case 3:
                {
                    _titleLab.text = @"配送方式";
                    _detailField.placeholder = @"请选择";
                    _detailField.enabled = NO;
                    NSString *tem = [_storeModel.distributionType copy];
                    tem = [tem stringByReplacingOccurrencesOfString:@"1" withString:@"快递"];
                    tem = [tem stringByReplacingOccurrencesOfString:@"2" withString:@"到店"];
                    tem = [tem stringByReplacingOccurrencesOfString:@"3" withString:@"上门"];
                    _detailField.text = tem;
                }
                    break;
                case 4:
                    _titleLab.text = @"店铺地址";
                    _detailField.placeholder = @"请选择";
                    _detailField.enabled = NO;
                    _detailField.text = _storeModel.shopAddress;
                    
                    break;
                    
                    
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            switch (index.row) {
                case 0:
                    _detailField.keyboardType = UIKeyboardTypeDecimalPad;
                    _detailField.textLimitInputType = LTTextLimitInputTypeNumber;
                    _titleLab.text = @"上门服务范围(公里/km)";
                    _detailField.placeholder = @"公里";
                    _detailField.enabled = YES;
                    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.width.mas_equalTo(0);
                    }];
                    _detailField.text = _storeModel.serviceScope;
                    
                    break;
                case 1:{
                    _titleLab.text = @"营业时间";
                    if (_storeModel.storeTime) {
                        _detailLab.hidden = NO;
                        _detailField.hidden = YES;
                        
                        _detailLab.numberOfLines = 0;
                        _detailLab.text = _storeModel.yingyeTime;
                    }else{
                        _detailField.placeholder = @"请选择";
                        _detailField.enabled = YES;
                        _detailField.hidden = NO;
                    }
                    
                }
                    
                    break;
                case 2:
                    _titleLab.text = @"快递配送区域";
                    _detailField.placeholder = @"";
                    if (!_storeModel.whetherNationalArea) {
                        _detailField.text = @"全国";
                    }else{
                        if (_storeModel.areaData.length) {
                            _detailField.text = @"已选择";
                        }else{
                            _detailField.text = @"";
                        }
                    }
                    _detailField.enabled = NO;
                    break;
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (index.row) {
                case 0:
                    _titleLab.text = @"服务电话";
                    _detailField.textLimitType = LTTextLimitTypeLength;
                    _detailField.textLimitSize = 18;
                    _detailField.keyboardType = UIKeyboardTypePhonePad;
                    _detailField.textLimitInputType = LTTextLimitInputTypeNumber;
                    _detailField.text = _storeModel.serviceMobile;
                    _detailField.enabled = YES;
                    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.width.mas_equalTo(0);
                    }];
                    //                    _detailField.text = _storeModel.storePhone;
                    
                    break;
                case 2:
                    _titleLab.text = @"营业时间";
                    _detailField.placeholder = @"请选择";
                    _detailField.enabled = NO;
                    _detailField.text = [_storeModel.storeTime.shipWeek componentsJoinedByString:@","];
                    
                    break;
                case 1:
                    [_arrowImageView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.width.mas_equalTo(0);
                    }];
                    
                    _titleLab.text = @"店铺简介";
                    _detailField.hidden = YES;
                    _textView.hidden = NO;
                    //                    _textView.placeholder = @"店铺简介";
                    _detailField.enabled = NO;
                    _textView.text = _storeModel.shopIntroduce;
                    
                    break;
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField*)textField{
    if (self.textFieldChangeBlock) {
        self.textFieldChangeBlock(textField.text);
    }
    
}


#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    CGRect rect = [textView.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 130, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil];
    _storeModel.shopIntroduce = textView.text;
    if (self.textViewChange) {
        self.textViewChange(rect.size.height);
    }
}


/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.detailField];
    [self.backView addSubview:self.detailImageView];
    [self.backView addSubview:self.arrowImageView];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.textView];
    [self.backView addSubview:self.detailLab];
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.top.equalTo(self.backView.mas_top).offset(13);
    }];
    [_arrowImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.width.equalTo(@17);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_detailField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left);
        make.left.equalTo(self.titleLab.mas_right).offset(15);
        //        make.centerY.equalTo(self.backView.mas_centerY);
        make.top.equalTo(self.titleLab.mas_top);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left);
        make.left.equalTo(self.titleLab.mas_right).offset(15);
        //        make.centerY.equalTo(self.backView.mas_centerY);
        make.top.equalTo(self.titleLab.mas_top);
    }];
    [_detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImageView.mas_left);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.backView);
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.height.mas_equalTo(@1);
    }];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.left.equalTo(self.titleLab.mas_right).offset(15);
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.top.equalTo(self.titleLab.mas_top).offset(-8);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-5);
        make.width.mas_equalTo(SCREEN_WIDTH - 100);
    }];
    [_titleLab setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];//不可以被压缩，尽量显示完整
    [_detailField setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];//宽度不够时，可以被压缩
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"F4F4F4"];
    }
    return _lineView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}
- (UILabel *)detailLab{
    if (!_detailLab) {
        _detailLab = [UILabel new];
        _detailLab.textColor = color_TextOne;
        _detailLab.font = [UIFont systemFontOfSize:14];
        _detailLab.textAlignment = NSTextAlignmentRight;
    }
    return _detailLab;
}

- (LTLimitTextField *)detailField{
    if (!_detailField) {
        _detailField = [LTLimitTextField new];
        _detailField.textColor = color_TextOne;
        _detailField.font = [UIFont systemFontOfSize:14];
        _detailField.textAlignment = NSTextAlignmentRight;
        _detailField.delegate = self;
        [_detailField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _detailField;
}
- (LTPlaceholderTextView *)textView{
    if (!_textView) {
        _textView = [LTPlaceholderTextView new];
        //        _textView.scrollEnabled = NO;
        _textView.textColor = color_TextOne;
        _textView.placeholderTextColor = [UIColor colorWithHexString:@"#999999"];
        //        _textView.placeholder = @"请输入店铺简介";
        _textView.font = [UIFont systemFontOfSize:14];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.delegate = self;
    }
    return _textView;
}
- (UIImageView *)detailImageView{
    if (!_detailImageView) {
        _detailImageView = [UIImageView new];
    }
    return _detailImageView;
}
- (UIImageView *)arrowImageView{
    if (!_arrowImageView) {
        _arrowImageView = [UIImageView new];
        _arrowImageView.image = [UIImage imageNamed:@"right_arrow_black"];
        _arrowImageView.contentMode = UIViewContentModeRight;
        _arrowImageView.clipsToBounds = YES;
    }
    return _arrowImageView;
}
@end
