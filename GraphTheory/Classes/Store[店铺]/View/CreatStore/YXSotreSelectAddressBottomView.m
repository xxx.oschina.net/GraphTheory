
//
//  YXSotreSelectAddressBottomView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/8.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXSotreSelectAddressBottomView.h"


@interface YXSotreSelectAddressBottomView ()

@property (nonatomic ,strong) UIImageView *bgView;

@property (nonatomic ,strong) UILabel *nameLab;

@property (nonatomic ,strong) UILabel *distanceLab;

@property (nonatomic ,strong) UILabel *addressLab;

@property (nonatomic ,strong) UIButton *saveBtn;

@end

@implementation YXSotreSelectAddressBottomView

- (void)setName:(NSString *)name {
    _name = name;
    _nameLab.text = _name;
}

- (void)setDistance:(NSString *)distance {
    _distance = distance;
    if ([distance isEqualToString:@""]) {
        _distanceLab.text = @"";
    }else {
        _distanceLab.text = [NSString stringWithFormat:@"距您%@公里",_distance];
    }
}

- (void)setAddress:(NSString *)address {
    _address = address;
    _addressLab.text = _address;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bgView];
        [self addSubview:self.nameLab];
        [self addSubview:self.distanceLab];
        [self addSubview:self.addressLab];
        [self addSubview:self.saveBtn];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIImageView *)bgView {
    if (!_bgView) {
        _bgView = [UIImageView new];
        _bgView.backgroundColor = [UIColor whiteColor];
    }
    return _bgView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
//        _nameLab.text = @"信息产业大厦";
        _nameLab.font = [UIFont boldSystemFontOfSize:16.0f];
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)distanceLab {
    if (!_distanceLab) {
        _distanceLab = [[UILabel alloc] init];
//        _distanceLab.text = @"距您19公里";
        _distanceLab.font = [UIFont systemFontOfSize:14.0f];
        _distanceLab.textColor = color_TextOne;
    }
    return _distanceLab;
}

- (UILabel *)addressLab {
    if (!_addressLab) {
        _addressLab = [[UILabel alloc] init];
//        _addressLab.text = @"河南省郑州市";
        _addressLab.font = [UIFont systemFontOfSize:14.0f];
        _addressLab.textColor = color_TextThree;
    }
    return _addressLab;
}

- (UIButton *)saveBtn {
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _saveBtn.layer.masksToBounds = YES;
        _saveBtn.layer.cornerRadius = 4.0f;
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
        [_saveBtn setTitle:@"保存" forState:(UIControlStateNormal)];
        [_saveBtn addTarget:self action:@selector(aciton) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _saveBtn;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15.0);
        make.right.equalTo(@-15.0);
        make.height.equalTo(@16.0);
    }];
    
    [_distanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.top.equalTo(weakSelf.nameLab.mas_bottom).offset(15);
        make.right.equalTo(@-15.0);
        make.height.equalTo(@14.0);
    }];
    
    [_addressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.top.equalTo(weakSelf.distanceLab.mas_bottom).offset(15);
        make.right.equalTo(@-15.0);
        make.height.equalTo(@14.0);
    }];
    
    [_saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.addressLab.mas_bottom).offset(15);
        make.left.equalTo(@15.0);
        make.right.bottom.equalTo(@-15.0);
    }];
}


#pragma mark - Touch Even
- (void)aciton {
    
    if (self.clickSaveBtnBlock) {
        self.clickSaveBtnBlock();
    }
    
}


@end
