//
//  YXSotreSelectAddressBottomView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/8.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXSotreSelectAddressBottomView : DDBaseView

/**
 回调点击保存按钮
 */
@property (nonatomic ,copy)void(^clickSaveBtnBlock)(void);


@property (nonatomic ,strong) NSString *name;
@property (nonatomic ,strong) NSString *distance;
@property (nonatomic ,strong) NSString *address;


@end

NS_ASSUME_NONNULL_END
