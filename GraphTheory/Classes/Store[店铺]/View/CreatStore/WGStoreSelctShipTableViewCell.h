//
//  WGStoreSelctShipTableViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelctShipTableViewCell : DDBaseTableViewCell

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic ,assign) NSInteger type;///<0:选择配送方式 1:选择商品分组
@property (nonatomic, strong) NSString *titleStr;


@end

NS_ASSUME_NONNULL_END
