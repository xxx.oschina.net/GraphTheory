//
//  WGTitleCollectionReusableView.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGTitleCollectionReusableView : UICollectionReusableView
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) UIColor *backColor;

@end

NS_ASSUME_NONNULL_END
