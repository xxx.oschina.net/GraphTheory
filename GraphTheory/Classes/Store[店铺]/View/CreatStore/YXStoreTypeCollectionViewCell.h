//
//  YXStoreTypeCollectionViewCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/8/11.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXStoreTypeCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UIView *backView;
@property (strong, nonatomic) UIView *lineView;
@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UILabel *detailLab;


@end

NS_ASSUME_NONNULL_END
