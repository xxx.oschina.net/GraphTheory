//
//  WGStoreSelctTimeCollectionViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WGStoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelctTimeCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) NSIndexPath *index;
@property (nonatomic, strong) WGStoreOpenModel *lastmodel;
@property (nonatomic, strong) WGStoreOpenModel *model;

@property (nonatomic ,copy) void(^selectStoreTime)(WGStoreOpenModel *model,NSIndexPath *index,NSInteger type);


@end

NS_ASSUME_NONNULL_END
