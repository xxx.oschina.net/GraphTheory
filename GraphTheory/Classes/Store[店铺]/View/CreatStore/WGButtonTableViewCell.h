//
//  WGButtonTableViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/30.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGButtonTableViewCell : DDBaseTableViewCell

@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) UIColor *backColor;


@end

NS_ASSUME_NONNULL_END
