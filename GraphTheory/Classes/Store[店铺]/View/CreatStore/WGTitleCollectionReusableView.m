//
//  WGTitleCollectionReusableView.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGTitleCollectionReusableView.h"

@interface WGTitleCollectionReusableView ()

@property (strong, nonatomic) UILabel *titleLab;

@end

@implementation WGTitleCollectionReusableView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = titleStr;
}
- (void)setBackColor:(UIColor *)backColor{
    _backColor = backColor;
    _titleLab.textColor = backColor;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.titleLab];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).mas_offset(UIEdgeInsetsMake(0, 15, 0, 15));
    }];
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextTwo;
        _titleLab.font = [UIFont systemFontOfSize:12];
        _titleLab.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLab;
}

@end
