//
//  WGStoreSelectAddressViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectAddressViewController.h"
#import <BaiduMapAPI_Base/BMKBaseComponent.h>
#import <BaiduMapAPI_Map/BMKMapComponent.h>
#import <BaiduMapAPI_Map/BMKMapView.h>
#import <BMKLocationkit/BMKLocationComponent.h>
#import <BMKLocationKit/BMKLocationAuth.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <BaiduMapAPI_Utils/BMKUtilsComponent.h>

#import "YZLocationManager.h"

#import "YXSotreSelectAddressBottomView.h"

@interface WGStoreSelectAddressViewController ()<BMKMapViewDelegate,BMKLocationManagerDelegate,BMKLocationAuthDelegate,BMKGeoCodeSearchDelegate>

@property (nonatomic, strong) BMKMapView *mapView;
@property (nonatomic, strong) BMKUserLocation *userLocation;
@property (nonatomic, strong) BMKLocationManager *locationManager;
@property (nonatomic, strong) BMKGeoCodeSearch *search;

/** 定位按钮*/
@property (nonatomic, strong) UIButton *positionBtn;
/** 底部视图*/
@property (nonatomic, strong) YXSotreSelectAddressBottomView *bottomView;
/** 标注*/
@property (nonatomic, strong) BMKPointAnnotation * pointAnnotation;
/** 标注的经纬度*/
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
/** 存储两个经纬度之间的距离*/
@property (nonatomic, strong) NSString *distance;
/** 存储地址*/
@property (nonatomic, strong) NSString *address;
/** 存储经纬度*/
@property (nonatomic, strong) NSString *longitude;

@end

@implementation WGStoreSelectAddressViewController

- (BMKMapView *)mapView {
    if (!_mapView) {
        _mapView = [[BMKMapView alloc]initWithFrame:self.view.bounds];
        _mapView.delegate = self;
        _mapView.showsUserLocation = YES;
        _mapView.userTrackingMode = BMKUserTrackingModeNone;
        [_mapView setZoomLevel:14];//将当前地图显示缩放等级设置为14级
   
    }
    return _mapView;
}

- (BMKUserLocation *)userLocation {
    if (!_userLocation) {
        _userLocation = [[BMKUserLocation alloc] init];
    }
    return _userLocation;
}

- (BMKLocationManager *)locationManager{
    if (!_locationManager) {
        //初始化实例
        _locationManager = [[BMKLocationManager alloc] init];
        //设置delegate
        _locationManager.delegate = self;
        //设置返回位置的坐标系类型
        _locationManager.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //设置应用位置类型
        _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        //设置是否自动停止位置更新
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        //设置是否允许后台定位
        //    bmklocationManager.allowsBackgroundLocationUpdates = YES;
        //设置位置获取超时时间
        _locationManager.locationTimeout = 10;
        //设置获取地址信息超时时间
        _locationManager.reGeocodeTimeout = 10;
        
        // 单次定位
        [self positionMethods];

    }
    return _locationManager;
}
- (BMKGeoCodeSearch *)search {
    if (!_search) {
        _search = [[BMKGeoCodeSearch alloc] init];
        _search.delegate = self;
    }
    return _search;
}
- (UIButton *)positionBtn {
    if (!_positionBtn) {
        _positionBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_positionBtn setBackgroundImage:[UIImage imageNamed:@"store_position"] forState:(UIControlStateNormal)];
        [_positionBtn addTarget:self action:@selector(positionMethods) forControlEvents:UIControlEventTouchUpInside];
    }
    return _positionBtn;
}

- (YXSotreSelectAddressBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[YXSotreSelectAddressBottomView alloc] initWithFrame:CGRectZero];
        YXWeakSelf
        [_bottomView setClickSaveBtnBlock:^{
            [weakSelf saveAction];
        }];
    }
    return _bottomView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择店铺地址";
    // Do any additional setup after loading the view.
//    BMKMapManager *mapManager = [[BMKMapManager alloc] init];
//    // 如果要关注网络及授权验证事件，请设定generalDelegate参数
//    BOOL ret = [mapManager start:@"CWDLRxGNipQ2MK8Yrofd7Vl3vuFcX4GY"  generalDelegate:nil];
//    if (!ret) {
//        NSLog(@"manager start failed!");
//    }
    [BMKMapManager setCoordinateTypeUsedInBaiduMapSDK: BMK_COORDTYPE_BD09LL];
    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:@"CWDLRxGNipQ2MK8Yrofd7Vl3vuFcX4GY" authDelegate:self];
    [self locationManager];
    
    [self.view addSubview:self.mapView];
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.positionBtn];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(@0.0);
        make.height.equalTo(@163.0);
    }];
    
    [self.positionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15.0);
        make.bottom.equalTo(self.bottomView.mas_top).offset(-12);
        make.size.mas_equalTo(CGSizeMake(24, 24));
    }];
    
    
//    DDWeakSelf;
//    [YZLocationManager sharedLocationManager].location = ^(BMKLocation *location, NSError *error) {
//        if (error) {
//            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
//        }
//        if (!location) {
//            return;
//        }
//        if (!weakSelf.userLocation) {
//            weakSelf.userLocation = [[BMKUserLocation alloc] init];
//        }
////        weakSelf.mapView.showsUserLocation = YES;
//        weakSelf.userLocation.location = location.location;
//        [weakSelf.mapView updateLocationData:weakSelf.userLocation];
//
//        NSString *longitude = [NSString stringWithFormat:@"%f,%f",weakSelf.userLocation.location.coordinate.longitude,weakSelf.userLocation.location.coordinate.latitude];
//        NSString *address = [NSString stringWithFormat:@"%@%@%@%@",location.rgcData.province,location.rgcData.city,location.rgcData.district,location.rgcData.street];
//        if (weakSelf.selectAddressBlock) {
//            weakSelf.selectAddressBlock(longitude, address);
//        }
//
//    };
//    [[YZLocationManager sharedLocationManager] startLocationService];
}



/**
 
 *根据anntation生成对应的View
 *@param mapView 地图View
 *@param annotation 指定的标注
 */
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation {
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
        BMKPinAnnotationView*annotationView = (BMKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.image = [UIImage imageNamed:@"store_mark"];
        annotationView.pinColor = BMKPinAnnotationColorRed;
        annotationView.canShowCallout= YES;      //设置气泡可以弹出，默认为NO
        annotationView.animatesDrop=YES;         //设置标注动画显示，默认为NO
        annotationView.draggable = YES;          //设置标注可以拖动，默认为NO
        return annotationView;
    }
    return nil;
}


/**
 
 *点中底图空白处会回调此接口
 *@param mapView 地图View
 *@param coordinate 空白处坐标点的经纬度
 */
- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate {
    
    self.coordinate = coordinate;
    [self addPointAnnotation];
}


/**
 * 添加标注
 */
- (void)addPointAnnotation{
    
    // 添加一个pointAnnotation
    _pointAnnotation = [[BMKPointAnnotation alloc]init];
    CLLocationCoordinate2D coor;
    coor.latitude = self.coordinate.latitude;
    coor.longitude = self.coordinate.longitude;
    _pointAnnotation.coordinate = coor;

    [_mapView addAnnotation:self.pointAnnotation];
    
    
    //调用发地址编码方法，让其在代理方法onGetReverseGeoCodeResult中输出
    BMKReverseGeoCodeSearchOption *reverseGeoCodeOption = [[BMKReverseGeoCodeSearchOption alloc]init];
    reverseGeoCodeOption.location = self.coordinate;
    // 是否访问最新版行政区划数据（仅对中国数据生效）
    reverseGeoCodeOption.isLatestAdmin = YES;
    BOOL flag = [self.search reverseGeoCode: reverseGeoCodeOption];
    if (flag) {
        NSLog(@"逆geo检索发送成功");
    }  else  {
        NSLog(@"逆geo检索发送失败");
    }
    
    NSLog(@"添加标注的位置: %f,%f",_userLocation.location.coordinate.latitude,_userLocation.location.coordinate.longitude);
    
}

/**
 * 计算两个经纬度之间的距离
 */
- (void)getDistanceMethods {
    
    BMKMapPoint point1 = BMKMapPointForCoordinate(self.coordinate);
    
    BMKMapPoint point2 = BMKMapPointForCoordinate(self.userLocation.location.coordinate);
    
    CLLocationDistance distance = BMKMetersBetweenMapPoints(point1,point2);
    
    NSLog(@"%f",distance);
    
    self.distance = [NSString stringWithFormat:@"%.2f",distance / 1000];
    
}


/**
 反向地理编码检索结果回调
 
 @param searcher 检索对象
 @param result 反向地理编码检索结果
 @param error 错误码，@see BMKCloudErrorCode
 */
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeSearchResult *)result errorCode:(BMKSearchErrorCode)error {
    if (error == BMK_SEARCH_NO_ERROR) {
        //在此处理正常结果
        NSLog(@"%@ - %@ - %@ - %@ - %@", result.addressDetail.province, result.addressDetail.city, result.addressDetail.district, result.addressDetail.streetNumber, result.address);
        
        self.address = [NSString stringWithFormat:@"%@%@%@%@%@%@",result.addressDetail.province, result.addressDetail.city, result.addressDetail.district,result.addressDetail.town,result.addressDetail.streetName,result.addressDetail.streetNumber];
        self.longitude = [NSString stringWithFormat:@"%f,%f",self.coordinate.latitude,self.coordinate.longitude];
        self.pointAnnotation.title = result.address;
        [self getDistanceMethods];
        [self.bottomView setName:result.sematicDescription];
        [self.bottomView setDistance:self.distance];
        [self.bottomView setAddress:self.address];
        
    }
    else {
        NSLog(@"检索失败");
    }
}

#pragma mark - 定位
- (void)positionMethods {
    
    YXWeakSelf
    [_locationManager requestLocationWithReGeocode:YES
                                  withNetworkState:YES
                                   completionBlock:^(BMKLocation * _Nullable location,
                                                     BMKLocationNetworkState state, NSError * _Nullable error) {
                                       if (error) {
                                           NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
                                       }
                                       if (location) {//得到定位信息，添加annotation
                                           
                                           if (location.location) {
                                               NSLog(@"LOC = %@",location.location);
                                           }
                                           if (location.rgcData) {
                                               NSLog(@"rgc = %@",[location.rgcData description]);
                                           }
                                           
                                           weakSelf.userLocation.location = location.location;
                                           [weakSelf.mapView setCenterCoordinate:weakSelf. userLocation.location.coordinate animated:YES];
                                           [weakSelf.mapView updateLocationData:weakSelf.userLocation];
                                           [weakSelf.locationManager stopUpdatingLocation];
                                       }
                                   }];
    
    
}

#pragma mark - 点击保存
- (void)saveAction {
    
    if (self.address.length == 0 && self.longitude.length ==0) {
        return [YJProgressHUD showMessage:@"请选择店铺地址"];
    }
    
    if (self.selectAddressBlock) {
        self.selectAddressBlock(self.longitude,self.address);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_mapView viewWillAppear];
    _mapView.delegate = self;
    _locationManager.delegate = self;
    _search.delegate = self;

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_mapView viewWillDisappear];
    _mapView.delegate = nil;
    _locationManager.delegate = nil;
    _search.delegate = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
