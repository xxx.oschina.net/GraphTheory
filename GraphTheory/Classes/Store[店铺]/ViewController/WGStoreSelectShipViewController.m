//
//  WGStoreSelectShipViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectShipViewController.h"
#import "WGStoreSelctShipTableViewCell.h"

@interface WGStoreSelectShipViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSArray *titleArr;


@end

@implementation WGStoreSelectShipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = @"配送方式";
    _titleArr = @[@"到店",@"上门",@"快递"];
    if (!_selectArr) {
        _selectArr = [NSMutableArray new];
        [_selectArr addObjectsFromArray:@[@"到店",@"上门"]];
    }
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
}
- (void)back{
    if (self.selectStoreShipMethod) {
        NSMutableSet *set1 = [NSMutableSet setWithArray:_selectArr];
        NSMutableSet *set2 = [NSMutableSet setWithArray:_titleArr];
        [set1 intersectSet:set2];
        self.selectStoreShipMethod([set1 allObjects]);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WGStoreSelctShipTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelctShipTableViewCell" forIndexPath:indexPath];
    cell.titleStr = _titleArr[indexPath.row];
    cell.isSelect = [_selectArr containsObject:_titleArr[indexPath.row]];
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_selectArr containsObject:_titleArr[indexPath.row]]) {
        [_selectArr removeObject:_titleArr[indexPath.row]];
    }else{
        [_selectArr addObject:_titleArr[indexPath.row]];
    }
    [_tableView reloadData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.separatorColor = color_LineColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[WGStoreSelctShipTableViewCell class] forCellReuseIdentifier:@"WGStoreSelctShipTableViewCell"];
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
