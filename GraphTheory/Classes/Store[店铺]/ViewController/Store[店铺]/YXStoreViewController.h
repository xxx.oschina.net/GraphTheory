//
//  YXStoreViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 店铺首页
 */
@interface YXStoreViewController : DDBaseViewController

@end

NS_ASSUME_NONNULL_END
