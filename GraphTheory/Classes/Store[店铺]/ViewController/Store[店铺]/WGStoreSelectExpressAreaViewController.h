//
//  WGStoreSelectExpressAreaViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 快递配送区域
 */
@interface WGStoreSelectExpressAreaViewController : DDBaseViewController

@property (nonatomic, assign) BOOL isAll;
@property (nonatomic, strong) NSString *areaData;

@property (nonatomic ,copy) void(^selctExpressArea)(BOOL whetherNationalArea,NSString *areaData);


@end

NS_ASSUME_NONNULL_END
