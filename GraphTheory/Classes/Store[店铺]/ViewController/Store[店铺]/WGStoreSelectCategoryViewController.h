//
//  WGStoreSelectCategoryViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "WGStoreModel.h"

NS_ASSUME_NONNULL_BEGIN

/**
 选择商品分类
 */
@interface WGStoreSelectCategoryViewController : DDBaseViewController

/**
 创建店铺回调
 */
@property (nonatomic ,copy)void(^clickSelectCategoryBlock)(NSString *categoryId,NSString *categoryName);
/**
 新增商品回调
 */
@property (nonatomic ,copy)void(^clickSelectCategoryModelBlock)(WGStoreCategoryModel *model);

@end

NS_ASSUME_NONNULL_END
