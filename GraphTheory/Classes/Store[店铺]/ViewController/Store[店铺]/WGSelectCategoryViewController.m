//
//  WGSelectCategoryViewController.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/6.
//  Copyright © 2020 杨旭. All rights reserved.
//





#import "WGSelectCategoryViewController.h"
#import "DDBaseView.h"
#import "STDPickerView.h"
#import "WGStoreModel.h"
@interface WGMultiSelectView : DDBaseView
@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *selectBtn;
@property (nonatomic, strong) WGStoreCategoryModel *model;

@end

@implementation WGMultiSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)selectCatogory{
    _selectBtn.selected = !_selectBtn.selected;
    _model.isSelect = _selectBtn.selected;
}
- (void)setModel:(WGStoreCategoryModel *)model{
    _model = model;
    _selectBtn.selected = model.isSelect;
    _titleLab.text = model.title;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.selectBtn];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.clipsToBounds = YES;
    }
    return _backView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"主体类型";
    }
    return _titleLab;
}
- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        
        [_selectBtn setImage:[UIImage imageNamed:@"advert_check_unselect"] forState:(UIControlStateNormal)];
        [_selectBtn setImage:[UIImage imageNamed:@"advert_check_select"] forState:(UIControlStateSelected)];
        _selectBtn.userInteractionEnabled = NO;
        //        [_selectBtn addTarget:self action:@selector(selectCatogory) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _selectBtn;
}

@end


@interface WGSelectCategoryViewController ()<STDPickerViewDataSource,STDPickerViewDelegate>

@property (strong, nonatomic) STDPickerView *pickerView;
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, assign) NSInteger selectIndex;
@property (nonatomic, strong) WGStoreCategoryModel *leave1;
@end

@implementation WGSelectCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_maxSelectCount == 0) {
        _maxSelectCount = 1;
    }
    CGFloat h = 273;
    self.title = @"主营类目";
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithCustomView:self.saveBtn];
    self.navigationItem.rightBarButtonItem = right;
    UIBarButtonItem *left = [[UIBarButtonItem alloc]initWithCustomView:self.closeBtn];
    self.navigationItem.leftBarButtonItem = left;
    [self.view layoutIfNeeded];
    
    self.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
    self.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
    [self.navigationController.view layoutIfNeeded];
    self.navigationController.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
    self.navigationController.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
    [self.view addSubview:self.pickerView];
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            
        }else{
            make.edges.equalTo(self.view);
        }
    }];
    
    
}
- (void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)save{
    if (_selectArr.count == 0) {
        KPOP(@"请选择主营类目");
        return;
    }
    
    if (self.selectCategory) {
        self.selectCategory(_selectArr);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - STDPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(STDPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return _categorys.count;
    }else if (component == 1){
        WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
        return leave1.children.count;
    }
    return 0;
}

- (UIView *)pickerView:(STDPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (component == 0 || _maxSelectCount == 1) {
        UILabel *label = (UILabel *)view;
        if (!label) {
            label = [[UILabel alloc]init];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = color_TextOne;
            label.font = [UIFont systemFontOfSize:14];
            // 字体自适应属性
            label.adjustsFontSizeToFitWidth = YES;
            // 自适应最小字体缩放比例
            label.minimumScaleFactor = 0.5f;
        }
        
        if (component == 0) {
            WGStoreCategoryModel *leave1 = _categorys[row];
            label.text = leave1.title;
        }else{
            WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
            WGStoreCategoryModel *leave2 = leave1.children[row];
            label.text = leave2.title;
        }
        return label;
    }else if (component == 1 && _maxSelectCount > 1){
        WGMultiSelectView *selectView = (WGMultiSelectView *)view;
        if (!selectView) {
            selectView = [[WGMultiSelectView alloc]init];
            selectView.backgroundColor = [UIColor clearColor];
        }
        WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
        WGStoreCategoryModel *leave2 = leave1.children[row];
        selectView.model = leave2;
        return selectView;
    }
    return view;
}

- (CGFloat)pickerView:(STDPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 45;
}

- (void)pickerView:(STDPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"%ld - %ld",row ,component);
    
    if (component == 0) {
        _selectIndex = row;
        [pickerView reloadAllComponents];
    }
    if (_maxSelectCount == 1 ) {
         WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
         WGStoreCategoryModel *leave2 = leave1.children[row];
         leave2.isSelect = YES;
         [self.selectArr removeAllObjects];
         [self.selectArr addObject:leave2];
     }
//    if (_maxSelectCount == 1 && component == 1) {
//        WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
//        WGStoreCategoryModel *leave2 = leave1.children[row];
//        leave2.isSelect = YES;
//        [self.selectArr removeAllObjects];
//        [self.selectArr addObject:leave2];
//    }

}
- (void)pickerView:(STDPickerView *)pickerView didOnlySelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (_maxSelectCount > 1 && component == 1) {
        int i = 0;
        WGStoreCategoryModel *leave1 = _categorys[_selectIndex];
        WGStoreCategoryModel *leave2 = leave1.children[row];
        if (leave2.isSelect == YES) {
            leave2.isSelect = NO;
            if ([self.selectArr containsObject:leave2]) {
                [self.selectArr removeObject:leave2];
            }
            [pickerView reloadComponent:1];
        }else{
            for (WGStoreCategoryModel *tem1 in _categorys) {
                for (WGStoreCategoryModel *tem2 in tem1.children) {
                    if (tem2.isSelect) {
                        i++;
                    }
                }
            }
            if (i < _maxSelectCount) {
                if ([self.selectArr containsObject:leave2]) {
                    [self.selectArr removeObject:leave2];
                }else{
                    [self.selectArr addObject:leave2];
                }
                leave2.isSelect = !leave2.isSelect;
                [pickerView reloadComponent:1];
            }else{
                NSString *tip = [NSString stringWithFormat:@"最多选择%ld个",(long)_maxSelectCount];
                KPOP(tip);
            }
        }
        
        
        
    }
}
- (NSMutableArray *)selectArr{
    if (!_selectArr) {
        _selectArr = [NSMutableArray new];
    }
    return _selectArr;
}
- (STDPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[STDPickerView alloc] init];
        
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        _pickerView.forceItemTypeText = NO;
        _pickerView.selectionIndicatorStyle = STDPickerViewSelectionIndicatorStyleDefault;
        _pickerView.showVerticalDivisionLine = NO;
//        _pickerView.spacingOfComponents = 30;
        
        _pickerView.textColor = [UIColor colorWithHexString:@"#333333" alpha:0.6];
        _pickerView.selectedTextColor = color_TextOne;
        _pickerView.font = [UIFont systemFontOfSize:14];
    }
    return _pickerView;
}
- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _closeBtn.frame = CGRectMake(0, 0, 44, 44);
        [_closeBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_closeBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_closeBtn addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.frame = CGRectMake(0, 0, 44, 44);
        [_saveBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:APPTintColor forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBtn;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
