//
//  YXStoreViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXStoreViewController.h"
#import "WGMineSetionBackLayout.h"
#import "WGTodayHeadCollectionViewCell.h"
#import "WGTodayDetailCollectionViewCell.h"
#import "WGStoreOrderCollectionViewCell.h"
#import "WGStroeHeadCollectionReusableView.h"
#import "WGStoreMenuCollectionViewCell.h"
#import "WGHomeBannerCollectionViewCell.h"
#import "YXStoreActivityCollectionViewCell.h"
#import "WGLoginViewController.h"

//#import "ZLJGoodsVC.h" // 商品列表
#import "YCMenuView.h"
#import "WGCreatStoreViewController.h"
//#import "WGCreatStoreOneViewController.h"

#import "YXStoreViewModel.h"
#import "YXMarketViewModel.h"
#import "WGStoreModel.h"
#import "YXActivityModel.h"
#import "ZLJShopViewModle.h"
//#import "WGIMManage.h"
//#import "YXLocationManager.h"
#import "YXVersion.h"

#import "WGOrderViewController.h"
#import "DDScanViewController.h"
#import "DDBaseNavigationController.h"
#import "WGOrderViewController.h"
#import "YXSalesAfterController.h"
#import "UIScrollView+DRRefresh.h"
@interface YXStoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,WGCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIButton *storeBtn;
@property (strong, nonatomic) UIButton *scanBtn;

@property (strong, nonatomic) NSMutableArray *storeArr;

@property (strong, nonatomic) DDStoreDataModel *storeDataModel;
@property (strong, nonatomic) WGStoreModel *storeModel;
@property (strong, nonatomic) WGStoreDetailNumModel *storeDetailModel;

@property (nonatomic, strong) NSMutableArray *activityArr;

@end

@implementation YXStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.navigationController.navigationBar.translucent = NO;
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:self.storeBtn];
      self.navigationItem.leftBarButtonItem = leftItem;
      UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.scanBtn];
      self.navigationItem.rightBarButtonItem = rightItem;
    
        
    [self loadSubViews];
    
}

- (void)loadSubViews {
    
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.edges.mas_equalTo(self.view).mas_offset(UIEdgeInsetsMake(0, 15, 0, 15));
        }
    }];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
        
    if ([YXUserInfoManager getUserInfo].personalState == 2) {
        if (self.storeModel==nil) {
            [self queryShopDetailQueryMethod];
        }else{
            [self loadShopDetaiNum];
        }
    }
    [self queryCustomActivity];

}

- (void)userChangeUpDate{
    [self queryShopDetailQueryMethod];
}
#pragma mark - 请求数据
- (void)queryCustomActivity {
    YXWeakSelf
    [YXMarketViewModel queryCustomActivityWithPageNum:@"1" PageSize:@"5" ActivityName:nil Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf.activityArr removeAllObjects];
            weakSelf.activityArr = [YXActivityModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
}
- (void)loadShopDetaiNum{
    if (!_storeModel) {
        [YJProgressHUD showLoading:@""];
    }
    DDWeakSelf
    [ZLJShopViewModle queryOrderNumberShopId:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        [weakSelf.collectionView headerEndRefreshing];

        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            weakSelf.storeDetailModel = [WGStoreDetailNumModel mj_objectWithKeyValues:model.data];
            [weakSelf.collectionView reloadData];
        }else{
//            [YJProgressHUD showError:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [weakSelf.collectionView headerEndRefreshing];
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
        
    }];
}
#pragma mark - 点击导航栏按钮事件
- (void)changeStore{
    if (self.storeModel == nil) {
        WGCreatStoreViewController *creat = [WGCreatStoreViewController new];
        [self.navigationController pushViewController:creat animated:YES];
    }else if (self.storeArr.count) {
        YCMenuView *view = [YCMenuView menuWithActions:self.storeArr width:140 relyonView:self.storeBtn];
        [view show];
    }
}
- (void)scan{

    NSLog(@"扫描");
    DDWeakSelf;
    DDScanViewController *scan = [DDScanViewController new];
    scan.type = DDSCANTYPEALL;
    scan.scanSuccess = ^(UIViewController * _Nonnull vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    DDBaseNavigationController *nav = [[DDBaseNavigationController alloc]initWithRootViewController:scan];
    [weakSelf presentViewController:nav animated:YES completion:nil];
    
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    if (section == 0){
        return 8;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return 3;
    }else if (section == 3) {
        return self.activityArr.count;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section == 0){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 74);
    }else if (indexPath.section == 1){
        return CGSizeMake((SCREEN_WIDTH - 30), 100);
    }else if (indexPath.section == 2){
        return CGSizeMake((SCREEN_WIDTH - 30) / 3, 80);
    }else if (indexPath.section == 3){
        return CGSizeMake((SCREEN_WIDTH - 30), 36);
    }
    
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    if (section == 0){
        return UIEdgeInsetsMake(10, 0, 10, 0);
    }else if (section == 1){
        return UIEdgeInsetsMake(0, 0, 10, 0);
    }else if (section == 2){
        return UIEdgeInsetsMake(0, 0, 10, 0);
    }else if (section == 3) {
        return UIEdgeInsetsMake(0, 0, 10, 0);
    }
    
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 2||section == 3) {
        return CGSizeMake(KWIDTH - 30, 44);
    }else if (section == 0|| section == 1){
        return CGSizeMake(KWIDTH - 30, 0);
    }
    return CGSizeZero;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section{
    if (section == 1) {
        return 1;
    }else if (section == 0 || section == 2||section == 3){
        return 2;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 2||indexPath.section == 3) {
            WGStroeHeadCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGStroeHeadCollectionReusableView" forIndexPath:indexPath];
            header.index = indexPath;
            [header setClickCheckBtnBlock:^{
//                YXFlashSaleViewController *vc = [[YXFlashSaleViewController alloc] init];
//                vc.type = MarketingTypeStateActivity;
//                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
            return header;
        }
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0){
        WGStoreMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreMenuCollectionViewCell" forIndexPath:indexPath];
             cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 1){
        WGHomeBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGHomeBannerCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }else if (indexPath.section == 2){
        WGStoreOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        cell.detail = _storeDetailModel;
        return cell;
    }else if (indexPath.section == 3){
        YXStoreActivityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXStoreActivityCollectionViewCell" forIndexPath:indexPath];
        cell.acitvityModel = self.activityArr[indexPath.row];
        return cell;
    }
    
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        WGMineSetionBackLayout * layout = [[WGMineSetionBackLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;

        [_collectionView setRefreshWithHeaderBlock:^{
            if (weakSelf.storeModel == nil) {
                [weakSelf loadShopDetaiNum];
            }else{
                [weakSelf queryShopDetailQueryMethod];
            }
        } footerBlock:nil];
        
        [_collectionView registerClass:[WGHomeBannerCollectionViewCell class] forCellWithReuseIdentifier:@"WGHomeBannerCollectionViewCell"];
        [_collectionView registerClass:[YXStoreActivityCollectionViewCell class] forCellWithReuseIdentifier:@"YXStoreActivityCollectionViewCell"];
        
        [_collectionView registerClass:[WGStoreOrderCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell"];
        [_collectionView registerClass:[WGStoreMenuCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreMenuCollectionViewCell"];
        [_collectionView registerClass:[WGStroeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGStroeHeadCollectionReusableView"];
        
    }
    return _collectionView;
}

- (UIButton *)storeBtn{
    if (!_storeBtn) {
        _storeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _storeBtn.frame = CGRectMake(0, 0, 44, 44);
        [_storeBtn setImage:[UIImage imageNamed:@"store_home_scan"] forState:UIControlStateNormal];
        [_storeBtn addTarget:self action:@selector(changeStore) forControlEvents:UIControlEventTouchUpInside];

    }
    return _storeBtn;
}
- (UIButton *)scanBtn{
    if (!_scanBtn) {
        _scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _scanBtn.frame = CGRectMake(0, 0, 44, 44);
        [_scanBtn setImage:[UIImage imageNamed:@"store_home_scan"] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _scanBtn;
}
- (NSMutableArray *)storeArr{
    if (!_storeArr) {
        _storeArr = [NSMutableArray new];
    }
    return _storeArr;
}
- (NSMutableArray *)activityArr {
    if (!_activityArr) {
        _activityArr = [NSMutableArray array];
    }
    return _activityArr;
}

#pragma mark - Network Methods
- (void)queryShopDetailQueryMethod{
    if (!_storeModel) {
        [YJProgressHUD showLoading:@""];
    }

    DDWeakSelf
    [YXStoreViewModel queryShopDetailQueryCompletion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            weakSelf.storeDataModel = [DDStoreDataModel mj_objectWithKeyValues:responesObj[@"data"]];
            [YXUserInfoManager shareUserInfoManager].storModel = weakSelf.storeDataModel.storeModel;
            weakSelf.storeModel = weakSelf.storeDataModel.storeModel;
            [weakSelf.storeModel setShopId:weakSelf.storeModel.kId];
            [weakSelf.storeBtn setTitle:weakSelf.storeModel.shopName forState:(UIControlStateNormal)];
            
            // 保存店铺id
            if (weakSelf.storeDataModel.storeModel.kId.length > 0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"shopId":weakSelf.storeDataModel.storeModel.kId?weakSelf.storeDataModel.storeModel.kId:@""}];
            }
                    
//            [[YXLocationManager shareLocation] queryProvinceWithLongitude:weakSelf.storeModel.longitude];
//            [YXLocationManager shareLocation].bmkDelegate = weakSelf;
            
            [weakSelf loadShopDetaiNum];
        
            
//            [WGIMManage dd_IM_LoginIMWithShopModel:[YXUserInfoManager shareUserInfoManager].storModel Success:^(id  _Nonnull data, NSString * _Nonnull msg) {
//                
//            } fail:^(id  _Nonnull data, NSInteger errorType, NSString * _Nonnull msg) {
//                
//            }];
            
        }else {
            
            weakSelf.storeModel = nil;
            weakSelf.storeDetailModel = nil;
            [YXUserInfoManager shareUserInfoManager].storModel = nil;
            [weakSelf.storeBtn setTitle:@"创建店铺" forState:UIControlStateNormal];

        }
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView headerEndRefreshing];
    } Failure:^(NSError * _Nonnull error) {
        [weakSelf.collectionView headerEndRefreshing];
        [YJProgressHUD hideHUD];
    }];
    
}

#pragma mark - YXBMKLocationManagerDelegate
- (void)userLocationProvince:(NSString *)province city:(NSString *)city county:(NSString *)county {
    
    self.storeModel.province = [NSString stringWithFormat:@"%@",province];
    self.storeModel.city = [NSString stringWithFormat:@"%@",city];
    self.storeModel.county = [NSString stringWithFormat:@"%@",county];
}


@end
