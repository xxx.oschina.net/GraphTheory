//
//  WGStoreSelectExpressAreaViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectExpressAreaViewController.h"
#import "WGStoreSelectExpressAreaTableViewCell.h"
#import "YXStoreViewModel.h"
#import "WGAreaModel.h"


@interface WGStoreSelectExpressAreaViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *saveBtn;

@property (nonatomic, strong) NSMutableArray *titleArr;
@property (nonatomic, strong) NSMutableArray *selctArr;



@end

@implementation WGStoreSelectExpressAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = @"快递配送区域";
    if ([YXUserInfoManager shareUserInfoManager].allArea) {
        NSMutableArray *temArr = [WGAreaModel mj_objectArrayWithKeyValuesArray:[YXUserInfoManager shareUserInfoManager].allArea];
        [self handleAreaData:temArr];
    }else{
        [self requsetArea:@"0" currentIndex:0];
    }
    [self.view addSubview:self.saveBtn];
    [self.view addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-15);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.bottom.equalTo(self.view.mas_bottom).offset(-15);
            make.left.equalTo(self.view.mas_left).offset(15);
            make.right.equalTo(self.view.mas_right).offset(-15);
        }
        make.height.equalTo(@44.0);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.top.equalTo(self.view.mas_top);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
        }
        make.bottom.equalTo(self.saveBtn.mas_top);
    }];
    
}
- (void)save{
    if (self.selctExpressArea) {
        if (_isAll) {
            self.selctExpressArea(NO, @"");
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSMutableArray *temArr = [NSMutableArray new];
            for (int i = 1; i < _titleArr.count; i ++) {
                WGAreaModel *temArea = _titleArr[i];
                if (temArea.isSelect) {
                    NSDictionary *dic = @{@"areaId":temArea.areaId,@"areaName":temArea.areaName};
                    [temArr addObject:dic];
                }else{
                    for (WGAreaModel *next in temArea.areaData) {
                        if (next.isSelect) {
                            NSDictionary *dic = @{@"areaId":next.areaId,@"areaName":next.areaName};
                            [temArr addObject:dic];
                        }
                    }
                }
            }
            if (temArr.count == 0) {
                KPOP(@"请选择配送区域");
            }else{
                self.selctExpressArea(YES, [temArr mj_JSONString]);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}
- (void)setAreaData:(NSString *)areaData{
    _areaData = areaData;
    NSArray *tem = [NSDictionary mj_objectArrayWithKeyValuesArray:areaData];
    for (NSDictionary *dic in tem) {
        [self.selctArr addObject:dic[@"areaId"]];
    }
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    WGAreaModel *temArea = (WGAreaModel *)_titleArr[section];
    if (temArea.isOpen) {
        return temArea.areaData.count + 1;
    }
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.titleArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf
    WGStoreSelectExpressAreaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelectExpressAreaTableViewCell" forIndexPath:indexPath];
    cell.select = ^{
        if (indexPath.section == 0) {
            weakSelf.isAll = !weakSelf.isAll;
            for (int i = 1; i < weakSelf.titleArr.count; i ++) {
                WGAreaModel *temArea = weakSelf.titleArr[i];
                temArea.isSelect = weakSelf.isAll;
                for (WGAreaModel *next in temArea.areaData) {
                    next.isSelect = weakSelf.isAll;
                }
            }
            [weakSelf.tableView reloadData];
        }else{
            WGAreaModel *temArea = weakSelf.titleArr[indexPath.section];
            if (indexPath.row == 0) {
                temArea.isSelect = !temArea.isSelect;
                if (!temArea.isSelect) {
                    weakSelf.isAll = NO;
                }
                for (WGAreaModel *next in temArea.areaData) {
                    next.isSelect = temArea.isSelect;
                }
                [weakSelf.tableView reloadData];
            }else{
                WGAreaModel *nexrArea = temArea.areaData[indexPath.row - 1];
                nexrArea.isSelect = !nexrArea.isSelect;
                if (!nexrArea.isSelect) {
                    weakSelf.isAll = NO;
                }
                temArea.isSelect = [weakSelf isAllselect:temArea];
                [weakSelf.tableView reloadData];
            }
        }
    };
    if (indexPath.section == 0) {
        cell.titleStr = @"全国";
        cell.isSelect = _isAll;
    }else if (indexPath.section > 0){
        WGAreaModel *temArea = _titleArr[indexPath.section];
        if (indexPath.row == 0) {
            cell.titleStr = temArea.areaName;
            cell.isSelect = temArea.isSelect;
        }else{
            WGAreaModel *nexrArea = temArea.areaData[indexPath.row - 1];
            NSArray *tem = [nexrArea.areaName componentsSeparatedByString:@"-"];
            cell.titleStr = tem.lastObject;
            cell.isSelect = nexrArea.isSelect;
        }
    }
    cell.isFirst = indexPath.row == 0;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        _isAll = !_isAll;
        for (int i = 1; i < _titleArr.count; i ++) {
            WGAreaModel *temArea = _titleArr[i];
            temArea.isSelect = _isAll;
            for (WGAreaModel *next in temArea.areaData) {
                next.isSelect = _isAll;
            }
        }
        [self.tableView reloadData];
    }else{
        WGAreaModel *temArea = _titleArr[indexPath.section];
        if (indexPath.row == 0) {
            temArea.isOpen = !temArea.isOpen;
            [self.tableView reloadData];
            
            //            if (temArea.areaData.count == 0 && !temArea.isZXS) {
            //                [self requsetArea:temArea.areaId currentIndex:indexPath.section];
            //            }else{
            //                [self.tableView reloadData];
            //            }
        }else{
            WGAreaModel *nexrArea = temArea.areaData[indexPath.row - 1];
            nexrArea.isSelect = !nexrArea.isSelect;
            if (!nexrArea.isSelect) {
                _isAll = NO;
            }
            temArea.isSelect = [self isAllselect:temArea];
            [self.tableView reloadData];
        }
    }
}
- (BOOL)isAllselect:(WGAreaModel *)model{
    for (WGAreaModel *next in model.areaData) {
        if (next.isSelect == NO) {
            return NO;
        }
    }
    return YES;
    
}
- (void)requsetArea:(NSString *)areaId currentIndex:(NSInteger)current{
    [YJProgressHUD showLoading:@""];
    DDWeakSelf
    [YXStoreViewModel getAreaDataAreaId:areaId Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSMutableArray *temArr = [WGAreaModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
            [YXUserInfoManager shareUserInfoManager].allArea = responesObj[@"data"];
            [weakSelf handleAreaData:temArr];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
}
- (void)handleAreaData:(NSArray *)area{
    for (WGAreaModel *firstArea in area) {
        if (_isAll) {
            firstArea.isSelect = _isAll;
        }else{
            firstArea.isSelect = [self.selctArr containsObject:firstArea.areaId];
        }
        for (WGAreaModel *next in firstArea.areaData) {
            if (firstArea.isSelect) {
                next.isSelect = firstArea.isSelect;
            }else{
                next.isSelect = [self.selctArr containsObject:next.areaId];
                if (next.isSelect) {
                    firstArea.isOpen = YES;
                }
            }
        }
    }
    [self.titleArr addObjectsFromArray:area];
    [self.tableView reloadData];
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.separatorColor = color_LineColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[WGStoreSelectExpressAreaTableViewCell class] forCellReuseIdentifier:@"WGStoreSelectExpressAreaTableViewCell"];
    }
    return _tableView;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.layer.cornerRadius = 4;
        //        _saveBtn.enabled = NO;
        //        _saveBtn.alpha = 0.5;
    }
    return _saveBtn;
}
- (NSMutableArray *)titleArr{
    if (!_titleArr) {
        _titleArr = [NSMutableArray new];
        NSArray *tem = @[@"全国"];
        [_titleArr addObjectsFromArray:tem];
        
    }
    return _titleArr;
}
- (NSMutableArray *)selctArr{
    if (!_selctArr) {
        _selctArr = [NSMutableArray new];
    }
    return _selctArr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
