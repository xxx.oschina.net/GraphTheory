//
//  WGCreatStoreOneViewController.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGCreatStoreOneViewController : DDBaseViewController

// 店铺类型 0:创建 1:重新认证 2:修改店铺信息
@property (nonatomic ,assign) NSInteger type;
// 店铺类型 1:线上商场 2:本地商家
@property (nonatomic ,assign) NSInteger shopType;
// 店铺id
@property (nonatomic, strong) NSString *shopId;
// 店铺信息
@property (nonatomic, strong) WGStoreModel *storeModel;

@property (nonatomic, strong) DDStoreDataModel *storeDataModel;

@property (nonatomic ,copy)void(^updateStoreDetail)(void);

@end

NS_ASSUME_NONNULL_END
