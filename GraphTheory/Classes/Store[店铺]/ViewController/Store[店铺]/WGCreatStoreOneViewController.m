//
//  WGCreatStoreOneViewController.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGCreatStoreOneViewController.h"
#import "WGCreatStoreCollectionViewCell.h"
#import "WGStoreModel.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import "WGStoreSelectShipViewController.h"
#import "WGStoreSelectTimeViewController.h"
#import "WGStoreSelectExpressAreaViewController.h"
#import "WGStoreSelectCategoryViewController.h"
//#import "WGStoreSelectAddressViewController.h"
#import "NSString+Addition.h"

#import "YXStoreViewModel.h"

#import "WGShopStepCollectionViewCell.h"
#import "WGTitleCollectionReusableView.h"
#import "WGShopSubjectSelectCollectionViewCell.h"
#import "WGShopCategoryCollectionViewCell.h"
#import "WGShopCertificationCollectionViewCell.h"
#import "YXStoreTypeCollectionViewCell.h"
#import "WGCreatStoreTwoViewController.h"
#import "WGHomeNavViewController.h"
#import "WGSelectCategoryViewController.h"
@interface WGCreatStoreOneViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TZImagePickerControllerDelegate>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, assign) CGFloat h;
@property (nonatomic, strong) NSArray *shipArr;
@property (nonatomic, strong) NSArray *categoryArr;

@end

@implementation WGCreatStoreOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    
    if (self.type == 0) {
        self.title = @"创建店铺";
        _h = 0;
        [self initDefaultData];
    }else if (self.type == 1) {
        self.title = @"创建店铺";
        if (_storeModel==nil) {
            [self initDefaultData];
        }
    }else {
        self.title = @"修改店铺信息";
    }
    
    
    [self loadSubViews];
    
    [self requestDefalut];

}

- (void)initDefaultData {
    
    _storeModel = [WGStoreModel new];
    _storeModel.serviceMobile = [YXUserInfoManager getUserInfo].mobile;
    _storeModel.subjectType = 1;
    _storeModel.shopType = _shopType;
    _storeModel.distPlatCommon = nil;
    if (_shopType == 1) {
        _storeModel.whetherNationalArea = 0;
    }
}


#pragma mark - 添加子视图
- (void)loadSubViews {
    [self.view addSubview:self.saveBtn];
     [self.view addSubview:self.collectionView];
     if (@available(iOS 11.0, *)) {
         _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
     }else{
         self.automaticallyAdjustsScrollViewInsets = NO;
     }
     [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
         if (@available(iOS 11.0,*)) {
             make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-15);
             make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
             make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
         }else{
             make.bottom.equalTo(self.view.mas_bottom).offset(-15);
             make.left.equalTo(self.view.mas_left).offset(15);
             make.right.equalTo(self.view.mas_right).offset(-15);
         }
         make.height.equalTo(@44.0);
     }];
     [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
         if (@available(iOS 11.0,*)) {
             make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
             make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
             make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
         }else{
             make.top.equalTo(self.view.mas_top);
             make.left.equalTo(self.view.mas_left);
             make.right.equalTo(self.view.mas_right);
         }
         make.bottom.equalTo(self.saveBtn.mas_top);
     }];
    
}

#pragma mark - 点击底部按钮
- (void)save{
    
    if (self.type == 2) { // 保存
        [self updateStoreMethods];
    }else {  // 下一步
        if ([self verifyData:YES]) {
            WGCreatStoreTwoViewController *cer = [WGCreatStoreTwoViewController new];
            self.storeModel.shopId = self.shopId;
            cer.storeModel = _storeModel;
            [self.navigationController pushViewController:cer animated:YES];
        }
    }
    
}
- (void)requestDefalut{
    [YJProgressHUD showLoading:@""];
    DDWeakSelf
    dispatch_group_t group = dispatch_group_create();
    if (_shipArr.count == 0) {
        dispatch_group_enter(group);
        [YXStoreViewModel queryDistByShopType:_shopType IsPlat:_storeModel.distPlatCommon Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                weakSelf.shipArr = [WGStoreShipModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            dispatch_group_leave(group);
                      
        } failure:^(NSError * _Nonnull error) {
            KPOP(REQUESTERR);
            dispatch_group_leave(group);
        }];
    }
    if (_categoryArr.count == 0) {
        dispatch_group_enter(group);
        [YXStoreViewModel queryCategoryByShopType:_shopType Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                NSArray *arr = [WGStoreCategoryModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                for (WGStoreCategoryModel *tem1 in arr) {
                    tem1.serviceName = tem1.title;
                    tem1.serviceId = tem1.key;
                    for (WGStoreCategoryModel *tem2 in tem1.children) {
                        tem2.serviceName = [NSString stringWithFormat:@"%@/%@",tem1.title,tem2.title];
                        tem2.serviceId = [NSString stringWithFormat:@"%@,%@",tem1.key,tem2.key];
                    }
                }
                weakSelf.categoryArr = arr;
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            dispatch_group_leave(group);
        } failure:^(NSError * _Nonnull error) {
            KPOP(REQUESTERR);
            dispatch_group_leave(group);
            
        }];
    }
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
    });
    
    
}


// 修改店铺信息
- (void)updateStoreMethods {
    
    if ([self verifyData:YES]){
        DDWeakSelf
        [YJProgressHUD showLoading:@""];
        dispatch_group_t group = dispatch_group_create();
        __block BOOL isUploadSucess = YES;
        if (_storeModel.storeImage) {
             dispatch_group_enter(group);
             NSString *url = [NSString stringWithFormat:@"%@system/client/sys/oss/fileUpload",kBaseURL];
             NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
             [parameters setValue:@"2" forKey:@"type"];
             [NetWorkTools uploadImagesPOSTWithUrl:url parameters:parameters images:@[_storeModel.storeImage].mutableCopy success:^(id responesObj) {
                 if (REQUESTDATASUCCESS) {
                     weakSelf.storeModel.ossId = responesObj[@"data"][@"id"];
                     weakSelf.storeModel.shopLogo = responesObj[@"data"][@"filepath"];
                 }else {
                     isUploadSucess = NO;
                 }
                 dispatch_group_leave(group);
             } failure:^(NSError *error) {
                 isUploadSucess = NO;
                 dispatch_group_leave(group);
             }];
         }
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [YJProgressHUD hideHUD];
            if (isUploadSucess) {
                [YJProgressHUD showLoading:@""];
                [YXStoreViewModel requestUpdateStoreModel:self.storeModel Completion:^(id  _Nonnull responesObj) {
                    [YJProgressHUD hideHUD];
                    if (REQUESTDATASUCCESS) {
                        KPOP(@"修改成功");
                        if (weakSelf.updateStoreDetail) {
                            weakSelf.updateStoreDetail();
                        }
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }else {
                        [YJProgressHUD showMessage:responesObj[@"msg"]];
                    }
                } failure:^(NSError * _Nonnull error) {
                    [YJProgressHUD hideHUD];
                    KPOP(REQUESTERR);
                }];
            }else {
                [YJProgressHUD hideHUD];
                KPOP(@"上传图片资源失败,请重新提交")
            }
         
        });
    }
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.type == 2 ? 3:2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        if (self.type == 2) {
            return 12;
        }else {
            return 13;
        }
    }else if (section == 2) {
        if (self.type == 2) {
            if (self.storeDataModel.storeModel.subjectType == 0||
                self.storeDataModel.storeModel.subjectType == 1) {
                return 2;
            }else {
                return 3;
            }
        }else {
            return 0;
        }
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (self.type==2) {
            return CGSizeMake(KWIDTH, 0.01);
        }else {
            return CGSizeMake(KWIDTH, 94);
        }
    }else if (indexPath.section == 1){
        if (_shopType == 1 &&(indexPath.row == 7 || indexPath.row == 8)) {
            return CGSizeMake(SCREEN_WIDTH , 0.01);
        }
        if (_shopType == 2) {
            if (indexPath.row == 9) {
                return CGSizeMake(SCREEN_WIDTH , 0.01);
            }
            if (_storeModel.distributionType.length > 0) {
                if ([_storeModel.distributionType containsString:@"3"]) {
                    if (indexPath.row == 7 || indexPath.row == 8) {
                        return CGSizeMake(SCREEN_WIDTH , 44);
                    }
                }else {
                    if (indexPath.row == 7 || indexPath.row == 8) {
                        return CGSizeMake(SCREEN_WIDTH , 0.01);
                    }
                }
            }else {
                if (indexPath.row == 7 || indexPath.row == 8) {
                    return CGSizeMake(SCREEN_WIDTH , 0.01);
                }
            }
        }
        if (indexPath.row == 11 || indexPath.row == 6) {
            return CGSizeMake(KWIDTH, 77);
        }else if (indexPath.row == 1){
            return CGSizeMake(KWIDTH, 63);
        }
        return CGSizeMake(KWIDTH, 44);
        
    }else if (indexPath.section == 2) {
        if (self.type == 2) {
            if (self.storeDataModel.storeModel.subjectType == 0||
                self.storeDataModel.storeModel.subjectType == 1) {
                return CGSizeMake(KWIDTH, 154);
            }else {
                if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
                    return CGSizeMake(KWIDTH, 154);
                }else if (indexPath.row == 3) {
                    return CGSizeMake(KWIDTH, 195);
                }else if (indexPath.row == 4) {
                    return CGSizeMake(KWIDTH, 177);
                }
            }
        }else {
            return CGSizeMake(KWIDTH , 0.01);
        }
    }
    return CGSizeMake(SCREEN_WIDTH , 0.01);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return CGSizeMake(KWIDTH, 44);
    }else if (section == 2) {
        return CGSizeMake(KWIDTH, 44);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 2) {
        return UIEdgeInsetsMake(10, 0, 10, 0);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section == 2) {
        return 10;
    }
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (section == 2) {
        return 10;
    }
    return 0;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1) {
            WGTitleCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGTitleCollectionReusableView" forIndexPath:indexPath];
            header.titleStr = @"基本信息";
            header.backColor = color_TextOne;
            header.backgroundColor = HEX_COLOR(@"#F4F4F4");
            return header;
        }else if (indexPath.section == 2) {
            WGTitleCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGTitleCollectionReusableView" forIndexPath:indexPath];
            header.titleStr = @"认证信息";
            header.backColor = color_TextOne;
            header.backgroundColor = HEX_COLOR(@"#F4F4F4");
            return header;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf;
    if (indexPath.section == 0) {
        
        WGShopStepCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopStepCollectionViewCell" forIndexPath:indexPath];
        cell.step = 0;
        return cell;
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            if (self.type == 2) {
                YXStoreTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXStoreTypeCollectionViewCell" forIndexPath:indexPath];
                cell.titleLab.text = @"主体类型";
                if (self.storeDataModel.storeModel.subjectType == 0||
                    self.storeDataModel.storeModel.subjectType == 1) {
                    cell.detailLab.text = @"个人认证";
                }else {
                    cell.detailLab.text = @"企业认证";
                }
                return cell;
            }else {
                WGShopSubjectSelectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopSubjectSelectCollectionViewCell" forIndexPath:indexPath];
                cell.storeModel = _storeModel;
                [cell setClickTypeBtnBlock:^(NSInteger subjectType) {
                    weakSelf.storeModel = [WGStoreModel new];
                    weakSelf.storeModel.serviceMobile = [YXUserInfoManager getUserInfo].mobile;
                    weakSelf.storeModel.subjectType = subjectType;
                    weakSelf.storeModel.shopType = weakSelf.shopType;
                    weakSelf.storeModel.distPlatCommon = nil;
                    if (weakSelf.shopType == 1) {
                        weakSelf.storeModel.whetherNationalArea = 0;
                    }
                    if (weakSelf.categoryArr.count>0) {
                        for (WGStoreCategoryModel *leave1 in weakSelf.categoryArr) {
                            for (WGStoreCategoryModel *leave2 in leave1.children) {
                                leave2.isSelect = NO;
                            }
                        }
                    }
                    
                    [weakSelf.collectionView reloadData];
                }];
                return cell;
            }
        }
        if (indexPath.row == 12) {
            WGShopCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopCategoryCollectionViewCell" forIndexPath:indexPath];
            return cell;
        }
        WGCreatStoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGCreatStoreCollectionViewCell" forIndexPath:indexPath];
        cell.storeModel = _storeModel;
        cell.shopIndex = indexPath;
        
        [cell setTextFieldChangeBlock:^(NSString * _Nonnull content) {
            if (indexPath.row == 2) {
                weakSelf.storeModel.shopName = content;
                [weakSelf verifyData:NO];
            }else if(indexPath.row == 6){
                weakSelf.storeModel.shopAddress = content;
                [weakSelf verifyData:NO];
            }else if(indexPath.row == 7){
                weakSelf.storeModel.serviceScope = content;
                [weakSelf verifyData:NO];
            }else if(indexPath.row == 10){
                weakSelf.storeModel.serviceMobile = content;
                [weakSelf verifyData:NO];
            }else if(indexPath.row == 11){
                weakSelf.storeModel.shopIntroduce = content;
                [weakSelf verifyData:NO];
            }
        }];
        cell.textViewChange = ^(CGFloat height) {
            weakSelf.h = height;
            [weakSelf verifyData:NO];
            
        };
        return cell;
    }else if (indexPath.section == 2) {
        WGShopCertificationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopCertificationCollectionViewCell" forIndexPath:indexPath];
        cell.indexPath = indexPath;
        cell.storeDataModel = self.storeDataModel;
        return cell;
    }
    return nil;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1
                                                                                                  delegate:self];
            imagePicker.allowCrop = YES;
            imagePicker.cropRect = CGRectMake(0, (kHEIGHT - SCREEN_WIDTH)/2, KWIDTH, KWIDTH);
            [imagePicker setSortAscendingByModificationDate:NO];
            imagePicker.isSelectOriginalPhoto = YES;
            imagePicker.allowPickingVideo = NO;
            imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:imagePicker
                               animated:YES
                             completion:nil];
            
        }else if (indexPath.row == 4){
            if (_shipArr.count == 0) {
                [self requestDefalut];
                return;
            }
            WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
            ship.selectArr = [[NSMutableArray alloc]initWithArray:_storeModel.shioModels];
            ship.titleArr = _shipArr;
            ship.type = 0;
            ship.selectStoreShipMethod = ^(NSArray * _Nonnull arr) {
                weakSelf.storeModel.shioModels = arr;
                NSMutableArray *temShip = [NSMutableArray new];
                for (WGStoreShipModel *ship in arr) {
                    [temShip addObject:ship.Id];
                }
                weakSelf.storeModel.distributionType = [temShip componentsJoinedByString:@","];
                [weakSelf verifyData:NO];
                [weakSelf.collectionView reloadData];
            };
            WGHomeNavViewController *nav = [[WGHomeNavViewController alloc] initWithRootViewController:ship];
            
            [nav popupWithPopType:HWPopTypeSlideInFromBottom dismissType:HWDismissTypeSlideOutToBottom position:HWPopPositionBottom];
        }else if (indexPath.row == 3){
            if (self.type == 2) {
                return KPOP(@"主营类目不允许修改");
            }
            if (_categoryArr.count == 0) {
                [self requestDefalut];
                return;
            }
            if (_shopType == 1) {
                WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
                ship.selectArr = [[NSMutableArray alloc]initWithArray:_storeModel.categorys];
                ship.titleArr = _categoryArr;
                ship.type = 2;
                ship.maxSelectCount = _storeModel.subjectType == 2?5:1;
                ship.selectStoreShipMethod = ^(NSArray * _Nonnull arr) {
                    weakSelf.storeModel.categorys = arr;
                    if ([arr count] > 0) {
                        if ([arr count] == 1) {
                            for (WGStoreCategoryModel *categoryModel in arr) {
                                weakSelf.storeModel.mainCategory = categoryModel.key;
                                weakSelf.storeModel.mainCategoryName = categoryModel.title;
                            }
                        }else if ([arr count] > 1) {
                            NSMutableArray *Ids = [NSMutableArray array];
                            NSMutableArray *names = [NSMutableArray array];
                            for (WGStoreCategoryModel *categoryModel in arr) {
                                [Ids addObject:categoryModel.key];
                                [names addObject:categoryModel.title];
                            }
                            if (Ids.count > 0) {
                                weakSelf.storeModel.mainCategory = [Ids componentsJoinedByString:@";"];
                            }
                            if (names > 0) {
                                weakSelf.storeModel.mainCategoryName = [names componentsJoinedByString:@"/"];
                            }
                        }
    
                    }
                    [weakSelf verifyData:NO];
                    [weakSelf.collectionView reloadData];
                };
                WGHomeNavViewController *nav = [[WGHomeNavViewController alloc] initWithRootViewController:ship];
                
                [nav popupWithPopType:HWPopTypeSlideInFromBottom dismissType:HWDismissTypeSlideOutToBottom position:HWPopPositionBottom];
            }else{
                WGSelectCategoryViewController *ship = [WGSelectCategoryViewController new];
                ship.shopType = self.shopType;
                ship.categorys = _categoryArr;
                ship.selectArr = [[NSMutableArray alloc]initWithArray:_storeModel.categorys];
                ship.maxSelectCount = _storeModel.subjectType == 2?5:1;
                ship.selectCategory = ^(NSMutableArray * _Nonnull arr) {
                    weakSelf.storeModel.categorys = arr;
                    if ([arr count] == 1) {
                        for (WGStoreCategoryModel *categoryModel in arr) {
                            weakSelf.storeModel.mainCategory = categoryModel.serviceId;
                            weakSelf.storeModel.mainCategoryName = categoryModel.serviceName;
                        }
                    }else if ([arr count] > 1) {
                        NSMutableArray *Ids = [NSMutableArray array];
                        NSMutableArray *names = [NSMutableArray array];
                        for (WGStoreCategoryModel *categoryModel in arr) {
                            [Ids addObject:categoryModel.serviceId];
                            [names addObject:categoryModel.serviceName];
                        }
                        if (Ids.count > 0) {
                            weakSelf.storeModel.mainCategory = [Ids componentsJoinedByString:@";"];
                        }
                        if (names > 0) {
                            weakSelf.storeModel.mainCategoryName = [names componentsJoinedByString:@";"];
                        }
                    }
                    [weakSelf verifyData:NO];
                    [weakSelf.collectionView reloadData];
                };
                WGHomeNavViewController *nav = [[WGHomeNavViewController alloc] initWithRootViewController:ship];
                
                [nav popupWithPopType:HWPopTypeSlideInFromBottom dismissType:HWDismissTypeSlideOutToBottom position:HWPopPositionBottom];
            }

            
        }else if (indexPath.row == 5){
           
        }else if (indexPath.row == 8){
            WGStoreSelectTimeViewController *time = [WGStoreSelectTimeViewController new];
            time.timeModel = _storeModel.storeTime;
            time.selectStoreTime = ^(WGStoreTimeModel * _Nonnull model) {
                weakSelf.storeModel.storeTime = model;
                [weakSelf verifyData:NO];
                [weakSelf.collectionView reloadData];
            };
            [self.navigationController pushViewController:time animated:YES];
        }else if (indexPath.row == 9){
            WGStoreSelectExpressAreaViewController *time = [WGStoreSelectExpressAreaViewController new];
            time.isAll = !_storeModel.whetherNationalArea;
            time.areaData = _storeModel.areaData;
            time.selctExpressArea = ^(BOOL whetherNationalArea, NSString * _Nonnull areaData) {
                weakSelf.storeModel.whetherNationalArea = whetherNationalArea;
                weakSelf.storeModel.areaData = areaData;
                [weakSelf.collectionView reloadData];
            };
            //            time.timeModel = _storeModel.storeDoorTime;
            //            time.selectStoreTime = ^(WGStoreTimeModel * _Nonnull model) {
            //                weakSelf.storeModel.storeDoorTime = model;
            //                [weakSelf.collectionView reloadData];
            //            };
            [self.navigationController pushViewController:time animated:YES];
        }
    }
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    UIImage *seleImage = photos[0];
    _storeModel.storeImage = seleImage;
    [self verifyData:NO];
    [self.collectionView reloadData];
}
- (BOOL)verifyData:(BOOL)showMsg{
    NSString *msg = @"";
    BOOL correct = YES;
    if (_storeModel.subjectType == 0){
        msg = @"请选择主体类型";
        correct = NO;
    }else if (_storeModel.shopName.length == 0){
        msg = @"请输入店铺名称";
        correct = NO;
    }else if (_storeModel.mainCategory.length == 0){
        msg = @"请选择主营类目";
        correct = NO;
    }else if (_storeModel.distributionType.length == 0){
        msg = @"请选择配送方式";
        correct = NO;
    }else if (_storeModel.province.length == 0){
        msg = @"请选择店铺地址";
        correct = NO;
    }else if (_storeModel.shopAddress.length == 0){
        msg = @"请输入详细地址";
        correct = NO;
    }  else  if (_storeModel.serviceMobile.length == 0) {
        msg = @"请输入服务电话";
        correct = NO;
    }
    else if (_storeModel.serviceMobile.length > 18) {
        msg = @"请输入不大于18位的服务电话";
        correct = NO;
    }
    if ([_storeModel.distributionType containsString:@"3"]){
        if (_storeModel.serviceScope.length == 0) {
            msg = @"请输入上门服务范围";
            correct = NO;
        }else if (_storeModel.storeTime.shipWeek.count == 0 && _storeModel.storeTime.shipTime.count == 0){
            msg = @"请选择营业时间";
            correct = NO;
        }
    }
    if ([_storeModel.distributionType containsString:@"1"]){
        if (!_storeModel.whetherNationalArea && _storeModel.areaData) {
            msg = @"请选择快递配送区域";
            correct = NO;
        }
    }
    
    if (showMsg) {
        KPOP(msg);
    }
    _saveBtn.enabled = correct;
    _saveBtn.alpha = correct?1:0.5;
    return correct;
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerClass:[WGShopStepCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopStepCollectionViewCell"];
        [_collectionView registerClass:[WGCreatStoreCollectionViewCell class] forCellWithReuseIdentifier:@"WGCreatStoreCollectionViewCell"];
        [_collectionView registerClass:[WGShopCategoryCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopCategoryCollectionViewCell"];
        
        [_collectionView registerClass:[WGShopSubjectSelectCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopSubjectSelectCollectionViewCell"];
        
        [_collectionView registerClass:[WGShopCertificationCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopCertificationCollectionViewCell"];
        [_collectionView registerClass:[YXStoreTypeCollectionViewCell class] forCellWithReuseIdentifier:@"YXStoreTypeCollectionViewCell"];
        
        [_collectionView registerClass:[WGTitleCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGTitleCollectionReusableView"];
    }
    return _collectionView;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.layer.cornerRadius = 4;
        if (_type == 0||_type ==1) {
            _saveBtn.enabled = YES;
            _saveBtn.alpha = 0.5;
            [_saveBtn setTitle:@"下一步" forState:UIControlStateNormal];
        }else if (_type == 2) {
            [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        }
    }
    return _saveBtn;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
