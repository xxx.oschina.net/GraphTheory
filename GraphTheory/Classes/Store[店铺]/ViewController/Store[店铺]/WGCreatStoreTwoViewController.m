//
//  WGCreatStoreTwoViewController.m
//  BusinessFine
//
//  Created by DinDo on 2020/8/5.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "WGCreatStoreTwoViewController.h"
#import "WGStoreModel.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import "NSString+Addition.h"
#import "YXStoreViewModel.h"
#import "WGShopStepCollectionViewCell.h"
#import "WGTitleCollectionReusableView.h"
#import "WGShopCertificationCollectionViewCell.h"
#import "WGPublicBottomView.h"
//#import "WGTakePhotoViewController.h"
#import "YXLonginViewModel.h"
#import "YXUserViewModel.h"
#import "YXMerchantInfoModel.h"
@interface WGCreatStoreTwoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TZImagePickerControllerDelegate>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic, strong) NSIndexPath *selcetIndex;


@end

@implementation WGCreatStoreTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
    _storeModel.picArr = nil;

    self.title = @"创建店铺";
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-15);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.bottom.equalTo(self.view.mas_bottom).offset(-15);
            make.left.equalTo(self.view.mas_left).offset(15);
            make.right.equalTo(self.view.mas_right).offset(-15);
        }
        make.height.equalTo(@44.0);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.top.equalTo(self.view.mas_top);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)commit{
    if (![self verifyData:YES]) {
        return;
    }
    [YJProgressHUD showLoading:@"资料正在提交，请耐心等待~"];
    DDWeakSelf
    dispatch_group_t group = dispatch_group_create();
    __block BOOL isUploadSucess = YES;
    
    if (_storeModel.storeImage) {
        dispatch_group_enter(group);
        NSString *url = [NSString stringWithFormat:@"%@system/client/sys/oss/fileUpload",kBaseURL];
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"2" forKey:@"type"];
        [NetWorkTools uploadImagesPOSTWithUrl:url parameters:parameters images:@[_storeModel.storeImage].mutableCopy success:^(id responesObj) {
            if (REQUESTDATASUCCESS) {
                weakSelf.storeModel.ossId = responesObj[@"data"][@"id"];
                weakSelf.storeModel.shopLogo = responesObj[@"data"][@"filepath"];
            }else {
                isUploadSucess = NO;
            }
            dispatch_group_leave(group);
        } failure:^(NSError *error) {
            isUploadSucess = NO;
            dispatch_group_leave(group);
        }];
    }
    
    if (_storeModel.subjectType == 1) { // 个人认证
        NSArray *temArr = _storeModel.picArr[0];
        for (WGGoodsImageModel *goodImag in temArr) {
            if (goodImag.imagePath.length == 0) {
                dispatch_group_enter(group);
                NSString *url = [NSString stringWithFormat:@"%@system/client/sys/oss/fileUpload",kBaseURL];
                NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@"2" forKey:@"type"];
                [NetWorkTools uploadImagesPOSTWithUrl:url parameters:parameters images:@[goodImag.thumbnail].mutableCopy success:^(id responesObj) {
                    if (REQUESTDATASUCCESS) {
                        goodImag.imagePath = responesObj[@"data"][@"filepath"];
                    }else {
                        isUploadSucess = NO;
                    }
                    dispatch_group_leave(group);
                } failure:^(NSError *error) {
                    isUploadSucess = NO;
                    dispatch_group_leave(group);
                }];
            }
        }
    }else { // 企业认证
        for (NSArray *temArr in _storeModel.picArr) {
            for (WGGoodsImageModel *goodImag in temArr) {
                if (goodImag.imagePath.length == 0) {
                    dispatch_group_enter(group);
                    NSString *url = [NSString stringWithFormat:@"%@system/client/sys/oss/fileUpload",kBaseURL];
                    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
                    [parameters setValue:@"2" forKey:@"type"];
                    [NetWorkTools uploadImagesPOSTWithUrl:url parameters:parameters images:@[goodImag.thumbnail].mutableCopy success:^(id responesObj) {
                        if (REQUESTDATASUCCESS) {
                            goodImag.imagePath = responesObj[@"data"][@"filepath"];
                        }else {
                            isUploadSucess = NO;
                        }
                        dispatch_group_leave(group);
                    } failure:^(NSError *error) {
                        isUploadSucess = NO;
                        dispatch_group_leave(group);
                    }];
                }
            }
        }
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
        if (isUploadSucess) {
            [YJProgressHUD showLoading:@""];
            [YXStoreViewModel requestAddStoreModel:weakSelf.storeModel Completion:^(id  _Nonnull responesObj) {
                [YJProgressHUD hideHUD];
                if (REQUESTDATASUCCESS) {
                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                    [YJProgressHUD showMessage:@"保存成功"];
                }else {
                    [YJProgressHUD showMessage:responesObj[@"msg"]];
                }
            } failure:^(NSError * _Nonnull error) {
                [YJProgressHUD hideHUD];
                KPOP(REQUESTERR);
            }];
        }else{
            [YJProgressHUD hideHUD];
            KPOP(@"上传图片资源失败,请重新提交")
        }
    });
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 2;
    }else if (section == 2){
        return 1;
    }else if (section == 3){
        return 1;
    }else if (section == 4){
        return 1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (_storeModel.subjectType == 1) {
        return 2;
    }else if (_storeModel.subjectType == 2){
        return 3;
    }
    return 2;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH, 94);
    }else if (indexPath.section == 1){
        return CGSizeMake(KWIDTH, 154);
    }else if (indexPath.section == 2){
        return CGSizeMake(KWIDTH, 154);
    }else if (indexPath.section == 3){
        return CGSizeMake(KWIDTH, 195);
    }else if (indexPath.section == 4){
        return CGSizeMake(KWIDTH, 177);
    }
    return CGSizeMake(SCREEN_WIDTH , 0.01);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section != 0) {
        return CGSizeMake(KWIDTH, 44);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section != 0) {
        return UIEdgeInsetsMake(10, 0, 10, 0);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section != 0) {
            NSString *str;
            if (indexPath.section == 1) {
                str = @"证件照";
            }else if (indexPath.section == 2){
                str = @"营业执照";
            }else if (indexPath.section == 3){
                str = @"特殊资质证明";
            }else if (indexPath.section == 4){
                str = @"特种行业经营许可证";
            }
            WGTitleCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGTitleCollectionReusableView" forIndexPath:indexPath];
            header.titleStr = str;
            header.backColor = color_TextOne;
            header.backgroundColor = HEX_COLOR(@"#F4F4F4");
            return header;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf;
    if (indexPath.section == 0) {
        WGShopStepCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopStepCollectionViewCell" forIndexPath:indexPath];
        cell.step = 1;
        return cell;
    }else {
        WGShopCertificationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGShopCertificationCollectionViewCell" forIndexPath:indexPath];
        cell.goodsModel = _storeModel.picArr[indexPath.section - 1][indexPath.row];
        return cell;
    }
    return nil;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    _selcetIndex = indexPath;
    WGGoodsImageModel *goodImage = _storeModel.picArr[indexPath.section - 1][indexPath.row];
    if (indexPath.section == 1) {
//        WGTakePhotoViewController *vc = [WGTakePhotoViewController new];
//        vc.type = indexPath.row;
//        vc.takePhotoBlock = ^(UIImage * _Nonnull image, NSInteger type) {
//            goodImage.thumbnail = image;
////            [weakSelf verifyData:NO];
//            [weakSelf.collectionView reloadData];
//        };
//        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section > 1){
        TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1
                                                                                              delegate:self];
        [imagePicker setSortAscendingByModificationDate:NO];
        imagePicker.isSelectOriginalPhoto = YES;
        imagePicker.allowPickingVideo = NO;
        imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:imagePicker
                           animated:YES
                         completion:nil];
    }
    
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    WGGoodsImageModel *goodImage = _storeModel.picArr[_selcetIndex.section - 1][_selcetIndex.row];
    
    UIImage *seleImage = photos[0];
    goodImage.thumbnail = seleImage;
    
//    [self verifyData:NO];
    [self.collectionView reloadData];
}
- (BOOL)verifyData:(BOOL)showMsg{

    BOOL correct = YES;
    WGGoodsImageModel *goodImage1 = _storeModel.picArr[0][0];
    WGGoodsImageModel *goodImage2 = _storeModel.picArr[0][1];
    WGGoodsImageModel *goodImage3 = _storeModel.picArr[1][0];
//    WGGoodsImageModel *goodImage4 = _storeModel.picArr[2][0];
//    WGGoodsImageModel *goodImage5 = _storeModel.picArr[3][0];

    if (!goodImage1.thumbnail && !goodImage1.imagePath){
        KPOP(@"请上传身份证正面照片");
        return NO;
    }else if (!goodImage2.thumbnail && !goodImage2.imagePath){
        KPOP(@"请上传身份证反面照片");
        return NO;
    }
    if (_storeModel.subjectType == 2) {
        if (!goodImage3.thumbnail && !goodImage3.imagePath){
            KPOP(@"请上传营业执照");
            return NO;
        }
//        if (!goodImage4.thumbnail && !goodImage4.imagePath){
//            KPOP(@"请上传特殊资质证明");
//            return NO;
//        }
//        if (!goodImage5.thumbnail && !goodImage5.imagePath){
//            KPOP(@"请上传特种行业经营许可证");
//            return NO;
//        }
    }

    return correct;
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerClass:[WGShopStepCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopStepCollectionViewCell"];
        [_collectionView registerClass:[WGShopCertificationCollectionViewCell class] forCellWithReuseIdentifier:@"WGShopCertificationCollectionViewCell"];
        [_collectionView registerClass:[WGTitleCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGTitleCollectionReusableView"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@[@"上一步",@"提交审核"]];
        
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (index == 0) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }else {
                [weakSelf commit];
            }
        }];
    }
    return _bottomView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
