//
//  WGStoreSelectShipViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectShipViewController.h"
#import "WGStoreSelctShipTableViewCell.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "WGButtonTableViewCell.h"

@interface WGStoreSelectShipViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic, strong) UIButton *saveBtn;

@end

@implementation WGStoreSelectShipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    [self.view addSubview:self.tableView];

    if (_type == 1) {
        self.title = @"商品分组";
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                make.top.equalTo(self.view.mas_top);
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            }else{
                make.edges.mas_equalTo(self.view);
            }
        }];
    }else{
        CGFloat h = 251;
        self.title = @"配送方式";
        if (_type == 2) {
            self.title = @"主营类目";
            h = 427;
        }
        self.view.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithCustomView:self.closeBtn];
        self.navigationItem.rightBarButtonItem = right;
        
        [self.view layoutIfNeeded];
        
        self.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
        self.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
        [self.navigationController.view layoutIfNeeded];
        self.navigationController.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
        self.navigationController.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
        [self.view addSubview:self.saveBtn];
        [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_bottom);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.height.equalTo(@44.0);
        }];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            }else{
                make.top.equalTo(self.view.mas_top);
                make.left.equalTo(self.view.mas_left);
                make.right.equalTo(self.view.mas_right);
            }
            make.bottom.equalTo(self.saveBtn.mas_top);
        }];
    }
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }

}
- (void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)save{
    if (_selectArr.count == 0) {
        if (_type == 0){
            KPOP(@"请选择配送方式");
        }else if (_type == 2){
            KPOP(@"请选择主营类目");
        }
    }else{
        if (self.selectStoreShipMethod) {
            self.selectStoreShipMethod(_selectArr);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_type == 1 ) {
        return _titleArr.count?_titleArr.count + 1:0;
    }
    return _titleArr.count;

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_type == 1) {
        return 0;
    }
    return 30;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (_type == 0){
        return @"请选择配送方式";
    }else if (_type == 2){
        return @"请选择主营类目";
    }
    return @"";
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = color_TextTwo;
    header.textLabel.font = [UIFont systemFontOfSize:12];
    header.contentView.backgroundColor = HEX_COLOR(@"#F8F8F8");
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _titleArr.count) {
        return 60;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _titleArr.count) {
        WGButtonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGButtonTableViewCell" forIndexPath:indexPath];
        cell.enabled = YES;
        return cell;
        
    }
    WGStoreSelctShipTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelctShipTableViewCell" forIndexPath:indexPath];
    cell.type = _type;
    if (_type == 1) {
        FindItemsGroupDataItem *model = _titleArr[indexPath.row];
        cell.titleStr = model.groupName;
        
    }else if(_type == 0){
        WGStoreShipModel *shipModel = _titleArr[indexPath.row];
        cell.titleStr = shipModel.text;
        cell.isSelect = [_selectArr containsObject:shipModel];
    }else if (_type == 2){
        WGStoreCategoryModel *shipModel = _titleArr[indexPath.row];
        cell.titleStr = shipModel.title;
        cell.isSelect = [_selectArr containsObject:shipModel];
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _titleArr.count) {
        if (self.selectStoreShipMethod) {
            self.selectStoreShipMethod(_selectArr);
        }
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (self.selectGoodsGroup) {
        self.selectGoodsGroup(_titleArr[indexPath.row]);
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if ([_selectArr containsObject:_titleArr[indexPath.row]]) {
        [_selectArr removeObject:_titleArr[indexPath.row]];
    }else{
        if (_selectArr.count >= _maxSelectCount && _maxSelectCount > 0) {
            NSString *tip = [NSString stringWithFormat:@"最多选择%ld个",(long)_maxSelectCount];
            KPOP(tip);
            return;
        }
        [_selectArr addObject:_titleArr[indexPath.row]];
    }
    [_tableView reloadData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.separatorColor = color_LineColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[WGStoreSelctShipTableViewCell class] forCellReuseIdentifier:@"WGStoreSelctShipTableViewCell"];
        [_tableView registerClass:[WGButtonTableViewCell class] forCellReuseIdentifier:@"WGButtonTableViewCell"];
        
        
        //        [_tableView setupEmptyDataText:@"暂无商品分组" tapBlock:nil];
    }
    return _tableView;
}
- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _closeBtn.frame = CGRectMake(0, 0, 44, 44);
        _closeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
        [_closeBtn setImage:[UIImage imageNamed:@"good_close"] forState:(UIControlStateNormal)];
        [_closeBtn addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBtn;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
