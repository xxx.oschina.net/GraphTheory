//
//  WGCreatStoreTwoViewController.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/5.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGCreatStoreTwoViewController : DDBaseViewController

// 店铺信息
@property (nonatomic, strong) WGStoreModel *storeModel;

@end

NS_ASSUME_NONNULL_END
