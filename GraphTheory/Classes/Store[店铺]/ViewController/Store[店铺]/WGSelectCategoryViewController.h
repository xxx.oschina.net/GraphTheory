//
//  WGSelectCategoryViewController.h
//  BusinessFine
//
//  Created by DinDo on 2020/8/6.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import <HWPop.h>

NS_ASSUME_NONNULL_BEGIN
@class WGStoreModel;
@interface WGSelectCategoryViewController : DDBaseViewController

@property (nonatomic, strong) NSArray *categorys;
@property (nonatomic, assign) NSInteger maxSelectCount;
@property (nonatomic ,strong) NSMutableArray *selectArr;
@property (nonatomic ,assign) NSInteger shopType;

@property (nonatomic ,copy) void(^selectCategory)(NSMutableArray *arr);

@end

NS_ASSUME_NONNULL_END
