//
//  WGStoreSelectCategoryViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectCategoryViewController.h"
#import "WGStoreSelectCategoryView.h"

#import "YXStoreViewModel.h"
#import "WGStoreModel.h"
@interface WGStoreSelectCategoryViewController ()

@property (nonatomic, strong) WGStoreSelectCategoryView *category1;
@property (nonatomic, strong) WGStoreSelectCategoryView *category2;

@property (nonatomic, strong) NSMutableArray *categoryArr1;
@property (nonatomic, strong) NSMutableArray *categoryArr2;

@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *categoryName;

@end

@implementation WGStoreSelectCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = @"主营类目";
    if (self.clickSelectCategoryModelBlock) {
        self.title = @"商品分类";
    }
    [self loadDataWithCategoryId:@"" Level:@"1"];
    [self setUpLayout];
    _category1.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    _category1.dataArr = @[@"分组1",@"分组2",@"分组3",@"分组4",@"分组5",@"分组6",@"分组7"];
//    _category2.dataArr = @[@"分组1",@"分组1",@"分组1",@"分组1",@"分组1",@"分组1",@"分组1"];

}

#pragma mark - 请求查询主营分类数据
- (void)loadDataWithCategoryId:(NSString *)categoryId Level:(NSString *)level {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXStoreViewModel queryMainCategoryWithCategoryId:categoryId Level:level Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            if ([level isEqualToString:@"1"]) {
                [weakSelf.categoryArr1 removeAllObjects];
                weakSelf.categoryArr1 = [WGStoreCategoryModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                NSMutableArray *nameArr = [NSMutableArray array];
                for (WGStoreCategoryModel *categoryModel in weakSelf.categoryArr1) {
                    [nameArr addObject:categoryModel.category_name];
                }
                weakSelf.category1.dataArr = nameArr;
                if (weakSelf.categoryArr1.count > 0) {
                    WGStoreCategoryModel *categoryModel= weakSelf.categoryArr1[0];
                    weakSelf.categoryId = categoryModel.category_id;
                    weakSelf.categoryName = categoryModel.category_name;
                    [weakSelf loadDataWithCategoryId:categoryModel.category_id Level:@"2"];
                }
            }else {
                [weakSelf.categoryArr2 removeAllObjects];
                weakSelf.categoryArr2 = [WGStoreCategoryModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                NSMutableArray *nameArr = [NSMutableArray array];
                for (WGStoreCategoryModel *categoryModel in weakSelf.categoryArr2) {
                    [nameArr addObject:categoryModel.category_name];
                }
                weakSelf.category2.dataArr = nameArr;
            }
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
    
}

- (void)setUpLayout{
    [self.view addSubview:self.category1];
    [self.view addSubview:self.category2];
    [_category1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.and.bottom.equalTo(self.view);
        make.width.mas_equalTo(125);
    }];
    [_category2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.right.and.bottom.equalTo(self.view);
        make.left.equalTo(self.category1.mas_right);
    }];

}
- (WGStoreSelectCategoryView *)category1{
    if (!_category1) {
        _category1 = [WGStoreSelectCategoryView new];
        _category1.type = 0;
        
        YXWeakSelf
        [_category1 setSelectCategory:^(NSInteger type, NSIndexPath * _Nonnull indexPath) {
            WGStoreCategoryModel *categoryModel = weakSelf.categoryArr1[indexPath.row];
            weakSelf.categoryId = categoryModel.category_id;
            weakSelf.categoryName = categoryModel.category_name;
            [weakSelf loadDataWithCategoryId:categoryModel.category_id Level:@"2"];
        }];
    }
    return _category1;
}
- (WGStoreSelectCategoryView *)category2{
    if (!_category2) {
        _category2 = [WGStoreSelectCategoryView new];
        _category2.type = 1;
        
        YXWeakSelf
        [_category2 setSelectCategory:^(NSInteger type, NSIndexPath * _Nonnull indexPath) {
            WGStoreCategoryModel *categoryModel = weakSelf.categoryArr2[indexPath.row];
            NSString *categoryId = [NSString stringWithFormat:@"%@,%@",weakSelf.categoryId,categoryModel.category_id];
            NSString *categoryName = [NSString stringWithFormat:@"%@/%@",weakSelf.categoryName,categoryModel.category_name];
            NSLog(@"%@ -- %@",categoryId,categoryName);
            if (weakSelf.clickSelectCategoryBlock) {
                weakSelf.clickSelectCategoryBlock(categoryId, categoryName);
            }
            if (weakSelf.clickSelectCategoryModelBlock) {
                weakSelf.clickSelectCategoryModelBlock(categoryModel);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }
    return _category2;

}

- (NSMutableArray *)categoryArr1 {
    if (!_categoryArr1) {
        _categoryArr1 = [NSMutableArray array];
    }
    return _categoryArr1;
}
- (NSMutableArray *)categoryArr2 {
    if (!_categoryArr2) {
        _categoryArr2 = [NSMutableArray array];
    }
    return _categoryArr2;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
