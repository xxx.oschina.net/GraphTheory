//
//  DDScanViewController.h
//  DDLife
//
//  Created by wanggang on 2019/7/24.
//  Copyright © 2019年 赵越. All rights reserved.
//

#import <LBXScanViewController.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger ,DDSCANTYPE){
    DDSCANTYPEALL,///<所有
    DDSCANTYPECoustomNo,///<消费码
    DDSCANTYPEExpressNO,///<快递单号

};

@interface DDScanViewController : LBXScanViewController

@property(nonatomic, strong) UIViewController *vc;
@property(nonatomic, assign) DDSCANTYPE type;

@property (nonatomic,copy) void(^scanSuccess)(UIViewController *vc);
@property (nonatomic,copy) void(^scanSuccessReslut)(NSString *reslut);
@property (nonatomic,copy) void(^scanCoustomSuccessReslut)(NSString *coustomNo,NSString *orderNO);


@end

NS_ASSUME_NONNULL_END
