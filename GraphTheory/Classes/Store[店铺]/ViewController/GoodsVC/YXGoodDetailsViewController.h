//
//  YXGoodDetailsViewController.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/2.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class WGGoodsInfoModel;
@interface YXGoodDetailsViewController :DDBaseViewController

@property (nonatomic ,strong) WGGoodsInfoModel *goodsInfo;

@property (nonatomic ,strong) NSString *goodsID;

@end

NS_ASSUME_NONNULL_END
