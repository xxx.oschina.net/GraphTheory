//
//  YXGoodDetailsViewController.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/2.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "YXGoodDetailsViewController.h"

//#import "YXHomeSectionView.h"
#import "WGGuiGeView.h"
//#import "WGGoodsDetailBottomView.h"

#import "WGGoodDetailBannerCollectionViewCell.h"
#import "WGGoodDetailsInfoCollectionViewCell.h"
#import "WGGoodDetailMoreCollectionViewCell.h"
//#import "WGGoodDetailTagCollectionViewCell.h"
#import "WGGoodDetailNormalPriceCollectionViewCell.h"
//#import "WGGoodDetailGrouponCollectionViewCell.h"
//#import "WGGoodDetailFlashSaleDetailCollectionViewCell.h"
//#import "WGGoodDetailRemindCollectionViewCell.h"
//#import "WGGoodDetailStoreHeadCollectionReusableView.h"
#import "WGGoodDetailImAndTCollectionReusableView.h"
//#import "WGGoodDetailGrouponCollectionReusableView.h"
#import "WGGoodDetailWebCollectionViewCell.h"
#import "WGEmptyCollectionReusableView.h"
//#import "WGGrouponPesonCollectionViewCell.h"
//#import "WGGrouponIngHeadCollectionReusableView.h"
//#import "WGGoodsDetailGrouponStatusCollectionReusableView.h"
//#import "WGGoodsThirdTiketCollectionViewCell.h"
//#import "WGGoodsShopHeadCollectionReusableView.h"
//
//#import "WGOrderDetailViewController.h"
//#import "YXCreateAddressViewController.h"
//#import "JVShopcartViewController.h"
//#import "WGGoodOtherDetailViewController.h"
//#import "WGShopViewController.h"
//#import "WGGroupoIngViewController.h"

#import "ZLJShopViewModle.h"
#import "WGGoodsDetailViewModel.h"
//#import "ZKTimerService.h"
#import "WGGoodsInfoModel.h"
#import "UIImage+ColorImage.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "UIViewController+KNSemiModal.h"
//#import "XWLocationManager.h"
//#import <HWPop.h>
//#import "WGThirdShareViewController.h"
//#import "WGHomeNavViewController.h"
//#import "WGAliBCManage.h"
//#import "WGChartViewController.h"

@interface YXGoodDetailsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>{
}

@property (nonatomic ,strong) UIButton *shareBtn;

///** 导航栏菜单*/
//@property (nonatomic ,strong) YXHomeSectionView *titleView;
///** 商品详情底部菜单*/
//@property (nonatomic ,strong) WGGoodsDetailBottomView *detaiBottom;
/** 规格视图*/
@property (nonatomic ,strong) WGGuiGeView *guigeView;
/** 商品详情列表视图*/
@property (nonatomic ,strong) UICollectionView *collectionView;
/** 商品详情*/
@property (nonatomic ,strong) WGGoodDetailWebCollectionViewCell *detailWebCell;
/** 地址管理*/
@property (nonatomic ,strong) WGAddressModel *addressModel;
/** 详情数据驱动*/
@property (nonatomic ,strong) WGGoodsDetailViewModel *viewsModel;
/** 记录详情加载完成*/
@property (nonatomic ,assign) BOOL isLoadWeb;
/** 记录详情加载完成*/
//@property (nonatomic ,assign) WGGoodsButtonType currentAction;

@end

@implementation YXGoodDetailsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"商品详情";
    
    _viewsModel = [WGGoodsDetailViewModel new];
    _viewsModel.goodsInfo = _goodsInfo;
    [self request];

    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.edges.equalTo(self.view);
        }
    }];
}

/**
 请求商品详情
 */
- (void)request{
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [ZLJShopViewModle queryitemDetailId:self.goodsID UserId:[YXUserInfoManager getUserInfo].user_id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            weakSelf.goodsInfo = [WGGoodsInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.goodsInfo.goodStatus = 1;
            if (weakSelf.goodsInfo.specSkuSort.count == 0){
            } else if (weakSelf.goodsInfo.specSkuSort.count == 1) {
                weakSelf.goodsInfo.selectSku = weakSelf.goodsInfo.specSkuSort[0];
                weakSelf.goodsInfo.noSelect = [NSString stringWithFormat:@"已选:%@ ",[weakSelf.goodsInfo.selectSku.spec componentsJoinedByString:@","]];
            }else{
                NSMutableArray *arr = [NSMutableArray new];
                for (SpecList *list in weakSelf.goodsInfo.specListSort) {
                    [arr addObject:list.specName];
                }
                weakSelf.goodsInfo.noSelect = [NSString stringWithFormat:@"请选择:%@",[arr componentsJoinedByString:@","]];
            }
            weakSelf.viewsModel = [WGGoodsDetailViewModel new];
            weakSelf.viewsModel.goodsInfo = weakSelf.goodsInfo;
            //                weakSelf.detaiBottom.viewsModel = weakSelf.viewsModel;
            //                weakSelf.detaiBottom.goodsState = 1;
            weakSelf.isLoadWeb = NO;
            weakSelf.guigeView.model = weakSelf.goodsInfo;
            weakSelf.guigeView.viewsModel = weakSelf.viewsModel;
            weakSelf.guigeView.goodStatus = 1;
            
        }else {
             [YJProgressHUD showError:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
        
}
#pragma mark -- collectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (section) {
        case 0:{
            return 3;
        }
            break;
        case 1:{
            return 1;
        }
            break;
        case 2:{
            return 1;
        }
            break;
        default:
            break;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return CGSizeMake(KWIDTH, KWIDTH);
        }else if (indexPath.row == 1) {
            return CGSizeMake(KWIDTH, 44);
        }else if (indexPath.row == 2) {
            return CGSizeMake(KWIDTH, 50);
        }
    }else  if (indexPath.section == 1) {
        return CGSizeMake(KWIDTH, 44);
    }else {
        return CGSizeMake(KWIDTH, _goodsInfo.particularsWebViewHeight == 0?350:_goodsInfo.particularsWebViewHeight);
        return CGSizeMake(KWIDTH, 350);

    }

    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 2) {
       return CGSizeMake(KWIDTH, 40);
    }
    return CGSizeZero;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 10);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsZero;;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            WGGoodDetailBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGoodDetailBannerCollectionViewCell" forIndexPath:indexPath];
            cell.model = _goodsInfo;
            return cell;
        }else if (indexPath.row == 1){
            WGGoodDetailNormalPriceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGoodDetailNormalPriceCollectionViewCell" forIndexPath:indexPath];
            cell.model = _goodsInfo;
            return cell;
        }else {
            WGGoodDetailsInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGoodDetailsInfoCollectionViewCell" forIndexPath:indexPath];
            cell.model = _goodsInfo;
            return cell;
        }
    }else if (indexPath.section == 1) {
        WGGoodDetailMoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGoodDetailMoreCollectionViewCell" forIndexPath:indexPath];
        cell.type = GoodsDetailMoreTypeGuiGe;
        NSString *tem = [_goodsInfo.noSelect stringByReplacingOccurrencesOfString:@"已选:" withString:@""];
        if ([tem containsString:@"请选择"]) {
            cell.content = [NSString stringWithFormat:@"%@",tem.length?tem:@""];
            
        }else{
            cell.content = [NSString stringWithFormat:@"%@ %ld件",tem.length?tem:@"",(long)_goodsInfo.goodsCount];
        }
        return cell;
    }else {
        WGGoodDetailWebCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGGoodDetailWebCollectionViewCell" forIndexPath:indexPath];
        if (!_isLoadWeb&&_goodsInfo.nappintroduction.length) {
            cell.html = _goodsInfo;
            _isLoadWeb = YES;
        }
        _detailWebCell = cell;
        [cell setWebViewFinishLoadBlock:^(CGFloat webViewHeight) {
            weakSelf.goodsInfo.particularsWebViewHeight = webViewHeight;
            [weakSelf.collectionView reloadData];
        }];
        return cell;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 2) {
            WGGoodDetailImAndTCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGGoodDetailImAndTCollectionReusableView" forIndexPath:indexPath];
            return view;
        }
        
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        WGEmptyCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGEmptyCollectionReusableView" forIndexPath:indexPath];
        return view;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        [self selectSpecification:WGGoodsButtonTypeNone];
    }
}

/**
 选择商品sku
 
 @param type 弹出类型
 */
- (void)selectSpecification:(WGGoodsButtonType)type{
    
    self.guigeView.vc = self;
//    self.guigeView.type = type;
    [self presentSemiView:self.guigeView withOptions:@{
                                                       KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                       KNSemiModalOptionKeys.parentAlpha : @(0.8)
                                                       }];
    
}


- (void)dealloc{
    if (_detailWebCell) {
        [_detailWebCell removeKVO];
    }
}
- (WGGuiGeView *)guigeView{
    if (!_guigeView) {
        _guigeView =[[WGGuiGeView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, kHEIGHT * 0.7)];
        YXWeakSelf;
//        [_guigeView setUpDataModel:^(WGGoodsInfoModel * _Nonnull model) {
//            [weakSelf volidItems:model address:weakSelf.addressModel];
//            [weakSelf.collectionView reloadData];
//        }];
//        [_guigeView setActionType:^(WGGoodsButtonType type) {
//            [weakSelf action:type];
//        }];
    }
    return _guigeView;
}

- (UIButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _shareBtn.frame = CGRectMake(0, 0, 44, 44);
        _shareBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
        [_shareBtn setImage:[UIImage imageNamed:@"good_detail_share"] forState:(UIControlStateNormal)];
        [_shareBtn addTarget:self action:@selector(shareThirdTiket) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView setupEmptyDataText:@"商品已下架" verticalOffset:-100 emptyImage:[UIImage imageNamed:@"goods_status_soldOut"] tapBlock:^{
            
        }];
        [_collectionView registerClass:[WGGoodDetailBannerCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailBannerCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodDetailFlashSaleDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailFlashSaleDetailCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodDetailRemindCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailRemindCollectionViewCell"];
//
//
        [_collectionView registerClass:[WGGoodDetailNormalPriceCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailNormalPriceCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodDetailGrouponCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailGrouponCollectionViewCell"];
        [_collectionView registerClass:[WGGoodDetailsInfoCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailsInfoCollectionViewCell"];
        [_collectionView registerClass:[WGGoodDetailMoreCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailMoreCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodDetailTagCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailTagCollectionViewCell"];
        [_collectionView registerClass:[WGGoodDetailWebCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodDetailWebCollectionViewCell"];
//        [_collectionView registerClass:[WGGrouponPesonCollectionViewCell class] forCellWithReuseIdentifier:@"WGGrouponPesonCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodsThirdTiketCollectionViewCell class] forCellWithReuseIdentifier:@"WGGoodsThirdTiketCollectionViewCell"];
//        [_collectionView registerClass:[WGGoodsShopHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGoodsShopHeadCollectionReusableView"];
//        [_collectionView registerClass:[WGGrouponIngHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGrouponIngHeadCollectionReusableView"];
//        [_collectionView registerClass:[WGGoodDetailStoreHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGoodDetailStoreHeadCollectionReusableView"];
        [_collectionView registerClass:[WGGoodDetailImAndTCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGoodDetailImAndTCollectionReusableView"];
//        [_collectionView registerClass:[WGGoodDetailGrouponCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGoodDetailGrouponCollectionReusableView"];
//        [_collectionView registerClass:[WGGoodsDetailGrouponStatusCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGGoodsDetailGrouponStatusCollectionReusableView"];
//
//
        [_collectionView registerClass:[WGEmptyCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"WGEmptyCollectionReusableView"];
        
    }
    return _collectionView;
}



@end
