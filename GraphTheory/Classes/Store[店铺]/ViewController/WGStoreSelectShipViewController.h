//
//  WGStoreSelectShipViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN


/**
 选择配送方式
 */
@interface WGStoreSelectShipViewController : DDBaseViewController

@property (nonatomic ,strong) NSMutableArray *selectArr;
@property (nonatomic ,copy) void(^selectStoreShipMethod)(NSArray *arr);


@end

NS_ASSUME_NONNULL_END
