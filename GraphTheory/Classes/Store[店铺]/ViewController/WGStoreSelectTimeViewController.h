//
//  WGStoreSelectTimeViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "WGStoreModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelectTimeViewController : DDBaseViewController

@property (nonatomic ,strong) WGStoreTimeModel *timeModel;
@property (nonatomic ,copy) void(^selectStoreTime)(WGStoreTimeModel *model);


@end

NS_ASSUME_NONNULL_END
