//
//  WGCreatStoreViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGCreatStoreViewController.h"
#import "WGCreatStoreCollectionViewCell.h"
#import "WGStoreModel.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import "WGStoreSelectShipViewController.h"
#import "WGStoreSelectTimeViewController.h"
#import "WGStoreSelectExpressAreaViewController.h"
#import "WGStoreSelectCategoryViewController.h"
#import "WGStoreSelectAddressViewController.h"

#import "YXStoreViewModel.h"
@interface WGCreatStoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TZImagePickerControllerDelegate>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, assign) CGFloat h;


@end

@implementation WGCreatStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    
    if (self.type == 0) {
        self.title = @"创建店铺";
        _h = 0;
        _storeModel = [WGStoreModel new];
        _storeModel.storeShippingMethod = @[@"到店",@"上门"];
        _storeModel.shopName = @"唐僧洗发用飘柔";
        _storeModel.serviceMobile = [YXUserInfoManager getUserInfo].mobile;
        _storeModel.shopIntroduce = @"洗发水";
    }else {
        self.title = @"修改店铺信息";

    }

    [self.view addSubview:self.saveBtn];
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-15);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.bottom.equalTo(self.view.mas_bottom).offset(-15);
            make.left.equalTo(self.view.mas_left).offset(15);
            make.right.equalTo(self.view.mas_right).offset(-15);
        }
        make.height.equalTo(@44.0);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.top.equalTo(self.view.mas_top);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
        }
        make.bottom.equalTo(self.saveBtn.mas_top);
    }];
}

#pragma mark - 点击保存按钮
- (void)save{
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    NSString *url = [NSString stringWithFormat:@"%@system/client/sys/oss/fileUpload",kBaseURL];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:@"2" forKey:@"type"];
    [NetWorkTools uploadImagesPOSTWithUrl:url parameters:parameters images:@[_storeModel.storeImage].mutableCopy success:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            weakSelf.storeModel.ossId = responesObj[@"data"][@"id"];
            weakSelf.storeModel.shopLogo = responesObj[@"data"][@"filepath"];
            [weakSelf requestInsertOrUpdateShop];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];

}

// 请求新增/修改店铺
- (void)requestInsertOrUpdateShop {
    
    [YJProgressHUD showLoading:@""];
    [YXStoreViewModel requestInsertShopWithType:self.type StoreModel:self.storeModel Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 5;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 3;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (![_storeModel.storeShippingMethod containsObject:@"上门"]) {
        if (indexPath.section == 1 &&(indexPath.row == 0 || indexPath.row == 1)) {
            return CGSizeMake(SCREEN_WIDTH , 0.01);
        }
    }
    if (![_storeModel.storeShippingMethod containsObject:@"到店"]) {
        if (indexPath.section == 2 && indexPath.row == 1) {
            return CGSizeMake(SCREEN_WIDTH , 0.01);
        }
    }
    if (![_storeModel.storeShippingMethod containsObject:@"快递"]) {
        if (indexPath.section == 1 && indexPath.row == 2) {
            return CGSizeMake(SCREEN_WIDTH , 0.01);
        }
    }
    if (indexPath.section == 2 && indexPath.row == 2) {
//        if (_h != 0) {
            return CGSizeMake(SCREEN_WIDTH, 100);
//        }
    }
    return CGSizeMake(SCREEN_WIDTH , 44);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (![_storeModel.storeShippingMethod containsObject:@"上门"] && ![_storeModel.storeShippingMethod containsObject:@"快递"]) {
        if (section == 1) {
            return UIEdgeInsetsZero;
        }
    }
    return UIEdgeInsetsMake(10, 0, 0, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf;
    WGCreatStoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGCreatStoreCollectionViewCell" forIndexPath:indexPath];
    cell.storeModel = _storeModel;
    cell.index = indexPath;
    
    [cell setTextFieldChangeBlock:^(NSString * _Nonnull content) {
        if (indexPath.section == 1&&indexPath.row == 0) {
            weakSelf.storeModel.storeDoorArea = content;
        }else if(indexPath.section == 0 &&indexPath.row == 1){
            weakSelf.storeModel.shopName = content;
        }
    }];
    cell.textViewChange = ^(CGFloat height) {
        weakSelf.h = height;
//        [weakSelf.collectionView be];
//        [self.collectionView endUpdates];
    };
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1
                                                                                                  delegate:self];
            imagePicker.allowCrop = YES;
            imagePicker.cropRect = CGRectMake(0, (kHEIGHT - SCREEN_WIDTH)/2, KWIDTH, KWIDTH);
            [imagePicker setSortAscendingByModificationDate:NO];
            imagePicker.isSelectOriginalPhoto = YES;
            imagePicker.allowPickingVideo = NO;
            
            [self presentViewController:imagePicker
                               animated:YES
                             completion:nil];
            
        }else if (indexPath.row == 3){
            WGStoreSelectShipViewController *ship = [WGStoreSelectShipViewController new];
            if (_storeModel.storeShippingMethod) {
                ship.selectArr = [[NSMutableArray alloc]initWithArray:_storeModel.storeShippingMethod];
            }
            ship.selectStoreShipMethod = ^(NSArray * _Nonnull arr) {
                weakSelf.storeModel.storeShippingMethod = arr;
                [weakSelf.collectionView reloadData];
            };
            [self.navigationController pushViewController:ship animated:YES];
        }else if (indexPath.row == 2){
            WGStoreSelectCategoryViewController *ship = [WGStoreSelectCategoryViewController new];
            [self.navigationController pushViewController:ship animated:YES];
            [ship setClickSelectCategoryBlock:^(NSString * _Nonnull categoryId, NSString * _Nonnull categoryName) {
                weakSelf.storeModel.mainCategory = categoryId;
                weakSelf.storeModel.mainCategoryName = categoryName;
                [weakSelf.collectionView reloadData];
            }];
            
        }else if (indexPath.row == 4){
            WGStoreSelectAddressViewController *ship = [WGStoreSelectAddressViewController new];
            [self.navigationController pushViewController:ship animated:YES];

            YXWeakSelf
            [ship setSelectAddressBlock:^(NSString * _Nonnull longitude, NSString * _Nonnull address) {
                NSLog(@"%@ -- %@" ,longitude ,address);
                weakSelf.storeModel.shopAddress = address;
                weakSelf.storeModel.longitude = longitude;
                [weakSelf.collectionView reloadData];
            }];
        }
    }else if (indexPath.section == 1){
        if (indexPath.row == 1) {
            WGStoreSelectTimeViewController *time = [WGStoreSelectTimeViewController new];
            time.timeModel = _storeModel.storeDoorTime;
            time.selectStoreTime = ^(WGStoreTimeModel * _Nonnull model) {
                weakSelf.storeModel.storeDoorTime = model;
                [weakSelf.collectionView reloadData];
            };
            [self.navigationController pushViewController:time animated:YES];

        }else if (indexPath.row == 2){
            WGStoreSelectExpressAreaViewController *time = [WGStoreSelectExpressAreaViewController new];
//            time.timeModel = _storeModel.storeDoorTime;
//            time.selectStoreTime = ^(WGStoreTimeModel * _Nonnull model) {
//                weakSelf.storeModel.storeDoorTime = model;
//                [weakSelf.collectionView reloadData];
//            };
            [self.navigationController pushViewController:time animated:YES];
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == 1) {
            WGStoreSelectTimeViewController *time = [WGStoreSelectTimeViewController new];
            time.timeModel = _storeModel.storeTime;
            time.selectStoreTime = ^(WGStoreTimeModel * _Nonnull model) {
                weakSelf.storeModel.storeTime = model;
                [weakSelf.collectionView reloadData];
            };
            [self.navigationController pushViewController:time animated:YES];
            
        }
    }
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    UIImage *seleImage = photos[0];
    _storeModel.storeImage = seleImage;
    [self.collectionView reloadData];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[WGCreatStoreCollectionViewCell class] forCellWithReuseIdentifier:@"WGCreatStoreCollectionViewCell"];
        //        [_collectionView registerClass:[WGCertificationCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGCertificationCollectionReusableView"];
    }
    return _collectionView;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.layer.cornerRadius = 4;
        _saveBtn.enabled = YES;
        _saveBtn.alpha = 0.5;
    }
    return _saveBtn;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
