//
//  WGCreatStoreViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 创建店铺
 */
@class WGStoreModel;
@interface WGCreatStoreViewController : DDBaseViewController

// 店铺类型 0:创建 1:修改
@property (nonatomic ,assign) NSInteger type;

// 店铺信息
@property (nonatomic, strong) WGStoreModel *storeModel;


@end

NS_ASSUME_NONNULL_END
