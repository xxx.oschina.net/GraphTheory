//
//  YXSalesAfterListController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/4.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXSalesAfterListController : DDBaseViewController


/**
 售后状态：0进行中,1已完成,2已关闭
 */
@property (nonatomic ,strong) NSString *refundState;

@end

NS_ASSUME_NONNULL_END
