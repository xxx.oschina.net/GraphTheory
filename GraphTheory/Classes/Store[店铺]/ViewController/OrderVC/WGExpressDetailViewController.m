//
//  WGExpressDetail ViewController.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressDetailViewController.h"
#import "WGExpressCollectionViewCell.h"
#import "WGExpressDetailCollectionViewCell.h"
#import "WGOrderGoodCollectionViewCell.h"
#import "WGExpressModel.h"
@interface WGExpressDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;


@end

@implementation WGExpressDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"物流信息";
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
//    [self requsetExpress];
}
- (void)setExpressModel:(WGExpressModel *)expressModel{
    _expressModel = expressModel;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 1;
    }else if (section == 2){
        return _expressModel.listSort.count;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH, 120);
    }else if (indexPath.section == 1){
        return CGSizeMake(SCREEN_WIDTH, 44);
    }else if (indexPath.section == 2){
        if (indexPath.row < _expressModel.listSort.count) {
            OrderTrack *tr = _expressModel.listSort[indexPath.row];
            return CGSizeMake(SCREEN_WIDTH, tr.height);

        }else{
            return CGSizeZero;
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    if (indexPath.section == 0) {
        WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        cell.detailModel = _expressModel.item[indexPath.row];
        cell.isHideChange = YES;
        return cell;
    }else if (indexPath.section == 1){
        WGExpressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGExpressCollectionViewCell" forIndexPath:indexPath];
        cell.orderModel = self.expressModel;
        return cell;
    }else if (indexPath.section == 2){
        WGExpressDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell" forIndexPath:indexPath];
        cell.isFirst = indexPath.row == 0;
        cell.model = _expressModel.listSort[indexPath.row];
        return cell;
    }
    return nil;
    
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];

        [_collectionView registerClass:[WGExpressCollectionViewCell class] forCellWithReuseIdentifier:@"WGExpressCollectionViewCell"];
        [_collectionView registerClass:[WGExpressDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell"];
        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
    }
    return _collectionView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
