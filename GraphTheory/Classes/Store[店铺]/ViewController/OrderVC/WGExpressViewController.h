//
//  WGExpressViewController.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/19.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class WGShopModel;
@interface WGExpressViewController :DDBaseViewController
@property (nonatomic, strong) WGShopModel *shop;
@property (nonatomic, strong) NSString *refundId;

@end

NS_ASSUME_NONNULL_END
