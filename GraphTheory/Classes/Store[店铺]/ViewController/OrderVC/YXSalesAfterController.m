//
//  YXSalesAfterController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/4.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXSalesAfterController.h"
#import "YXSalesAfterListController.h"
#import "XLSlideSwitch.h"

@interface YXSalesAfterController ()

@property (nonatomic ,strong) XLSlideSwitch *slideSwitch;

@end

@implementation YXSalesAfterController

- (XLSlideSwitch *)slideSwitch {
    if (!_slideSwitch) {
        NSArray *titles = @[@"待处理",@"已完成",@"已关闭"];
        YXSalesAfterListController *conduct = [[YXSalesAfterListController alloc] init];
        conduct.refundState = @"0";
        YXSalesAfterListController *complete = [[YXSalesAfterListController alloc] init];
        complete.refundState = @"1";
        YXSalesAfterListController *closed = [[YXSalesAfterListController alloc] init];
        closed.refundState = @"2";
        NSArray *viewControllers = @[conduct,complete,closed];
        _slideSwitch = [[XLSlideSwitch alloc] initWithFrame:CGRectMake(0,0 , KWIDTH, kHEIGHT - SafeAreaTopHeight) Titles:titles viewControllers:viewControllers];
        //    _slideSwitch.delegate = self;
        _slideSwitch.backgroundColor = [UIColor whiteColor];
        _slideSwitch.selectedIndex = 0;
        _slideSwitch.itemSelectedColor = APPTintColor;
        _slideSwitch.itemNormalColor = color_TextOne;
        _slideSwitch.customTitleSpacing = KWIDTH / 3.0 - 48;//间距
    }
    return _slideSwitch;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"售后";
    [self.slideSwitch showInViewController:self];
}

@end
