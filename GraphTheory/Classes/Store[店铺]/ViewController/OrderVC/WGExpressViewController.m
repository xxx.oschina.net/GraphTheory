//
//  WGExpressViewController.m
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/19.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "WGExpressViewController.h"
#import "WGExpressGoodsCollectionViewCell.h"
#import "WGExpressCollectionViewCell.h"
#import "WGExpressDetailCollectionViewCell.h"
#import "WGEmptyCollectionReusableView.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "WGExpressDetailViewController.h"
#import "UIScrollView+DREmptyDataSet.h"
//#import "ZFFlowLayout.h"
#import "WGOrderViewModel.h"
#import "WGOrderModel.h"
#import "WGExpressModel.h"
#import "YCMenuView.h"
#import "YXPublicCollectionReusableView.h"
@interface WGExpressViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) WGExpressModel *orderModel;
@property (nonatomic ,strong) NSMutableArray *logisticsArr;
@end

@implementation WGExpressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"物流信息";

    [self requsetExpress];
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}


- (void)setShop:(WGShopModel *)shop {
    _shop = shop;
}

- (void)requsetExpress{
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel queryOrderLogisticsInfoWithOrderNo:_shop.orderNo Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.logisticsArr removeAllObjects];
            NSArray *tem = [WGExpressModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
            [weakSelf.dataArr addObjectsFromArray:tem];
            if (weakSelf.dataArr.count > 0) {
                weakSelf.orderModel = [weakSelf.dataArr firstObject];
            }
            for (WGExpressModel *orderModel in weakSelf.dataArr) {
                YCMenuAction *action = [YCMenuAction actionWithTitle:orderModel.expressNo image:nil handler:^(YCMenuAction *action) {
                    if ([orderModel.expressNo containsString:action.title]) {
                        weakSelf.orderModel = orderModel;
                        [weakSelf.collectionView reloadData];
                    }
                }];
                [weakSelf.logisticsArr addObjectsFromArray:@[action]];
            }
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];

}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else {
        return _orderModel.listSort.count;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH, 77);
    }else {
        if (indexPath.row < _orderModel.listSort.count) {
            OrderTrack *tr = _orderModel.listSort[indexPath.row];
            return CGSizeMake(SCREEN_WIDTH, tr.height);
            
        }else{
            return CGSizeZero;
        }
    }
    
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return CGSizeMake(SCREEN_WIDTH, 30);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(SCREEN_WIDTH, 10);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 0, 15);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        YXPublicCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
        headerView.titleLab.text = @"订单跟踪";
        return headerView;
        
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        WGEmptyCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGEmptyCollectionReusableView" forIndexPath:indexPath];
        return view;
    }
    return nil;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        WGExpressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGExpressCollectionViewCell" forIndexPath:indexPath];
        cell.dataArr = self.dataArr;
        cell.orderModel = self.orderModel;
        YXWeakSelf
        [cell setClickSelectCourierNoBtn:^(UIButton * _Nonnull btn) {
            if (weakSelf.logisticsArr.count > 0) {
                YCMenuView *view = [YCMenuView menuWithActions:weakSelf.logisticsArr width:140 relyonView:btn];
                [view show];
            }
        }];
        return cell;
    }else {
        WGExpressDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell" forIndexPath:indexPath];
        cell.isFirst = indexPath.row == 0;
        cell.model = _orderModel.listSort[indexPath.row];
        return cell;
    }
    return [UICollectionViewCell new];
    
}
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    WGExpressModel *tem = _dataArr[indexPath.section];
//    WGExpressDetailViewController *detail = [WGExpressDetailViewController new];
//    detail.expressModel = tem;
//    
//    [self.navigationController pushViewController:detail animated:YES];
//}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        //        _collectionView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
        [_collectionView setupEmptyDataText:@"暂无物流数据,请稍后再试" tapBlock:^{
            
        }];
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXPublicCollectionReusableView"];
        
        [_collectionView registerClass:[WGEmptyCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"WGEmptyCollectionReusableView"];
        
        [_collectionView registerClass:[WGExpressCollectionViewCell class] forCellWithReuseIdentifier:@"WGExpressCollectionViewCell"];
        [_collectionView registerClass:[WGExpressDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell"];
    }
    return _collectionView;
}
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (NSMutableArray *)logisticsArr {
    if (!_logisticsArr) {
        _logisticsArr = [NSMutableArray array];
    }
    return _logisticsArr;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
