//
//  YXApplyRefundViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXApplyRefundViewController : DDBaseViewController
@property (nonatomic, strong) WGShopModel *orderModel;

@end

NS_ASSUME_NONNULL_END
