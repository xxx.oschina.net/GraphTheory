//
//  YXApplyRefundViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXApplyRefundViewController.h"
#import "WGHomeNavViewController.h"
#import "YXRefundReasonViewController.h"

#import "WGPublicBottomView.h"
#import "WGOrderGoodCollectionViewCell.h"
//#import "YXPublicCollectionViewCell.h"

#import "WGOrderViewModel.h"
@interface YXApplyRefundViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSString *refundReason; // 退款原因
@property (nonatomic ,strong) NSString *refundExplain; // 退款说明

@end

@implementation YXApplyRefundViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"申请退款";
    
    self.refundExplain = @"";
    
    [self loadSubViews];
    
}

- (void)requestRefund {
    
    if ([NSString isBlankString:self.refundReason]) {
        return [YJProgressHUD showMessage:@"请选择退款原因"];
    }
    
    if ([NSString isBlankString:self.refundExplain]) {
        return [YJProgressHUD showMessage:@"请输入退款说明"];
    }
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel requestOrderRefundWithOrderNo:self.orderModel.orderNo refundReason:self.refundReason refundAmount:_orderModel.mainPayPrice refundExplain:self.refundExplain Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}



#pragma mark - 添加子视图
- (void)loadSubViews {
    YXWeakSelf
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.bottom.and.right.equalTo(self.view);
        }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return _orderModel.items.count;
    }else if (section == 1) {
        return 4;
    }else if (section == 2) {
        return 1;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH, 100);
    }else if (indexPath.section == 1) {
        return CGSizeMake(KWIDTH, 44);
    }else if (indexPath.section == 2) {
        return CGSizeMake(KWIDTH, 120);
    }
    return CGSizeZero;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
//    if (indexPath.section == 0) {
//        WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
//        cell.type = 1;
//        cell.detailModel = _orderModel.items[indexPath.row];
//        cell.isHideChange = _orderModel.orderState != 0;
//        return cell;
//    }else if (indexPath.section == 1) {
//        if (indexPath.row ==  1) {
//            YXPublicCollectionViewCell1 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXPublicCollectionViewCell1" forIndexPath:indexPath];
//            cell.titleLab.text = @"退款原因";
//            cell.contentLab.text = self.refundReason?self.refundReason:@"请选择";
//            return cell;
//        }else {
//            YXPublicCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXPublicCollectionViewCell" forIndexPath:indexPath];
//            if (indexPath.row == 0) {
//                cell.titleLab.text = @"退款类型";
//                cell.contentLab.text = @"仅退款";
//                cell.contentLab.textAlignment = NSTextAlignmentRight;
//            }else if (indexPath.row == 2){
//                cell.titleLab.text = @"退款金额";
//                cell.contentLab.text = [NSString stringWithFormat:@"¥%@",_orderModel.mainPayPrice];
//                cell.contentLab.textAlignment = NSTextAlignmentRight;
//            }else if (indexPath.row == 3) {
//                cell.contentLab.hidden = YES;
//                cell.titleLab.text = [NSString stringWithFormat:@"最多可退款¥%@",_orderModel.mainPayPrice];
//                cell.titleLab.font = [UIFont systemFontOfSize:12.0f];
//                cell.titleLab.textColor = color_RedColor;
//            }
//            return cell;
//        }
//    }else if (indexPath.section == 2) {
//        YXPublicCollectionViewCell2 *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXPublicCollectionViewCell2" forIndexPath:indexPath];
//        cell.titleLab.text = @"退款说明";
//        YXWeakSelf
//        [cell setTextViewChange:^(NSString * _Nonnull text) {
//            if (text.length > 0) {
//                weakSelf.refundExplain = text;
//            }
//        }];
//        return cell;
//    }
    return nil;
    
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        YXRefundReasonViewController *vc = [YXRefundReasonViewController new];
        vc.refundReason = self.refundReason;
        YXWeakSelf
        [vc setSelectRefundReasonBlock:^(NSString * _Nonnull refundReason) {
            weakSelf.refundReason = refundReason;
            NSIndexPath *index = [NSIndexPath indexPathForItem:1 inSection:1];
            [weakSelf.collectionView reloadItemsAtIndexPaths:@[index]];
        }];
        
        WGHomeNavViewController *nav = [[WGHomeNavViewController alloc] initWithRootViewController:vc];
        
        [nav popupWithPopType:HWPopTypeSlideInFromBottom dismissType:HWDismissTypeSlideOutToBottom position:HWPopPositionBottom];
    }
    
}


#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];

        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
//        [_collectionView registerClass:[YXPublicCollectionViewCell class] forCellWithReuseIdentifier:@"YXPublicCollectionViewCell"];
//        [_collectionView registerClass:[YXPublicCollectionViewCell1 class] forCellWithReuseIdentifier:@"YXPublicCollectionViewCell1"];
//        [_collectionView registerClass:[YXPublicCollectionViewCell2 class] forCellWithReuseIdentifier:@"YXPublicCollectionViewCell2"];
    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"退款"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf requestRefund];
        }];
    }
    return _bottomView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
