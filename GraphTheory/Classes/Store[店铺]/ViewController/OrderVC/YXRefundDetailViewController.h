//
//  YXRefundDetailsViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/4.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
/**
 退款详情
 */
@class YXRefundModel;
@interface YXRefundDetailViewController : DDBaseViewController

/// 退款状态
@property (nonatomic ,strong) NSString *refundState;

/// 退款记录id
@property (nonatomic, strong) NSString *refundId;

@property (nonatomic, strong) YXRefundModel *refundModel;


@end

NS_ASSUME_NONNULL_END
