//
//  WGOrderListViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"


NS_ASSUME_NONNULL_BEGIN

/**
 订单列表
 */
@interface WGOrderListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>

@property (nonatomic ,strong) NSString *orderStatus;///<订单状态 0待付款1待发货 2待服务3已发货4已完成5已关闭6售后、退款 all全部 1001搜索
@end

NS_ASSUME_NONNULL_END
