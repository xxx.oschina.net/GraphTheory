//
//  YXSalesAfterListController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/4.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXSalesAfterListController.h"
#import "YXRefundDetailViewController.h"

#import "WGOrderHeadCollectionViewCell.h"
#import "WGOrderGoodCollectionViewCell.h"
#import "WGOrderDisTypeCollectionViewCell.h"
#import "WGOrderListALlCollectionViewCell.h"
#import "WGOrderFootCollectionViewCell.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "YXCustomAlertActionView.h"

#import "WGOrderViewModel.h"
#import "YXRefundModel.h"
//#import "WGIMManage.h"
@interface YXSalesAfterListController ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) YXCustomAlertActionView *agreedAlertView;
@property (nonatomic ,strong) YXCustomAlertActionView *refusedAlertView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) YXRefundModel *refundModel;
@end

@implementation YXSalesAfterListController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self refreshList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.page = 1;
        
    [self loadSubViews];
   
}

- (void)refreshList {
    
    YXWeakSelf;
    _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf loadData];
    }];
    _collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        weakSelf.page ++ ;
        [weakSelf loadData];
    }];
    [_collectionView.mj_header beginRefreshing];

}

- (void)loadData {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel queryRefundDataListWithPageNum:@(self.page).stringValue PageSize:@"10" ShopId:nil RefundState:self.refundState RefundId:nil OrderState:nil OrderNo:nil StartTime:nil EndTime:nil PayMethod:nil Condition:nil LikeCondition:nil TimeType:nil DistributionType:nil PayType:nil SearchType:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            NSMutableArray *tempArr = [NSMutableArray array];
            NSArray *dataArr = [YXRefundModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
            for (YXRefundModel *refundModel in dataArr) {
                if (refundModel.refundState != 0) {
                    [tempArr addObject:refundModel];
                }
            }
            [weakSelf.dataArr addObjectsFromArray:tempArr];
            if ([dataArr count] < 10) {
                [weakSelf.collectionView.mj_footer setHidden:YES];
            }else{
                [weakSelf.collectionView.mj_footer setHidden:NO];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    }];
    
}

/**
 请求同意退款
 */
- (void)requestAgreed {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel requestAgreedToByRefundId:self.refundModel.refundId Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf refreshList];
            [YJProgressHUD showMessage:@"退款成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
}

/**
 请求拒绝退款

 @param refundReason 拒绝原因
 */
- (void)requestRefuseToRefund:(NSString *)refundReason {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel requestRefuseToByRefundId:self.refundModel.refundId refundReason:refundReason Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf refreshList];
            [YJProgressHUD showMessage:@"拒绝退款成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
}

#pragma mark - 添加子视图
- (void)loadSubViews {
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section < _dataArr.count) {
        YXRefundModel *refundModel = _dataArr[section];
        if ([self.refundState isEqualToString:@"0"]) {
            return refundModel.items.count + 3;
        }else {
            return refundModel.items.count + 2;
        }
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YXRefundModel *refundModel;
    if (indexPath.section < _dataArr.count) {
        refundModel = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        return CGSizeMake(KWIDTH, 44);
    }else if (indexPath.row < (refundModel.items.count + 1) ){
        return CGSizeMake(KWIDTH, 100);
    }else if (indexPath.row == refundModel.items.count + 1){
        return CGSizeMake(KWIDTH, 44);
    }else if (indexPath.row == refundModel.items.count + 2){
        if ([self.refundState isEqualToString:@"0"]) {
            return CGSizeMake(KWIDTH, 44);
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    YXRefundModel *refundModel;
    if (indexPath.section < _dataArr.count) {
        refundModel = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        WGOrderHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderHeadCollectionViewCell" forIndexPath:indexPath];
        cell.refundModel = refundModel;
        [cell setClickMsgBlock:^{
            if (refundModel.userId.length == 0) {
                return;
            }
//            [WGIMManage dd_IM_TurnChartDetailWithUserId:refundModel.userId FromType:@"4" fromModel:refundModel parm:refundModel.userId title:refundModel.userName vc:weakSelf success:^(id  _Nonnull data, NSString * _Nonnull msg) {
//                
//            } fail:^(id  _Nonnull data, NSInteger errorType, NSString * _Nonnull msg) {
//                
//            }];
        }];
        return cell;
    }else if (indexPath.row < (refundModel.items.count + 1) ){
        WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
        cell.type = 0;
        cell.refundItemsModel = refundModel.items[indexPath.row - 1];
        cell.refundModel = refundModel;
        return cell;
    }else if (indexPath.row == refundModel.items.count + 1){
        WGOrderListALlCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderListALlCollectionViewCell" forIndexPath:indexPath];
        cell.refundModel = refundModel;
        return cell;
    }else if (indexPath.row == refundModel.items.count + 2){
        WGOrderFootCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderFootCollectionViewCell" forIndexPath:indexPath];
        cell.refundModel = refundModel;
        [cell setRefundAction:^(YXRefundActionModel * _Nonnull actionModel) {
            weakSelf.refundModel = refundModel;
            if (actionModel.actionType == RefundActionTypeAgreed) {
                [weakSelf.agreedAlertView showAnimation];
            }else if (actionModel.actionType == RefundActionTypeRefused) {
                [weakSelf.refusedAlertView showAnimation];
            }
        }];
        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YXRefundDetailViewController *detai = [YXRefundDetailViewController new];
    detai.refundState = self.refundState;
    detai.refundModel = _dataArr[indexPath.section];
    [self.navigationController pushViewController:detai animated:YES];
}


- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.emptyDataSetSource = self;
        _collectionView.emptyDataSetDelegate = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
        
        [_collectionView registerClass:[WGOrderHeadCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderHeadCollectionViewCell"];
        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDisTypeCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell"];
        [_collectionView registerClass:[WGOrderFootCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderFootCollectionViewCell"];
        [_collectionView registerClass:[WGOrderListALlCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderListALlCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无订单" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        }];
    }
    return _collectionView;
}

- (YXCustomAlertActionView *)agreedAlertView {
    if (!_agreedAlertView) {
        _agreedAlertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:@"确认同意退款吗？" sureBtn:@"确认" cancleBtn:@"取消"];
        YXWeakSelf
        [_agreedAlertView setSureClick:^(NSString * _Nonnull string) {
            [weakSelf requestAgreed];
        }];
    }
    return _agreedAlertView;
}

- (YXCustomAlertActionView *)refusedAlertView {
    if (!_refusedAlertView) {
        _refusedAlertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertTextField) Title:@"拒绝原因" Message:@"请输入拒绝原因" sureBtn:@"保存" cancleBtn:@"取消"];
        YXWeakSelf
        [_refusedAlertView setSureClick:^(NSString * _Nonnull string) {
            [weakSelf requestRefuseToRefund:string];
        }];
    }
    return _refusedAlertView;
}
@end
