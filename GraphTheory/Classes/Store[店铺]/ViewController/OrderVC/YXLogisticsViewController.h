//
//  YXLogisticsViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/10.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXLogisticsViewController : DDBaseViewController

@property (nonatomic ,strong) NSString *refundId;

@end

NS_ASSUME_NONNULL_END
