//
//  YXRefundDetailsViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/4.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "YXRefundDetailViewController.h"
#import "YXLogisticsViewController.h"

#import "WGOrderGoodCollectionViewCell.h"
#import "WGOrderDetailInfoCollectionViewCell.h"
#import "WGOrderDisTypeCollectionViewCell.h"
#import "WGOderDetailPriceInfoCollectionViewCell.h"
#import "WGExpressDetailCollectionViewCell.h"
#import "YXRefundInfoCollectionViewCell.h"
#import "YXRefundProgressCollectionViewCell.h"
#import "YXRefundAddressCollectionViewCell.h"
#import "YXCollectionHeaderView.h"
#import "WGOrderDetailStatusCollectionReusableView.h"
#import "WGPublicBottomView.h"

#import "WGOrderViewModel.h"
#import "YXRefundModel.h"
@interface YXRefundDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXCustomAlertActionView *agreedAlertView;
@property (nonatomic ,strong) YXCustomAlertActionView *refusedAlertView;
@end

@implementation YXRefundDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"退款详情";
    
    [self request];
    
    [self loadSubViews];
}

- (void)request{
    YXWeakSelf;
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel queryRefundDataListWithPageNum:nil PageSize:nil ShopId:nil RefundState:@(self.refundModel.refundState).stringValue RefundId:self.refundModel.refundId OrderState:nil OrderNo:nil StartTime:nil EndTime:nil PayMethod:nil Condition:nil LikeCondition:nil TimeType:nil DistributionType:nil PayType:nil SearchType:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSArray *tem = [YXRefundModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
            if (tem.count) {
                weakSelf.refundModel = tem[0];
                if (weakSelf.refundModel.refundState == 1 || weakSelf.refundModel.refundState == 2) {
                    weakSelf.bottomView.hidden = YES;
                    [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.mas_equalTo(0);
                    }];
                }else{
                    if (weakSelf.refundModel.refundState == 4) {
                        [weakSelf.bottomView setTitle:@[@"拒绝退款"]];
                    }else {
                        [weakSelf.bottomView setTitle:@[@"同意退款",@"拒绝退款"]];
                    }
                    weakSelf.bottomView.hidden = NO;
                    [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.height.mas_equalTo(60);
                    }];
                }
            }
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
    
}


/**
 请求同意退款
 */
- (void)requestAgreed {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel requestAgreedToByRefundId:self.refundModel.refundId Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
//            [weakSelf request];
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:@"退款成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
}

/**
 请求拒绝退款
 
 @param refundReason 拒绝原因
 */
- (void)requestRefuseToRefund:(NSString *)refundReason {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [WGOrderViewModel requestRefuseToByRefundId:self.refundModel.refundId refundReason:refundReason Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
//            [weakSelf request];
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:@"拒绝退款成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
}


#pragma mark - 添加子视图
- (void)loadSubViews {
    YXWeakSelf
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.bottom.and.right.equalTo(self.view);
        }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.right.equalTo(self.view);
        if (weakSelf.refundModel.refundState == 1 || weakSelf.refundModel.refundState == 2) {
            self.bottomView.hidden = YES;
            make.bottom.equalTo(self.view.mas_bottom);
        }else {
            self.bottomView.hidden = NO;
            make.bottom.equalTo(self.bottomView.mas_top);
        }
    }];
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) { ///订单状态以及地址
        if (self.refundModel.refundState == 3) {// 退款中(等待商家处理)
            return 1;
        }else if (self.refundModel.refundState == 4) { // 退款中(等待买家发货)
            return 1;
        }else if (self.refundModel.refundState == 5) { // 退款中(买家已发货)
            if (self.refundModel.signTime.length > 0 && self.refundState.length > 0) {
                return 2;
            }else {
                return 1;
            }
        }else if (self.refundModel.refundState == 1) { // 退款成功
            if (self.refundModel.refundType == 0) { // 仅退款
                return 3;
            }else { // 退货退款
                return 4;
            }
        }else if (self.refundModel.refundState == 2) { // 退款失败
            return 1;
        }
    }else if (section  == 1){ //退款进度
        return _refundModel.refundsSort.count;
    }else if (section == 2){ //商品等信息
        return _refundModel.items.count + 1;
    }else if (section == 3){ //退款编号等信息
        return 1;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if ((self.refundModel.refundState == 5 && self.refundModel.refundType == 1) ||
            (self.refundModel.refundState == 1 && self.refundModel.refundType == 1)) {
            if (self.refundModel.signTime.length > 0 && self.refundModel.signStatus > 0) {
                if (indexPath.row == 0) {
                    return CGSizeMake(KWIDTH, 80);
                }
            }
        }
        return CGSizeMake(KWIDTH, 44);
    }else if (indexPath.section  == 1){
        return CGSizeMake(KWIDTH, 50);
    }else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            return CGSizeMake(KWIDTH, 44);
        }
        return CGSizeMake(KWIDTH, 100);
    }else if (indexPath.section == 3){
        if (self.refundModel.refundCredentialsInfo.length > 0) {
            return CGSizeMake(KWIDTH, 230) ;
        }else {
            return CGSizeMake(KWIDTH, 170) ;
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 10, 0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(KWIDTH, 68);
    }else if (section == 1) {
        return CGSizeMake(KWIDTH, 40);
    }
    return CGSizeZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section ==  0) {
            WGOrderDetailStatusCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGOrderDetailStatusCollectionReusableView" forIndexPath:indexPath];
            view.refundModel = _refundModel;
            return view;
        }else if (indexPath.section == 1) {
            YXCollectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXCollectionHeaderView" forIndexPath:indexPath];
            headerView.titleLab.text = _refundModel.refundsTitle;
            headerView.titleLab.font = [UIFont systemFontOfSize:14.0];
            headerView.doubtBtn.hidden = YES;
            headerView.selectBtn.hidden = YES;
            return headerView;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 0) {
        if (self.refundModel.refundState == 3||
            self.refundModel.refundState == 4||
            self.refundModel.refundState == 2) {
            WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
            cell.rightLabColor = color_TextOne;
            WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
            model.keyStr = @"退款金额";
            model.valueStr = [NSString stringWithFormat:@"¥%@",_refundModel.refundAmount];
            cell.model = model;
            return cell;
        }else if (self.refundModel.refundState == 5) { // 退款中(买家已发货)
            if (self.refundModel.signTime.length > 0 && self.refundModel.signStatus.length > 0) {
                if (indexPath.row == 0) {
                    YXRefundAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXRefundAddressCollectionViewCell" forIndexPath:indexPath];
                    cell.refundModel = _refundModel;
                    return cell;
                }else {
                    WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
                    cell.rightLabColor = color_TextOne;
                    WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                    model.keyStr = @"退款金额";
                    model.valueStr = [NSString stringWithFormat:@"¥%@",_refundModel.refundAmount];
                    cell.model = model;
                    return cell;
                }
            }else {
                WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
                cell.rightLabColor = color_TextOne;
                WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                model.keyStr = @"退款金额";
                model.valueStr = [NSString stringWithFormat:@"¥%@",_refundModel.refundAmount];
                cell.model = model;
                return cell;
            }
        }else if (self.refundModel.refundState == 1) { // 退款成功
            if (self.refundModel.refundType == 0) {
                WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
                cell.rightLabColor = color_TextOne;
                if (indexPath.row == 0) {
                    WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                    model.keyStr = @"退款金额";
                    model.valueStr = [NSString stringWithFormat:@"¥%@",_refundModel.refundAmount];
                    cell.model = model;
                }else if (indexPath.row == 1) {
                    WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                    model.keyStr = @"退款账户";
                    model.valueStr = @"原支付账户";
                    cell.model = model;
                }else if (indexPath.row == 2) {
                    WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                    model.keyStr = @"到账时间";
                    model.valueStr = self.refundModel.refundSuccessTime.length > 0?self.refundModel.refundSuccessTime:@"预计24小时内到账";
                    cell.model = model;
                }
                return cell;
            }else {
                if (indexPath.row == 0) {
                    YXRefundAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXRefundAddressCollectionViewCell" forIndexPath:indexPath];
                    cell.refundModel = _refundModel;
                    return cell;
                }else {
                    WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
                    cell.rightLabColor = color_TextOne;
                    if (indexPath.row == 1) {
                        WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                        model.keyStr = @"退款金额";
                        model.valueStr = [NSString stringWithFormat:@"¥%@",_refundModel.refundAmount];
                        cell.model = model;
                    }else if (indexPath.row == 2) {
                        WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                        model.keyStr = @"退款账户";
                        model.valueStr = @"原支付账户";
                        cell.model = model;
                    }else if (indexPath.row == 3) {
                        WGOderDetailInfoModel *model = [[WGOderDetailInfoModel alloc] init];
                        model.keyStr = @"到账时间";
                        model.valueStr = self.refundModel.refundSuccessTime.length > 0?self.refundModel.refundSuccessTime:@"预计24小时内到账";
                        cell.model = model;
                    }
                    return cell;
                }
            }
        }
    }else if (indexPath.section == 1) {
        WGExpressDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell" forIndexPath:indexPath];
        cell.isFirst = indexPath.row == 0;
        cell.refundsModel = self.refundModel.refundsSort[indexPath.row];
        return cell;
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            WGOrderDisTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell" forIndexPath:indexPath];
            cell.refundModel = _refundModel;
            return cell;
        }else if (indexPath.row <= _refundModel.items.count){
            WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
            cell.type = 1;
            cell.refundItemsModel = _refundModel.items[indexPath.row - 1];
            cell.isHideChange = _refundModel.orderState != 0;
            return cell;
        }
    }else if (indexPath.section == 3) {
        YXRefundInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXRefundInfoCollectionViewCell" forIndexPath:indexPath];
        cell.refundModel = _refundModel;
        return cell;
    }

    return [UICollectionViewCell new];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ((_refundModel.refundState == 5 && _refundModel.refundType == 1) ||
        (_refundModel.refundState == 1 && _refundModel.refundType == 1)) {
        if (indexPath.section == 0 && indexPath.row == 0 && _refundModel.signTime.length > 0 &&_refundModel.signStatus > 0) {
            YXLogisticsViewController *vc = [[YXLogisticsViewController alloc] init];
            vc.refundId = self.refundModel.refundId;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    

}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        
        [_collectionView registerClass:[WGOderDetailPriceInfoCollectionViewCell class] forCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell"];
        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDetailInfoCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDetailInfoCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDisTypeCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell"];
        [_collectionView registerClass:[YXRefundAddressCollectionViewCell class] forCellWithReuseIdentifier:@"YXRefundAddressCollectionViewCell"];
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellID"];
        [_collectionView registerClass:[YXRefundInfoCollectionViewCell class] forCellWithReuseIdentifier:@"YXRefundInfoCollectionViewCell"];
        [_collectionView registerClass:[YXRefundProgressCollectionViewCell class] forCellWithReuseIdentifier:@"YXRefundProgressCollectionViewCell"];
        [_collectionView registerClass:[WGExpressDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGExpressDetailCollectionViewCell"];
        
        [_collectionView registerClass:[WGOrderDetailStatusCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGOrderDetailStatusCollectionReusableView"];
        [_collectionView registerClass:[YXCollectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
         withReuseIdentifier:@"YXCollectionHeaderView"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        if (_refundModel.refundState == 4) {
            [_bottomView setTitle:@[@"拒绝退款"]];
        }else {
            [_bottomView setTitle:@[@"同意退款",@"拒绝退款"]];
        }
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.refundModel.refundState == 4) {
                [weakSelf.refusedAlertView showAnimation];
            }else {
                if (index == 0) {
                    [weakSelf.agreedAlertView showAnimation];
                }else {
                    [weakSelf.refusedAlertView showAnimation];
                }
            }
        }];
    }
    return _bottomView;
}
- (YXCustomAlertActionView *)agreedAlertView {
    if (!_agreedAlertView) {
        _agreedAlertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:@"确认同意退款吗？" sureBtn:@"确认" cancleBtn:@"取消"];
        YXWeakSelf
        [_agreedAlertView setSureClick:^(NSString * _Nonnull string) {
            [weakSelf requestAgreed];
        }];
    }
    return _agreedAlertView;
}

- (YXCustomAlertActionView *)refusedAlertView {
    if (!_refusedAlertView) {
        _refusedAlertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertTextField) Title:@"拒绝原因" Message:@"请输入拒绝原因" sureBtn:@"保存" cancleBtn:@"取消"];
        YXWeakSelf
        [_refusedAlertView setSureClick:^(NSString * _Nonnull string) {
            [weakSelf requestRefuseToRefund:string];
        }];
    }
    return _refusedAlertView;
}

@end
