//
//  WGOrderListViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderListViewController.h"
#import "WGOrderDetailViewController.h"
#import "WGExpressViewController.h"

//#import "YXSpellGroupHeaderView.h"

#import "WGOrderHeadCollectionViewCell.h"
#import "WGOrderGoodCollectionViewCell.h"
#import "WGOrderDisTypeCollectionViewCell.h"
#import "WGOrderListALlCollectionViewCell.h"
#import "WGOrderFootCollectionViewCell.h"
#import "WGOrderActionView.h"
#import "WGOrderViewModel.h"
#import "YXMessageViewModel.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "WGExpressView.h"
#import "UIViewController+KNSemiModal.h"
#import "ZLJSearchGoodsView.h"
//#import "WGIMManage.h"

#define COLOR_WITH_RGB(R,G,B,A) [UIColor colorWithRed:R green:G blue:B alpha:A]


@interface WGOrderListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout,UISearchBarDelegate>
@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
/** 搜索框*/
@property (nonatomic ,strong) ZLJSearchGoodsView *searchView;
@property (nonatomic ,strong) UIButton *searchBtn;

@property (nonatomic ,strong) NSString *keyWord;///<搜索


@end

@implementation WGOrderListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _page = 1;
    [self setNav];
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    YXWeakSelf;
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf request];
    }];
    self.collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        weakSelf.page ++ ;
        [weakSelf request];
    }];

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.orderStatus.integerValue != 1001) {
        self.page = 1;
        [self request];
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section < _dataArr.count) {
        WGShopModel *shop = _dataArr[section];
        if (shop.actionArr.count) {
            return shop.items.count + 3;
        }else{
            return shop.items.count + 2;
        }
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    WGShopModel *shop;
    if (indexPath.section < _dataArr.count) {
        shop = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        return CGSizeMake(KWIDTH-30, 44);
    }else if (indexPath.row < (shop.items.count + 1) ){
        return CGSizeMake(KWIDTH-30, 100);
    }else if (indexPath.row == shop.items.count + 1){
        return CGSizeMake(KWIDTH-30, 44);
    }else if (indexPath.row == shop.items.count + 2){
        if (shop.actionArr.count) {
            return CGSizeMake(KWIDTH-30, 44);
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(0, 15, 15, 15);
//    return UIEdgeInsetsMake(0, 0, 10, 0);

}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    WGShopModel *shop;
    if (indexPath.section < _dataArr.count) {
        shop = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        WGOrderHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderHeadCollectionViewCell" forIndexPath:indexPath];
        cell.model = shop;
        [cell setClickMsgBlock:^{
            if (shop.userId.length == 0) {
                return;
            }
//            [WGIMManage dd_IM_TurnChartDetailWithUserId:shop.userId FromType:@"4" fromModel:shop parm:shop.userId title:shop.userName vc:weakSelf success:^(id  _Nonnull data, NSString * _Nonnull msg) {
//                
//            } fail:^(id  _Nonnull data, NSInteger errorType, NSString * _Nonnull msg) {
//                
//            }];
        }];
        return cell;
    }else if (indexPath.row < (shop.items.count + 1) ){
        WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
        cell.type = 0;
        cell.detailModel = shop.items[indexPath.row - 1];
        cell.orderModel = shop;
        return cell;
    }else if (indexPath.row == shop.items.count + 1){
        WGOrderListALlCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderListALlCollectionViewCell" forIndexPath:indexPath];
        cell.model = shop;
        return cell;
    }else if (indexPath.row == shop.items.count + 2){
        WGOrderFootCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderFootCollectionViewCell" forIndexPath:indexPath];
        cell.model = shop;
        cell.orderAction = ^(OrderActionType type) {
            if (type < 5) {
                if (type == OrderActionTypeDeliver || type == OrderActionTypeVerifyPayNO ||
                    type == OrderActionTypeContinueDeliver ) {
                    if (shop.operationState == 1) {
                        return [YJProgressHUD showMessage:@"退款期间不能发货"];
                    }
                    if (shop.orderType == 1) {
                        return [YJProgressHUD showMessage:@"拼团期间不能发货"];
                    }
                }
                WGOrderActionView *orderAction = [WGOrderActionView new];
                orderAction.subView = weakSelf.navigationController.view;
                orderAction.actionType = type;
                [orderAction show];
                orderAction.deliverAction = ^(WGDeliveryModel * _Nonnull model, NSString * _Nonnull expressNo) {
                    NSString *deliveryTypeName=@"";
                    if ([model.deliveryTypeName isEqualToString:@"自定义快递公司"]) {
                        deliveryTypeName = model.customTypeName;
                    }else {
                        deliveryTypeName = model.deliveryTypeName;
                    }
                    [YJProgressHUD showLoading:@""];
                    [WGOrderViewModel sendGoodsOrderNo:shop.orderNo deliveryTypeId:model.Id deliveryTypeName:deliveryTypeName expressNo:expressNo Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"发货成功"];
                            [weakSelf requestDetail:indexPath.section order:shop];

                        }else{
                            [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                    }];
                };
                
                orderAction.noteAction = ^(NSString * _Nonnull note) {
                    [YJProgressHUD showLoading:@""];
                    [WGOrderViewModel addOrderRemark:note OrderNo:shop.orderNo Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"添加备注成功"];
                            [weakSelf requestDetail:indexPath.section order:shop];
                        }else{
                            [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                    }];
                };
                
                orderAction.verifyPayNo = ^(NSString * _Nonnull payNo, NSString * _Nonnull orderNo) {
                    if (orderNo.length && ![orderNo isEqualToString:shop.orderNo]) {
//                        KPOP(@"消费码错误,请重新输入");
                        [YJProgressHUD showSuccess:@"消费码错误,请重新输入"];
                        return ;
                    }
                    [YJProgressHUD showLoading:@""];
                    [WGOrderViewModel confirmConsumerCodeOrderNo:shop.orderNo consumerCode:payNo Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"确认消费码成功"];
                            [weakSelf requestDetail:indexPath.section order:shop];
                        }else{
                            [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                        
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                    }];
                };
                orderAction.receviedAction = ^(WGShopRecivedModel * _Nonnull receivedMode) {
                    [YJProgressHUD showLoading:@""];
                    
                    [WGOrderViewModel takeServiceOrderOrderNo:shop.orderNo receivedId:receivedMode.Id receivedName:receivedMode.name Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"接单成功"];
                            [weakSelf requestDetail:indexPath.section order:shop];
                        }else{
                            [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                        
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                        
                    }];
                };
            }else if (type == OrderActionTypeDistribution){
//                WGExpressView *pop = [[WGExpressView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 170 + Home_Indicator_HEIGHT)];
//                pop.model = weakSelf.dataArr[indexPath.section];
//                pop.vc = weakSelf;
//                [weakSelf presentSemiView:pop withOptions:@{
//                                                            KNSemiModalOptionKeys.pushParentBack : @(NO),
//                                                            KNSemiModalOptionKeys.parentAlpha : @(0.8)
//                                                            }];
                WGExpressViewController *exp = [WGExpressViewController new];
                exp.shop = shop;
                [weakSelf.navigationController pushViewController:exp animated:YES];
                
                
            }else if (type == OrderActionTypeCancle){
                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定取消订单吗?" sureBtn:@"确认" cancleBtn:@"取消"];
                [alertView showAnimation];
                [alertView setSureClick:^(NSString * _Nonnull string) {
                    [weakSelf requestDetail:indexPath.section order:shop];
                    
                }];
                
            }else if (type == OrderActionTypeDone){
                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定完成订单吗?" sureBtn:@"确认" cancleBtn:@"取消"];
                [alertView showAnimation];
                [alertView setSureClick:^(NSString * _Nonnull string) {
                    [YJProgressHUD showLoading:@""];
                    [WGOrderViewModel confirmServiceOrderNo:shop.orderNo Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"确认订单成功"];
                            [weakSelf requestDetail:indexPath.section order:shop];
                        }else{
                            [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                        
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                    }];
                }];
            }else if (type == OrderActionTypeRemindPay){
                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定提醒付款吗?" sureBtn:@"确认" cancleBtn:@"取消"];
                [alertView showAnimation];
                [alertView setSureClick:^(NSString * _Nonnull string) {
                    [YJProgressHUD showLoading:@""];
                    [WGOrderViewModel remindPayOrderNo:shop.orderNo Completion:^(id  _Nonnull responesObj) {
                        [YJProgressHUD hideHUD];
                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                        if (model.state) {
                            [YJProgressHUD showSuccess:@"提醒成功"];
                        }else{
                                [YJProgressHUD showError:responesObj[@"msg"]];
                            }
                    } failure:^(NSError * _Nonnull error) {
                        [YJProgressHUD hideHUD];
                        [YJProgressHUD showError:REQUESTERR];
                    }];
                }];
            }
        };
        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    WGOrderDetailViewController *detai = [WGOrderDetailViewController new];
    detai.orderModel = _dataArr[indexPath.section];
    [self.navigationController pushViewController:detai animated:YES];
}


/**
 请求订单列表
 */
- (void)request{
    YXWeakSelf;
    if (_page == 1&&_dataArr.count == 0) {
        [YJProgressHUD showLoading:@""];
    }
    NSString *temStatus;
    if ([_orderStatus isEqualToString:@"all"]||_orderStatus.integerValue == 1001) {
        
    }else{
        temStatus = _orderStatus;
    }
    [WGOrderViewModel getOrderShopId:nil OrderStatus:temStatus StartTime:nil EndTime:nil PayMethod:nil Condition:_keyWord TimeType:nil DistributionType:nil PayType:nil SearchType:nil orderNo:nil pageNum:[NSString stringWithFormat:@"%ld",(long)_page] pageSize:@"20" Completion:^(id  _Nonnull responesObj) {
        if (weakSelf.page == 1 && weakSelf.dataArr.count == 0) {
            [YJProgressHUD hideHUD];
        }
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }

        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            NSArray *tem = [WGShopModel mj_objectArrayWithKeyValuesArray:model.data];
            [weakSelf.dataArr addObjectsFromArray:tem];
            [weakSelf.collectionView reloadData];
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
            if ([tem count] < 20) {
                [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }else{
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        
    }];
}

/**
 更新订单详情

 @param row 当前点击
 @param order 订单
 */
- (void)requestDetail:(NSInteger)row order:(WGShopModel *)order{
    YXWeakSelf;
//    [YJProgressHUD showLoading:@""];
    
    [WGOrderViewModel getOrderShopId:nil OrderStatus:nil StartTime:nil EndTime:nil PayMethod:nil Condition:nil TimeType:nil DistributionType:nil PayType:nil SearchType:nil orderNo:order.orderNo pageNum:nil pageSize:@"20" Completion:^(id  _Nonnull responesObj) {
//        [YJProgressHUD hideHUD];
        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            NSArray *tem = [WGShopModel mj_objectArrayWithKeyValuesArray:model.data];
            if (tem.count) {
                WGShopModel *temMOdel = tem[0];
                if (temMOdel.orderState != order.orderState) {
                    [weakSelf.dataArr removeObjectAtIndex:row];
                }else{
                    [weakSelf.dataArr replaceObjectAtIndex:row withObject:temMOdel];
                }
            }
            [weakSelf.collectionView reloadData];
            
            // 通知更新订单角标
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNoticeOrderBadge object:nil];
        }else{
            [YJProgressHUD showError:REQUESTERR];
        }
    } failure:^(NSError * _Nonnull error) {
//        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
}

/**
 开始搜索
 */
- (void)seachBtnClick{
    if (_keyWord.length) {
        _page = 1;
        [self request];
    }
}

/**
 设置导航栏
 */
- (void)setNav {
    self.navigationItem.titleView = self.searchView;
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:self.searchBtn];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray new];
    }
    return _dataArr;
}
- (UIButton *)searchBtn {
    if (!_searchBtn) {
        _searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0, 36, 20)];
        [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
        [_searchBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_searchBtn addTarget:self action:@selector(seachBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchBtn;
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
//        YXSpellGroupCollectFlowLayout *flowLayout = [[YXSpellGroupCollectFlowLayout alloc] init];

        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.emptyDataSetSource = self;
        _collectionView.emptyDataSetDelegate = self;
//        _collectionView.backgroundColor = [UIColor colorWithRed:248.0/255 green:248.0/255 blue:248.0/255 alpha:1];
        _collectionView.backgroundColor = color_LineColor;
        
        [_collectionView registerClass:[WGOrderHeadCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderHeadCollectionViewCell"];
        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDisTypeCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell"];
        [_collectionView registerClass:[WGOrderFootCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderFootCollectionViewCell"];
        [_collectionView registerClass:[WGOrderListALlCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderListALlCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无订单" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf request];
        }];
    }
    return _collectionView;
}
- (ZLJSearchGoodsView *)searchView{
    if (!_searchView) {
        DDWeakSelf
        _searchView = [[ZLJSearchGoodsView alloc] initWithPlaceHoder:@"请输入"];
        _searchView.center = CGPointMake(self.navigationController.navigationBar.centerX, self.navigationController.navigationBar.centerY);
        _searchView.characterChange = ^(NSString * _Nonnull keyWord) {
            weakSelf.keyWord = keyWord;
        };
        _searchView.search = ^(NSString * _Nonnull keyWord) {
            if (keyWord.length) {
                weakSelf.keyWord = keyWord;
                [weakSelf seachBtnClick];
            }else{
                KPOP(@"请输入订单号、收货人姓名、手机号");
            }
        };
    }
    return _searchView;
}

- (UIView *)listView {
    return self.view;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
