//
//  YXRefundReasonViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import <HWPop.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXRefundReasonViewController : DDBaseViewController

@property (nonatomic ,copy) void(^selectRefundReasonBlock)(NSString *refundReason);

@property (nonatomic ,strong) NSString *refundReason;

@end

NS_ASSUME_NONNULL_END
