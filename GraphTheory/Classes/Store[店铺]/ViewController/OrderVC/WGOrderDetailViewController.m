//
//  WGOrderDetailViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderDetailViewController.h"
#import "WGExpressViewController.h"
#import "YXApplyRefundViewController.h"

//#import "YXOrderSubmitBottomView.h"
#import "YXPublicCollectionReusableView.h"
#import "YXOrderGoodFooterCollectionReusableView.h"

#import "WGOrderDetailInfoCollectionViewCell.h"
#import "WGOrderDetailNoteCollectionViewCell.h"
#import "WGOderDetailPriceInfoCollectionViewCell.h"
#import "WGOrderDetailStatusCollectionReusableView.h"
#import "WGOrderDetailAddressCollectionViewCell.h"
#import "WGOrderDetailALLCollectionViewCell.h"
#import "WGOrderDisTypeCollectionViewCell.h"
#import "WGOrderGoodCollectionViewCell.h"
#import "WGOrderViewModel.h"
#import "WGOrderActionView.h"
#import "WGExpressView.h"
#import "UIViewController+KNSemiModal.h"


@interface WGOrderDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic ,strong) UICollectionView *collectionView;
//@property (nonatomic ,strong) YXOrderSubmitBottomView *bottomView;
@property (nonatomic ,strong) NSDictionary *kehuInfoMapDic;
@property (nonatomic ,strong) NSString *orderRemark;

@end

@implementation WGOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    self.view.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
    
    [self request];
    

    [self loadSubViews];
}

- (void)loadSubViews {
    
//    [self.view addSubview:self.collectionView];
//    [self.view addSubview:self.bottomView];
//    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.right.equalTo(self.view);
//        make.bottom.equalTo(self.view.mas_bottom).mas_offset(-Home_Indicator_HEIGHT);
//        make.height.mas_equalTo(74);
//    }];
//    if (_orderModel.actionArr.count) {
//        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(0);
//        }];
//    }
//
//    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.top.and.right.equalTo(self.view);
//        make.bottom.equalTo(self.bottomView.mas_top);
//    }];
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.orderRemark.length > 0 && _orderModel.orderState == 1) {
        return 5;
    }else {
        return 4;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;///订单状态以及地址
    }else if (section  == 1){
        return _orderModel.items.count + 1;//商品等信息
    }else if (section == 2){
        return _orderModel.priceArr.count + 1;///订单金额的信息
    }else if (section == 3){ //订单编号等信息
        return 1;
    }else if (section == 4) { //订单备注
        return 1;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH, 80);
    }else if (indexPath.section  == 1){
        if (indexPath.row == 0) {
            return CGSizeMake(KWIDTH, 44);
        }
        return CGSizeMake(KWIDTH, 100);
    }else if (indexPath.section == 2){
        return CGSizeMake(KWIDTH, 44);
    }else if (indexPath.section == 3){
        return CGSizeMake(KWIDTH, _orderModel.infoArr.count > 7?_orderModel.infoArr.count:7 * 25 + 20) ;
    }else if (indexPath.section == 4){
        CGFloat height = [UILabel heightForString:self.orderRemark fontSize:14.0f andWidth:KWIDTH - 30];
        return CGSizeMake(KWIDTH, height + 20);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 1 && _orderModel.orderState == 1) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 10, 0);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(KWIDTH, 68);
    }else if (section == 2 || section == 3|| section == 4){
        return CGSizeMake(KWIDTH, 44);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 1 && _orderModel.orderState == 1) {
        return CGSizeMake(KWIDTH, 54);
    }
    return CGSizeZero;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section ==  0) {
            WGOrderDetailStatusCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGOrderDetailStatusCollectionReusableView" forIndexPath:indexPath];
            view.orderModel = _orderModel;
            return view;
        }else if (indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4) {
            YXPublicCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
            if (indexPath.section == 2) {
                headerView.titleLab.text = @"费用明细";
            }else if (indexPath.section == 3) {
                headerView.titleLab.text = @"订单信息";
            }else if (indexPath.section == 4) {
                headerView.titleLab.text = @"订单备注";
            }
            return headerView;
        }
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 1 && _orderModel.orderState == 1) {
            YXOrderGoodFooterCollectionReusableView *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXOrderGoodFooterCollectionReusableView" forIndexPath:indexPath];
            YXWeakSelf
            [footerView setClickRefundBtnBlock:^{
                YXApplyRefundViewController *vc = [YXApplyRefundViewController new];
                vc.orderModel = weakSelf.orderModel;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }];
            return footerView;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    if (indexPath.section == 0) {
        WGOrderDetailAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDetailAddressCollectionViewCell" forIndexPath:indexPath];
        cell.model = _orderModel;
        return cell;
    }else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            WGOrderDisTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell" forIndexPath:indexPath];
            cell.detailModel = _orderModel;
            return cell;
        }else if (indexPath.row <= _orderModel.items.count){
            WGOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell" forIndexPath:indexPath];
            cell.type = 1;
            cell.detailModel = _orderModel.items[indexPath.row - 1];
            cell.isHideChange = _orderModel.orderState != 0;
            cell.changePriceAction = ^(NSString * _Nonnull price) {
                [YJProgressHUD showLoading:@""];
                NSMutableDictionary *dic = [NSMutableDictionary new];
                [dic setValue:weakSelf.orderModel.orderNo forKey:@"orderNo"];
                NSMutableArray *temArr = [NSMutableArray new];
                NSMutableDictionary *itemDic = [NSMutableDictionary new];
                WGOderDetailModel *model = weakSelf.orderModel.items[indexPath.row - 1];
                [itemDic setValue:model.itemId forKey:@"itemId"];
                [itemDic setValue:price forKey:@"itemPrice"];
                [itemDic setValue:model.itemNum forKey:@"itemNum"];
                [temArr addObject:itemDic];
                [dic setValue:temArr forKey:@"itemPrices"];

                [WGOrderViewModel updateServicePriceParm:dic Completion:^(id  _Nonnull responesObj) {
                    [YJProgressHUD hideHUD];
                    WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
                    if (model.state) {
                        [YJProgressHUD showSuccess:@"修改成功"];
                        [weakSelf request];
                    }else{
                        [YJProgressHUD showError:responesObj[@"msg"]];
                        }
                } failure:^(NSError * _Nonnull error) {
                    [YJProgressHUD hideHUD];
                    [YJProgressHUD showError:REQUESTERR];

                }];
            };
            return cell;
        }
    }else if (indexPath.section == 2){
        if (indexPath.row == _orderModel.priceArr.count) {
            WGOrderDetailALLCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDetailALLCollectionViewCell" forIndexPath:indexPath];
            cell.model = _orderModel;
            return cell;
        }else if (indexPath.row < _orderModel.priceArr.count){
            WGOderDetailPriceInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell" forIndexPath:indexPath];
            cell.model = _orderModel.priceArr[indexPath.row];
            return cell;
        }
        
    }else if (indexPath.section == 3){
        WGOrderDetailInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDetailInfoCollectionViewCell" forIndexPath:indexPath];
        cell.arr = _orderModel.infoArr;
        return cell;
    }else if (indexPath.section == 4) {
        WGOrderDetailNoteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGOrderDetailNoteCollectionViewCell" forIndexPath:indexPath];
        cell.noteStr = _orderRemark;
        return cell;
    }
    return nil;
    
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)request{
    YXWeakSelf;
//    [YJProgressHUD showLoading:@""];
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [WGOrderViewModel queryOrderInfoWithOrderNo:_orderModel.orderNo?:_orderNo Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.kehuInfoMapDic = responesObj[@"data"][@"kehuInfoMap"];
            weakSelf.orderRemark = responesObj[@"data"][@"itemInfoMap"][@"orderRemark"];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        dispatch_group_leave(group);
    } failure:^(NSError * _Nonnull error) {
        dispatch_group_leave(group);
    }];
        
    dispatch_group_enter(group);
    [WGOrderViewModel getOrderShopId:nil OrderStatus:nil StartTime:nil EndTime:nil PayMethod:nil Condition:nil TimeType:nil DistributionType:nil PayType:nil SearchType:nil orderNo:_orderModel.orderNo?:_orderNo pageNum:nil pageSize:@"20" Completion:^(id  _Nonnull responesObj) {
//        [YJProgressHUD hideHUD];
        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            NSArray *tem = [WGShopModel mj_objectArrayWithKeyValuesArray:model.data];
            if (tem.count) {
                weakSelf.orderModel = tem[0];
                [weakSelf.orderModel setSystemNo:weakSelf.kehuInfoMapDic[@"systemNo"]];
                [weakSelf.orderModel setTradeNo:weakSelf.kehuInfoMapDic[@"tradeNo"]];
//                weakSelf.bottomView.orderModel = tem[0];
            }
        }else{
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
        dispatch_group_leave(group);
    } failure:^(NSError * _Nonnull error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
        if (weakSelf.orderModel.actionArr.count) {
//            [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(74);
//            }];
        }else{
//            [weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.height.mas_equalTo(0);
//            }];
        }
        [weakSelf.collectionView reloadData];
    });
    
}

//- (YXOrderSubmitBottomView *)bottomView {
//    if (!_bottomView) {
//        YXWeakSelf;
//        _bottomView = [[YXOrderSubmitBottomView alloc] init];
//        _bottomView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
//
//        _bottomView.orderModel = _orderModel;
//        [_bottomView setOrderAction:^(OrderActionType type) {
//            if (type < 5) {
//                if (type == OrderActionTypeDeliver || type == OrderActionTypeVerifyPayNO ||
//                    type == OrderActionTypeContinueDeliver) {
//                    if (weakSelf.orderModel.operationState == 1) {
//                        return [YJProgressHUD showMessage:@"退款期间不能发货"];
//                    }
//                    if (weakSelf.orderModel.orderType == 1) {
//                        return [YJProgressHUD showMessage:@"拼团期间不能发货"];
//                    }
//                }
//                WGOrderActionView *orderAction = [WGOrderActionView new];
//                orderAction.subView = weakSelf.navigationController.view;
//                orderAction.note = weakSelf.orderRemark;
//                orderAction.actionType = type;
//                [orderAction show];
//                orderAction.deliverAction = ^(WGDeliveryModel * _Nonnull model, NSString * _Nonnull expressNo) {
//                    [YJProgressHUD showLoading:@""];
//                    NSString *deliveryTypeName=@"";
//                    if ([model.deliveryTypeName isEqualToString:@"自定义快递公司"]) {
//                        deliveryTypeName = model.customTypeName;
//                    }else {
//                        deliveryTypeName = model.deliveryTypeName;
//                    }
//                    [WGOrderViewModel sendGoodsOrderNo:weakSelf.orderModel.orderNo deliveryTypeId:model.Id deliveryTypeName:deliveryTypeName expressNo:expressNo Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"发货成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//                    }];
//                };
//                orderAction.noteAction = ^(NSString * _Nonnull note) {
//                    [YJProgressHUD showLoading:@""];
//                    [WGOrderViewModel addOrderRemark:note OrderNo:weakSelf.orderModel.orderNo Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"添加备注成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//                    }];
//                };
//                orderAction.verifyPayNo = ^(NSString * _Nonnull payNo, NSString * _Nonnull orderNo) {
//                    if (orderNo.length && ![orderNo isEqualToString:weakSelf.orderModel.orderNo]) {
//                        KPOP(@"消费码错误,请重新输入");
//                        return ;
//                    }
//                    [YJProgressHUD showLoading:@""];
//                    [WGOrderViewModel confirmConsumerCodeOrderNo:weakSelf.orderModel.orderNo consumerCode:payNo Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"确认消费码成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//                    }];
//                };
//                orderAction.receviedAction = ^(WGShopRecivedModel * _Nonnull receivedMode) {
//                    [YJProgressHUD showLoading:@""];
//
//                    [WGOrderViewModel takeServiceOrderOrderNo:weakSelf.orderModel.orderNo receivedId:receivedMode.Id receivedName:receivedMode.name Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"接单成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//
//                    }];
//                };
//
//            }else if (type == OrderActionTypeDistribution){
////                WGExpressView *pop = [[WGExpressView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 170 + Home_Indicator_HEIGHT)];
////                pop.model = weakSelf.orderModel;
////                pop.vc = weakSelf;
////                [weakSelf presentSemiView:pop withOptions:@{
////                                                            KNSemiModalOptionKeys.pushParentBack : @(NO),
////                                                            KNSemiModalOptionKeys.parentAlpha : @(0.8)
////                                                            }];
//                WGExpressViewController *exp = [WGExpressViewController new];
//                exp.shop = weakSelf.orderModel;
//                [weakSelf.navigationController pushViewController:exp animated:YES];
//
//
//            }else if (type == OrderActionTypeCancle){
//                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定取消订单吗?" sureBtn:@"确认" cancleBtn:@"取消"];
//                [alertView showAnimation];
//                [alertView setSureClick:^(NSString * _Nonnull string) {
//                    [weakSelf request];
//                }];
//
//            }else if (type == OrderActionTypeDone){
//                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定完成订单吗?" sureBtn:@"确认" cancleBtn:@"取消"];
//                [alertView showAnimation];
//                [alertView setSureClick:^(NSString * _Nonnull string) {
//                    [YJProgressHUD showLoading:@""];
//                    [WGOrderViewModel confirmServiceOrderNo:weakSelf.orderModel.orderNo Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"确认订单成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//                    }];
//                }];
//            }else if (type == OrderActionTypeRemindPay){
//                YXCustomAlertActionView *alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:AlertText Title:@"提示" Message:@"确定提醒付款吗?" sureBtn:@"确认" cancleBtn:@"取消"];
//                [alertView showAnimation];
//                [alertView setSureClick:^(NSString * _Nonnull string) {
//                    [YJProgressHUD showLoading:@""];
//                    [WGOrderViewModel remindPayOrderNo:weakSelf.orderModel.orderNo Completion:^(id  _Nonnull responesObj) {
//                        [YJProgressHUD hideHUD];
//                        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
//                        if (model.state) {
//                            [YJProgressHUD showSuccess:@"提醒成功"];
//                            [weakSelf request];
//                        }else{
//                            [YJProgressHUD showError:responesObj[@"msg"]];
//                        }
//                    } failure:^(NSError * _Nonnull error) {
//                        [YJProgressHUD hideHUD];
//                        [YJProgressHUD showError:REQUESTERR];
//
//                    }];
//                }];
//            }
//        }];
//    }
//    return _bottomView;
//}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        
        [_collectionView registerClass:[WGOrderDetailALLCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDetailALLCollectionViewCell"];
        [_collectionView registerClass:[WGOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderGoodCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDetailInfoCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDetailInfoCollectionViewCell"];
        [_collectionView registerClass:[WGOderDetailPriceInfoCollectionViewCell class] forCellWithReuseIdentifier:@"WGOderDetailPriceInfoCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDetailAddressCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDetailAddressCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDisTypeCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell"];
        [_collectionView registerClass:[WGOrderDetailNoteCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDetailNoteCollectionViewCell"];

        
        [_collectionView registerClass:[WGOrderDetailStatusCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGOrderDetailStatusCollectionReusableView"];
        
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXPublicCollectionReusableView"];
        
        [_collectionView registerClass:[YXOrderGoodFooterCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXOrderGoodFooterCollectionReusableView"];
        
    }
    return _collectionView;
}

- (NSDictionary *)kehuInfoMapDic {
    if (!_kehuInfoMapDic) {
        _kehuInfoMapDic = [NSDictionary dictionary];
    }
    return _kehuInfoMapDic;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
