//
//  WGExpressDetail ViewController.h
//  ZanXiaoQu
//
//  Created by wanggang on 2019/8/13.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 物流信息
 */
@class WGExpressModel;
@interface WGExpressDetailViewController : DDBaseViewController

@property (nonatomic ,strong) WGExpressModel *expressModel;


@end

NS_ASSUME_NONNULL_END
