//
//  WGOrderViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGOrderViewController.h"
#import "ZLJSearchGoodsView.h"
#import <JXCategoryView.h>
#import "WGOrderListViewController.h"
#import "WGOrderViewModel.h"
#import "WGDeliveryModel.h"
#import "WGOrderModel.h"
@interface WGOrderViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) UIButton *searchBtn;

@property (nonatomic, strong) NSArray *titles;//标题数组
@property (nonatomic, strong) NSArray *keys;//标题数组


@property (nonatomic, strong) JXCategoryNumberView *categoryView;//类型选择器

@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;//类型视图选择器

@end

@implementation WGOrderViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单";
    
    //0待付款1待发货 2待服务3已发货4已完成5已关闭6售后、退款

    [self requestAlldeliy];
    [self queryOrderStateCount];
    _titles = @[@"待发货",@"待服务",@"已发货",@"已完成",@"已关闭",@"待付款",];
    _keys = @[@"1",@"2",@"3",@"4",@"5",@"0",];
    [self addSubviewsToVcView];
    _categoryView.titles = _titles;
    _categoryView.counts = @[@0,@0,@0,@0,@0,@0]; // 角标显示数量

    // 通知更新订单角标
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queryOrderStateCount) name:kNotificationNoticeOrderBadge object:nil];
    
}

/**
 查询所有快递类型
 */
- (void)requestAlldeliy{
    if (![YXUserInfoManager shareUserInfoManager].allDelivery.count) {
        [YJProgressHUD showLoading:@""];
    }
    [WGOrderViewModel queryAllDeliveryCompletion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        NSMutableArray *tem = [NSMutableArray array];
        [tem removeAllObjects];
        WGRequestModel *model = [WGRequestModel mj_objectWithKeyValues:responesObj];
        if (model.state) {
            WGDeliveryModel *deModel = [WGDeliveryModel new];
            [deModel setDeliveryTypeName:@"自定义快递公司"];
            tem = [WGDeliveryModel mj_objectArrayWithKeyValuesArray:model.data];
            [tem addObject:deModel];
            [YXUserInfoManager shareUserInfoManager].allDelivery = tem;
        }else{
            [YJProgressHUD showError:REQUESTERR];
        }

    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
}

/**
查询订单角标数量
*/
- (void)queryOrderStateCount {
    
    YXWeakSelf
    [WGOrderViewModel queryOrderStateCountCompletion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            WGOderBadgeModel *badgeModel = [WGOderBadgeModel mj_objectWithKeyValues:responesObj[@"data"]];
            NSMutableArray *numArr = [NSMutableArray array];
            if (badgeModel) {
                [numArr addObject:@(badgeModel.state1)];
                [numArr addObject:@(badgeModel.state2)];
                [numArr addObject:@(0)];
                [numArr addObject:@(0)];
                [numArr addObject:@(0)];
                [numArr addObject:@(badgeModel.state0)];
            }
            weakSelf.categoryView.counts = numArr; // 角标显示数量
            [weakSelf.categoryView reloadDataWithoutListContainer]; // 刷新角标数据
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
    }];
    
}


#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}
#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    WGOrderListViewController *list = [[WGOrderListViewController alloc] init];
    list.orderStatus = _keys[index];
    return list;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}
#pragma mark - Intial Methods
- (void)addSubviewsToVcView{//添加子视图
    
    UIView *topBackView = [UIView new];
    topBackView.backgroundColor = [UIColor whiteColor];
    
    [topBackView addSubview:self.searchBtn];
    [self.view addSubview:topBackView];

    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.listContainerView];
    [topBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.height.mas_equalTo(@65);
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(0);
        }
    }];

    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(topBackView.mas_right).offset(-15);
        make.left.equalTo(topBackView.mas_left).offset(15);
        make.centerY.equalTo(topBackView.mas_centerY);
        make.height.mas_equalTo(@44);
    }];
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.height.mas_equalTo(@44);
        make.top.mas_equalTo(self.searchBtn.mas_bottom);
    }];
    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.categoryView.mas_bottom);
        if (@available(iOS 11.0,*)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(0);
        }
    }];
}
- (JXCategoryNumberView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryNumberView alloc]init];
        _categoryView.delegate = self;
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleFont = [UIFont systemFontOfSize:16.0];
        _categoryView.titleColor = color_TextOne;
        _categoryView.titleSelectedColor = APPTintColor;
        _categoryView.defaultSelectedIndex = _selectIndex;
        _categoryView.numberBackgroundColor = [UIColor redColor];
        _categoryView.numberLabelFont = [UIFont systemFontOfSize:10.0f];
        _categoryView.numberLabelHeight = 12.0f;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorWidth = JXCategoryViewAutomaticDimension;
        //可以试试宽度补偿
        lineView.indicatorColor = APPTintColor;
        lineView.indicatorHeight = 2;
        lineView.indicatorWidthIncrement = 0;
        lineView.verticalMargin = 6;
        _categoryView.indicators = @[lineView];
        _categoryView.listContainer = self.listContainerView;
        _listContainerView = self.listContainerView;
    }
    return _categoryView;
}
- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_CollectionView delegate:self];
//        _listContainerView = [[JXCategoryListContainerView alloc] initWithDelegate:self];
    }
    return _listContainerView;
}
- (UIButton *)searchBtn{
    if (!_searchBtn) {
        DDWeakSelf
        _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchBtn setTitle:@"订单号、收货人姓名、手机号" forState:UIControlStateNormal];
        [_searchBtn setTitleColor:[UIColor colorWithHexString:@"#8E8E93"] forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        _searchBtn.backgroundColor = [UIColor colorWithHexString:@"#F8F8F8"];
        _searchBtn.layer.cornerRadius = 10.0f;
        _searchBtn.layer.masksToBounds  = YES;
        [_searchBtn addAction:^(UIButton * _Nonnull btn) {
            WGOrderListViewController *sear = [WGOrderListViewController new];
            sear.orderStatus = @"1001";
            [weakSelf.navigationController pushViewController:sear animated:YES];
        }];
    }
    return _searchBtn;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
