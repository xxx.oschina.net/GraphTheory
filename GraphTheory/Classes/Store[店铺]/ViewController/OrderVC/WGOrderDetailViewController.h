//
//  WGOrderDetailViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 订单详情
 */
@interface WGOrderDetailViewController : DDBaseViewController

@property (nonatomic, strong) WGShopModel *orderModel;
@property (nonatomic, strong) NSString *orderNo;



@end

NS_ASSUME_NONNULL_END
