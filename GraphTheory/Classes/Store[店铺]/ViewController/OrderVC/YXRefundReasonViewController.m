//
//  YXRefundReasonViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXRefundReasonViewController.h"
#import "WGStoreSelctShipTableViewCell.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "WGButtonTableViewCell.h"
#import "WGOrderViewModel.h"
#import "YXRefundModel.h"
@interface YXRefundReasonViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIButton *closeBtn;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) NSMutableArray *selectArr;
@end

@implementation YXRefundReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"退款原因";
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithCustomView:self.closeBtn];
    self.navigationItem.rightBarButtonItem = right;
    
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    if (self.refundReason.length > 0) {
        [self.selectArr addObject:self.refundReason];
    }
    
    [self loadData];

    [self loadSubViews];
  

}

- (void)loadData {
    YXWeakSelf
    [WGOrderViewModel queryRefundReasonCompletion:^(id  _Nonnull responesObj) {
        weakSelf.dataArr = [YXRefundReasonModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

- (void)loadSubViews {
    
    [self.view layoutIfNeeded];
      CGFloat h = 291;
      
      self.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
      self.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
      [self.navigationController.view layoutIfNeeded];
      self.navigationController.contentSizeInPop = CGSizeMake(SCREEN_WIDTH, h);
      self.navigationController.contentSizeInPopWhenLandscape = CGSizeMake(SCREEN_WIDTH, h);
      [self.view addSubview:self.saveBtn];
      [self.view addSubview:self.tableView];
      [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
          make.bottom.equalTo(self.view.mas_bottom);
          make.left.equalTo(self.view.mas_left);
          make.right.equalTo(self.view.mas_right);
          make.height.equalTo(@44.0);
      }];
      [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
          if (@available(iOS 11.0,*)) {
              make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.top.equalTo(self.view.mas_top);
              make.left.equalTo(self.view.mas_left);
              make.right.equalTo(self.view.mas_right);
          }
          make.bottom.equalTo(self.saveBtn.mas_top);
      }];
      
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"请选择退款原因";
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = color_TextTwo;
    header.textLabel.font = [UIFont systemFontOfSize:12];
    header.contentView.backgroundColor = HEX_COLOR(@"#F8F8F8");
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WGStoreSelctShipTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelctShipTableViewCell" forIndexPath:indexPath];
    cell.type = 0;
    YXRefundReasonModel *model = self.dataArr[indexPath.row];
    cell.titleStr = model.busRefundReason;
    cell.isSelect = [self.selectArr containsObject:model.busRefundReason];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self.selectArr removeAllObjects];
    YXRefundReasonModel *model = self.dataArr[indexPath.row];
    if ([self.selectArr containsObject:model.busRefundReason]) {
        [self.selectArr removeObject:model.busRefundReason];
     }else{
         [self.selectArr addObject:model.busRefundReason];
     }
    [self.tableView reloadData];
    
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.separatorColor = color_LineColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.showsVerticalScrollIndicator = NO;
        [_tableView registerClass:[WGStoreSelctShipTableViewCell class] forCellReuseIdentifier:@"WGStoreSelctShipTableViewCell"];
        [_tableView registerClass:[WGButtonTableViewCell class] forCellReuseIdentifier:@"WGButtonTableViewCell"];
    }
    return _tableView;
}
- (UIButton *)closeBtn {
    if (!_closeBtn) {
        _closeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _closeBtn.frame = CGRectMake(0, 0, 44, 44);
        _closeBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
        [_closeBtn setImage:[UIImage imageNamed:@"good_close"] forState:(UIControlStateNormal)];
        [_closeBtn addTarget:self action:@selector(close) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _closeBtn;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"完成" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    }
    return _saveBtn;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (NSMutableArray *)selectArr {
    if (!_selectArr) {
        _selectArr = [NSMutableArray array];
    }
    return _selectArr;
}


- (void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)save {
    
    if (self.selectArr.count == 0) {
        return [YJProgressHUD showMessage:@"请选择退款原因"];
    }
    
    if (self.selectArr.count > 0) {
        NSString *text = [self.selectArr firstObject];
        if (self.selectRefundReasonBlock) {
            self.selectRefundReasonBlock(text);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
