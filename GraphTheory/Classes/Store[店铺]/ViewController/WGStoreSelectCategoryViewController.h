//
//  WGStoreSelectCategoryViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelectCategoryViewController : DDBaseViewController

@property (nonatomic ,copy)void(^clickSelectCategoryBlock)(NSString *categoryId,NSString *categoryName);

@end

NS_ASSUME_NONNULL_END
