//
//  WGStoreSelectExpressAreaViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectExpressAreaViewController.h"
#import "WGStoreSelectExpressAreaTableViewCell.h"

@interface WGStoreSelectExpressAreaViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, strong) NSMutableArray *titleArr;
@property (nonatomic, strong) NSMutableArray *selctArr;



@end

@implementation WGStoreSelectExpressAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = @"快递配送区域";
    [self.view addSubview:self.saveBtn];
    [self.view addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-15);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.bottom.equalTo(self.view.mas_bottom).offset(-15);
            make.left.equalTo(self.view.mas_left).offset(15);
            make.right.equalTo(self.view.mas_right).offset(-15);
        }
        make.height.mas_equalTo(@44);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.bottom.equalTo(self.view.mas_bottom);
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
        }
        make.bottom.equalTo(self.saveBtn.mas_top);
    }];
}
- (void)save{
    
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *tem = self.titleArr[section];
    return tem.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.titleArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WGStoreSelectExpressAreaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelectExpressAreaTableViewCell" forIndexPath:indexPath];
    NSArray *tem = self.titleArr[indexPath.section];
    cell.titleStr = tem[indexPath.row];
    cell.isSelect = [_selctArr containsObject:indexPath];
    cell.isFirst = indexPath.row == 0;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.selctArr containsObject:indexPath]) {
        [_selctArr removeObject:indexPath];
    }else{
        [_selctArr addObject:indexPath];

    }
    [_tableView reloadData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.separatorColor = color_LineColor;
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[WGStoreSelectExpressAreaTableViewCell class] forCellReuseIdentifier:@"WGStoreSelectExpressAreaTableViewCell"];
    }
    return _tableView;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"保存" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.layer.cornerRadius = 4;
        _saveBtn.enabled = NO;
        _saveBtn.alpha = 0.5;
    }
    return _saveBtn;
}
- (NSMutableArray *)titleArr{
    if (!_titleArr) {
        _titleArr = [NSMutableArray new];
        NSArray *tem = @[@[@"全国"],@[@"河南省",@"郑州市",@"开封市",@"安阳市"],@[@"河南省",@"郑州市",@"开封市",@"安阳市"],@[@"河南省",@"郑州市",@"开封市",@"安阳市"]];
        [_titleArr addObjectsFromArray:tem];

    }
    return _titleArr;
}
- (NSMutableArray *)selctArr{
    if (!_selctArr) {
        _selctArr = [NSMutableArray new];
    }
    return _selctArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
