//
//  YXStoreViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXStoreViewController.h"
#import "WGMineSetionBackLayout.h"
#import "WGTodayHeadCollectionViewCell.h"
#import "WGTodayDetailCollectionViewCell.h"
#import "WGStoreOrderCollectionViewCell.h"
#import "WGStroeHeadCollectionReusableView.h"
#import "WGStoreMenuCollectionViewCell.h"
#import "WGLoginViewController.h"

#import "ZLJGoodsVC.h" // 商品列表
#import "YCMenuView.h"
#import "WGCreatStoreViewController.h"

#import "YXStoreViewModel.h"
#import "WGStoreModel.h"

#import "ZLJShopViewModle.h"
#import "ZLJShopByRelationIdModel.h" //右侧选择店铺数据源 - model

#import "WGOrderViewController.h"
#import "DDScanViewController.h"
#import "DDBaseNavigationController.h"


@interface YXStoreViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,WGCollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) UIButton *storeBtn;
@property (strong, nonatomic) UIButton *scanBtn;

@property (strong, nonatomic) NSMutableArray *storeArr;

@property (strong, nonatomic) WGStoreModel *storeModel;

@property (nonatomic,strong) NSMutableArray <ShopByRelationIdDataList *>*relationModelArray;//选择店铺数据源
@end

@implementation YXStoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = nil;
    self.navigationController.navigationBar.translucent = NO;

    [self loadData];
    
    [self.view addSubview:self.collectionView];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:self.storeBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.scanBtn];
    self.navigationItem.rightBarButtonItem = rightItem;

    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.edges.mas_equalTo(self.view).mas_offset(UIEdgeInsetsMake(0, 15, 0, 15));
        }
    }];
    DDWeakSelf
    YCMenuAction *action = [YCMenuAction actionWithTitle:@"店" image:nil handler:^(YCMenuAction *action) {
        NSLog(@"点击了%@",action.title);
        [weakSelf.storeBtn setTitle:action.title forState:UIControlStateNormal];
    }];
    YCMenuAction *action1 = [YCMenuAction actionWithTitle:@"小芳的小芳的小芳的小芳的小芳的" image:nil handler:^(YCMenuAction *action) {
        NSLog(@"点击了%@",action.title);
        [weakSelf.storeBtn setTitle:action.title forState:UIControlStateNormal];

    }];
    YCMenuAction *action2 = [YCMenuAction actionWithTitle:@"创建店铺" image:nil handler:^(YCMenuAction *action) {
        NSLog(@"点击了%@",action.title);
        WGCreatStoreViewController *creat = [WGCreatStoreViewController new];
        [weakSelf.navigationController pushViewController:creat animated:YES];
    }];


    [self.storeArr addObjectsFromArray:@[action,action1,action2]];

#warning mark -- 此方法仅用于赵龙杰调试使用
    self.relationModelArray = [NSMutableArray array];
    [self queryShopByRelationIdMethod];
}

#pragma mark - 请求数据
- (void)loadData {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@"加载中"];
    [YXStoreViewModel queryShopDetailCompletion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            
            // 无店铺
            if ([responesObj[@"data"] isKindOfClass:[NSString class]]) {
                
            }else if ([responesObj[@"data"] isKindOfClass:[NSDictionary class]]) { // 有店铺
                weakSelf.storeModel = [WGStoreModel mj_objectWithKeyValues:responesObj[@"data"][@"storeShopEntity"]];
                [weakSelf.storeBtn setTitle:weakSelf.storeModel.shopName forState:(UIControlStateNormal)];
            }
           
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];

    }];
    
}

#pragma mark - 点击导航栏按钮事件
- (void)changeStore{
    YCMenuView *view = [YCMenuView menuWithActions:self.storeArr width:140 relyonView:self.storeBtn];
    
    [view show];
}
- (void)scan{
    NSLog(@"扫描");
    DDWeakSelf;
    DDScanViewController *scan = [DDScanViewController new];
    scan.scanSuccess = ^(UIViewController * _Nonnull vc) {
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    DDBaseNavigationController *nav = [[DDBaseNavigationController alloc]initWithRootViewController:scan];
    [weakSelf presentViewController:nav animated:YES completion:nil];

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 7;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 8;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0) {
            return CGSizeMake(SCREEN_WIDTH - 30, 50);
        }else {
            return CGSizeMake((SCREEN_WIDTH - 30) / 3, 75);
        }
        
    }else if (indexPath.section == 1){
        return CGSizeMake((SCREEN_WIDTH - 30) / 3, 75);
    }else if (indexPath.section == 2){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 65);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(20, 0, 10, 0);
    }else if (section == 1){
        return UIEdgeInsetsMake(0, 0, 10, 0);
    }else if (section == 2){
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    return UIEdgeInsetsZero;
}
- (UIEdgeInsets)cccollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }else if (section == 1){
        return UIEdgeInsetsMake(-5, 0, 10, 0);
    }else if (section == 2){
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1||section == 2) {
        return CGSizeMake(KWIDTH - 30, 44);
    }else if (section == 0){
        return CGSizeMake(KWIDTH - 30, 0);
    }
    return CGSizeZero;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout backgroundColorForSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1 || section == 2){
        return 2;
    }
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1||indexPath.section == 2) {
            WGStroeHeadCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGStroeHeadCollectionReusableView" forIndexPath:indexPath];
            header.index = indexPath;
            return header;
        }
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0){
        if (indexPath.row == 0) {
            WGTodayHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGTodayHeadCollectionViewCell" forIndexPath:indexPath];
            return cell;
        }else {
            WGTodayDetailCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGTodayDetailCollectionViewCell" forIndexPath:indexPath];
            cell.index = indexPath;
            return cell;
        }
    }else if (indexPath.section == 1){
        WGStoreOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;

        return cell;
    }else if (indexPath.section == 2){
        WGStoreMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreMenuCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;

        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {// 店铺
            WGCreatStoreViewController *creat = [WGCreatStoreViewController new];
            creat.type = 1;
            creat.storeModel = self.storeModel;
            [self.navigationController pushViewController:creat animated:YES];
        }else if(indexPath.row == 1){
            [self jumpGoodsVC];
        }else if (indexPath.row == 2){
            WGOrderViewController *order = [WGOrderViewController new];
            [self.navigationController pushViewController:order animated:YES];
        }
    }else {
        
    }
    
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        WGMineSetionBackLayout * layout = [[WGMineSetionBackLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        [_collectionView registerClass:[WGTodayHeadCollectionViewCell class] forCellWithReuseIdentifier:@"WGTodayHeadCollectionViewCell"];
        [_collectionView registerClass:[WGTodayDetailCollectionViewCell class] forCellWithReuseIdentifier:@"WGTodayDetailCollectionViewCell"];
        [_collectionView registerClass:[WGStoreOrderCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreOrderCollectionViewCell"];
        [_collectionView registerClass:[WGStoreMenuCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreMenuCollectionViewCell"];
        [_collectionView registerClass:[WGStroeHeadCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"WGStroeHeadCollectionReusableView"];
    }
    return _collectionView;
}

- (UIButton *)storeBtn{
    if (!_storeBtn) {
        _storeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _storeBtn.frame = CGRectMake(0, 0, 120, 44);
        [_storeBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        [_storeBtn setTitle:@"小草的店" forState:UIControlStateNormal];
        [_storeBtn addTarget:self action:@selector(changeStore) forControlEvents:UIControlEventTouchUpInside];
        _storeBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;//左边排列
        _storeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_storeBtn setImage:[UIImage imageNamed:@"store_home_arrow"] forState:UIControlStateNormal];
        [_storeBtn setImgViewStyle:ButtonImgViewStyleRight imageSize:CGSizeMake(24, 12) space:5];
        [_storeBtn.titleLabel setSingleLineAutoResizeWithMaxWidth:100];
        _storeBtn.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _storeBtn.titleLabel.sd_layout
        .maxWidthIs(100)
        .leftSpaceToView(_storeBtn, 0);
        _storeBtn.imageView.sd_layout
        .leftSpaceToView(_storeBtn.titleLabel, 5)
        .centerYEqualToView(_storeBtn)
        .widthIs(12)
        .heightIs(6);
    }
    return _storeBtn;
}
- (UIButton *)scanBtn{
    if (!_scanBtn) {
        _scanBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _scanBtn.frame = CGRectMake(0, 0, 44, 44);
        [_scanBtn setImage:[UIImage imageNamed:@"store_home_scan"] forState:UIControlStateNormal];
        [_scanBtn addTarget:self action:@selector(scan) forControlEvents:UIControlEventTouchUpInside];

    }
    return _scanBtn;
}
- (NSMutableArray *)storeArr{
    if (!_storeArr) {
        _storeArr = [NSMutableArray new];
    }
    return _storeArr;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Life Cycle Methods

#pragma mark - Override Methods

#pragma mark - Intial Methods

#pragma mark - Network Methods
- (void)queryShopByRelationIdMethod{
    YXUserInfoModel *userModel = [YXUserInfoManager getUserInfo];
    DD_STRING_NN(userModel.unitId);
    [ZLJShopViewModle zljQueryShopByRelationId:userModel.unitId
                                    Completion:^(id  _Nonnull responesObj) {
                                        NSLog(@"%@",responesObj);
                                        ZLJShopByRelationIdModel *model = [ZLJShopByRelationIdModel mj_objectWithKeyValues:responesObj];
                                        for (ShopByRelationIdDataList *listModel in model.data.shopList) {
                                            [self.relationModelArray addObject:listModel];
                                        }
                                        
                                    } Failure:^(NSError * _Nonnull error) {
                                        
                                    }];
}
#pragma mark - Target Methods

#pragma mark - Public Methods

#pragma mark - Private Methods
//TODO:跳转到商品界面 -- zlj
- (void)jumpGoodsVC{
    ZLJGoodsVC *vc = [[ZLJGoodsVC alloc] init];
    if (self.relationModelArray.count > 0) {
        vc.listModel = self.relationModelArray[0];
    }
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Lazy Loads

#pragma mark - NSCopying  

#pragma mark - NSObject  Methods

#if TARGET_IPHONE_SIMULATOR 
-(void)injected {
    [self queryShopByRelationIdMethod];
}
#endif 
@end
