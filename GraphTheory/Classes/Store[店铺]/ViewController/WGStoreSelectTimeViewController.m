//
//  WGStoreSelectTimeViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectTimeViewController.h"
#import "WGStoreSelctTimeWeekCollectionViewCell.h"
#import "WGStoreSelctTimeCollectionViewCell.h"
#import "WGButtomCollectionViewCell.h"
#import "WGTitleCollectionReusableView.h"

@interface WGStoreSelectTimeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) WGStoreTimeModel *temTimeModel;

@end

@implementation WGStoreSelectTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    self.view.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    self.title = @"上门时间";
    _titleArr = @[@"周一",@"周二",@"周三",@"周四",@"周五",@"周六",@"周日"];
    if (_temTimeModel.shipTime.count == 0) {
        [_temTimeModel.shipTime addObject:[WGStoreOpenModel new]];
    }
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
}
- (void)setTimeModel:(WGStoreTimeModel *)timeModel{
    _timeModel = timeModel;
    _temTimeModel = [WGStoreTimeModel new];
    [_temTimeModel.shipWeek addObjectsFromArray:_timeModel.shipWeek];
    [_temTimeModel.shipTime addObjectsFromArray:_timeModel.shipTime];
}
- (BOOL)verifyData:(BOOL)showMsg{
    NSString *msg = @"";
    BOOL correct = YES;
    WGStoreOpenModel *open = _temTimeModel.shipTime.lastObject;
    if (open.beginTime.length == 0) {
        msg = @"请选择开始时间";
        correct = NO;
    }
    else if (open.endTime.length == 0) {
        msg = @"请选择选择结束时间";
        correct = NO;
    }else if (_temTimeModel.shipWeek.count == 0){
        msg = @"请选择上门时间";
        correct = NO;
    }
    if (showMsg) {
        KPOP(msg);
    }
    return correct;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return _titleArr.count;
    }else if (section == 1){
        return _temTimeModel.shipTime.count;
    }else if (section == 2){
        return 1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake((SCREEN_WIDTH - 30 - 33)/4, 28);
    }else if (indexPath.section == 1){
        return CGSizeMake(SCREEN_WIDTH, 32);
    }else if (indexPath.section == 2){
        return CGSizeMake(SCREEN_WIDTH - 30 , 44);
    }
    return CGSizeMake(SCREEN_WIDTH - 30 , 44);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return CGSizeMake(SCREEN_WIDTH - 30, 44);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 0, 15);
    }else if (section == 1){
        return UIEdgeInsetsMake(15, 0, 0, 0);
    }else if (section == 2){
        return UIEdgeInsetsMake(15, 15, 0, 15);
    }
    return UIEdgeInsetsZero;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 10;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 2) {
            WGTitleCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"WGTitleCollectionReusableView" forIndexPath:indexPath];
            header.titleStr = @"在非营业时间内用户进入店铺将会看到“店铺已打烊”";
            return header;
        }
    }
    return nil;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf;
    if (indexPath.section == 0) {
        
        WGStoreSelctTimeWeekCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell" forIndexPath:indexPath];
        cell.titleStr = _titleArr[indexPath.row];
        cell.isSelect = [_temTimeModel.shipWeek containsObject:_titleArr[indexPath.row]];
        return cell;
        
    }else if (indexPath.section == 1){
        WGStoreSelctTimeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGStoreSelctTimeCollectionViewCell" forIndexPath:indexPath];
        cell.model = _temTimeModel.shipTime[indexPath.row];
        cell.index = indexPath;
        cell.selectStoreTime = ^(WGStoreOpenModel * _Nonnull model, NSIndexPath * _Nonnull index, NSInteger type) {
            if (type == 2) {
                if (index.row == 0) {
                    WGStoreOpenModel *open = weakSelf.temTimeModel.shipTime.lastObject;
                    if (open.beginTime.length == 0) {
                        KPOP(@"请选择开始时间")
                        return ;
                    }
                    if (open.endTime.length == 0) {
                        KPOP(@"请选择结束时间")
                        return ;
                    }
                    [weakSelf.temTimeModel.shipTime addObject:[WGStoreOpenModel new]];
                }else{
                    [weakSelf.temTimeModel.shipTime removeObjectAtIndex:index.row];
                }
                [weakSelf.collectionView reloadData];
                
            }else{
                weakSelf.temTimeModel.shipTime[index.row] = model;
                [weakSelf.collectionView reloadData];
            }
        };
        return cell;
        
    }else if (indexPath.section == 2) {
        
        WGButtomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WGButtomCollectionViewCell" forIndexPath:indexPath];
        cell.enabled = [self verifyData:NO];
        return cell;
        
    }else
        return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    if (indexPath.section == 0) {
        if ([_temTimeModel.shipWeek containsObject:_titleArr[indexPath.row]]) {
            [_temTimeModel.shipWeek removeObject:_titleArr[indexPath.row]];
        }else{
            [_temTimeModel.shipWeek addObject:_titleArr[indexPath.row]];
        }
        [self.collectionView reloadData];
    }else if (indexPath.section == 2){
        if ([self verifyData:YES]) {
            if (self.selectStoreTime) {
                NSMutableSet *set1 = [NSMutableSet setWithArray:_temTimeModel.shipWeek];
                NSMutableSet *set2 = [NSMutableSet setWithArray:_titleArr];
                [set1 intersectSet:set2];
                NSComparator finderSortBlock = ^(id string1,id string2) {
                    NSInteger indx1 = [weakSelf.titleArr indexOfObject:string1];
                    NSInteger indx2 = [weakSelf.titleArr indexOfObject:string2];
                    if (indx1 > indx2) {
                        return NSOrderedDescending;
                    }else if (indx1 < indx2){
                        return NSOrderedAscending;
                    }else{
                        return NSOrderedSame;
                    }
                };
                
                NSArray *finderSortArray = [[set1 allObjects] sortedArrayUsingComparator:finderSortBlock];
                NSLog(@"finderSortArray: %@", finderSortArray);
                _temTimeModel.shipWeek = [[NSMutableArray alloc]initWithArray:finderSortArray];
                self.selectStoreTime(_temTimeModel);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            
        }
    }
    
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor colorWithHexString:@"#FFFFFF"];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[WGStoreSelctTimeWeekCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreSelctTimeWeekCollectionViewCell"];
        [_collectionView registerClass:[WGStoreSelctTimeCollectionViewCell class] forCellWithReuseIdentifier:@"WGStoreSelctTimeCollectionViewCell"];
        [_collectionView registerClass:[WGButtomCollectionViewCell class] forCellWithReuseIdentifier:@"WGButtomCollectionViewCell"];
        [_collectionView registerClass:[WGTitleCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"WGTitleCollectionReusableView"];
    }
    return _collectionView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
