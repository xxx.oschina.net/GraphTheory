//
//  YXUserInfoManager.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/12/3.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "YXUserInfoManager.h"
#define KEY @"USERINFO"


static YXUserInfoModel *userInfoModel = nil;
@implementation YXUserInfoManager

+ (YXUserInfoManager *)shareUserInfoManager {
    static YXUserInfoManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[YXUserInfoManager alloc] init];
    });
    return manager;
}

/** 保存用户信息 */
+ (void)saveUserInfoWithModel:(YXUserInfoModel *)entity {
    //NSUserDefaults 继承于NSObject, 单例模式设计, 存储信息采用键值对的形式
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:entity];
    [userDefault setObject:data forKey:KEY];
    [userDefault synchronize];
    userInfoModel = entity;
    
}
/** 清空用户信息 */
+ (void)cleanUserInfo {
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault removeObjectForKey:KEY];
    [userDefault synchronize];
    userInfoModel = nil;
    
}
/** 获取用户信息 */
+ (YXUserInfoModel *)getUserInfo {
    
    if (!userInfoModel) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSData *data = [userDefault objectForKey:KEY];
        if (data) {
            userInfoModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
        } else {
            userInfoModel = nil;
        }
    }
    return userInfoModel;
    
    
}
/** 判断用户登录状态 */
+ (BOOL)isLoad {
    
    if ([YXUserInfoManager getUserInfo] == nil) {
        return NO;
    } else {
        return YES;
    }
    
}

/**  修改用户信息 */
+ (void)resetUserInfoMessageWithDic:(NSDictionary *)dic {
    
    YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
    NSString *key = [dic allKeys].firstObject;
    
    if ([key isEqualToString:@"wechat_headimgurl"]) {
        model.wechat_headimgurl = dic[@"wechat_headimgurl"];
    }
    
    if ([key isEqualToString:@"nike"]) {
        model.nike = dic[@"nike"];
    }
    if ([key isEqualToString:@"birthday"]) {
        model.birthday = dic[@"birthday"];
    }
    if ([key isEqualToString:@"college"]) {
        model.college = dic[@"college"];
    }
    if ([key isEqualToString:@"signature"]) {
        model.signature = dic[@"signature"];
    }
    
    if ([key isEqualToString:@"sex"]) {
        model.sex = [dic[@"sex"] integerValue];
    }
    
    if ([key isEqualToString:@"shopId"]) {
        model.shopId = dic[@"shopId"];
    }
    
    if ([key isEqualToString:@"personalState"]) {
        model.personalState = [dic[@"personalState"] integerValue];
    }
    
    
    if ([key isEqualToString:@"areaId"]) {
        model.areaId = dic[@"areaId"];
    }
    
    if ([key isEqualToString:@"cityId"]) {
        model.cityId = dic[@"cityId"];
    }
    
    if ([key isEqualToString:@"longitude"]) {
        model.longitude = dic[@"longitude"];
    }
    
    if ([key isEqualToString:@"lat"]) {
        model.lat = dic[@"lat"];
    }
    
    if ([key isEqualToString:@"lng"]) {
        model.lng = dic[@"lng"];
    }
    
    [YXUserInfoManager saveUserInfoWithModel:model];
    
}


@end
