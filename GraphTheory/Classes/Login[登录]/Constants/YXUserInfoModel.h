//
//  YXUserInfoModel.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/12/3.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "DDBaseModel.h"
#import "WGStoreModel.h"

@interface YXUserInfoModel : DDBaseModel <NSCoding>

/**
 * token
 */
@property (nonatomic ,copy) NSString *token;
/**
 * 用户Id
 */
@property (nonatomic ,copy) NSString *user_id;
/**
 * 商家Id
 */
@property (nonatomic ,copy) NSString *merchantId;
/**
 * 商家地址
 */
@property (nonatomic ,copy) NSString *address;

/**
 * 管理员手机号
 */
@property (nonatomic ,copy) NSString *adminMobile;
/**
 * 角色类型
 */
@property (nonatomic ,assign) NSInteger role;
/**
 * 角色
 */
@property (nonatomic ,copy) NSString *roleName;
/**
 * 余额
 */
@property (nonatomic ,copy) NSString *balance;
/**
 * 手机号码
 */
@property (nonatomic ,copy) NSString *mobile;
/**
 * 用户头像
 */
@property (nonatomic ,copy) NSString *wechat_headimgurl;
/**
 * 用户生日
 */
@property (nonatomic ,copy) NSString *birthday;
/**
 * 用户名
 */
@property (nonatomic ,copy) NSString *wechat_name;
/**
 * 用户真实姓名
 */
@property (nonatomic ,copy) NSString *name;
/**
 * 用户昵称
 */
@property (nonatomic ,copy) NSString *nike;
/**
 * 店铺认证审核状态 0未提交 1已提交未审核 2审核通过 3审核不通过 9审核通过,未开店
 */
@property (nonatomic ,assign) NSInteger personalState;

/**
 * 性别
 */
@property (nonatomic ,assign) NSInteger sex;
/**
 * 身份证正面附件
 */
@property (nonatomic ,copy) NSString *idCardFront;

/**
 * 身份证反面附件
 */
@property (nonatomic ,copy) NSString *idCardReverse;
/**
 * 手持身份证照片附件
 */
@property (nonatomic ,copy) NSString *idCardHand;

/**
 * 设备token
 */
@property (nonatomic ,copy) NSString *deviceToken;

/**
 * 友盟token
 */
@property (nonatomic ,copy) NSString *umToken;

/**
 商家Id
 */
@property (nonatomic,copy) NSString *unitId;

/**
 商家名称
 */
@property (nonatomic,copy) NSString *unitName;

/**
 区域编码
 */
@property (nonatomic,copy) NSString *areaId;
@property (nonatomic,copy) NSString *cityId;

/**
 经纬度
 */
@property (nonatomic,copy) NSString *longitude;

/**
  学校
*/
@property (nonatomic ,copy) NSString *college;
/**
 个性签名
*/
@property (nonatomic ,copy) NSString *signature;
/**
 服务电话
 */
@property (nonatomic ,copy) NSString *serviceTel;
/**
 服务时间
 */
@property (nonatomic ,copy) NSString *serviceTime;
/**
 支付密码
 */
@property (nonatomic ,copy) NSString *pay_pwd;
/**
 创业佣金
 */
@property (nonatomic ,copy) NSString *earn_money;
/**
 走货收入
 */
@property (nonatomic ,copy) NSString *goods_income;

/**
 店铺id
*/
@property (nonatomic ,copy) NSString *shopId;
/**
 店铺经纬度
*/
@property (nonatomic ,copy) NSString *shoplongitude;

@property (nonatomic ,copy) NSString *show_id;
@property (nonatomic ,copy) NSString *show_video_id;
@property (nonatomic ,copy) NSString *role_pass;

@property (nonatomic ,copy) NSString *lat;
@property (nonatomic ,copy) NSString *lng;


@property (nonatomic ,strong) NSDictionary *applyInfo;

@end

