//
//  YXUserInfoManager.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/12/3.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YXUserInfoModel.h"

#define DDUSERNAME @"DDUSERNAME"
#define DDUSERCHANGE @"DDUSERCHAGE" ///<登录用户改变

@interface YXUserInfoManager : NSObject

@property (nonatomic,strong) WGStoreModel *storModel;//商店信息
@property (nonatomic,strong) NSArray *allDelivery;//所有快递类型
@property (nonatomic,strong) id allArea;//所有地区信息


+ (YXUserInfoManager *) shareUserInfoManager;

/**  修改用户信息 */
+ (void)resetUserInfoMessageWithDic:(NSDictionary *)dic;
/** 保存用户信息 */
+ (void)saveUserInfoWithModel:(YXUserInfoModel *)entity;
/** 清空用户信息 */
+ (void)cleanUserInfo;
/** 获取用户信息 */
+ (YXUserInfoModel *)getUserInfo;
/** 判断用户登录状态 */
+ (BOOL)isLoad;

@end


