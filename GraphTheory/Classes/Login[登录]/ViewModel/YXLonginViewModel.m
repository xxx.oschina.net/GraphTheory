//
//  YXLonginViewModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXLonginViewModel.h"

@implementation YXLonginViewModel





#pragma mark - app端注册/忘记密码/修改密码获取验证码的接口
+ (void)requestSendMobileCodeWithType:(NSString *)type
                               Mobile:(NSString *)mobile
                                 Code:(NSString *)code
                            RandomStr:(NSString *)randomStr
                           Completion:(void(^)(id responesObj))completion
                              Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    if ([type isEqualToString:@"other"]) {
        urlStr = [NSString stringWithFormat:@"%@user/sendPhoneCode",kBaseURL];
    }else {
//        urlStr = [NSString stringWithFormat:@"%@auth/sendMobileCodeUpdate/v2/mobile",kBaseURL];
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:mobile forKey:@"phone"];
    [parameters setValue:[YXDataTimeTool currentTimeStr] forKey:@"requestStartTime"];
    [parameters setValue:@"other" forKey:@"type"];
    [parameters setValue:@"fake" forKey:@"true"];

    [NetWorkTools postWithUrl:urlStr parameters:parameters success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 注册接口
+ (void)requestRegisterMerchantWithMobile:(NSString *)mobile
                                     Code:(NSString *)code
                                RandomStr:(NSString *)randomStr
                                 Password:(NSString *)password
                               InviteCode:(NSString *)inviteCode
                               Completion:(void(^)(id responesObj))completion
                                  Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    if ([randomStr isEqualToString:@"0"]) {
        urlStr = [NSString stringWithFormat:@"%@user/register",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@user/findPwd",kBaseURL];
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:mobile forKey:@"phone"];
    [parameters setValue:code forKey:@"code"];
    [parameters setValue:password forKey:@"pwd"];
    [parameters setValue:inviteCode forKey:@"rpwd"];

    [NetWorkTools postWithUrl:urlStr parameters:parameters success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 商家注册上传实名认证图片
+ (void)requestRegisterUploadFileWithImages:(NSArray *)imageArray
                                 Completion:(void(^)(id responesObj))completion
                                    Failure:(void(^)(NSError *error))conError {
    
    //向服务器提交图片
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/javascript",@"text/html", nil];
    manager.requestSerializer.timeoutInterval = 30.f;
    //post请求头 的设置
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];// 将token存到请求头中
    
    NSString *url = [NSString stringWithFormat:@"%@auth/merchant/manage/uploadFile",kBaseURL];
    //显示进度
    [manager POST:url parameters:nil headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //上传多张照片
        for (NSInteger i = 0; i < imageArray.count; i++) {

            NSData *imageData = [imageArray[i] compressWithMaxLength:1024 * 1024 * 0.5];
            //根据当前时间设置上传的名字
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //设置日期格式
            formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            //上传的参数名
            NSString *name = [NSString stringWithFormat:@"%@%d", [NSDate date], arc4random()/1000];
            //上传文件名字fileName
            NSString *fileName = [NSString stringWithFormat:@"%@.jpeg", name];
            
            NSString *file;
            if (i == 0) {
                file = [NSString stringWithFormat:@"file"];
            }else {
                file = [NSString stringWithFormat:@"file%ld",(long)i];
            }
            [formData appendPartWithFileData:imageData name:file fileName:fileName mimeType:@"image/jpeg"];
        }
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (completion) {
            completion(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"错误 %@", error.localizedDescription);
        if (conError) {
            conError(error);
        }
        
    }];
}





#pragma mark - 微信登录
+ (void)requestWXLoginWithAccess_token:(NSString *)access_token
                                openid:(NSString *)openid
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError; {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@user/wxauthLogin",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:access_token forKey:@"access_token"];
    [params setValue:openid forKey:@"openid"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

+ (void)requestAccountLoginWithPhone:(NSString *)phone
                            Password:(NSString *)password
                                Type:(NSString *)type
                          Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:phone forKey:@"phone"];
    if ([type isEqualToString:@"1"]) {
        urlStr = [NSString stringWithFormat:@"%@user/phoneCodeLogin",kBaseURL];
        [params setValue:password forKey:@"code"];
    }else {
        urlStr = [NSString stringWithFormat:@"%@user/accountLogin",kBaseURL];
        [params setValue:password forKey:@"pwd"];
    }
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  商家忘记密码
+ (void)requestUpdateMerchantPasswordWithMobile:(NSString *)mobile
                                           Code:(NSString *)code
                                      RandomStr:(NSString *)randomStr
                                         NewPwd:(NSString *)newPwd
                                     Completion:(void(^)(id responesObj))completion
                                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/merchant/manage/updateMerchantPassword",kBaseURL];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:mobile forKey:@"mobile"];
    [params setValue:newPwd forKey:@"newPwd"];
    [params setValue:randomStr forKey:@"randomStr"];
    [params setValue:code forKey:@"code"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}




@end
