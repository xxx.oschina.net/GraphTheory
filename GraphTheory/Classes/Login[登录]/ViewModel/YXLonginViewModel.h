//
//  YXLonginViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXLonginViewModel : DDBaseViewModel






/**
 app端注册/忘记密码/修改密码商家获取验证码的接口

 @param type        0: 是注册  1:是忘记密码/修改密码
 @param mobile      手机号
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestSendMobileCodeWithType:(NSString *)type
                               Mobile:(NSString *)mobile
                                 Code:(NSString *)code
                            RandomStr:(NSString *)randomStr
                           Completion:(void(^)(id responesObj))completion
                              Failure:(void(^)(NSError *error))conError;




/**
 注册接口

 @param mobile      手机号
 @param code        短信验证码
 @param randomStr   0:注册 1:找回密码
 @param password    密码
 @param inviteCode  邀请码
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestRegisterMerchantWithMobile:(NSString *)mobile
                                     Code:(NSString *)code
                                RandomStr:(NSString *)randomStr
                                 Password:(NSString *)password
                               InviteCode:(NSString *)inviteCode
                               Completion:(void(^)(id responesObj))completion
                                  Failure:(void(^)(NSError *error))conError;




/**
 商家注册上传实名认证图片

 @param imageArray  上传图片数组
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestRegisterUploadFileWithImages:(NSArray *)imageArray
                                 Completion:(void(^)(id responesObj))completion
                                    Failure:(void(^)(NSError *error))conError;





/**
 微信登录
 
 @param access_token       token
 @param openid              openid
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestWXLoginWithAccess_token:(NSString *)access_token
                                openid:(NSString *)openid
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;
/**
 登录
 
 @param phone          账号
 @param password        密码
 @param type           0:密码登录 1:验证码登录
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestAccountLoginWithPhone:(NSString *)phone
                            Password:(NSString *)password
                                Type:(NSString *)type
                          Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;



/**
 商家忘记密码

 @param mobile          手机号/账号
 @param code            验证码
 @param newPwd          新密码
 @param randomStr       发送手机验证码时,返回的随机字符串
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateMerchantPasswordWithMobile:(NSString *)mobile
                                           Code:(NSString *)code
                                      RandomStr:(NSString *)randomStr
                                         NewPwd:(NSString *)newPwd
                                     Completion:(void(^)(id responesObj))completion
                                        failure:(void(^)(NSError *error))conError;





@end

NS_ASSUME_NONNULL_END
