//
//  WGRegisteViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGRegisteViewController.h"
#import "DDBaseTabBarController.h"
#import "NSString+Addition.h"
#import "BYTimer.h"
#import "YXLonginViewModel.h"
#import "WGWebViewController.h"
#import "YXWebViewController.h"
#import "ZKVerifyAlertView.h"
//#import "YXRegistePopView.h"
#import "YXCustomAlertActionView.h"

@interface WGRegisteViewController ()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *registeBtn;
@property (nonatomic, strong) UIButton *agreeBtn;
@property (nonatomic, strong) UIButton *LoginBtn;
@property (nonatomic, strong) UIButton *xieyiBtn;
@property (nonatomic, strong) UIButton *getCodeBtn;
@property (nonatomic, strong) UIView *textFieldView;

@property (nonatomic, strong) NSMutableArray <LTLimitTextField *>*textFieldArr;

@property (nonatomic, strong) NSString *randomStr;
@property (nonatomic, strong) NSString *code;
@end

@implementation WGRegisteViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if (self.type == 0) {
        self.title = @"注册";
    }
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.LoginBtn];
    [self setUpLayout];
    
//    YXCustomAlertActionView *alert = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertTextWeb) Title:@"隐私政策" Message:@"" sureBtn:@"同意" cancleBtn:@"不同意"];
//    [alert showAnimation];
//    DDWeakSelf
//    [alert setCancelClick:^{
//        [weakSelf.navigationController popViewControllerAnimated:YES];
//    }];
    
}
- (void)turnLogin{
    [self.navigationController popViewControllerAnimated:YES];
}

// 发送短信验证码
- (void)sendMobileCode {
    NSLog(@"获取验证码");
    DDWeakSelf
    if (_textFieldArr[0].text.length == 0) {
        KPOP(@"请输入手机号");
        return;
    }
    
    if (![_textFieldArr[0].text isTelephoneFullNumber]) {
        KPOP(@"手机号错误，请重新输入");
        return;
    }
    
    [YJProgressHUD showLoading:@"发送中..."];
    [YXLonginViewModel requestSendMobileCodeWithType:@"other" Mobile:self.textFieldArr[0].text Code:self.code RandomStr:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:@"发送验证码成功"];
            [weakSelf openCountdown];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
        
    }];
}


- (void)openCountdown {
    YXWeakSelf
    [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
        [weakSelf.getCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%d)",presentTime] forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = NO;
    } finish:^{
        [weakSelf.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = YES;
    }];
}

- (void)showOrHidePassWord:(UIButton *)btn{
    btn.selected = !btn.selected;
    UITextField *tem = _textFieldArr[2];
    tem.secureTextEntry = !btn.selected;
}
- (void)agreeAction:(UIButton *)btn{
    btn.selected = !btn.selected;
    [self verifyData:NO];

}
- (void)xieyi{
    NSLog(@"协议");
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAboutOtherInfoWithType:@"3" Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = responesObj[@"data"][@"us_title"];
        vc.url = responesObj[@"data"][@"us_content"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
//    WGWebViewController *xieyi = [WGWebViewController new];
//    [self.navigationController pushViewController:xieyi animated:YES];
}
// 点击注册
- (void)registe{
    if ([self verifyData:YES]) {
        
        YXWeakSelf
        [YJProgressHUD showLoading:@""];
        [YXLonginViewModel requestRegisterMerchantWithMobile:_textFieldArr[0].text
                                                        Code:_textFieldArr[1].text
                                                   RandomStr:@(self.type).stringValue
                                                    Password:_textFieldArr[2].text
                                                  InviteCode:_textFieldArr[3].text
                                                  Completion:^(id  _Nonnull responesObj) {
                                                        [YJProgressHUD hideHUD];
                                                      if (REQUESTDATASUCCESS) {
                                                          [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                                          [YJProgressHUD showMessage:responesObj[@"msg"]];

                                                      }else {
                                                          [YJProgressHUD showMessage:responesObj[@"msg"]];
                                                      }
                                                  } Failure:^(NSError * _Nonnull error) {
                                                      [YJProgressHUD showError:REQUESTERR];

                                                  }];
        
    }
}
- (void)textFieldChanged:(UITextField*)textField{
    [self verifyData:NO];
}
- (BOOL)verifyData:(BOOL)showMsg{
    NSString *msg = @"";
    BOOL correct = YES;
    if (_textFieldArr[3].text.length < 6 && _textFieldArr[3].text.length > 32) {
        msg = @"密码格式有误，请重新设置";
        correct = NO;
    }else if (_textFieldArr[2].text.length < 6||_textFieldArr[2].text.length > 32){
        msg = @"密码格式有误，请重新设置";
        correct = NO;
    }else if (_textFieldArr[1].text.length != 4){
        msg = @"验证码错误，请重新输入";
        correct = NO;
    }
  else  if (_textFieldArr[0].text.length == 0) {
      msg = @"请输入手机号";
      correct = NO;
    }

    else if (![_textFieldArr[0].text isTelephoneFullNumber]) {
        msg = @"手机号错误，请重新输入";
        correct = NO;
    }
    if (self.type == 0) {
        if (_agreeBtn.selected == NO){
            msg = @"请同意《图论社注册协议》";
            correct = NO;
        }
    }
   
    if (showMsg) {
        KPOP(msg);
    }
    _registeBtn.enabled = correct;
    _registeBtn.alpha = correct?1:0.5;
    return correct;
}
- (void)setUpLayout{
    [self.view addSubview:self.titleLab];
    [self configEnter];
    [self.view addSubview:self.textFieldView];
    [self.view addSubview:self.registeBtn];
   

    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15);
        [self.titleLab sizeToFit];
    }];
    
    [_textFieldView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.equalTo(self.view);
        make.top.equalTo(self.titleLab.mas_bottom).offset(50);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.mas_equalTo(@200);
    }];
    [_textFieldArr mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:50 leadSpacing:0 tailSpacing:0];
    [_textFieldArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.equalTo(self.view);
        make.right.equalTo(self.view.mas_right).offset(-15);
    }];
   
    [_registeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textFieldView.mas_bottom).offset(15);
        make.left.equalTo(self.view.mas_left).offset(15);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.equalTo(@44);
    }];

    if (self.type == 0) {
        [self.view addSubview:self.xieyiBtn];
        [self.view addSubview:self.agreeBtn];
        [_xieyiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view.mas_bottom).offset(-30);
            make.centerX.equalTo(self.view.mas_centerX);
        }];
        
        [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.xieyiBtn.mas_centerY);
            make.right.equalTo(self.xieyiBtn.mas_left).offset(0);
        }];
    }
 
    
}

/**
 配置输入框样式
 */
- (void)configEnter{
    NSArray *temArr = @[@"请输入手机号",@"验证码",@"请输入6-32位新密码",@"请再次输入密码"];
    NSArray *imgArr = @[@"phone_bbbcca",@"yanzhengma_bbbcca",@"password_bbbcca",@"password_bbbcca"];
    for (int i = 0; i < temArr.count; i ++ ) {
        LTLimitTextField * temTextField = [LTLimitTextField new];
        temTextField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
        temTextField.textLimitType = LTTextLimitTypeLength;
        temTextField.textLimitSize = 32;
        temTextField.textColor = color_TextOne;
        temTextField.font = [UIFont systemFontOfSize:16];
        temTextField.backgroundColor = [UIColor whiteColor];
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
        UIImageView *leftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imgArr[i]]];
        leftImage.contentMode = UIViewContentModeScaleAspectFit;
        [leftView addSubview:leftImage];
        [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(13, 13, 13, 13));
        }];
        temTextField.leftView = leftView;
        temTextField.leftViewMode = UITextFieldViewModeAlways;
        if (i == 0) {
            temTextField.textLimitInputType = LTTextLimitInputTypeTelephoneNumber;
            temTextField.keyboardType = UIKeyboardTypePhonePad;
            temTextField.placeholder = @"请输入手机号";
        }else if (i == 1){
            temTextField.textLimitInputType = LTTextLimitInputTypeNumber;
            temTextField.textLimitType = LTTextLimitTypeLength;
            temTextField.textLimitSize = 4;
            temTextField.keyboardType = UIKeyboardTypePhonePad;
            temTextField.rightView = self.getCodeBtn;
            temTextField.rightViewMode = UITextFieldViewModeAlways;
            temTextField.placeholder = @"验证码";

        }else if (i == 2){
            temTextField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
            temTextField.textLimitType = LTTextLimitTypeLength;
            temTextField.textLimitSize = 32;
            temTextField.secureTextEntry = YES;
//            UIButton *rightView = [UIButton buttonWithType:UIButtonTypeCustom];
//            rightView.frame = CGRectMake(0, 0, 44, 44);
//            [rightView setImage:[UIImage imageNamed:@"login_password_hide"] forState:UIControlStateNormal];
//            [rightView setImage:[UIImage imageNamed:@"login_password_show"] forState:UIControlStateSelected];
//            [rightView addTarget:self action:@selector(showOrHidePassWord:) forControlEvents:UIControlEventTouchUpInside];
//            temTextField.rightView = rightView;
            temTextField.rightViewMode = UITextFieldViewModeAlways;
            temTextField.placeholder = @"请输入6-32位新密码";

        }else if (i == 3){
            temTextField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
            temTextField.textLimitType = LTTextLimitTypeLength;
            temTextField.textLimitSize = 32;
            temTextField.secureTextEntry = YES;
            temTextField.placeholder = @"请再次输入密码";
//            UIView *rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 50)];
//            temTextField.rightView = rightView;
            temTextField.rightViewMode = UITextFieldViewModeAlways;
            
        }
        if (i != 4) {
            UIView *lineView = [UIView new];
            lineView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
            [temTextField addSubview:lineView];
            [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.and.bottom.equalTo(temTextField);
                make.left.equalTo(temTextField.mas_left).offset(15);
                make.height.mas_equalTo(@1);
            }];
        }
        [temTextField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];

        [self.textFieldView addSubview:temTextField];
        [self.textFieldArr addObject:temTextField];
    }
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor blackColor];
        if (_type == 0) {
            _titleLab.text = @"欢迎注册图论社";
        }else {
            _titleLab.text = @"找回密码";
        }
        _titleLab.font = [UIFont systemFontOfSize:20.0f];
    }
    return _titleLab;
}

- (NSMutableArray *)textFieldArr{
    if (!_textFieldArr) {
        _textFieldArr = [NSMutableArray new];
    }
    return _textFieldArr;
}
- (UIView *)textFieldView{
    if (!_textFieldView) {
        _textFieldView = [UIView new];
    }
    return _textFieldView;
}
- (UIButton *)agreeBtn{
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_agree_onselect"] forState:UIControlStateNormal];
        [_agreeBtn setImage:[UIImage imageNamed:@"login_agree_select"] forState:UIControlStateSelected];
        [_agreeBtn addTarget:self action:@selector(agreeAction:) forControlEvents:UIControlEventTouchUpInside];
        _agreeBtn.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);

    }
    return _agreeBtn;
}
- (UIButton *)LoginBtn{
    if (!_LoginBtn) {
        _LoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _LoginBtn.frame = CGRectMake(0, 0, 40, 30);
        [_LoginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [_LoginBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _LoginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
//        _LoginBtn.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        [_LoginBtn addTarget:self action:@selector(turnLogin) forControlEvents:UIControlEventTouchUpInside];
    }
    return _LoginBtn;
}
- (UIButton *)getCodeBtn{
    if (!_getCodeBtn) {
        _getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeBtn.frame = CGRectMake(0, 0, 100, 32);
        [_getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _getCodeBtn.backgroundColor = APPTintColor;
        _getCodeBtn.layer.masksToBounds = YES;
        _getCodeBtn.layer.cornerRadius = 4.0f;
        [_getCodeBtn addTarget:self action:@selector(sendMobileCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getCodeBtn;
}
- (UIButton *)xieyiBtn{
    if (!_xieyiBtn) {
        _xieyiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_xieyiBtn setTitle:@"同意《平台用户协议》和《隐私协议》" forState:UIControlStateNormal];
        [_xieyiBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _xieyiBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_xieyiBtn addTarget:self action:@selector(xieyi) forControlEvents:UIControlEventTouchUpInside];
    }
    return _xieyiBtn;
}
- (UIButton *)registeBtn{
    if (!_registeBtn) {
        _registeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _registeBtn.backgroundColor = APPTintColor;
        if (_type == 0) {
            [_registeBtn setTitle:@"提交注册" forState:UIControlStateNormal];
        }else{
            [_registeBtn setTitle:@"完成设置" forState:UIControlStateNormal];
        }
        [_registeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _registeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_registeBtn addTarget:self action:@selector(registe) forControlEvents:UIControlEventTouchUpInside];
        _registeBtn.layer.cornerRadius = 4;
        _registeBtn.enabled = NO;
        _registeBtn.alpha = 0.5;
    }
    return _registeBtn;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
