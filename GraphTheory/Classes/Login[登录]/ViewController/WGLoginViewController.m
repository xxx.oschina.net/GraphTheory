//
//  WGLoginViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGLoginViewController.h"
#import "WGRegisteViewController.h"
#import "WGForgetPassWordViewController.h"
#import "DHGuidePageHUD.h"
#import "NSString+Addition.h"
#import "DDBaseNavigationController.h"
#import "DDBaseTabBarController.h"
#import "YXLonginViewModel.h"
#import "BYTimer.h"
#import <UMShare/UMShare.h>
#import "DDPayMentTools.h"
#import "YXUserViewModel.h"
@interface WGLoginViewController ()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) LTLimitTextField *phoneField;
@property (nonatomic, strong) LTLimitTextField *passWordField;
@property (nonatomic, strong) UIButton *LoginBtn;
@property (nonatomic, strong) UIButton *registeBtn;
@property (nonatomic, strong) UIButton *switchLoginBtn;
@property (nonatomic, strong) UIButton *forgetBtn;
@property (nonatomic, strong) UILabel *loginTypeLab;
@property (nonatomic, strong) UIButton *WXBtn;
@property (nonatomic, strong) UIButton *getCodeBtn;

@end

@implementation WGLoginViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"登录";
    self.navigationController.navigationBar.barTintColor=[UIColor whiteColor];
    
    
    UIButton *rightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    rightBtn.frame = CGRectMake(0, 0, 60, 30);
    [rightBtn setTitle:@"游客" forState:(UIControlStateNormal)];
    [rightBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [rightBtn addTarget:self action:@selector(rightAction) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
//   _phoneField.text = @"18052283616"
//    _passWordField.text = @"900518qq";
//    _phoneField.text = @"15190678936"
//    _passWordField.text = @"123456";
    
    [self setUpLayout];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *mobie = [userDefault objectForKey:DDUSERNAME];
    _phoneField.text = mobie;
        
}

- (void)rightAction {
    
    DDBaseTabBarController *tabBar = [[DDBaseTabBarController alloc] init];
    [UIApplication sharedApplication].keyWindow.rootViewController = tabBar;
}

- (void)showOrHidePassWord:(UIButton *)btn{
    btn.selected = !btn.selected;
    _passWordField.secureTextEntry = !btn.selected;
}
- (void)turnRegiste{
    WGRegisteViewController *vc = [WGRegisteViewController new];
    vc.type = 0;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)switchBtn {
    if (_type == 0) {
        WGLoginViewController *vc = [WGLoginViewController new];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (void)turnForgetPassWord{
    WGRegisteViewController *vc = [WGRegisteViewController new];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
    
}

// 微信第三方登录
- (void)WXBtnAction {
    
    if ([WXApi isWXAppInstalled]) {
        DDWeakSelf
        [YJProgressHUD showLoading:@""];
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
             [YJProgressHUD hideHUD];
            if (error) {
            } else {
                UMSocialUserInfoResponse *resp = result;
                [YJProgressHUD showStatus:(YJProgressHUDStatusLoading) text:@"登录中..."];
                [YXLonginViewModel requestWXLoginWithAccess_token:resp.accessToken openid:resp.openid Completion:^(id  _Nonnull responesObj) {
                    [YJProgressHUD hideHUD];
                    if (REQUESTDATASUCCESS) {
                        [YXUserInfoManager cleanUserInfo];
                        //存入
                        YXUserInfoModel *model = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
                        //保存用户信息
                        [YXUserInfoManager saveUserInfoWithModel:model];
                        [weakSelf queryUserInfo:model];
                    }else {
                        [YJProgressHUD showMessage:responesObj[@"msg"]];
                    }
                } failure:^(NSError * _Nonnull error) {
                    [YJProgressHUD showError:REQUESTERR];
                }];
            }
        }];
    }else {
        [YJProgressHUD showMessage:@"请安装微信~"];
    }

}



- (void)textFieldChanged:(UITextField*)textField{
    [self verifyData:NO];
}
- (void)login{
    if ([self verifyData:YES]) {
        
        YXWeakSelf
        [YJProgressHUD showStatus:(YJProgressHUDStatusLoading) text:@"登录中..."];
        [YXLonginViewModel requestAccountLoginWithPhone:_phoneField.text
                                               Password:_passWordField.text
                                                   Type:@(_type).stringValue
                                             Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [YXUserInfoManager cleanUserInfo];
                //存入
                YXUserInfoModel *model = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
                //保存用户信息
                [YXUserInfoManager saveUserInfoWithModel:model];
                [weakSelf queryUserInfo:model];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError * _Nonnull error) {
            [YJProgressHUD showError:REQUESTERR];
        }];
    }
}

- (void)queryUserInfo:(YXUserInfoModel*)model{
  
    DDWeakSelf
    [YXUserViewModel queryUserInfoCompletion:^(id responesObj) {
        YXUserInfoModel *userInfo = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
        userInfo.token = model.token;
        userInfo.mobile = model.mobile;
        [YXUserInfoManager saveUserInfoWithModel:userInfo];
        [weakSelf loginSuccss];
    } failure:^(NSError *error) {
    }];
    
}


// 发送短信验证码
- (void)sendMobileCode {
    NSLog(@"获取验证码");
    DDWeakSelf
    if (_phoneField.text.length == 0) {
        KPOP(@"请输入手机号");
        return;
    }
    
    if (![_phoneField.text isTelephoneFullNumber]) {
        KPOP(@"手机号错误，请重新输入");
        return;
    }
    
    [YJProgressHUD showLoading:@"发送中..."];
    [YXLonginViewModel requestSendMobileCodeWithType:@"other" Mobile:_phoneField.text Code:_passWordField.text RandomStr:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:@"发送验证码成功"];
            [weakSelf openCountdown];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
        
    }];
}

- (void)openCountdown {
    YXWeakSelf
    [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
        [weakSelf.getCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%d)",presentTime] forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = NO;
    } finish:^{
        [weakSelf.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = YES;
    }];
}


- (void)loginSuccss{
    
    NSString *currentMobile = _phoneField.text;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:currentMobile forKey:DDUSERNAME];
    [userDefault synchronize];
    
    if (_isPresent) {
        for (UINavigationController *nav in self.cyl_tabBarController.viewControllers) {
            [nav popToRootViewControllerAnimated:YES];
        }
        self.cyl_tabBarController.selectedIndex = 0;
        [[NSNotificationCenter defaultCenter] postNotificationName:DDUSERCHANGE object:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }else{
        DDBaseTabBarController *tabBar = [[DDBaseTabBarController alloc] init];
        [UIApplication sharedApplication].keyWindow.rootViewController = tabBar;
        
    }
}
- (BOOL)verifyData:(BOOL)showMsg{
    NSString *msg = @"";
    BOOL correct = YES;
    if (_type == 0) {
        if (_passWordField.text.length < 6||_passWordField.text.length > 32){
            msg = @"请设置密码";
            correct = NO;
        }else if (![_phoneField.text isTelephoneFullNumber]) {
            msg = @"请输入手机号";
            correct = NO;
        }
    }else {
        if (![_phoneField.text isTelephoneFullNumber]) {
           msg = @"请输入手机号";
           correct = NO;
        }else if (_passWordField.text.length != 4) {
            msg = @"验证码错误，请重新输入";
            correct = NO;
        }
    }
  
    if (showMsg) {
        KPOP(msg);
    }
    _LoginBtn.enabled = correct;
    _LoginBtn.alpha = correct?1:0.5;
    return correct;
}
- (void)setUpLayout{
    
    [self.view addSubview:self.titleLab];
    [self.view addSubview:self.passWordField];
    [self.view addSubview:self.phoneField];
    [self.view addSubview:self.registeBtn];
    [self.view addSubview:self.switchLoginBtn];
    [self.view addSubview:self.forgetBtn];
    [self.view addSubview:self.LoginBtn];
    [self.view addSubview:self.loginTypeLab];
    [self.view addSubview:self.WXBtn];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15);
        [self.titleLab sizeToFit];
    }];
    
    [_phoneField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.equalTo(self.view);
        make.top.equalTo(self.titleLab.mas_top).offset(50);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.mas_equalTo(@50);
    }];
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    [_phoneField addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.phoneField);
        make.left.equalTo(self.phoneField.mas_left).offset(15);
        make.height.mas_equalTo(@1);
    }];
    
    [_passWordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.equalTo(self.view);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.top.equalTo(self.phoneField.mas_bottom).offset(0);
        make.height.mas_equalTo(@50);
    }];
    
    UIView *lineView1 = [UIView new];
    lineView1.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    [_phoneField addSubview:lineView1];
    [lineView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.equalTo(self.passWordField);
        make.left.equalTo(self.passWordField.mas_left).offset(15);
        make.height.mas_equalTo(@1);
    }];
    

    [_LoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passWordField.mas_bottom).offset(15);
        make.left.equalTo(self.view.mas_left).offset(15);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.equalTo(@44);
    }];
    
    [_registeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.LoginBtn.mas_bottom);
        make.left.equalTo(self.view.mas_left);
    }];
    
    [_switchLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.LoginBtn.mas_bottom);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    [_forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.LoginBtn.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
    
    [_loginTypeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_bottom).offset(-100);
    }];
    [_WXBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.loginTypeLab.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor blackColor];
        _titleLab.text = @"请输入账号和密码";
        _titleLab.font = [UIFont systemFontOfSize:20.0f];
    }
    return _titleLab;
}

- (LTLimitTextField *)phoneField{
    if (!_phoneField) {
        _phoneField = [LTLimitTextField new];
        _phoneField.textLimitInputType = LTTextLimitInputTypeTelephoneNumber;
        _phoneField.textColor = color_TextOne;
        _phoneField.font = [UIFont systemFontOfSize:16];
        _phoneField.keyboardType = UIKeyboardTypePhonePad;
        _phoneField.backgroundColor = [UIColor whiteColor];
        _phoneField.placeholder = @"请输入手机号";
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
        UIImageView *leftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"phone_bbbcca"]];
        leftImage.contentMode = UIViewContentModeScaleAspectFit;
        [leftView addSubview:leftImage];
        [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(13, 13, 13, 13));
        }];
        _phoneField.leftView = leftView;
        _phoneField.leftViewMode = UITextFieldViewModeAlways;
        [_phoneField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
        
    }
    return _phoneField;
}
- (LTLimitTextField *)passWordField{
    if (!_passWordField) {
        _passWordField = [LTLimitTextField new];
        _passWordField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
        _passWordField.textLimitType = LTTextLimitTypeLength;
        _passWordField.textLimitSize = 32;
        _passWordField.textColor = color_TextOne;
        _passWordField.font = [UIFont systemFontOfSize:16];
        _passWordField.secureTextEntry = YES;
        _passWordField.backgroundColor = [UIColor whiteColor];
        _passWordField.placeholder = @"请输入6-32位密码";
        
      
        if (_type == 0) {
            
            UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
            UIImageView *leftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"password_bbbcca"]];
            leftImage.contentMode = UIViewContentModeScaleAspectFit;
            [leftView addSubview:leftImage];
            [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(13, 13, 13, 13));
            }];
            _passWordField.leftView = leftView;
            _passWordField.leftViewMode = UITextFieldViewModeAlways;
            
            UIView *rightView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
            UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [rightBtn setImage:[UIImage imageNamed:@"login_password_hide"] forState:UIControlStateNormal];
            [rightBtn setImage:[UIImage imageNamed:@"login_password_show"] forState:UIControlStateSelected];
            [rightBtn addTarget:self action:@selector(showOrHidePassWord:) forControlEvents:UIControlEventTouchUpInside];
            [rightView addSubview:rightBtn];
            [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(rightView);
                make.size.mas_equalTo(CGSizeMake(44, 44));
            }];
            _passWordField.rightView = rightView;
        }else {
            
            UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
            UIImageView *leftImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"yanzhengma_bbbcca"]];
            leftImage.contentMode = UIViewContentModeScaleAspectFit;
            [leftView addSubview:leftImage];
            [leftImage mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(UIEdgeInsetsMake(13, 13, 13, 13));
            }];
            _passWordField.leftView = leftView;
            _passWordField.leftViewMode = UITextFieldViewModeAlways;
            
            _passWordField.textLimitInputType = LTTextLimitInputTypeNumber;
            _passWordField.textLimitType = LTTextLimitTypeLength;
            _passWordField.textLimitSize = 4;
            _passWordField.keyboardType = UIKeyboardTypePhonePad;
            _passWordField.rightView = self.getCodeBtn;
            _passWordField.rightViewMode = UITextFieldViewModeAlways;
            _passWordField.placeholder = @"验证码";
        }
        
        _passWordField.rightViewMode = UITextFieldViewModeAlways;
        [_passWordField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _passWordField;
}
- (UIButton *)registeBtn{
    if (!_registeBtn) {
        _registeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_registeBtn setTitle:@"新用户注册" forState:UIControlStateNormal];
        [_registeBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _registeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _registeBtn.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        [_registeBtn addTarget:self action:@selector(turnRegiste) forControlEvents:UIControlEventTouchUpInside];
    }
    return _registeBtn;
}
- (UIButton *)switchLoginBtn{
    if (!_switchLoginBtn) {
        _switchLoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (_type == 0) {
            [_switchLoginBtn setTitle:@"验证码登录" forState:UIControlStateNormal];
        }else {
            [_switchLoginBtn setTitle:@"密码登录" forState:UIControlStateNormal];
        }
        [_switchLoginBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _switchLoginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _switchLoginBtn.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        [_switchLoginBtn addTarget:self action:@selector(switchBtn) forControlEvents:UIControlEventTouchUpInside];
    
    }
    return _switchLoginBtn;
}

- (UIButton *)forgetBtn{
    if (!_forgetBtn) {
        _forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_forgetBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
        [_forgetBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _forgetBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _forgetBtn.contentEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15);
        [_forgetBtn addTarget:self action:@selector(turnForgetPassWord) forControlEvents:UIControlEventTouchUpInside];
    
    }
    return _forgetBtn;
}

- (UIButton *)getCodeBtn{
    if (!_getCodeBtn) {
        _getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeBtn.frame = CGRectMake(0, 0, 100, 32);
        [_getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _getCodeBtn.backgroundColor = APPTintColor;
        _getCodeBtn.layer.masksToBounds = YES;
        _getCodeBtn.layer.cornerRadius = 4.0f;
        [_getCodeBtn addTarget:self action:@selector(sendMobileCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getCodeBtn;
}

- (UIButton *)LoginBtn{
    if (!_LoginBtn) {
        _LoginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _LoginBtn.backgroundColor = APPTintColor;
        [_LoginBtn setTitle:@"登录" forState:UIControlStateNormal];
        [_LoginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _LoginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_LoginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        _LoginBtn.layer.cornerRadius = 4;
        _LoginBtn.enabled = NO;
        _LoginBtn.alpha = 0.5;
    }
    return _LoginBtn;
}
- (UILabel *)loginTypeLab {
    if (!_loginTypeLab) {
        _loginTypeLab = [UILabel new];
        _loginTypeLab.text = @"———————其他方式登录——————";
        _loginTypeLab.font = [UIFont systemFontOfSize:14.0f];
        _loginTypeLab.textColor = [UIColor blackColor];
        if ([WXApi isWXAppInstalled]) {
            _loginTypeLab.hidden = NO;
        }else {
            _loginTypeLab.hidden = YES;
        }
    }
    return _loginTypeLab;
}
- (UIButton *)WXBtn {
    if (!_WXBtn) {
        _WXBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_WXBtn setImage:[UIImage imageNamed:@"wx"] forState:(UIControlStateNormal)];
        [_WXBtn addTarget:self action:@selector(WXBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
        if ([WXApi isWXAppInstalled]) {
            _WXBtn.hidden = NO;
        }else {
            _WXBtn.hidden = YES;
        }
    }
    return _WXBtn;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
