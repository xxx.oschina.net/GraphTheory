//
//  WGForgetPassWordViewController.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGForgetPassWordViewController.h"
#import "BYTimer.h"
#import "NSString+Addition.h"
#import "YXLonginViewModel.h"
#import "WGLoginViewController.h"
#import "DDBaseNavigationController.h"
#import "ZKVerifyAlertView.h"
@interface WGForgetPassWordViewController ()

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UIButton *saveBtn;
@property (nonatomic, strong) UIButton *getCodeBtn;
@property (nonatomic, strong) UIView *textFieldView;

@property (nonatomic, strong) NSMutableArray <LTLimitTextField *>*textFieldArr;

@property (nonatomic, strong) NSString *randomStr;
@property (nonatomic, strong) NSString *code;

@end

@implementation WGForgetPassWordViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.type == 0) {
        self.title = @"忘记密码";
    }else {
        self.title = @"修改密码";
    }
    [self setUpLayout];
}
//- (void)getCode{
//
//    if (_textFieldArr[0].text.length == 0) {
//         KPOP(@"请输入手机号");
//         return;
//     }
//
//    DDWeakSelf
//     [YXLonginViewModel queryGetCodeCompletion:^(id  _Nonnull responesObj) {
//         weakSelf.code = responesObj[@"data"];
//         NSString *randomStr = responesObj[@"randomStr"];
//         ZKVerifyAlertView *verifyView = [[ZKVerifyAlertView alloc] initWithMaximumVerifyNumber:3 results:^(ZKVerifyState state) {
//             if (state == ZKVerifyStateSuccess) {
//                 [weakSelf sendMobileCodeWithRandomStr:randomStr];
//             }
//         }];
//         [verifyView show];
//     } Failure:^(NSError * _Nonnull error) {
//         [YJProgressHUD showError:REQUESTERR];
//     }];
//
//    NSLog(@"获取验证码");
//
//}

// 发送短信验证码
- (void)sendMobileCodeWithRandomStr:(NSString *)randomStr {
    NSLog(@"获取验证码");
    DDWeakSelf
    if (_textFieldArr[0].text.length == 0) {
        KPOP(@"请输入手机号");
        return;
    }
    if (![_textFieldArr[0].text isTelephoneFullNumber]) {
        KPOP(@"手机号错误，请重新输入");
        return;
    }
    if (randomStr.length == 0) {
        KPOP(@"验证码序列号有误，请重新获取");
        return;
    }
    [YJProgressHUD showLoading:@"发送中..."];
    [YXLonginViewModel requestSendMobileCodeWithType:@"other" Mobile:self.textFieldArr[0].text Code:self.code RandomStr:randomStr Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:@"发送验证码成功"];
            [weakSelf openCountdown];
            weakSelf.randomStr = [NSString stringWithFormat:@"%@",responesObj[@"data"][@"randomStr"]];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}



- (void)openCountdown {
    YXWeakSelf
    [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
        [weakSelf.getCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%d)",presentTime] forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = NO;
    } finish:^{
        [weakSelf.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = YES;
    }];
}

- (void)showOrHidePassWord:(UIButton *)btn{
    btn.selected = !btn.selected;
    UITextField *tem = _textFieldArr[2];
    tem.secureTextEntry = !btn.selected;
}
- (void)save{
    if ([self verifyData:YES]) {
        
        YXWeakSelf
        [YJProgressHUD showLoading:@"加载中..."];
        [YXLonginViewModel requestUpdateMerchantPasswordWithMobile:_textFieldArr[0].text
                                                              Code:_textFieldArr[1].text
                                                         RandomStr:self.randomStr
                                                            NewPwd:_textFieldArr[2].text
                                                        Completion:^(id  _Nonnull responesObj) {
                                                            if (REQUESTDATASUCCESS) {
                                                                [YJProgressHUD showMessage:responesObj[@"msg"]];
                                                                if (weakSelf.type == 1) {
                                                                    [NetWorkTools pushLoginVC];
                                                                    
                                                                }else{
                                                                    [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                                                }
                                                            }else {
                                                                [YJProgressHUD showMessage:responesObj[@"msg"]];
                                                            }
                                                        } failure:^(NSError * _Nonnull error) {
                                                            [YJProgressHUD showError:REQUESTERR];
                                                        }];
        
    }
}
- (void)textFieldChanged:(UITextField*)textField{
    [self verifyData:NO];
}
- (BOOL)verifyData:(BOOL)showMsg{
    NSString *msg = @"";
    BOOL correct = YES;
    if (_textFieldArr[2].text.length < 6||_textFieldArr[2].text.length > 18){
        msg = @"密码格式有误，请重新设置";
        correct = NO;
    }else if (_textFieldArr[1].text.length != 4){
        msg = @"验证码错误，请重新提示";
        correct = NO;
    }  else  if (_textFieldArr[0].text.length == 0) {
        msg = @"请输入手机号";
        correct = NO;
    }
    
    else if (![_textFieldArr[0].text isTelephoneFullNumber]) {
        msg = @"手机号错误，请重新输入";
        correct = NO;
    }
    if (showMsg) {
        KPOP(msg);
    }
    _saveBtn.enabled = correct;
    _saveBtn.alpha = correct?1:0.5;
    return correct;
}
- (void)setUpLayout{
    [self.view addSubview:self.titleLab];
    [self configEnter];
    [self.view addSubview:self.textFieldView];
    [self.view addSubview:self.saveBtn];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15);
        [self.titleLab sizeToFit];
    }];
    
    [_textFieldView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.and.right.equalTo(self.view);
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.top.equalTo(self.titleLab.mas_bottom).offset(50);
        make.height.mas_equalTo(@200);
    }];
    [_textFieldArr mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedItemLength:50 leadSpacing:0 tailSpacing:0];
    [_textFieldArr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self.view);
    }];
    [_saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textFieldView.mas_bottom).offset(15);
        make.left.equalTo(self.view.mas_left).offset(15);
        make.right.equalTo(self.view.mas_right).offset(-15);
        make.height.equalTo(@44);
    }];
    
    
}
/**
 配置输入框样式
 */
- (void)configEnter{
    NSArray *temArr = @[@"请输入手机号",@"验证码",@"请输入6-32位新密码",@"请再次输入密码"];
    for (int i = 0; i < temArr.count; i ++ ) {
        LTLimitTextField * temTextField = [LTLimitTextField new];
        temTextField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
        temTextField.textLimitType = LTTextLimitTypeLength;
        temTextField.textLimitSize = 18;
        temTextField.textColor = color_TextOne;
        temTextField.font = [UIFont systemFontOfSize:14];
        temTextField.backgroundColor = [UIColor whiteColor];
        temTextField.placeholder = temArr[i];
        UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 50)];
        temTextField.leftView = leftView;
        temTextField.leftViewMode = UITextFieldViewModeAlways;
        
        if (i == 0) {
            temTextField.textLimitInputType = LTTextLimitInputTypeTelephoneNumber;
            temTextField.keyboardType = UIKeyboardTypePhonePad;
        }else if (i == 1){
            temTextField.textLimitInputType = LTTextLimitInputTypeNumber;
            temTextField.textLimitType = LTTextLimitTypeLength;
            temTextField.textLimitSize = 4;
            temTextField.keyboardType = UIKeyboardTypePhonePad;
            temTextField.rightView = self.getCodeBtn;
            temTextField.rightViewMode = UITextFieldViewModeAlways;
            
        }else if (i == 2 || i == 3){
            temTextField.textLimitInputType = LTTextLimitInputTypeEnglishOrNumber;
            temTextField.textLimitType = LTTextLimitTypeLength;
            temTextField.textLimitSize = 18;
            temTextField.secureTextEntry = YES;
            UIButton *rightView = [UIButton buttonWithType:UIButtonTypeCustom];
            rightView.frame = CGRectMake(0, 0, 44, 44);
            [rightView setImage:[UIImage imageNamed:@"login_password_hide"] forState:UIControlStateNormal];
            [rightView setImage:[UIImage imageNamed:@"login_password_show"] forState:UIControlStateSelected];
            [rightView addTarget:self action:@selector(showOrHidePassWord:) forControlEvents:UIControlEventTouchUpInside];
            temTextField.rightView = rightView;
            temTextField.rightViewMode = UITextFieldViewModeAlways;
            
        }
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
        [temTextField addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.and.bottom.equalTo(temTextField);
            make.left.equalTo(temTextField.mas_left).offset(15);
            make.height.mas_equalTo(@1);
        }];

        [temTextField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [self.textFieldView addSubview:temTextField];
        [self.textFieldArr addObject:temTextField];
        
       
    }
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = [UIColor blackColor];
        _titleLab.text = @"找回密码";
        _titleLab.font = [UIFont systemFontOfSize:20.0f];
    }
    return _titleLab;
}

- (NSMutableArray *)textFieldArr{
    if (!_textFieldArr) {
        _textFieldArr = [NSMutableArray new];
    }
    return _textFieldArr;
}
- (UIView *)textFieldView{
    if (!_textFieldView) {
        _textFieldView = [UIView new];
    }
    return _textFieldView;
}

- (UIButton *)getCodeBtn{
    if (!_getCodeBtn) {
        _getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeBtn.frame = CGRectMake(0, 0, 100, 50);
        [_getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getCodeBtn setTitleColor:color_TextOne forState:UIControlStateNormal];
        _getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_getCodeBtn addTarget:self action:@selector(getCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getCodeBtn;
}
- (UIButton *)saveBtn{
    if (!_saveBtn) {
        _saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _saveBtn.backgroundColor = APPTintColor;
        [_saveBtn setTitle:@"完成设置" forState:UIControlStateNormal];
        [_saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _saveBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_saveBtn addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
        _saveBtn.layer.cornerRadius = 4;
        _saveBtn.enabled = NO;
        _saveBtn.alpha = 0.5;
    }
    return _saveBtn;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
