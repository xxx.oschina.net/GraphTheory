//
//  WGLoginViewController.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 登录
 */
@interface WGLoginViewController : DDBaseViewController

@property (nonatomic ,assign) BOOL isPresent;///模态弹出

@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
