//
//  WGWebViewController.m
//  DDLife
//
//  Created by wanggang on 2019/10/30.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "WGWebViewController.h"
#import <WebKit/WebKit.h>


@interface WGWebViewController ()<WKNavigationDelegate>

@property(strong,nonatomic) WKWebView *wkWebView;

@end

@implementation WGWebViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"注册协议";
    [self.view addSubview:self.wkWebView];
    [self.wkWebView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_top);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft).offset(15);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight).offset(-15);
        }else{
            make.edges.mas_equalTo(self.view).mas_offset(UIEdgeInsetsMake(0, 15, 0, 15));
        }
    }];
//    NSString *bundleStr = [[NSBundle mainBundle] pathForResource:@"xieyi" ofType:@"html"];
//
    [_wkWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://syjtest.dd2007.cn/registAgreement.html"]]];
}
- (WKWebView *)wkWebView{
    if (!_wkWebView) {
        NSMutableString *jScript = [NSMutableString string];
        [jScript appendString:@"document.documentElement.style.webkitUserSelect='none';"];//禁止选择
        [jScript appendString:@"document.documentElement.style.webkitTouchCallout='none';"];//禁止长按
        
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];
        
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;
        wkWebConfig.preferences.javaScriptCanOpenWindowsAutomatically = true;
        //是否支持JavaScript
        wkWebConfig.preferences.javaScriptEnabled = true;
        [wkWebConfig.preferences setValue:@YES forKey:@"allowFileAccessFromFileURLs"];
        _wkWebView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:wkWebConfig];
        _wkWebView.navigationDelegate = self;
        _wkWebView.backgroundColor = [UIColor whiteColor];
        _wkWebView.opaque = NO;
        if (@available(iOS 11.0, *)) {
            _wkWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }else {
        }
        _wkWebView.scrollView.showsVerticalScrollIndicator = NO;
    }
    return _wkWebView;
}



@end
