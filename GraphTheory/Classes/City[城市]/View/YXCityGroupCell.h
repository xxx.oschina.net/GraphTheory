//
//  YXCityGroupCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YXCityModel.h"




@protocol CityGroupCellDelegate <NSObject>

- (void) cityGroupCellDidSelectCity:(YXCityModel *)city;

@end

@interface YXCityGroupCell : UITableViewCell

@property (nonatomic, assign) id <CityGroupCellDelegate> delegate;

/**
 *  标题
 */
@property (nonatomic, strong) UILabel *titleLabel;
/**
 *  暂无数据
 */
@property (nonatomic, strong) UILabel *noDataLabel;
/**
 *  btn数组
 */
@property (nonatomic, strong) NSMutableArray *arrayCityButtons;
/**
 *  城市数据信息
 */
@property (nonatomic, strong) NSArray *cityArray;
/**
 *  返回cell高度
 *
 *  @param cityArray cell的数量
 *
 *  @return 返回cell高度
 */
+ (CGFloat) getCellHeightOfCityArray:(NSArray *)cityArray;

@end

