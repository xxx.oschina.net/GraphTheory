//
//  YXCityGroupCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#define     MIN_SPACE           8           // 城市button最小间隙
#define     MAX_SPACE           10

#define     WIDTH_LEFT          13.5        // button左边距
#define     WIDTH_RIGHT         28          // button右边距

#define     MIN_WIDTH_BUTTON    75
#define     HEIGHT_BUTTON       38
#import "YXCityGroupCell.h"

@implementation YXCityGroupCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
//        [self addSubview:self.titleLabel];
        [self.contentView addSubview:self.noDataLabel];
    }
    return self;
}
#pragma mark - Getter
//- (UILabel *) titleLabel
//{
//    if (_titleLabel == nil) {
//        _titleLabel = [[UILabel alloc] init];
//        [_titleLabel setFont:[UIFont systemFontOfSize:14.0]];
//        _titleLabel.backgroundColor = [UIColor whiteColor];
//    }
//    return _titleLabel;
//}

- (UILabel *) noDataLabel
{
    if (_noDataLabel == nil) {
        _noDataLabel = [[UILabel alloc] init];
        [_noDataLabel setText:@"暂无数据"];
        [_noDataLabel setTextColor:[UIColor grayColor]];
        [_noDataLabel setFont:[UIFont systemFontOfSize:14.0f]];
    }
    return _noDataLabel;
}

- (NSMutableArray *) arrayCityButtons
{
    if (_arrayCityButtons == nil) {
        _arrayCityButtons = [[NSMutableArray alloc] init];
    }
    return _arrayCityButtons;
}

- (void) setCityArray:(NSArray *)cityArray
{
    _cityArray = cityArray;
    [self.noDataLabel setHidden:(cityArray != nil && cityArray.count > 0)];
    
    for (int i = 0; i < cityArray.count; i ++) {
        YXCityModel *city = [cityArray objectAtIndex:i];
        UIButton *button = nil;
        if (i < self.arrayCityButtons.count) {
            button = [self.arrayCityButtons objectAtIndex:i];
        }
        else {
            button = [[UIButton alloc] init];
            [button setBackgroundColor:APPTintColor];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
            [button.layer setMasksToBounds:YES];
            [button.layer setCornerRadius:2.0f];
            [button.layer setBorderColor:APPTintColor.CGColor];
            [button.layer setBorderWidth:1.0f];
            [button addTarget:self action:@selector(cityButtonDown:) forControlEvents:UIControlEventTouchUpInside];
            [self.arrayCityButtons addObject:button];
            [self.contentView addSubview:button];
        }
//        [button setTitle:[city.name substringToIndex:city.name.length - 1] forState:UIControlStateNormal];
        if (city.name) {
            [button setTitle:city.name forState:UIControlStateNormal];
        }else {
            [button setTitle:city.district forState:UIControlStateNormal];
        }
        button.tag = i;
    }
    while (cityArray.count < self.arrayCityButtons.count) {
        [self.arrayCityButtons removeLastObject];
    }
}

#pragma mark - Event Response
- (void) cityButtonDown:(UIButton *)sender
{
    YXCityModel *city = [self.cityArray objectAtIndex:sender.tag];
    if (_delegate && [_delegate respondsToSelector:@selector(cityGroupCellDidSelectCity:)]) {
        [_delegate cityGroupCellDidSelectCity:city];
    }
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    float x = WIDTH_LEFT;
    float y = 5;
//    CGSize size = [self.titleLabel sizeThatFits:CGSizeMake(MAXFLOAT, MAXFLOAT)];
//    [self.titleLabel setFrame:CGRectMake(x, y, self.frame.size.width - x, size.height)];
//    y += size.height + 3;
//    [self.noDataLabel setFrame:CGRectMake(x + 5, y, self.frame.size.width - x - 5, self.titleLabel.frame.size.height)];
    [self.noDataLabel setFrame:CGRectMake(x + 5, y, self.frame.size.width - x - 5, 15)];

    y += 7;
    float space = MIN_SPACE;            // 最小空隙
    float width = MIN_WIDTH_BUTTON;     // button最小宽度
    int t = (self.frame.size.width - WIDTH_LEFT - WIDTH_RIGHT + space) / (width + space);
    
    // 修正空隙宽度
    space = (self.frame.size.width - WIDTH_LEFT - WIDTH_RIGHT - width * t) / (t - 1);
    
    if (space > MAX_SPACE) {                                                  // 修正button宽度
        width += (space - MAX_SPACE) * (t - 1) / t;
        space = MAX_SPACE;
    }
    
    for (int i = 0; i < self.arrayCityButtons.count; i ++) {
        UIButton *button = [self.arrayCityButtons objectAtIndex:i];
        [button setFrame:CGRectMake(x, y, width, HEIGHT_BUTTON)];
        if ((i + 1) % t == 0) {
            y += HEIGHT_BUTTON + 5;
            x = WIDTH_LEFT;
        }else {
            x += width + space;
        }
    }
}

+ (CGFloat) getCellHeightOfCityArray:(NSArray *)cityArray
{
    float h = 15;
    if (cityArray != nil && cityArray.count > 0) {
        float space = MIN_SPACE;            // 最小空隙
        float width = MIN_WIDTH_BUTTON;     // button最小宽度
        int t = ([UIScreen mainScreen].bounds.size.width - WIDTH_LEFT - WIDTH_RIGHT + space) / (width + space);
        
        space = ([UIScreen mainScreen].bounds.size.width - WIDTH_LEFT - WIDTH_RIGHT - width * t) / (t - 1);        // 修正空隙宽度
        if (space > MAX_SPACE) {                                                                // 修正button宽度
            width += (space - MAX_SPACE) * (t - 1) / t;
            space = MAX_SPACE;
        }
        
        h += (0 + (HEIGHT_BUTTON + 5) * (cityArray.count / t + (cityArray.count % t == 0 ? 0 : 1)));
    }
    else {
        h += 17;
    }
    return h;
}


@end
