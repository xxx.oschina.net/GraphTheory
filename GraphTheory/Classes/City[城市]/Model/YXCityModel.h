//
//  YXSelectCityModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCityModel : DDBaseModel

/**
 *  城市id
 */
@property (nonatomic, strong) NSString *areaId;
/**
 *  城市名字
 */
@property (nonatomic, strong) NSString *name;
/**
 *  城市拼音
 */
@property (nonatomic, strong) NSString *en;
/**
 *  是否是热门城市
 */
@property (nonatomic, strong) NSString *HostName;
/**
 *  纬度
 */
@property (nonatomic, assign) CGFloat lat;
/**
 *  经度
 */
@property (nonatomic, assign) CGFloat lon;
/**
 *  经纬度
 */
@property (nonatomic, strong) NSString *longitude;

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *district;
@property (nonatomic, copy) NSString *is_open;
@property (nonatomic, copy) NSString *level;
@property (nonatomic, copy) NSString *pid;
@property (nonatomic, copy) NSString *sort_order;

@property (nonatomic, strong) YXCityModel  *child;


- (instancetype)initWithDic:(NSDictionary *)dic;


@end


@interface YXOpenCityModel : DDBaseModel

/**
 *  城市拼音
 */
@property (nonatomic, copy) NSString *FIRST_LETTER;

/**
 *  城市分组
 */
@property (nonatomic, strong) NSArray *citys;

@end


@interface YXCitysModel : DDBaseModel

/**
 *  城市id
 */
@property (nonatomic, copy) NSString *Id;

/**
 *  城市名称
 */
@property (nonatomic, copy) NSString *text;

@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *cityName;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *weather;
@property (nonatomic, copy) NSString *POIName;
@property (nonatomic, assign) CGFloat lat;
@property (nonatomic, assign) CGFloat lng;


@end

NS_ASSUME_NONNULL_END
