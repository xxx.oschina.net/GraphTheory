//
//  YXSelectCityModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXCityModel.h"

@implementation YXCityModel

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dic];
    }
    return self;
}


@end


@implementation YXOpenCityModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"citys" : @"YXCitysModel"};
}

@end


@implementation YXCitysModel


@end
