//
//  YXCityListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXCityModel;
@interface YXCityListViewController : DDBaseViewController

@property (nonatomic ,copy)void(^selectCiytBlock)(YXCityModel *model);

@end

NS_ASSUME_NONNULL_END
