//
//  YXSelectCityViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXCityModel,YXCitysModel;
@interface YXSelectCityViewController : DDBaseViewController


/**
 回调定位城市
 */
@property (nonatomic ,copy)void(^locationCityBlock)(YXCityModel *cityModel);

/**
 回调选择已开通城市
 */
@property (nonatomic ,copy)void(^selectCityBlock)(YXCitysModel *citysModel);

// type 0:广告  1:首页推荐
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
