//
//  YXSelectCityViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXSelectCityViewController.h"

#import "YXPublicTableHeaderView.h"
#import "YXCityGroupCell.h"

#import "YXHomeViewModel.h"
#import "YXCityModel.h"
//#import "YXLocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import <AFNetworking.h>

#import "YXAMapManager.h"
#import "PinYin4Objc.h"
NSString *const cityGroupCell = @"CityGroupCell";
@interface YXSelectCityViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,CityGroupCellDelegate>
/**
 *  定位城市
 */
@property (nonatomic, strong) NSMutableArray *localCityData;

/**
 *  已开通城市
 */
@property (nonatomic, strong) NSMutableArray *openCityArr;

/**
 *  拼音
 */
@property (nonatomic, strong) NSMutableArray *pinyinArr;

@property (nonatomic, strong) NSMutableArray *hotArr;


@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (nonatomic,strong) YXCityModel *cityModel;
@property(nonatomic,strong) NSArray *lettersArray;
@property(nonatomic,strong) NSMutableDictionary *nameDic;

@end

@implementation YXSelectCityViewController

- (NSMutableArray *)localCityData {
    if (_localCityData == nil) {
        _localCityData = [NSMutableArray array];
    }
    return _localCityData;
}

- (NSMutableArray *)openCityArr {
    if (!_openCityArr) {
        _openCityArr = [NSMutableArray array];
    }
    return _openCityArr;
}

- (NSMutableArray *)pinyinArr {
    if (!_pinyinArr) {
        _pinyinArr = [NSMutableArray array];
        [self.pinyinArr addObject:@"定位"];
        [self.pinyinArr addObject:@"热门"];
    }
    return _pinyinArr;
}


- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = color_LineColor;
        _tableView.dataSource = self;
        _tableView.delegate = self;
        [_tableView setSectionIndexColor:color_TextOne];
        [_tableView registerClass:[YXPublicTableHeaderView class] forHeaderFooterViewReuseIdentifier:@"YXPublicTableHeaderView"];
        [_tableView registerClass:[YXCityGroupCell class] forCellReuseIdentifier:cityGroupCell];
        [_tableView registerClass:[YXCityGroupCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"定位";
    self.hotArr = [NSMutableArray array];
    self.lettersArray = [[NSArray alloc]init];
    self.nameDic = [[NSMutableDictionary alloc]init];

    // 定位
    DDWeakSelf
    [[YXAMapManager shareLocation] getLocationInfo:^(YXCitysModel * _Nonnull cityModel) {
        weakSelf.cityModel = [[YXCityModel alloc] init];
        weakSelf.cityModel.name = cityModel.cityName;
        weakSelf.cityModel.Id = cityModel.cityId;
        [weakSelf.localCityData addObject:weakSelf.cityModel];
        [weakSelf.tableView reloadData];
    }];
    
    // 加载数据
    [self loadData];
    
    // 加载子视图
    [self loadSubViews];
    
}

#pragma mark - 请求已开通的城市列表
- (void)loadData {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@"加载中..."];
    [YXHomeViewModel queryAllDistrictsCompletion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSArray *dataArr = [YXCityModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
            for (YXCityModel *cityModel in dataArr) {
                [weakSelf.openCityArr addObject:cityModel.child];
            }
            [weakSelf handleLettersArray];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
    [YXHomeViewModel queryOpenServiceCityCompletion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.hotArr = [YXCityModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
    
}
     
     //处理letterArray，包括按英文字母顺序排序
- (void)handleLettersArray  {
    NSMutableDictionary *tempDic = [[NSMutableDictionary alloc]init];
    
    for(YXCityModel *friends  in self.openCityArr)
    {
        HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
        formatter.caseType = CaseTypeLowercase;
        formatter.vCharType = VCharTypeWithV;
        formatter.toneType = ToneTypeWithoutTone;
        
        NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:friends.district withHanyuPinyinOutputFormat:formatter withNSString:@""];
        //        NSLog(@"%@",[[outputPinyin substringToIndex:1] uppercaseString]);
        [tempDic setObject:friends forKey:[[outputPinyin substringToIndex:1] uppercaseString]];
    }
    
    self.lettersArray = tempDic.allKeys;
    
    for (NSString *letter in self.lettersArray) {
        NSMutableArray *tempArry = [[NSMutableArray alloc] init];
        
        for (NSInteger i = 0; i<self.openCityArr.count; i++) {
            YXCityModel *friends = self.openCityArr[i];
            HanyuPinyinOutputFormat *formatter =  [[HanyuPinyinOutputFormat alloc] init];
            formatter.caseType = CaseTypeUppercase;
            formatter.vCharType = VCharTypeWithV;
            formatter.toneType = ToneTypeWithoutTone;
            
            //把friend的userName汉子转为汉语拼音，比如：张磊---->zhanglei
            NSString *outputPinyin=[PinyinHelper toHanyuPinyinStringWithNSString:friends.district withHanyuPinyinOutputFormat:formatter withNSString:@""];
            if ([letter isEqualToString:[[outputPinyin substringToIndex:1] uppercaseString]]) {
                [tempArry addObject:friends];
                
            }
            
        }
        [self.nameDic setObject:tempArry forKey:letter];
    }
    
    self.lettersArray = tempDic.allKeys;
    //排序，排序的根据是字母
    NSComparator cmptr = ^(id obj1, id obj2){
        if ([obj1 characterAtIndex:0] > [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 characterAtIndex:0] < [obj2 characterAtIndex:0]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    self.lettersArray = [[NSMutableArray alloc]initWithArray:[self.lettersArray sortedArrayUsingComparator:cmptr]];
    [self.pinyinArr addObjectsFromArray:self.lettersArray];
}



#pragma mark - 加载子视图
- (void)loadSubViews {
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.edges.mas_equalTo(self.view.safeAreaInsets);
        }else{
            make.edges.offset(0);
        }
    }];
    
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.lettersArray.count + 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1) {
        return 1;
    }else {
//        YXOpenCityModel *openCityModel = self.openCityArr[section-2];
//        return openCityModel.citys.count;
        NSArray *nameArray = [self.nameDic objectForKey:self.lettersArray[section-2]];
        return nameArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        YXCityGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:cityGroupCell];
        cell.delegate = self;
        cell.noDataLabel.text = @"无法定位当前城市，请稍后再试";
        [cell setCityArray:self.localCityData];
        return cell;
    }else if (indexPath.section == 1) {
        YXCityGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.delegate = self;
        [cell setCityArray:self.hotArr];
        return cell;
    }else if (indexPath.section > 1) {
        static NSString *cellId = @"CityCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellId];
        }
        YXCityModel *frends = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section-2]] objectAtIndex:indexPath.row];
        cell.textLabel.text = frends.district;
//        YXOpenCityModel *openCityModel = self.openCityArr[indexPath.section-2];
//        YXCitysModel *citysModel = openCityModel.citys[indexPath.row];
//        cell.textLabel.text = citysModel.text;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0 || section == 1) {
        return 40;
    }
    return 28;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return [YXCityGroupCell getCellHeightOfCityArray:self.localCityData];
    }else if (indexPath.section == 1) {
        return [YXCityGroupCell getCellHeightOfCityArray:self.hotArr];
    }
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    YXPublicTableHeaderView *headerView = [[YXPublicTableHeaderView alloc] initWithReuseIdentifier:@"YXPublicTableHeaderView"];
    if (section == 0) {
        headerView.title = @"当前定位城市";
    }else if (section == 1) {
        headerView.title = @"热门城市";
    }else {
//        YXOpenCityModel *openCityModel = self.openCityArr[section-2];
//        headerView.title = openCityModel.FIRST_LETTER;
        
        headerView.title = [self.lettersArray objectAtIndex:section-2];
    }
//    headerView.title = [self.sectionAy[section] uppercaseString];
    return headerView;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return self.pinyinArr;

}


-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    if (index == 0) {
        return -1;
    }
    return index - 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
    }
    
    if (indexPath.section > 1) {
//        YXOpenCityModel *openCityModel = self.openCityArr[indexPath.section-2];
//        YXCitysModel *citysModel = openCityModel.citys[indexPath.row];
//        if (self.selectCityBlock) {
//            self.selectCityBlock(citysModel);
//        }
        YXCityModel *cityModel = [[self.nameDic objectForKey:[self.lettersArray objectAtIndex:indexPath.section-2]] objectAtIndex:indexPath.row];
        NSLog(@"%@",cityModel.district);
        if (self.locationCityBlock) {
            self.locationCityBlock(cityModel);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark CityGroupCellDelegate代理方法(定位, 热门, 最近访问)
- (void) cityGroupCellDidSelectCity:(YXCityModel *)city
{
    NSLog(@"%@",city.name);
    if (city.name) {
        city.district = city.name;
    }
    if (self.locationCityBlock) {
        self.locationCityBlock(city);
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)dealloc {
    NSLog(@"%s",__func__);
}


@end
