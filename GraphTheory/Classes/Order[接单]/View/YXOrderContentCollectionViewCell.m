//
//  YXOrderContentCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderContentCollectionViewCell.h"
#import "YXUserOrderModel.h"

@interface YXOrderContentCollectionViewCell ()

@property (nonatomic ,strong) UIImageView *leftImgView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic ,strong) UIView *lineView;

@end

@implementation YXOrderContentCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setModel:(YXUserOrderModelList *)model {
    _model = model;
    
    if (_type == 2) {
        [_leftImgView sd_setImageWithURL:[NSURL URLWithString:_model.product_details.imgs]];
        _titleLab.text = _model.product_details.name;
    }else {
        _titleLab.text = _model.content;
        [_leftImgView sd_setImageWithURL:[NSURL URLWithString:_model.product_icon]];
    }
    _timeLab.text = _model.create_time;

}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.leftImgView];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.timeLab];
        [self.contentView addSubview:self.lineView];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lozy Loding
- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [UIImageView new];
        _leftImgView.image = [UIImage imageNamed:@"marketing_coupons"];
    }
    return _leftImgView;
}
- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"这个题目如何用方程式解答？";
    }
    return _titleLab;
}
- (UILabel *)timeLab {
    if (!_timeLab ) {
        _timeLab = [UILabel new];
        _timeLab.textColor = color_TextThree;
        _timeLab.font = [UIFont systemFontOfSize:12];
        _timeLab.text = @"10-12 13:18";
    }
    return _timeLab;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

- (void)addLayout {
    
    YXWeakSelf
    [_leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.leftImgView.mas_right).offset(15);
        make.top.equalTo(weakSelf.leftImgView.mas_top);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.leftImgView.mas_right).offset(15);
        make.bottom.equalTo(weakSelf.leftImgView.mas_bottom);
        [weakSelf.timeLab sizeToFit];
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(15);
        make.right.equalTo(self.mas_right).offset(-15);
        make.bottom.equalTo(self.mas_bottom);
        make.height.equalTo(@0.5);
    }];
}

@end
