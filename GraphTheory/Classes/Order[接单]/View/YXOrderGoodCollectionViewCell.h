//
//  YXOrderGoodCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCateDetailModel,YXGoodsInfoModel;
@interface YXOrderGoodCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) YXCateDetailModel* model;

@property (nonatomic ,strong) YXGoodsInfoModel* goodsModel;

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,copy)void(^clickCouponsBlock)(void);

@end

NS_ASSUME_NONNULL_END
