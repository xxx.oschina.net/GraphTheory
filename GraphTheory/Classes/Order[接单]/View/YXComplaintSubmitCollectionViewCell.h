//
//  YXComplaintSubmitCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXComplaintSubmitCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (nonatomic ,copy)void(^clickBtnBlock)(void);
@end

NS_ASSUME_NONNULL_END
