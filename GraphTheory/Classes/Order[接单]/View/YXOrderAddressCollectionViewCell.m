//
//  YXOrderAddressCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderAddressCollectionViewCell.h"
#import "YXAddressModel.h"
@implementation YXOrderAddressCollectionViewCell

- (void)setModel:(YXAddressModel *)model {
    _model = model;
    if (!_model) {
        self.icon.hidden = YES;
        self.nameLab.hidden = YES;
        self.addressLab.hidden = YES;
        self.titleLab.hidden = NO;
    }else {
        self.icon.hidden = NO;
        self.nameLab.hidden = NO;
        self.addressLab.hidden = NO;
        self.titleLab.hidden = YES;
        self.nameLab.text = _model.username;
        self.addressLab.text = [NSString stringWithFormat:@"%@%@%@%@",_model.province,_model.city,_model.region,_model.address];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    
}

@end
