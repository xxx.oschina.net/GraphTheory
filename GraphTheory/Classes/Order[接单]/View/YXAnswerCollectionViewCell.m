//
//  YXAnswerCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAnswerCollectionViewCell.h"
#import "YXOrderInfoModel.h"
#import "YXDataTimeTool.h"
#import "YXPreviewManage.h"
#import "WZHPhotoBrowserView.h"

@interface YXAnswerCollectionViewCell ()
@property (strong, nonatomic)  WZHPhotoBrowserView *photoView;

@end

@implementation YXAnswerCollectionViewCell

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    _contentLab.text = _model.answer_desc;
    NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.finish_time integerValue]];
    _timeLab.text = [NSString stringWithFormat:@"完成时间：%@",time];

    if (_model.answer.count > 0) {
        _photoView.dataArray = _model.answer;
        self.photoView.hidden = NO;
    }else  {
        self.photoView.hidden = YES;
    }
    
//    if (_model.answer.count) {
//        [_imgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.answer[0]]]];
//    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.contentView addSubview:self.photoView];
    _timeLab.hidden = YES;
//    self.imgView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
//    [self.imgView addGestureRecognizer:tap];
}

- (WZHPhotoBrowserView *)photoView {
    if (!_photoView) {
        _photoView = [[WZHPhotoBrowserView alloc] initWithFrame:(CGRectMake(15, 40, SCREEN_WIDTH - 30, 75))];
//        _photoView.backgroundColor = [UIColor redColor];
    }
    return _photoView;
}

// 预览图片
- (void)click {

    if (_model.answer.count) {
        [[YXPreviewManage sharePreviewManage] showPhotoWithImgArr:_model.answer currentIndex:0];
    }
}


@end
