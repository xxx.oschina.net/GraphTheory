//
//  YXOrderMenuCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class DDPayMentModel;
@interface YXOrderMenuCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic)  UILabel *nameLab;

@property (nonatomic ,strong) DDPayMentModel *model;

@end

NS_ASSUME_NONNULL_END
