//
//  YXComplaintCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXComplaintCollectionViewCell.h"
#import "YXDataTimeTool.h"
#import "YXOrderInfoModel.h"
@implementation YXComplaintCollectionViewCell

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    _contentLab.text = _model.complain;
    NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.complain_time integerValue]];
    _timeLab.text = [NSString stringWithFormat:@"申诉时间：%@",time];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
