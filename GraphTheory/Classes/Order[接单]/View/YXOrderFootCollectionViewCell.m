//
//  YXOrderFootCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderFootCollectionViewCell.h"
#import "YXUserOrderModel.h"
@interface YXOrderFootCollectionViewCell ()
@property (nonatomic ,strong) UILabel *titleLab;
@end

@implementation YXOrderFootCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
 }

- (void)setModel:(YXUserOrderModelList *)model {
    _model = model;
    _titleLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
    
    if (self.type == 0) {
        if ([model.status isEqualToString:@"1"]) {// 待支付
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"5"]) { // 待接单
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"取消" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"10"]) { // 进行中
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"进行中" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"15"]) {  // 待确认
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"确认" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"25"]) { //  已评价
            if ([model.c_model isEqualToString:@"1"]) {
                self.deleteBtn.hidden = NO;
                self.playBtn.hidden = YES;
                [self.deleteBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
            }else {
                self.deleteBtn.hidden = NO;
                self.playBtn.hidden = NO;
                [self.deleteBtn setTitle:@"已评价" forState:(UIControlStateNormal)];
                [self.playBtn setTitle:@"推荐学习" forState:(UIControlStateNormal)];
            }
        }else if ([model.status isEqualToString:@"20"]) { // 已完成 
            if ([model.order_type isEqualToString:@"2"]) { // 课堂小纸条
                self.deleteBtn.hidden = NO;
                self.playBtn.hidden = YES;
                [self.deleteBtn setTitle:@"课堂小纸条" forState:(UIControlStateNormal)];
            }else {
                if ([model.c_model isEqualToString:@"1"]) {
                    self.deleteBtn.hidden = NO;
                    self.playBtn.hidden = YES;
                    [self.deleteBtn setTitle:@"去评价" forState:(UIControlStateNormal)];
                }else {
                    self.deleteBtn.hidden = NO;
                    self.playBtn.hidden = NO;
                    [self.deleteBtn setTitle:@"去评价" forState:(UIControlStateNormal)];
                    [self.playBtn setTitle:@"推荐学习" forState:(UIControlStateNormal)];
                }
            }
        }else {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
        }
    }else if (self.type == 2) {
        if ([model.status isEqualToString:@"1"]) {// 待支付
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"待支付" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"10"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"待发货" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"15"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"待收货" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"20"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已完成" forState:(UIControlStateNormal)];
        }else {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
        }
    }else {
        if ([model.status isEqualToString:@"10"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"去处理" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"15"]) {
            if ([model.is_complain isEqualToString:@"1"]) {
                self.deleteBtn.hidden = NO;
                self.playBtn.hidden = YES;
                [self.deleteBtn setTitle:@"被申诉" forState:(UIControlStateNormal)];
            }else {
                self.deleteBtn.hidden = NO;
                self.playBtn.hidden = YES;
                [self.deleteBtn setTitle:@"等待确认" forState:(UIControlStateNormal)];
            }
        }else if ([model.status isEqualToString:@"20"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已完成" forState:(UIControlStateNormal)];
        }else if ([model.status isEqualToString:@"25"]) {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已完成" forState:(UIControlStateNormal)];
        }else {
            self.deleteBtn.hidden = NO;
            self.playBtn.hidden = YES;
            [self.deleteBtn setTitle:@"已取消" forState:(UIControlStateNormal)];
        }
    }
  
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLab];
        [self addBottonBtnView];
        [self addLayout];
    }
    return self;
}

#pragma mark - 添加底部按钮
- (void)addBottonBtnView {
    
    self.deleteBtn = [self createBtnWithTitle:@"已取消"];
    self.playBtn = [self createBtnWithTitle:@"推荐学习"];

}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
        _titleLab.text = @"¥0.1";
    }
    return _titleLab;
}

- (void)addLayout {
    
    DDWeakSelf
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(15);
        [self.titleLab sizeToFit];
    }];
    
    [_deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
         make.right.equalTo(weakSelf.mas_right).offset(-15);
         make.size.mas_equalTo(CGSizeMake(68, 28));
     }];
    
    [_playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.deleteBtn.mas_centerY);
        make.right.equalTo(weakSelf.deleteBtn.mas_left).offset(-15);
        make.size.mas_equalTo(CGSizeMake(68, 28));
    }];
}



- (UIButton *)createBtnWithTitle:(NSString *)title  {
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [btn setTitle:title forState:(UIControlStateNormal)];
    [btn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
    btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 4.0f;
    btn.layer.borderWidth = 1.0f;
    btn.layer.borderColor = APPTintColor.CGColor;
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:btn];
    return btn;
    
}
#pragma mark - Touch Even
- (void)btnAction:(UIButton *)sender {
    if (self.clickBottomBtnBlock) {
        self.clickBottomBtnBlock(_indexPath,sender);
    }
}

@end
