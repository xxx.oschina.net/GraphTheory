//
//  YXOrderRemarkFooterReusableView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderRemarkFooterReusableView : UICollectionReusableView

@property (nonatomic ,copy) void(^textViewChange)(NSString *text);

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UITextView *textView;
@property (nonatomic ,strong) UILabel *priceLab;
@property (nonatomic ,strong) UILabel *subtotalLab;
@property (nonatomic ,strong) UILabel *numLab;

@end

NS_ASSUME_NONNULL_END
