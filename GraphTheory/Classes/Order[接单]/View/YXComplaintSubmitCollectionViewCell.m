//
//  YXComplaintSubmitCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXComplaintSubmitCollectionViewCell.h"

@implementation YXComplaintSubmitCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.btn.layer.masksToBounds = YES;
    self.btn.layer.cornerRadius = 4.0;
}
- (IBAction)complanBtnAction:(UIButton *)sender {
    if (self.clickBtnBlock) {
        self.clickBtnBlock();
    }
}

@end
