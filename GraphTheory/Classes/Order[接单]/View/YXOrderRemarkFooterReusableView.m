//
//  YXOrderRemarkFooterReusableView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderRemarkFooterReusableView.h"
#import "UITextView+Placeholder.h"
@interface YXOrderRemarkFooterReusableView ()<UITextViewDelegate>

@end

@implementation YXOrderRemarkFooterReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}

- (void)addLayout {
    
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.textView];
    [self.backView addSubview:self.priceLab];
    [self.backView addSubview:self.subtotalLab];
    [self.backView addSubview:self.numLab];
    
    YXWeakSelf
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.top.bottom.equalTo(self);
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.backView.mas_left).offset(10);
        make.top.equalTo(weakSelf.backView.mas_top).offset(20);
    }];
    
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLab.mas_bottom);
        make.left.equalTo(weakSelf.backView.mas_left).offset(5);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-10);
        make.height.equalTo(@40);
    }];
    
    [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        make.bottom.equalTo(weakSelf.backView.mas_bottom).offset(-15);
        [weakSelf.priceLab sizeToFit];
    }];
    [_subtotalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.priceLab.mas_centerY);
        make.right.equalTo(weakSelf.priceLab.mas_left).offset(-5);
    }];
    [_numLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.subtotalLab.mas_centerY);
        make.right.equalTo(weakSelf.subtotalLab.mas_left).offset(-5);
    }];
    
   
}

#pragma mark - Lazy loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
        _titleLab.text = @"订单备注";
    }
    return _titleLab;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [UITextView new];
        _textView.textColor = color_TextOne;
        _textView.placeholder = @"选填，利于商家更好的了解您的需求";
        _textView.font = [UIFont systemFontOfSize:12.0f];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.delegate = self;
    }
    return _textView;
}

- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] init];
        _priceLab.font = [UIFont systemFontOfSize:10.0f];
        _priceLab.textColor = APPTintColor;
        _priceLab.text = @"¥0.10";
    }
    return _priceLab;
}
- (UILabel *)subtotalLab {
    if (!_subtotalLab) {
        _subtotalLab = [[UILabel alloc] init];
        _subtotalLab.font = [UIFont systemFontOfSize:10.0f];
        _subtotalLab.textColor = color_TextOne;
        _subtotalLab.text = @"小计：";
    }
    return _subtotalLab;
}
- (UILabel *)numLab {
    if (!_numLab) {
        _numLab = [[UILabel alloc] init];
        _numLab.font = [UIFont systemFontOfSize:10.0f];
        _numLab.textColor = color_TextOne;
        _numLab.text = @"共1项";
    }
    return _numLab;
}
#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewChange) {
        self.textViewChange(textView.text);
    }
}


@end
