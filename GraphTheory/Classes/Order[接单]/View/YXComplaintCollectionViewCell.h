//
//  YXComplaintCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXComplaintCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (nonatomic ,strong) YXOrderInfoModel *model;
@end

NS_ASSUME_NONNULL_END
