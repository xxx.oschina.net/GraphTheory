//
//  YXOrderHeadCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderHeadCollectionViewCell.h"
#import "YXUserOrderModel.h"
@interface YXOrderHeadCollectionViewCell ()

@property (strong, nonatomic)  UILabel *nameLab;
@property (strong, nonatomic)  UILabel *groupLab;
@property (strong, nonatomic)  UIButton *deleteBtn;
@property (strong, nonatomic)  UIView *lineView;
@end

@implementation YXOrderHeadCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
    [self addLayout];

}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
}

- (void)setModel:(YXUserOrderModelList *)model {
    _model = model;
    _groupLab.text = _model.product_details.name;
    _nameLab.text = [NSString stringWithFormat:@"单号：%@",_model.order_no];
    if (self.type == 1) {
        _deleteBtn.hidden = YES;
    }
    
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)addLayout{
    
    [self.contentView addSubview:self.nameLab];
    [self.contentView addSubview:self.deleteBtn];
    [self.contentView addSubview:self.groupLab];
    [self.contentView addSubview:self.lineView];
    if (self.type == 2) {
        [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.left.equalTo(self.mas_left).offset(15);
            [self.nameLab sizeToFit];
        }];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nameLab.mas_centerY);
            make.right.equalTo(self.mas_right).offset(-15);
            make.size.mas_equalTo(CGSizeMake(24, 24));
        }];
        
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(15);
            make.right.equalTo(self.mas_right).offset(-15);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@0.5);
        }];

    }else {
        [self.groupLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.left.equalTo(self.mas_left).offset(15);
            make.size.mas_equalTo(CGSizeMake(50, 20));
        }];
        [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.left.equalTo(self.groupLab.mas_right).offset(15);
            [self.nameLab sizeToFit];
        }];
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nameLab.mas_centerY);
            make.right.equalTo(self.mas_right).offset(-15);
            make.size.mas_equalTo(CGSizeMake(24, 24));
        }];
        
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(15);
            make.right.equalTo(self.mas_right).offset(-15);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@0.5);
        }];

    }
 

}

#pragma mark - Lazy Loading
- (UILabel *)groupLab{
    if (!_groupLab) {
        _groupLab = [UILabel new];
        _groupLab.backgroundColor = APPTintColor;
        _groupLab.textColor = [UIColor whiteColor];
        _groupLab.font = [UIFont systemFontOfSize:12.0f];
        _groupLab.layer.masksToBounds = YES;
        _groupLab.layer.cornerRadius = 4.0f;
        _groupLab.textAlignment = NSTextAlignmentCenter;
        _groupLab.text = @"教育";
    }
    return _groupLab;
}

- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.textColor = color_TextOne;
        _nameLab.font = [UIFont systemFontOfSize:14];
        _nameLab.text = @"单号：299990293402";
    }
    return _nameLab;
}

- (UIButton *)deleteBtn{
    if (!_deleteBtn) {
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteBtn setImage:[UIImage imageNamed:@"del"] forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(deleteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _deleteBtn;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}



#pragma mark - Touch Even
- (void)deleteBtnAction:(UIButton *)sender {
    if (self.clickDeleteBtn) {
        self.clickDeleteBtn(self.indexPath);
    }
}


@end
