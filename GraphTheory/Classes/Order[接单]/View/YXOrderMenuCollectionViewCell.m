//
//  YXOrderMenuCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderMenuCollectionViewCell.h"
#import "DDPayMentModel.h"
@implementation YXOrderMenuCollectionViewCell

- (void)setModel:(DDPayMentModel *)model {
    _model = model;
    _nameLab.text = _model.text;
    if (_model.selected) {
        _nameLab.layer.borderColor = APPTintColor.CGColor;
        _nameLab.layer.masksToBounds = YES;
        _nameLab.layer.cornerRadius = 4.0;
    }else {
        _nameLab.layer.borderColor = color_LineColor.CGColor;
        _nameLab.layer.masksToBounds = YES;
        _nameLab.layer.cornerRadius = 4.0;
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}

- (void)addLayout{
    [self.contentView addSubview:self.nameLab];
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];

}

#pragma mark - Lazy Loading
- (UILabel *)nameLab{
    if (!_nameLab) {
        _nameLab = [UILabel new];
        _nameLab.textColor = color_TextOne;
        _nameLab.font = [UIFont systemFontOfSize:14];
        _nameLab.text = @"支付宝";
        _nameLab.layer.masksToBounds = YES;
        _nameLab.layer.cornerRadius = 4.0;
        _nameLab.layer.borderColor = color_LineColor.CGColor;
        _nameLab.layer.borderWidth = 1.0f;
        _nameLab.textAlignment = NSTextAlignmentCenter;
    }
    return _nameLab;
}



@end
