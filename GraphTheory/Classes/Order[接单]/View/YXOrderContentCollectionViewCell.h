//
//  YXOrderContentCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXUserOrderModelList;
@interface YXOrderContentCollectionViewCell : UICollectionViewCell

// type 0:消费订单 1：接单 2:采购订单
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXUserOrderModelList *model;

@end

NS_ASSUME_NONNULL_END
