//
//  YXOrderGoodCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderGoodCollectionViewCell.h"
#import "YXCateModel.h"
#import "YXVideoModel.h"
@interface YXOrderGoodCollectionViewCell ()

@property (strong, nonatomic)  UILabel *goodName;
@property (strong, nonatomic)  UILabel *goodPrice;
@property (strong, nonatomic)  UILabel *goodCount;
@property (strong, nonatomic)  UILabel *goodSku;
@property (strong, nonatomic)  UILabel *goodTotal;
@property (strong, nonatomic)  UILabel *msgLab;
@property (strong, nonatomic)  UILabel *statusLab;


@property (strong, nonatomic)  UIImageView *goodImage;
@property (strong, nonatomic)  UIView *goodView;
@property (strong, nonatomic)  UIButton *changePrice;

@property (strong, nonatomic)  UIView *lineView;
@property (strong, nonatomic)  UIView *couponsView;
@property (strong, nonatomic)  UILabel *couponsLab;
@property (strong, nonatomic)  UIImageView *arrow;
@property (strong, nonatomic)  UILabel *dayLab;
@property (strong, nonatomic)  UIView *bottomLineView;


@end
@implementation YXOrderGoodCollectionViewCell


- (void)setType:(NSInteger)type {
    _type = type;
    if (_type == 0) {
        [self addLayoutDetail];
    }else {
        [self addLayout];
    }
}

- (void)setModel:(YXCateDetailModel *)model {
    _model = model;
    
    [_goodImage sd_setImageWithURL:[NSURL URLWithString:_model.img]];
    _goodName.text = _model.name;
    _goodPrice.text = [NSString stringWithFormat:@"¥%@",_model.c_price];
    _goodSku.text = [NSString stringWithFormat:@"%@%@%@",_model.c_remarka,_model.c_remarkb,_model.c_remarkc];
    _dayLab.text = @"劵码有效期：60天";
    if ([_model.c_model isEqualToString:@"1"]) {
        _dayLab.hidden = NO;
    }else {
        _dayLab.hidden = YES;
    }
    if ([_model.c_coupons_price floatValue] == 0) {
        _msgLab.text = @"请选择优惠劵";
    }else {
        _msgLab.text = [NSString stringWithFormat:@"%@",_model.price_title];
    }
}

- (void)setGoodsModel:(YXGoodsInfoModel *)goodsModel {
    _goodsModel = goodsModel;
    
    if (_goodsModel.banner.count) {
        [_goodImage sd_setImageWithURL:[NSURL URLWithString:_goodsModel.banner[0]]];
    }
    _goodName.text = _goodsModel.title;
    _goodPrice.text = [NSString stringWithFormat:@"¥%@",_goodsModel.sales_price];
    _goodPrice.textColor = [UIColor blackColor];
    
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)addLayoutDetail{
    
    [self.contentView addSubview:self.goodView];
    [self.goodView addSubview:self.goodImage];
    [self.goodView addSubview:self.goodName];
    [self.goodView addSubview:self.goodPrice];
//    [self.goodView addSubview:self.goodCount];
//    [self.goodView addSubview:self.goodSku];
//    [self.goodView addSubview:self.msgLab];
//    [self.goodView addSubview:self.changePrice];


    _goodPrice.font = [UIFont boldSystemFontOfSize:16];
    _goodPrice.textColor = color_RedColor;
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.equalTo(@100);
    }];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.left.equalTo(self.goodView).offset(10);
        make.top.equalTo(self.goodView.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];

    [self.goodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
    }];
    [self.goodPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
    }];
    
//    [self.goodSku mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.goodName.mas_bottom);
//        make.bottom.equalTo(self.goodPrice.mas_top);
//        make.left.equalTo(self.goodName.mas_left);
//        make.right.equalTo(self.goodView.mas_right).offset(-10);
//
//    }];
//    [self.msgLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.goodSku.mas_bottom).offset(3);
//        make.left.equalTo(self.goodName.mas_left);
//    }];
//    [self.goodCount mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.goodSku.mas_centerY);
//        make.right.equalTo(self.goodView.mas_right).offset(-10);
//    }];
//    [self.changePrice mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.goodPrice.mas_centerY);
//        make.right.equalTo(self.goodView.mas_right).offset(-15);
//    }];

    [_goodPrice setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];//不可以被压缩，尽量显示完整
    [_goodName setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];//宽度不够时，可以被压缩
}

- (void)addLayout{


    [self.contentView addSubview:self.goodView];
    [self.goodView addSubview:self.goodImage];
    [self.goodView addSubview:self.goodName];
    [self.goodView addSubview:self.goodPrice];
    [self.goodView addSubview:self.goodCount];
    [self.goodView addSubview:self.goodSku];
    [self.goodView addSubview:self.dayLab];
    [self.goodView addSubview:self.lineView];
    [self.contentView addSubview:self.couponsView];
    [self.couponsView addSubview:self.couponsLab];
    [self.contentView addSubview:self.arrow];
    [self.couponsView addSubview:self.msgLab];
    [self.couponsView addSubview:self.bottomLineView];
    [self.couponsView addSubview:self.changePrice];

    _goodPrice.font = [UIFont systemFontOfSize:14];
    _goodPrice.textColor = color_TextOne;
    
    [self.goodView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.height.equalTo(@100);
    }];
    [self.goodImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.left.equalTo(self.goodView).offset(10);
        make.top.equalTo(self.goodView.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];

    [self.goodName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
    }];
    [self.goodPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodImage.mas_top);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
        make.left.equalTo(self.goodName.mas_right).offset(10);
    }];
    
    [self.goodCount mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.goodView.mas_centerY);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
        [self.goodCount sizeToFit];
    }];
    [self.goodSku mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodName.mas_bottom).offset(10);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
        make.right.equalTo(self.goodCount.mas_left).offset(-10);
    }];
 
    [self.dayLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodImage.mas_bottom);
        make.left.equalTo(self.goodImage.mas_right).offset(10);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.goodView.mas_bottom);
        make.left.equalTo(self.goodView.mas_left).offset(10);
        make.right.equalTo(self.goodView.mas_right).offset(-10);
        make.height.equalTo(@1);
    }];
    
    [self.couponsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.goodView.mas_bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(@50);
    }];
    
    [self.couponsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponsView.mas_centerY);
        make.left.equalTo(self.couponsView.mas_left).offset(10);
    }];
    
    [self.arrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponsView.mas_centerY);
        make.right.equalTo(self.couponsView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(7, 12));
    }];
    
    [self.msgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.couponsView.mas_centerY);
        make.right.equalTo(self.arrow.mas_left).offset(-20);
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left).offset(10);
        make.right.equalTo(self.contentView.mas_right).offset(-10);
        make.height.equalTo(@1);
    }];
    
    [self.changePrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.couponsView);
    }];
    
    [_goodPrice setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];//不可以被压缩，尽量显示完整
    [_goodName setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];//宽度不够时，可以被压缩
}
- (UIImageView *)goodImage{
    if (!_goodImage) {
        _goodImage = [UIImageView new];
        _goodImage.layer.cornerRadius = 5;
    }
    return _goodImage;
}
- (UILabel *)goodName{
    if (!_goodName) {
        _goodName = [UILabel new];
        _goodName.textColor = color_TextOne;
        _goodName.font = [UIFont systemFontOfSize:14];
        _goodName.numberOfLines = 2;
    }
    return _goodName;
}
- (UILabel *)goodTotal{
    if (!_goodTotal) {
        _goodTotal = [UILabel new];
        _goodTotal.textColor = color_TextOne;
        _goodTotal.font = [UIFont systemFontOfSize:14];
    }
    return _goodTotal;
}
- (UILabel *)goodPrice{
    if (!_goodPrice) {
        _goodPrice = [UILabel new];
        _goodPrice.textColor = color_TextOne;
        _goodPrice.font = [UIFont systemFontOfSize:14];
        _goodPrice.textAlignment = NSTextAlignmentRight;
    }
    return _goodPrice;
}
- (UILabel *)goodSku{
    if (!_goodSku) {
        _goodSku = [UILabel new];
        _goodSku.textColor = color_TextTwo;
        _goodSku.font = [UIFont systemFontOfSize:10];
        _goodSku.numberOfLines = 2;
    }
    return _goodSku;
}
- (UILabel *)msgLab{
    if (!_msgLab) {
        _msgLab = [UILabel new];
        _msgLab.textColor = color_TextTwo;
        _msgLab.font = [UIFont systemFontOfSize:12];
        _msgLab.text = @"请选择优惠劵";
    }
    return _msgLab;
}
- (UILabel *)statusLab{
    if (!_statusLab) {
        _statusLab = [UILabel new];
        _statusLab.textColor = [UIColor whiteColor];
        _statusLab.backgroundColor = [UIColor colorWithHexString:@"333333" alpha:0.5];
        _statusLab.font = [UIFont systemFontOfSize:12];
        _statusLab.textAlignment = NSTextAlignmentCenter;
    }
    return _statusLab;
}
- (UILabel *)goodCount{
    if (!_goodCount) {
        _goodCount = [UILabel new];
        _goodCount.textColor = color_TextTwo;
        _goodCount.font = [UIFont systemFontOfSize:10];
        _goodCount.textAlignment = NSTextAlignmentRight;
        _goodCount.text = @"x1";
    }
    return _goodCount;
}

- (UILabel *)dayLab{
    if (!_dayLab) {
        _dayLab = [UILabel new];
        _dayLab.textColor = color_TextOne;
        _dayLab.font = [UIFont systemFontOfSize:12];
    }
    return _dayLab;
}

- (UIButton *)changePrice{
    if (!_changePrice) {
        _changePrice = [UIButton buttonWithType:UIButtonTypeCustom];
        _changePrice.backgroundColor = [UIColor clearColor];
        [_changePrice addTarget:self action:@selector(changePriceBtn) forControlEvents:UIControlEventTouchUpInside];
    }
    return _changePrice;
}

- (UIView *)goodView{
    if (!_goodView) {
        _goodView = [UIView new];
    }
    return _goodView;
}
- (UIView *)couponsView{
    if (!_couponsView) {
        _couponsView = [UIView new];
    }
    return _couponsView;
}
- (UILabel *)couponsLab{
    if (!_couponsLab) {
        _couponsLab = [UILabel new];
        _couponsLab.textColor = color_TextOne;
        _couponsLab.font = [UIFont systemFontOfSize:14];
        _couponsLab.text = @"优惠券";
    }
    return _couponsLab;
}
- (UIImageView *)arrow {
    if (!_arrow) {
        _arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"right_arrow_black"]];
    }
    return _arrow;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}
- (UIView *)bottomLineView{
    if (!_bottomLineView) {
        _bottomLineView = [UIView new];
        _bottomLineView.backgroundColor = color_LineColor;
    }
    return _bottomLineView;
}

- (void)changePriceBtn {
    if (self.clickCouponsBlock) {
        self.clickCouponsBlock();
    }
}

@end
