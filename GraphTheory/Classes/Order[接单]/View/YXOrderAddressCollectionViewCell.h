//
//  YXOrderAddressCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXAddressModel;
@interface YXOrderAddressCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (nonatomic ,strong) YXAddressModel *model;
@end

NS_ASSUME_NONNULL_END
