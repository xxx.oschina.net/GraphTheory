//
//  YXSubmitOrderModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/9.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXSubmitOrderModel : DDBaseModel
// 订单价格
@property (copy,nonatomic)   NSString *price;
// 订单备注
@property (nonatomic ,copy) NSString *remark;

@end

NS_ASSUME_NONNULL_END
