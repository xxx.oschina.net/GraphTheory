//  Created by li qiao robot 
#import "YXOrderInfoModel.h" 
@implementation YXOrderInfoModelExpressInfo
@end

@implementation YXOrderInfoModelTakeUserInfo
@end

@implementation YXOrderInfoModelProduct_detailsAddress
@end

@implementation YXOrderInfoModelProduct_details
@end

@implementation YXOrderInfoModelCoupons
@end

@implementation YXOrderInfoModelFilterTakeUsersUserInfoAddress
@end

@implementation YXOrderInfoModelFilterTakeUsersUserInfo
+(NSDictionary*)mj_objectClassInArray{  
       return @{ @"images" : [NSString class] }; 
}
@end

@implementation YXOrderInfoModelFilterTakeUsers
@end

@implementation YXOrderInfoModelFrom_address
@end

@implementation YXOrderInfoModelReceiver
@end

@implementation YXOrderInfoModelUserInfo
@end

@implementation YXOrderInfoModel
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"filterTakeUsers" : [YXOrderInfoModelFilterTakeUsers class] ,@"answer" : [NSString class],@"imgs" : [NSString class],@"coupons" : [YXOrderInfoModelCoupons class]}; 
}
@end

