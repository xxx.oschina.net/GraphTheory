//  Created by li qiao robot 
#import "DDBaseModel.h"
@interface YXOrderInfoModelExpressInfo : DDBaseModel
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *express_code;
@property (copy,nonatomic)   NSString *express_name;
@property (copy,nonatomic)   NSString *express_no;
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *sub_order_id;
@property (copy,nonatomic)   NSString *user_id;

@end

@interface YXOrderInfoModelTakeUserInfo : DDBaseModel
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *show_id;
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *email;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;
@end

@interface YXOrderInfoModelProduct_detailsAddress : DDBaseModel
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXOrderInfoModelProduct_details : DDBaseModel
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *cate_id;
@property (copy,nonatomic)   NSString *imgs;
@property (copy,nonatomic)   NSString *content;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *belongMode;
@property (copy,nonatomic)   NSString *c_address;
@property (strong,nonatomic) NSNumber *coupon_id;
@property (copy,nonatomic)   NSString *c_pass;
@property (strong,nonatomic) YXOrderInfoModelProduct_detailsAddress *address;
@property (strong,nonatomic) NSNumber *number;
@property (copy,nonatomic)   NSString *rebate;
@property (copy,nonatomic)   NSString *remark;
@property (copy,nonatomic)   NSString *c_studyprice;
@property (strong,nonatomic) NSNumber *coupon_price;
@property (copy,nonatomic)   NSString *service_city_id;
@property (copy,nonatomic)   NSString *c_model;
@end

@interface YXOrderInfoModelCoupons : DDBaseModel
@end

@interface YXOrderInfoModelFilterTakeUsersUserInfoAddress : DDBaseModel
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXOrderInfoModelFilterTakeUsersUserInfo : DDBaseModel
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *apply_time;
@property (copy,nonatomic)   NSString *check_m_id;
@property (copy,nonatomic)   NSString *id_card;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *check_status;
@property (copy,nonatomic)   NSString *check_time;
@property (strong,nonatomic) YXOrderInfoModelFilterTakeUsersUserInfoAddress *address;
@property (copy,nonatomic)   NSString *userphone;
@property (copy,nonatomic)   NSString *service_ids;
@property (copy,nonatomic)   NSString *username;
@property (strong,nonatomic) NSArray *images;
@property (copy,nonatomic)   NSString *service_city_ids;
@property (copy,nonatomic)   NSString *apply_role;
@end

@interface YXOrderInfoModelFilterTakeUsers : DDBaseModel
@property (copy,nonatomic)   NSString *us_account;
@property (copy,nonatomic)   NSString *show_id;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *email;
@property (copy,nonatomic)   NSString *user_id;
@property (strong,nonatomic) YXOrderInfoModelFilterTakeUsersUserInfo *userInfo;
@property (copy,nonatomic)   NSString *nike;
@end

@interface YXOrderInfoModelFrom_address : DDBaseModel
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXOrderInfoModelReceiver : DDBaseModel
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *province;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *city;
@property (copy,nonatomic)   NSString *region;
@property (copy,nonatomic)   NSString *username;
@property (copy,nonatomic)   NSString *is_default;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *userphone;
@end

@interface YXOrderInfoModelUserInfo : DDBaseModel
@property (copy,nonatomic)   NSString *email;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXOrderInfoModel : DDBaseModel
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *take_price;
@property (copy,nonatomic)   NSString *product_icon;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *user_id;
@property (strong,nonatomic) YXOrderInfoModelUserInfo *userInfo;
@property (copy,nonatomic)   NSString *complain;
@property (copy,nonatomic)   NSString *is_share;
@property (copy,nonatomic)   NSString *share_check;
@property (copy,nonatomic)   NSString *m_updatetime;
@property (copy,nonatomic)   NSString *delete_time;
@property (copy,nonatomic)   NSString *share_m_id;
@property (strong,nonatomic) YXOrderInfoModelReceiver *receiver;
@property (copy,nonatomic)   NSString *answer_desc;
@property (strong,nonatomic) YXOrderInfoModelFrom_address *from_address;
@property (copy,nonatomic)   NSString *pay_type;
@property (strong,nonatomic) NSNumber *isExpressed;
@property (copy,nonatomic)   NSString *status;
@property (copy,nonatomic)   NSString *yiyimoney;
@property (copy,nonatomic)   NSString *complain_time;
@property (strong,nonatomic) NSArray *filterTakeUsers;
@property (copy,nonatomic)   NSString *pay_price;
@property (copy,nonatomic)   NSString *solve;
@property (copy,nonatomic)   NSString *coupon_price;
@property (strong,nonatomic) NSArray *answer;
@property (strong,nonatomic) NSArray *imgs;
@property (strong,nonatomic) NSArray *coupons;
@property (copy,nonatomic)   NSString *complete_time;
@property (copy,nonatomic)   NSString *is_complain;
@property (copy,nonatomic)   NSString *c_model;
@property (copy,nonatomic)   NSString *c_pass;
@property (copy,nonatomic)   NSString *order_no;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *c_address;
@property (copy,nonatomic)   NSString *coupon_code;
@property (copy,nonatomic)   NSString *statusTitle;
@property (copy,nonatomic)   NSString *belongmode;
@property (copy,nonatomic)   NSString *pay_time;
@property (copy,nonatomic)   NSString *is_delete;
@property (strong,nonatomic) YXOrderInfoModelProduct_details *product_details;
@property (copy,nonatomic)   NSString *remark;
@property (copy,nonatomic)   NSString *take_time;
@property (copy,nonatomic)   NSString *share_shenhe_time;
@property (copy,nonatomic)   NSString *coupon_id;
@property (copy,nonatomic)   NSString *solve_time;
@property (copy,nonatomic)   NSString *m_id;
@property (copy,nonatomic)   NSString *content;
@property (strong,nonatomic) YXOrderInfoModelTakeUserInfo *takeUserInfo;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *order_type;
@property (copy,nonatomic)   NSString *share_time;
@property (copy,nonatomic)   NSString *order_id;
@property (copy,nonatomic)   NSString *take_user_id;
@property (copy,nonatomic)   NSString *finish_time;
@property (copy,nonatomic)   NSString *is_overdue;
@property (strong,nonatomic) YXOrderInfoModelExpressInfo *expressInfo;
@property (copy,nonatomic)   NSString *service_city_id;
@end

