//
//  YXOrderViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderViewModel.h"
#import "NSString+YXCommon.h"
#import "YXCateModel.h"
@implementation YXOrderViewModel

#pragma mark - 知识内容/生活服务 下单
+ (void)requestGeneOrderWithAddress_id:(NSString *)address_id
                                remark:(NSString *)remark
                              services:(NSArray *)services
                        Completion:(void(^)(id responesObj))completion
                               Failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/geneOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:address_id forKey:@"address_id"];
    if (services.count) {
        for (int i = 0; i < services.count ;i++) {
            YXCateDetailModel *model = [services objectAtIndex:i];
            [params setValue:model.Id forKey:[NSString stringWithFormat:@"services[%d][cate_id]",i]];
            [params setValue:model.imgs forKey:[NSString stringWithFormat:@"services[%d][imgs]",i]];
            [params setValue:model.content forKey:[NSString stringWithFormat:@"services[%d][content]",i]];
            [params setValue:model.lat forKey:[NSString stringWithFormat:@"services[%d][address][lat]",i]];
            [params setValue:model.lng forKey:[NSString stringWithFormat:@"services[%d][address][lng]",i]];
            [params setValue:model.address forKey:[NSString stringWithFormat:@"services[%d][address][address]",i]];
            [params setValue:model.coupon_id forKey:[NSString stringWithFormat:@"services[%d][coupon_id]",i]];
            [params setValue:remark forKey:[NSString stringWithFormat:@"services[%d][remark]",i]];
        }
    }
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 分享订单下单
+ (void)requestGeneOrderByShareWithSub_order_id:(NSString *)sub_order_id
                                        type:(NSString *)type
                                     Completion:(void(^)(id responesObj))completion
                                        Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/geneOrderByShare",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:sub_order_id forKey:@"sub_order_id"];
    [params setValue:type forKey:@"type"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  视频关联商品下单
+ (void)requestGeneOrderByVideoWithAddress_id:(NSString *)address_id
                                     goods_id:(NSString *)goods_id
                                       number:(NSString *)number
                                   Completion:(void(^)(id responesObj))completion
                                      Failure:(void(^)(NSError *error))conError {
    

    NSString *urlStr = [NSString stringWithFormat:@"%@order/geneOrderByVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:address_id forKey:@"address_id"];
    [params setValue:goods_id forKey:@"goods[0][goods_id]"];
    [params setValue:number forKey:@"goods[0][number]"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  创业者接单列表
+ (void)queryTakeOrderListWithPage:(NSString *)page
                            status:(NSString *)status
                       is_complain:(NSString *)is_complain
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/takeOrderList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    if ([status isEqualToString:@"完成"]) {
    }else if ([status isEqualToString:@"进行中"]) {
        [params setValue:@"10" forKey:@"status"];
    }else if ([status isEqualToString:@"待确认"]) {
        [params setValue:@"15" forKey:@"status"];
        [params setValue:@"0" forKey:@"is_complain"];
    }else if ([status isEqualToString:@"有异议"]) {
        [params setValue:@"15" forKey:@"status"];
        [params setValue:@"1" forKey:@"is_complain"];
    }else if ([status isEqualToString:@"已完成"]) {
        [params setValue:@"20" forKey:@"status"];
    }
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  创业者接单详情
+ (void)queryTakeOrderInfoWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/takeOrderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  抢单操作
+ (void)queryGrabSubOrderWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                        Failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/grabSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  券码查单
+ (void)getSubOrderInfoByWithCoupon_code:(NSString *)coupon_code
                              Completion:(void(^)(id responesObj))completion
                                 Failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/getSubOrderInfoByCouponCode",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:coupon_code forKey:@"coupon_code"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  券码接单操作
+ (void)takeSubOrderByWithCoupon_code:(NSString *)coupon_code
                           Completion:(void(^)(id responesObj))completion
                              Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/takeSubOrderByCouponCode",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:coupon_code forKey:@"coupon_code"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  完成任务操作
+ (void)requestFinishSubOrderWithId:(NSString *)Id
                             answer:(NSString *)answer
                        answer_desc:(NSString *)answer_desc
                         Completion:(void(^)(id responesObj))completion
                            Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/finishSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:answer forKey:@"answer"];
    [params setValue:answer_desc forKey:@"answer_desc"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  异议订单解决方案
+ (void)requestFinishSubOrderWithId:(NSString *)Id
                            content:(NSString *)content
                         Completion:(void(^)(id responesObj))completion
                            Failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/solveComplainSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:content forKey:@"content"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  APP支付
+ (void)requestPayWithOrder_type:(NSString *)order_type
                        pay_type:(NSString *)pay_type
                        order_id:(NSString *)order_id
                         pay_pwd:(NSString *)pay_pwd
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@pay/index",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:order_type?order_type:@"big" forKey:@"order_type"];
    [params setValue:pay_type forKey:@"pay_type"];
    [params setValue:order_id forKey:@"order_id"];
    [params setValue:pay_pwd forKey:@"pay_pwd"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



@end
