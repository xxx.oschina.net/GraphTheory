//
//  YXOrderViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderViewModel : DDBaseViewModel




/**
 知识内容/生活服务 下单

 @param address_id       地址id
 @param remark       备注
 @param services       分类数组
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestGeneOrderWithAddress_id:(NSString *)address_id
                                remark:(NSString *)remark
                              services:(NSArray *)services
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;


/**
 分享订单下单

 @param sub_order_id       订单id
 @param type       study 学习 list 生活
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestGeneOrderByShareWithSub_order_id:(NSString *)sub_order_id
                                        type:(NSString *)type
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;


/**
 视频关联商品下单

 @param address_id       地址id
 @param goods_id           商品id
 @param number           商品数量
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestGeneOrderByVideoWithAddress_id:(NSString *)address_id
                                     goods_id:(NSString *)goods_id
                                       number:(NSString *)number
                                   Completion:(void(^)(id responesObj))completion
                                      Failure:(void(^)(NSError *error))conError;




/**
 创业者接单列表

 @param page       当前页码
 @param status           状态
 @param is_complain    1
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryTakeOrderListWithPage:(NSString *)page
                            status:(NSString *)status
                       is_complain:(NSString *)is_complain
                        Completion:(void(^)(id responesObj))completion
                           Failure:(void(^)(NSError *error))conError;

/**
 创业者接单详情

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryTakeOrderInfoWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;

/**
 抢单操作

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryGrabSubOrderWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;

/**
 券码查单

 @param coupon_code      劵码
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)getSubOrderInfoByWithCoupon_code:(NSString *)coupon_code
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;

/**
 券码接单操作

 @param coupon_code      劵码
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)takeSubOrderByWithCoupon_code:(NSString *)coupon_code
                           Completion:(void(^)(id responesObj))completion
                              Failure:(void(^)(NSError *error))conError;

/**
 完成任务操作

 @param Id      订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestFinishSubOrderWithId:(NSString *)Id
                             answer:(NSString *)answer
                        answer_desc:(NSString *)answer_desc
                         Completion:(void(^)(id responesObj))completion
                            Failure:(void(^)(NSError *error))conError;

/**
 异议订单解决方案

 @param Id      订单id
 @param content      内容
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestFinishSubOrderWithId:(NSString *)Id
                            content:(NSString *)content
                         Completion:(void(^)(id responesObj))completion
                            Failure:(void(^)(NSError *error))conError;


/**
 APP支付

 @param order_type    订单类型
 @param pay_type           支付类型
 @param order_id   订单id
 @param pay_pwd   支付密码
 @param conError    返回失败
 */
+ (void)requestPayWithOrder_type:(NSString *)order_type
                        pay_type:(NSString *)pay_type
                        order_id:(NSString *)order_id
                         pay_pwd:(NSString *)pay_pwd
                      Completion:(void(^)(id responesObj))completion
                         Failure:(void(^)(NSError *error))conError;

@end

NS_ASSUME_NONNULL_END
