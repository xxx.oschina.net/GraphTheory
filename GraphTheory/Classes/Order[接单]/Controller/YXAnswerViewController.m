//
//  YXAnswerViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAnswerViewController.h"

#import "YXOrderUserInfoCollectionViewCell.h"
#import "YXOrderDetailsCollectionViewCell.h"
#import "YXAnswerCollectionViewCell.h"
#import "YXPhotoCollectionViewCell.h"
#import "WGPublicBottomView.h"
#import "YXOrderViewModel.h"
#import "DDPublicCell.h"
#import "YXOrderInfoModel.h"
#import "UITextView+Placeholder.h"
#import "YXAddressPopView.h"

@interface YXAnswerViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (strong, nonatomic) YXOrderInfoModel *model;
@property (strong, nonatomic) NSMutableArray *selectImgArr;
@property (strong, nonatomic) NSString *answer;
@property (strong, nonatomic) NSString *answer_desc;
@end

@implementation YXAnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    self.selectImgArr = [NSMutableArray array];
    [self loadData];
    [self addSubViews];

}
- (void)loadData {
    
    DDWeakSelf
    [YXOrderViewModel queryTakeOrderInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.model = [YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        }else {
            [YJProgressHUD showMessage:responesObj[@"data"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        
    }];
    
}

// 上传图片
- (void)uploadImg {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    dispatch_group_t group = dispatch_group_create();
    __block NSMutableArray *images = [NSMutableArray array];
    for (UIImage *image in self.selectImgArr) {
        dispatch_group_enter(group);
        [YXUserViewModel requestUploadImg:image Completion:^(id responesObj) {
            if (REQUESTDATASUCCESS) {
                NSArray *dataArr = responesObj[@"data"];
                if (dataArr.count) {
                    [images addObject:dataArr[0]];
                }
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            dispatch_group_leave(group);
        } failure:^(NSError *error) {
            dispatch_group_leave(group);
        }];
    }

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
        if (images.count) {
            weakSelf.answer = [images componentsJoinedByString:@","];
            [weakSelf save];
        }
    });

}


// 提交答案
- (void)save {
    
    
    if ([NSString isBlankString:self.answer_desc]) {
        return [YJProgressHUD showMessage:@"请输入描述"];
    }
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel requestFinishSubOrderWithId:self.Id answer:self.answer answer_desc:self.answer_desc Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:@"提交成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
}


- (void)addSubViews {
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 4;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH-30, 150);
    }else if (indexPath.section == 1) {
        return CGSizeMake(SCREEN_WIDTH-30, 340);
    }else if (indexPath.section == 2) {
        return CGSizeMake(SCREEN_WIDTH-30, 80);
    }else {
        return CGSizeMake(SCREEN_WIDTH-30, 100);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(20, 15, 20, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf
    if (indexPath.section == 0) {
        YXOrderUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderUserInfoCollectionViewCell" forIndexPath:indexPath];
        cell.indexPath = indexPath;
        cell.type = 1;
        cell.model = self.model;
        [cell setClickAddressBlock:^{
            YXAddressPopView *popView = [[YXAddressPopView alloc] initWithFrame:[UIScreen mainScreen].bounds model:weakSelf.model];
            [popView showAnimation];
        }];
        return cell;
    }else if (indexPath.section == 1 ){
        YXOrderDetailsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderDetailsCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 2) {
        DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
        cell.type = CellViewTypeStateTextView;
        cell.textView.placeholder = @"请简单描述...";
        [cell setTextViewChange:^(NSString * _Nonnull text) {
            weakSelf.answer_desc = text;
        }];
        return cell;
    }else {
        YXPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXPhotoCollectionViewCell" forIndexPath:indexPath];
        [cell.imgPicker observeSelectedMediaArray:^(NSArray<LLImagePickerModel *> *list){
            [weakSelf.selectImgArr removeAllObjects];
             for (LLImagePickerModel *model in list){
                 [weakSelf.selectImgArr addObject:model.image];
             }
         }];
        return cell;
    }
    return [UICollectionViewCell new];
}


#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
//        flowLayout.isCalculateHeader = YES;
//        flowLayout.isCalculateFooter = YES;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[YXOrderUserInfoCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderUserInfoCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXPhotoCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXPhotoCollectionViewCell"];
        [_collectionView registerClass:[DDPublicCollectionCell class] forCellWithReuseIdentifier:@"DDPublicCollectionCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXOrderDetailsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXOrderDetailsCollectionViewCell"];

    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"提交答案"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.selectImgArr.count > 0) {
                [weakSelf uploadImg];
            }else {
                [weakSelf save];
            }
        }];
    }
    return _bottomView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
