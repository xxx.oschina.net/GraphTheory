//
//  YXOrderPayViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderPayViewController.h"
#import "WGPublicBottomView.h"
#import <JJCollectionViewRoundFlowLayout.h>
#import "DDPublicCell.h"
#import "YXOrderMenuCollectionViewCell.h"
#import "LMPopInputPasswordView.h"
#import "YXOrderViewModel.h"
#import "DDPayMentTools.h"
#import "DDPayMentModel.h"
@interface YXOrderPayViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,LMPopInputPassViewDelegate>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) DDPayMentModel *model;
@end

@implementation YXOrderPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付中心";
    
    self.dataArr = [NSMutableArray array];
    NSArray *titls = @[@"支付宝",@"微信",@"余额"];
    for (int i = 0; i < titls.count; i++) {
        DDPayMentModel *model = [DDPayMentModel new];
        model.Id = [NSString stringWithFormat:@"%d",i];
        model.text = titls[i];
        model.pay_type = [NSString stringWithFormat:@"%d",i+1];
        [self.dataArr addObject:model];
    }
    
    [self addSubViews];


}

- (void)payMethods {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel requestPayWithOrder_type:self.order_type pay_type:self.model.pay_type order_id:self.big_order_id pay_pwd:self.model.pay_pwd Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            if ([weakSelf.model.pay_type isEqualToString:@"3"]) {
                [weakSelf pushVC];
            }else {
                [[DDPayMentTools shareDDPayMentTools] payType:[self.model.pay_type integerValue] PayMent:self.model parms:responesObj callBack:^(DDPayResultCode errCode, NSString * _Nonnull errStr) {
                    if (errCode == DDPayResultCodeSuccess) {
                        [weakSelf pushVC];
                    }else{
                        [YJProgressHUD showMessage:errStr];
                    }
                }];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
    
}

- (void)pushVC {
    
    [YJProgressHUD showMessage:@"支付成功"];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)addSubViews {
    
    [self.view addSubview:self.collectionView];
    [self.view addSubview:self.bottomView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
        make.height.equalTo(@170);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.collectionView.mas_bottom);
        make.right.left.mas_equalTo(self.view);
        make.height.equalTo(@60);
    }];
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else {
        return self.dataArr.count;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH-30, 40);
    }else {
        return CGSizeMake((SCREEN_WIDTH-90)/3, 40);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(20, 15, 20, 15);
    }else {
        return UIEdgeInsetsMake(15, 30, 15, 30);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
        cell.type = CellViewTypeStateText;
        cell.titleLab.text = @"合计金额";
        cell.contentLab.text = [NSString stringWithFormat:@"¥%@",self.price];
        return cell;
    }else if (indexPath.section == 1 ){
        YXOrderMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderMenuCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.model = self.dataArr[indexPath.row];
    for (DDPayMentModel *model in self.dataArr) {
        if (self.model == model) {
            model.selected = YES;
        }else {
            model.selected = NO;
        }
    }
    [self.collectionView reloadData];
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(0, 15, 0, 15);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma pop代理
- (void)buttonClickedAtIndex:(NSUInteger)index withText:(NSString *)text {
        
    // 验证支付密码
    DDWeakSelf;
    [YXUserViewModel requestValidatePayPwdWithPay_pwd:text Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.model.pay_pwd = text;
            [weakSelf payMethods];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];

}
- (void)dismiss:(LMPopInputPasswordView *)view{
    view.delegate = nil;
//    [self willEnterForeground];
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        JJCollectionViewRoundFlowLayout * flowLayout = [[JJCollectionViewRoundFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[DDPublicCollectionCell class] forCellWithReuseIdentifier:@"DDPublicCollectionCell"];
        [_collectionView registerClass:[YXOrderMenuCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderMenuCollectionViewCell"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"支付"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.model ==nil) {
                return [YJProgressHUD showLoading:@"请选择支付方式"];
            }else {
                if ([weakSelf.model.pay_type isEqualToString:@"3"]) {
                    LMPopInputPasswordView *popView = [[LMPopInputPasswordView alloc]init];
                    popView.frame = CGRectMake((SCREEN_WIDTH - 250)*0.5, 50, 250, 120);
                    popView.delegate = weakSelf;
                    [popView pop];
                }else {
                    [weakSelf payMethods];
                }
            }
        }];
    }
    return _bottomView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
