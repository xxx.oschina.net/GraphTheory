//
//  YXOrderListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderListViewController.h"
#import "YXOrderDetailsViewController.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "YXAnswerViewController.h"
#import "YXCustomAlertActionView.h"

#import "YXOrderHeadCollectionViewCell.h"
#import "YXOrderFootCollectionViewCell.h"
#import "YXOrderContentCollectionViewCell.h"
#import "YXOrderViewModel.h"
#import "YXUserOrderModel.h"
#import "YXUserOrderViewModel.h"
@interface YXOrderListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) YXCustomAlertActionView *alertView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) YXUserOrderModelList *model;
@end

@implementation YXOrderListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.page = 1;
    [self request];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadSubViews];

}

- (void)request {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel queryTakeOrderListWithPage:@(self.page).stringValue status:self.orderStatus is_complain:@"1" Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            YXUserOrderModel *model = [YXUserOrderModel mj_objectWithKeyValues:responesObj[@"data"]];
            NSArray *tem = [YXUserOrderModelList mj_objectArrayWithKeyValuesArray:model.list];
            [weakSelf.dataArr addObjectsFromArray:tem];
            if ([tem count] < 10) {
                [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    }];
    
}

// 请求删除订单
- (void)delete {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryDeleteSubOrderWithId:self.model.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

- (void)loadSubViews {
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{    
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YXUserOrderModelList *shop;
    if (indexPath.section < _dataArr.count) {
        shop = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        return CGSizeMake(KWIDTH-30, 44);
    }else if (indexPath.row == 1 ){
        return CGSizeMake(KWIDTH-30, 70);
    }else if (indexPath.row == 2){
        return CGSizeMake(KWIDTH-30, 44);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(0, 15, 15, 15);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf
    YXUserOrderModelList *model;
    if (indexPath.section < _dataArr.count) {
        model = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        YXOrderHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderHeadCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        cell.indexPath = indexPath;
        cell.model = model;
        [cell setClickDeleteBtn:^(NSIndexPath * _Nonnull indexPath) {
            weakSelf.model = weakSelf.dataArr[indexPath.section];
            [weakSelf showAlertViewWithType:0 Message:@"确定要删除本条数据吗？"];
        }];
        return cell;
    }else if (indexPath.row == 1){
        YXOrderContentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderContentCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        cell.model = model;
        return cell;
    }else if (indexPath.row == 2){
        YXOrderFootCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderFootCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        cell.indexPath = indexPath;
        cell.model = model;
        YXWeakSelf
        [cell setClickBottomBtnBlock:^(NSIndexPath * _Nonnull indexPath, UIButton * _Nonnull btn) {
            NSLog(@"btnTitle == %@",btn.titleLabel.text);
            weakSelf.model =weakSelf.dataArr[indexPath.section];
            if ([btn.titleLabel.text isEqualToString:@"去处理"]) {
                YXAnswerViewController *vc = [YXAnswerViewController new];
                vc.Id = weakSelf.model.Id;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }

        }];
        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YXUserOrderModelList *model = self.dataArr[indexPath.section];
    if ([model.status isEqualToString:@"10"]) {
        YXAnswerViewController *vc = [YXAnswerViewController new];
        vc.Id = model.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
        vc.type = 1;
        vc.Id = model.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }
  
}

/**
 显示弹窗

 @param type        类型 0.删除
 @param message     提示语
 */
- (void)showAlertViewWithType:(NSInteger)type Message:(NSString *)message{
    
    _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:message sureBtn:@"确认" cancleBtn:@"取消"];
    [_alertView showAnimation];
    YXWeakSelf
    [_alertView setSureClick:^(NSString * _Nonnull string) {
        if (type == 0) {
            [weakSelf delete];
        }
    }];
    
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.emptyDataSetSource = self;
        _collectionView.emptyDataSetDelegate = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        [_collectionView registerClass:[YXOrderHeadCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderHeadCollectionViewCell"];
        [_collectionView registerClass:[YXOrderContentCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderContentCollectionViewCell"];
//        [_collectionView registerClass:[WGOrderDisTypeCollectionViewCell class] forCellWithReuseIdentifier:@"WGOrderDisTypeCollectionViewCell"];
        [_collectionView registerClass:[YXOrderFootCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderFootCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无订单" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf request];
        }];
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf request];
        }];
        _collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
            weakSelf.page ++ ;
            [weakSelf request];
        }];
        
    }
    return _collectionView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
