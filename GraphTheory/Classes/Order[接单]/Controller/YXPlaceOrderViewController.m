//
//  YXPlaceOrderViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/2.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPlaceOrderViewController.h"
#import "YXAddrssListViewController.h"
#import "YXCreateAddressViewController.h"
#import "YXOrderPayViewController.h"

#import <JJCollectionViewRoundFlowLayout.h>
#import "WGPublicBottomView.h"
#import "YXOrderGoodCollectionViewCell.h"
#import "YXOrderAddressCollectionViewCell.h"
#import "YXPublicCollectionReusableView.h"
#import "YXUserViewModel.h"
#import "YXAddressModel.h"
#import "YXOrderViewModel.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
@interface YXPlaceOrderViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *addressArr;
@property (nonatomic ,strong) YXAddressModel *addressModel;
@property (nonatomic ,strong) YXGoodsInfoModel *goodsModel;

@end

@implementation YXPlaceOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"下单";
    [self loadData];

    [self setup];
}

- (void)loadData {
    DDWeakSelf
    [YXUserViewModel queryUserAddressListCompletion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSArray *data = [YXAddressModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
            [weakSelf.addressArr addObjectsFromArray:data];
            if (weakSelf.addressArr.count) {
                for (YXAddressModel *addressModel in weakSelf.addressArr) {
                    if (addressModel.is_default == 1) {
                        weakSelf.addressModel = addressModel;
                    }
                }
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
    
    [YXVideoViewModel queryGoodsInfoWithId:self.goods_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
           weakSelf.goodsModel = [YXGoodsInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}

- (void)save {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel requestGeneOrderByVideoWithAddress_id:self.addressModel.Id goods_id:self.goods_id number:@"1" Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSString *big_order_id = responesObj[@"data"][@"big_order_id"];
            if (big_order_id.length) {
                YXOrderPayViewController *vc = [YXOrderPayViewController new];
                vc.order_type = @"big";
                vc.big_order_id = big_order_id;
                vc.price = weakSelf.goodsModel.sales_price;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}


- (void)setup {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@70);
        if (@available(iOS 11.0,*)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(0);
        }
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
      
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else {
        return 1;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH-30, 60);
    }else {
        return CGSizeMake(SCREEN_WIDTH-30, 100);

    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 0) {
        return UIEdgeInsetsMake(20, 15, 20, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf
    if (indexPath.section == 0) {
        YXOrderAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderAddressCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.addressModel;
        return cell;
    }else if (indexPath.section == 1 ){
        YXOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderGoodCollectionViewCell" forIndexPath:indexPath];
        cell.type = 0;
        cell.goodsModel = self.goodsModel;
        return cell;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return CGSizeMake(KWIDTH - 30, 40);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1) {
            YXPublicCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
            header.titleLab.text = @"订单信息";
            header.backgroundColor = [UIColor clearColor];
            return header;
        }
    }
    return nil;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        DDWeakSelf
        if (self.addressArr.count) {
            YXAddrssListViewController *vc = [YXAddrssListViewController new];
            vc.type = 1;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setSelectAddressBlock:^(YXAddressModel * _Nonnull model) {
                weakSelf.addressModel = model;
                [weakSelf.collectionView reloadData];
            }];
        }else {
            YXCreateAddressViewController *vc = [[YXCreateAddressViewController alloc] init];
            vc.type = EmployeesTypeStateAdd;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setCreateAddressBlock:^{
                [weakSelf loadData];
            }];
        }
    }
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }
    
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        JJCollectionViewRoundFlowLayout * flowLayout = [[JJCollectionViewRoundFlowLayout alloc]init];
        flowLayout.isCalculateHeader = YES;
//        flowLayout.isCalculateFooter = YES;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXPublicCollectionReusableView"];
        [_collectionView registerClass:[YXOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderGoodCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXOrderAddressCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXOrderAddressCollectionViewCell"];
        
    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"提交订单"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf save];
        }];
    }
    return _bottomView;
}
- (NSMutableArray *)addressArr {
    if (!_addressArr) {
        _addressArr = [NSMutableArray array];
    }
    return _addressArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
