//
//  YXOrderAffirmViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderAffirmViewController.h"
#import "YXAddrssListViewController.h"
#import "YXCreateAddressViewController.h"
#import "YXOrderPayViewController.h"
#import "YXCouponsViewController.h"
#import <JJCollectionViewRoundFlowLayout.h>
#import "YXOrderSubmitBottomView.h"
#import "YXOrderGoodCollectionViewCell.h"
#import "YXOrderAddressCollectionViewCell.h"
#import "YXPublicCollectionReusableView.h"
#import "YXOrderRemarkFooterReusableView.h"
#import "YXUserViewModel.h"
#import "YXAddressModel.h"
#import "YXOrderViewModel.h"
#import "YXCateModel.h"
#import "YXSubmitOrderModel.h"
@interface YXOrderAffirmViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) YXOrderSubmitBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *addressArr;
@property (nonatomic ,strong) YXAddressModel *addressModel;
@property (nonatomic ,strong) YXGoodsInfoModel *goodsModel;
@property (nonatomic ,strong) YXSubmitOrderModel *submitModel;
@end

@implementation YXOrderAffirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"确认订单";
    self.submitModel = [YXSubmitOrderModel new];
    
    [self changePrice];
    
    [self loadData];

    [self setup];
    
}

- (void)loadData {
    DDWeakSelf
    [YXUserViewModel queryUserAddressListCompletion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSArray *data = [YXAddressModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
            [weakSelf.addressArr addObjectsFromArray:data];
            if (weakSelf.addressArr.count) {
                for (YXAddressModel *addressModel in weakSelf.addressArr) {
                    if (addressModel.is_default == 1) {
                        weakSelf.addressModel = addressModel;
                    }
                }
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}

- (void)setup {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@50);
        if (@available(iOS 11.0,*)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(0);
        }
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
}

// 计算总金额
- (void)changePrice {
    if (self.dataArr.count) {
        NSMutableArray *priceArr = [NSMutableArray array];
        for (YXCateDetailModel *model in self.dataArr) {
            if ([model.c_coupons_price floatValue] == 0) {
                [priceArr addObject:model.c_price];
            }else {
                CGFloat priceValue = [model.c_price floatValue] - [model.c_coupons_price floatValue];
                NSString *c_price = [NSString stringWithFormat:@"%.02f",priceValue];
                [priceArr addObject:c_price];
            }
        }
        CGFloat sum = [[priceArr valueForKeyPath:@"@sum.floatValue"] floatValue];
        _submitModel.price = [NSString stringWithFormat:@"%.02f",sum];
        _bottomView.priceLab.text = [NSString stringWithFormat:@"¥%@",self.submitModel.price];
    }
    
}


// 跳转选择优惠券
- (void)pushCouponsVC:(YXCateDetailModel *)model {
    YXCouponsViewController *vc = [YXCouponsViewController new];
    vc.type = 1;
    vc.Id = model.Id;
    vc.sales_price = model.c_price;
    [self.navigationController pushViewController:vc animated:YES];
    DDWeakSelf
    [vc setSelectCouponsBlock:^(NSString * _Nonnull coupon_id, NSString * _Nonnull price_title, NSString * _Nonnull price) {
        model.coupon_id = coupon_id;
        model.price_title = price_title;
        model.c_coupons_price = price;
        [weakSelf changePrice];
        [weakSelf.collectionView reloadData];
    }];

}

// 上传图片
- (void)uploadImg {
    
    if (self.addressModel == nil) {
        return [YJProgressHUD showMessage:@"请添加地址"];
    }
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    dispatch_group_t group = dispatch_group_create();
    //        __block NSMutableArray *images = [NSMutableArray array];
    for (YXCateDetailModel *model in self.dataArr) {
        dispatch_group_enter(group);
        if (model.imgArr.count) {
            [YXUserViewModel requestUploadImg:model.imgArr[0] Completion:^(id responesObj) {
                if (REQUESTDATASUCCESS) {
                    NSArray *dataArr = responesObj[@"data"];
                    if (dataArr.count) {
                        model.imgs = dataArr[0];
                    }
                }
                dispatch_group_leave(group);
            } failure:^(NSError *error) {
                dispatch_group_leave(group);
            }];
        }else{
            dispatch_group_leave(group);
        }
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
        [weakSelf submitOrder];
    });
           
}


// 提交订单
- (void)submitOrder {
  
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel requestGeneOrderWithAddress_id:self.addressModel.Id remark:self.submitModel.remark services:self.dataArr Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSString *big_order_id = responesObj[@"data"][@"big_order_id"];
            if (big_order_id.length) {
                YXOrderPayViewController *vc = [YXOrderPayViewController new];
                vc.order_type = @"big";
                vc.big_order_id = big_order_id;
                vc.price = weakSelf.submitModel.price;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else {
        return self.dataArr.count;
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(SCREEN_WIDTH-30, 60);
    }else {
        return CGSizeMake(SCREEN_WIDTH-30, 150);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 0) {
        return UIEdgeInsetsMake(20, 15, 20, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf
    if (indexPath.section == 0) {
        YXOrderAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderAddressCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.addressModel;
        return cell;
    }else if (indexPath.section == 1 ){
        YXOrderGoodCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderGoodCollectionViewCell" forIndexPath:indexPath];
        cell.type = 1;
        YXCateDetailModel *model = self.dataArr[indexPath.row];
        cell.model = model;
        [cell setClickCouponsBlock:^{
            [weakSelf pushCouponsVC:model];
        }];
        return cell;
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return CGSizeMake(KWIDTH - 30, 40);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return CGSizeMake(KWIDTH - 30, 120);
    }
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 1) {
            YXPublicCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
            header.titleLab.text = @"订单信息";
            header.backgroundColor = [UIColor clearColor];
            return header;
        }
    }else if ([kind isEqualToString:UICollectionElementKindSectionFooter])  {
        if (indexPath.section == 1) {
            YXOrderRemarkFooterReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXOrderRemarkFooterReusableView" forIndexPath:indexPath];
            footer.backgroundColor = [UIColor clearColor];
            footer.priceLab.text = [NSString stringWithFormat:@"¥%@",_submitModel.price];
            footer.numLab.text = [NSString stringWithFormat:@"共%ld项",self.dataArr.count];
            [footer setTextViewChange:^(NSString * _Nonnull text) {
                weakSelf.submitModel.remark = text;
            }];
            return footer;
        }
    }
    return nil;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        DDWeakSelf
        if (self.addressArr.count) {
            YXAddrssListViewController *vc = [YXAddrssListViewController new];
            vc.type = 1;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setSelectAddressBlock:^(YXAddressModel * _Nonnull model) {
                weakSelf.addressModel = model;
                [weakSelf.collectionView reloadData];
            }];
        }else {
            YXCreateAddressViewController *vc = [[YXCreateAddressViewController alloc] init];
            vc.type = EmployeesTypeStateAdd;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setCreateAddressBlock:^{
                [weakSelf loadData];
            }];
        }
    }
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 0, 15);
    }
    
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        JJCollectionViewRoundFlowLayout * flowLayout = [[JJCollectionViewRoundFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.isCalculateHeader = YES;
        flowLayout.isCalculateFooter = YES;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXPublicCollectionReusableView"];
        [_collectionView registerClass:[YXOrderRemarkFooterReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXOrderRemarkFooterReusableView"];
        [_collectionView registerClass:[YXOrderGoodCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderGoodCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXOrderAddressCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXOrderAddressCollectionViewCell"];
            
    }
    return _collectionView;
}

- (YXOrderSubmitBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[YXOrderSubmitBottomView alloc] initWithFrame:CGRectZero];
        _bottomView.numLab.text = [NSString stringWithFormat:@"共%ld项",self.dataArr.count];
        _bottomView.priceLab.text = [NSString stringWithFormat:@"¥%@",self.submitModel.price];
        DDWeakSelf
        [_bottomView setOrderAction:^{
            [weakSelf uploadImg];
        }];
    }
    return _bottomView;
}
- (NSMutableArray *)addressArr {
    if (!_addressArr) {
        _addressArr = [NSMutableArray array];
    }
    return _addressArr;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
