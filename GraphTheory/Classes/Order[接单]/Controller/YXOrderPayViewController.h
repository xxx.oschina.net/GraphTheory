//
//  YXOrderPayViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderPayViewController : DDBaseViewController

// big:商品订单 sub:小纸条订单
@property (nonatomic ,strong) NSString *order_type;
// 订单id
@property (nonatomic ,strong) NSString *big_order_id;
// 订单价格
@property (nonatomic ,strong) NSString *price;


@end

NS_ASSUME_NONNULL_END
