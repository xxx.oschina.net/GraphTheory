//
//  YXOrderAffirmViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderAffirmViewController : DDBaseViewController

@property (nonatomic ,strong) NSArray *dataArr;

@end

NS_ASSUME_NONNULL_END
