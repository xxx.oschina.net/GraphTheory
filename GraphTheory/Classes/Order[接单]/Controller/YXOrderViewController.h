//
//  DDOrderViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/30.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import <JXCategoryView.h>
NS_ASSUME_NONNULL_BEGIN

@interface YXOrderViewController : DDBaseViewController

@property (nonatomic, strong) JXCategoryNumberView *categoryView;//类型选择器
@property (nonatomic, assign) NSInteger selectIndex;

@end

NS_ASSUME_NONNULL_END
