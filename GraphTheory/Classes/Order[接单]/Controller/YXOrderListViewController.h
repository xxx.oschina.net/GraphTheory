//
//  YXOrderListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>

@property (nonatomic ,strong) NSString *orderStatus;///<订单状态 0全部1进行中 2已完成3待确认4退款/售后

@end

NS_ASSUME_NONNULL_END
