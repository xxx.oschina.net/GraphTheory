//
//  YXCateContentViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/17.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateContentViewController.h"
#import "YXWebViewController.h"
#import "YXLocationViewController.h"

#import "YXCateListTableViewCell.h"
#import "YXCateStandardTableViewCell.h"
#import "YXCateTextViewTableViewCell.h"
#import "YXCateAddressTableViewCell.h"
#import "YXPhotoTableViewCell.h"
#import <MAMapKit/MAMapKit.h>
#import "YXCateModel.h"
#import "YXHomeViewModel.h"
#import "UITextView+Placeholder.h"
#import "YXAMapManager.h"
#import "YXCityModel.h"
@interface YXCateContentViewController ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate>

@property (nonatomic ,strong) UITableView *tableView;
//地图
@property (nonatomic, strong) MAMapView *mapView;

@property (nonatomic, strong) NSMutableArray *addressArr;
@property (nonatomic, strong) NSMutableArray *selectImgArr;

@end

@implementation YXCateContentViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectImgArr = [NSMutableArray array];
    self.addressArr = [NSMutableArray array];
    
    [self loadSubViews];
     
}

- (void)loadData {
    
    DDWeakSelf
    [YXHomeViewModel queryCateDetailWithCate_id:self.childModel.Id Completion:^(id  _Nonnull responesObj) {
        [weakSelf.addressArr removeAllObjects];
        weakSelf.model = [YXCateDetailModel mj_objectWithKeyValues:responesObj[@"data"]];
        weakSelf.model.address = weakSelf.cityModel.POIName;
        weakSelf.model.lat = [NSString stringWithFormat:@"%f",weakSelf.cityModel.lat];
        weakSelf.model.lng =[NSString stringWithFormat:@"%f",weakSelf.cityModel.lng];
        if (weakSelf.model.takeUsers.count) {
            for (YXTakeUsers *takeUsers in weakSelf.model.takeUsers) {
                if (takeUsers.userInfo.address) {
                    [weakSelf.addressArr addObject:takeUsers.userInfo.address];
                }
            }
        }
        if (weakSelf.currentCateDetailModel) {
            weakSelf.currentCateDetailModel(weakSelf.model);
        }
        [weakSelf createCustomAnnotation];
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)createCustomAnnotation {
    
    CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(34.273544, 117.165335);
    //设置地图的定位中心点坐标
    self.mapView.centerCoordinate = coor;
    if (self.addressArr.count) {
        for (YXTakeUsersUserInfoAddress *address in self.addressArr) {
            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
            [pointAnnotation setCoordinate:CLLocationCoordinate2DMake([address.lat doubleValue], [address.lng doubleValue])];
            //将点添加到地图上，即所谓的大头针
            pointAnnotation.title = address.address;
            [self.mapView addAnnotation:pointAnnotation];
        }
    }
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 80;
    }else if (indexPath.section == 1) {
        return 40;
    }else if (indexPath.section == 2) {
        return 100;
    }else if (indexPath.section == 3) {
        return 60;
    }else if (indexPath.section == 4) {
        return 60;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DDWeakSelf
    if (indexPath.section == 0) {
        YXCateListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCateListTableViewCell" forIndexPath:indexPath];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:self.model.c_remarkimg]];
        cell.titleLab.text = self.model.c_remarka;
        cell.subTitleLab.text = self.model.c_remarkb;
        cell.priceLab.text = self.model.c_remarkc;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }else if (indexPath.section == 1) {
        YXCateStandardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCateStandardTableViewCell" forIndexPath:indexPath];
        [cell.typeBtn setTitle:self.model.c_remarktitle forState:(UIControlStateNormal)];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }else if (indexPath.section == 2) {
        YXPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPhotoTableViewCell"];
        if (!cell) {
            cell = [[YXPhotoTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPhotoTableViewCell"];
        }
        
        [cell.imgPicker observeSelectedMediaArray:^(NSArray<LLImagePickerModel *> *list){
            [weakSelf.selectImgArr removeAllObjects];
             for (LLImagePickerModel *model in list){
                 [weakSelf.selectImgArr addObject:model.image];
             }
            weakSelf.model.imgArr = weakSelf.selectImgArr;
         }];
        return cell;
    }else if (indexPath.section == 3) {
        YXCateTextViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCateTextViewTableViewCell" forIndexPath:indexPath];
        cell.textView.placeholder = self.model.c_remarktips;
        [cell setTextViewChange:^(NSString * _Nonnull text) {
            weakSelf.model.content = text;
        }];
        return cell;
    }else if (indexPath.section == 4) {
        YXCateAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCateAddressTableViewCell" forIndexPath:indexPath];
        cell.titleLab.text = self.cityModel.POIName;
        cell.contentLab.text = self.cityModel.address;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    return [UITableViewCell new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = self.model.name;
        vc.url = self.model.c_content;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 1) {
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = self.model.typeInfo.ctype_name;
        vc.url = self.model.c_contentprice;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 4) {
        YXLocationViewController *vc = [YXLocationViewController new];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
        DDWeakSelf
        [vc setSelectAddressPOIBlock:^(NSString * _Nonnull name, NSString * _Nonnull address, CGFloat lat, CGFloat lng) {
            weakSelf.cityModel.POIName = name;
            weakSelf.cityModel.address = address;
            weakSelf.model.address = name;
            weakSelf.model.lat = [NSString stringWithFormat:@"%f",lat];
            weakSelf.model.lng = [NSString stringWithFormat:@"%f",lng];
            [weakSelf.tableView reloadData];
        }];
    }
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [self createFooterView];
        [_tableView registerNib:[UINib nibWithNibName:@"YXCateListTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXCateListTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXCateStandardTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXCateStandardTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXCateTextViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXCateTextViewTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXCateAddressTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXCateAddressTableViewCell"];
        [_tableView registerClass:[YXPhotoTableViewCell class] forCellReuseIdentifier:@"YXPhotoTableViewCell"];
        
    }
    return _tableView;
}

- (UIView *)createFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, SCREEN_WIDTH, 300))];
    //地图初始化
   self.mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 300)];
    _mapView.backgroundColor = [UIColor whiteColor];
    self.mapView.delegate = self;
    //设置定位精度
    _mapView.desiredAccuracy = kCLLocationAccuracyBest;
   //设置定位距离
    _mapView.distanceFilter = 5.0f;
     _mapView.zoomEnabled = YES;
    _mapView.showsCompass = NO;
    //普通样式
    _mapView.mapType = MAMapTypeStandard;
     //缩放等级
     [_mapView setZoomLevel:12 animated:YES];
   //开启定位
//    _mapView.showsUserLocation = YES;
    [footerView addSubview:self.mapView];
    return footerView;
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
       MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.image = [UIImage imageNamed:@"map_b1"];
        return annotationView;
    }
    return nil;
}


- (UIView *)listView {
    return self.view;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
