//
//  YXAllCateViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXAllCateViewController : DDBaseViewController

@property (nonatomic ,strong) NSString *type;
@property (nonatomic ,strong) NSString *city_ids;

@end

NS_ASSUME_NONNULL_END
