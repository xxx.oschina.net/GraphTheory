//
//  YXCateDetailsListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"
NS_ASSUME_NONNULL_BEGIN
@class YXCateChildModel,YXCateDetailModel,YXCitysModel;
@interface YXCateDetailsListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>

@property (nonatomic ,strong) YXCateChildModel *childModel;

@property (nonatomic, strong) NSMutableArray *titles;

@property (nonatomic, strong) YXCitysModel *cityModel;

// 选择当前的模型回调
@property (nonatomic, copy)void(^currentCateDetailModel)(YXCateDetailModel *model);

@end

NS_ASSUME_NONNULL_END
