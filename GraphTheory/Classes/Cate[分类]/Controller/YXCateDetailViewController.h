//
//  YXCateDetailViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCateDetailViewController : DDBaseViewController

// 分类id
@property (nonatomic ,strong) NSString *cate_id;

@end

NS_ASSUME_NONNULL_END
