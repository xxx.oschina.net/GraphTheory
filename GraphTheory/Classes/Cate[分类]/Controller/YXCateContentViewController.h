//
//  YXCateContentViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/17.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"
NS_ASSUME_NONNULL_BEGIN
@class YXCateChildModel,YXCateDetailModel,YXCitysModel;
@interface YXCateContentViewController : DDBaseViewController  <JXCategoryListContentViewDelegate>

@property (nonatomic ,strong) YXCateChildModel *childModel;

@property (nonatomic, strong) YXCateDetailModel *model;

@property (nonatomic, strong) YXCitysModel *cityModel;

@property (nonatomic, copy)void(^currentCateDetailModel)(YXCateDetailModel *model);

@end

NS_ASSUME_NONNULL_END
