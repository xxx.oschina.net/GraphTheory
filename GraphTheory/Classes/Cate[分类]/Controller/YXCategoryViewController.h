//
//  YXCategoryViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCategoryViewController : DDBaseViewController

// type 0:B1  1:B2
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
