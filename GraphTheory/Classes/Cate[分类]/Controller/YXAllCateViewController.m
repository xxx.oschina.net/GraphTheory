//
//  YXAllCateViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAllCateViewController.h"
#import "YXCateDetailViewController.h"
#import "DDSearchViewController.h"
#import "YXServiceContentView.h"
#import "YXAllCateTableViewCell.h"
#import "YXPublicTableHeaderView.h"
#import "YXSearchView.h"

#import "YXHomeViewModel.h"
#import "YXCateModel.h"

@interface YXAllCateViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) YXCateCategorysModel *model;
@property (strong, nonatomic) YXSearchView *searchView;

@end

@implementation YXAllCateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createNavUI];
    
    [self loadData];
    
    [self loadSubViews];
}

- (void)loadData {
    DDWeakSelf
    [YXHomeViewModel queryHomeAllCatesWithType:self.type city_ids:@"229" c_model:nil Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.dataArr = [YXCateCategorysModel mj_objectArrayWithKeyValuesArray:  responesObj[@"data"]];
            weakSelf.model = [weakSelf.dataArr firstObject];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

- (void)createNavUI {
    
    UIButton *backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    backBtn.frame = CGRectMake(0, 0, 60, 30);
    [backBtn setTitle:@"首页" forState:(UIControlStateNormal)];
    [backBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [backBtn setImage:[UIImage imageNamed:@"arrow_left"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.navigationItem.titleView = self.searchView;
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(260, 30));
    }];
}

- (void)backClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.edges.equalTo(self.view);
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.model.categorys.count ;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    YXCateModel *cateModel = self.model.categorys[section];
    return cateModel.child.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXCateModel *cateModel = self.model.categorys[indexPath.section];
    YXCateChildModel *childModel = cateModel.child[indexPath.row];
    return childModel.cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   static  NSString *cellID = @"YXAllCateTableViewCell";
    YXAllCateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[YXAllCateTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellID];
    }
    YXCateModel *cateModel = self.model.categorys[indexPath.section];
    cell.childModel = cateModel.child[indexPath.row];
    DDWeakSelf
    [cell setClickItemBlock:^(YXCateChildModel * _Nonnull childModel) {
        YXCateDetailViewController *vc = [YXCateDetailViewController new];
        vc.cate_id = cateModel.Id;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    YXPublicTableHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"YXPublicTableHeaderView"];
    YXCateModel *cateModel = self.model.categorys[section];
    headerView.title = cateModel.name;
    return headerView;
}
- (void)click {
    DDSearchViewController *vc = [DDSearchViewController new];
    vc.placeholder = @"知识任务";
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Lazy Loading
- (YXSearchView *)searchView {
    if (!_searchView) {
        _searchView = [[YXSearchView alloc] initWithFrame:CGRectZero];
        if(@available(iOS 11.0, *)) {
            [[_searchView.heightAnchor constraintEqualToConstant:44] setActive:YES];
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [_searchView addGestureRecognizer:tap];
    }
    return _searchView;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[YXAllCateTableViewCell class] forCellReuseIdentifier:@"YXAllCateTableViewCell"];
        [_tableView registerClass:[YXPublicTableHeaderView class] forHeaderFooterViewReuseIdentifier:@"YXPublicTableHeaderView"];
    }
    return _tableView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
@end
