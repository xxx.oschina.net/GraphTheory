//
//  YXCateDetailViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateDetailViewController.h"
#import "YXCateDetailsListViewController.h"
#import "YXOrderAffirmViewController.h"
#import "DDSearchViewController.h"
#import <JXCategoryView.h>
#import "JXCategoryTitleImageCellModel.h"
#import "YXHomeViewModel.h"
#import "YXCateModel.h"
#import "YXCateBottomView.h"
#import "YXSearchView.h"
#import "YXCatePopView.h"
#import "YXAMapManager.h"
#import "YXCityModel.h"

@interface YXCateDetailViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>
@property (nonatomic, strong) JXCategoryTitleImageView *categoryView;//类型选择器
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;//类型视图选择器
@property (nonatomic, strong) YXCateBottomView *bottomView;
@property (strong, nonatomic) YXSearchView *searchView;

@property (nonatomic, strong) NSMutableArray *titles;
@property (strong, nonatomic) NSMutableArray *selectArr;
@property (nonatomic, strong) YXCateModel *cateModel;
@property (nonatomic, strong) YXCateDetailModel *currentModel;
@property (nonatomic, strong) YXCitysModel *cityModel;
@end

@implementation YXCateDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNavUI];

    DDWeakSelf
    [[YXAMapManager shareLocation] getLocationInfo:^(YXCitysModel * _Nonnull cityModel) {
        weakSelf.cityModel = cityModel;
        [weakSelf.categoryView reloadData];
    }];
        
    [self loadData];

}

- (void)loadData {
    
    YXWeakSelf
    [YXHomeViewModel queryTopCateDetailWithCate_id:self.cate_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.cateModel = [YXCateModel mj_objectWithKeyValues:responesObj[@"data"]];
            if (weakSelf.cateModel.child.count) {
                NSMutableArray *ctypeName = [NSMutableArray array];
                NSMutableArray *ctypeImg= [NSMutableArray array];
                for (YXCateChildModel *childModel in weakSelf.cateModel.child) {
                    [ctypeName addObject:childModel.ctype_name];
                    [ctypeImg addObject:[NSURL URLWithString:childModel.ctype_img]];
                }
                weakSelf.categoryView.titles = ctypeName;
                weakSelf.categoryView.imageURLs = ctypeImg;
                weakSelf.categoryView.selectedImageURLs = ctypeImg;

                NSMutableArray *types = [NSMutableArray array];
                for (int i = 0; i < ctypeName.count; i++) {
                    [types addObject:@(JXCategoryTitleImageType_TopImage)];
                }
                weakSelf.categoryView.imageTypes = types;
                [weakSelf.categoryView setLoadImageCallback:^(UIImageView *imageView, NSURL *imageURL) {
                    [imageView sd_setImageWithURL:imageURL];
                }];

            }
        
            [weakSelf loadSubViews];
          
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)createNavUI {
    
    UIButton *backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    backBtn.frame = CGRectMake(0, 0, 60, 30);
    [backBtn setTitle:@"首页" forState:(UIControlStateNormal)];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [backBtn setImage:[UIImage imageNamed:@"arrow_left"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.navigationItem.titleView = self.searchView;
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(280, 30));
    }];
}

- (void)backClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.listContainerView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        if (@available(iOS 11.0,*)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(0);
        }
        make.height.equalTo(@70);
    }];
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.height.mas_equalTo(@80);
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(0);
        }
    }];
    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.categoryView.mas_bottom);
        make.bottom.mas_equalTo(self.bottomView.mas_top);
    }];
    
 
    
}

#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}
#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    YXCateDetailsListViewController *list = [YXCateDetailsListViewController new];
    YXCateChildModel *childMode = self.cateModel.child[index];
    list.childModel = childMode;
    list.cityModel = self.cityModel;
    DDWeakSelf
    [list setCurrentCateDetailModel:^(YXCateDetailModel * _Nonnull model) {
        NSLog(@"当前实体类  == %@",model.name);
        weakSelf.currentModel = model;
    }];
    
    return list;
}
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.cateModel.child.count;
}

#pragma mark - Lazy Loading
- (JXCategoryTitleImageView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryTitleImageView alloc]init];
        _categoryView.delegate = self;
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleFont = [UIFont systemFontOfSize:14.0];
        _categoryView.titleColor = [UIColor whiteColor];
        _categoryView.titleSelectedColor = [UIColor whiteColor];
        _categoryView.defaultSelectedIndex = 0;
        _categoryView.backgroundColor = APPTintColor;
        _categoryView.imageSize = CGSizeMake(24, 24);
        _categoryView.averageCellSpacingEnabled = NO;
        JXCategoryIndicatorTriangleView *triangleView = [[JXCategoryIndicatorTriangleView alloc] init];
        triangleView.indicatorWidth = JXCategoryViewAutomaticDimension;
        //可以试试宽度补偿
        triangleView.indicatorColor = [UIColor whiteColor];
        triangleView.indicatorWidth = 15;
        triangleView.indicatorHeight = 10;
        triangleView.indicatorWidthIncrement = 0;
        triangleView.verticalMargin = 0;
        _categoryView.indicators = @[triangleView];
        _categoryView.listContainer = self.listContainerView;
        _listContainerView = self.listContainerView;
       
    }
    return _categoryView;
}
- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_CollectionView delegate:self];
    }
    return _listContainerView;
}
- (YXCateBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[YXCateBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView.numBtn setTitle:[NSString stringWithFormat:@"共%ld项",self.selectArr.count] forState:(UIControlStateNormal)];
        DDWeakSelf
        [_bottomView setClickBtnBlock:^(UIButton * _Nonnull btn) {
            if ([btn.titleLabel.text isEqualToString:@"成为雇主"]) {
                [weakSelf addMethods];
            }else if ([btn.titleLabel.text isEqualToString:@"下单"]) {
                [weakSelf orderMethods];
            }else {
                [weakSelf popMehtods];
            }
        }];
    }
    return _bottomView;
}
- (YXSearchView *)searchView {
    if (!_searchView) {
        _searchView = [[YXSearchView alloc] initWithFrame:CGRectZero];
        if(@available(iOS 11.0, *)) {
            [[_searchView.heightAnchor constraintEqualToConstant:44] setActive:YES];
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [_searchView addGestureRecognizer:tap];
    }
    return _searchView;
}
- (NSMutableArray *)selectArr {
    if (!_selectArr) {
        _selectArr = [NSMutableArray array];
    }
    return _selectArr;
}

#pragma mark - Touch Even
// 添加雇主
- (void)addMethods {
    if (self.selectArr.count) {
        NSMutableArray *Ids = [NSMutableArray array];
        for (YXCateDetailModel *tmpeMode in self.selectArr) {
            [Ids addObject:tmpeMode.Id];
        }
        if (![Ids containsObject:self.currentModel.Id]) {
            [self.selectArr addObject:self.currentModel];
            [self.bottomView.numBtn setTitle:[NSString stringWithFormat:@"共%ld项",self.selectArr.count] forState:(UIControlStateNormal)];
        }else {
            return [YJProgressHUD showMessage:@"此服务已添加"];
        }
    }else {
        [self.selectArr addObject:self.currentModel];
        [self.bottomView.numBtn setTitle:[NSString stringWithFormat:@"共%ld项",self.selectArr.count] forState:(UIControlStateNormal)];
    }
}
// 下单
- (void)orderMethods {
    
    if ([YXUserInfoManager getUserInfo].token == nil) {
        KPOP(@"游客不能使用此功能，请先登录");
        return;
    }
    
    if (self.selectArr.count) {
        YXOrderAffirmViewController *vc = [YXOrderAffirmViewController new];
        vc.dataArr = self.selectArr;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        return [YJProgressHUD showMessage:@"请添加服务项"];
    }
}
// 弹窗
- (void)popMehtods {
    if (self.selectArr.count) {
        DDWeakSelf
        YXCatePopView *popView = [[YXCatePopView alloc] initWithFrame:[UIScreen mainScreen].bounds withArray:self.selectArr];
        [popView show];
        [popView setDelectBlock:^(NSArray * _Nonnull dataArr) {
            weakSelf.selectArr = [NSMutableArray arrayWithArray:dataArr];
            [weakSelf.bottomView.numBtn setTitle:[NSString stringWithFormat:@"共%ld项",weakSelf.selectArr.count] forState:(UIControlStateNormal)];
        }];
    }else {
        return [YJProgressHUD showMessage:@"请添加服务项"];
    }
}

// 点击搜索
- (void)click {
    DDSearchViewController *vc = [DDSearchViewController new];
    vc.placeholder = @"知识任务";
    [self.navigationController pushViewController:vc animated:YES];
}
    
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
