//
//  YXCategoryViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCategoryViewController.h"
#import "YXVideoListViewController.h"
#import "DDSearchViewController.h"

#import "WGStoreSelectCategoryView.h"
#import "YXCategoryContentView.h"
#import "YXSearchView.h"

#import "YXStoreViewModel.h"
#import "WGStoreModel.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"
#import "YXHomeViewModel.h"
#import "YXCateModel.h"
@interface YXCategoryViewController ()
@property (nonatomic, strong) WGStoreSelectCategoryView *category1;
@property (nonatomic, strong) YXCategoryContentView *category2;
@property (strong, nonatomic) YXSearchView *searchView;

@property (nonatomic, strong) NSMutableArray *categoryArr1;
@property (nonatomic, strong) NSMutableArray *categoryArr2;

@property (nonatomic, strong) NSString *categoryId;
@property (nonatomic, strong) NSString *categoryName;
@end

@implementation YXCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNavUI];
    
    [self loadData];
    [self setUpLayout];
    _category1.indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
}

- (void)loadData {
    DDWeakSelf
    if (self.type == 0) {
        [YXHomeViewModel queryHomeAllCatesWithType:@"0" city_ids:@"229" c_model:@"0" Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                [weakSelf.categoryArr1 removeAllObjects];
                [weakSelf.categoryArr2 removeAllObjects];
                NSArray *dataArr = [YXCateCategorysModel mj_objectArrayWithKeyValuesArray:  responesObj[@"data"]];
                YXCateCategorysModel *cateCategorysModel = [dataArr firstObject];
                weakSelf.categoryArr1 = [NSMutableArray arrayWithArray:cateCategorysModel.categorys];
                NSMutableArray *nameArr = [NSMutableArray array];
                for (YXCateModel *categoryModel in weakSelf.categoryArr1) {
                    [nameArr addObject:categoryModel.name];
                }
                weakSelf.category1.dataArr = nameArr;
                if (weakSelf.categoryArr1.count > 0) {
                    YXCateModel *categoryModel= weakSelf.categoryArr1[0];
                    weakSelf.categoryArr2 = [NSMutableArray arrayWithArray:categoryModel.child];
                    weakSelf.category2.dataArr = weakSelf.categoryArr2;
                }
            }
        } failure:^(NSError * _Nonnull error) {
            
        }];
    }else {
        [YXVideoViewModel queryAllBrandsWithCity_id:@"229" Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                [weakSelf.categoryArr1 removeAllObjects];
                [weakSelf.categoryArr2 removeAllObjects];
                weakSelf.categoryArr1 = [YXBrandModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
                NSMutableArray *nameArr = [NSMutableArray array];
                for (YXBrandModel *categoryModel in weakSelf.categoryArr1) {
                    [nameArr addObject:categoryModel.brand_name];
                }
                weakSelf.category1.dataArr = nameArr;
                if (weakSelf.categoryArr1.count > 0) {
                    YXBrandModel *categoryModel= weakSelf.categoryArr1[0];
                    weakSelf.categoryArr2 = [NSMutableArray arrayWithArray:categoryModel.child];
                    weakSelf.category2.dataArr = weakSelf.categoryArr2;
                }
            }
        } failure:^(NSError * _Nonnull error) {
            
        }];
    }
}


- (void)createNavUI {
    
    UIButton *backBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    backBtn.frame = CGRectMake(0, 0, 70, 30);
    [backBtn setTitle:@"返回" forState:(UIControlStateNormal)];
    [backBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [backBtn setImage:[UIImage imageNamed:@"arrow_left"] forState:(UIControlStateNormal)];
    backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    self.navigationItem.titleView = self.searchView;
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(260, 30));
    }];
}
- (void)backClick {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)setUpLayout{
    [self.view addSubview:self.category1];
    [self.view addSubview:self.category2];
    [_category1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.left.and.bottom.equalTo(self.view);
        make.width.mas_equalTo(125);
    }];
    [_category2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.right.and.bottom.equalTo(self.view);
        make.left.equalTo(self.category1.mas_right);
    }];

}
- (WGStoreSelectCategoryView *)category1{
    if (!_category1) {
        _category1 = [WGStoreSelectCategoryView new];
        _category1.type = 0;
        
        YXWeakSelf
        [_category1 setSelectCategory:^(NSInteger type, NSIndexPath * _Nonnull indexPath) {
            [weakSelf.categoryArr2 removeAllObjects];
            if (weakSelf.type == 0) {
                YXCateModel *categoryModel = weakSelf.categoryArr1[indexPath.row];
                weakSelf.categoryArr2 = [NSMutableArray arrayWithArray:categoryModel.child];
                weakSelf.category2.dataArr = weakSelf.categoryArr2;
            }else {
                YXBrandModel *categoryModel = weakSelf.categoryArr1[indexPath.row];
                weakSelf.categoryArr2 = [NSMutableArray arrayWithArray:categoryModel.child];
                weakSelf.category2.dataArr = weakSelf.categoryArr2;
            }
//            weakSelf.categoryId = categoryModel.brand_id;
//            weakSelf.categoryName = categoryModel.brand_name;
            
        }];
    }
    return _category1;
}
- (YXCategoryContentView *)category2{
    if (!_category2) {
        _category2 = [YXCategoryContentView new];
        _category2.type = self.type;
        YXWeakSelf
        [_category2 setSelectCategory:^(NSIndexPath * _Nonnull indexPath) {
            
            if ([YXUserInfoManager getUserInfo].token == nil) {
                KPOP(@"游客不能使用此功能，请先登录");
                return;
            }
            
            if (weakSelf.type == 0) {
                YXCateModel *categoryModel = weakSelf.categoryArr2[indexPath.section];
                YXCateChildModel *childModel = categoryModel.child[indexPath.row];
                YXVideoListViewController *vc = [YXVideoListViewController new];
                vc.type = weakSelf.type;
                vc.cate_id = childModel.Id;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                YXBrandModel *categoryModel = weakSelf.categoryArr2[indexPath.row];
                YXVideoListViewController *vc = [YXVideoListViewController new];
                vc.type = weakSelf.type;
                vc.brand_id = categoryModel.brand_id;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
         
        }];

    }
    return _category2;

}
- (YXSearchView *)searchView {
    if (!_searchView) {
        _searchView = [[YXSearchView alloc] initWithFrame:CGRectZero];
        if(@available(iOS 11.0, *)) {
            [[_searchView.heightAnchor constraintEqualToConstant:44] setActive:YES];
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [_searchView addGestureRecognizer:tap];
    }
    return _searchView;
}
- (NSMutableArray *)categoryArr1 {
    if (!_categoryArr1) {
        _categoryArr1 = [NSMutableArray array];
    }
    return _categoryArr1;
}
- (NSMutableArray *)categoryArr2 {
    if (!_categoryArr2) {
        _categoryArr2 = [NSMutableArray array];
    }
    return _categoryArr2;
}

- (void)click {
    DDSearchViewController *vc = [DDSearchViewController new];
    vc.placeholder = @"知识任务";
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
