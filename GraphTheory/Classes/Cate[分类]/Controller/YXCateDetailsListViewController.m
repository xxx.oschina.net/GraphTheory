//
//  YXCateDetailsListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateDetailsListViewController.h"
#import "YXCateContentViewController.h"
#import <JXCategoryView.h>
#import "YXCateModel.h"
@interface YXCateDetailsListViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) JXCategoryTitleView *categoryView;
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;//类型视图选择器

@end

@implementation YXCateDetailsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    self.titles = [NSMutableArray array];
    if (self.childModel.child.count) {
        for (YXCateChildModel *childModel in self.childModel.child) {
            [self.titles addObject:childModel.name];
        }
        self.categoryView.titles = self.titles;
    }
 
    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.listContainerView];

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    self.categoryView.frame = CGRectMake(0, 0, self.view.bounds.size.width, [self preferredCategoryViewHeight]);
    self.listContainerView.frame = CGRectMake(0, [self preferredCategoryViewHeight], self.view.bounds.size.width, self.view.bounds.size.height-[self preferredCategoryViewHeight]);
}



- (CGFloat)preferredCategoryViewHeight {
    return 40;
}
#pragma mark - JXCategoryViewDelegate

// 点击选中或者滚动选中都会调用该方法。适用于只关心选中事件，不关心具体是点击还是滚动选中的。
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
    // 侧滑手势处理
//    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
//    YXCateChildModel *childModel = self.childModel.child[index];
//    if (self.selectCurrentBlock) {
//        self.selectCurrentBlock(childModel);
//    }
}

// 滚动选中的情况才会调用该方法
- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}
#pragma mark - JXCategoryListContainerViewDelegate

// 返回列表的数量
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

// 返回各个列表菜单下的实例，该实例需要遵守并实现 <JXCategoryListContentViewDelegate> 协议
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    YXCateChildModel *childModel = self.childModel.child[index];
    YXCateContentViewController *list = [[YXCateContentViewController alloc] init];
    list.childModel = childModel;
    list.cityModel = self.cityModel;
    DDWeakSelf
    [list setCurrentCateDetailModel:^(YXCateDetailModel * _Nonnull model) {
        if (weakSelf.currentCateDetailModel) {
            weakSelf.currentCateDetailModel(model);
        }
    }];
    return list;
}
// 分页菜单视图
- (JXCategoryTitleView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryTitleView alloc]init];
        _categoryView.delegate = self;
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleFont = [UIFont systemFontOfSize:14.0];
        _categoryView.titleColor = color_TextOne;
        _categoryView.titleSelectedColor = color_TextOne;
        _categoryView.defaultSelectedIndex = 0;
        _categoryView.backgroundColor = [UIColor whiteColor];
        _categoryView.averageCellSpacingEnabled = NO;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorWidth = JXCategoryViewAutomaticDimension;
        //可以试试宽度补偿
        lineView.indicatorColor = APPTintColor;
        lineView.indicatorHeight = 2;
        lineView.indicatorWidthIncrement = 0;
        lineView.verticalMargin = 0;
        _categoryView.indicators = @[lineView];
        _categoryView.listContainer = self.listContainerView;
        _listContainerView = self.listContainerView;
    }
    return _categoryView;
}


// 列表容器视图
- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_CollectionView delegate:self];
    }
    return _listContainerView;
}


- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
