//
//  YXCateStandardTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateStandardTableViewCell.h"

@implementation YXCateStandardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.typeBtn.layer.masksToBounds = YES;
    self.typeBtn.layer.cornerRadius = 4.0f;
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.typeBtn.backgroundColor = APPTintColor;
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
