//
//  WGStoreSelectCategoryView.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectCategoryView.h"
#import "WGStoreSelectCategoryTableViewCell.h"

@interface WGStoreSelectCategoryView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong)UILabel *titleLab;
@end


@implementation WGStoreSelectCategoryView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        if (_type == 0) {
            self.backgroundColor = [UIColor colorWithHexString:@"#f8f8f8"];
        }else{
            self.backgroundColor = [UIColor whiteColor];
        }
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout{
    
    [self addSubview:self.titleLab];
    [self addSubview:self.tableView];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@50);
    }];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom);
        make.left.right.bottom.equalTo(self);
    }];
//    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self).mas_offset(UIEdgeInsetsMake(0, 0, -Home_Indicator_HEIGHT, 0));
//    }];
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 0) {
        self.backgroundColor = [UIColor colorWithHexString:@"#f8f8f8"];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.separatorColor = color_LineColor;
    }else{
        self.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    }
}
- (void)setDataArr:(NSArray *)dataArr{
    _dataArr = dataArr;
    [self.tableView reloadData];
}
- (void)setIndexPath:(NSIndexPath *)indexPath{
    _indexPath = indexPath;
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WGStoreSelectCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WGStoreSelectCategoryTableViewCell" forIndexPath:indexPath];
    cell.titleStr = _dataArr[indexPath.row];
    cell.isSelect = _type == 0?_indexPath == indexPath:NO;
    return cell;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 50;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UILabel *titleLab = [[UILabel alloc] initWithFrame:(CGRectMake(0, 0, self.width, 50))];
//    titleLab.backgroundColor = [UIColor clearColor];
//    titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
//    titleLab.text = @"推荐分类";
//    titleLab.font = [UIFont systemFontOfSize:14.0f];
//    titleLab.textAlignment = NSTextAlignmentCenter;
//    return titleLab;
//}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _indexPath = indexPath;
    [_tableView reloadData];
    if (self.selectCategory) {
        self.selectCategory(_type, indexPath);
    }
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.backgroundColor = color_LineColor;
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLab.text = @"推荐分类";
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLab;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        if (_type == 1) {
            _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        }else{
            _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
            _tableView.separatorColor = color_LineColor;

        }
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        [_tableView registerClass:[WGStoreSelectCategoryTableViewCell class] forCellReuseIdentifier:@"WGStoreSelectCategoryTableViewCell"];
    }
    return _tableView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
