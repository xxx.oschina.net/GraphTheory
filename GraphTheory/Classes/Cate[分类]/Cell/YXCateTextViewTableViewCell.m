//
//  YXCateTextViewTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateTextViewTableViewCell.h"

@interface YXCateTextViewTableViewCell ()<UITextViewDelegate>

@end

@implementation YXCateTextViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.textView.delegate =self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewChange) {
        self.textViewChange(textView.text);
    }
}


@end
