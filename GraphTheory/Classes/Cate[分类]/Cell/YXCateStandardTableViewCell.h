//
//  YXCateStandardTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCateStandardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;

@end

NS_ASSUME_NONNULL_END
