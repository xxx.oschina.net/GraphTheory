//
//  YXPhotoTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LLImagePickerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXPhotoTableViewCell : UITableViewCell
@property (strong, nonatomic)LLImagePickerView *imgPicker;

@end

NS_ASSUME_NONNULL_END
