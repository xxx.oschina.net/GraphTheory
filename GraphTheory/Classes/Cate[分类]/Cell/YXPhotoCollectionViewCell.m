//
//  YXPhotoCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPhotoCollectionViewCell.h"

@implementation YXPhotoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _imgPicker = [[LLImagePickerView alloc]initWithFrame:CGRectMake(10, 15, SCREEN_WIDTH-20-20, 70)];
    _imgPicker.type = LLImageTypePhoto;
    _imgPicker.maxImageSelected = 5;
    _imgPicker.allowMultipleSelection = NO;
    //    pickerV.showDelete = NO;
    _imgPicker.allowPickingVideo = NO;
    [self.contentView addSubview:_imgPicker];
}

@end
