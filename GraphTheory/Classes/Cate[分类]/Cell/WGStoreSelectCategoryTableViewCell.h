//
//  WGStoreSelectCategoryTableViewCell.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelectCategoryTableViewCell : DDBaseTableViewCell

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, strong) NSString *titleStr;


@end

NS_ASSUME_NONNULL_END
