//
//  YXPhotoCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LLImagePickerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXPhotoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (strong, nonatomic)LLImagePickerView *imgPicker;

@end

NS_ASSUME_NONNULL_END
