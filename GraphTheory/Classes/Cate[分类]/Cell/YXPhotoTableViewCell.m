//
//  YXPhotoTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPhotoTableViewCell.h"

@implementation YXPhotoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _imgPicker = [[LLImagePickerView alloc]initWithFrame:CGRectMake(10, 15, SCREEN_WIDTH-20-20, 70)];
        _imgPicker.type = LLImageTypePhoto;
        _imgPicker.maxImageSelected = 1;
        _imgPicker.allowMultipleSelection = NO;
        //    pickerV.showDelete = NO;
        _imgPicker.allowPickingVideo = NO;
        [self.contentView addSubview:self.imgPicker];
    }
    return self;
}

@end
