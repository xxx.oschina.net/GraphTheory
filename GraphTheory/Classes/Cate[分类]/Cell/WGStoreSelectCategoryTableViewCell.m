//
//  WGStoreSelectCategoryTableViewCell.m
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "WGStoreSelectCategoryTableViewCell.h"

@interface WGStoreSelectCategoryTableViewCell ()

@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UIImageView *selectImageView;

@end

@implementation WGStoreSelectCategoryTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style
                    reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        [self setUpLayout];
    }
    return self;
}
- (void)setIsSelect:(BOOL)isSelect{
    _isSelect = isSelect;
    self.backgroundColor = _isSelect?[UIColor whiteColor]:[UIColor clearColor];
    _selectImageView.image = _isSelect?[UIImage imageNamed:@"store_creat_Category_select"]:nil;
    
}
- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLab.text = _titleStr;
}
/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.selectImageView];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);
    }];
    [_selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(2);
    }];
}

- (UILabel *)titleLab{
    if (!_titleLab) {
        _titleLab = [UILabel new];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}
- (UIImageView *)selectImageView{
    if (!_selectImageView) {
        _selectImageView = [UIImageView new];
        _selectImageView.image = [UIImage imageNamed:@"store_creat_mutSelect"];
    }
    return _selectImageView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
