//
//  WGStoreSelectCategoryView.h
//  BusinessFine
//
//  Created by wanggang on 2019/9/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGStoreSelectCategoryView : UIView

@property (nonatomic, assign) NSInteger type;///<0:一级分类 1:二级分类
@property (nonatomic ,strong) NSArray *dataArr;
@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic, copy) void(^selectCategory)(NSInteger type,NSIndexPath *indexPath);



@end

NS_ASSUME_NONNULL_END
