//
//  YXCateModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/15.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCateCategorysModel : DDBaseModel

@property (nonatomic, copy) NSString *city_id;
@property (nonatomic, copy) NSString *city_name;
@property (nonatomic, strong) NSArray *categorys;

@end

@interface YXCateModel : DDBaseModel

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *imgbanner;
@property (nonatomic, copy) NSString *topid;
@property (nonatomic, copy) NSString *pid;
@property (nonatomic, copy) NSString *c_rc_district;
@property (nonatomic, copy) NSString *c_model;
@property (nonatomic, strong) NSArray *child;


@end

@interface YXCateChildModel : DDBaseModel

@property (nonatomic, copy) NSString *ctype_id;
@property (nonatomic, copy) NSString *ctype_rc_district;
@property (nonatomic, copy) NSString *ctype_name;
@property (nonatomic, copy) NSString *ctype_img;
@property (nonatomic, strong) NSArray *child;

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *c_ctype_id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *img;
@property (nonatomic, copy) NSString *rebate;

@property (nonatomic, assign) CGFloat cellHeight;

@property (nonatomic, assign) CGFloat cellHeight2;

@property (nonatomic, assign) BOOL selected;


@end


@interface YXTakeUsersUserInfoAddress : DDBaseModel
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXTakeUsersUserInfo : DDBaseModel
@property (strong,nonatomic) YXTakeUsersUserInfoAddress *address;

@end

@interface YXTakeUsers : DDBaseModel
@property (strong,nonatomic) YXTakeUsersUserInfo *userInfo;

@end


@interface YXCateDetailModel : DDBaseModel

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *c_model;
// 图片
@property (nonatomic, copy) NSString *img;
// 名称
@property (nonatomic, copy) NSString *name;
// 价格
@property (nonatomic, copy) NSString *c_price;
// 单位
@property (nonatomic, copy) NSString *c_unitname;
// 定价标准详情说明
@property (nonatomic, copy) NSString *c_contentprice;
// 描述信息
@property (nonatomic, copy) NSString *c_remarkimg;
@property (nonatomic, copy) NSString *c_remarka;
@property (nonatomic, copy) NSString *c_remarkb;
@property (nonatomic, copy) NSString *c_remarkc;
// 产品介绍
@property (nonatomic, copy) NSString *c_content;
// 备注提示
@property (nonatomic, copy) NSString *c_remarktips;
// 需求拍照上传
@property (nonatomic, copy) NSString *c_remarktitle;
// 满减金额
@property (nonatomic, copy) NSString *price_title;
// 优惠金额
@property (nonatomic, copy) NSString *c_coupons_price;
// 子分类信息
@property (nonatomic, strong) YXCateChildModel *typeInfo;
// 用户信息
@property (nonatomic, strong) NSArray *takeUsers;


/// 本地数据
@property (nonatomic, copy) NSArray *imgArr;
@property (nonatomic, copy) NSString *imgs;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lng;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *coupon_id;


@end







NS_ASSUME_NONNULL_END
