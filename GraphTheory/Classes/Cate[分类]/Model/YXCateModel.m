//
//  YXCateModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/15.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCateModel.h"

@implementation YXCateCategorysModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"categorys" : @"YXCateModel"};
}

@end

@implementation YXCateModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"child" : @"YXCateChildModel"};
}


@end

@implementation YXCateChildModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"child" : @"YXCateChildModel"};
}

- (CGFloat)cellHeight {
    _cellHeight = 40;
    NSInteger count = self.child.count / 4 ;
    if (self.child.count == 0 || self.child.count <=4) {
        _cellHeight += 80;
    }else {
        _cellHeight += 80;
        _cellHeight += 80 * count ;
    }
    return _cellHeight;
}

- (CGFloat)cellHeight2 {
    _cellHeight2= 50;
    NSInteger count = self.child.count / 4 ;
    if (self.child.count == 0 || self.child.count <=4) {
        _cellHeight2 += 50;
    }else {
        _cellHeight2 += 50;
        _cellHeight2 += 50 * count ;
    }
    return _cellHeight2;
}




@end

@implementation YXTakeUsersUserInfoAddress

@end

@implementation YXTakeUsersUserInfo

@end

@implementation YXTakeUsers

@end

@implementation YXCateDetailModel

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"takeUsers" : @"YXTakeUsers"};
}

@end


