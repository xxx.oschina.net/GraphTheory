//
//  DDSearchViewController.h
//  DDLife
//
//  Created by DinDo on 2020/12/21.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface DDSearchViewController : DDBaseViewController

@property (nonatomic, copy) NSString *searchHistoriesCachePath;
@property (nonatomic, copy) NSArray<id> *hotsSearch;
@property (nonatomic, copy) NSString *placeholder;
@property (nonatomic, copy) NSString *city_id;

@property (nonatomic ,copy)void(^didSearchBlock)(NSString *keyWord);
+ (void)saveSearch:(NSString *)search path:(NSString *)path;
@end

NS_ASSUME_NONNULL_END
