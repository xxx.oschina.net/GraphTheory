//
//  DDSearchViewController.m
//  DDLife
//
//  Created by DinDo on 2020/12/21.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import "DDSearchViewController.h"
#import "YXCateDetailViewController.h"
#import "YXOrderDetailsViewController.h"
#import "YXMediaDetailsTwoViewController.h"
#import "YXHomeOrderCollectionViewCell.h"
#import "YXMediaListCollectionViewCell.h"
#import "DDSearchCateCollectionViewCell.h"

#import "DDSearchCollectionViewCell.h"
#import "DDSearchCollectionReusableView.h"
#import "DDSearchView.h"
#import "UICollectionView+ARDynamicCacheHeightLayoutCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "YXHomeViewModel.h"
#import "YXHotKeyModel.h"
#import "YXSeachModel.h"
#import "YXShareOrdersModel.h"
#import "YXVideoModel.h"
@interface DDSearchViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) DDSearchView *searchView;
@property (nonatomic,strong) NSString *keyWord;
@property (nonatomic,strong) NSString *searhString;

@property (nonatomic, copy) NSMutableArray<NSString *> *searchHistories;
@property (nonatomic, strong)  YXSeachModel *searchModel;
@end

@implementation DDSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_searchHistoriesCachePath.length == 0) {
        _searchHistoriesCachePath = PYSEARCH_SEARCH_HISTORY_CACHE_PATH;
        DDWeakSelf
        [YXHomeViewModel queryHotRecKeywordCompletion:^(id  _Nonnull responesObj) {
            weakSelf.hotsSearch = [YXHotKeyModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"hotKeyword"]];
            [weakSelf.collectionView reloadData];
        } Failure:^(NSError * _Nonnull error) {
            
        }];
    }
    
    //    _hotsSearch = @[@"测试"];
    [self configurateNavigationBar];
    [self.view addSubview:self.collectionView];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _searchHistories = nil;
    [_collectionView reloadData];
}
- (void)beginSearch{
    if (!_keyWord.length) {
        if (_searhString.length) {
            _keyWord = _searhString;
            [self saveSearchCacheAndRefreshView:_keyWord];
        }
        self.searchModel = nil;
        [self.collectionView reloadData];
    }else{
        [self saveSearchCacheAndRefreshView:_keyWord];
        DDWeakSelf
        [YXHomeViewModel querySearchWithCity_id:self.city_id keyword:_keyWord Completion:^(id  _Nonnull responesObj) {
            if (REQUESTDATASUCCESS) {
                weakSelf.searchModel = [YXSeachModel mj_objectWithKeyValues:responesObj[@"data"]];
            }
            [weakSelf.collectionView reloadData];
        } Failure:^(NSError * _Nonnull error) {
            
        }];
    }
    
}
- (void)emptySearchHistoryDidClick
{
    [self.searchHistories removeAllObjects];
    [NSKeyedArchiver archiveRootObject:self.searchHistories toFile:self.searchHistoriesCachePath];
    [self.collectionView reloadData];
}
- (void)saveSearchCacheAndRefreshView:(NSString *)search
{
    NSString *searchText = search;
    if (searchText.length > 0) {
        [self.searchHistories removeObject:searchText];
        [self.searchHistories insertObject:searchText atIndex:0];
        [NSKeyedArchiver archiveRootObject:self.searchHistories toFile:self.searchHistoriesCachePath];
        [self.collectionView reloadData];
    }
}
+ (void)saveSearch:(NSString *)search path:(NSString *)path{
    NSMutableArray *allSearch = [[NSMutableArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithFile:path]];
    if (search.length > 0) {
        [allSearch removeObject:search];
        [allSearch insertObject:search atIndex:0];
        [NSKeyedArchiver archiveRootObject:allSearch toFile:path];
    }
}
#pragma mark -- collectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.searchModel) {
        return 3;
    }else {
        return 2;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.searchModel) {
        if (section == 0) {
            return self.searchModel.categories.list.count;
        }else if (section == 1) {
            return self.searchModel.shareOrders.list.count;
        }else {
            return self.searchModel.videoes.list.count;
        }
    }else {
        if (section == 0) {
            return self.searchHistories.count;
        }else{
            return _hotsSearch.count;
        }
    }
  
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchModel) {
        if (indexPath.section == 0) {
            return CGSizeMake(KWIDTH , 50);
        }else if (indexPath.section == 1) {
            return CGSizeMake((SCREEN_WIDTH -45) / 2, 220);
        }else {
            return CGSizeMake((SCREEN_WIDTH -45) / 2, 250);
        }
    }else {
        DDWeakSelf
        return [collectionView ar_sizeForCellWithIdentifier:@"DDSearchCollectionViewCell" indexPath:indexPath configuration:^(__kindof DDSearchCollectionViewCell *cell) {
            if (indexPath.section == 0) {
                cell.tagString = weakSelf.searchHistories[indexPath.row];
            }else{
                YXHotKeyModel *model = weakSelf.hotsSearch[indexPath.row];
                cell.tagString = model.name;
            }
            cell.type = indexPath.section;
        }];
    }
 
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 18;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 12;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (self.searchModel) {
        return CGSizeZero;
    }else {
        if (section == 0 && _searchHistories.count) {
            return CGSizeMake(SCREEN_WIDTH, 47);
        }else if(section == 1 && _hotsSearch.count){
            return CGSizeMake(SCREEN_WIDTH, 47);
        }
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.searchModel) {
        if (section == 0) {
            return UIEdgeInsetsMake(0, 0, 0, 0);
        }else if (section ==1) {
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }else {
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }
    }else {
        if (section == 0 && _searchHistories.count) {
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }else if(section == 1 && _hotsSearch.count){
            return UIEdgeInsetsMake(0, 15, 15, 15);
        }
    }
    
    return UIEdgeInsetsZero;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchModel) {
        if (indexPath.section == 0) {
            DDSearchCateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDSearchCateCollectionViewCell" forIndexPath:indexPath];
            YXSeachModelCategoriesList *model = self.searchModel.categories.list[indexPath.row];
            cell.titleLab.text = model.name;
            return cell;
        }else if (indexPath.section == 1) {
            YXHomeOrderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell" forIndexPath:indexPath];
            YXShareOrdersModelList *model = self.searchModel.shareOrders.list[indexPath.row];
            cell.listModel = model;
            return cell;
        }else {
            YXMediaListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMediaListCollectionViewCell" forIndexPath:indexPath];
            YXVideoModel *model = self.searchModel.videoes.list[indexPath.row];
            cell.model = model;
            return cell;
        }
    }else {
        DDSearchCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDSearchCollectionViewCell" forIndexPath:indexPath];
        if (indexPath.section == 0) {
            cell.tagString = _searchHistories[indexPath.row];
            cell.type = indexPath.section;
        }else{
            YXHotKeyModel *model = _hotsSearch[indexPath.row];
            cell.tagString = model.name;
            cell.type = 0;
        }
        return cell;
    };
    return [UICollectionViewCell new];
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.searchModel) {
        if (indexPath.section == 0) {
            YXSeachModelCategoriesList *model = self.searchModel.categories.list[indexPath.row];
            YXCateDetailViewController *vc = [YXCateDetailViewController new];
            vc.cate_id = model.pid;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.section == 1) {
            YXShareOrdersModelList *model = self.searchModel.shareOrders.list[indexPath.row];
            YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
            vc.Id = model.Id;
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            YXVideoModel *model = self.searchModel.videoes.list[indexPath.row];
            YXMediaDetailsTwoViewController *vc = [YXMediaDetailsTwoViewController new];
            vc.Id = model.v_id;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else {
        if (indexPath.section == 0) {
            _keyWord = _searchHistories[indexPath.row];
            [self beginSearch];
            _keyWord = @"";
        }else{
            YXHotKeyModel *model = _hotsSearch[indexPath.row];
            YXCateDetailViewController *vc = [YXCateDetailViewController new];
            vc.cate_id = model.pid;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
   

}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        DDWeakSelf
        DDSearchCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDSearchCollectionReusableView" forIndexPath:indexPath];
        view.type = indexPath.section;
        view.didDelectBlock = ^{
            [weakSelf emptySearchHistoryDidClick];
        };
        return view;
    }
    return nil;
}
- (void)configurateNavigationBar{
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(0, 0, 44, 44);
    searchBtn.contentMode = UIViewContentModeRight;
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [searchBtn setTitleColor:color_TextTwo forState:UIControlStateNormal];
    [searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [searchBtn addTarget:self
                  action:@selector(beginSearch)
        forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
    self.navigationItem.titleView = self.searchView;
}
- (NSMutableArray *)searchHistories{
    if (!_searchHistories) {
        _searchHistories = [NSMutableArray new];
        [_searchHistories addObjectsFromArray:[NSKeyedUnarchiver unarchiveObjectWithFile:self.searchHistoriesCachePath]];
    }
    return _searchHistories;
}
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewLeftAlignedLayout * flowLayout = [[UICollectionViewLeftAlignedLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[DDSearchCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDSearchCollectionReusableView"];
        [_collectionView registerClass:[DDSearchCollectionViewCell class] forCellWithReuseIdentifier:@"DDSearchCollectionViewCell"];
        [_collectionView registerClass:[YXHomeOrderCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeOrderCollectionViewCell"];
        [_collectionView registerClass:[YXMediaListCollectionViewCell class] forCellWithReuseIdentifier:@"YXMediaListCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"DDSearchCateCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"DDSearchCateCollectionViewCell"];
        
    }
    return _collectionView;
}
- (DDSearchView *)searchView{
    if (!_searchView) {
        DDWeakSelf
        _searchView = [[DDSearchView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 100, 44)];
        _searchView.placeholder = _placeholder;
        _searchView.searchBackColor = HEX_COLOR(@"#FFFFFF");
        _searchView.editoring = ^(NSString * _Nonnull search) {
            weakSelf.keyWord = search;
        };
        _searchView.searchDone = ^(NSString * _Nonnull search) {
            [weakSelf beginSearch];
        };
    }
    return _searchView;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
