//
//  DDSearchCateCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDSearchCateCollectionViewCell.h"

@implementation DDSearchCateCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.titleLab.font = [UIFont boldSystemFontOfSize:15.0];
}

@end
