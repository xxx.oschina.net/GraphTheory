//
//  DDSearchView.m
//  DDLife
//
//  Created by DinDo on 2020/6/5.
//  Copyright © 2020 点都. All rights reserved.
//

#import "DDSearchView.h"
#import "LTLimitTextField.h"

@interface DDSearchView ()<UITextFieldDelegate>

@property (nonatomic ,strong) UIImageView *searchIcon;
@property (nonatomic ,strong) LTLimitTextField *searchField;

@end

@implementation DDSearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpLayout];
    }
    return self;
}
- (void)setUpLayout{
    [self addSubview:self.backView];
    [self.backView addSubview:self.searchIcon];
    [self.backView addSubview:self.searchField];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
        make.height.equalTo(@32);
//        make.size.mas_equalTo(CGSizeMake(275, 32));
    }];
    [self.searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backView.mas_right).offset(-10);
        make.centerY.equalTo(self.backView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    [self.searchField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(10);
        make.right.equalTo(self.searchIcon.mas_left).offset(-15);
        make.top.bottom.equalTo(self.backView);
    }];
}
- (void)setSearchBackColor:(UIColor *)searchBackColor{
    _searchBackColor = searchBackColor;
    _backView.backgroundColor = searchBackColor;
}
- (void)setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    _searchField.placeholder = placeholder;
}
- (void)setEnabled:(BOOL)enabled{
    _enabled = enabled;
    _searchField.userInteractionEnabled = enabled;
}
- (void)tapSearchBack{
    if (self.clickSearchViewBlock) {
        self.clickSearchViewBlock();
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if (self.searchDone) {
        self.searchDone(textField.text);
    }
    return YES;
}
- (void)closeKeyBorad{
    [_searchField resignFirstResponder];
}
- (void)textFieldDidChange:(UITextField *)textField
{
    if (self.editoring) {
        self.editoring(textField.text);
    }
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = HEX_COLOR(@"#F3F3F3");
        _backView.layer.cornerRadius = 4;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapSearchBack)];
        [_backView addGestureRecognizer:tap];
    }
    return _backView;
}
- (UIImageView *)searchIcon{
    if (!_searchIcon) {
        _searchIcon = [UIImageView new];
        _searchIcon.image = [UIImage imageNamed:@"search_707070"];
    }
    return _searchIcon;
}
- (LTLimitTextField *)searchField{
    if (!_searchField) {
        _searchField = [[LTLimitTextField alloc]init];
        _searchField.keyboardType = UIKeyboardTypeDefault;
        _searchField.returnKeyType = UIReturnKeySearch;
        _searchField.textAlignment = NSTextAlignmentLeft;
        _searchField.font = [UIFont systemFontOfSize:16];
        [_searchField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        _searchField.delegate = self;

    }
    return _searchField;
}
- (CGSize)intrinsicContentSize {
    return UILayoutFittingExpandedSize;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
