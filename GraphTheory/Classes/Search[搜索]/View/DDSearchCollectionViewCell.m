//
//  DDSearchCollectionViewCell.m
//  DDLife
//
//  Created by DinDo on 2020/12/21.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import "DDSearchCollectionViewCell.h"

@interface DDSearchCollectionViewCell ()

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIView *tagView;
@property (nonatomic ,strong) UILabel *tagLab;
@property (nonatomic ,strong) UIImageView *hotImageView;

@end

@implementation DDSearchCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setTagString:(NSString *)tagString{
    _tagString = tagString;
    _tagLab.text = _tagString;
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 1) {
        [self.hotImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.tagView.mas_left);
            make.centerY.equalTo(self.tagView.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(10, 13));
        }];
        [self.tagLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.and.right.equalTo(self.tagView);
            make.left.equalTo(self.hotImageView.mas_right).offset(9);
            make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH - 30 - 35 - 30);

        }];
    }else{
        [self.hotImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeZero);
        }];
        [self.tagLab mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.and.right.equalTo(self.tagView);
            make.left.equalTo(self.tagView.mas_left);
            make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH - 30 - 35 - 30);
        }];

    }
}
- (void)addLayout {
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.tagView];
    [self.tagView addSubview:self.tagLab];
    [self.tagView addSubview:self.hotImageView];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.tagView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.backView).mas_offset(UIEdgeInsetsMake(8, 17, 8, 17));
    }];
    [self.hotImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.tagView.mas_left);
        make.centerY.equalTo(self.tagView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(10, 13));
        
    }];
    [self.tagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.and.right.equalTo(self.tagView);
        make.left.equalTo(self.hotImageView.mas_right).offset(9);
        make.width.mas_lessThanOrEqualTo(SCREEN_WIDTH - 30 - 35 - 30);

    }];
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = HEX_COLOR(@"#F4F4F4");
        _backView.layer.cornerRadius = 4;
    }
    return _backView;
}
- (UIView *)tagView{
    if (!_tagView) {
        _tagView = [UIView new];
    }
    return _tagView;
}
- (UILabel *)tagLab{
    if (!_tagLab) {
        _tagLab = [UILabel new];
        _tagLab.textColor = HEX_COLOR(@"#656565");
        _tagLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _tagLab;
}
- (UIImageView *)hotImageView{
    if (!_hotImageView) {
        _hotImageView = [UIImageView new];
//        _hotImageView.image = UIImageMake(@"search_hot");
        _hotImageView.clipsToBounds = YES;
    }
    return _hotImageView;
}
@end
