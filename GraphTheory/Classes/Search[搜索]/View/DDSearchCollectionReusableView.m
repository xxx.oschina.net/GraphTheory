//
//  DDSearchCollectionReusableView.m
//  DDLife
//
//  Created by DinDo on 2020/12/21.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import "DDSearchCollectionReusableView.h"

@interface DDSearchCollectionReusableView ()

@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) UIView *lineView;
@property (nonatomic ,strong) UILabel *tagLab;
@property (nonatomic ,strong) UIButton *delectBtn;

@end

@implementation DDSearchCollectionReusableView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addLayout];
    }
    return self;
}
- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 1) {
        _tagLab.text = @"推荐搜索";
        _delectBtn.hidden = YES;
    }else{
        _tagLab.text = @"热门搜索";
        _delectBtn.hidden = NO;
    }
}
- (void)delect{
    if (self.didDelectBlock) {
        self.didDelectBlock();
    }
}
- (void)addLayout {
    [self addSubview:self.backView];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.tagLab];
    [self.backView addSubview:self.delectBtn];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(2, 10));
        make.centerY.equalTo(self.backView.mas_centerY);
    }];
    [self.tagLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.lineView.mas_right).offset(7);
        make.centerY.equalTo(self.lineView.mas_centerY);
    }];
    [self.delectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.right.equalTo(self.backView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(27, 27));
    }];
    
}
- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.layer.cornerRadius = 4;
    }
    return _backView;
}
- (UIView *)lineView{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = APPTintColor;
    }
    return _lineView;
}
- (UILabel *)tagLab{
    if (!_tagLab) {
        _tagLab = [UILabel new];
        _tagLab.textColor = color_TextOne;
        _tagLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _tagLab;
}
- (UIButton *)delectBtn{
    if (!_delectBtn ) {
        _delectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_delectBtn setImage:[UIImage imageNamed:@"del"] forState:UIControlStateNormal];
        [_delectBtn addTarget:self action:@selector(delect) forControlEvents:UIControlEventTouchUpInside];
    }
    return _delectBtn;
}
@end

