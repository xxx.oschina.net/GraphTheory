//
//  DDSearchView.h
//  DDLife
//
//  Created by DinDo on 2020/6/5.
//  Copyright © 2020 点都. All rights reserved.
//

#import "DDBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DDSearchView : DDBaseView
@property (nonatomic ,strong) UIView *backView;
@property (nonatomic ,strong) NSString *placeholder;
@property (nonatomic ,strong) UIColor *searchBackColor;
@property (nonatomic ,assign) BOOL enabled;
@property (nonatomic ,copy)void(^clickSearchViewBlock)(void);
@property (nonatomic ,copy)void(^editoring)(NSString *search);
@property (nonatomic ,copy)void(^searchDone)(NSString *search);

- (void)closeKeyBorad;

@end

NS_ASSUME_NONNULL_END
