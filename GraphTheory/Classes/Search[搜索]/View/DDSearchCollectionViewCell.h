//
//  DDSearchCollectionViewCell.h
//  DDLife
//
//  Created by DinDo on 2020/12/21.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DDSearchCollectionViewCell : UICollectionViewCell
@property (nonatomic ,strong) NSString *tagString;
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
