//  Created by li qiao robot 
#import <Foundation/Foundation.h>
@interface YXSeachModelCategoriesListTopCate : NSObject
@property (copy,nonatomic)   NSString *img;
@property (copy,nonatomic)   NSString *topid;
@property (copy,nonatomic)   NSString *c_rc_district;
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *subtitle;
@property (copy,nonatomic)   NSString *imgbanner;
@property (copy,nonatomic)   NSString *c_model;
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXSeachModelCategoriesList : NSObject
@property (copy,nonatomic)   NSString *rebate;
@property (copy,nonatomic)   NSString *img;
@property (copy,nonatomic)   NSString *subtitle;
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *c_ctype_id;
@property (strong,nonatomic) YXSeachModelCategoriesListTopCate *topCate;
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXSeachModelCategories : NSObject
@property (strong,nonatomic) NSNumber *totalPage;
@property (strong,nonatomic) NSNumber *pageLimit;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *page;
@end

@interface YXSeachModelShareOrdersListUserInfo : NSObject
@property (copy,nonatomic)   NSString *email;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXSeachModelShareOrdersListReceiver : NSObject
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *province;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *city;
@property (copy,nonatomic)   NSString *region;
@property (copy,nonatomic)   NSString *username;
@property (copy,nonatomic)   NSString *is_default;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *userphone;
@end

@interface YXSeachModelShareOrdersListFrom_address : NSObject
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXSeachModelShareOrdersListProduct_detailsAddress : NSObject
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXSeachModelShareOrdersListProduct_details : NSObject
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *cate_id;
@property (copy,nonatomic)   NSString *imgs;
@property (copy,nonatomic)   NSString *content;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *belongMode;
@property (copy,nonatomic)   NSString *c_address;
@property (strong,nonatomic) NSNumber *coupon_id;
@property (copy,nonatomic)   NSString *c_pass;
@property (strong,nonatomic) YXSeachModelShareOrdersListProduct_detailsAddress *address;
@property (strong,nonatomic) NSNumber *number;
@property (copy,nonatomic)   NSString *rebate;
@property (copy,nonatomic)   NSString *remark;
@property (copy,nonatomic)   NSString *c_studyprice;
@property (strong,nonatomic) NSNumber *coupon_price;
@property (copy,nonatomic)   NSString *service_city_id;
@property (copy,nonatomic)   NSString *c_model;
@end

@interface YXSeachModelShareOrdersList : NSObject
@property (copy,nonatomic)   NSString *c_model;
@property (copy,nonatomic)   NSString *coupon_code;
@property (strong,nonatomic) YXSeachModelShareOrdersListProduct_details *product_details;
@property (copy,nonatomic)   NSString *belongmode;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *m_updatetime;
@property (copy,nonatomic)   NSString *complain;
@property (copy,nonatomic)   NSString *statusTitle;
@property (copy,nonatomic)   NSString *yiyimoney;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *remark;
@property (copy,nonatomic)   NSString *is_overdue;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *answer_desc;
@property (copy,nonatomic)   NSString *share_m_id;
@property (copy,nonatomic)   NSString *answer;
@property (copy,nonatomic)   NSString *solve;
@property (strong,nonatomic) YXSeachModelShareOrdersListFrom_address *from_address;
@property (copy,nonatomic)   NSString *service_city_id;
@property (strong,nonatomic) NSArray *imgs;
@property (copy,nonatomic)   NSString *c_pass;
@property (copy,nonatomic)   NSString *share_shenhe_time;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *complete_time;
@property (copy,nonatomic)   NSString *is_share;
@property (copy,nonatomic)   NSString *pay_price;
@property (copy,nonatomic)   NSString *take_user_id;
@property (copy,nonatomic)   NSString *order_id;
@property (copy,nonatomic)   NSString *finish_time;
@property (strong,nonatomic) YXSeachModelShareOrdersListReceiver *receiver;
@property (copy,nonatomic)   NSString *coupon_price;
@property (copy,nonatomic)   NSString *take_price;
@property (copy,nonatomic)   NSString *status;
@property (copy,nonatomic)   NSString *c_address;
@property (copy,nonatomic)   NSString *solve_time;
@property (copy,nonatomic)   NSString *product_icon;
@property (copy,nonatomic)   NSString *order_no;
@property (copy,nonatomic)   NSString *pay_time;
@property (copy,nonatomic)   NSString *share_time;
@property (copy,nonatomic)   NSString *content;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *coupon_id;
@property (copy,nonatomic)   NSString *share_check;
@property (copy,nonatomic)   NSString *is_complain;
@property (copy,nonatomic)   NSString *complain_time;
@property (copy,nonatomic)   NSString *order_type;
@property (copy,nonatomic)   NSString *pay_type;
@property (copy,nonatomic)   NSString *delete_time;
@property (copy,nonatomic)   NSString *take_time;
@property (copy,nonatomic)   NSString *m_id;
@property (strong,nonatomic) YXSeachModelShareOrdersListUserInfo *userInfo;
@end

@interface YXSeachModelShareOrders : NSObject
@property (strong,nonatomic) NSNumber *totalPage;
@property (strong,nonatomic) NSNumber *pageLimit;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *page;
@end

@interface YXSeachModelVideoesList : NSObject
@property (strong,nonatomic) NSNumber *totalPage;
@property (strong,nonatomic) NSNumber *pageLimit;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *page;
@end

@interface YXSeachModelVideoes : NSObject
@property (strong,nonatomic) NSNumber *page;
@property (strong,nonatomic) NSNumber *limit;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSNumber *pageCount;
@end

@interface YXSeachModel : NSObject
@property (strong,nonatomic) YXSeachModelVideoes *videoes;
@property (strong,nonatomic) YXSeachModelShareOrders *shareOrders;
@property (strong,nonatomic) YXSeachModelCategories *categories;
@end

