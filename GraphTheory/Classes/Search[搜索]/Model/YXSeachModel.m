//  Created by li qiao robot 
#import "YXSeachModel.h" 
@implementation YXSeachModelCategoriesListTopCate
@end

@implementation YXSeachModelCategoriesList
@end

@implementation YXSeachModelCategories
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"list" : [YXSeachModelCategoriesList class] }; 
}
@end

@implementation YXSeachModelShareOrdersListUserInfo
@end

@implementation YXSeachModelShareOrdersListReceiver
@end

@implementation YXSeachModelShareOrdersListFrom_address
@end

@implementation YXSeachModelShareOrdersListProduct_detailsAddress
@end

@implementation YXSeachModelShareOrdersListProduct_details
@end

@implementation YXSeachModelShareOrdersList
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"imgs" : [NSString class] }; 
}
@end

@implementation YXSeachModelShareOrders
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"list" : [YXSeachModelShareOrdersList class] }; 
}
@end

@implementation YXSeachModelVideoesList
@end

@implementation YXSeachModelVideoes
+(NSDictionary*)mj_objectClassInArray{
       return @{ @"list" : [YXSeachModelVideoesList class] }; 
}
@end

@implementation YXSeachModel
@end

