//
//  YXCollectionVideoTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCollectionVideoTableViewCell.h"
#import "YXCollectionModel.h"
@interface YXCollectionVideoTableViewCell ()

/** 图片*/
@property (nonatomic ,strong) UIImageView *imgView;
/** 名称*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 播放次数*/
@property (nonatomic ,strong) UILabel *playLab;
/** 时间*/
@property (nonatomic ,strong) UILabel *timeLab;
@end

@implementation YXCollectionVideoTableViewCell

- (void)setModel:(YXCollectionModel *)model {
    _model = model;
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.videoInfo.v_cover_img]];
    _nameLab.text = _model.videoInfo.v_name;
    _playLab.text = [NSString stringWithFormat:@"播放：%@次",_model.videoInfo.view_count];
    _timeLab.text = _model.videoInfo.v_time;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.playLab];
        [self.contentView addSubview:self.timeLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
    }
    return _imgView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)playLab {
    if (!_playLab) {
        _playLab = [[UILabel alloc] init];
        _playLab.textColor = color_TextTwo;
        _playLab.text = @"已禁用";
        _playLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _playLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.font = [UIFont systemFontOfSize:12.0f];
        _timeLab.text = @"客服";
        _timeLab.textColor = color_TextTwo;
    }
    return _timeLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@10);
        make.top.equalTo(@15);
        make.bottom.equalTo(@-15);
        make.width.equalTo(@120);
    }];
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.imgView.mas_top).offset(10);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_playLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.playLab sizeToFit];
    }];

    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.imgView.mas_bottom).offset(-10);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.timeLab sizeToFit];
    }];

}


@end
