//
//  YXCollectionVideoListTableView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCollectionVideoListTableView : UITableView

/**
 回调点击cell方法
 */
@property (nonatomic ,copy)void(^clickSelectIndex)(NSInteger index);

/**
 回调侧滑删除方法
 */
@property (nonatomic ,copy)void(^clickDeleteIndex)(NSInteger index);


/**
 数据源
 */
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

NS_ASSUME_NONNULL_END
