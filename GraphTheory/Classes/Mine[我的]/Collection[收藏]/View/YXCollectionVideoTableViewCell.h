//
//  YXCollectionVideoTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCollectionModel;
@interface YXCollectionVideoTableViewCell : UITableViewCell

@property (nonatomic ,strong) YXCollectionModel *model;

@end

NS_ASSUME_NONNULL_END
