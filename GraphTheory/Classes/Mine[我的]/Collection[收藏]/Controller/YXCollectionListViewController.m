//
//  YXCollectionListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCollectionListViewController.h"
#import "YXMediaDetailsViewController.h"
#import "YXMediaDetailsTwoViewController.h"

#import "UIScrollView+DREmptyDataSet.h"
#import "YXCollectionVideoListTableView.h"
#import "YXUserViewModel.h"
#import "YXCollectionModel.h"
#import "YXVideoViewModel.h"
@interface YXCollectionListViewController ()

@property (nonatomic ,strong) YXCollectionVideoListTableView *tableView;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@end

@implementation YXCollectionListViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 加载刷新
    if ([self.status isEqualToString:@"0"]) {
        
    }else {
        [self addRefrsh];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.page = 1;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.offset(0);
        }
    }];
}

- (void)addRefrsh {
    
    __weak typeof(self)weakSelf = self;
    _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf loadData];
    }];
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
    [_tableView.mj_header beginRefreshing];
    
}

#pragma mark - 网络请求
- (void)loadData {
    
    YXWeakSelf
    if ([self.status isEqualToString:@"0"]) {
        
    }else {
        [YJProgressHUD showLoading:@"加载中"];
        [YXUserViewModel queryMyCollectVideoListWithPage:@(_page).stringValue Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
            }
            if (REQUESTDATASUCCESS) {
                weakSelf.page++;
                NSArray *data = [YXCollectionModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
                [weakSelf.dataArr addObjectsFromArray:data];
                if (data.count < 10) {
                    [weakSelf.tableView.mj_footer setHidden:YES];
                }else{
                    [weakSelf.tableView.mj_footer setHidden:NO];
                }
                weakSelf.tableView.dataArr = weakSelf.dataArr;
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
        } failure:^(NSError *error) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }

}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


- (YXCollectionVideoListTableView *)tableView {
    if (!_tableView) {
        _tableView = [[YXCollectionVideoListTableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStylePlain)];
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{
            
        }];
        YXWeakSelf
        [_tableView setClickSelectIndex:^(NSInteger index) {
            YXMediaDetailsTwoViewController *vc = [[YXMediaDetailsTwoViewController alloc] init];
            YXCollectionModel *model = weakSelf.dataArr[index];
            vc.Id = model.video_id;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }];
        
        [_tableView setClickDeleteIndex:^(NSInteger index) {
            YXCollectionModel *model = weakSelf.dataArr[index];
            [YXVideoViewModel queryCollectVideoWithtype:@"0" video_id:model.videoInfo.v_id Completion:^(id  _Nonnull responesObj) {
                if (REQUESTDATASUCCESS) {
                    weakSelf.page = 1;
                    [weakSelf loadData];
                }
            } failure:^(NSError * _Nonnull error) {
                
            }];
        }];
    }
    return _tableView;
}

- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
