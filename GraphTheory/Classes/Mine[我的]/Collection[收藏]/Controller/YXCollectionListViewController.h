//
//  YXCollectionListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCollectionListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>

///<0 : 创业者列表 1：视频列表
@property (nonatomic ,strong) NSString *status;

@end

NS_ASSUME_NONNULL_END
