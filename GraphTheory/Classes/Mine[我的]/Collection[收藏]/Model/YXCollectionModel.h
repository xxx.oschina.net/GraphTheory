//
//  YXCollectionModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"
#import "YXVideoModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface YXCollectionModel : DDBaseModel

@property (nonatomic ,strong) NSString *video_id;

@property (nonatomic ,strong) YXVideoModel *videoInfo;

@end

NS_ASSUME_NONNULL_END
