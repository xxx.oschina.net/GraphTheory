//
//  YXPublishModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXPublishModel : DDBaseModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *Top_category;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *category_name;
@property (nonatomic, strong) NSString *cover_img;
@property (nonatomic, strong) UIImage *fileImg;
@property (nonatomic, strong) NSString *introduce;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSString *videoPath;
@property (nonatomic, strong) UIImage *videoImg;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *goods_id;
@property (nonatomic, strong) NSString *goods_name;

@end

NS_ASSUME_NONNULL_END
