//
//  YXEnterModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/28.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXEnterModel : DDBaseModel

@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *cardID;
@property (nonatomic, strong) NSString *images;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *cityName;
@property (nonatomic, strong) NSString *service_city_ids;
@property (nonatomic, strong) NSString *service_ids;
@property (nonatomic, strong) NSString *service_name;
@property (nonatomic, assign) BOOL agree;

@end

NS_ASSUME_NONNULL_END
