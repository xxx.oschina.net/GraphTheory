//
//  YXLocationViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXLocationViewController.h"
#import "YXAMapManager.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
@interface YXLocationViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,AMapLocationManagerDelegate,AMapSearchDelegate>
@property (nonatomic ,strong) UIView *searchView;
@property (nonatomic,strong) UITextField *searchTxt;//搜索框
@property (nonatomic ,strong) UIView *lineView;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic,assign) BOOL isSearchAround;
@property (nonatomic,strong) NSMutableArray *addressArray;//搜索到的地址数组
@property (nonatomic,strong) AMapLocationManager *locationManager;//定位管理
@property (nonatomic,strong) AMapSearchAPI *searchAPI;//搜索类
@property (nonatomic,strong) CLLocation *location;//当前定位的位置
@property (nonatomic,copy) NSString *currentCity;//当前城市
@property (nonatomic,copy) NSString *district;//当前区

@end

@implementation YXLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"定位信息";

    [self loadSubViews];
    
    //开始定位
    [self.locationManager startUpdatingLocation];

}

- (void)loadSubViews {
    
    [self.view addSubview:self.searchView];
    [self.searchView addSubview:self.searchTxt];
    [self.searchView addSubview:self.lineView];
    [self.view addSubview:self.tableView];
    [self.searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.top.right.equalTo(self.view);
        }
        make.height.equalTo(@50);
    }];
    [self.searchTxt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchView.mas_left).offset(15);
        make.right.equalTo(self.searchView.mas_right).offset(-15);
        make.top.bottom.equalTo(self.searchView);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.searchView);
        make.height.equalTo(@1);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.bottom.right.equalTo(self.view);
        }
        make.top.equalTo(self.searchView.mas_bottom);
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.addressArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"YXCityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.textLabel.textColor = color_TextOne;
    if (self.isSearchAround == YES) {//POI周边搜索
        AMapPOI *poi = self.addressArray[indexPath.row];
        cell.textLabel.text = poi.name;
        cell.detailTextLabel.text = poi.address;
    }else{//输入提示搜索
        AMapTip *tip = self.addressArray[indexPath.row];
        cell.textLabel.text = tip.name;
        cell.detailTextLabel.text = tip.address;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (self.isSearchAround == YES) {//POI周边搜索
        AMapPOI *poi = self.addressArray[indexPath.row];
        if (self.type == 1) {
            if (self.selectAddressPOIBlock) {
                self.selectAddressPOIBlock(poi.name, poi.address,poi.location.latitude,poi.location.longitude);
            }
        }else {
            NSString *addrss = [NSString stringWithFormat:@"%@%@%@",self.currentCity,self.district,poi.name];
            if (self.selectAddressBlock) {
                self.selectAddressBlock(addrss);
            }
        }
    }else{//输入提示搜索
        AMapTip *tip = self.addressArray[indexPath.row];
        if (self.type == 1) {
            if (self.selectAddressPOIBlock) {
                self.selectAddressPOIBlock(tip.name, tip.address,tip.location.latitude,tip.location.longitude);
            }
        }else {
            NSString *addrss = [NSString stringWithFormat:@"%@%@%@",self.currentCity,self.district,tip.name];
            if (self.selectAddressBlock) {
                self.selectAddressBlock(addrss);
            }
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - AMapLocationManagerDelegate
 
/* 当定位发生错误时，会调用代理的此方法 */
- (void)amapLocationManager:(AMapLocationManager *)manager didFailWithError:(NSError *)error
{
    //定位错误
    NSLog(@"定位❌error = %@",error);
}
 
- (void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager *)locationManager {    
    [locationManager requestAlwaysAuthorization];
}

/* 位置更新回调 */
- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location
{
    //经纬度
    NSLog(@"location:{lat:%f; lon:%f}", location.coordinate.latitude, location.coordinate.longitude)
    self.location = location;
    
    //逆地理编码
    [self reGoecodeWithLocation:location];
    
    //发起周边搜索
    [self searchAroundWithKeywords:nil];
    
    //停止定位
    [self.locationManager stopUpdatingLocation];
}
 
//逆地理编码
- (void)reGoecodeWithLocation:(CLLocation *)location
{
    AMapReGeocodeSearchRequest *request = [[AMapReGeocodeSearchRequest alloc] init];
    request.location =[AMapGeoPoint locationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    [self.searchAPI AMapReGoecodeSearch:request];
}
 
//根据定位坐标或关键字进行周边搜索
- (void)searchAroundWithKeywords:(NSString *)keywords{
    if (keywords.length == 0) { //未输入关键字，则为POI周边搜索
        self.isSearchAround = YES;
        //构造AMapPOIAroundSearchRequest对象，设置周边搜索参数
        AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
        request.location = [AMapGeoPoint locationWithLatitude:self.location.coordinate.latitude longitude:self.location.coordinate.longitude];
        //types属性表示限定搜索POI的类别，默认为：餐饮服务|商务住宅|生活服务
        //POI的类型共分为20种大类别
        request.types = @"汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施";
        
        request.sortrule = 0;
        request.requireExtension = YES;
        //发起周边搜索请求
        [self.searchAPI AMapPOIAroundSearch:request];
    }else{ //输入了关键字，则为搜索提示请求
        self.isSearchAround = NO;
        AMapInputTipsSearchRequest *tipsRequest = [[AMapInputTipsSearchRequest alloc] init];
        tipsRequest.city = self.currentCity;//查询城市默认为当前定位的城市
        tipsRequest.keywords = keywords;
        [self.searchAPI AMapInputTipsSearch:tipsRequest];
    }
}
 
 
#pragma mark - AMapSearchDelegate
 
/* 逆地理编码查询回调函数 */
- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    self.currentCity = response.regeocode.addressComponent.city;
    self.district = response.regeocode.addressComponent.district;
    if (self.currentCity.length == 0) {
        self.currentCity = response.regeocode.addressComponent.province;
    }
    NSLog(@"当前定位城市 = %@",self.currentCity);
}
 
/* 实现POI搜索对应的回调函数 */
- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    [self.addressArray removeAllObjects];
    if (response.pois.count>0) {
        //将搜索出的POI结果保存到数组
        [self.addressArray addObjectsFromArray:response.pois];
    }else{
//        [self.mainTableView tableViewShowMessage:@"无结果" forDataCount:0];
    }
    [self.tableView reloadData];
}
 
/* 输入提示查询回调函数 */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response
{
    [self.addressArray removeAllObjects];
    if (response.tips.count>0) {
        //将搜索出的结果保存到数组
        [self.addressArray addObjectsFromArray:response.tips];
    }else{
//        [self.tableView tableViewShowMessage:@"无结果" forDataCount:0];
    }
    [self.tableView reloadData];
}


#pragma mark - 监听textField值改变
- (void)textFieldValueChanged:(UITextField *)textField
{
    [self searchAroundWithKeywords:textField.text];
}
 
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Lazy loading
- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [UIView new];
    }
    return _searchView;
}
- (UITextField *)searchTxt {
    if (!_searchTxt) {
        _searchTxt = [[UITextField alloc] initWithFrame:CGRectZero];
        _searchTxt.placeholder = @"请输入地址关键字搜索";
        _searchTxt.delegate = self;
        _searchTxt.font = [UIFont systemFontOfSize:14.0];
        _searchTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_searchTxt addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _searchTxt;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorColor = color_LineColor;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}

- (NSMutableArray *)addressArray
{
   if (!_addressArray) {
       _addressArray = [NSMutableArray array];
   }
   return _addressArray;
}

- (AMapLocationManager *)locationManager
{
   if (!_locationManager) {
       _locationManager = [[AMapLocationManager alloc] init];
       [_locationManager setDelegate:self];
       [_locationManager setAllowsBackgroundLocationUpdates:YES];
       //iOS 9（不包含iOS 9） 之前设置允许后台定位参数，保持不会被系统挂起
       [_locationManager setPausesLocationUpdatesAutomatically:NO];
       //iOS 9（包含iOS 9）之后新特性：将允许出现这种场景，同一app中多个locationmanager：一些只能在前台定位，另一些可在后台定位，并可随时禁止其后台定位。
       if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
           _locationManager.allowsBackgroundLocationUpdates = YES;
       }
   }
   return _locationManager;
}

- (AMapSearchAPI *)searchAPI
{
   if (!_searchAPI) {
       _searchAPI = [[AMapSearchAPI alloc] init];
       _searchAPI.delegate = self;
   }
   return _searchAPI;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
