//
//  YXSelectCYServiceViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXSelectCYServiceViewController.h"
#import "DDWorkHeaderView.h"
#import "YXYCServiceCollectionViewCell.h"
#import "WGPublicBottomView.h"
#import "YXHomeViewModel.h"
#import "YXCateModel.h"
@interface YXSelectCYServiceViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) YXCateCategorysModel *model;
@property (nonatomic ,strong) NSMutableArray *selectArr;

@end

@implementation YXSelectCYServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"创业申请";
    self.dataArr = [NSMutableArray array];
    self.selectArr = [NSMutableArray array];
    [self loadData];
    
    [self loadSubViews];
}

- (void)loadData {
    DDWeakSelf
    [YXHomeViewModel queryHomeAllCatesWithType:nil city_ids:self.city_id c_model:nil Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.dataArr = [YXCateCategorysModel mj_objectArrayWithKeyValuesArray:  responesObj[@"data"]];
            weakSelf.model = [weakSelf.dataArr firstObject];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
}
#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}
#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.model.categorys.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    YXCateModel *cateModel = self.model.categorys[section];
    return cateModel.child.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YXCateModel *cateModel = self.model.categorys[indexPath.section];
    YXCateChildModel *childModel = cateModel.child[indexPath.row];
    return CGSizeMake((KWIDTH - 60), childModel.cellHeight2);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 30, 15, 30);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 40);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        DDWorkHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDWorkHeaderView" forIndexPath:indexPath];
        YXCateModel *cateModel = self.model.categorys[indexPath.section];
        header.titleLab.text = cateModel.name;
        header.rightBtn.hidden = YES;
        header.rightImg.hidden = YES;
        return header;
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXYCServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXYCServiceCollectionViewCell" forIndexPath:indexPath];
    YXCateModel *cateModel = self.model.categorys[indexPath.section];
    cell.childModel = cateModel.child[indexPath.row];
    DDWeakSelf
    [cell setClickItemBlock:^(YXCateChildModel * _Nonnull childModel) {
        if (![weakSelf.selectArr containsObject:childModel]) {
            [weakSelf.selectArr addObject:childModel];
        }
    }];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[DDWorkHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDWorkHeaderView"];
        [_collectionView registerClass:[YXYCServiceCollectionViewCell class] forCellWithReuseIdentifier:@"YXYCServiceCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
                    
        }];
    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"下一步"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.selectArr.count == 0) {
                return [YJProgressHUD showMessage:@"请选择您要申请的类型"];
            }
            if (weakSelf.selectServiceBlock) {
                weakSelf.selectServiceBlock(weakSelf.selectArr);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }
    return _bottomView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
