//
//  YXSelectCYServiceViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXSelectCYServiceViewController : DDBaseViewController

@property (nonatomic ,strong) void(^selectServiceBlock)(NSMutableArray *selectArr);

@property (nonatomic ,strong) NSString *city_id;


@end

NS_ASSUME_NONNULL_END
