//
//  YXSelectServiceViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXSelectServiceViewController.h"
#import "YXServiceCollectionViewCell.h"
#import "WGPublicBottomView.h"
#import "DDWorkHeaderView.h"
#import "YXVideoViewModel.h"
#import "YXVideoModel.h"

@interface YXSelectServiceViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) YXBrandModel *model;
@end

@implementation YXSelectServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"创业申请";
    
    [self loadData];
    
    [self loadSubViews];

}

- (void)loadData {

    DDWeakSelf
    [YXVideoViewModel queryAllBrandsWithCity_id:self.city_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf.dataArr removeAllObjects];
            weakSelf.dataArr = [YXBrandModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
            
    }];

}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    YXBrandModel *brandModel = self.dataArr[section];
    return brandModel.child.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KWIDTH - 60)/3, 32);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 40);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        DDWorkHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDWorkHeaderView" forIndexPath:indexPath];
        YXBrandModel *brandModel = self.dataArr[indexPath.section];
        header.titleLab.text = brandModel.brand_name;
        header.rightBtn.hidden = YES;
        header.rightImg.hidden = YES;
        return header;
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXServiceCollectionViewCell" forIndexPath:indexPath];
    YXBrandModel *brandModel = self.dataArr[indexPath.section];
    YXBrandModel *model = brandModel.child[indexPath.row];
    cell.titleLab.text = model.brand_name;
    if (model.selected == YES) {
        cell.selectImg.hidden = NO;
    }else {
        cell.selectImg.hidden = YES;
    }
     return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    YXBrandModel *brandModel = self.dataArr[indexPath.section];
    self.model = brandModel.child[indexPath.row];
    for (YXBrandModel *brandModel2 in self.dataArr) {
        for (YXBrandModel *model2 in brandModel2.child) {
            if (model2 == self.model) {
                model2.selected = YES;
            }else {
                model2.selected = NO;
            }
        }
    }
    [self.collectionView reloadData];
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 15;
        layout.minimumInteritemSpacing = 15;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[DDWorkHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDWorkHeaderView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXServiceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXServiceCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
                    
        }];
    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"下一步"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.model == nil) {
                return [YJProgressHUD showMessage:@"请选择您要申请的类型"];
            }
            if (weakSelf.selectServiceBlock) {
                weakSelf.selectServiceBlock(weakSelf.model);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }
    return _bottomView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
