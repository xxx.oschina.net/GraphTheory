//
//  YXSelectServiceViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXBrandModel;
@interface YXSelectServiceViewController : DDBaseViewController

@property (nonatomic ,strong) void(^selectServiceBlock)(YXBrandModel *model);


@property (nonatomic ,strong) NSString *city_id;

@end

NS_ASSUME_NONNULL_END
