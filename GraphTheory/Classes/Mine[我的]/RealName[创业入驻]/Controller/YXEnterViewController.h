//
//  YXEnterViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXEnterViewController : DDBaseViewController

// type 2:创业入驻 3:视频号入驻
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
