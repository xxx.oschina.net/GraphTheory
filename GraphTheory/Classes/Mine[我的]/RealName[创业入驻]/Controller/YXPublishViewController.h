//
//  YXPublishViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXVideoModel;
@interface YXPublishViewController : DDBaseViewController

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXVideoModel *videoModel;

@end

NS_ASSUME_NONNULL_END
