//
//  YXEnterViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEnterViewController.h"
#import "YXLocationViewController.h"
#import "YXCityListViewController.h"
#import "YXSelectServiceViewController.h"
#import "YXSelectCYServiceViewController.h"
#import "YXWebViewController.h"

#import "WGPublicBottomView.h"
#import "YXEnterFooterView.h"
#import "YXPublicCollectionReusableView.h"
#import <JJCollectionViewRoundFlowLayout.h>
#import "YXEnterCollectionViewCell.h"
#import "YXEnterModel.h"
#import "YXCityModel.h"
#import "YXUserViewModel.h"
#import "YXVideoModel.h"
#import "YXCateModel.h"
@interface YXEnterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) NSMutableArray *selectImgArr;
@property (nonatomic ,strong) YXEnterModel *model;
@end

@implementation YXEnterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.selectImgArr = [NSMutableArray array];
    self.model = [[YXEnterModel alloc] init];
    self.model.agree = NO;
    if (self.type == 2) {
        self.title = @"创业入驻";
        self.model.role = @"1";
    }else {
        self.title = @"视频号入驻";
        self.model.role = @"2";
    }

    [self loadSubViews];
}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}

// 上传图片
- (void)uploadImg {
    
    if ([NSString isBlankString:self.model.name]) {
        return [YJProgressHUD showMessage:@"请输入真实姓名"];
    }

    if ([NSString isBlankString:self.model.cardID]) {
        return [YJProgressHUD showMessage:@"请输入身份证号码"];
    }

    if (![NSString checkIdentityCardNo:self.model.cardID]) {
        return [YJProgressHUD showMessage:@"请输入合法的身份证号"];
    }

    if ([NSString isBlankString:self.model.address]) {
        return [YJProgressHUD showMessage:@"请设置地址信息"];
    }

    if ([NSString isBlankString:self.model.service_city_ids]) {
        return [YJProgressHUD showMessage:@"请选择服务城市"];
    }

    if ([NSString isBlankString:self.model.service_ids]) {
        return [YJProgressHUD showMessage:@"请选择您能提供的服务"];
    }

    if ([NSString isBlankString:self.model.phone]) {
        return [YJProgressHUD showMessage:@"请输入手机号"];
    }

    if (self.selectImgArr.count == 0) {
        return [YJProgressHUD showMessage:@"请选择图片"];
    }
    
    if (self.model.agree == NO) {
        return [YJProgressHUD showMessage:@"请同意入驻协议！"];
    }
    
    DDWeakSelf
    dispatch_group_t group = dispatch_group_create();
    __block NSMutableArray *images = [NSMutableArray array];
    for (UIImage *image in self.selectImgArr) {
        dispatch_group_enter(group);
        [YXUserViewModel requestUploadImg:image Completion:^(id responesObj) {
            if (REQUESTDATASUCCESS) {
                NSArray *dataArr = responesObj[@"data"];
                if (dataArr.count) {
                    [images addObject:dataArr[0]];
                }
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            dispatch_group_leave(group);
        } failure:^(NSError *error) {
            dispatch_group_leave(group);
        }];
    }

    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD hideHUD];
        if (images.count) {
            weakSelf.model.images = [images componentsJoinedByString:@","];
            [weakSelf save];
        }
    });
    
}


- (void)save {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel requestApplyRole:self.model.role
                            userphone:self.model.phone
                             username:self.model.name
                               images:self.model.images
                              id_card:self.model.cardID
                              address:self.model.address
                          service_ids:self.model.service_ids
                     service_city_ids:self.model.service_city_ids
                           Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD hideHUD];
    }];
    
}

- (void)xieyi{
    NSLog(@"协议");
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAboutOtherInfoWithType:self.type==2?@"4":@"5" Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = responesObj[@"data"][@"us_title"];
        vc.url = responesObj[@"data"][@"us_content"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];

}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 7;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section == 2){
        return CGSizeMake(SCREEN_WIDTH - 30, 100);
    }else {
        return CGSizeMake(SCREEN_WIDTH - 30, 40);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    if (section == 0){
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
    return UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (section == 2||section == 6) {
        return CGSizeMake(KWIDTH - 30, 40);
    }
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        if (indexPath.section == 2) {
            YXPublicCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXPublicCollectionReusableView" forIndexPath:indexPath];
            footer.backgroundColor = [UIColor clearColor];
            footer.titleLab.text = @"请上传身份证正反面照片、相关资质证书及作品";
            footer.titleLab.textColor = color_TextThree;
            return footer;
        }else if (indexPath.section == 6) {
            YXEnterFooterView *footer =[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXEnterFooterView" forIndexPath:indexPath];
            footer.backgroundColor = [UIColor clearColor];
            [footer setAgreeBtnBlock:^(BOOL isAgree) {
                weakSelf.model.agree = isAgree;
            }];
            [footer setXieyiBtnBlock:^{
                [weakSelf xieyi];
            }];
            return footer;
        }
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    YXEnterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXEnterCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.model;
    cell.indexPath = indexPath;
    DDWeakSelf
    [cell setEditTextBlock:^(NSString * _Nonnull text) {
        if (indexPath.section == 0) {
            weakSelf.model.name = text;
        }else if (indexPath.section == 1) {
            weakSelf.model.cardID = text;
        }else if (indexPath.section == 6) {
            weakSelf.model.phone = text;
        }
    }];
    
    [cell.imgPicker observeSelectedMediaArray:^(NSArray<LLImagePickerModel *> *list){
        [weakSelf.selectImgArr removeAllObjects];
         for (LLImagePickerModel *model in list){
             [weakSelf.selectImgArr addObject:model.image];
         }
     }];
    
     return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    DDWeakSelf
    if (indexPath.section == 3) {
        YXLocationViewController *vc = [YXLocationViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        [vc setSelectAddressBlock:^(NSString * _Nonnull addrsss) {
            weakSelf.model.address = addrsss;
            [weakSelf.collectionView reloadData];
        }];
    }else if (indexPath.section == 4) {
        YXCityListViewController *vc = [YXCityListViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        [vc setSelectCiytBlock:^(YXCityModel * _Nonnull model) {
            weakSelf.model.service_city_ids = model.Id;
            weakSelf.model.cityName = model.district;
            [weakSelf.collectionView reloadData];
        }];
    }else if (indexPath.section == 5) {
        
        if ([NSString isBlankString:self.model.service_city_ids]) {
            return [YJProgressHUD showMessage:@"请选择服务城市"];
        }
        if (self.type == 2) {
            YXSelectCYServiceViewController *vc = [YXSelectCYServiceViewController new];
            vc.city_id = self.model.service_city_ids;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setSelectServiceBlock:^(NSMutableArray * _Nonnull selectArr) {
                NSMutableArray *serviceIds = [NSMutableArray array];
                NSMutableArray *serviceNames = [NSMutableArray array];
                if (selectArr.count) {
                    for (YXCateChildModel *model in selectArr) {
                        [serviceIds addObject:model.Id];
                        [serviceNames addObject:model.name];
                    }
                    weakSelf.model.service_ids =[serviceIds componentsJoinedByString:@","];
                    weakSelf.model.service_name = [serviceNames componentsJoinedByString:@","];
                    [weakSelf.collectionView reloadData];
                }
            }];
        }else {
            YXSelectServiceViewController *vc = [YXSelectServiceViewController new];
            vc.city_id = self.model.service_city_ids;
            [self.navigationController pushViewController:vc animated:YES];
            [vc setSelectServiceBlock:^(YXBrandModel * _Nonnull model) {
                weakSelf.model.service_ids = model.brand_id;
                weakSelf.model.service_name = model.brand_name;
                [weakSelf.collectionView reloadData];
            }];
        }
     
    }
    
}


#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[YXEnterCollectionViewCell class] forCellWithReuseIdentifier:@"YXEnterCollectionViewCell"];
        [_collectionView registerClass:[YXPublicCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXPublicCollectionReusableView"];
        [_collectionView registerClass:[YXEnterFooterView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXEnterFooterView"];
    }
    return _collectionView;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"提交审核"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf uploadImg];
        }];
    }
    return _bottomView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
