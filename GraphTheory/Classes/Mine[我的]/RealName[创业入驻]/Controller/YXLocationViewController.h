//
//  YXLocationViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXLocationViewController : DDBaseViewController

@property (nonatomic ,copy)void(^selectAddressBlock)(NSString *addrsss);

@property (nonatomic ,copy)void(^selectAddressPOIBlock)(NSString *name,NSString *address,CGFloat lat,CGFloat lng);

@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
