//
//  YXPublishViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPublishViewController.h"
#import "YXLocationViewController.h"
#import "YXProductListViewController.h"

#import "WGPublicBottomView.h"
#import "YXTextTableViewCell.h"
#import "YXInputTableViewCell.h"
#import "YXNextTableViewCell.h"
#import "YXPublishImgTableViewCell.h"

#import "YXVideoViewModel.h"
#import "YXPublishModel.h"
#import "YXProductModel.h"
#import "YXCateModel.h"
#import "YXVideoModel.h"
#import "YXUploadFileManager.h"
#import "YXFileModel.h"
#import "YXGoodsViewModel.h"
#import "YXPreviewManage.h"
@interface YXPublishViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXPublishModel *model;
@property (nonatomic ,strong) NSMutableArray *selectFileArr;
@property (nonatomic ,strong) YXUploadModel *uploadModel;
@end

@implementation YXPublishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"动态发布";
    self.model = [YXPublishModel new];
    if (self.type == 1) {
        self.model.Top_category = self.videoModel.top_category;
        self.model.category = self.videoModel.category;
        self.model.category_name = [NSString stringWithFormat:@"%@/%@",self.videoModel.brand.brand_name,self.videoModel.topBrandInfo.brand_name];
        self.model.title = self.videoModel.v_name;
        self.model.introduce = self.videoModel.v_introduce;
        self.model.cover_img = self.videoModel.v_cover_img;
        self.model.videoPath = self.videoModel.v_videoroute;
        UIImageView *imgView = [UIImageView new];
        [imgView sd_setImageWithURL:[NSURL URLWithString:_videoModel.v_cover_img]];
        self.model.fileImg = imgView.image;
        self.model.videoImg = [UIImage getVideoPreViewImageWithPath:[NSURL URLWithString:self.videoModel.v_videoroute]];
        self.model.videoPath = self.videoModel.v_videoroute;
        self.model.goods_id = self.videoModel.v_p_id;
        self.model.lat = self.videoModel.v_location.lat;
        self.model.lng = self.videoModel.v_location.lng;
        [self loadData];
    }else {
        YXUserInfoModel *userInfo = [YXUserInfoManager getUserInfo];
        NSArray *services = userInfo.applyInfo[@"video"][@"services"];
        if (services.count) {
            YXBrandModel *brandModel = [YXBrandModel mj_objectWithKeyValues:services[0]];
            self.model.category = brandModel.brand_id;
            YXTopBrandInfoModel *topBrandInfoModel = brandModel.child[0];
            self.model.Top_category = topBrandInfoModel.brand_id;
            self.model.category_name = [NSString stringWithFormat:@"%@/%@",brandModel.brand_name,topBrandInfoModel.brand_name];
        }
    }
    
    
    [self loadSubViews];

}

- (void)loadData {
    
    DDWeakSelf
    [YXGoodsViewModel queryMyGoodsInfoWithId:self.videoModel.v_p_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            YXGoodsInfoModel *model = [YXGoodsInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.model.goods_name = model.title;
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


#pragma mark - 发布
- (void)save {
    
    if ([NSString isBlankString:self.model.title]) {
        return [YJProgressHUD showMessage:@"请输入标题"];
    }
    
    if (self.model.fileImg == nil) {
        return [YJProgressHUD showMessage:@"请选择封面"];
    }
    
    if (self.model.videoPath == nil && self.model.videoImg) {
        return [YJProgressHUD showMessage:@"请选择视频"];
    }
    
    if ([NSString isBlankString:self.model.introduce]) {
        return [YJProgressHUD showMessage:@"请添加描述"];
    }
    
    if ([NSString isBlankString:self.model.lat]&&[NSString isBlankString:self.model.lng]) {
        return [YJProgressHUD showMessage:@"请选择地址"];
    }
    
    if ([NSString isBlankString:self.model.goods_id]) {
        return [YJProgressHUD showMessage:@"请选择商品链接"];
    }
    
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [YXUserViewModel requestUploadImg:self.model.fileImg Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            NSArray *dataArr = responesObj[@"data"];
            if (dataArr.count) {
                weakSelf.model.cover_img = dataArr[0];
            }
        }
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_enter(group);
    [YXUserViewModel requestUploadVideoFileName:self.model.videoPath Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            NSArray *dataArr = responesObj[@"data"];
            if (dataArr.count) {
                weakSelf.model.video = dataArr[0];
            }
        }
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [YJProgressHUD showLoading:@""];
        [YXVideoViewModel requestCreateVideoWithType:@(self.type).stringValue Id:self.videoModel.v_id Title:self.model.title Top_category:self.model.category category:self.model.Top_category cover_img:self.model.cover_img introduce:self.model.introduce video:self.model.video lat:self.model.lat lng:self.model.lng goods_id:self.model.goods_id Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
                [YJProgressHUD showMessage:@"发布成功"];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError * _Nonnull error) {
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    });
    
 
}


#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        return 150;
    }
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf
    if (indexPath.section == 0) {
        YXTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXTextTableViewCell" forIndexPath:indexPath];
        cell.contentLab.text = self.model.category_name;
        return cell;
    }else if (indexPath.section == 2) {
        YXPublishImgTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublishImgTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        YXWeakCell
        [cell setSelectBlock:^(NSInteger index) {
            if (index == 0) {
                if (weakSelf.model.fileImg) {
                    [[YXPreviewManage sharePreviewManage] showPhotoWithImgArr:@[weakSelf.model.fileImg] currentIndex:0];
                }else {
                    [[YXUploadFileManager shareUploadManager] actionPhotoWithBlock:^(NSMutableArray *uploadArray) {
                        NSLog(@"-- %@",uploadArray);
                        weakSelf.selectFileArr = uploadArray;
                        if (weakSelf.selectFileArr.count > 0) {
                            YXUploadModel *uploadModel = [weakSelf.selectFileArr firstObject];
                            weakSelf.model.fileImg = uploadModel.fileImg;
                            [weakSelf.tableView reloadData];
                        }
                    }];
                }
                
            }else {
                if (weakSelf.model.videoPath&&weakSelf.model.videoImg) {
                    [[YXPreviewManage sharePreviewManage] showVideoWithUrl:weakSelf.model.videoPath withController:weakSelf];
                }else {
                    [[YXUploadFileManager shareUploadManager] actionVideoWithDuration:5 Block:^(NSMutableArray *uploadArray) {
                        NSLog(@"%@",uploadArray);
                        weakSelf.selectFileArr = uploadArray;
                        if (weakSelf.selectFileArr.count > 0) {
                            YXUploadModel *uploadModel = [weakSelf.selectFileArr firstObject];
                            weakSelf.model.videoImg = uploadModel.fileImg;
                            weakSelf.model.videoPath = [uploadModel.path absoluteString];
                            [weakSelf.tableView reloadData];
                        }
                    }];
                }
            }
        }];
        [cell setDeleteImgBlock:^{
            weakSelf.model.fileImg = nil;
            [weakSelf.tableView reloadData];
        }];
        [cell setDeleteVideoBlock:^{
            weakSelf.model.videoImg = nil;
            weakSelf.model.videoPath = nil;
            [weakSelf.tableView reloadData];
        }];
        
        return cell;
    }else if (indexPath.section == 1 || indexPath.section == 3) {
        YXInputTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXInputTableViewCell" forIndexPath:indexPath];
        if (indexPath.section == 1) {
            cell.textField.text =self.model.title;
            cell.textField.placeholder = @"请输入动态标题";
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                weakSelf.model.title = text;
            }];
        }else {
            cell.textField.text =self.model.introduce;
            cell.textField.placeholder = @"添加描述";
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                weakSelf.model.introduce = text;
            }];
        }
        return cell;
    }else if (indexPath.section == 4|| indexPath.section == 5) {
        YXNextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXNextTableViewCell" forIndexPath:indexPath];
        if (indexPath.section == 4) {
            cell.imgView.image = [UIImage imageNamed:@"weizhi"];
            cell.titleLab.text = self.model.address ? self.model.address : @"所在位置";
        }else {
            cell.imgView.image = [UIImage imageNamed:@"lianjie"];
            cell.titleLab.text = self.model.goods_name ? self.model.goods_name : @"带货链接";
        }
        return cell;
    }
    return [UITableViewCell new];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DDWeakSelf
    if (indexPath.section == 4) {
        YXLocationViewController *vc = [YXLocationViewController new];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
        [vc setSelectAddressPOIBlock:^(NSString * _Nonnull name, NSString * _Nonnull addrsss, CGFloat lat, CGFloat lng) {
            weakSelf.model.address = name;
            weakSelf.model.lat = [NSString stringWithFormat:@"%f",lat];
            weakSelf.model.lng = [NSString stringWithFormat:@"%f",lng];
            [weakSelf.tableView reloadData];
        }];
    }else if (indexPath.section == 5) {
        YXProductListViewController *vc = [YXProductListViewController new];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
        [vc setSelectGoodsBlock:^(YXProductModelList * _Nonnull model) {
            weakSelf.model.goods_id = model.goods_id;
            weakSelf.model.goods_name = model.title;
            [weakSelf.tableView reloadData];
        }];
    }
}

- (NSMutableArray *)selectFileArr {
    if (!_selectFileArr) {
        _selectFileArr = [NSMutableArray array];
    }
    return _selectFileArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [_tableView registerNib:[UINib nibWithNibName:@"YXTextTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXTextTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXInputTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXInputTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXNextTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXNextTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXPublishImgTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXPublishImgTableViewCell"];
    }
    return _tableView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        if (self.type == 0) {
            [_bottomView setTitle:@"发布"];
        }else {
            [_bottomView setTitle:@"修改"];
        }
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf save];
        }];
    }
    return _bottomView;
}

#pragma mark - 添加上传文件
- (void)uploadImageWithSection:(NSInteger)section Image:(UIImage *)image  {
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:section];
//    YXAddFileCell *cell = (YXAddFileCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
//    cell.addImgView.contentMode = UIViewContentModeScaleAspectFit;
//    cell.addImgView.clipsToBounds = YES;
//    cell.addImgView.image = image;

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
