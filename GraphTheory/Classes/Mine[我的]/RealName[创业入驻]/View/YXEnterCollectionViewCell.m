//
//  YXEnterCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEnterCollectionViewCell.h"
#import "YXEnterModel.h"
@interface YXEnterCollectionViewCell ()
@end

@implementation YXEnterCollectionViewCell

- (void)setModel:(YXEnterModel *)model {
    _model = model;
    
}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    
    if (_indexPath.section == 0) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"请输入真实姓名";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.text = _model.name;
    }else if (_indexPath.section == 1) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"请输入身份证号码";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.text = _model.cardID;
    }else if (_indexPath.section == 2) {
        self.type = CellViewTypeStateImage;
        self.imgPicker.maxImageSelected = 3;
    }else if (_indexPath.section == 3) {
        self.type = CellViewTypeStateSelect;
        self.titleLab.text = self.model.address?self.model.address:@"请设置地址信息";
        self.rightImg.image = [UIImage imageNamed:@"chengshi0309"];
    }else if (_indexPath.section == 4) {
        self.type = CellViewTypeStateSelect;
        self.titleLab.text = self.model.cityName?self.model.cityName:@"请选择服务城市";
        self.rightImg.image = [UIImage imageNamed:@"dingwei0309"];
    }else if (_indexPath.section == 5) {
        self.type = CellViewTypeStateNext;
        if (self.model.service_ids) {
            self.titleLab.text = self.model.service_name;
        }else {
            self.titleLab.text =  @"请选择您能提供的服务";
        }
    }else if (_indexPath.section == 6) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"请输入手机号";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.text = _model.phone;
    }
}



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 8.0f;
    }
    return self;
}

@end
