//
//  YXServiceCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXServiceCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *selectImg;

@end

NS_ASSUME_NONNULL_END
