//
//  YXEnterFooterView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEnterFooterView.h"

@implementation YXEnterFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)setup {
    [self addSubview:self.agreeBtn];
    [self addSubview:self.xieyiBtn];
    [self.agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    [self.xieyiBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.left.equalTo(self.agreeBtn.mas_right).offset(10);
    }];
}

- (UIButton *)agreeBtn {
    if (!_agreeBtn) {
        _agreeBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_agreeBtn setImage:[UIImage imageNamed:@"store_creat_mutNoSelect"] forState:(UIControlStateNormal)];
        [_agreeBtn setImage:[UIImage imageNamed:@"store_creat_mutSelect"] forState:(UIControlStateSelected)];
        [_agreeBtn addTarget:self action:@selector(agreeAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _agreeBtn;
}
- (UIButton *)xieyiBtn {
    if (!_xieyiBtn) {
        _xieyiBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_xieyiBtn setTitle:@"我同意《入驻协议说明》" forState:(UIControlStateNormal)];
        [_xieyiBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        _xieyiBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_xieyiBtn addTarget:self action:@selector(xieyiBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _xieyiBtn;
}


- (void)agreeAction:(UIButton *)sender {
    sender.selected = ! sender.selected;
    if (self.agreeBtnBlock) {
        self.agreeBtnBlock(sender.selected);
    }
}
- (void)xieyiBtnAction:(UIButton *)sender {
    if (self.xieyiBtnBlock) {
        self.xieyiBtnBlock();
    }
}

@end
