//
//  YXServiceCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXServiceCollectionViewCell.h"

@implementation YXServiceCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.layer.masksToBounds = YES;
    self.contentView.layer.cornerRadius = 4.0;
    self.contentView.layer.borderWidth = 1.0f;
    self.contentView.layer.borderColor = color_TextThree.CGColor;
}

@end
