//
//  YXTextTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXTextTableViewCell.h"

@implementation YXTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
