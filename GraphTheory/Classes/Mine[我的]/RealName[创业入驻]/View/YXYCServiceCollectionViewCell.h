//
//  YXYCServiceCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCateChildModel;
@interface YXYCServiceCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickItemBlock)(YXCateChildModel *childModel);

@property (nonatomic ,strong)YXCateChildModel *childModel;

@end

NS_ASSUME_NONNULL_END
