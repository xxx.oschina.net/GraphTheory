//
//  YXEnterFooterView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/8.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXEnterFooterView : UICollectionReusableView
@property (strong, nonatomic) UIButton *agreeBtn;
@property (strong, nonatomic) UIButton *xieyiBtn;
@property (nonatomic ,copy)void(^agreeBtnBlock)(BOOL isAgree);
@property (nonatomic ,copy)void(^xieyiBtnBlock)(void);

@end

NS_ASSUME_NONNULL_END
