//
//  YXPublishImgTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPublishImgTableViewCell.h"
#import "YXPublishModel.h"

@interface YXPublishImgTableViewCell ()

@end

@implementation YXPublishImgTableViewCell

- (void)setModel:(YXPublishModel *)model {
    _model = model;
    if (_model.fileImg) {
        _photoImg.image = _model.fileImg;
        self.deleteImgBtn.hidden = NO;
    }else {
        _photoImg.image = [UIImage imageNamed:@"goods_add_addImage"];
        self.deleteImgBtn.hidden = YES;
    }
    if (_model.videoImg) {
        _videoImg.image = _model.videoImg;
        self.deleteVideoBtn.hidden = NO;
    }else {
        _videoImg.image = [UIImage imageNamed:@"goods_add_addImage"];
        self.deleteVideoBtn.hidden = YES;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.photoImg.userInteractionEnabled = YES;
    self.videoImg.userInteractionEnabled = YES;
    UITapGestureRecognizer *photoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click1)];
    [self.photoImg addGestureRecognizer:photoTap];
    UITapGestureRecognizer *videoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click2)];
    [self.videoImg addGestureRecognizer:videoTap];
    self.deleteImgBtn.hidden = YES;
    self.deleteVideoBtn.hidden = YES;
    
}

- (void)click1 {
    if (self.selectBlock) {
        self.selectBlock(0);
    }
}
- (void)click2 {
    if (self.selectBlock) {
        self.selectBlock(1);
    }
}
- (IBAction)deleteImgAction:(UIButton *)sender {
    if (self.deleteImgBlock) {
        self.deleteImgBlock();
    }
}

- (IBAction)deleteVideoAction:(UIButton *)sender {
    if (self.deleteVideoBlock) {
        self.deleteVideoBlock();
    }
}


@end
