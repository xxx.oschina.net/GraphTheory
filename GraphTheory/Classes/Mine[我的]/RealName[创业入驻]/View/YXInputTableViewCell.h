//
//  YXInputTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXInputTableViewCell : UITableViewCell
@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@end

NS_ASSUME_NONNULL_END
