//
//  YXPublishImgTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXPublishModel;
@interface YXPublishImgTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImg;
@property (weak, nonatomic) IBOutlet UIImageView *videoImg;
@property (weak, nonatomic) IBOutlet UIButton *deleteImgBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteVideoBtn;

@property (nonatomic ,copy)void(^selectBlock)(NSInteger index);
@property (nonatomic ,copy)void(^deleteImgBlock)(void);
@property (nonatomic ,copy)void(^deleteVideoBlock)(void);


@property (nonatomic ,strong) YXPublishModel *model;

@end

NS_ASSUME_NONNULL_END
