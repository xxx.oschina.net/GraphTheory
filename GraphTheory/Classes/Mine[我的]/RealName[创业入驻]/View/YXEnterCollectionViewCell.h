//
//  YXEnterCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDPublicCell.h"

NS_ASSUME_NONNULL_BEGIN
@class YXEnterModel;
@interface YXEnterCollectionViewCell : DDPublicCollectionCell

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,strong) YXEnterModel *model;

@end

NS_ASSUME_NONNULL_END
