//
//  YXInputTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/30.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXInputTableViewCell.h"

@implementation YXInputTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)edit:(UITextField *)sender {
    if (self.editTextBlock) {
        self.editTextBlock(sender.text);
    }
}

@end
