//
//  YXCouponsViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCouponsViewModel : DDBaseViewModel


/**
我的优惠券列表

@param page    分页数
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryMyCouponListWithPage:(NSString *)page
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
待领取优惠券列表

@param page    分页数
@param city_id    城市id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryCouponListWithPage:(NSString *)page
                        city_id:(NSString *)city_id
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
分享优惠券banner列表

@param page    分页数
@param city_id    城市id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryCouponBannerWithPage:(NSString *)page
                        city_id:(NSString *)city_id
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;



/**
领取优惠券

@param type_id    优惠券id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryReceiveCouponWithType_id:(NSString *)type_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
符合服务的优惠券

@param Id                       服务id
@param sales_price   价格
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryValidateServiceCouponWithId:(NSString *)Id
                             sales_price:(NSString *)sales_price
                              Completion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError;

/**
 服务子订单 重新选择优惠券

@param Id                       服务id
@param coupon_id   优惠券id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)setSubOrderCouponCouponWithId:(NSString *)Id
                            coupon_id:(NSString *)coupon_id
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;
@end

NS_ASSUME_NONNULL_END
