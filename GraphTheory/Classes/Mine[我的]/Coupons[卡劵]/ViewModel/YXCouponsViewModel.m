//
//  YXCouponsViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCouponsViewModel.h"

@implementation YXCouponsViewModel

#pragma mark - 我的优惠券列表
+ (void)queryMyCouponListWithPage:(NSString *)page
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myCouponList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 待领取优惠券列表
+ (void)queryCouponListWithPage:(NSString *)page
                        city_id:(NSString *)city_id
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/couponList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [params setValue:city_id forKey:@"city_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 分享优惠券banner列表
+ (void)queryCouponBannerWithPage:(NSString *)page
                        city_id:(NSString *)city_id
                     Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/couponBanner",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [params setValue:city_id forKey:@"city_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark - 领取优惠券
+ (void)queryReceiveCouponWithType_id:(NSString *)type_id
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError{
    NSString *urlStr = [NSString stringWithFormat:@"%@center/receiveCoupon",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type_id forKey:@"type_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 符合服务的优惠券
+ (void)queryValidateServiceCouponWithId:(NSString *)Id
                             sales_price:(NSString *)sales_price
                              Completion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/validateServiceCoupon",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:sales_price forKey:@"sales_price"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 服务子订单 重新选择优惠券
+ (void)setSubOrderCouponCouponWithId:(NSString *)Id
                            coupon_id:(NSString *)coupon_id
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/setSubOrderCoupon",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:coupon_id forKey:@"coupon_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



@end
