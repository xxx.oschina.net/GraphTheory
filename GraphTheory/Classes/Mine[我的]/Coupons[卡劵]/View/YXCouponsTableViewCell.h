//
//  YXCouponsTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCouponsModelList;
@interface YXCouponsTableViewCell : UITableViewCell

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXCouponsModelList *model;

@property (nonatomic ,copy)void(^clickReceiveBlock)(void);

@end

NS_ASSUME_NONNULL_END
