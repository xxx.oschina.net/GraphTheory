//
//  YXCouponsTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCouponsTableViewCell.h"
#import "YXCouponsModel.h"
@interface YXCouponsTableViewCell ()
/** 背景*/
@property (nonatomic ,strong) UIView *backView;
/** 名称*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 播放次数*/
@property (nonatomic ,strong) UILabel *playLab;
/** 金额*/
@property (nonatomic ,strong) UILabel *priceLab;

@property (nonatomic ,strong) UIButton *receiveBtn;

@end

@implementation YXCouponsTableViewCell


- (void)setType:(NSInteger)type {
    _type = type;
    if (_type == 1) {
        [self addLayout1];
    }else {
        [self addLayout];
    }
}

- (void)setModel:(YXCouponsModelList *)model {
    _model = model;
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.price];
    _nameLab.text = _model.title;
    _playLab.text = _model.price_title;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
 
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 4;
    }
    return _backView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont boldSystemFontOfSize:16.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)playLab {
    if (!_playLab) {
        _playLab = [[UILabel alloc] init];
        _playLab.textColor = color_TextOne;
        _playLab.text = @"已禁用";
        _playLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _playLab;
}

- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [[UILabel alloc] init];
        _priceLab.font = [UIFont systemFontOfSize:20.0f];
        _priceLab.text = @"¥0.20";
        _priceLab.textColor = APPTintColor;
    }
    return _priceLab;
}
- (UIButton *)receiveBtn {
    if (!_receiveBtn) {
        _receiveBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _receiveBtn.layer.masksToBounds = YES;
        _receiveBtn.layer.cornerRadius = 4.0;
        _receiveBtn.backgroundColor = APPTintColor;
        [_receiveBtn setTitle:@"领取" forState:(UIControlStateNormal)];
        [_receiveBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _receiveBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
        [_receiveBtn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _receiveBtn;
}


#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.priceLab];
    [self.backView addSubview:self.nameLab];
    [self.backView addSubview:self.playLab];
    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.top.equalTo(@0);
        make.bottom.right.equalTo(@-10);
    }];
    [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.left.equalTo(weakSelf.backView.mas_left).offset(10);
        [weakSelf.priceLab sizeToFit];
    }];
    
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.backView.mas_top).offset(15);
        make.left.equalTo(weakSelf.priceLab.mas_right).offset(15);
        [weakSelf.nameLab sizeToFit];
    }];

    [_playLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.nameLab.mas_bottom).offset(10);
        make.left.equalTo(weakSelf.priceLab.mas_right).offset(15);
        [weakSelf.playLab sizeToFit];
    }];

}

- (void)addLayout1 {
    
    YXWeakSelf
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.priceLab];
    [self.backView addSubview:self.nameLab];
    [self.backView addSubview:self.playLab];
    [self.backView addSubview:self.receiveBtn];

    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [_priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.left.equalTo(weakSelf.backView.mas_left).offset(10);
        [weakSelf.priceLab sizeToFit];
    }];
    
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.backView.mas_top).offset(15);
        make.left.equalTo(weakSelf.priceLab.mas_right).offset(15);
        [weakSelf.nameLab sizeToFit];
    }];

    [_playLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.nameLab.mas_bottom).offset(10);
        make.left.equalTo(weakSelf.priceLab.mas_right).offset(15);
        [weakSelf.playLab sizeToFit];
    }];
    [_receiveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(40, 20));
    }];

}

- (void)click {
    if (self.clickReceiveBlock) {
        self.clickReceiveBlock();
    }
}


@end
