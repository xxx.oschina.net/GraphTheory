//  Created by li qiao robot 
#import <Foundation/Foundation.h>
@interface YXCouponsModelList : NSObject
@property (copy,nonatomic)   NSString *validate;
@property (copy,nonatomic)   NSString *validate_title;
@property (copy,nonatomic)   NSString *is_outdate;
@property (copy,nonatomic)   NSString *min_price;
@property (copy,nonatomic)   NSString *price_title;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *coupon_id;
@property (copy,nonatomic)   NSString *title;
@property (copy,nonatomic)   NSString *type;
@property (copy,nonatomic)   NSString *price;
@property (copy,nonatomic)   NSString *unique;
@property (copy,nonatomic)   NSString *is_use;
@property (copy,nonatomic)   NSString *coupon_type_id;
@property (copy,nonatomic)   NSString *type_id;
@property (copy,nonatomic)   NSString *use_time;
@property (copy,nonatomic)   NSString *link_ids;
@property (copy,nonatomic)   NSString *use_range_title;
@property (copy,nonatomic)   NSString *validate_time;
@property (copy,nonatomic)   NSString *status;
@property (copy,nonatomic)   NSString *add_time;
@property (copy,nonatomic)   NSString *desc_content;
@property (assign,nonatomic)  BOOL selected;

@end

@interface YXCouponsModel : NSObject
@property (strong,nonatomic) NSNumber *total;
@property (strong,nonatomic) NSNumber *limit;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *totalPage;
@property (strong,nonatomic) NSNumber *page;
@end

