//
//  YXCouponsViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXCouponsViewController : DDBaseViewController

// 0 :我的卡劵 1：选择卡劵
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSString *Id;

@property (nonatomic ,strong) NSString *sales_price;

@property (nonatomic ,copy)void(^selectCouponsBlock)(NSString *coupon_id,NSString *price_title,NSString *price);

@end

NS_ASSUME_NONNULL_END
