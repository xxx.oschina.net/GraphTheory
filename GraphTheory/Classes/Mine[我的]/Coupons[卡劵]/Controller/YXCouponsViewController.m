//
//  YXCouponsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCouponsViewController.h"
#import "YXCouponsViewModel.h"
#import "YXCouponsTableViewCell.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "YXCouponsModel.h"

@interface YXCouponsViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

@implementation YXCouponsViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.page = 1;
    [self requestUnRead];
}

- (void)refresh {
    YXWeakSelf;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf requestUnRead];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        weakSelf.page ++ ;
        [weakSelf requestUnRead];
    }];
}
- (void)requestUnRead{
    
    YXWeakSelf
    if (self.type == 1) {
        [YJProgressHUD showLoading:@"加载中"];
        [YXCouponsViewModel queryValidateServiceCouponWithId:self.Id sales_price:self.sales_price Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
            }
            if (REQUESTDATASUCCESS) {
                weakSelf.page++;
                YXCouponsModel *model = [YXCouponsModel mj_objectWithKeyValues:responesObj[@"data"]];
                NSArray *data = [YXCouponsModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.dataArr addObjectsFromArray:data];
                if (data.count < 10) {
                    [weakSelf.tableView.mj_footer setHidden:YES];
                }else{
                    [weakSelf.tableView.mj_footer setHidden:NO];
                }
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
        } failure:^(NSError * _Nonnull error) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }else {
        [YJProgressHUD showLoading:@"加载中"];
        [YXCouponsViewModel queryMyCouponListWithPage:@(_page).stringValue Completion:^(id  _Nonnull responesObj) {
            [YJProgressHUD hideHUD];
            if (weakSelf.page == 1) {
                [weakSelf.dataArr removeAllObjects];
            }
            if (REQUESTDATASUCCESS) {
                weakSelf.page++;
                YXCouponsModel *model = [YXCouponsModel mj_objectWithKeyValues:responesObj[@"data"]];
                NSArray *data = [YXCouponsModelList mj_objectArrayWithKeyValuesArray:model.list];
                [weakSelf.dataArr addObjectsFromArray:data];
                if (data.count < 10) {
                    [weakSelf.tableView.mj_footer setHidden:YES];
                }else{
                    [weakSelf.tableView.mj_footer setHidden:NO];
                }
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
            [weakSelf.tableView reloadData];
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
        } failure:^(NSError * _Nonnull error) {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }
    
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.type == 1) {
        self.title = @"选择卡劵";
    }else {
        self.title = @"卡劵";
    }
    _page = 1;
    [self loadSubViews];
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.bottom.left.top.right.equalTo(self.view);
        }
    }];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXCouponsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXCouponsTableViewCell"];
    if (!cell) {
        cell = [[YXCouponsTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXCouponsTableViewCell"];
    }
    cell.type = 0;
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type == 1) {
        YXCouponsModelList *model = self.dataArr[indexPath.row];
        if (self.selectCouponsBlock) {
            self.selectCouponsBlock(model.coupon_id,model.price_title,model.price);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - Lazy Loading
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[YXCouponsTableViewCell class] forCellReuseIdentifier:@"YXCouponsTableViewCell"];
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{
        }];
    }
    return _tableView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
