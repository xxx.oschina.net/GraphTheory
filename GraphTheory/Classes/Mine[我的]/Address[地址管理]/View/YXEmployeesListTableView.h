//
//  YXEmployeesListTableView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXEmployeesListTableView : UITableView

/**
 回调点击cell方法
 */
@property (nonatomic ,copy)void(^clickSelectIndex)(NSInteger index);

/**
 回调侧滑删除方法
 */
@property (nonatomic ,copy)void(^clickDeleteIndex)(NSInteger index);


/**
 数据源
 */
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

