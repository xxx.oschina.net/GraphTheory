//
//  YXAddressCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAddressCollectionViewCell.h"
#import "YXAddressModel.h"

@interface YXAddressCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *descLab;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *updateBtn;
@end

@implementation YXAddressCollectionViewCell

- (void)setModel:(YXAddressModel *)model {
    _model = model;
    _nameLab.text = _model.username;
    _phoneLab.text = _model.userphone;
    _addressLab.text = [NSString stringWithFormat:@"%@%@%@%@",_model.province,_model.city,_model.region,_model.address];
    if (_model.is_default == 1) {
        _descLab.hidden = NO;
    }else {
        _descLab.hidden = YES;
    }
}

- (IBAction)clickUpdate:(UIButton *)sender {
    if (self.clickUpdateBlock) {
        self.clickUpdateBlock(self.model);
    }
}
- (IBAction)clickDelete:(UIButton *)sender {
    if (self.clickDeleteBlock) {
        self.clickDeleteBlock(self.model);
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor whiteColor];
    _phoneLab.textColor = color_TextTwo;
    _addressLab.textColor = color_TextTwo;
    _descLab.textColor = color_TextTwo;
}

@end
