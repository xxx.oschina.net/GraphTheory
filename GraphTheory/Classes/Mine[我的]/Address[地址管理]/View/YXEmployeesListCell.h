//
//  YXEmployeesListCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXEmployeesModel;
@interface YXEmployeesListCell : UITableViewCell

@property (nonatomic ,strong) YXEmployeesModel *model;

@end


NS_ASSUME_NONNULL_END
