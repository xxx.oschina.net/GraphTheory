//
//  YXEmployeesListCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/12.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEmployeesListCell.h"
#import "YXEmployeesModel.h"
@interface YXEmployeesListCell ()


/** 姓名*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 状态*/
@property (nonatomic ,strong) UILabel *stateLab;
/** 岗位*/
@property (nonatomic ,strong) UILabel *jobsLab;

@end

@implementation YXEmployeesListCell

- (void)setModel:(YXEmployeesModel *)model {
    _model = model;
    
    _nameLab.text = [NSString stringWithFormat:@"%@(%@)",_model.name,_model.mobile];
    _jobsLab.text = [NSString stringWithFormat:@"%@",_model.roleName?_model.roleName:@""];
    if (_model.state ==0) {
        _stateLab.hidden = YES;
    }else {
        _stateLab.hidden = NO;
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.stateLab];
        [self.contentView addSubview:self.jobsLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)stateLab {
    if (!_stateLab) {
        _stateLab = [[UILabel alloc] init];
        _stateLab.textColor = color_RedColor;
        _stateLab.text = @"已禁用";
        _stateLab.font = [UIFont systemFontOfSize:10.0f];
        _stateLab.textAlignment = NSTextAlignmentCenter;
        _stateLab.layer.masksToBounds = YES;
        _stateLab.layer.cornerRadius = 4.0f;
        _stateLab.layer.borderColor = color_RedColor.CGColor;
        _stateLab.layer.borderWidth = 1.0f;
        _stateLab.hidden = YES;
    }
    return _stateLab;
}

- (UILabel *)jobsLab {
    if (!_jobsLab) {
        _jobsLab = [[UILabel alloc] init];
        _jobsLab.font = [UIFont systemFontOfSize:14.0f];
        _jobsLab.text = @"客服";
        _jobsLab.textColor = APPTintColor;
    }
    return _jobsLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@25);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_stateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.nameLab.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(43, 18));
    }];
    
    [_jobsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(@-25);
        [weakSelf.jobsLab sizeToFit];
    }];

}


@end



