//
//  YXAddressCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXAddressModel;
@interface YXAddressCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickUpdateBlock)(YXAddressModel *model);
@property (nonatomic ,copy)void(^clickDeleteBlock)(YXAddressModel *model);

@property (nonatomic ,strong) YXAddressModel *model;
@end

NS_ASSUME_NONNULL_END
