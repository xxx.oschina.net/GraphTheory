//
//  YXEmployeesModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/26.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXEmployeesModel : DDBaseModel

/** id*/
@property (nonatomic ,copy) NSString *Id;
/** 姓名*/
@property (nonatomic ,copy) NSString *name;
/** 手机号码*/
@property (nonatomic ,copy) NSString *mobile;
/** 角色id*/
@property (nonatomic ,copy) NSString *role;
/** 角色名称 */
@property (nonatomic ,copy) NSString *roleName;
/** 状态 0 是启用 1是禁用*/
@property (nonatomic ,assign) NSInteger state;



@end

NS_ASSUME_NONNULL_END
