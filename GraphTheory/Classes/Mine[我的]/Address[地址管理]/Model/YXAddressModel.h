//
//  YXAddressModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXAddressModel : DDBaseModel

/** id*/
@property (nonatomic ,copy) NSString *Id;
/** 姓名*/
@property (nonatomic ,copy) NSString *username;
/** 手机号码*/
@property (nonatomic ,copy) NSString *userphone;
/** 省*/
@property (nonatomic ,copy) NSString *province;
/** 市*/
@property (nonatomic ,copy) NSString *city;
/** 区*/
@property (nonatomic ,copy) NSString *region;
/** 省市区 */
@property (nonatomic ,copy) NSString *addressStr;
/** 详细地址 */
@property (nonatomic ,copy) NSString *address;
/** 默认地址*/
@property (nonatomic ,assign) NSInteger is_default;

@end

NS_ASSUME_NONNULL_END
