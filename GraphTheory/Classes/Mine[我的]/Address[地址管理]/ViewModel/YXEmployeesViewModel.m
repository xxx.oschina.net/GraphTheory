//
//  YXEmployeesViewModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXEmployeesViewModel.h"

@implementation YXEmployeesViewModel

#pragma mark -  查询员工列表
+ (void)queryStaffListWithToken:(NSString *)token
                        PageNum:(NSString *)pageNum
                         Search:(NSString *)search
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/staff/manage/queryStaffList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:token forKey:@"token"];
    [params setValue:pageNum forKey:@"pageNum"];
    [params setValue:@"20" forKey:@"pageSize"];
    [params setValue:search forKey:@"search"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
    
}


#pragma mark -  添加员工
+ (void)requestAddStaffWithName:(NSString *)name
                          Mobile:(NSString *)mobile
                          RoleId:(NSString *)roleId
                     StaffStatus:(NSString *)staffStatus
                      Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/staff/manage/addStaff",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:name forKey:@"name"];
    [params setValue:mobile forKey:@"mobile"];
    [params setValue:roleId forKey:@"roleId"];
    [params setValue:staffStatus forKey:@"staffStatus"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  删除员工
+ (void)requestDeleteStaffWithStaffId:(NSString *)staffId
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/staff/manage/deleteStaff",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:staffId forKey:@"id"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  修改员工
+ (void)requestUpdateStaffWithStaffId:(NSString *)staffId
                                 Name:(NSString *)name
                               Mobile:(NSString *)mobile
                               RoleId:(NSString *)roleId
                          StaffStatus:(NSString *)staffStatus
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/staff/manage/updateStaff",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:staffId forKey:@"id"];
    [params setValue:name forKey:@"name"];
    [params setValue:mobile forKey:@"mobile"];
    [params setValue:roleId forKey:@"roleId"];
    [params setValue:staffStatus forKey:@"staffStatus"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  启用禁用员工
+ (void)requestUpdateStaffStatusWithStaffId:(NSString *)staffId
                             PeopleStatus:(NSString *)peopleStatus
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/staff/manage/updateStatus",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:staffId forKey:@"id"];
    [params setValue:peopleStatus forKey:@"peopleStatus"];
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}


#pragma mark -  查询角色列表
+ (void)queryRoleListCompletion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@system/newRole/queryRoleList",kBaseURL];
    [NetWorkTools getWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

@end
