//
//  YXEmployeesViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/25.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

@interface YXEmployeesViewModel : DDBaseViewModel



/**
 查询员工列表

 @param token       token验证
 @param pageNum     当前页
 @param search      搜索内容
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryStaffListWithToken:(NSString *)token
                        PageNum:(NSString *)pageNum
                         Search:(NSString *)search
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;




/**
 添加员工

 @param name            姓名
 @param mobile          手机号
 @param roleId          角色id
 @param staffStatus     员工状态0启用1禁用
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestAddStaffWithName:(NSString *)name
                         Mobile:(NSString *)mobile
                         RoleId:(NSString *)roleId
                    StaffStatus:(NSString *)staffStatus
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;





/**
 删除员工

 @param staffId         员工id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestDeleteStaffWithStaffId:(NSString *)staffId
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;




/**
 修改员工

 @param staffId         员工id
 @param name            姓名
 @param mobile          手机号
 @param roleId          角色id
 @param staffStatus     员工状态
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateStaffWithStaffId:(NSString *)staffId
                                 Name:(NSString *)name
                               Mobile:(NSString *)mobile
                               RoleId:(NSString *)roleId
                          StaffStatus:(NSString *)staffStatus
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;




/**
 启用禁用员工

 @param staffId         员工id
 @param peopleStatus    人员状态,0启用,1禁用
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateStaffStatusWithStaffId:(NSString *)staffId
                             PeopleStatus:(NSString *)peopleStatus
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError;




/**
 查询角色列表

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryRoleListCompletion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

@end

