//
//  YXAddrssListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAddrssListViewController.h"
#import "YXCreateAddressViewController.h"
#import "YXAddressCollectionViewCell.h"
#import "YXUserViewModel.h"
#import "YXCustomAlertActionView.h"
#import "YXAddressModel.h"
#import "UIScrollView+DREmptyDataSet.h"

@interface YXAddrssListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) YXCustomAlertActionView *alertView;
@property (strong, nonatomic) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) YXAddressModel *model;
@end

@implementation YXAddrssListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"地址信息";
    // Do any additional setup after loading the view.
    self.dataArr = [NSMutableArray array];
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 30, 30);
    [btn setImage:[UIImage imageNamed:@"plus"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *addItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = addItem;
    
    [self setup];

}

- (void)click {
    YXCreateAddressViewController *vc = [[YXCreateAddressViewController alloc] init];
    vc.type = EmployeesTypeStateAdd;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 加载刷新
    [self addRefrsh];
    
}
- (void)addRefrsh {
    
    __weak typeof(self)weakSelf = self;
    _collectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf loadData];
    }];
    _collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadData];
    }];
    [_collectionView.mj_header beginRefreshing];
    
}


- (void)loadData {
    DDWeakSelf
    [YJProgressHUD showLoading:@"加载中"];
    [YXUserViewModel queryUserAddressListCompletion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            weakSelf.page++;
            NSArray *data = [YXAddressModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
            [weakSelf.dataArr addObjectsFromArray:data];
            if (data.count < 20) {
                [weakSelf.collectionView.mj_footer setHidden:YES];
            }else{
                [weakSelf.collectionView.mj_footer setHidden:NO];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

// 请求删除地址
- (void)requestDelete{
    
    [YXUserViewModel requestdeleteUserAddressId:self.model.Id Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

- (void)setup {
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.top.mas_equalTo(self.view);
            make.bottom.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArr.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREEN_WIDTH, 120);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(15, 15, 15, 15);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXAddressCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXAddressCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.dataArr[indexPath.row];
    YXWeakSelf;
    [cell setClickUpdateBlock:^(YXAddressModel * _Nonnull model) {
        YXCreateAddressViewController *vc = [[YXCreateAddressViewController alloc] init];
        vc.type = EmployeesTypeStateDetail;
        vc.model = model;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [cell setClickDeleteBlock:^(YXAddressModel * _Nonnull model) {
        weakSelf.model = model;
        [weakSelf deleteBtnAction];
    }];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.type ==1) {
        YXAddressModel *model = self.dataArr[indexPath.row];
        if (self.selectAddressBlock) {
            self.selectAddressBlock(model);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

// 点击删除员工按钮
- (void)deleteBtnAction {
    [self.alertView showAnimation];
}

#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXAddressCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXAddressCollectionViewCell"];
        DDWeakSelf
        [_collectionView setupEmptyDataText:@"暂无数据" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        }];
    }
    return _collectionView;
}
- (YXCustomAlertActionView *)alertView {
    if (!_alertView) {
        _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:@"确认删除该地址吗？" sureBtn:@"确定" cancleBtn:@"取消"];
        YXWeakSelf
        [_alertView setSureClick:^(NSString * _Nonnull string) {
            [weakSelf requestDelete];
        }];
    }
    return _alertView;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
