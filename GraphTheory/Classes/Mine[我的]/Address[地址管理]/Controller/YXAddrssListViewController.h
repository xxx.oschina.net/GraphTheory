//
//  YXAddrssListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXAddressModel;
@interface YXAddrssListViewController : DDBaseViewController

@property (nonatomic ,copy)void(^selectAddressBlock)(YXAddressModel *model);

@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
