//
//  YXCreateAddressViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCreateAddressViewController.h"
#import "YXPublicCell.h"
#import "SelectView.h"

#import "YXEmployeesModel.h"
#import "YXEmployeesViewModel.h"
#import "YXUserViewModel.h"
#import "YXAddressModel.h"

@interface YXCreateAddressViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic ,strong) NSArray *titlesArr;
@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation YXCreateAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"地址信息";
    if (self.type == EmployeesTypeStateAdd) {
        self.model = [[YXAddressModel alloc] init];
    }else if (self.type == EmployeesTypeStateDetail){
        self.model.addressStr = [NSString stringWithFormat:@"%@%@%@",self.model.province,self.model.city,self.model.region];
    }
    self.titlesArr = @[@"姓名",@"手机号",@"所选省市区",@"详细地址",@"设置为默认地址"];
    [self.view addSubview:self.tableView];

}


#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.titlesArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    YXWeakSelf
    if (indexPath.row == 0 || indexPath.row == 1 ||indexPath.row == 3) {
        YXPublicCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell2"];
        if (!cell) {
            cell = [[YXPublicCell2 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell2"];
        }
        cell.titleLab.text = self.titlesArr[indexPath.row];
        cell.contentTF.font = [UIFont systemFontOfSize:14.0f];
        if (self.type == EmployeesTypeStateAdd) {
            cell.contentTF.placeholder = @"请输入";
        }else {
            if (indexPath.row == 0) {
                cell.contentTF.text = self.model.username;
            }else if (indexPath.row == 1) {
                cell.contentTF.text = self.model.userphone;
            }else {
                cell.contentTF.text = self.model.address;
            }
        }
        YXWeakCell
        if (indexPath.row == 0) {
            cell.contentTF.keyboardType = UIKeyboardTypeDefault;
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 16) {
                    weakCell.contentTF.text = [text substringToIndex:16];
                    weakSelf.model.username = weakCell.contentTF.text;
                }else {
                    weakSelf.model.username = text;
                    
                }
            }];
        }else if (indexPath.row ==1){
            cell.contentTF.keyboardType = UIKeyboardTypeNumberPad;
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 11) {
                    weakCell.contentTF.text = [text substringToIndex:11];
                    weakSelf.model.userphone = weakCell.contentTF.text;
                }else {
                    weakSelf.model.userphone = text;
                }
            }];
        }else if (indexPath.row ==3){
            cell.contentTF.keyboardType = UIKeyboardTypeDefault;
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 30) {
                    weakCell.contentTF.text = [text substringToIndex:30];
                    weakSelf.model.address = weakCell.contentTF.text;
                }else {
                    weakSelf.model.address = text;
                }
            }];
        }
        
        return cell;
    }else if (indexPath.row == 2) {
        static NSString *cellID = @"YXEmployeesTypeCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
        cell.textLabel.textColor = color_TextOne;
        cell.textLabel.text= self.titlesArr[indexPath.row];
        cell.detailTextLabel.textColor = color_TextThree;
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];
//        if (self.type == EmployeesTypeStateAdd) {
//            cell.detailTextLabel.text = @"请选择";
//        }else {
//            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@%@%@",self.model.province,self.model.city,self.model.region];
//        }
        if (![_model.addressStr isEqualToString:@""] && ![_model.addressStr isEqualToString:@"请选择"]) {
            cell.detailTextLabel.textColor = color_TextOne;
            cell.detailTextLabel.text = _model.addressStr;
        } else {
            cell.detailTextLabel.text = @"请选择";
            cell.detailTextLabel.textColor = color_TextThree;
        }
        
        YXWeakCell
        [cell.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakCell.mas_centerY);
            make.left.equalTo(@15.0);
            [weakCell.textLabel sizeToFit];
        }];
        
        return cell;
    }else {
        YXPublicCell5 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell5"];
        if (!cell) {
            cell = [[YXPublicCell5 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell5"];
        }
        cell.titleLab.text= self.titlesArr[indexPath.row];
        cell.isSwitch.on = self.model.is_default;
        [cell setSwtichOnOrOff:^(BOOL status) {
            weakSelf.model.is_default = status;
        }];
        return cell;
    }
    
    return [[UITableViewCell alloc] init];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        SelectView *city = [[SelectView alloc] initWithZGQFrame:[UIScreen mainScreen].bounds SelectCityTtitle:@"城市选择"];
        DDWeakSelf
       [city showCityView:^(NSString *proviceStr, NSString *cityStr, NSString *disStr) {
           weakSelf.model.province = proviceStr;
           weakSelf.model.city = cityStr;
           weakSelf.model.region = disStr;
           weakSelf.model.addressStr = [NSString stringWithFormat:@"%@%@%@",self.model.province,self.model.city,self.model.region];
           NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
           if (indexPath) {
               [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationFade)];
           }
       }];
    }
    
}
#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, kHEIGHT)) style:(UITableViewStylePlain)];
        _tableView.backgroundColor = color_LineColor;
        _tableView.separatorColor = color_LineColor;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [self createFooterView];
        [_tableView registerClass:[YXPublicCell2 class] forCellReuseIdentifier:@"YXPublicCell2"];
        [_tableView registerClass:[YXPublicCell5 class] forCellReuseIdentifier:@"YXPublicCell5"];
    }
    return _tableView;
}


- (UIView *)createFooterView {
    
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 100))];
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(20, 30, KWIDTH - 40, 44);
    btn.backgroundColor = APPTintColor;
    [btn setTitle:@"提交" forState:(UIControlStateNormal)];
    [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    btn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 4.0f;
    [btn addTarget:self action:@selector(save:) forControlEvents:(UIControlEventTouchUpInside)];
    [footerView addSubview:btn];
    return footerView;
}


#pragma mark - Touch Even
// 点击保存按钮
- (void)save:(UIButton *)sender {
    
    if ([NSString isBlankString:self.model.username]) {
        return [YJProgressHUD showMessage:@"请输入姓名"];
    }
    
    if ([NSString isBlankString:self.model.userphone]) {
        return [YJProgressHUD showMessage:@"请输入手机号"];
    }
    
    if ([NSString isBlankString:self.model.address]) {
        return [YJProgressHUD showMessage:@"请输入详细地址"];
    }
    
    YXWeakSelf
    // 添加地址
    if (self.type == EmployeesTypeStateAdd) {
        [YJProgressHUD showLoading:@"保存中..."];
        [YXUserViewModel requestCreateUserAddressWithType:0 Id:nil userphone:self.model.userphone username:self.model.username province:self.model.province city:self.model.city region:self.model.region address:self.model.address is_default:@(self.model.is_default).stringValue Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                if (weakSelf.createAddressBlock) {
                    weakSelf.createAddressBlock();
                }
                [weakSelf.navigationController popViewControllerAnimated:YES];
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showMessage:REQUESTERR];
        }];

    }else { // 修改地址信息
        
        [YJProgressHUD showLoading:@"保存中..."];
        [YXUserViewModel requestCreateUserAddressWithType:1 Id:self.model.Id userphone:self.model.userphone username:self.model.username province:self.model.province city:self.model.city region:self.model.region address:self.model.address is_default:@(self.model.is_default).stringValue Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }

}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
