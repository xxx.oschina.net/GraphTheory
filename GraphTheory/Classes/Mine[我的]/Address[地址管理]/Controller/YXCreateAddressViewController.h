//
//  YXCreateAddressViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

typedef NS_ENUM(NSInteger,EmployeesType) {
    EmployeesTypeStateAdd,          // 新增员工
    EmployeesTypeStateDetail,       // 员工详情
};

@class YXAddressModel;
@interface YXCreateAddressViewController : DDBaseViewController

@property (nonatomic ,assign) EmployeesType type;
/** 地址信息*/
@property (nonatomic ,strong) YXAddressModel *model;

/** 选择地址回调*/
@property (nonatomic ,copy)void(^selectAddressBlock)(YXAddressModel *model);
/** 添加地址回调*/
@property (nonatomic ,copy)void(^createAddressBlock)(void);

@end
