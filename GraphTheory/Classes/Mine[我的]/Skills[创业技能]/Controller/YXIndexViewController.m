//
//  YXIndexViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXIndexViewController.h"

@interface YXIndexViewController ()
@property (weak, nonatomic) IBOutlet UIButton *scoreBtn;

@end

@implementation YXIndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"达人指数";
    self.scoreBtn.layer.masksToBounds = YES;
    self.scoreBtn.layer.cornerRadius = 4.0;
    self.scoreBtn.layer.borderWidth =1.0;
    self.scoreBtn.layer.borderColor = APPTintColor.CGColor;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
