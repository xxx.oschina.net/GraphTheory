//
//  YXSkillsViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXSkillsViewController : DDBaseViewController

@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
