//
//  YXCouponCodeViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXCouponCodeViewController.h"
#import "YXOrderInfoModel.h"
#import "YXOrderViewModel.h"
@interface YXCouponCodeViewController ()
@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UILabel *codeLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UIButton *stateBtn;
@property (weak, nonatomic) IBOutlet UIButton *validationBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backH;

@end

@implementation YXCouponCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"劵码验证";
    [self setup];
    
    self.codeLab.text = self.model.order_no;
    self.typeLab.text = self.model.product_details.name;
    self.nameLab.text = self.model.receiver.username;
    self.timeLab.text = self.model.create_time;
    self.priceLab.text = [NSString stringWithFormat:@"¥%@",self.model.sales_price];
    self.contentLab.text = self.model.content;

}

- (void)setup {
    
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.stateBtn.layer.masksToBounds = YES;
    self.stateBtn.layer.cornerRadius = 4.0;
    self.stateBtn.layer.borderWidth =1.0;
    self.stateBtn.layer.borderColor = APPTintColor.CGColor;
    [self.stateBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
    self.validationBtn.layer.masksToBounds = YES;
    self.validationBtn.layer.cornerRadius = 4.0;
    self.validationBtn.backgroundColor = APPTintColor;
    [self.validationBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
}

- (IBAction)saveAction:(UIButton *)sender {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel takeSubOrderByWithCoupon_code:self.model.coupon_code Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.stateBtn setTitle:@"状态: 已完成" forState:(UIControlStateNormal)];
            weakSelf.validationBtn.hidden = YES;
            weakSelf.backH.constant = 300;
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
