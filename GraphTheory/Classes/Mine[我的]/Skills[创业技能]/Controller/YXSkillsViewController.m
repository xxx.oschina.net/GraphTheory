//
//  YXSkillsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/1.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXSkillsViewController.h"
#import "DDWorkHeaderView.h"
#import "YXServiceCollectionViewCell.h"
#import "YXVideoModel.h"
#import "YXApplyRoleInfoModel.h"
@interface YXSkillsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

@implementation YXSkillsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的技能";
    
    [self loadData];
    
    [self loadSubViews];
}

- (void)loadData {
    
    DDWeakSelf
    [YXUserViewModel queryUserInfoCompletion:^(id responesObj) {
        [weakSelf.dataArr removeAllObjects];
        YXUserInfoModel *userInfo = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
        if (weakSelf.type == 0) {
            weakSelf.dataArr = [YXApplyRoleInfoModelApplyInfoServices mj_objectArrayWithKeyValuesArray:userInfo.applyInfo[@"worker"][@"services"]];
        }else {
            weakSelf.dataArr = [YXBrandModel mj_objectArrayWithKeyValuesArray:userInfo.applyInfo[@"video"][@"services"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
    }];

}

- (void)loadSubViews {
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.top.mas_equalTo(self.view);
            make.bottom.mas_equalTo(self.view);
        }
        make.right.left.mas_equalTo(self.view);
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.type == 0) {
        YXApplyRoleInfoModelApplyInfoServices *brandModel = self.dataArr[section];
        return brandModel.child.count;
    }else {
        YXBrandModel *brandModel = self.dataArr[section];
        return brandModel.child.count;
    }
 
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KWIDTH - 60)/3, 32);
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(KWIDTH, 40);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        DDWorkHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDWorkHeaderView" forIndexPath:indexPath];
        header.leftCons = 0;
        if (self.type == 0) {
            YXApplyRoleInfoModelApplyInfoServices *brandModel = self.dataArr[indexPath.section];
            header.titleLab.text = brandModel.name;
        }else {
            YXBrandModel *brandModel = self.dataArr[indexPath.section];
            header.titleLab.text = brandModel.brand_name;
        }
        header.rightBtn.hidden = YES;
        header.rightImg.hidden = YES;
        return header;
    }
    return nil;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXServiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXServiceCollectionViewCell" forIndexPath:indexPath];
    cell.selectImg.hidden = YES;
    if (self.type == 0) {
        YXApplyRoleInfoModelApplyInfoServices *brandModel = self.dataArr[indexPath.section];
        YXApplyRoleInfoModelApplyInfoServicesChild *model = brandModel.child[indexPath.row];
        cell.titleLab.text = model.name;
    }else {
        YXBrandModel *brandModel = self.dataArr[indexPath.section];
        YXBrandModel *model = brandModel.child[indexPath.row];
        cell.titleLab.text = model.brand_name;
    }
     return cell;
}

#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 15;
        layout.minimumInteritemSpacing = 15;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;

        [_collectionView registerClass:[DDWorkHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDWorkHeaderView"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXServiceCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXServiceCollectionViewCell"];
    }
    return _collectionView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
