//
//  YXCouponCodeViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/7.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXCouponCodeViewController : DDBaseViewController
@property (nonatomic ,strong) YXOrderInfoModel *model;
@end

NS_ASSUME_NONNULL_END
