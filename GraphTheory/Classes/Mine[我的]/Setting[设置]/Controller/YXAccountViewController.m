//
//  YXSettingViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/27.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXAccountViewController.h"
#import "YXWebViewController.h"
#import "YXAccountTypeViewController.h"
@interface YXAccountViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) NSArray *titles;

@end

@implementation YXAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"账号与安全";
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.offset(0);
        }
    }];
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"YXSettingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    cell.textLabel.textColor = color_TextOne;
    cell.textLabel.text = self.titles[indexPath.row];
    UIImageView *rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"advert_arrow_right"]];
     cell.accessoryView = rightImg;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    YXAccountTypeViewController *vc = [YXAccountTypeViewController new];
    vc.type = indexPath.row;
    [self.navigationController pushViewController:vc animated:YES];

//    if (indexPath.row == 2) {
//        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",[YXUserInfoManager getUserInfo].serviceTel?:@"400-666-1127"];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
//    }

}
- (NSArray *)titles {
    if (!_titles) {
        _titles = @[@"设置登录密码",@"绑定手机号",@"绑定支付宝账号"];
    }
    return _titles;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.contentInset = UIEdgeInsetsMake(10,0, 0, 0);
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}

@end
