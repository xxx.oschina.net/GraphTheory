//
//  YXInputViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXInputViewController : DDBaseViewController

@property (nonatomic ,copy)void(^saveBlock)(NSString *content);

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSString *content;

@end

NS_ASSUME_NONNULL_END
