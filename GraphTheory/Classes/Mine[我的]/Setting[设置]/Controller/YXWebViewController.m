//
//  YXWebViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/2.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXWebViewController.h"
#import <WebKit/WebKit.h>
#import <WKWebViewJavascriptBridge.h>
#import "LMWebProgressLayer.h"

static void *KINWebBrowserContext = &KINWebBrowserContext;

@interface YXWebViewController ()<WKUIDelegate,WKNavigationDelegate>

@property (strong, nonatomic)  WKWebView *webView;
@property (strong, nonatomic)  LMWebProgressLayer *progressLayer; ///< 网页加载进度条
@property (strong, nonatomic)  WKWebViewJavascriptBridge* bridge;

@end

@implementation YXWebViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)dealloc {
    [self.webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
//    [self.webView removeObserver:self forKeyPath:@"title"];
    [self finishedLoad];
    [self.webView setNavigationDelegate:nil];
    [self.webView setUIDelegate:nil];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == self.webView) {
//        self.title= self.webView.title;
        if ([self.webView canGoBack]) {
            self.navigationItem.rightBarButtonItem=nil;
        }else{
        }
        if(self.webView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
            } completion:^(BOOL finished) {
            }];
        }
//    } else if ([keyPath isEqualToString:@"title"]){
//        if (object == self.webView)
//        {
//            self.title = self.webView.title;
//        }
//        else
//        {
//            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
//        }
    }else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=self.titleStr;
    [self.view addSubview:self.webView];
    [self.view.layer addSublayer:self.progressLayer];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.mas_equalTo(self.view);
        }
    }];
    
    
    [self.webView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:KINWebBrowserContext];
//    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    if (_url.length > 0) {
//        _url = [_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_url]]];
        // 设置行间距26px
        _url = [NSString stringWithFormat:@"<div style=\"line-height: 26px\">%@",_url];
        [self.webView loadHTMLString:self.url baseURL:nil];
    }
    
    if (_bridge) { return; }
    [WKWebViewJavascriptBridge enableLogging];
    _bridge = [WKWebViewJavascriptBridge bridgeForWebView:self.webView];
    [_bridge setWebViewDelegate:self];
      //申明js调用oc方法的处理事件，这里写了后，h5那边只要请求了，oc内部就会响应
//      [self JS2OC];
}

- (void)JS2OC{
    /*
     含义：JS调用OC
     @param registerHandler 要注册的事件名称(比如这里我们为loginAction)
     @param handel 回调block函数 当后台触发这个事件的时候会执行block里面的代码
     */
    [_bridge registerHandler:@"click" handler:^(id data, WVJBResponseCallback responseCallback) {
        // data js页面传过来的参数  假设这里是用户名和姓名，字典格式
        NSLog(@"JS调用OC，并传值过来");
        // 利用data参数处理自己的逻辑

    }];
}



#pragma mark - WKWebView Delegate
-(void)webView:(WKWebView*)webView didStartProvisionalNavigation:(WKNavigation*)navigation{
    [_progressLayer startLoad];
}
-(void)webView:(WKWebView*)webView didFinishNavigation:(WKNavigation*)navigation{
    //禁止用户选择
//    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:nil];
//    [webView evaluateJavaScript:@"document.activeElement.blur();" completionHandler:nil];
//    // 适当增大字体大小
//    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'" completionHandler:nil];
    //修改字体颜色
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#444444'"completionHandler:nil];
    [self finishedLoad];
}
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [self finishedLoad];
}

- (void)finishedLoad {
    [_progressLayer finishedLoad];
    _progressLayer = nil;
    [_progressLayer closeTimer];
}
- (void)setUrl:(NSString *)url {
    _url = url;
}

- (WKWebView *)webView{
    if (!_webView) {
        //js脚本
        NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
        //注入
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES]; WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];
        //配置对象
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;
        // 创建设置对象
        WKPreferences *preference = [[WKPreferences alloc]init];
        // 设置字体大小(最小的字体大小)
        preference.minimumFontSize = 14;
        // 设置偏好设置对象
        wkWebConfig.preferences = preference;
        
        _webView = [[WKWebView alloc]initWithFrame:CGRectZero configuration:wkWebConfig];
        _webView.scrollView.showsVerticalScrollIndicator = NO;
        _webView.scrollView.showsHorizontalScrollIndicator = NO;
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
    }
    return _webView;
}
- (LMWebProgressLayer *)progressLayer {
    if (!_progressLayer) {
        _progressLayer = [LMWebProgressLayer new];
        _progressLayer.frame = CGRectMake(0, 0, KWIDTH, 1.5);
    }
    return _progressLayer;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
