//
//  YXAboutUsViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXAboutUsViewController.h"
#import "YXWebViewController.h"
#import "YXUserViewModel.h"
@interface YXAboutUsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;

@property (nonatomic ,strong) NSArray *titles;

@end

@implementation YXAboutUsViewController

- (NSArray *)titles {
    if (!_titles) {
        NSString  *appVersion = BundleIdentifier;
        _titles = @[@"关于图论社",[NSString stringWithFormat:@"当前版本V%@",appVersion]];
    }
    return _titles;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.backgroundColor = color_LineColor;
        _tableView.separatorColor = color_LineColor;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = [self createHeaderView];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于图论社";
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.offset(0);
        }
    }];
}

- (void)request {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAboutUsCompletion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = @"关于图论社";
        vc.url = responesObj[@"data"][@"us_content"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error) {
        
    }];
    
}


#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"YXAboutUsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    cell.textLabel.textColor = color_TextOne;
    cell.textLabel.text = self.titles[indexPath.row];
    if (indexPath.row == 0) {
        UIImageView *rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"advert_arrow_right"]];
        cell.accessoryView = rightImg;
    }else if (indexPath.row == 1) {
        UIImageView *rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"about_us_copy"]];
        cell.accessoryView = rightImg;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.dd2007.com"]];
        [self request];
    }else {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = @"cos.dd2007.com";
    }
}

- (UIView *)createHeaderView {
    
    UIView *headerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 270))];
    headerView.backgroundColor = color_LineColor;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:(CGRectMake((KWIDTH - 60)/2, 50, 100, 60))];
    imgView.image = [UIImage imageNamed:@"about_us"];
    [headerView addSubview:imgView];
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:(CGRectMake(0, CGRectGetMaxY(imgView.frame) + 30, KWIDTH, 20))];
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.textColor = color_TextOne;
    titleLab.font = [UIFont systemFontOfSize:16.0f];
    titleLab.text = @"图论社";
    [headerView addSubview:titleLab];
    
    return headerView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
