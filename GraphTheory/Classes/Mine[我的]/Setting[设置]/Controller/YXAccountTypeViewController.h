//
//  YXAccountTypeViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,AccountType) {
    AccountTypeStatePwd,        // 登录密码
    AccountTypeStatePhone,      // 绑定手机号
    AccountTypeStateZFB,        // 绑定支付宝
    AccountTypeStateNext,       // 设置登录密码
};

@interface YXAccountTypeViewController : DDBaseViewController

@property (nonatomic ,assign) AccountType type;

@property (nonatomic, strong) NSString *phone; // 手机号
@property (nonatomic, strong) NSString *code; // 验证码

@end

NS_ASSUME_NONNULL_END
