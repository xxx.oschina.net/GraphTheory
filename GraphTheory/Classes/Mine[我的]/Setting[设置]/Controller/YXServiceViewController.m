//
//  YXServiceViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXServiceViewController.h"
#import "YXUserViewModel.h"
#import "YXServiceContentView.h"
#import "LBXScanNative.h"
@interface YXServiceViewController ()

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *contentLab;
@property (nonatomic ,strong) YXServiceContentView *contentView;

@end

@implementation YXServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"客服中心";
    [self addSubViews];
    
    [self loadData];

}

- (void)loadData {
    
    DDWeakSelf
    [YXUserViewModel queryServiceDataCompletion:^(id responesObj) {
        NSDictionary *dic = responesObj[@"data"];
        if (dic.count) {
            UIImage *image = [LBXScanNative createQRWithString:dic[@"ab_route"] QRSize:CGSizeMake(200, 200)];
            weakSelf.contentView.codeImg.image = image;
        }
     
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)addSubViews {
    
    [self.view addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
}

- (YXServiceContentView *)contentView {
    if (!_contentView) {
        _contentView = [[YXServiceContentView alloc] initWithFrame:CGRectZero];
    }
    return _contentView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
