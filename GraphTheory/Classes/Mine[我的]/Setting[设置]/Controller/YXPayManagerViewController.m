//
//  YXPayManagerViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXPayManagerViewController.h"
#import "WGPublicBottomView.h"
#import "YXUserViewModel.h"
#import "XMPayCodeView.h"

@interface YXPayManagerViewController ()
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (strong, nonatomic) XMPayCodeView *payCodeView;
@property (nonatomic ,strong) NSString *pwdStr;
@end

@implementation YXPayManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付密码设置";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubViews];
    [self.payCodeView becomeKeyBoardFirstResponder];
}

- (void)addSubViews {
    
    [self.view addSubview:self.titleLab];
    [self.view addSubview:self.payCodeView];
    [self.view addSubview:self.bottomView];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@20);
        make.left.equalTo(@10);
        [self.titleLab sizeToFit];
    }];
    [self.payCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLab.mas_bottom).offset(20);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@60);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.payCodeView.mas_bottom).offset(30);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@60);
    }];

}


- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16.0f];
        _titleLab.text = @"请设置支付密码";
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"提交"];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf saveMethods];
        }];
    }
    return _bottomView;
}

- (XMPayCodeView *)payCodeView {
    if (!_payCodeView) {
        _payCodeView = [[XMPayCodeView alloc]initWithFrame:CGRectZero];
        _payCodeView.secureTextEntry = YES;
        _payCodeView.endEditingOnFinished = NO;
        DDWeakSelf
        [_payCodeView setPayBlock:^(NSString *payCode) {
            NSLog(@"payCode==%@",payCode);
            weakSelf.pwdStr = payCode;
        }];
    }
    return _payCodeView;
}


- (void)saveMethods{
    
    if ([NSString isBlankString:self.pwdStr]) {
        return [YJProgressHUD showMessage:@"支付密码不能为空！"];
    }
    
    if (self.pwdStr.length < 6) {
        return [YJProgressHUD showMessage:@"请输入6位支付密码！"];
    }
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel requestUpdatePayPwdWithPay_pwd:self.pwdStr Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:@"支付密码设置成功！"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
