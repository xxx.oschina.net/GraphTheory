//
//  YXInputViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXInputViewController.h"
#import "YXCouponCodeViewController.h"
#import "WGPublicBottomView.h"
#import "YXOrderViewModel.h"
#import "YXOrderInfoModel.h"
@interface YXInputViewController ()
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) UIView *inputView;
@property (nonatomic ,strong) UITextField *textField;
@end

@implementation YXInputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (self.type == 0) {
        self.title = @"修改姓名";
    }else if (self.type == 1) {
        self.title = @"修改毕业院校";
    }else if (self.type == 2) {
        self.title = @"修改个性签名";
    }else if (self.type == 3) {
        self.title = @"劵码验证";
    }
    
    [self addSubViews];
}
- (void)addSubViews {
    
    [self.view addSubview:self.inputView];
    [self.inputView addSubview:self.textField];
    [self.view addSubview:self.bottomView];
    [self.inputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15);
        make.right.equalTo(@-15);
        make.height.equalTo(@50);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.inputView.mas_left).offset(15);
        make.top.bottom.right.equalTo(self.inputView);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputView.mas_bottom).offset(15);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@60);
    }];
}

#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    self.content = sender.text;
}


#pragma mark - Lazy Loading
- (UIView *)inputView {
    if (!_inputView) {
        _inputView = [[UIView alloc] initWithFrame:CGRectZero];
        _inputView.backgroundColor = [UIColor whiteColor];
        _inputView.layer.masksToBounds = YES;
        _inputView.layer.cornerRadius = 4.0;
    }
    return _inputView;
}
- (UITextField *)textField {
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.font = [UIFont systemFontOfSize:15];
        _textField.backgroundColor = [UIColor whiteColor];
        if (self.type == 3) {
            _textField.placeholder = @"请输入有效劵码";
            _textField.keyboardType = UIKeyboardTypeNumberPad;
        }else {
            _textField.placeholder = @"请输入";
        }
        [_textField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _textField;
}

- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        if (self.type == 3) {
            [_bottomView setTitle:@"点击验证"];
        }else {
            [_bottomView setTitle:@"提交"];
        }
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            if (weakSelf.type == 3) {
                [weakSelf queryCode];
            }else {
                [weakSelf saveMethods];
            }
        }];
    }
    return _bottomView;
}

- (void)saveMethods {
    
    if ([NSString isBlankString:self.content]) {
        return [YJProgressHUD showMessage:@"请输入内容"];
    }
    if (self.saveBlock) {
        self.saveBlock(self.content);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)queryCode {
    
    if ([NSString isBlankString:self.content]) {
        return [YJProgressHUD showMessage:@"请输入有效劵码"];
    }
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel getSubOrderInfoByWithCoupon_code:self.content Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            YXOrderInfoModel *model = [YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            YXCouponCodeViewController *vc = [YXCouponCodeViewController new];
            vc.model = model;
            [weakSelf.navigationController pushViewController:vc animated:YES];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
        
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
