//
//  YXMerchantsViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXSettingViewController.h"
#import "YXUserInfoViewController.h"
#import "YXAccountViewController.h"
#import "YXAboutUsViewController.h"
#import "YXAddrssListViewController.h"
#import "YXPayManagerViewController.h"
#import "YXWebViewController.h"

#import "YXUserViewModel.h"
#import "WGPublicBottomView.h"
#import "YXCustomAlertActionView.h"
@interface YXSettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSArray *titles;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXCustomAlertActionView *alertView;

@end

@implementation YXSettingViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";

    [self loadSubViews];
}

- (void)queryAbout {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAboutOtherInfoWithType:@"7" Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = responesObj[@"data"][@"us_title"];
        vc.url = responesObj[@"data"][@"us_content"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.titles[section] count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        static NSString *cellID = @"YXMerchantNameCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = self.titles[indexPath.section][indexPath.row];
        if (indexPath.row == 0) {
            cell.detailTextLabel.text = @"头像、昵称、生日";
        }else {
            cell.detailTextLabel.text = @"登录密码、更换手机";
        }
        cell.detailTextLabel.textColor = color_TextThree;
        return cell;
    }else {
        static NSString *cellID = @"YXMerchantCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellID];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        cell.textLabel.textColor = color_TextOne;
        cell.textLabel.text = self.titles[indexPath.section][indexPath.row];
        return cell;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            YXUserInfoViewController *vc = [YXUserInfoViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            YXAccountViewController *vc = [YXAccountViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 1) {
        YXAddrssListViewController *vc = [YXAddrssListViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            YXPayManagerViewController *vc = [YXPayManagerViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            [self queryAbout];
        }
    }else if (indexPath.section == 3) {
        YXAboutUsViewController *vc = [YXAboutUsViewController new];
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}

#pragma mark - Lazy Loading
- (NSArray *)titles {
    if (!_titles) {
        _titles = @[@[@"个人信息",@"账户安全"],@[@"通信地址"],@[@"支付设置",@"通用及隐私"],@[@"关于图论社"]];
    }
    return _titles;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    }
    return _tableView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"退出账号"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf.alertView showAnimation];
        }];
    }
    return _bottomView;
}
- (YXCustomAlertActionView *)alertView {
    if (!_alertView) {
        _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:@"确认退出登录吗？" sureBtn:@"确定" cancleBtn:@"取消"];
        YXWeakSelf
        [_alertView setSureClick:^(NSString * _Nonnull string) {
            [YJProgressHUD hideHUD];
            [weakSelf cleanUserInfo];
        }];
    }
    return _alertView;
}
#pragma mark - 清空用户信息返回登录页
- (void)cleanUserInfo {
    [NetWorkTools pushLoginVC];
}

@end
