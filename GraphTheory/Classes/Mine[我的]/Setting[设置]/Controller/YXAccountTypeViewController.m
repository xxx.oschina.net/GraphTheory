//
//  YXAccountTypeViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/18.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAccountTypeViewController.h"
#import "YXPublicCell.h"
#import "BYTimer.h"
#import "YXLonginViewModel.h"
#import "YXUserViewModel.h"
#import "YXAlipayModel.h"
@interface YXAccountTypeViewController ()<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>

@property (nonatomic ,strong) NSArray *titlesArr;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *getCodeBtn;

@property (nonatomic, strong) NSString *account; // 账号
@property (nonatomic, strong) NSString *name; // 姓名
@property (nonatomic, strong) NSString *onePwd; //新密码
@property (nonatomic, strong) NSString *twoPwd; //确认密码

@end

@implementation YXAccountTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    // Do any additional setup after loading the view.
    if (self.type == AccountTypeStatePwd) {
        self.title = @"设置登录密码";
        self.titlesArr = @[@"请完成认证",@"请输入短信验证码"];
        self.phone = [YXUserInfoManager getUserInfo].mobile;
    }else if (self.type == AccountTypeStatePhone) {
        self.title = @"绑定手机号";
        self.titlesArr = @[@"请输入新的手机号",@"请输入短信验证码"];
    }else if (self.type == AccountTypeStateZFB) {
        self.title = @"绑定支付宝账号";
        self.titlesArr = @[@"请输入姓名",@"请输入支付宝账号"];
        [self queryUserAlipay];
    }else if (self.type == AccountTypeStateNext) {
        self.title = @"设置登录密码";
        self.titlesArr = @[@"请输入新密码",@"请确认密码"];
    }
    
    [self.view addSubview:self.tableView];

}

- (void)queryUserAlipay {
    
    YXWeakSelf
    [YXUserViewModel queryUserAlipayAccountCompletion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            YXAlipayModel *model = [YXAlipayModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.account = model.alipay_account;
            weakSelf.name = model.alipay_username;
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
    
}

// 发送短信验证码
- (void)sendMobileCode {
    NSLog(@"获取验证码");
    
    if (_phone.length == 0) {
        KPOP(@"请输入手机号");
        return;
    }

//    if (![_phoneField.text isTelephoneFullNumber]) {
//        KPOP(@"手机号错误，请重新输入");
//        return;
//    }

    YXWeakSelf
    [YJProgressHUD showLoading:@"发送中..."];
    [YXLonginViewModel requestSendMobileCodeWithType:@"other" Mobile:_phone Code:_code RandomStr:nil Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:@"发送验证码成功"];
            [weakSelf openCountdown];
        }else {
            [YJProgressHUD showError:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];

    }];
}


- (void)openCountdown {
    YXWeakSelf
    [BYTimer timerWithTimeout:60 handlerBlock:^(int presentTime) {
        [weakSelf.getCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%d)",presentTime] forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = NO;
    } finish:^{
        [weakSelf.getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        weakSelf.getCodeBtn.enabled = YES;
    }];
}

- (void)save:(UIButton *)sender {
    
    YXWeakSelf
    if (self.type == AccountTypeStatePwd) {
      
        if ([NSString isBlankString:self.code]) {
            KPOP(@"请输入验证码");
            return;
        }
        YXAccountTypeViewController *vc = [YXAccountTypeViewController new];
        vc.type = AccountTypeStateNext;
        vc.phone = self.phone;
        vc.code = self.code;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else if (self.type == AccountTypeStatePhone) {
      
        if ([NSString isBlankString:self.phone]) {
            KPOP(@"请输入手机号");
            return;
        }
        if ([NSString isBlankString:self.code]) {
            KPOP(@"请输入验证码");
            return;
        }
        [YJProgressHUD showLoading:@""];
        [YXUserViewModel requestBindPhone:self.phone code:self.code Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showError:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showError:REQUESTERR];
        }];
        
    }else if (self.type == AccountTypeStateZFB) {
     
        
        if ([NSString isBlankString:self.name]) {
            KPOP(@"请输入姓名");
            return;
        }
        if ([NSString isBlankString:self.account]) {
            KPOP(@"请输入支付宝账号");
            return;
        }
        
        [YJProgressHUD showLoading:@""];
        [YXUserViewModel requestBindAlipayWithAccount:self.account username:self.name Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showError:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showError:REQUESTERR];
        }];
    }else if (self.type == AccountTypeStateNext) {
        
        if ([NSString isBlankString:self.onePwd]) {
            KPOP(@"请输入新密码");
            return;
        }
        
        if ([NSString isBlankString:self.twoPwd]) {
            KPOP(@"请输入确认密码");
            return;
        }
        if (self.onePwd.length < 6 && self.onePwd.length > 32) {
            KPOP(@"密码格式有误，请重新设置");
            return;
        }
        
        if (self.twoPwd.length < 6 && self.twoPwd.length > 32) {
            KPOP(@"密码格式有误，请重新设置");
            return;
        }
        
        if (![self.onePwd isEqualToString:self.twoPwd]) {
            KPOP(@"两次密码输入不相同");
            return;
        }
        
        
        [YJProgressHUD showLoading:@""];
        [YXLonginViewModel requestRegisterMerchantWithMobile:self.phone
                                                        Code:self.code
                                                   RandomStr:@"1"
                                                    Password:self.onePwd
                                                  InviteCode:self.twoPwd
                                                  Completion:^(id  _Nonnull responesObj) {
                                                        [YJProgressHUD hideHUD];
                                                      if (REQUESTDATASUCCESS) {
                                                          [weakSelf.navigationController popToRootViewControllerAnimated:YES];
                                                          [YJProgressHUD showMessage:@"修改密码成功"];

                                                      }else {
                                                          [YJProgressHUD showMessage:responesObj[@"msg"]];
                                                      }
                                                  } Failure:^(NSError * _Nonnull error) {
                                                      [YJProgressHUD showError:REQUESTERR];

                                                  }];
    }
    
}


#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.titlesArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    YXWeakSelf
    if (self.type == AccountTypeStatePwd) {
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleSubtitle) reuseIdentifier:@"cell"];
            }
            cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
            cell.detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
            cell.detailTextLabel.textColor = color_TextTwo;
            cell.textLabel.text = self.titlesArr[indexPath.row];
            NSString *phone = [NSString numberSuitScanf:[YXUserInfoManager getUserInfo].mobile];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"请输入%@手机收到的验证码",phone];
            return cell;
            
        }else {
            YXPublicCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell6"];
            if (!cell) {
                cell = [[YXPublicCell6 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell6"];
            }
            cell.contentTF.placeholder = self.titlesArr[indexPath.row];
            cell.contentTF.font = [UIFont systemFontOfSize:14.0f];
            cell.contentTF.textLimitInputType = LTTextLimitInputTypeNumber;
            cell.contentTF.textLimitType = LTTextLimitTypeLength;
            cell.contentTF.textLimitSize = 4;
            cell.contentTF.keyboardType = UIKeyboardTypeNumberPad;
            cell.contentTF.rightView = self.getCodeBtn;
            cell.contentTF.rightViewMode = UITextFieldViewModeAlways;
            YXWeakCell
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 4) {
                    weakCell.contentTF.text = [text substringToIndex:4];
                    weakSelf.code = weakCell.contentTF.text;
                }else {
                    weakSelf.code = text;
                }
            }];
            return cell;
        }
    }else if (self.type == AccountTypeStatePhone) {
       
        YXPublicCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell6"];
        if (!cell) {
            cell = [[YXPublicCell6 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell6"];
        }
        cell.contentTF.placeholder = self.titlesArr[indexPath.row];
        cell.contentTF.font = [UIFont systemFontOfSize:14.0f];
        if (indexPath.row == 0) {
            cell.contentTF.textLimitInputType = LTTextLimitInputTypeNumber;
            cell.contentTF.textLimitType = LTTextLimitTypeLength;
            cell.contentTF.textLimitSize = 11;
            cell.contentTF.keyboardType = UIKeyboardTypeNumberPad;
        }else {
            cell.contentTF.textLimitInputType = LTTextLimitInputTypeNumber;
            cell.contentTF.textLimitType = LTTextLimitTypeLength;
            cell.contentTF.textLimitSize = 4;
            cell.contentTF.keyboardType = UIKeyboardTypeNumberPad;
            cell.contentTF.rightView = self.getCodeBtn;
            cell.contentTF.rightViewMode = UITextFieldViewModeAlways;
        }
        YXWeakCell
        if (indexPath.row == 0) {
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 11) {
                    weakCell.contentTF.text = [text substringToIndex:11];
                    weakSelf.phone = weakCell.contentTF.text;
                }else {
                    weakSelf.phone = text;
                }
            }];
        }else {
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 4) {
                    weakCell.contentTF.text = [text substringToIndex:4];
                    weakSelf.code = weakCell.contentTF.text;
                }else {
                    weakSelf.code = text;
                }
            }];
        }
        return cell;
        
    }else if (self.type == AccountTypeStateZFB) {
        YXPublicCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell6"];
        if (!cell) {
            cell = [[YXPublicCell6 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell6"];
        }
        cell.contentTF.placeholder = self.titlesArr[indexPath.row];
        cell.contentTF.font = [UIFont systemFontOfSize:14.0f];
        YXWeakCell
        if (indexPath.row == 0) {
            cell.contentTF.text = self.name;
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 16) {
                    weakCell.contentTF.text = [text substringToIndex:16];
                    weakSelf.name = weakCell.contentTF.text;
                }else {
                    weakSelf.name = text;
                }
            }];
        }else {
            cell.contentTF.text = self.account;
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 24) {
                    weakCell.contentTF.text = [text substringToIndex:24];
                    weakSelf.account = weakCell.contentTF.text;
                }else {
                    weakSelf.account = text;
                }
            }];
        }
        return cell;
    }else if (self.type == AccountTypeStateNext) {
        YXPublicCell6 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell6"];
        if (!cell) {
            cell = [[YXPublicCell6 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell6"];
        }
        cell.contentTF.placeholder = self.titlesArr[indexPath.row];
        cell.contentTF.font = [UIFont systemFontOfSize:14.0f];
        cell.contentTF.secureTextEntry = YES;
        YXWeakCell
        if (indexPath.row == 0) {
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 32) {
                    weakCell.contentTF.text = [text substringToIndex:16];
                    weakSelf.onePwd = weakCell.contentTF.text;
                }else {
                    weakSelf.onePwd = text;
                }
            }];
        }else {
            [cell setEditTextBlock:^(NSString * _Nonnull text) {
                if (text.length > 32) {
                    weakCell.contentTF.text = [text substringToIndex:24];
                    weakSelf.twoPwd = weakCell.contentTF.text;
                }else {
                    weakSelf.twoPwd = text;
                }
            }];
        }
        return cell;
    }
    return [[UITableViewCell alloc] init];
}



#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, kHEIGHT)) style:(UITableViewStylePlain)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [self createFooterView];
        [_tableView registerClass:[YXPublicCell2 class] forCellReuseIdentifier:@"YXPublicCell2"];
        [_tableView registerClass:[YXPublicCell6 class] forCellReuseIdentifier:@"YXPublicCell6"];
    }
    return _tableView;
}

- (UIButton *)getCodeBtn{
    if (!_getCodeBtn) {
        _getCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _getCodeBtn.frame = CGRectMake(0, 0, 100, 32);
        [_getCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_getCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _getCodeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _getCodeBtn.backgroundColor = APPTintColor;
        _getCodeBtn.layer.masksToBounds = YES;
        _getCodeBtn.layer.cornerRadius = 4.0f;
        [_getCodeBtn addTarget:self action:@selector(sendMobileCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _getCodeBtn;
}

- (UIView *)createFooterView {
    
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 100))];
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(20, 30, KWIDTH - 40, 44);
    btn.backgroundColor = APPTintColor;
    if (self.type == AccountTypeStatePwd) {
        [btn setTitle:@"下一步" forState:(UIControlStateNormal)];
    }else {
        [btn setTitle:@"提交" forState:(UIControlStateNormal)];
    }
    [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    btn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    btn.layer.masksToBounds = YES;
    btn.layer.cornerRadius = 4.0f;
    [btn addTarget:self action:@selector(save:) forControlEvents:(UIControlEventTouchUpInside)];
    [footerView addSubview:btn];
    return footerView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
