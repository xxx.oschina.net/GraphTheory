//
//  YXShareViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/29.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXShareViewController.h"
#import "YXWebViewController.h"
#import "YXCouponsViewModel.h"
#import "YXCouponsModel.h"
#import "YXPublicSectionView.h"
#import "YXShareTableViewCell.h"
#import "YXShareContentTableViewCell.h"
#import "WGPublicBottomView.h"
#import "YXUMShareManager.h"

@interface YXShareViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) UIImageView *headerView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXCouponsModelList *model;
@end

@implementation YXShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"邀请可得消费抵扣劵";
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 60, 30);
    [btn setTitle:@"活动规则" forState:(UIControlStateNormal)];
    btn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [btn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
    [self loadData];
    
    [self loadSubViews];
}

- (void)click {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAboutOtherInfoWithType:@"6" Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        YXWebViewController *vc = [YXWebViewController new];
        vc.titleStr = responesObj[@"data"][@"us_title"];
        vc.url = responesObj[@"data"][@"us_content"];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

- (void)loadData {
    
    DDWeakSelf
    [YXCouponsViewModel queryCouponBannerWithPage:@"1" city_id:@"229" Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            NSArray *data = responesObj[@"data"];
            if (data.count) {
                [weakSelf.headerView sd_setImageWithURL:[NSURL URLWithString:data[0][@"cb_picpath"]]];
            }
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
    [YXCouponsViewModel queryCouponListWithPage:@"1" city_id:@"229" Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            YXCouponsModel *model = [YXCouponsModel mj_objectWithKeyValues:responesObj[@"data"]];
            weakSelf.dataArr = [YXCouponsModelList mj_objectArrayWithKeyValuesArray:model.list];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
    
}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.left.and.top.and.right.and.bottom.equalTo(self.view);
        }
    }];
   
}

// 微信分享
- (void)shareWX {
    
    if (self.model == nil) {
        return [YJProgressHUD showMessage:@"请选择优惠券"];
    }
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/app/apple-store/id1578154227?ct=web&mt=8"]];

    // 判断手机是否安装微信
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
        NSString *url = [NSString stringWithFormat:@"http://app.itulun.com/Home/Share/proinfo?params=id-%@",[YXUserInfoManager getUserInfo].user_id];
        [[YXUMShareManager shared] shareWebPageToPlatformType:(UMSocialPlatformType_WechatSession) thumbURL:@"" title:@"[创业者]的另一种选择！" description:self.model.title webpageUrl:url];
    }else {
        [YJProgressHUD showMessage:@"您未安装微信客户端,不能分享哟~"];
    }
    
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return section == 0 ? self.dataArr.count : 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 140;
    }else if (indexPath.section == 2) {
        return 120;
    }else {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        YXShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXShareTableViewCell" forIndexPath:indexPath];
        cell.model = self.dataArr[indexPath.row];
        return cell;
    }else if (indexPath.section == 2) {
        YXShareContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXShareContentTableViewCell" forIndexPath:indexPath];
        cell.contentLab.text = @"图论社app是一款为用户提供灵活雇佣、灵活创业的信息服务类软件。我们的产品宗旨在于摒弃包工和分销，摒弃网红经济和互联网零售，开辟一条最短路径一对一的共享雇佣渠道，让用户享受精准、高效的价值服务。";
        return cell;
    }
    return [UITableViewCell new];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0 || section == 2) {
        return 50;
    }
    return 70;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0 || section == 2) {
        YXPublicSectionView *headerView = [[YXPublicSectionView alloc] initWithFrame:(CGRectMake(0, 0, KWIDTH, 50))];
        if (section == 0) {
            headerView.titleLab.text = @"邀请奖励";
        }else if (section == 2) {
            headerView.titleLab.text = @"如何向好友介绍图论社";
        }
        return headerView;
    }else {
        return self.bottomView;
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return section == 1 ? 10 : 0.01;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
       self.model = self.dataArr[indexPath.row];
        for (YXCouponsModelList *listModel in self.dataArr) {
            if (self.model == listModel) {
                listModel.selected = YES;
            }else{
                listModel.selected = NO;
            }
        }
        [self.tableView reloadData];
    }
}


#pragma mark - Lazy Loading
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStyleGrouped)];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headerView;
        [_tableView registerNib:[UINib nibWithNibName:@"YXShareTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXShareTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXShareContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXShareContentTableViewCell"];
    }
    return _tableView;
}
- (UIImageView *)headerView {
    if (!_headerView) {
        _headerView = [UIImageView new];
        _headerView.frame = CGRectMake(0, 0, KWIDTH, 220);
        _headerView.backgroundColor = [UIColor whiteColor];
    }
    return _headerView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectMake(0, 0, KWIDTH, 70)];
        [_bottomView setTitle:@"立即邀请好友"];
        _bottomView.bgbackgroundColor = [UIColor redColor];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf shareWX];
        }];
    }
    return _bottomView;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
