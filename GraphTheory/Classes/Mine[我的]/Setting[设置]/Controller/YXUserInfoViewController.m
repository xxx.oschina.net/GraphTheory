//
//  YXUserInfoViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXUserInfoViewController.h"
#import "WGLoginViewController.h"
#import "WGForgetPassWordViewController.h"
#import "YXInputViewController.h"

#import "YXUserHeaderImgCell.h"
#import "YXPublicCell.h"
#import "YXUserViewModel.h"
#import "ZZYPhotoHelper.h"
#import "DDBaseNavigationController.h"
#import "WGPublicBottomView.h"
#import "YXCustomBottomPopView.h"
#import <BRPickerView/BRDatePickerView.h>

@interface YXUserInfoViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSArray *titlesArr;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXUserInfoModel *userInfo;
@property (nonatomic ,strong) UIImage *headerImg;
@end

@implementation YXUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"个人信息";
    
    self.userInfo = [YXUserInfoManager getUserInfo];
    
    [self loadSubViews];
}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}
#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titlesArr.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        YXUserHeaderImgCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXUserHeaderImgCell"];
        if (!cell) {
            cell = [[YXUserHeaderImgCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXUserHeaderImgCell"];
        }
        if (self.headerImg) {
            cell.headerImg.image = self.headerImg;
        }else {
            NSString *url = [NSString stringWithFormat:@"%@",[YXUserInfoManager getUserInfo].wechat_headimgurl];
            [cell.headerImg sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"mine_head_default"]];
        }
        cell.textLabel.text= self.titlesArr[indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        cell.textLabel.textColor = color_TextOne;
        return cell;
    }else {
        static NSString *cellID = @"YXUserInfoCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier:cellID];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
        cell.textLabel.textColor = color_TextOne;
        cell.textLabel.text= self.titlesArr[indexPath.row];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:16.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == 1) {
            cell.detailTextLabel.text = self.userInfo.nike;
        }else if (indexPath.row == 2) {
            if (self.userInfo.sex == 0) {
                cell.detailTextLabel.text = @"男";
            }else {
                cell.detailTextLabel.text = @"女";
            }
        }else if (indexPath.row == 3) {
            cell.detailTextLabel.text = self.userInfo.birthday;
        }else if (indexPath.row == 4) {
            cell.detailTextLabel.text = self.userInfo.college;
        }else if (indexPath.row == 5) {
            cell.detailTextLabel.text = self.userInfo.signature;
        }
        return cell;
    }
    return [[UITableViewCell alloc] init];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return section == 1 ? 10 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        YXWeakSelf
        [[ZZYPhotoHelper shareHelper] showImageViewSelcteWithResultBlock:^(id data) {
            NSLog(@"%@",data);
            UIImage *image = (UIImage *)data;
            weakSelf.headerImg = image;
            [weakSelf.tableView reloadData];
        }];
    }else if (indexPath.row == 1) {
        [self pushVCWithType:0];
    }else if (indexPath.row == 2) {
        [self showPopView];
    }else if (indexPath.row == 3) {
        DDWeakSelf
        NSString *selectValue = [NSDate br_stringFromDate:[NSDate date] dateFormat:@"yyyy-MM-dd"];
        [BRDatePickerView showDatePickerWithMode:BRDatePickerModeYMD title:@"生日" selectValue:selectValue minDate:nil maxDate:nil isAutoSelect:NO resultBlock:^(NSDate * _Nullable selectDate, NSString * _Nullable selectValue) {
            weakSelf.userInfo.birthday = selectValue;
            [weakSelf.tableView reloadData];
        }];
    }else if (indexPath.row == 4) {
        [self pushVCWithType:1];
    }else {
        [self pushVCWithType:2];
    }

}

// 跳转
- (void)pushVCWithType:(NSInteger)type {
    YXInputViewController *vc = [[YXInputViewController alloc] init];
    vc.type = type;
    [self.navigationController pushViewController:vc animated:YES];
    DDWeakSelf
    [vc setSaveBlock:^(NSString * _Nonnull content) {
        if (type == 0) {
            weakSelf.userInfo.nike = content;
        }else if (type == 1) {
            weakSelf.userInfo.college = content;
        }else {
            weakSelf.userInfo.signature = content;
        }
        [weakSelf.tableView reloadData];
    }];
}


#pragma mark - Lazy Loading
- (NSArray *)titlesArr {
    if (!_titlesArr) {
        _titlesArr = @[@"头像",@"姓名",@"性别",@"生日",@"毕业院校/机构名称",@"个性签名"];
    }
    return _titlesArr;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.contentInset = UIEdgeInsetsMake(10,0, 0, 0);
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0 );
        _tableView.separatorColor = [UIColor groupTableViewBackgroundColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [_tableView registerClass:[YXUserHeaderImgCell class] forCellReuseIdentifier:@"YXUserHeaderImgCell"];
        [_tableView registerClass:[YXPublicCell2 class] forCellReuseIdentifier:@"YXPublicCell2"];
    }
    return _tableView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"提交"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf saveMethods];
        }];
    }
    return _bottomView;
}

#pragma mark - 清空用户信息返回登录页
- (void)cleanUserInfo {
    [NetWorkTools pushLoginVC];
}


#pragma mark - 保存
- (void)saveMethods {
    // 判断是否有用户头像
    if (self.headerImg) {
        [self uploadImageWithImage:self.headerImg];
    }else {
        [self requestUpdateUserInfo];
    }
}


#pragma mark -- 上传头像
- (void)uploadImageWithImage:(UIImage *)image  {
    
    YXWeakSelf
    [YJProgressHUD showLoading:@"正在上传中..."];
    [YXUserViewModel requestUploadImg:image Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        NSLog(@"上传成功 %@", responesObj);
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showLoading:@"更新中..."];
            NSArray *dataArr = responesObj[@"data"];
            if (dataArr.count) {
                weakSelf.userInfo.wechat_headimgurl = dataArr[0];
                [weakSelf requestUpdateUserInfo];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD showError:@"上传失败,请检查网络设置"];
    }];
}

// 修改用户信息
- (void)requestUpdateUserInfo {
    
    YXWeakSelf
    [YXUserViewModel requestUpdateUserInfoWithHeardImage:self.userInfo.wechat_headimgurl
                                                    Name:self.userInfo.nike
                                                     sex:@(self.userInfo.sex).stringValue
                                                birthday:self.userInfo.birthday
                                                 college:self.userInfo.college
                                               signature:self.userInfo.signature
                                              Completion:^(id responesObj) {
        if (REQUESTDATASUCCESS) {
            // 保存头像
            if (weakSelf.userInfo.wechat_headimgurl.length >0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"wechat_headimgurl":weakSelf.userInfo.wechat_headimgurl}];
            }
            // 保存昵称
            if (weakSelf.userInfo.nike.length > 0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"nike":weakSelf.userInfo.nike}];
            }
            // 保存性别
            if (weakSelf.userInfo.sex) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"sex":@(weakSelf.userInfo.sex).stringValue}];
            }
            // 保存生日
            if (weakSelf.userInfo.birthday.length > 0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"birthday":weakSelf.userInfo.birthday}];
            }
            // 保存毕业院校
            if (weakSelf.userInfo.college.length > 0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"college":weakSelf.userInfo.college}];
            }
            // 个性签名
            if (weakSelf.userInfo.signature.length > 0) {
                [YXUserInfoManager resetUserInfoMessageWithDic:@{@"signature":weakSelf.userInfo.signature}];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationImageUrl object:nil userInfo:nil];
            [YJProgressHUD showMessage:@"修改成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [YJProgressHUD showError:REQUESTERR];
    }];

}

- (void)showPopView {
    
    YXCustomBottomPopView *popView = [[YXCustomBottomPopView alloc] initWithFrame: CGRectMake(0, kHEIGHT - 200,KWIDTH, 200) midArry:[NSMutableArray arrayWithArray:@[@"男",@"女"]]];
    popView.title = @"选择性别";
    [self.view addSubview:popView];
    [popView show];
    
    YXWeakSelf
    [popView setSelectPickerViewBlock:^(NSString *text, NSInteger index) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = text;
        weakSelf.userInfo.sex = index;
    }];
    
}



@end
