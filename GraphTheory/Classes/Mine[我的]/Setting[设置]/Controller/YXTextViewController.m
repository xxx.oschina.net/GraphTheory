//
//  YXTextViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/27.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXTextViewController.h"

@interface YXTextViewController ()
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) UILabel *contentLab;
@end

@implementation YXTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addSubViews];
}
- (void)addSubViews {
    
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.contentLab];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.top.bottom.equalTo(self.view);
        }
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
    }];
    [self.contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
    }];
    
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        CGFloat height = [UILabel heightForString:self.content fontSize:14.0f andWidth:KWIDTH - 30];
        _scrollView.contentSize = CGSizeMake(KWIDTH - 30, height);
    }
    return _scrollView;
}

- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.numberOfLines = 0;
        _contentLab.font = [UIFont systemFontOfSize:14.0];
        _contentLab.textColor = [UIColor blackColor];
        _contentLab.text = self.content;
    }
    return _contentLab;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
