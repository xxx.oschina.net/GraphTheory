//
//  YXWebViewController.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/2.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXWebViewController : DDBaseViewController

@property (strong, nonatomic)  NSString *titleStr;

@property (strong, nonatomic)  NSString *url;

@end

NS_ASSUME_NONNULL_END
