//
//  YXAlipayModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/19.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXAlipayModel : DDBaseModel
@property (copy,nonatomic)   NSString *alipay_account;
@property (copy,nonatomic)   NSString *alipay_bind_time;
@property (copy,nonatomic)   NSString *alipay_username;
@property (copy,nonatomic)   NSString *min_cash;
@end

NS_ASSUME_NONNULL_END
