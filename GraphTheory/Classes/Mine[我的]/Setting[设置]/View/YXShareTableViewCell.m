//
//  YXShareTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/10.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXShareTableViewCell.h"
#import "YXCouponsModel.h"
@implementation YXShareTableViewCell

- (void)setModel:(YXCouponsModelList *)model {
    _model = model;
    
    _titleLab.text = [NSString stringWithFormat:@"您和好友各得%@",_model.price_title];
    _contentLab.text = _model.title;
    _timeLab.text = [NSString stringWithFormat:@"领取后%@内有效",_model.validate_title];
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.price];
    
    if (_model.selected == YES) {
        self.backView.layer.borderColor = APPTintColor.CGColor;
    }else {
        self.backView.layer.borderColor = color_LineColor.CGColor;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 10.0;
    self.backView.layer.borderWidth = 1;
    self.backView.layer.borderColor = color_LineColor.CGColor;
    self.timeLab.textColor= APPTintColor;
    self.contentLab.textColor = color_TextTwo;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
