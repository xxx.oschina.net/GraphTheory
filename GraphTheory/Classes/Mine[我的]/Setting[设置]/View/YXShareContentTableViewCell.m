//
//  YXShareContentTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/10.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXShareContentTableViewCell.h"

@implementation YXShareContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentLab.textColor = color_TextTwo;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
