//
//  YXServiceContentView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXServiceContentView.h"

@implementation YXServiceContentView

-(instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        // 从xib中找到我们定义的view
        NSArray *viewArray = [[NSBundle mainBundle]loadNibNamed:@"YXServiceContentView" owner:self options:nil];
        self = viewArray[0];
        self.frame = frame;
    }
    return self;
    
}


@end
