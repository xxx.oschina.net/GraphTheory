//
//  YXServiceContentView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXServiceContentView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *codeImg;

@end

NS_ASSUME_NONNULL_END
