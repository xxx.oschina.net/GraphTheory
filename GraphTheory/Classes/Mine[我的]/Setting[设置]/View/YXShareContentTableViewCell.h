//
//  YXShareContentTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/10.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXShareContentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;

@end

NS_ASSUME_NONNULL_END
