//
//  YXShareTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/10.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCouponsModelList;
@interface YXShareTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@property (nonatomic ,strong) YXCouponsModelList *model;

@end

NS_ASSUME_NONNULL_END
