//
//  YXMineViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXMineViewController.h"
#import "YXSettingViewController.h"
#import "YXUserOrderViewController.h"
#import "YXUserOrderListViewController.h"
#import "YXCollectionViewController.h"
#import "YXMessageViewController.h"
#import "YXCouponsViewController.h"
#import "YXServiceViewController.h"
#import "YXShareViewController.h"
#import "YXFocusViewController.h"
#import "YXFansViewController.h"
#import "YXProductListViewController.h"
#import "YXInputViewController.h"
#import "YXAccountLogViewController.h"
#import "YXIndexViewController.h"
#import "YXSkillsViewController.h"


#import <JJCollectionViewRoundFlowLayout.h>
#import "YXMineHeaderCollectionReusableView.h"
#import "YXMineMenuCollectionViewCell.h"
#import "YXHomeDataCollectionViewCell.h"
#import "DDWorkHeaderView.h"
#import "YXUserViewModel.h"
@interface YXMineViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDataSource>

@property (strong, nonatomic) UICollectionView *collectionView;

@property (strong, nonatomic) YXUserInfoModel *userInfo;
@end

@implementation YXMineViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle =  UIStatusBarStyleLightContent;
    // 设置导航栏背景色
    self.navigationItem.title = @"";
    //设置导航栏背景图片为一个空的image，这样就透明了
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (@available(iOS 11.0, *)) {
        _collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.collectionView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self queryUserInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeTop;

    // 添加子视图
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queryUserInfo) name:kNotificationImageUrl object:nil];
    
}

- (void)queryUserInfo {
  
    YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
    DDWeakSelf
    [YXUserViewModel queryUserInfoCompletion:^(id responesObj) {
        YXUserInfoModel *userInfo = [YXUserInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
        userInfo.token = model.token;
        userInfo.mobile = model.mobile;
        [YXUserInfoManager saveUserInfoWithModel:userInfo];
        weakSelf.userInfo = userInfo;
        [weakSelf.collectionView reloadData];
        [weakSelf.collectionView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf.collectionView.mj_header endRefreshing];
    }];
    
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 6;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    if (section == 0){
        return 0;
    }else if (section == 1){
        return 4;
    }else if (section == 2){
        return 2;
    }else if (section == 3) {
        return 4;
    }else if (section == 4) {
        return 2;
    }else if (section == 5) {
        return 2;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section == 0){
        return CGSizeZero;
    }else if (indexPath.section == 1){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 70);
    }else if (indexPath.section == 2){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 70);
    }else if (indexPath.section == 3){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 70);
    }else if (indexPath.section == 4){
        return CGSizeMake((SCREEN_WIDTH - 30) / 4, 70);
    }else if (indexPath.section == 5){
        return CGSizeMake((SCREEN_WIDTH - 30) / 2, 70);
    }
    
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 0) {
        return UIEdgeInsetsMake(0, 0, 15, 0);
    }else if (section == 1) {
        return UIEdgeInsetsMake(0, 15, 20, 15);
    }else if (section == 2) {
        return UIEdgeInsetsMake(0, 15, 20, 15);
    }else if (section == 3) {
        return UIEdgeInsetsMake(0, 15, 20, 15);
    }else if (section == 4) {
        return UIEdgeInsetsMake(0, 15, 20, 15);
    }else if (section == 5) {
        return UIEdgeInsetsMake(0, 15, 20, 15);
    }
    return UIEdgeInsetsZero;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(KWIDTH, 220);
    }else if (section == 5) {
        return CGSizeMake(KWIDTH, 10);
    }else {
        return CGSizeMake(KWIDTH-30, 40);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (indexPath.section == 0) {
            YXMineHeaderCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXMineHeaderCollectionReusableView" forIndexPath:indexPath];
            header.userInfo = self.userInfo;
            [header setClickBtnBlock:^(NSInteger index) {
                NSIndexPath *indexs = [NSIndexPath indexPathForRow:index inSection:indexPath.section];
                [weakSelf pushControllerWithIndexPath:indexs];
            }];
            
            [header setClickMsgBlock:^{
                if ([YXUserInfoManager getUserInfo].token == nil) {
                    [self showAlertWithMessage:@"该功能需要先登录\n是否登录账号" withConfirm:@"去登录" Handle:^(UIAlertAction * _Nonnull action) {
                        [NetWorkTools pushLoginVC];
                    }];
                }else {
                    YXMessageViewController *vc = [YXMessageViewController new];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
            }];
            
            [header setClickBalanceBlock:^{
                if ([YXUserInfoManager getUserInfo].token == nil) {
                    [self showAlertWithMessage:@"该功能需要先登录\n是否登录账号" withConfirm:@"去登录" Handle:^(UIAlertAction * _Nonnull action) {
                        [NetWorkTools pushLoginVC];
                    }];
                }else {
                    YXAccountLogViewController *vc = [YXAccountLogViewController new];
                    vc.type = 2;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
             
            }];
            
            return header;
        }else {
            DDWorkHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"DDWorkHeaderView" forIndexPath:indexPath];
            header.indexPath = indexPath;
          
            [header setClickAllBtn:^{
                if ([YXUserInfoManager getUserInfo].token == nil) {
                    [self showAlertWithMessage:@"该功能需要先登录\n是否登录账号" withConfirm:@"去登录" Handle:^(UIAlertAction * _Nonnull action) {
                        [NetWorkTools pushLoginVC];
                    }];
                }else {
                    if (indexPath.section == 1) {
                        YXUserOrderViewController *vc = [YXUserOrderViewController new];
                        vc.selectIndex = 0;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (indexPath.section == 2) {
                        YXInputViewController *vc = [YXInputViewController new];
                        vc.type = 3;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }else if (indexPath.section == 3) {
                        YXUserOrderListViewController *vc = [YXUserOrderListViewController new];
                        vc.orderStatus = @"3";
                        vc.type = 2;
                        [weakSelf.navigationController pushViewController:vc animated:YES];
                    }
                }                
              
            }];
            
            return header;
        }
    }
    return nil;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 5) {
        YXHomeDataCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeDataCollectionViewCell" forIndexPath:indexPath];
        if (indexPath.row == 0) {
            cell.titleLab.text = @"邀请更多小伙伴加入";
            cell.titleLab.textColor = color_TextThree;
            cell.detailLab.text = @"分享微信好友";
        }else {
            cell.titleLab.text = @"智能问答/人工智能";
            cell.titleLab.textColor = APPTintColor;
            cell.detailLab.text = @"帮助中心";
        }
        return cell;
    }else if (indexPath.section > 0) {
        YXMineMenuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXMineMenuCollectionViewCell" forIndexPath:indexPath];
        cell.index = indexPath;
        return cell;
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self pushControllerWithIndexPath:indexPath];
}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 15, 15, 15);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

#pragma mark - Lazy loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        YXWeakSelf
        JJCollectionViewRoundFlowLayout *flowLayout = [[JJCollectionViewRoundFlowLayout alloc] init];
//        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
//        flowLayout.sectionInset = UIEdgeInsetsMake(0, 15, 15, 15);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.isCalculateHeader = YES;
        flowLayout.isCalculateFooter = NO;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        
        [_collectionView registerClass:[YXMineHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"YXMineHeaderCollectionReusableView"];
        [_collectionView registerClass:[DDWorkHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"DDWorkHeaderView"];
        [_collectionView registerClass:[YXMineMenuCollectionViewCell class] forCellWithReuseIdentifier:@"YXMineMenuCollectionViewCell"];
        [_collectionView registerClass:[YXHomeDataCollectionViewCell class] forCellWithReuseIdentifier:@"YXHomeDataCollectionViewCell"];
        
        _collectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingBlock:^{
            [weakSelf queryUserInfo];
        }];
    }
    return _collectionView;
}

#pragma mark - 用户跳转相应模块控制器
- (void)pushControllerWithIndexPath:(NSIndexPath *)indexPath {
    
    if ([YXUserInfoManager getUserInfo].token == nil) {
        [self showAlertWithMessage:@"该功能需要先登录\n是否登录账号" withConfirm:@"去登录" Handle:^(UIAlertAction * _Nonnull action) {
            [NetWorkTools pushLoginVC];
        }];
        return;
    }
    
    YXWeakSelf
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            YXCollectionViewController *vc = [YXCollectionViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 1) {
            YXUserOrderViewController *vc = [YXUserOrderViewController new];
            vc.selectIndex = 5;
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2) {
            YXMessageViewController *vc = [YXMessageViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 3) {
            YXCouponsViewController *vc = [YXCouponsViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 4) {
            YXSettingViewController *vc = [[YXSettingViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 5) {
            YXServiceViewController *vc = [YXServiceViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 1) {
        YXUserOrderViewController *vc = [YXUserOrderViewController new];
        if (indexPath.row == 0) {
            vc.selectIndex = 1;
        }else {
            vc.selectIndex = indexPath.row + 2;
        }
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"创业者"]) {
                YXSkillsViewController *vc = [YXSkillsViewController new];
                vc.type = 0;
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                [YJProgressHUD showMessage:@"您还不是创业者"];
            }
        }else {
            YXIndexViewController *vc = [YXIndexViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            YXUserInfoModel *model = [YXUserInfoManager getUserInfo];
            if ([model.roleName containsString:@"自媒体从业者"]) {
                YXSkillsViewController *vc = [YXSkillsViewController new];
                vc.type = 1;
                [self.navigationController pushViewController:vc animated:YES];
            }else {
                [YJProgressHUD showMessage:@"您还不是自媒体从业者"];
            }
        }else if (indexPath.row == 1) {
            YXProductListViewController *vc = [YXProductListViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2) {
            YXFansViewController *vc = [YXFansViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            YXFocusViewController *vc = [YXFocusViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 4) {
        YXAccountLogViewController *vc = [YXAccountLogViewController new];
        vc.type = indexPath.row;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 5) {
        if (indexPath.row == 0) {
            YXShareViewController *vc = [YXShareViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }else {
            YXServiceViewController *vc = [YXServiceViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
   
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
