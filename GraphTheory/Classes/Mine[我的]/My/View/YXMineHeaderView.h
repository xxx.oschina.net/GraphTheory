//
//  YXMineHeaderView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,MineHeaderViewType) {
    MineStateUser,          //普通用户
    MineStateMerchants,     //商家
};


@interface YXMineHeaderView : UIView


/**
 点击用户头像回调
 */
@property (nonatomic ,copy) void (^clickUserImgBlock)(void);


/**
 点击消息回调
 */
@property (nonatomic ,copy) void (^clickMsgBlock)(void);


/**
 点击按钮回调
 */
@property (nonatomic ,copy) void (^clickBtnBlock)(NSInteger index);

/** 背景视图图*/
@property (nonatomic ,strong) UIView *bgView;
/** 背景图片*/
@property (nonatomic ,strong) UIImageView *bgImgView;
/** 背景遮罩*/
@property (nonatomic ,strong) UIView *maskView;
/** 底部视图*/
@property (nonatomic ,strong) UIView *bottomView;
/** 底部背景图*/
@property (nonatomic ,strong) UIImageView *bottomImgView;
/** 可提现金额标题*/
@property (nonatomic ,strong) UILabel *orderLab;
/** 查看详情按钮*/
@property (nonatomic ,strong) UIButton *moreOrderBtn;
/** 可提现金额*/
@property (nonatomic ,strong) UILabel *amountLab;
/** 提现按钮*/
@property (nonatomic ,strong) UIButton *txBtn;
/** 提现记录*/
@property (nonatomic ,strong) UIButton *txRecordBtn;
/** 用户头像*/
@property (nonatomic ,strong) UIImageView *userImgView;
/** 用户昵称*/
@property (nonatomic ,strong) UILabel *nickNameLab;
/** 用户手机号*/
@property (nonatomic ,strong) UILabel *phoneLab;

@property (nonatomic ,strong) YXUserInfoModel *infoModel;


/** 提现金额*/
@property (nonatomic ,assign) CGFloat money;

@property (nonatomic ,assign) MineHeaderViewType type;

- (instancetype)initWithFrame:(CGRect)frame ViewType:(MineHeaderViewType)type;

@end

NS_ASSUME_NONNULL_END
