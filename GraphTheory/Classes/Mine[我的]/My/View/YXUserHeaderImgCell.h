//
//  YXUserHeaderImgCell.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXUserHeaderImgCell : UITableViewCell


@property (nonatomic ,strong) UIImageView *headerImg;

@end

NS_ASSUME_NONNULL_END
