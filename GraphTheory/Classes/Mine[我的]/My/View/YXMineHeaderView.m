//
//  YXMineHeaderView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXMineHeaderView.h"

@implementation YXMineHeaderView

- (void)setInfoModel:(YXUserInfoModel *)infoModel {
    
    NSString *url = [NSString stringWithFormat:@"%@",[YXUserInfoManager getUserInfo].wechat_headimgurl];
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"mine_head_default"]];
    
    // 用户昵称如果为空显示手机号
    if ([NSString isBlankString:[YXUserInfoManager getUserInfo].nike] ) {
        _nickNameLab.text = [NSString stringWithFormat:@"昵称:%@",[YXUserInfoManager getUserInfo].mobile];
    }else {
        _nickNameLab.text = [NSString stringWithFormat:@"昵称:%@",[YXUserInfoManager getUserInfo].nike];
    }
}

- (void)setMoney:(CGFloat)money {
    _money = money;
    _amountLab.text = [NSString stringWithFormat:@"%.02f元",_money];
}


- (instancetype)initWithFrame:(CGRect)frame
                     ViewType:(MineHeaderViewType)type
{
    self = [super initWithFrame:frame];
    if (self) {
        self.type = type;
        [self setup];
        [self addLayout];
    }
    return self;
}

- (void)setup {
    
    
    if (self.type == MineStateUser) {
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.bgImgView];
        [self.bgView addSubview:self.maskView];
        [self.bgView addSubview:self.userImgView];
        [self.bgView addSubview:self.nickNameLab];
        [self.bgView addSubview:self.phoneLab];
    }else if (self.type == MineStateMerchants) {
        [self addSubview:self.bgView];
        [self.bgView addSubview:self.bgImgView];
        [self.bgView addSubview:self.maskView];
        [self addSubview:self.bottomView];
        [self.bottomView addSubview:self.bottomImgView];
        [self.bottomView addSubview:self.orderLab];
        [self.bottomView addSubview:self.moreOrderBtn];
        [self.bottomView addSubview:self.amountLab];
        [self.bottomView addSubview:self.txBtn];
        [self.bottomView addSubview:self.txRecordBtn];
        [self addSubview:self.userImgView];
        [self addSubview:self.nickNameLab];
        [self addSubview:self.phoneLab];
    }
    
}


#pragma mark - Lazy loading
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] init];
    }
    return _bgView;
}

- (UIImageView *)bgImgView {
    if (!_bgImgView) {
        _bgImgView  = [UIImageView new];
        _bgImgView.image = [UIImage imageNamed:@"mine_head_back"];
        
    }
    return _bgImgView;
}

- (UIView *)maskView {
    if (!_maskView) {
        _maskView = [[UIView alloc] init];
        _maskView.backgroundColor = [UIColor colorWithHexString:@"#000000" alpha:0.6];
    }
    return _maskView;
}


- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
    }
    return _bottomView;
}

- (UIImageView *)bottomImgView {
    if (!_bottomImgView) {
        _bottomImgView = [UIImageView new];
        _bottomImgView.image = [UIImage imageNamed:@"mine_head_top"];
        _bottomImgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapAction)];
        [_bottomImgView addGestureRecognizer:tap];
    }
    return _bottomImgView;
}

- (UILabel *)orderLab {
    if (!_orderLab) {
        _orderLab = [[UILabel alloc] init];
        _orderLab.textColor = color_TextOne;
        _orderLab.text = @"可提现金额";
        _orderLab.font = [UIFont systemFontOfSize:14.0];
    }
    return _orderLab;
}

- (UIButton *)moreOrderBtn {
    if (!_moreOrderBtn) {
        _moreOrderBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_moreOrderBtn setTitle:@"查看详情" forState:(UIControlStateNormal)];
        [_moreOrderBtn setTitleColor:color_TextTwo forState:(UIControlStateNormal)];
        _moreOrderBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_moreOrderBtn setImage:[UIImage imageNamed:@"advert_arrow_right"] forState:(UIControlStateNormal)];
        [_moreOrderBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(18, 33)) space:5];
        _moreOrderBtn.tag = 200;
        [_moreOrderBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _moreOrderBtn;
}

- (UILabel *)amountLab {
    if (!_amountLab) {
        _amountLab = [[UILabel alloc] init];
        _amountLab.text = @"0.00";
        _amountLab.textColor = color_OrangeColor;
        _amountLab.font = [UIFont systemFontOfSize:22.0f];
    }
    return _amountLab;
}

- (UIButton *)txBtn {
    if (!_txBtn) {
        _txBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_txBtn setTitle:@"提现" forState:(UIControlStateNormal)];
        [_txBtn setTitleColor:color_TextTwo forState:(UIControlStateNormal)];
        _txBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        _txBtn.layer.masksToBounds = YES;
        _txBtn.layer.cornerRadius = 14.0f;
        _txBtn.layer.borderColor = color_TextThree.CGColor;
        _txBtn.layer.borderWidth = 1.0f;
        _txBtn.tag = 201;
        [_txBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _txBtn;
}

- (UIButton *)txRecordBtn {
    if (!_txRecordBtn) {
        _txRecordBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_txRecordBtn setTitle:@"提现记录" forState:(UIControlStateNormal)];
        [_txRecordBtn setTitleColor:color_TextTwo forState:(UIControlStateNormal)];
        _txRecordBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        _txRecordBtn.layer.masksToBounds = YES;
        _txRecordBtn.layer.cornerRadius = 14.0f;
        _txRecordBtn.layer.borderColor = color_TextThree.CGColor;
        _txRecordBtn.layer.borderWidth = 1.0f;
        _txRecordBtn.tag = 202;
        [_txRecordBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];

    }
    return _txRecordBtn;
}

- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.layer.masksToBounds = YES;
        _userImgView.layer.cornerRadius = 24.0;
        _userImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImgView.layer.borderWidth = 1.0f;
        _userImgView.userInteractionEnabled = YES;
        
        NSString *url = [NSString stringWithFormat:@"%@",[YXUserInfoManager getUserInfo].wechat_headimgurl];
        [_userImgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"mine_head_default"]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userImgAction)];
        [_userImgView addGestureRecognizer:tap];
    }
    return _userImgView;
}

- (UILabel *)nickNameLab {
    if (!_nickNameLab) {
        _nickNameLab = [[UILabel alloc] init];
        _nickNameLab.textColor = [UIColor whiteColor];
        _nickNameLab.font = [UIFont boldSystemFontOfSize:14.0f];
        
        // 用户昵称如果为空显示手机号
        if ([NSString isBlankString:[YXUserInfoManager getUserInfo].nike] ) {
            _nickNameLab.text = [NSString stringWithFormat:@"昵称:%@",[YXUserInfoManager getUserInfo].mobile];
        }else {
            _nickNameLab.text = [NSString stringWithFormat:@"昵称:%@",[YXUserInfoManager getUserInfo].nike];
        }
    }
    return _nickNameLab;
}

- (UILabel *)phoneLab {
    if (!_phoneLab) {
        _phoneLab = [[UILabel alloc] init];
        _phoneLab.text = [YXUserInfoManager getUserInfo].mobile;
        _phoneLab.font = [UIFont systemFontOfSize:14.0f];
        _phoneLab.textColor = [UIColor whiteColor];
    }
    return _phoneLab;
}



#pragma mark - 布局
- (void)addLayout {
    
    YXWeakSelf
    if (self.type == MineStateUser) {
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.offset(0);
            make.height.offset(120);
        }];
        
        [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        
        [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(48);
            make.left.equalTo(weakSelf.mas_left).offset(15);
            make.size.mas_equalTo(CGSizeMake(48, 48));
        }];
        
        [self.nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
            make.top.equalTo(weakSelf.userImgView.mas_top).offset(0);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
        
        [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
            make.bottom.equalTo(weakSelf.userImgView.mas_bottom).offset(0);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
        
    }else if (self.type == MineStateMerchants) {
        [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.offset(0);
            make.height.offset(155);
        }];
        
        [self.bgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        
        [self.maskView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(0);
            make.right.equalTo(weakSelf.mas_right).offset(0);
            make.bottom.equalTo(weakSelf.mas_bottom).offset(0);
            make.height.offset(87);
        }];
        
        [self.bottomImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        
        [self.orderLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(35);
            make.top.equalTo(weakSelf.bottomView.mas_top).offset(15);
            [weakSelf.orderLab sizeToFit];
        }];
        
        [self.moreOrderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.centerY.equalTo(weakSelf.orderLab.mas_centerY);
            [weakSelf.moreOrderBtn sizeToFit];
        }];
        
        [self.amountLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.mas_left).offset(35);
            make.top.equalTo(weakSelf.orderLab.mas_bottom).offset(18);
            [weakSelf.amountLab sizeToFit];
        }];
        
        
        [self.txRecordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.centerY.equalTo(weakSelf.amountLab.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(68, 28));
        }];
        
        [self.txBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(weakSelf.txRecordBtn.mas_left).offset(-10);
            make.centerY.equalTo(weakSelf.amountLab.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(44, 28));
        }];
        
        [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(48);
            make.left.equalTo(weakSelf.mas_left).offset(15);
            make.size.mas_equalTo(CGSizeMake(48, 48));
        }];
        
        [self.nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
            make.top.equalTo(weakSelf.userImgView.mas_top).offset(0);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
        
        [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
            make.bottom.equalTo(weakSelf.userImgView.mas_bottom).offset(0);
            make.size.mas_equalTo(CGSizeMake(200, 20));
        }];
        
    }
    
   
}

#pragma mark - Touch Even
// 点击用户头像
- (void)userImgAction {
    
    if (self.clickUserImgBlock) {
        self.clickUserImgBlock();
    }
}


// 点击消息
- (void)msgAction {
    
    if (self.clickMsgBlock) {
        self.clickMsgBlock();
    }
}

// 点击提现背景视图
- (void)viewTapAction {
    
    if (self.clickBtnBlock) {
        self.clickBtnBlock(0);
    }
}

// 点击按钮
- (void)btnAction:(UIButton *)sender {
    
    if (self.clickBtnBlock) {
        self.clickBtnBlock(sender.tag - 200);
    }
    
}


@end
