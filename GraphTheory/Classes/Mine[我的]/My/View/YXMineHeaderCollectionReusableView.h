//
//  YXMineHeaderCollectionReusableView.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXMineHeaderCollectionReusableView : UICollectionReusableView

/**
 点击用户头像回调
 */
@property (nonatomic ,copy) void (^clickUserImgBlock)(void);

/**
 点击设置菜单回调
 */
@property (nonatomic ,copy) void (^clickBtnBlock)(NSInteger index);
/**
 点击消息回调
 */
@property (nonatomic ,copy) void (^clickMsgBlock)(void);
/**
 点击余额回调
 */
@property (nonatomic ,copy) void (^clickBalanceBlock)(void);


@property (nonatomic ,strong) YXUserInfoModel *userInfo;

/** 用户头像*/
@property (nonatomic ,strong) UIImageView *userImgView;
/** 用户昵称*/
@property (nonatomic ,strong) UILabel *nickNameLab;
/** 用户等级*/
@property (nonatomic ,strong) UIView *levelView;
/** 用户等级*/
@property (nonatomic ,strong) UILabel *levelLab;

@property (nonatomic ,strong) UIButton *loginBtn;

@end

NS_ASSUME_NONNULL_END
