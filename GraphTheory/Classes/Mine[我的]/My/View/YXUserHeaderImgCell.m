//
//  YXUserHeaderImgCell.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXUserHeaderImgCell.h"

@implementation YXUserHeaderImgCell

- (UIImageView *)headerImg {
    if (!_headerImg) {
        _headerImg = [UIImageView new];
        _headerImg.layer.masksToBounds = YES;
        _headerImg.layer.cornerRadius = 15.0f;
    }
    return _headerImg;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self.contentView addSubview:self.headerImg];
        YXWeakSelf
        [self.headerImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.mas_centerY);
            make.right.equalTo(@-15);
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
    }
    return self;
}



@end
