//
//  YXMineHeaderCollectionReusableView.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMineHeaderCollectionReusableView.h"

@interface YXMineHeaderCollectionReusableView ()

/** 背景*/
@property (nonatomic ,strong) UIView *backView;
/** 设置*/
@property (nonatomic ,strong) UIButton *settBtn;
/** 客服*/
@property (nonatomic ,strong) UIButton *serviceBtn;

@property (nonatomic ,strong) UILabel *balanceLab;

@property (nonatomic ,strong) UIView *middleView;

@property (nonatomic ,strong) UIView *bottomView;

@property (nonatomic ,strong) UIImageView *msgImgView;

@property (nonatomic ,strong) UILabel *msgLab;

@property (nonatomic ,strong) UIImageView *rightImgView;

@end

@implementation YXMineHeaderCollectionReusableView

- (void)setUserInfo:(YXUserInfoModel *)userInfo {
    _userInfo = userInfo;
    
    if (_userInfo.token == nil) {
        self.nickNameLab.hidden = YES;
        self.levelView.hidden = YES;
        self.loginBtn.hidden = NO;
    }else {
        self.loginBtn.hidden = YES;
        self.nickNameLab.hidden = NO;
        self.levelView.hidden = NO;
    }
    
    
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:_userInfo.wechat_headimgurl] placeholderImage:[UIImage imageNamed:@"mine_head_default"]];
    if (_userInfo.nike) {
        _nickNameLab.text = _userInfo.nike;
    }else {
        _nickNameLab.text = _userInfo.mobile;
    }
    if ([_userInfo.balance floatValue] > 0) {
        _balanceLab.text = [NSString stringWithFormat:@"余额:%@",_userInfo.balance];
    }else {
        _balanceLab.text = [NSString stringWithFormat:@"余额:0.00"];
    }
    _levelLab.text = _userInfo.roleName;
    
}


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setUpLayout];
     
    }
    return self;
}


/**
 页面布局
 */
- (void)setUpLayout{

        
    [self addSubview:self.backView];
    [self.backView addSubview:self.userImgView];
    [self.backView addSubview:self.loginBtn];
    [self.backView addSubview:self.nickNameLab];
    [self.backView addSubview:self.levelView];
    [self.levelView addSubview:self.levelLab];
    [self.backView addSubview:self.serviceBtn];
    [self.backView addSubview:self.settBtn];
    [self.backView addSubview:self.balanceLab];
    [self.backView addSubview:self.middleView];
    [self addSubview:self.bottomView];
    [self.bottomView addSubview:self.msgImgView];
    [self.bottomView addSubview:self.msgLab];
    [self.bottomView addSubview:self.rightImgView];
    
    YXWeakSelf
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(weakSelf);
        make.height.equalTo(@200);
    }];
    
    [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.backView.mas_top).offset(48);
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(48, 48));
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.userImgView.mas_centerY);
        make.left.equalTo(self.userImgView.mas_right).offset(15);
        [self.loginBtn sizeToFit];
    }];
    
    [self.nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
        make.top.equalTo(weakSelf.userImgView.mas_top).offset(0);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.userImgView.mas_bottom).offset(-2);
        make.left.equalTo(self.userImgView.mas_right).offset(15);
    }];
    [self.levelLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.levelView).mas_offset(UIEdgeInsetsMake(2, 5, 2, 5));
    }];
    
    [self.serviceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.userImgView.mas_centerY);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.settBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.serviceBtn.mas_centerY);
        make.right.equalTo(weakSelf.serviceBtn.mas_left).offset(-15);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.balanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.settBtn.mas_left).offset(-15);
        make.centerY.equalTo(weakSelf.levelLab.mas_centerY);
        [weakSelf.balanceLab sizeToFit];
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.userImgView.mas_bottom).offset(15);
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        make.height.equalTo(@50);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.bottom.equalTo(weakSelf);
        make.height.equalTo(@40);
    }];
    
    [self.msgImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.bottomView.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.msgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.left.equalTo(weakSelf.msgImgView.mas_right).offset(15);
        [weakSelf.msgLab sizeToFit];
    }];
    
    [self.rightImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.bottomView.mas_right).offset(-15);
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(6, 12));
    }];
    

    NSArray *titleArr = @[@"收藏",@"评价",@"消息",@"卡劵"];
    NSArray *imageArr = @[@"shoucang",@"pingjia",@"xiaoxi",@"kaquan"];
    NSMutableArray *viewArray = [NSMutableArray array];
    for (int i = 0; i < titleArr.count ; i ++) {
        UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [btn setTitle:titleArr[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [btn setImgViewStyle:(ButtonImgViewStyleTop) imageSize:(CGSizeMake(32, 32)) space:5];
        [btn setImage:[UIImage imageNamed:imageArr[i]] forState:(UIControlStateNormal)];
        [btn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        btn.tag = 1000+i;
        [self.middleView addSubview:btn];
        [viewArray addObject:btn];
    }
    [viewArray mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:0 leadSpacing:0 tailSpacing:0];
    [viewArray mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(weakSelf.middleView);
    }];

    
}

#pragma mark - Lozy Loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = APPTintColor;
    }
    return _backView;
}

- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.layer.masksToBounds = YES;
        _userImgView.layer.cornerRadius = 24.0;
        _userImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImgView.layer.borderWidth = 1.0f;
        _userImgView.userInteractionEnabled = YES;
//        _userImgView.image = [UIImage imageNamed:@"mine_head_default"];
 
    }
    return _userImgView;
}

- (UILabel *)nickNameLab {
    if (!_nickNameLab) {
        _nickNameLab = [[UILabel alloc] init];
        _nickNameLab.textColor = [UIColor whiteColor];
        _nickNameLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _nickNameLab.text = @"会飞的鱼";
    }
    return _nickNameLab;
}
- (UIView *)levelView{
    if (!_levelView) {
        _levelView = [[UIView alloc]init];
        _levelView.layer.borderColor = [UIColor whiteColor].CGColor;
        _levelView.layer.borderWidth = 1.0f;
        _levelView.layer.masksToBounds = YES;
        _levelView.layer.cornerRadius = 4;
    }
    return _levelView;
}
- (UILabel *)levelLab {
    if (!_levelLab) {
        _levelLab = [[UILabel alloc] init];
        _levelLab.text = @"普通会员";
        _levelLab.font = [UIFont systemFontOfSize:10.0f];
        _levelLab.textColor = [UIColor whiteColor];
        _levelLab.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLab;
}
- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_loginBtn setTitle:@"登 录" forState:(UIControlStateNormal)];
        [_loginBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _loginBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        [_loginBtn addTarget:self action:@selector(loginBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _loginBtn;
}


- (UIButton *)settBtn {
    if (!_settBtn) {
        _settBtn = [UIButton  buttonWithType:UIButtonTypeCustom];
        [_settBtn setImage:[UIImage imageNamed:@"mine_setting"] forState:UIControlStateNormal];
        [_settBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        _settBtn.tag = 1004;
    }
    return _settBtn;
}


- (UIButton *)serviceBtn {
    if (!_serviceBtn) {
        _serviceBtn = [UIButton  buttonWithType:UIButtonTypeCustom];
        [_serviceBtn setImage:[UIImage imageNamed:@"mine_kefu"] forState:UIControlStateNormal];
        [_serviceBtn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
        _serviceBtn.tag = 1005;
    }
    return _serviceBtn;
}

- (UILabel *)balanceLab {
    if (!_balanceLab) {
        _balanceLab = [[UILabel alloc] init];
        _balanceLab.text = @"余额:0.00";
        _balanceLab.font = [UIFont systemFontOfSize:12.0f];
        _balanceLab.textColor = [UIColor whiteColor];
        _balanceLab.textAlignment = NSTextAlignmentCenter;
        _balanceLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(balanceAction)];
        [_balanceLab addGestureRecognizer:tap];
    }
    return _balanceLab;
}

- (UIView *)middleView {
    if (!_middleView) {
        _middleView = [UIView new];
    }
    return _middleView;
}


- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.masksToBounds = YES;
        _bottomView.layer.cornerRadius = 8.0f;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(msgAction)];
        [_bottomView addGestureRecognizer:tap];
    }
    return _bottomView;
}

- (UIImageView *)msgImgView {
    if (!_msgImgView) {
        _msgImgView = [UIImageView new];
        _msgImgView.image = [UIImage imageNamed:@"7铃铛"];
    }
    return _msgImgView;
}

- (UILabel *)msgLab {
    if (!_msgLab) {
        _msgLab = [[UILabel alloc] init];
        _msgLab.text = @"点击进入消息中心";
        _msgLab.textColor = color_TextOne;
        _msgLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _msgLab;
}

- (UIImageView *)rightImgView {
    if (!_rightImgView) {
        _rightImgView = [UIImageView new];
        _rightImgView.image = [UIImage imageNamed:@"advert_arrow_right"];
    }
    return _rightImgView;
}

- (void)btnAction:(UIButton *)sender {
    if (self.clickBtnBlock) {
        self.clickBtnBlock(sender.tag-1000);
    }
}

- (void)msgAction {
    if (self.clickMsgBlock) {
        self.clickMsgBlock();
    }
}

- (void)balanceAction {
    if (self.clickBalanceBlock) {
        self.clickBalanceBlock();
    }
}

- (void)loginBtnAction {
    [NetWorkTools pushLoginVC];
}



@end
