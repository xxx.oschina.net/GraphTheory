//
//  YXMineMenuCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/10/31.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMineMenuCollectionViewCell.h"

@interface YXMineMenuCollectionViewCell ()

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 标题
 */
@property (nonatomic ,strong) UIImageView *iconImageView;
/**
 详细
 */
@property (nonatomic ,strong) UILabel *detailLab;
@end


@implementation YXMineMenuCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
          [self setUpLayout];
    }
    return self;
}

- (void)setIndex:(NSIndexPath *)index{
    _index = index;
    if (_index.section == 1) {
        switch (index.row) {
            case 0:{
                _iconImageView.image = [UIImage imageNamed:@"daifukuan"];
                _detailLab.text = @"待支付";
            }
                break;
            case 1:{
                _iconImageView.image = [UIImage imageNamed:@"jinxingzhong"];
                _detailLab.text = @"进行中";
            }
                break;
            case 2:{
                _iconImageView.image = [UIImage imageNamed:@"daipingjia"];
                _detailLab.text = @"待确认";
            }
                break;
            case 3:{
                _iconImageView.image = [UIImage imageNamed:@"yiwancheng"];
                _detailLab.text = @"已完成";
            }
                break;
            default:
                break;
        }
    }else if (_index.section == 2) {
        switch (index.row) {
            case 0:{
                _iconImageView.image = [UIImage imageNamed:@"jineng"];
                _detailLab.text = @"技能";
            }
                break;
            case 1:{
                _iconImageView.image = [UIImage imageNamed:@"darenzhishu"];
                _detailLab.text = @"达人指数";
            }
                break;
            default:
                break;
        }
    }else if (_index.section == 3) {
        switch (index.row) {
            case 0:{
                _iconImageView.image = [UIImage imageNamed:@"zimeiti"];
                _detailLab.text = @"B&M自媒体";
            }
                break;
            case 1:{
                _iconImageView.image = [UIImage imageNamed:@"chanpin"];
                _detailLab.text = @"产品";
            }
                break;
            case 2:{
                _iconImageView.image = [UIImage imageNamed:@"fensi"];
                _detailLab.text = @"粉丝";
            }
                break;
            case 3:{
                _iconImageView.image = [UIImage imageNamed:@"guanzhu"];
                _detailLab.text = @"关注";
            }
                break;
            default:
                break;
        }
    }else if (_index.section == 4) {
        switch (index.row) {
            case 0:{
                _iconImageView.image = [UIImage imageNamed:@"yongjin"];
                _detailLab.text = @"创业佣金";
            }
                break;
            case 1:{
                _iconImageView.image = [UIImage imageNamed:@"zouhuoshouru"];
                _detailLab.text = @"走货收入";
            }
                break;
            default:
                break;
        }
    }
    
   
}


/**
 页面布局
 */
- (void)setUpLayout{
    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.iconImageView];
    [self.backView addSubview:self.detailLab];
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.backView.mas_top).offset(10);
    }];
    [_detailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.backView.mas_centerX);
        make.bottom.equalTo(self.backView.mas_bottom).offset(-10);
    }];
    
}
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.image = [UIImage imageNamed:@"store_phone"];
    }
    return _iconImageView;
}
- (UILabel *)detailLab {
    if (!_detailLab) {
        _detailLab = [[UILabel alloc] init];
        _detailLab.text = @"店铺";
        _detailLab.textColor = color_TextTwo;
        _detailLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _detailLab;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
//        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

@end
