//
//  DDWorkHeaderView.h
//  ZWHelp
//
//  Created by 杨旭 on 2020/8/28.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DDWorkHeaderView : UICollectionReusableView

@property(strong, nonatomic) UIView *backView;
@property(strong, nonatomic) UILabel *titleLab;
@property(strong, nonatomic) UIView *lineView;
@property(strong, nonatomic) UIButton *rightBtn;
@property(strong, nonatomic) UIImageView *rightImg;
@property(strong, nonatomic) UIView *bottomLineView;
@property (nonatomic, assign) CGFloat leftCons;

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,copy)void(^clickAllBtn)(void);

@end

NS_ASSUME_NONNULL_END
