//
//  YXMineTableView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXMineTableView : UITableView

@property (nonatomic ,assign) NSInteger userType;


@property (nonatomic ,copy)void(^clickSelectRowAtIndexPath)(NSIndexPath *indexPath);


@end

NS_ASSUME_NONNULL_END
