//
//  DDWorkHeaderView.m
//  ZWHelp
//
//  Created by 杨旭 on 2020/8/28.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDWorkHeaderView.h"

@implementation DDWorkHeaderView

- (void)setLeftCons:(CGFloat)leftCons {
    _leftCons = leftCons;
    YXWeakSelf
    [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf);
    }];
    [self layoutIfNeeded];
}


- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    
    if (indexPath.section == 1) {
        self.lineView.hidden = NO;
        self.titleLab.hidden = NO;
        self.rightBtn.hidden = NO;
        self.rightImg.hidden = NO;
        self.titleLab.text = @"消费订单";
        [self.rightBtn setTitle:@"查看全部订单" forState:(UIControlStateNormal)];
    }else if (indexPath.section == 2) {
        self.lineView.hidden = NO;
        self.titleLab.hidden = NO;
        self.rightBtn.hidden = NO;
        self.rightImg.hidden = NO;
        self.titleLab.text = @"创业技能";
        [self.rightBtn setTitle:@"提取佣金" forState:(UIControlStateNormal)];
    }else if (indexPath.section == 3) {
        self.lineView.hidden = NO;
        self.titleLab.hidden = NO;
        self.rightBtn.hidden = NO;
        self.rightImg.hidden = NO;
        self.titleLab.text = @"自媒体中心";
        [self.rightBtn setTitle:@"采购订单" forState:(UIControlStateNormal)];
    }else if (indexPath.section == 4) {
        self.lineView.hidden = NO;
        self.titleLab.hidden = NO;
        self.rightBtn.hidden = YES;
        self.rightImg.hidden = YES;
        self.titleLab.text = @"收益及提现";
    }else if (indexPath.section == 5) {
        self.lineView.hidden = YES;
        self.titleLab.hidden = YES;
        self.rightBtn.hidden = YES;
        self.rightImg.hidden = YES;
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self addSubview:self.backView];
        [self.backView addSubview:self.lineView];
        [self.backView addSubview:self.titleLab];
        [self.backView addSubview:self.rightImg];
        [self.backView addSubview:self.rightBtn];
        [self.backView addSubview:self.bottomLineView];
        [self addLayout];
    }
    return self;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor clearColor];
    }
    return _backView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont boldSystemFontOfSize:14.0f];
        _titleLab.textAlignment  =   NSTextAlignmentLeft;
    }
    return _titleLab;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = APPTintColor;
    }
    return _lineView;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_rightBtn setTitle:@"查看全部订单" forState:(UIControlStateNormal)];
//        [_rightBtn setImage:[UIImage imageNamed:@"advert_arrow_right"] forState:(UIControlStateNormal)];
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_rightBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
        _rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
//        [_rightBtn setImgViewStyle:(ButtonImgViewStyleRight) imageSize:(CGSizeMake(20, 20)) space:5];
        [_rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _rightBtn;
}
- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:@"advert_arrow_right"];
    }
    return _rightImg;
}



- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = color_LineColor;
        _bottomLineView.hidden = YES;
    }
    return _bottomLineView;
}

- (void)addLayout {
    
    DDWeakSelf    
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.top.bottom.equalTo(weakSelf);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.left.equalTo(@15);
        make.size.mas_equalTo(CGSizeMake(5, 15));
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.left.equalTo(weakSelf.lineView.mas_right).offset(15);
        make.width.offset(100);
    }];
    
    [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-10);
//        make.size.mas_equalTo(CGSizeMake(20, 20));
        [_rightImg sizeToFit];
    }];
    
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.right.equalTo(weakSelf.rightImg.mas_left).offset(-10);
        [_rightBtn sizeToFit];
    }];
    
    [_bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        make.bottom.right.equalTo(@0.0);
        make.height.equalTo(@1);
    }];
    
    [self setNeedsLayout];
}

- (void)rightBtnAction:(UIButton *)sender {
    if (self.clickAllBtn){
        self.clickAllBtn();
    }
}

@end
