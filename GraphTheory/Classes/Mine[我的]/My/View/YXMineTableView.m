//
//  YXMineTableView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/16.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXMineTableView.h"

@interface YXMineTableView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) NSArray *titlesArr;

@property (nonatomic ,strong) NSArray *titleImgArr;

@end


@implementation YXMineTableView


- (void)setUserType:(NSInteger)userType {
    _userType = userType;
    if (_userType == 0) {
        
        _titlesArr = @[@[@"提现账户",@"商家信息",@"员工管理"],@[@"广告账户",@"广告物料",@"投放管理"],@[@"系统设置",@"关于我们"]];
        _titleImgArr = @[@[@"mine_icon1",@"mine_icon2",@"mine_icon3"],@[@"mine_icon5",@"mine_icon6",@"mine_icon7"],@[@"mine_icon10",@"mine_icon11"]];
    }else {
        _titlesArr = @[@[@"广告账户",@"广告物料",@"投放管理"],@[@"系统设置",@"关于我们"]];
        _titleImgArr = @[@[@"mine_icon5",@"mine_icon6",@"mine_icon7"],@[@"mine_icon10",@"mine_icon11"]];
    }

}


- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    if (self = [super initWithFrame:frame style:style]) {
//        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = color_LineColor;
        self.separatorColor = color_LineColor;
        self.delegate = self;
        self.dataSource = self;
        self.estimatedRowHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        self.estimatedSectionFooterHeight = 0;
        
    }
    return self;
}

#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titlesArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.titlesArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"YXMineCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellID];
    }
    UIImageView *rightImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"advert_arrow_right"]];
    cell.accessoryView = rightImg;
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.textLabel.textColor = color_TextOne;
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",self.titleImgArr[indexPath.section][indexPath.row]]];
    cell.textLabel.text= self.titlesArr[indexPath.section][indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.clickSelectRowAtIndexPath) {
        self.clickSelectRowAtIndexPath(indexPath);
    }
}


@end
