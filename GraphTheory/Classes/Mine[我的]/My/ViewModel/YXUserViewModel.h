//
//  YXMyViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/26.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

@class YXMerchantInfoModel;
@interface YXUserViewModel : DDBaseViewModel

/**
 查询用户信息

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryUserInfoCompletion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 上传图片

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUploadImg:(UIImage *)img
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError;


/**
 上传视频

 @param fileName       视频文件
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUploadVideoFileName:(NSString *)fileName
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;


/**
 获取APP的其他信息

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryAboutOthersCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;


/**
 获取APP的其他信息详情
 "1":"关于我们",
 "2":"联系客服",
 "3":"图论网络用户服务协议",
 "4":"图论软件信息服务协议",
 "5":"图论平台服务商入驻协议",
 "6":"活动规则",
 "7":"隐私政策"
 @param type      类型
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryAboutOtherInfoWithType:(NSString *)type
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;



/**
 修改用户信息

 @param heardImage      用户头像
 @param name            用户昵称
 @param sex            性别
 @param birthday            生日
 @param college            毕业院校
 @param signature           个性签名
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateUserInfoWithHeardImage:(NSString *)heardImage
                                       Name:(NSString *)name
                                        sex:(NSString *)sex
                                   birthday:(NSString *)birthday
                                    college:(NSString *)college
                                  signature:(NSString *)signature
                                 Completion:(void(^)(id responesObj))completion
                                    failure:(void(^)(NSError *error))conError;


/**
 绑定手机号

 @param phone             手机号
 @param code               验证码
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestBindPhone:(NSString *)phone
                    code:(NSString *)code
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError;



/**
 关联支付宝账号

 @param account             支付宝账号
 @param username             姓名
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestBindAlipayWithAccount:(NSString *)account
                            username:(NSString *)username
                          Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;

/**
 关联支付宝账号信息

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryUserAlipayAccountCompletion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError;


/**
 修改支付密码

 @param pay_pwd             支付密码
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdatePayPwdWithPay_pwd:(NSString *)pay_pwd
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;

/**
 验证支付密码

 @param pay_pwd             支付密码
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestValidatePayPwdWithPay_pwd:(NSString *)pay_pwd
                            Completion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError;



/**
 地址列表

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryUserAddressListCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError;


/**
 新增修改地址

 @param type              0:新增 1:修改
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestCreateUserAddressWithType:(NSInteger)type
                                      Id:(NSString *)Id
                               userphone:(NSString *)userphone
                                username:(NSString *)username
                                province:(NSString *)province
                                    city:(NSString *)city
                                  region:(NSString *)region
                                 address:(NSString *)address
                              is_default:(NSString *)is_default
                              Completion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError;

/**
 删除地址

 @param addressId      地址id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestdeleteUserAddressId:(NSString *)addressId
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;



///  提交身份申请
/// @param apply_role 角色
/// @param userphone 手机号
/// @param username 姓名
/// @param images 图片
/// @param id_card 身份证号
/// @param address 地址
/// @param service_ids 服务id
/// @param service_city_ids 服务城市id
/// @param completion  返回成功
/// @param conError 返回失败
+ (void)requestApplyRole:(NSString *)apply_role
               userphone:(NSString *)userphone
                username:(NSString *)username
                  images:(NSString *)images
                 id_card:(NSString *)id_card
                 address:(NSString *)address
             service_ids:(NSString *)service_ids
        service_city_ids:(NSString *)service_city_ids
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError;


/**
 获取身份申请信息

 @param role                1:创业 2:C2M
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryApplyRoleInfoWithRole:(NSString *)role
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 获取身份申请信息

 @param role                1:创业 2:C2M
 @param userphone       手机号
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUpdateApplyInfoWithRole:(NSString *)role
                             userphone:(NSString *)userphone
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 获取用户佣金收入、走货收入、余额记录

 @param log_type         类型
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryAccountLogWithPage:(NSString *)page
                       log_type:(NSString *)log_type
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 收益转为余额

 @param type         类型
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestUserMoneyTurnBalance:(NSString *)type
                     Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

/**
 申请提现

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestApplyCashOutCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

/**
 提现记录

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryUserCashOutLogsCompletion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;

/**
 获取收藏的视频列表

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryMyCollectVideoListWithPage:(NSString *)page
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;

/**
 我的粉丝列表

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryMyFansLIstWithPage:(NSString *)page
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

/**
 关注的博主列表

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryFollowAuthorListWithPage:(NSString *)page
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;


/**
 获取某人的名片

 @param type                类型
 @param business_user_id            用户id
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryBusinessCardWithtype:(NSString *)type
                 business_user_id:(NSString *)business_user_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
 获取客服信息

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryServiceDataCompletion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;

/**
 关于我们

 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)queryAboutUsCompletion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;

/**
 退出登录
 
 @param completion      返回成功
 @param conError        返回失败
 */
+ (void)requestLogoutCompletion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;

@end

