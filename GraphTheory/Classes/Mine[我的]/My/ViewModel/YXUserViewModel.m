//
//  YXMyViewModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/26.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXUserViewModel.h"
#import "YXMerchantInfoModel.h"
@implementation YXUserViewModel

#pragma mark -  查询用户信息
+ (void)queryUserInfoCompletion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/getUserData",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  上传图片
+ (void)requestUploadImg:(UIImage *)img
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/uploadImg",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools uploadImagesBase64POSTWithUrl:urlStr parameters:params images:img success:^(id responesObj) {
        if (completion) {
            completion(responesObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  上传视频
+ (void)requestUploadVideoFileName:(NSString *)fileName
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileName]];
    NSString *urlStr = [NSString stringWithFormat:@"%@center/uploadVideo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools uploadVideoBase64POSTWithUrl:urlStr parameters:params FileData:data MimeType:@"mp4" success:^(id responesObj) {
        if (completion) {
            completion(responesObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}




#pragma mark - 获取APP的其他信息
+ (void)queryAboutOthersCompletion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/aboutOthers",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 获取APP的其他信息详情
+ (void)queryAboutOtherInfoWithType:(NSString *)type
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/aboutOtherInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  修改用户信息
+ (void)requestUpdateUserInfoWithHeardImage:(NSString *)heardImage
                                       Name:(NSString *)name
                                        sex:(NSString *)sex
                                   birthday:(NSString *)birthday
                                    college:(NSString *)college
                                  signature:(NSString *)signature
                                 Completion:(void(^)(id responesObj))completion
                                    failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/updateUserInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:heardImage forKey:@"wechat_headimgurl"];
    [params setValue:name forKey:@"nike"];
    [params setValue:sex forKey:@"sex"];
    [params setValue:birthday forKey:@"birthday"];
    [params setValue:college forKey:@"college"];
    [params setValue:signature forKey:@"signature"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  绑定手机号
+ (void)requestBindPhone:(NSString *)phone
                    code:(NSString *)code
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/bindPhone",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:phone forKey:@"phone"];
    [params setValue:code forKey:@"code"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  关联支付宝账号
+ (void)requestBindAlipayWithAccount:(NSString *)account
                            username:(NSString *)username
                          Completion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/bindAlipayAccount",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:account forKey:@"account"];
    [params setValue:username forKey:@"username"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

}

#pragma mark - 关联支付宝账号信息
+ (void)queryUserAlipayAccountCompletion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/userAlipayAccount",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

    
}

#pragma mark -  修改支付密码
+ (void)requestUpdatePayPwdWithPay_pwd:(NSString *)pay_pwd
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/updatePayPwd",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pay_pwd forKey:@"pay_pwd"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  验证支付密码
+ (void)requestValidatePayPwdWithPay_pwd:(NSString *)pay_pwd
                            Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/validatePayPwd",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:pay_pwd forKey:@"pay_pwd"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  删除地址
+ (void)requestdeleteUserAddressId:(NSString *)addressId
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/deleteUserAddress",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:addressId forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  地址列表
+ (void)queryUserAddressListCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/getUserAddressList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -   新增修改地址
+ (void)requestCreateUserAddressWithType:(NSInteger)type
                                      Id:(NSString *)Id
                               userphone:(NSString *)userphone
                                username:(NSString *)username
                                province:(NSString *)province
                                    city:(NSString *)city
                                  region:(NSString *)region
                                 address:(NSString *)address
                              is_default:(NSString *)is_default
                              Completion:(void(^)(id responesObj))completion
                                 failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (type == 0) {
        urlStr = [NSString stringWithFormat:@"%@center/createUserAddress",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@center/updateUserAddress",kBaseURL];
        [params setValue:Id forKey:@"id"];
    }
    [params setValue:userphone forKey:@"userphone"];
    [params setValue:username forKey:@"username"];
    [params setValue:province forKey:@"province"];
    [params setValue:city forKey:@"city"];
    [params setValue:region forKey:@"region"];
    [params setValue:address forKey:@"address"];
    [params setValue:is_default forKey:@"is_default"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  提交身份申请
+ (void)requestApplyRole:(NSString *)apply_role
               userphone:(NSString *)userphone
                username:(NSString *)username
                  images:(NSString *)images
                 id_card:(NSString *)id_card
                 address:(NSString *)address
             service_ids:(NSString *)service_ids
        service_city_ids:(NSString *)service_city_ids
              Completion:(void(^)(id responesObj))completion
                 failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/applyRole",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:apply_role forKey:@"apply_role"];
    [params setValue:userphone forKey:@"userphone"];
    [params setValue:username forKey:@"username"];
    [params setValue:images forKey:@"images"];
    [params setValue:id_card forKey:@"id_card"];
    [params setValue:address forKey:@"address"];
    [params setValue:service_ids forKey:@"service_ids"];
    [params setValue:service_city_ids forKey:@"service_city_ids"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 获取身份申请信息
+ (void)queryApplyRoleInfoWithRole:(NSString *)role
                     Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/applyRoleInfo",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:role forKey:@"role"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 修改身份申请信息【审核通过后可修改】
+ (void)requestUpdateApplyInfoWithRole:(NSString *)role
                             userphone:(NSString *)userphone
                     Completion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/updateApplyInfo",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:role forKey:@"role"];
    [params setValue:userphone forKey:@"userphone"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}



#pragma mark - 获取用户佣金收入、走货收入、余额记录
+ (void)queryAccountLogWithPage:(NSString *)page
                       log_type:(NSString *)log_type
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/accountLog",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [params setValue:log_type forKey:@"log_type"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 收益转为余额
+ (void)requestUserMoneyTurnBalance:(NSString *)type
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/userMoneyTurnBalance",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 申请提现
+ (void)requestApplyCashOutCompletion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/applyCashOut",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}
#pragma mark - 申请提现记录
+ (void)queryUserCashOutLogsCompletion:(void(^)(id responesObj))completion
                               failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/getUserCashOutLogs",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -   获取收藏的视频列表
+ (void)queryMyCollectVideoListWithPage:(NSString *)page
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
        
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myCollectVideoList",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  我的粉丝列表
+ (void)queryMyFansLIstWithPage:(NSString *)page
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myFansLIst",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  关注的博主列表
+ (void)queryFollowAuthorListWithPage:(NSString *)page
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/followAuthorList",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  获取某人的名片
+ (void)queryBusinessCardWithtype:(NSString *)type
                 business_user_id:(NSString *)business_user_id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@index/businessCard",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:type forKey:@"type"];
    [params setValue:business_user_id forKey:@"business_user_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -   获取客服信息
+ (void)queryServiceDataCompletion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
        
    NSString *urlStr = [NSString stringWithFormat:@"%@index/serviceData",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -   关于我们
+ (void)queryAboutUsCompletion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
        
    NSString *urlStr = [NSString stringWithFormat:@"%@index/aboutUs",kBaseURL];;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}



#pragma mark -  退出登录
+ (void)requestLogoutCompletion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/logout",kBaseURL];
    [NetWorkTools postWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

@end
