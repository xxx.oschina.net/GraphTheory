//
//  YXGoodsViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/9.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXGoodsViewModel : DDBaseViewModel


/**
 带货商品列表

 @param page     当前页
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryMyGoodsListWithPage:(NSString *)page
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;


/**
 创建/修改带货商品

 @param type       0:创建 1：修改
 @param Id       商品id
 @param title       标题
 @param sales_status     状态
 @param banner      图片
 @param stock      库存
 @param sales_price     价格
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestCreateGoodsWithType:(NSString *)type
                                Id:(NSString *)Id
                             Title:(NSString *)title
                      sales_status:(NSString *)sales_status
                            banner:(NSString *)banner
                             stock:(NSString *)stock
                       sales_price:(NSString *)sales_price
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;


/**
 删除带货商品

 @param Id       商品id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestDeleteGoodsWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError;

/**
 带货商品信息

 @param Id       商品id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryMyGoodsInfoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError;



/**
 C2M订单列表

 @param page     当前页
 @param is_delete     是否删除
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryMyVideoOrderListWithPage:(NSString *)page
                            is_delete:(NSString *)is_delete
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError;


/**
 C2M订单详情

 @param Id       商品id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryMyVideoOrderInfoWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError;


/**
 商品发货

 @param sub_order_id        订单id
 @param express_name        快递名称
 @param express_code           发货码
 @param express_no              发货订单号
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)requestaddGoodsExpress:(NSString *)sub_order_id
                  express_name:(NSString *)express_name
                  express_code:(NSString *)express_code
                    express_no:(NSString *)express_no
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;


@end

NS_ASSUME_NONNULL_END
