//
//  YXGoodsViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/9.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXGoodsViewModel.h"

@implementation YXGoodsViewModel

#pragma mark - 带货商品列表
+ (void)queryMyGoodsListWithPage:(NSString *)page
                     Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myGoodsList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  创建/修改带货商品
+ (void)requestCreateGoodsWithType:(NSString *)type
                                Id:(NSString *)Id
                             Title:(NSString *)title
                      sales_status:(NSString *)sales_status
                            banner:(NSString *)banner
                             stock:(NSString *)stock
                       sales_price:(NSString *)sales_price
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    
    
    NSString *urlStr;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([type isEqualToString:@"0"]) {
        urlStr  = [NSString stringWithFormat:@"%@center/createGoods",kBaseURL];
    }else {
        urlStr  = [NSString stringWithFormat:@"%@center/updateGoods",kBaseURL];
        [params setValue:Id forKey:@"id"];
    }
    [params setValue:title forKey:@"title"];
    [params setValue:sales_status?sales_status:@"1" forKey:@"sales_status"];
    [params setValue:banner forKey:@"banner"];
    [params setValue:stock forKey:@"stock"];
    [params setValue:sales_price forKey:@"sales_price"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 删除带货商品
+ (void)requestDeleteGoodsWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/deleteGoods",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark - 带货商品信息
+ (void)queryMyGoodsInfoWithId:(NSString *)Id
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/myGoodsInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - C2M订单列表
+ (void)queryMyVideoOrderListWithPage:(NSString *)page
                            is_delete:(NSString *)is_delete
                           Completion:(void(^)(id responesObj))completion
                              failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/myVideoOrderList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:is_delete forKey:@"is_delete"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - C2M订单详情
+ (void)queryMyVideoOrderInfoWithId:(NSString *)Id
                         Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/myVideoOrderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 商品发货
+ (void)requestaddGoodsExpress:(NSString *)sub_order_id
                  express_name:(NSString *)express_name
                  express_code:(NSString *)express_code
                    express_no:(NSString *)express_no
                    Completion:(void(^)(id responesObj))completion
                       failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/addGoodsExpress",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:sub_order_id forKey:@"sub_order_id"];
    [params setValue:express_name forKey:@"express_name"];
    [params setValue:express_code forKey:@"express_code"];
    [params setValue:express_no forKey:@"express_no"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

@end
