//
//  YXAddProductViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/9.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAddProductViewController.h"
#import "WGPublicBottomView.h"
#import "YXAddProductCollectionViewCell.h"
#import "YXGoodsViewModel.h"
#import "YXProductModel.h"
#import "YXUserViewModel.h"
#import "YXCustomBottomPopView.h"
@interface YXAddProductViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (strong, nonatomic) UIButton *rightBtn;
@property (nonatomic ,strong) NSMutableArray *selectImgArr;
@property (nonatomic ,assign) BOOL state;
@end

@implementation YXAddProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self.type isEqualToString:@"0"]) {
        self.title = @"带货产品发布";
        self.model = [YXProductModelList new];
        self.model.sales_status = @"1";
    }else {
        self.title = @"修改带货产品";
        self.state = YES;
    }
    
    [self loadSubViews];

}

#pragma mark - 设置UI
- (void)loadSubViews {
    
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
//    self.navigationItem.rightBarButtonItem = rightItem;
        
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.collectionView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
              make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
              make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
              make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
          }else{
              make.left.and.bottom.and.right.equalTo(self.view);
          }
        make.height.equalTo(@60);
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.and.top.and.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
   
}

- (void)click {
    
    
    if ([NSString isBlankString:self.model.title]) {
        return [YJProgressHUD showMessage:@"请输入产品名称"];
    }
    
    if ([NSString isBlankString:self.model.sales_price]) {
        return [YJProgressHUD showMessage:@"请输入售卖价格"];
    }

    if ([self.type isEqualToString:@"0"] ) {
        if (self.selectImgArr.count == 0) {
            return [YJProgressHUD showMessage:@"请选择商品图片"];
        }
    }else if ([self.type isEqualToString:@"1"]) {
        if (self.state == YES) {
            if (self.model.banner.length == 0) {
                return [YJProgressHUD showMessage:@"请选择商品图片"];
            }
        }else {
            if (self.selectImgArr.count == 0) {
                return [YJProgressHUD showMessage:@"请选择商品图片"];
            }
        }
       
    }
    
    if ([NSString isBlankString:self.model.stock]) {
        return [YJProgressHUD showMessage:@"请输入库存数量"];
    }
    
    
    if ([self.type isEqualToString:@"0"]) {
        [self uploadImg];
    }else {
        if (self.state == YES) {
            [self save];
        }else {
            [self uploadImg];
        }
    }
}

- (void)uploadImg {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel requestUploadImg:self.selectImgArr[0] Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSArray *dataArr = responesObj[@"data"];
            if (dataArr.count) {
                weakSelf.model.banner = dataArr[0];
                [weakSelf save];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError *error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}


- (void)save {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXGoodsViewModel requestCreateGoodsWithType:self.type
                                              Id:self.model.goods_id
                                           Title:self.model.title
                                    sales_status:self.model.sales_status
                                          banner:self.model.banner
                                           stock:self.model.stock
                                     sales_price:self.model.sales_price
                                      Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 5;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    if (indexPath.section == 2){
        return CGSizeMake(SCREEN_WIDTH - 30, 100);
    }else {
        return CGSizeMake(SCREEN_WIDTH - 30, 40);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0){
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }else {
        return UIEdgeInsetsMake(0, 15, 15, 15);
    }
    return UIEdgeInsetsZero;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    YXAddProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXAddProductCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.model;
    cell.indexPath = indexPath;
    DDWeakSelf
    [cell setEditTextBlock:^(NSString * _Nonnull text) {
        if (indexPath.section == 0) {
            weakSelf.model.title = text;
        }else if (indexPath.section == 1) {
            weakSelf.model.sales_price = text;
        }else if (indexPath.section == 3) {
            weakSelf.model.stock = text;
        }
    }];
    
    [cell.imgPicker observeSelectedMediaArray:^(NSArray<LLImagePickerModel *> *list){
        weakSelf.state = NO;
        [weakSelf.selectImgArr removeAllObjects];
        for (LLImagePickerModel *model in list){
            if (model.image) {
                [weakSelf.selectImgArr addObject:model.image];
            }
        }
     }];
     return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.type isEqualToString:@"1"]) {
        if (indexPath.section == 4) {
            YXCustomBottomPopView *popView = [[YXCustomBottomPopView alloc] initWithFrame: CGRectMake(0, kHEIGHT - 200,KWIDTH, 200) midArry:[NSMutableArray arrayWithArray:@[@"上架",@"下架"]]];
            popView.title = @"选择状态";
            [self.view addSubview:popView];
            [popView show];
            
            YXWeakSelf
            [popView setSelectPickerViewBlock:^(NSString *text, NSInteger index) {
                if (index == 0) {
                    weakSelf.model.sales_status = @"1";
                }else {
                    weakSelf.model.sales_status = @"2";
                }
                [UIView animateWithDuration:0 animations:^{
                  [collectionView performBatchUpdates:^{
                    [collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:4]]];
                  } completion:nil];
                }];
            }];
        }
        
    }
}


#pragma mark - Lazy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.showsVerticalScrollIndicator = NO;
        [_collectionView registerClass:[YXAddProductCollectionViewCell class] forCellWithReuseIdentifier:@"YXAddProductCollectionViewCell"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        if ([_type isEqualToString:@"0"]) {
            [_bottomView setTitle:@"发布"];
        }else {
            [_bottomView setTitle:@"提交"];
        }
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf click];
        }];
    }
    return _bottomView;
}


//- (UIButton *)rightBtn {
//    if (!_rightBtn) {
//        _rightBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
//        _rightBtn.frame = CGRectMake(0, 0, 60, 32);
//        [_rightBtn setTitle:@"发布" forState:(UIControlStateNormal)];
//        [_rightBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
//        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
//        [_rightBtn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
//    }
//    return _rightBtn;
//}

- (NSMutableArray *)selectImgArr {
    if (!_selectImgArr) {
        _selectImgArr = [NSMutableArray array];
    }
    return _selectImgArr;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
