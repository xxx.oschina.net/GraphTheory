//
//  YXFansViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXFansViewController.h"
#import "YXFocusTableViewCell.h"
#import "YXUserViewModel.h"
#import "UIScrollView+DREmptyDataSet.h"
#import "YXFocusModel.h"
@interface YXFansViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,strong) NSMutableArray *dataArr;

@end

@implementation YXFansViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.page = 1;
    [self requestUnRead];
}

- (void)userChangeUpDate{
    self.page = 1;
    [self requestUnRead];
}

- (void)refresh {
    YXWeakSelf;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf requestUnRead];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        weakSelf.page ++ ;
        [weakSelf requestUnRead];
    }];
}
- (void)requestUnRead{
    
    YXWeakSelf
    [YJProgressHUD showLoading:@"加载中"];
    [YXUserViewModel queryMyFansLIstWithPage:@(_page).stringValue Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            weakSelf.page++;
            YXFocusModel *model = [YXFocusModel mj_objectWithKeyValues:responesObj[@"data"]];
            NSArray *data = [YXFocusModelList mj_objectArrayWithKeyValuesArray:model.list];
            [weakSelf.dataArr addObjectsFromArray:data];
            if (data.count < 10) {
                [weakSelf.tableView.mj_footer setHidden:YES];
            }else{
                [weakSelf.tableView.mj_footer setHidden:NO];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView reloadData];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [YJProgressHUD showMessage:REQUESTERR];
    }];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的粉丝";
    _page = 1;
    [self loadSubViews];
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.bottom.left.top.right.equalTo(self.view);
        }
    }];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXFocusTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXFocusTableViewCell"];
    if (!cell) {
        cell = [[YXFocusTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXFocusTableViewCell"];
    }
    cell.type = 0;
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

#pragma mark - Lazy Loading
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[YXFocusTableViewCell class] forCellReuseIdentifier:@"YXFocusTableViewCell"];
        DDWeakSelf
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf requestUnRead];
        }];
    }
    return _tableView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
