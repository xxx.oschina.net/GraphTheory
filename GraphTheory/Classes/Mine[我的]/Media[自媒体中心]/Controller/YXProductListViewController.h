//
//  YXProductListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXProductModelList;
@interface YXProductListViewController : DDBaseViewController

// type 0：修改 1:选择
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,copy)void(^selectGoodsBlock)(YXProductModelList *model);

@end

NS_ASSUME_NONNULL_END
