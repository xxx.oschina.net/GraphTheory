//  Created by li qiao robot 
#import <Foundation/Foundation.h>
@interface YXFocusModelListAuthorInfoRoleInfoAddress : NSObject
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXFocusModelListAuthorInfoRoleInfoServicesChild : NSObject
@property (copy,nonatomic)   NSString *brand_m_id;
@property (copy,nonatomic)   NSString *brand_id;
@property (copy,nonatomic)   NSString *brand_updatetime;
@property (copy,nonatomic)   NSString *brand_pid;
@property (copy,nonatomic)   NSString *brand_picpath;
@property (copy,nonatomic)   NSString *brand_num;
@property (copy,nonatomic)   NSString *brand_rc_district;
@property (copy,nonatomic)   NSString *brand_time;
@property (copy,nonatomic)   NSString *brand_status;
@property (copy,nonatomic)   NSString *brand_name;
@end

@interface YXFocusModelListAuthorInfoRoleInfoServices : NSObject
@property (copy,nonatomic)   NSString *brand_picpath;
@property (copy,nonatomic)   NSString *brand_id;
@property (copy,nonatomic)   NSString *brand_pid;
@property (copy,nonatomic)   NSString *brand_name;
@property (strong,nonatomic) NSArray *child;
@end

@interface YXFocusModelListAuthorInfoRoleInfoServiceCities : NSObject
@property (copy,nonatomic)   NSString *district;
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *is_open;
@property (copy,nonatomic)   NSString *sort_order;
@property (copy,nonatomic)   NSString *level;
@property (copy,nonatomic)   NSString *pid;
@end

@interface YXFocusModelListAuthorInfoRoleInfo : NSObject
@property (strong,nonatomic) NSArray *serviceCities;
@property (copy,nonatomic)   NSString *id_card;
@property (strong,nonatomic) NSArray *images;
@property (copy,nonatomic)   NSString *id;
@property (strong,nonatomic) NSArray *services;
@property (strong,nonatomic) YXFocusModelListAuthorInfoRoleInfoAddress *address;
@property (strong,nonatomic) NSArray *service_city_ids;
@property (copy,nonatomic)   NSString *username;
@property (copy,nonatomic)   NSString *userphone;
@property (strong,nonatomic) NSArray *service_ids;
@end

@interface YXFocusModelListAuthorInfo : NSObject
@property (strong,nonatomic) YXFocusModelListAuthorInfoRoleInfo *roleInfo;
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *wechat_name;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *show_video_id;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;
@end

@interface YXFansInfo : NSObject
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *wechat_name;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *show_video_id;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;

@end

@interface YXFocusModelList : NSObject
@property (copy,nonatomic)   NSString *id;
@property (copy,nonatomic)   NSString *fans_id;
@property (copy,nonatomic)   NSString *author_id;
@property (copy,nonatomic)   NSString *create_time;
@property (strong,nonatomic) YXFocusModelListAuthorInfo *authorInfo;
@property (strong,nonatomic) YXFansInfo *fansInfo;

@end

@interface YXFocusModel : NSObject
@property (copy,nonatomic)   NSString *page;
@property (copy,nonatomic)   NSString *limit;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSNumber *pageCount;
@end

