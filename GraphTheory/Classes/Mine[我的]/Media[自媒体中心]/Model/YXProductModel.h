//  Created by li qiao robot 
#import "DDBaseModel.h"
@interface YXProductModelList : DDBaseModel
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *goods_m_id;
@property (copy,nonatomic)   NSString *collect_count;
@property (copy,nonatomic)   NSString *is_check;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *stock;
@property (copy,nonatomic)   NSString *title;
@property (copy,nonatomic)   NSString *view_count;
@property (copy,nonatomic)   NSString *banner;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *goods_id;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *reply_count;
@property (copy,nonatomic)   NSString *update_time;
@property (copy,nonatomic)   NSString *goods_m_updatetime;
@property (copy,nonatomic)   NSString *sales_status;

@end

@interface YXProductModel : NSObject
@property (strong,nonatomic) NSNumber *page;
@property (strong,nonatomic) NSNumber *limit;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSNumber *pageCount;
@end

