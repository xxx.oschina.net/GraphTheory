//  Created by li qiao robot 
#import "YXFocusModel.h" 
@implementation YXFocusModelListAuthorInfoRoleInfoAddress
@end

@implementation YXFocusModelListAuthorInfoRoleInfoServicesChild
@end

@implementation YXFocusModelListAuthorInfoRoleInfoServices
-(NSDictionary*)objectClassInArray{  
       return @{ @"child" : [YXFocusModelListAuthorInfoRoleInfoServicesChild class] }; 
}
@end

@implementation YXFocusModelListAuthorInfoRoleInfoServiceCities
@end

@implementation YXFocusModelListAuthorInfoRoleInfo
-(NSDictionary*)objectClassInArray{  
       return @{ @"serviceCities" : [YXFocusModelListAuthorInfoRoleInfoServiceCities class] ,@"images" : [NSString class],@"services" : [YXFocusModelListAuthorInfoRoleInfoServices class],@"service_city_ids" : [NSString class],@"service_ids" : [NSString class]}; 
}
@end

@implementation YXFocusModelListAuthorInfo
@end

@implementation YXFansInfo
@end

@implementation YXFocusModelList
@end

@implementation YXFocusModel
-(NSDictionary*)objectClassInArray{  
       return @{ @"list" : [YXFocusModelList class] }; 
}
@end

