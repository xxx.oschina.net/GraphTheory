//
//  YXFocusTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXFocusTableViewCell.h"
#import "YXFocusModel.h"
@interface YXFocusTableViewCell ()
/** 图片*/
@property (nonatomic ,strong) UIImageView *imgView;
/** 名称*/
@property (nonatomic ,strong) UILabel *nameLab;
/** 播放次数*/
@property (nonatomic ,strong) UILabel *playLab;
/** 时间*/
@property (nonatomic ,strong) UILabel *timeLab;
@end

@implementation YXFocusTableViewCell

- (void)setType:(NSInteger)type{
    _type = type;
}

- (void)setModel:(YXFocusModelList *)model {
    _model = model;
    _timeLab.text = _model.create_time;

    if (_type == 0) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.fansInfo.wechat_headimgurl]];
        _nameLab.text = _model.fansInfo.nike;
        _playLab.text = [NSString stringWithFormat:@"签名:%@",_model.fansInfo.signature];
    }else {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.authorInfo.wechat_headimgurl]];
        _nameLab.text = _model.authorInfo.nike;
        _playLab.text = [NSString stringWithFormat:@"签名:%@",_model.authorInfo.signature];
        
        if (_model.authorInfo.roleInfo.services.count) {
            NSArray *arr = [YXFocusModelListAuthorInfoRoleInfoServices mj_objectArrayWithKeyValuesArray:_model.authorInfo.roleInfo.services];
            for (int i =0;i < arr.count ; i++) {
                YXFocusModelListAuthorInfoRoleInfoServices *service  = arr[i];
                UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(150+ i * 50 , 20, 50, 18)];
                lab.backgroundColor = APPTintColor;
                lab.text = service.brand_name;
                lab.layer.masksToBounds = YES;
                lab.layer.cornerRadius = 9;
                lab.textColor = [UIColor whiteColor];
                lab.font = [UIFont systemFontOfSize:12];
                lab.textAlignment = NSTextAlignmentCenter;
                [self.contentView addSubview:lab];
            }
        }
    }
   
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.nameLab];
        [self.contentView addSubview:self.playLab];
        [self.contentView addSubview:self.timeLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
        _imgView.layer.masksToBounds = YES;
        _imgView.layer.cornerRadius = 35;
    }
    return _imgView;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [[UILabel alloc] init];
        _nameLab.font = [UIFont systemFontOfSize:16.0f];
        _nameLab.text = @"牛大";
        _nameLab.textColor = color_TextOne;
    }
    return _nameLab;
}

- (UILabel *)playLab {
    if (!_playLab) {
        _playLab = [[UILabel alloc] init];
        _playLab.textColor = color_TextTwo;
        _playLab.text = @"已禁用";
        _playLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _playLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [[UILabel alloc] init];
        _timeLab.font = [UIFont systemFontOfSize:12.0f];
        _timeLab.text = @"客服";
        _timeLab.textColor = color_TextTwo;
    }
    return _timeLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@10);
        make.size.mas_equalTo(CGSizeMake(70, 70));
    }];
    [_nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.imgView.mas_top).offset(5);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.nameLab sizeToFit];
    }];
    
    [_playLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        make.right.equalTo(weakSelf.mas_right).offset(-10);
        [weakSelf.playLab sizeToFit];
    }];

    [_timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(weakSelf.imgView.mas_bottom).offset(-5);
        make.left.equalTo(weakSelf.imgView.mas_right).offset(10);
        [weakSelf.timeLab sizeToFit];
    }];

}



@end
