//
//  YXAddProductCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDPublicCell.h"
NS_ASSUME_NONNULL_BEGIN
@class YXProductModelList;
@interface YXAddProductCollectionViewCell : DDPublicCollectionCell

@property (nonatomic ,strong) YXProductModelList *model;

@property (nonatomic ,strong) NSIndexPath *indexPath;


@end

NS_ASSUME_NONNULL_END
