//
//  YXAddProductCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/11.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAddProductCollectionViewCell.h"
#import "YXProductModel.h"

@implementation YXAddProductCollectionViewCell

- (void)setModel:(YXProductModelList *)model {
    _model = model;
}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    
    if (_indexPath.section == 0) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"请输入产品名称";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.text = _model.title;
    }else if (_indexPath.section == 1) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"售卖价格";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.keyboardType = UIKeyboardTypeDecimalPad;
        self.contentTF.text = _model.sales_price;
    }else if (_indexPath.section == 2) {
        self.type = CellViewTypeStateImage;
        if (_model.banner.length > 0) {
            self.imgPicker.preShowMedias = @[_model.banner];
        }
    }else if (_indexPath.section == 3) {
        self.type = CellViewTypeStateInput;
        self.contentTF.placeholder = @"库存数量";
        self.contentTF.textAlignment = NSTextAlignmentLeft;
        self.contentTF.keyboardType = UIKeyboardTypeNumberPad;
        self.contentTF.text = _model.stock;
    }else if (_indexPath.section == 4) {
        self.type = CellViewTypeStateNext;
        self.titleLab.text = @"状态";
        if ([self.model.sales_status isEqualToString:@"2"]) {
            self.contentLab.text = @"下架";
        }else {
            self.contentLab.text = @"上架";
        }
        self.titleLab.textColor = color_TextThree;
        self.contentLab.textColor = color_TextThree;
    }
}



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 8.0f;
    }
    return self;
}

@end
