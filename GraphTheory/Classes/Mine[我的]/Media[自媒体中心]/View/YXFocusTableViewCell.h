//
//  YXFocusTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXFocusModelList;
@interface YXFocusTableViewCell : UITableViewCell

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXFocusModelList *model;
@end

NS_ASSUME_NONNULL_END
