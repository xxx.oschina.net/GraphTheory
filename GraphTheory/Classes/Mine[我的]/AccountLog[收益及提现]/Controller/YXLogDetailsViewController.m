//
//  YXLogDetailsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXLogDetailsViewController.h"
#import "YXUserViewModel.h"
#import "YXPublicCell.h"
#import "YXLogModel.h"

@interface YXLogDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic  ,strong) UITableView *tableView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@end

@implementation YXLogDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.page = 1;
    [self loadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"账单";
    self.page = 1;
    
    [self loadSubViews];
}

//查询记录
- (void)loadData {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserViewModel queryAccountLogWithPage:@(self.page).stringValue log_type:@(self.type + 1).stringValue Completion:^(id responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            NSArray *tem = [YXLogModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
            [weakSelf.dataArr addObjectsFromArray:tem];
            if ([tem count] < 10) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [YJProgressHUD showError:REQUESTERR];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    }];
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.bottom.left.top.right.equalTo(self.view);
        }
    }];
    
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXPublicCell3 *cell = [tableView dequeueReusableCellWithIdentifier:@"YXPublicCell3"];
    if (!cell) {
        cell = [[YXPublicCell3 alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXPublicCell3"];
    }
    YXLogModel *model= self.dataArr[indexPath.row];
    cell.titleLab.text = model.title;
    cell.subTitleLab.text = model.create_time;
    cell.rightLab.text = model.show_number;
    cell.rightLab.textColor = color_TextOne;
    return cell;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark - Lazy Loading
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[YXPublicCell3 class] forCellReuseIdentifier:@"YXPublicCell3"];
        DDWeakSelf
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        }];
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf loadData];
        }];
        _tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
            weakSelf.page ++ ;
            [weakSelf loadData];
        }];

    }
    return _tableView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
