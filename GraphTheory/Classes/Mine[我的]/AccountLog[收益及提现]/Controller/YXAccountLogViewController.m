//
//  YXAccountLogViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAccountLogViewController.h"
#import "YXLogDetailsViewController.h"
#import "YXLogContentView.h"
#import "YXUserViewModel.h"
@interface YXAccountLogViewController ()
@property (nonatomic ,strong) YXLogContentView *contentView;
@property (nonatomic ,strong) NSString *log_type;
@end

@implementation YXAccountLogViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.type ==2) {
        
    }else {
        [self loadData];
    }
}
- (void)loadData {
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (self.type ==0) {
        self.title = @"创业佣金";
        self.log_type = @"1";
        self.contentView.titleLab.text = [NSString stringWithFormat:@"¥%@",[YXUserInfoManager getUserInfo].earn_money];
    }else if (self.type == 1) {
        self.title = @"走货收入";
        self.log_type = @"2";
        self.contentView.titleLab.text = [NSString stringWithFormat:@"¥%@",[YXUserInfoManager getUserInfo].goods_income];
    }else {
        self.title = @"余额提现";
        self.contentView.titleLab.text = [NSString stringWithFormat:@"¥%@",[YXUserInfoManager getUserInfo].balance];
    }

    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.frame = CGRectMake(0, 0, 40, 30);
    [btn setTitle:@"明细" forState:(UIControlStateNormal)];
    btn.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [btn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self.view addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.edges.equalTo(self.view);
        }
    }];
}

- (YXLogContentView *)contentView {
    if (!_contentView) {
        _contentView = [[YXLogContentView alloc] initWithFrame:CGRectZero];
        _contentView.type = self.type;
        DDWeakSelf
        [_contentView setClickBtnBlock:^{
            [weakSelf save];
        }];
    }
    return _contentView;
}


- (void)click {
    YXLogDetailsViewController *vc = [YXLogDetailsViewController new];
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)save {
        
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    if (self.type == 2) {
        [YXUserViewModel requestApplyCashOutCompletion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }else {
        [YXUserViewModel requestUserMoneyTurnBalance:self.log_type Completion:^(id responesObj) {
            [YJProgressHUD hideHUD];
            if (REQUESTDATASUCCESS) {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }else {
                [YJProgressHUD showMessage:responesObj[@"msg"]];
            }
        } failure:^(NSError *error) {
            [YJProgressHUD showMessage:REQUESTERR];
        }];
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
