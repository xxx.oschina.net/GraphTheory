//
//  YXAccountLogViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXAccountLogViewController : DDBaseViewController

///  type 0 :创业佣金 1：走货收入 2:余额提现
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
