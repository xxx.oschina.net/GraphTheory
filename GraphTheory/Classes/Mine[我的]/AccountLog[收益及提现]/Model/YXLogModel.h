//
//  YXLogModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/31.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXLogModel : DDBaseModel

@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *desc;
@property (copy,nonatomic)   NSString *number;
@property (copy,nonatomic)   NSString *region_number;
@property (copy,nonatomic)   NSString *show_number;
@property (copy,nonatomic)   NSString *title;


@end

NS_ASSUME_NONNULL_END
