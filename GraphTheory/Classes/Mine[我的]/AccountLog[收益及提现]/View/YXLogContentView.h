//
//  YXLogContentView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXLogContentView : UIView

/**
 背景图
 */
@property (nonatomic ,strong) UIView *backView;
/**
 按钮
 */
@property (nonatomic ,strong) UIButton *btn;
/**
 名称
 */
@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UILabel *tsLab;

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,copy)void(^clickBtnBlock)(void);

@end

NS_ASSUME_NONNULL_END
