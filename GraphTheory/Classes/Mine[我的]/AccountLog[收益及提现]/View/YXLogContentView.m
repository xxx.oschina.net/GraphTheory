//
//  YXLogContentView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/6.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXLogContentView.h"

@implementation YXLogContentView

- (void)setType:(NSInteger)type{
    _type = type;
    if (_type == 2) {
        self.tsLab.hidden = NO;
        [self.btn setTitle:@"提现" forState:(UIControlStateNormal)];
    }else {
        self.tsLab.hidden = YES;
        [self.btn setTitle:@"转入余额" forState:(UIControlStateNormal)];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
  
    }
    return self;
}

- (void)setupLayout {
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    [self.backView addSubview:self.btn];
    [self addSubview:self.tsLab];
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@10);
        make.right.equalTo(@-10);
        make.height.equalTo(@100);
    }];
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backView.mas_left).offset(20);
        make.centerY.equalTo(self.backView.mas_centerY);
        [self.titleLab sizeToFit];
    }];
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.backView.mas_centerY);
        make.right.equalTo(self.backView.mas_right).offset(-20);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    [self.tsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@10);
        make.top.equalTo(self.backView.mas_bottom).offset(10);
        [self.tsLab sizeToFit];
    }];
}

- (UILabel *)tsLab {
    if (!_tsLab) {
        _tsLab = [[UILabel alloc] init];
        _tsLab.text = @"本次提现, 免手续费";
        _tsLab.textColor = color_TextOne;
        _tsLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _tsLab;
}


- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"¥0.00";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
    }
    return _titleLab;
}
- (UIButton *)btn {
    if (!_btn) {
        _btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_btn setTitle:@"转入余额" forState:(UIControlStateNormal)];
        _btn.backgroundColor = APPTintColor;
        [_btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _btn.layer.masksToBounds = YES;
        _btn.layer.cornerRadius = 4;
        _btn.titleLabel.font = [UIFont systemFontOfSize:15.0];
        [_btn addTarget:self action:@selector(click) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _btn;
}

- (UIView *)backView{
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 4;
    }
    return _backView;
}
- (void)click {
    if (self.clickBtnBlock) {
        self.clickBtnBlock();
    }
}

@end
