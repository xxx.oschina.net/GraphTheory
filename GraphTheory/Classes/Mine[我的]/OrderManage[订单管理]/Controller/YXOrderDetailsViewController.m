//
//  YXOrderDetailsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/1.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderDetailsViewController.h"
#import "YXOrderUserInfoCollectionViewCell.h"
#import "YXOrderDetailsCollectionViewCell.h"
#import "YXOrderPayViewController.h"

#import "DDPublicCell.h"
#import "YXAmountCollectionViewCell.h"
#import "YXAnswerCollectionViewCell.h"
#import "YXComplaintCollectionViewCell.h"
#import "YXComplaintSubmitCollectionViewCell.h"
#import "YXHomeBottomCollectionViewCell.h"
#import "YXHomeViewModel.h"
#import "YXOrderInfoModel.h"
#import "YXOrderViewModel.h"
#import "YXUserOrderViewModel.h"
#import "YXDataTimeTool.h"
#import "YXCustomAlertActionView.h"
#import "YXAddressPopView.h"
#import "YXMapCollectionReusableView.h"
@interface YXOrderDetailsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) YXOrderInfoModel *model;
@property (strong, nonatomic) YXCustomAlertActionView *alertView;

@end

@implementation YXOrderDetailsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"订单详情";
    
    [self loadData];
    
    [self addSubViews];
        
}

- (void)loadData {
    
    DDWeakSelf
    if (self.type == 0) {
        [YXUserOrderViewModel querySubOrderInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
            weakSelf.model = [YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        } failure:^(NSError * _Nonnull error) {
            
        }];
    }else if (self.type == 1) {
        [YXOrderViewModel queryTakeOrderInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
            weakSelf.model = [YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        } Failure:^(NSError * _Nonnull error) {
            
        }];
    }else if (self.type == 2) {
        [YXHomeViewModel queryShareOrderInfoWithId:self.Id Completion:^(id  _Nonnull responesObj) {
            weakSelf.model = [YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.collectionView reloadData];
        } Failure:^(NSError * _Nonnull error) {
            
        }];
    }
  
    
}

- (void)geneOrderByShare {
    
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXOrderViewModel requestGeneOrderByShareWithSub_order_id:self.model.Id type:self.model.belongmode Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            NSString *big_order_id = responesObj[@"data"][@"big_order_id"];
            if (big_order_id.length) {
                YXOrderPayViewController *vc = [YXOrderPayViewController new];
                vc.order_type = @"big";
                vc.big_order_id = big_order_id;
                vc.price = weakSelf.model.product_details.c_studyprice;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } Failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
    
}

// 请求发起异议
- (void)complainSub:(NSString *)content {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryComplainSubOrderWithId:self.Id content:content Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

- (void)addSubViews {
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)show {
    
    self.alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertTextField) Title:@"发起申诉" Message:@"如对答案不满意，可向平台申诉。。" sureBtn:@"确认" cancleBtn:@"取消"];
    [self.alertView showAnimation];
    YXWeakSelf
    [self.alertView setSureClick:^(NSString * _Nonnull string) {
        [weakSelf complainSub:string];
    }];
}


#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.type == 1) { // 接单
        if ([self.model.status isEqualToString:@"1"]) {
            return 3;
        }else if ([self.model.status isEqualToString:@"10"]) {
            return 3;
        }else if ([self.model.status isEqualToString:@"15"]) {
            if ([self.model.is_complain isEqualToString:@"0"]) {
                return 5;
            }else {
                return 7;
            }
        }else if ([self.model.status isEqualToString:@"20"]||
                  [self.model.status isEqualToString:@"25"]) {
            if ([self.model.c_model isEqualToString:@"1"]) {
                return 4;
            }else {
                return 5;
            }
        }else {
            return 2;
        }
    }else if (self.type == 2) { // 课堂小纸条
        if ([self.model.status isEqualToString:@"1"]) {
            return 4;
        }else if ([self.model.status isEqualToString:@"20"]||
                  [self.model.status isEqualToString:@"25"]) {
            if (self.isPay == 1) {
                return 4;
            }else {
                return 5;
            }
        }
    }else  { // 消费订单
        if ([self.model.status isEqualToString:@"1"]) {// 待支付
            return 3;
        }else if ([self.model.status isEqualToString:@"5"]) { // 待接单
            return 4;
        }else if ([self.model.status isEqualToString:@"10"]) { // 进行中
            return 4;
        }else if ([self.model.status isEqualToString:@"15"]) {  // 待确认
            if ([self.model.is_complain isEqualToString:@"0"]) {
                return 6;
            }else {
                return 7;
            }
        }else if ([self.model.status isEqualToString:@"20"]) { // 已完成
            if ([self.model.c_model isEqualToString:@"1"]) {
                return 4;
            }else {
                return 5;
            }
        }else if ([self.model.status isEqualToString:@"25"]) { // 已评价
            if ([self.model.c_model isEqualToString:@"1"]) {
                return 4;
            }else {
                return 5;
            }
        }else { // 已取消
            return 2;
        }
    }
    
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH-30, 150);
    }else if (indexPath.section == 1 ){
        return CGSizeMake(KWIDTH-30, 340);
    }else if (indexPath.section == 2){
        return CGSizeMake(KWIDTH-30, 50);
    }else if (indexPath.section == 3){
        if (self.type == 0 && [self.model.status isEqualToString:@"5"]) {
            return CGSizeMake(KWIDTH-30, 45);
        }
        return CGSizeMake(KWIDTH-30, 150);
    }else if (indexPath.section == 4){
        return CGSizeMake(KWIDTH-30, 150);
    }else if (indexPath.section == 5){
        if (self.type == 1) {
            return CGSizeMake(KWIDTH-30, 80);
        }else if (self.type == 0) {
            if ([self.model.is_complain isEqualToString:@"0"]) {
                return CGSizeMake(KWIDTH-30, 40);
            }else {
                return CGSizeMake(KWIDTH-30, 80);
            }
        }else {
            return CGSizeMake(KWIDTH-30, 40);
        }
    }else if (indexPath.section == 6){
        return CGSizeMake(KWIDTH-30, 40);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(0, 15, 15, 15);

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    if (indexPath.section == 0) {
        YXOrderUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderUserInfoCollectionViewCell" forIndexPath:indexPath];
        cell.type = self.type;
        cell.model = self.model;
        [cell setClickAddressBlock:^{
            YXAddressPopView *popView = [[YXAddressPopView alloc] initWithFrame:[UIScreen mainScreen].bounds model:weakSelf.model];
            [popView showAnimation];
        }];
        return cell;
    }else if (indexPath.section == 1 ){
        YXOrderDetailsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderDetailsCollectionViewCell" forIndexPath:indexPath];
        cell.isPay = self.isPay;
        cell.type = self.type;
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 2){
        if (self.type == 2) { // 课堂小纸条
            if ([self.model.status isEqualToString:@"1"]||self.isPay == 1) {
                YXAmountCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXAmountCollectionViewCell" forIndexPath:indexPath];
                cell.type = self.type;
                cell.model = self.model;
                [cell setClickPayBtnBlock:^{
                    [weakSelf geneOrderByShare];
                }];
                return cell;
            }else {
                DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
                cell.type = CellViewTypeStateText;
                cell.titleLab.text = @"已支付";
                cell.contentLab.text = [NSString stringWithFormat:@"¥%@",self.model.sales_price];
                return cell;
            }
        }else { 
            if ([self.model.status isEqualToString:@"1"]) {
                YXAmountCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXAmountCollectionViewCell" forIndexPath:indexPath];
                cell.type = self.type;
                cell.model = self.model;
                [cell setClickPayBtnBlock:^{
                    YXOrderPayViewController *vc = [YXOrderPayViewController new];
                    vc.order_type = @"big";
                    vc.big_order_id = weakSelf.model.order_id;
                    vc.price = weakSelf.model.product_details.sales_price;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }];
                return cell;
            }else {
                DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
                cell.type = CellViewTypeStateText;
                cell.titleLab.text = @"已支付";
                cell.contentLab.text = [NSString stringWithFormat:@"¥%@",self.model.sales_price];
                return cell;
            }
        }
    }else if (indexPath.section == 3) {
        if (self.type == 0 && [self.model.status isEqualToString:@"5"]) {
            YXHomeBottomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell" forIndexPath:indexPath];
            [cell.btn setTitle:@"等待服务者接单.." forState:(UIControlStateNormal)];
            return cell;
        }else {
            YXOrderUserInfoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderUserInfoCollectionViewCell" forIndexPath:indexPath];
            cell.indexPath = indexPath;
            cell.type = self.type;
            cell.model = self.model;
            return cell;
        }
    }else if (indexPath.section == 4) {
        YXAnswerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXAnswerCollectionViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 5) {
        if (self.type == 1) {
            YXComplaintCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXComplaintCollectionViewCell" forIndexPath:indexPath];
            cell.model = self.model;
            return cell;
        }else {
            if ([self.model.is_complain isEqualToString:@"0"]) {
                YXComplaintSubmitCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXComplaintSubmitCollectionViewCell" forIndexPath:indexPath];
                [cell setClickBtnBlock:^{
                    [weakSelf show];
                }];
                return cell;
            }else {
                YXComplaintCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXComplaintCollectionViewCell" forIndexPath:indexPath];
                cell.model = self.model;
                return cell;
            }
        }
    }else if (indexPath.section == 6) {
        if (self.type == 1) {
            DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
            cell.type = CellViewTypeStateText;
            cell.titleLab.text = @"等待系统处理...";
            cell.contentLab.hidden = YES;
            return cell;
        }else {
            YXComplaintSubmitCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXComplaintSubmitCollectionViewCell" forIndexPath:indexPath];
            [cell setClickBtnBlock:^{
                [weakSelf show];
            }];
            return cell;
        }
      
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (self.type == 0 && [self.model.c_model isEqualToString:@"1"]) {
        if ([self.model.status isEqualToString:@"1"] ||[self.model.status isEqualToString:@"5"] ) {
            if (section == 2) {
                return CGSizeMake(KWIDTH - 30, 200);
            }
        }else if ([self.model.status isEqualToString:@"10"]||[self.model.status isEqualToString:@"20"] || [self.model.status isEqualToString:@"25"]) {
            if (section == 3) {
                return CGSizeMake(KWIDTH - 30, 200);
            }
        }else if ([self.model.status isEqualToString:@"91"]) {
            if (section == 1) {
                return CGSizeMake(KWIDTH - 30, 200);
            }
        }
    }
    return CGSizeZero;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    DDWeakSelf
   if ([kind isEqualToString:UICollectionElementKindSectionFooter])  {
       if (self.type == 0 && [self.model.c_model isEqualToString:@"1"]) {
           if ([self.model.status isEqualToString:@"1"] ||[self.model.status isEqualToString:@"5"]) {
               if (indexPath.section == 2) {
                   YXMapCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXMapCollectionReusableView" forIndexPath:indexPath];
                   footer.orderModel = self.model;
                   return footer;
               }
           }else if ([self.model.status isEqualToString:@"10"]||[self.model.status isEqualToString:@"20"] || [self.model.status isEqualToString:@"25"]) {
               if (indexPath.section == 3) {
                   YXMapCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXMapCollectionReusableView" forIndexPath:indexPath];
                   footer.orderModel = self.model;
                   return footer;
               }
           }else if ([self.model.status isEqualToString:@"91"]) {
               if (indexPath.section == 1) {
                   YXMapCollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"YXMapCollectionReusableView" forIndexPath:indexPath];
                   footer.orderModel = self.model;
                   return footer;
               }
           }
       }
    }
    return nil;

}


#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[YXOrderUserInfoCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderUserInfoCollectionViewCell"];
        [_collectionView registerClass:[DDPublicCollectionCell class] forCellWithReuseIdentifier:@"DDPublicCollectionCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXAmountCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXAmountCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXOrderDetailsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXOrderDetailsCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXAnswerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXAnswerCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXComplaintCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXComplaintCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXComplaintSubmitCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXComplaintSubmitCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXHomeBottomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXHomeBottomCollectionViewCell"];
        [_collectionView registerClass:[YXMapCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"YXMapCollectionReusableView"];

    }
    return _collectionView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
