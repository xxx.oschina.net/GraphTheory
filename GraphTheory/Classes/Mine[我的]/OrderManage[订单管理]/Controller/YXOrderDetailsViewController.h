//
//  YXOrderDetailsViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/1.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderDetailsViewController : DDBaseViewController

// type  0:消费订单 1:接单 2:分享订单
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSString *Id;

// 分享订单是否支付 
@property (nonatomic ,assign) NSInteger isPay;

@end

NS_ASSUME_NONNULL_END
