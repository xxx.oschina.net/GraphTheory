//
//  YXOrderGoodsDetailsViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/25.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXOrderGoodsDetailsViewController : DDBaseViewController

// type 0:B端用户 1:C端用户
@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) NSString *orderId;

@end

NS_ASSUME_NONNULL_END
