//
//  YXOrderGoodsDetailsViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/25.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderGoodsDetailsViewController.h"
#import "YXOrderPayViewController.h"
#import "YXOrderReceiptInfoTableViewCell.h"
#import "YXOrderGoodsTableViewCell.h"
#import "YXExpressTableViewCell.h"
#import "WGPublicBottomView.h"
#import "YXUserOrderViewModel.h"
#import "YXOrderViewModel.h"
#import "YXGoodsViewModel.h"
#import "YXOrderInfoModel.h"
@interface YXOrderGoodsDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXOrderInfoModel *model;
@property (nonatomic ,strong) NSString *express_name;
@property (nonatomic ,strong) NSString *express_no;

@end

@implementation YXOrderGoodsDetailsViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"商品订单详情";
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];

    [self loadSubViews];

}

- (void)loadData {
    
    DDWeakSelf
    [YXUserOrderViewModel querySubOrderInfoWithId:self.orderId Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
          weakSelf.model  =[YXOrderInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            if ([weakSelf.model.status isEqualToString:@"1"]) {
                weakSelf.bottomView.hidden = NO;
            }else {
                weakSelf.bottomView.hidden = YES;
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showMessage:REQUESTERR];
    }];
}

// 待发货提交
- (void)save {
    
    if ([NSString isBlankString:self.express_name]) {
        return [YJProgressHUD showMessage:@"请输入快递名称"];
    }
    if ([NSString isBlankString:self.express_no]) {
        return [YJProgressHUD showMessage:@"请输入快递单号"];
    }
    
    YXWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXGoodsViewModel requestaddGoodsExpress:self.orderId express_name:self.express_name express_code:self.express_no express_no:self.express_no Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf loadData];
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
}

- (void)loadSubViews {
    
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.tableView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        if (@available(iOS 11.0,*)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.equalTo(self.view.mas_bottom);
        }
        make.height.equalTo(@70);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.left.top.right.equalTo(self.view);
        }
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
      
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.model.status isEqualToString:@"10"]) {
        if (self.type == 0) {
            return 3;
        }else {
            return 2;
        }
    }else if ([self.model.status isEqualToString:@"15"]) {
        return 3;
    }else {
        return 2;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        YXOrderGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXOrderGoodsTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else if (indexPath.section == 1){
        YXOrderReceiptInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXOrderReceiptInfoTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        return cell;
    }else {
        YXExpressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXExpressTableViewCell" forIndexPath:indexPath];
        cell.model = self.model;
        DDWeakSelf
        [cell setClickNameBlock:^(NSString * _Nonnull text) {
            weakSelf.express_name = text;
        }];
        [cell setClickCodeBlock:^(NSString * _Nonnull text) {
            weakSelf.express_no = text;
        }];
        [cell setClickSubmitBlock:^{
            [weakSelf save];
        }];
        return cell;
        
    }
    return [UITableViewCell new];
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 100;
    }else if (indexPath.section == 1) {
        return 200;
    }else {
        if ([self.model.status isEqualToString:@"10"]) {
            return 150;
        }else {
            return 100;
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}


#pragma mark - Lazy Loading
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"YXOrderGoodsTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXOrderGoodsTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXOrderReceiptInfoTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXOrderReceiptInfoTableViewCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"YXExpressTableViewCell" bundle:nil] forCellReuseIdentifier:@"YXExpressTableViewCell"];
    }
    return _tableView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        _bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _bottomView.hidden = YES;
        [_bottomView setTitle:@"支付"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf payMethod];
        }];
    }
    return _bottomView;
}
// 支付
- (void)payMethod {
    YXOrderPayViewController *vc = [YXOrderPayViewController new];
    vc.order_type = @"big";
    vc.big_order_id = self.model.order_id;
    vc.price = self.model.sales_price;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
