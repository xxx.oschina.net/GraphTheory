//
//  YXUserOrderListViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXUserOrderListViewController.h"
#import "YXOrderDetailsViewController.h"
#import "YXOrderGoodsDetailsViewController.h"
#import "YXOrderPayViewController.h"
#import "YXEvaluationViewController.h"

#import <JJCollectionViewRoundFlowLayout.h>
#import "UIScrollView+DREmptyDataSet.h"
#import "YXOrderHeadCollectionViewCell.h"
#import "YXOrderFootCollectionViewCell.h"
#import "YXOrderContentCollectionViewCell.h"
#import "YXCustomAlertActionView.h"
#import "YXUserOrderViewModel.h"
#import "YXUserOrderModel.h"

@interface YXUserOrderListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) YXCustomAlertActionView *alertView;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@property (nonatomic ,assign) NSInteger page;
@property (nonatomic ,assign) YXUserOrderModelList *model;
@end

@implementation YXUserOrderListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.page = 1;
    [self request];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.type == 2) {
        self.title = @"采购订单";
    }
    
    [self.view addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}
- (void)request {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel querySubOrderListWithPage:@(self.page).stringValue order_field:@"" order_type:self.orderStatus Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (weakSelf.page == 1) {
            [weakSelf.dataArr removeAllObjects];
        }
        if (REQUESTDATASUCCESS) {
            YXUserOrderModel *model = [YXUserOrderModel mj_objectWithKeyValues:responesObj[@"data"]];
            [weakSelf.dataArr addObjectsFromArray:model.list];
            [weakSelf.collectionView reloadData];
            [weakSelf.collectionView.mj_header endRefreshing];
            [weakSelf.collectionView.mj_footer endRefreshing];
            if ([model.list count] < 10) {
                [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
            }
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }        
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
        [YJProgressHUD showError:REQUESTERR];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
    }];
}

// 请求删除订单
- (void)delete {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryDeleteSubOrderWithId:self.model.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

// 请求取消订单
- (void)cancel {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryCancelSubOrderWithId:self.model.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}
// 请求确认订单
- (void)complete {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryCompleteSubOrderWithId:self.model.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

// 请求发起异议
- (void)complainSub:(NSString *)content {
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryComplainSubOrderWithId:self.model.Id content:content Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}


// 推荐学习
- (void)recommended {
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryShareSubOrderWithId:self.model.Id Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
            weakSelf.page = 1;
            [weakSelf request];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
    }];
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.dataArr.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 3;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    YXUserOrderModelList *shop;
    if (indexPath.section < _dataArr.count) {
        shop = _dataArr[indexPath.section];
    }
    if (indexPath.row == 0) {
        return CGSizeMake(KWIDTH-30, 44);
    }else if (indexPath.row == 1 ){
        return CGSizeMake(KWIDTH-30, 70);
    }else if (indexPath.row == 2){
        return CGSizeMake(KWIDTH-30, 44);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(15, 15, 15, 15);

}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    YXUserOrderModelList *model;
    if (indexPath.section < _dataArr.count) {
        model = _dataArr[indexPath.section];
        self.model = model;
    }
    if (indexPath.row == 0) {
        YXOrderHeadCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderHeadCollectionViewCell" forIndexPath:indexPath];
        cell.type = self.type;
        cell.indexPath = indexPath;
        cell.model = model;
        [cell setClickDeleteBtn:^(NSIndexPath * _Nonnull indexPath) {
            weakSelf.model = weakSelf.dataArr[indexPath.section];
            [weakSelf showAlertViewWithType:0 Message:@"确定要删除本条数据吗？"];
        }];
        return cell;
    }else if (indexPath.row == 1 ){
        YXOrderContentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderContentCollectionViewCell" forIndexPath:indexPath];
        cell.type = self.type;
        cell.model = model;
        return cell;
    }else if (indexPath.row == 2){
        YXOrderFootCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderFootCollectionViewCell" forIndexPath:indexPath];
        cell.type = self.type;
        cell.indexPath = indexPath;
        cell.model = model;
        YXWeakSelf
        [cell setClickBottomBtnBlock:^(NSIndexPath * _Nonnull indexPath, UIButton * _Nonnull btn) {
            NSLog(@"btnTitle == %@",btn.titleLabel.text);
            YXUserOrderModelList *model = weakSelf.dataArr[indexPath.section];
            weakSelf.model = model;
            if ([btn.titleLabel.text isEqualToString:@"待支付"]) {
                YXOrderPayViewController *vc = [YXOrderPayViewController new];
                vc.order_type = @"big";
                vc.big_order_id = model.order_id;
                vc.price = model.sales_price;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else if ([btn.titleLabel.text isEqualToString:@"取消"]) {
                [weakSelf showAlertViewWithType:2 Message:@"确认取消订单？"];
            }else if ([btn.titleLabel.text isEqualToString:@"进行中"]) {
                
            }else if ([btn.titleLabel.text isEqualToString:@"异议"]) {
                weakSelf.alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertTextField) Title:@"发起异议" Message:@"请输入异议内容" sureBtn:@"确认" cancleBtn:@"取消"];
                [weakSelf.alertView showAnimation];
                YXWeakSelf
                [weakSelf.alertView setSureClick:^(NSString * _Nonnull string) {
                    [weakSelf complainSub:string];
                }];
            }else if ([btn.titleLabel.text isEqualToString:@"确认"]) {
                [weakSelf showAlertViewWithType:3 Message:@"是否确认该服务？"];
            }else if ([btn.titleLabel.text isEqualToString:@"推荐学习"]) {
                [weakSelf showAlertViewWithType:4 Message:@"是否确认推荐到学习列表？"];
            }else if ([btn.titleLabel.text isEqualToString:@"去评价"]) {
                YXEvaluationViewController *vc = [YXEvaluationViewController new];
                vc.type = 0;
                vc.model = model;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else if ([btn.titleLabel.text isEqualToString:@"已评价"]) {
                YXEvaluationViewController *vc = [YXEvaluationViewController new];
                vc.type = 1;
                vc.model = model;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }

        }];
        return cell;
    }
    return nil;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    YXUserOrderModelList *model = self.dataArr[indexPath.section];
    if (self.type == 2) {
        YXOrderGoodsDetailsViewController *vc = [YXOrderGoodsDetailsViewController new];
        vc.type = 1;
        vc.orderId = model.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        YXOrderDetailsViewController *vc = [YXOrderDetailsViewController new];
        vc.type = 0;
        vc.Id = model.Id;
        [self.navigationController pushViewController:vc animated:YES];
    }

}

#pragma mark - JJCollectionViewDelegateRoundFlowLayout
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout borderEdgeInsertsForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(10, 15, 10, 15);
    }
    return UIEdgeInsetsMake(10, 15, 10, 15);
}

- (JJCollectionViewRoundConfigModel *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout configModelForSectionAtIndex:(NSInteger)section{
    JJCollectionViewRoundConfigModel *model = [[JJCollectionViewRoundConfigModel alloc]init];
    if (@available(iOS 13.0, *)) {
        model.backgroundColor = [UIColor colorWithDynamicProvider:^UIColor * _Nonnull(UITraitCollection * _Nonnull traitCollection) {
            return traitCollection.userInterfaceStyle == UIUserInterfaceStyleLight ? [UIColor whiteColor] : [UIColor blackColor];
        }];
    } else {
        // Fallback on earlier versions
        model.backgroundColor = [UIColor whiteColor];
    }
    model.cornerRadius = 8;
    return model;
}

/**
 显示弹窗

 @param type        类型 0.删除 1.待支付 2.取消 3.确认 4.推荐学习
 @param message     提示语
 */
- (void)showAlertViewWithType:(NSInteger)type Message:(NSString *)message{
    
    _alertView = [[YXCustomAlertActionView alloc] initWithFrame:[UIScreen mainScreen].bounds ViewType:(AlertText) Title:@"提示" Message:message sureBtn:@"确认" cancleBtn:@"取消"];
    [_alertView showAnimation];
    YXWeakSelf
    [_alertView setSureClick:^(NSString * _Nonnull string) {
        if (type == 0) {
            [weakSelf delete];
        }else if (type == 1) {
            
        }else if (type == 2) {
            [weakSelf cancel];
        }else if (type == 3) {
            [weakSelf complete];
        }else if (type == 4) {
            [weakSelf recommended];
        }
    }];
    
}




#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        DDWeakSelf
        JJCollectionViewRoundFlowLayout *flowLayout = [[JJCollectionViewRoundFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.isCalculateHeader = NO;
        flowLayout.isCalculateFooter = NO;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.frame = CGRectMake(0, 0, KWIDTH, kHEIGHT-50);
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.emptyDataSetSource = self;
        _collectionView.emptyDataSetDelegate = self;
        _collectionView.backgroundColor = color_LineColor;
        
        [_collectionView registerClass:[YXOrderHeadCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderHeadCollectionViewCell"];
        [_collectionView registerClass:[YXOrderContentCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderContentCollectionViewCell"];
        [_collectionView registerClass:[YXOrderFootCollectionViewCell class] forCellWithReuseIdentifier:@"YXOrderFootCollectionViewCell"];
        [_collectionView setupEmptyDataText:@"暂无订单" tapBlock:^{
            weakSelf.page = 1;
            [weakSelf request];
        }];
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            weakSelf.page = 1;
            [weakSelf request];
        }];
        _collectionView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
            weakSelf.page ++ ;
            [weakSelf request];
        }];
        
    }
    return _collectionView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
