//
//  YXEvaluationViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN
@class YXUserOrderModelList;
@interface YXEvaluationViewController : DDBaseViewController
// type 0 未评价 1 已评价
@property (nonatomic ,assign) NSInteger type;
@property (nonatomic ,strong)  YXUserOrderModelList *model;
@end

NS_ASSUME_NONNULL_END
