//
//  YXEvaluationViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEvaluationViewController.h"
#import "YXUserOrderModel.h"
#import "YXEvaluationCollectionViewCell.h"
#import "YXOrderDetailsHeaderCollectionViewCell.h"
#import "WGPublicBottomView.h"
#import "DDPublicCell.h"
#import "YXUserOrderViewModel.h"
#import "UITextView+Placeholder.h"
#import "HCSStarRatingView.h"
#import "YXEvaluationModel.h"
@interface YXEvaluationViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) UICollectionView *collectionView;
@property (nonatomic ,strong) WGPublicBottomView *bottomView;
@property (nonatomic ,strong) YXEvaluationModel *elModel;

@end

@implementation YXEvaluationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"发表评价";
    
    if (self.type == 1) {
        [self loadData];
    }else {
        self.elModel = [YXEvaluationModel new];
        self.elModel.total_star = @"5";
        self.elModel.process_star = @"5";
        self.elModel.speed_star = @"5";
        self.elModel.attitude_star = @"5";
        self.elModel.efficiency_star = @"5";
    }
    
    [self loadSubViews];

}

- (void)loadData {
    
    DDWeakSelf
    [YXUserOrderViewModel queryCommentSubOrderWithId:self.model.Id type:1 title:nil content:nil total_star:nil process_star:nil speed_star:nil attitude_star:nil efficiency_star:nil Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.elModel = [YXEvaluationModel mj_objectWithKeyValues:responesObj[@"data"]];
        }
        [weakSelf.collectionView reloadData];
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


- (void)save {
    
    if ([NSString isBlankString:self.elModel.content]) {
        return [YJProgressHUD showMessage:@"请输入评论内容"];
    }
    
    DDWeakSelf
    [YJProgressHUD showLoading:@""];
    [YXUserOrderViewModel queryCommentSubOrderWithId:self.model.Id type:0 title:self.elModel.content content:self.elModel.content total_star:self.elModel.total_star process_star:self.elModel.process_star speed_star:self.elModel.speed_star attitude_star:self.elModel.attitude_star efficiency_star:self.elModel.efficiency_star Completion:^(id  _Nonnull responesObj) {
        [YJProgressHUD hideHUD];
        if (REQUESTDATASUCCESS) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [YJProgressHUD showMessage:@"评价成功"];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD hideHUD];
    }];
    
}

#pragma mark - 设置UI
- (void)loadSubViews {
    
    if (self.type == 1) {
        [self.view addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
                make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            }else{
                make.left.and.top.and.bottom.and.right.equalTo(self.view);
            }
        }];
        
    }else {
        [self.view addSubview:self.bottomView];
        [self.view addSubview:self.collectionView];
        [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                  make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
                  make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                  make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
              }else{
                  make.left.and.bottom.and.right.equalTo(self.view);
              }
            make.height.equalTo(@60);
        }];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (@available(iOS 11.0,*)) {
                make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
                make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
                make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            }else{
                make.left.and.top.and.right.equalTo(self.view);
            }
            make.bottom.equalTo(self.bottomView.mas_top);
        }];
    }
    
 
   
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        return CGSizeMake(KWIDTH-30, 150);
    }else if (indexPath.section == 1 ){
        return CGSizeMake(KWIDTH-30, 100);
    }else if (indexPath.section == 2){
        return CGSizeMake(KWIDTH-30, 200);
    }
    return CGSizeZero;
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section == 0) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsMake(0, 15, 15, 15);

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    YXWeakSelf;
    if (indexPath.section == 0) {
        YXOrderDetailsHeaderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXOrderDetailsHeaderCollectionViewCell" forIndexPath:indexPath];
        cell.type = self.type;
        cell.model = self.model;
        cell.elModel = self.elModel;
        [cell setClickStarBlock:^(CGFloat value) {
            weakSelf.elModel.total_star = [NSString stringWithFormat:@"%f",value];
        }];
        return cell;
    }else if (indexPath.section == 1 ){
        DDPublicCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPublicCollectionCell" forIndexPath:indexPath];
        cell.type = CellViewTypeStateTextView;
        cell.textView.placeholder = @"请输入评论内容...";
        cell.textView.text = self.elModel.content;
        if (self.type == 1) {
            cell.textView.editable = NO;
        }
        [cell setTextViewChange:^(NSString * _Nonnull text) {
            weakSelf.elModel.content = text;
        }];
        return cell;
    }else if (indexPath.section == 2){
        YXEvaluationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YXEvaluationCollectionViewCell" forIndexPath:indexPath];
        cell.starView1.value = [self.elModel.process_star floatValue];
        cell.starView2.value = [self.elModel.speed_star floatValue];
        cell.starView3.value = [self.elModel.attitude_star floatValue];
        cell.starView4.value = [self.elModel.efficiency_star floatValue];
        if (self.type == 1) {
            cell.starView1.userInteractionEnabled = NO;
            cell.starView2.userInteractionEnabled = NO;
            cell.starView3.userInteractionEnabled = NO;
            cell.starView4.userInteractionEnabled = NO;
        }
        [cell setClickStarBlock:^(HCSStarRatingView * _Nonnull starView) {
            if (starView.tag == 1000) {
                weakSelf.elModel.process_star = [NSString stringWithFormat:@"%lf",starView.value];
            }else if (starView.tag == 1002) {
                weakSelf.elModel.speed_star = [NSString stringWithFormat:@"%lf",starView.value];
            }else if (starView.tag == 1003) {
                weakSelf.elModel.attitude_star = [NSString stringWithFormat:@"%lf",starView.value];
            }else if (starView.tag == 1004) {
                weakSelf.elModel.efficiency_star = [NSString stringWithFormat:@"%lf",starView.value];
            }
        }];
        return cell;
    }
    return nil;
}


#pragma mark - Lozy Loading
- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout * flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.collectionViewLayout = flowLayout;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [_collectionView registerClass:[DDPublicCollectionCell class] forCellWithReuseIdentifier:@"DDPublicCollectionCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXEvaluationCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXEvaluationCollectionViewCell"];
        [_collectionView registerNib:[UINib nibWithNibName:@"YXOrderDetailsHeaderCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YXOrderDetailsHeaderCollectionViewCell"];
    }
    return _collectionView;
}
- (WGPublicBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[WGPublicBottomView alloc] initWithFrame:CGRectZero];
        [_bottomView setTitle:@"发布"];
        YXWeakSelf
        [_bottomView setClickButtonBlock:^(NSInteger index) {
            [weakSelf save];
        }];
    }
    return _bottomView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
