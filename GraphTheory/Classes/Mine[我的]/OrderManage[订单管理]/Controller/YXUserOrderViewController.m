//
//  YXUserOrderViewController.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXUserOrderViewController.h"
#import <JXCategoryView.h>
#import "YXUserOrderListViewController.h"
#import "YXUserOrderViewModel.h"
@interface YXUserOrderViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray *titles;//标题数组
@property (nonatomic, strong) NSArray *keys;//标题数组
@property (nonatomic, strong) JXCategoryNumberView *categoryView;//类型选择器
@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;//类型视图选择器
@end

@implementation YXUserOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"消费订单";
    
//    [self loadData];

    _titles = @[@"全部",@"待支付",@"待接单",@"进行中",@"待确认",@"已完成"];
    _keys = @[@"1,2",@"1",@"5",@"10",@"15",@"20"];
    [self addSubviewsToVcView];
    _categoryView.titles = _titles;
    _categoryView.counts = @[@0,@0,@0,@0,@0,@0]; // 角标显示数量
    
}

- (void)loadData {
    
    DDWeakSelf
    [YXUserOrderViewModel queryOrderStatusCompletion:^(id  _Nonnull responesObj) {
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}


#pragma mark - JXCategoryViewDelegate
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    NSLog(@"%@", NSStringFromSelector(_cmd));
}
#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    YXUserOrderListViewController *list = [[YXUserOrderListViewController alloc] init];
    list.orderStatus = _keys[index];
    list.type = 0;
    return list;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}
#pragma mark - Intial Methods
- (void)addSubviewsToVcView{//添加子视图

    [self.view addSubview:self.categoryView];
    [self.view addSubview:self.listContainerView];
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.height.mas_equalTo(@44);
        if (@available(iOS 11.0,*)) {
            make.top.mas_equalTo(self.view.mas_safeAreaLayoutGuideTop);
        }else{
            make.top.mas_equalTo(0);
        }
    }];
    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(self.view);
        make.top.mas_equalTo(self.categoryView.mas_bottom);
        if (@available(iOS 11.0,*)) {
            make.bottom.mas_equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        }else{
            make.bottom.mas_equalTo(0);
        }
    }];
}
- (JXCategoryNumberView *)categoryView {
    if (!_categoryView) {
        _categoryView = [[JXCategoryNumberView alloc]init];
        _categoryView.delegate = self;
        _categoryView.titleColorGradientEnabled = YES;
        _categoryView.titleFont = [UIFont systemFontOfSize:16.0];
        _categoryView.titleColor = color_TextOne;
        _categoryView.titleSelectedColor = APPTintColor;
        _categoryView.defaultSelectedIndex = _selectIndex;
        _categoryView.numberBackgroundColor = [UIColor redColor];
        _categoryView.numberLabelFont = [UIFont systemFontOfSize:10.0f];
        _categoryView.numberLabelHeight = 12.0f;
        JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
        lineView.indicatorWidth = JXCategoryViewAutomaticDimension;
        //可以试试宽度补偿
        lineView.indicatorColor = APPTintColor;
        lineView.indicatorHeight = 2;
        lineView.indicatorWidthIncrement = 0;
        lineView.verticalMargin = 6;
        _categoryView.indicators = @[lineView];
        _categoryView.listContainer = self.listContainerView;
        _listContainerView = self.listContainerView;
    }
    return _categoryView;
}
- (JXCategoryListContainerView *)listContainerView {
    if (!_listContainerView) {
        _listContainerView = [[JXCategoryListContainerView alloc]initWithType:JXCategoryListContainerType_CollectionView delegate:self];
    }
    return _listContainerView;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
