//
//  YXUserOrderListViewController.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXUserOrderListViewController : DDBaseViewController<JXCategoryListContentViewDelegate>

@property (nonatomic ,strong) NSString *orderStatus;///<订单状态 0全部1进行中 2已完成3待确认4退款/售后

// type 0:消费订单 1:接单 2:采购订单
@property (nonatomic ,assign) NSInteger type;

@end

NS_ASSUME_NONNULL_END
