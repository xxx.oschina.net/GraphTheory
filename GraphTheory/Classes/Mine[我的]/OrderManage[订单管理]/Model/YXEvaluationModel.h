//
//  YXEvaluationModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/13.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXEvaluationModel : DDBaseModel

@property (nonatomic ,copy) NSString *content;
@property (nonatomic ,copy) NSString *total_star;
@property (nonatomic ,copy) NSString *process_star;
@property (nonatomic ,copy) NSString *speed_star;
@property (nonatomic ,copy) NSString *attitude_star;
@property (nonatomic ,copy) NSString *efficiency_star;

@end

NS_ASSUME_NONNULL_END
