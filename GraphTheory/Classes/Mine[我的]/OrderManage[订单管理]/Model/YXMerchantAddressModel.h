//
//  YXMerchantAddressModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/11/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXMerchantAddressModel : DDBaseModel

@property (nonatomic ,copy) NSString *Id;
/** 省id*/
@property (nonatomic ,copy) NSString *parentId;
/** 城市id*/
@property (nonatomic ,copy) NSString *areaId;
/** 城市名称*/
@property (nonatomic ,copy) NSString *areaName;
@property (nonatomic ,copy) NSString *fullName;
@property (nonatomic ,copy) NSString *level;

@end

NS_ASSUME_NONNULL_END
