//
//  YXMerchantInfoModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/8/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXMerchantInfoModel : DDBaseModel

/** 商家id*/
@property (nonatomic ,copy) NSString *merchantId;
/** 商家名称*/
@property (nonatomic ,copy) NSString *businessName;
@property (nonatomic ,copy) NSString *merchantName;
/** 管理员账号*/
@property (nonatomic ,copy) NSString *adminMobile;
/** 管理员姓名*/
@property (nonatomic ,copy) NSString *name;
/** 管理员身份证号*/
@property (nonatomic ,copy) NSString *cardNo;
/** 商家地址*/
@property (nonatomic ,copy) NSString *businessAddress;
@property (nonatomic ,copy) NSString *address;

/** 省*/
@property (nonatomic ,copy) NSString *province;
@property (nonatomic ,copy) NSString *shengName;
/** 市*/
@property (nonatomic ,copy) NSString *city;
@property (nonatomic ,copy) NSString *shiName;
/** 县*/
@property (nonatomic ,copy) NSString *county;
@property (nonatomic ,copy) NSString *xianName;

@property (nonatomic ,assign) NSInteger personalState;

@end

NS_ASSUME_NONNULL_END
