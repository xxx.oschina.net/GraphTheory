//  Created by li qiao robot 
#import "DDBaseModel.h"
@interface YXUserOrderModelListUserInfo : DDBaseModel
@property (copy,nonatomic)   NSString *email;
@property (copy,nonatomic)   NSString *nike;
@property (copy,nonatomic)   NSString *us_account;
@property (copy,nonatomic)   NSString *signature;
@property (copy,nonatomic)   NSString *wechat_headimgurl;
@property (copy,nonatomic)   NSString *address;
@end

@interface YXUserOrderModelListReceiver : DDBaseModel
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *province;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *city;
@property (copy,nonatomic)   NSString *region;
@property (copy,nonatomic)   NSString *username;
@property (copy,nonatomic)   NSString *is_default;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *userphone;
@end

@interface YXUserOrderModelListFrom_address : DDBaseModel
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXUserOrderModelListProduct_detailsAddress : DDBaseModel
@property (copy,nonatomic)   NSString *address;
@property (copy,nonatomic)   NSString *lat;
@property (copy,nonatomic)   NSString *lng;
@end

@interface YXUserOrderModelListProduct_details : DDBaseModel
@property (copy,nonatomic)   NSString *name;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *cate_id;
@property (copy,nonatomic)   NSString *imgs;
@property (copy,nonatomic)   NSString *content;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *belongMode;
@property (copy,nonatomic)   NSString *c_address;
@property (strong,nonatomic) NSNumber *coupon_id;
@property (copy,nonatomic)   NSString *c_pass;
@property (strong,nonatomic) YXUserOrderModelListProduct_detailsAddress *address;
@property (strong,nonatomic) NSNumber *number;
@property (copy,nonatomic)   NSString *rebate;
@property (copy,nonatomic)   NSString *remark;
@property (strong,nonatomic) NSNumber *coupon_price;
@property (copy,nonatomic)   NSString *service_city_id;
@property (copy,nonatomic)   NSString *c_model;
@end

@interface YXUserOrderModelList : DDBaseModel
@property (copy,nonatomic)   NSString *c_model;  
@property (copy,nonatomic)   NSString *coupon_code;
@property (strong,nonatomic) YXUserOrderModelListProduct_details *product_details;
@property (copy,nonatomic)   NSString *belongmode;
@property (copy,nonatomic)   NSString *is_delete;
@property (copy,nonatomic)   NSString *Id;
@property (copy,nonatomic)   NSString *m_updatetime;
@property (copy,nonatomic)   NSString *complain;
@property (copy,nonatomic)   NSString *statusTitle;
@property (copy,nonatomic)   NSString *yiyimoney;
@property (copy,nonatomic)   NSString *c_longtime;
@property (copy,nonatomic)   NSString *remark;
@property (copy,nonatomic)   NSString *is_overdue;
@property (copy,nonatomic)   NSString *sales_price;
@property (copy,nonatomic)   NSString *answer_desc;
@property (copy,nonatomic)   NSString *share_m_id;
@property (copy,nonatomic)   NSString *answer;
@property (copy,nonatomic)   NSString *solve;
@property (strong,nonatomic) YXUserOrderModelListFrom_address *from_address;
@property (copy,nonatomic)   NSString *service_city_id;
@property (strong,nonatomic) NSArray *imgs;
@property (copy,nonatomic)   NSString *c_pass;
@property (copy,nonatomic)   NSString *share_shenhe_time;
@property (copy,nonatomic)   NSString *create_time;
@property (copy,nonatomic)   NSString *complete_time;
@property (copy,nonatomic)   NSString *is_share;
@property (copy,nonatomic)   NSString *pay_price;
@property (copy,nonatomic)   NSString *take_user_id;
@property (copy,nonatomic)   NSString *order_id;
@property (copy,nonatomic)   NSString *finish_time;
@property (strong,nonatomic) YXUserOrderModelListReceiver *receiver;
@property (copy,nonatomic)   NSString *coupon_price;
@property (copy,nonatomic)   NSString *take_price;
@property (copy,nonatomic)   NSString *status;
@property (copy,nonatomic)   NSString *c_address;
@property (copy,nonatomic)   NSString *solve_time;
@property (copy,nonatomic)   NSString *product_icon;
@property (copy,nonatomic)   NSString *order_no;
@property (copy,nonatomic)   NSString *pay_time;
@property (copy,nonatomic)   NSString *share_time;
@property (copy,nonatomic)   NSString *content;
@property (copy,nonatomic)   NSString *user_id;
@property (copy,nonatomic)   NSString *coupon_id;
@property (copy,nonatomic)   NSString *share_check;
@property (copy,nonatomic)   NSString *is_complain;
@property (copy,nonatomic)   NSString *complain_time;
@property (copy,nonatomic)   NSString *order_type; // 1:消费订单 2：课堂小纸条
@property (copy,nonatomic)   NSString *pay_type;
@property (copy,nonatomic)   NSString *delete_time;
@property (copy,nonatomic)   NSString *take_time;
@property (copy,nonatomic)   NSString *m_id;
@property (strong,nonatomic) YXUserOrderModelListUserInfo *userInfo;
@end

@interface YXUserOrderModel : DDBaseModel
@property (strong,nonatomic) NSNumber *totalPage;
@property (strong,nonatomic) NSNumber *pageLimit;
@property (strong,nonatomic) NSNumber *totalCount;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) NSNumber *page;
@end

