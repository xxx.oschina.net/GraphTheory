//
//  YXEvaluationCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEvaluationCollectionViewCell.h"
#import "HCSStarRatingView.h"

@interface  YXEvaluationCollectionViewCell ()

@end

@implementation YXEvaluationCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.starView1.tag = 1000;
    self.starView2.tag = 1001;
    self.starView3.tag = 1002;
    self.starView4.tag = 1003;
    self.starView1.tintColor = APPTintColor;
    self.starView2.tintColor = APPTintColor;
    self.starView3.tintColor = APPTintColor;
    self.starView4.tintColor = APPTintColor;

}

- (IBAction)starClick:(HCSStarRatingView *)sender {
    if (self.clickStarBlock) {
        self.clickStarBlock(sender);
    }
}



@end
