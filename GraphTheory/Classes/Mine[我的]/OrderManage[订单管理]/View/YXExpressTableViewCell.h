//
//  YXExpressTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXExpressTableViewCell : UITableViewCell

@property (nonatomic ,strong) YXOrderInfoModel *model;
@property (nonatomic ,copy)void(^clickNameBlock)(NSString *text);
@property (nonatomic ,copy)void(^clickCodeBlock)(NSString *text);
@property (nonatomic ,copy)void(^clickSubmitBlock)(void);


@end

NS_ASSUME_NONNULL_END
