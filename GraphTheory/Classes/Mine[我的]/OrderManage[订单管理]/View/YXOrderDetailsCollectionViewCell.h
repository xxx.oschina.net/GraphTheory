//
//  YXOrderDetailsCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;

@interface YXOrderDetailsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *numberLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *noteLab;

@property (weak, nonatomic) IBOutlet UILabel *couponLab;

@property (weak, nonatomic) IBOutlet UILabel *couponCodeLab;

@property (weak, nonatomic) IBOutlet UILabel *typeLab;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *orderStateLab;
@property (weak, nonatomic) IBOutlet UILabel *payTypeLab;

// 分享订单是否支付
@property (nonatomic ,assign) NSInteger isPay;

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXOrderInfoModel *model;

@end

NS_ASSUME_NONNULL_END
