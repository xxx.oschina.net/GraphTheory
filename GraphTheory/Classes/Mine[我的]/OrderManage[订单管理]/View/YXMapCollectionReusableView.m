//
//  YXMapCollectionReusableView.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/24.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMapCollectionReusableView.h"
#import <MAMapKit/MAMapKit.h>
#import "YXOrderInfoModel.h"

@interface YXMapCollectionReusableView ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@end

@implementation YXMapCollectionReusableView

- (void)setOrderModel:(YXOrderInfoModel *)orderModel {
    _orderModel = orderModel;
    if (_orderModel.filterTakeUsers.count) {
        YXOrderInfoModelFilterTakeUsers *takeUsers = _orderModel.filterTakeUsers[0];
        CGFloat lat = [takeUsers.userInfo.address.lat floatValue];
        CGFloat lng = [takeUsers.userInfo.address.lng floatValue];
        CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lng);
        self.mapView.centerCoordinate = coor;
        NSMutableArray *tempArr = [NSMutableArray array];
        for (YXOrderInfoModelFilterTakeUsers *takeUsers in _orderModel.filterTakeUsers) {
            MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc] init];
            CGFloat lat = [takeUsers.userInfo.address.lat floatValue];
            CGFloat lng = [takeUsers.userInfo.address.lng floatValue];
            CLLocationCoordinate2D coor = CLLocationCoordinate2DMake(lat, lng);
            pointAnnotation.coordinate = coor;
            pointAnnotation.title = takeUsers.userInfo.username;
            pointAnnotation.subtitle = takeUsers.userInfo.userphone;
            [tempArr addObject:pointAnnotation];
            [self.mapView addAnnotations:tempArr];

        }
    }

}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.mapView];
    }
    return self;
}

- (MAMapView *)mapView {
    if (!_mapView) {
        _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
         _mapView.backgroundColor = [UIColor whiteColor];
        _mapView.delegate = self;
         //设置定位精度
         _mapView.desiredAccuracy = kCLLocationAccuracyBest;
        //设置定位距离
         _mapView.distanceFilter = 5.0f;
          _mapView.zoomEnabled = YES;
        _mapView.showsCompass = NO;
          //缩放等级
          [_mapView setZoomLevel:14 animated:YES];
//        _mapView.showsUserLocation = YES;

    }
    return _mapView;
}

- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id <MAAnnotation>)annotation {
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
    if ([annotation isKindOfClass:[MAPointAnnotation class]]) {
        static NSString *pointReuseIndentifier = @"pointReuseIndentifier";
       MAAnnotationView*annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndentifier];
        if (annotationView == nil) {
            annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:pointReuseIndentifier];
        }
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"map_b1"];
        return annotationView;
    }
    return nil;
}




@end
