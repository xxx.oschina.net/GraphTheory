//
//  YXOrderDetailsHeaderCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

NS_ASSUME_NONNULL_BEGIN
@class YXUserOrderModelList,YXEvaluationModel;
@interface YXOrderDetailsHeaderCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starView;
@property (nonatomic ,copy)void(^clickStarBlock)(CGFloat value);
// type 0 未评价 1 已评价
@property (nonatomic ,assign) NSInteger type;
@property (nonatomic ,strong) YXUserOrderModelList *model;
@property (nonatomic ,strong) YXEvaluationModel *elModel;
@end

NS_ASSUME_NONNULL_END
