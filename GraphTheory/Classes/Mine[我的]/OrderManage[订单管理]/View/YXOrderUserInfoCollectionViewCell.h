//
//  YXOrderUserInfoCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/1.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXOrderUserInfoCollectionViewCell : UICollectionViewCell

@property (nonatomic ,copy)void(^clickAddressBlock)(void);

@property (nonatomic ,strong) NSIndexPath *indexPath;

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXOrderInfoModel *model;


@end

NS_ASSUME_NONNULL_END
