//
//  YXOrderReceiptInfoTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/25.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;

@interface YXOrderReceiptInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backView;

@property (weak, nonatomic) IBOutlet UILabel *nameLab;

@property (weak, nonatomic) IBOutlet UILabel *phoneLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *stateLab;
@property (nonatomic ,strong) YXOrderInfoModel *model;

@end

NS_ASSUME_NONNULL_END
