//
//  YXAmountCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAmountCollectionViewCell.h"
#import "YXOrderInfoModel.h"

@implementation YXAmountCollectionViewCell

- (void)setType:(NSInteger)type{
    _type = type;
}

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    
    if (_type == 2) {
        _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.product_details.c_studyprice];
    }else {
        _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.product_details.sales_price];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.payBtn.layer.masksToBounds = YES;
    self.payBtn.layer.cornerRadius = 4.0;
    self.payBtn.backgroundColor = APPTintColor;
    
}
- (IBAction)payBtnAction:(UIButton *)sender {
    if (self.clickPayBtnBlock) {
        self.clickPayBtnBlock();
    }
}

@end
