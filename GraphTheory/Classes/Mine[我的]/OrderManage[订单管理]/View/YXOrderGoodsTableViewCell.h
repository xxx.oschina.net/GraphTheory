//
//  YXOrderGoodsTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXOrderGoodsTableViewCell : UITableViewCell

@property (nonatomic ,strong) YXOrderInfoModel *model;

@end

NS_ASSUME_NONNULL_END
