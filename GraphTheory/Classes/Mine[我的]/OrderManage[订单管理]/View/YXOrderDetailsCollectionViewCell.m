//
//  YXOrderDetailsCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderDetailsCollectionViewCell.h"
#import "YXOrderInfoModel.h"
#import "YXPreviewManage.h"
@implementation YXOrderDetailsCollectionViewCell

- (void)setIsPay:(NSInteger)isPay {
    _isPay = isPay;
}
- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    
    _timeLab.text = _model.create_time;
    _numberLab.text = [NSString stringWithFormat:@"单号:%@",_model.order_no];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.product_details.imgs]];
    _titleLab.text = _model.product_details.content;
    _noteLab.text = _model.remark?_model.remark:@"无";
    
    if (_isPay == 1) {
        _orderStateLab.text = @"未支付";
    }else {
        _orderStateLab.text = _model.statusTitle;
    }
    
    _nameLab.text = _model.userInfo.nike?_model.userInfo.nike:@"";


    if ([_model.c_model isEqualToString:@"1"]) {
        _typeLab.text = @"劵码模式";
        _couponCodeLab.hidden = NO;
        _couponLab.hidden = NO;
    }else {
        _typeLab.text = @"抢单模式";
        _couponCodeLab.hidden = YES;
        _couponLab.hidden = YES;
    }
    _couponCodeLab.text = _model.coupon_code;
    
    if ([_model.pay_type isEqualToString:@"balance"]) {
        _payTypeLab.text = @"余额支付";
    }else if ([_model.pay_type isEqualToString:@"wxpay"]) {
        _payTypeLab.text = @"微信支付";
    }else if ([_model.pay_type isEqualToString:@"alipay"]) {
        _payTypeLab.text = @"支付宝支付";
    }else  {
        _payTypeLab.text = @"———";
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.imgView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
    [self.imgView addGestureRecognizer:tap];
}

// 预览图片
- (void)click {

    if (_model.product_details.imgs.length > 0) {
        [[YXPreviewManage sharePreviewManage] showPhotoWithImgArr:@[_model.product_details.imgs] currentIndex:0];
    }
}

@end
