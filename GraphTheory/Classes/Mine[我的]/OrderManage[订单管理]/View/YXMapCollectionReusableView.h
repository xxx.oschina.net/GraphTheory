//
//  YXMapCollectionReusableView.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/24.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXMapCollectionReusableView : UICollectionReusableView
@property (strong, nonatomic) YXOrderInfoModel *orderModel;

@end

NS_ASSUME_NONNULL_END
