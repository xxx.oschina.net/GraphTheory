//
//  YXOrderGoodsTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/26.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderGoodsTableViewCell.h"
#import "YXUserOrderModel.h"
#import "YXOrderInfoModel.h"
@interface YXOrderGoodsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@end

@implementation YXOrderGoodsTableViewCell

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;

    [_imgView sd_setImageWithURL:[NSURL URLWithString:_model.product_details.imgs]];
    _titleLab.text = _model.product_details.name;
    _orderNoLab.text = _model.order_no;
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.product_details.sales_price];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.imgView.layer.masksToBounds = YES;
    self.imgView.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
