//
//  YXAmountCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXOrderInfoModel;
@interface YXAmountCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@property (nonatomic ,copy)void(^clickPayBtnBlock)(void);

@property (nonatomic ,assign) NSInteger type;

@property (nonatomic ,strong) YXOrderInfoModel *model;


@end

NS_ASSUME_NONNULL_END
