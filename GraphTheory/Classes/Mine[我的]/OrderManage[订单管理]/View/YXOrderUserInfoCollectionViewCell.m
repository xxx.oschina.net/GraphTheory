//
//  YXOrderUserInfoCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2020/11/1.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXOrderUserInfoCollectionViewCell.h"
#import "YXOrderInfoModel.h"
#import "YXDataTimeTool.h"
@interface YXOrderUserInfoCollectionViewCell ()

@property (nonatomic ,strong) UIView *backView;

@property (nonatomic ,strong) UIImageView *userImgView;

@property (nonatomic ,strong) UILabel *nickNameLab;

@property (nonatomic ,strong) UILabel *positionLab;

@property (nonatomic ,strong) UIButton *msgBtn;

@property (nonatomic ,strong) UIButton *phoneBtn;

@property (nonatomic ,strong) UIView *lineView;

@property (nonatomic ,strong) UIView *bottomView;

@property (nonatomic ,strong) UIImageView *icon;

@property (nonatomic ,strong) UILabel *addressLab;

@property (nonatomic ,strong) UIView *verticalView;

@property (nonatomic ,strong) UILabel *timeLab;
@property (nonatomic,assign) NSInteger secondsCountDown;
@property (nonatomic,strong) NSTimer *timer;
@end

@implementation YXOrderUserInfoCollectionViewCell

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
}

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    
    if (_indexPath.section == 0) {
        [_userImgView sd_setImageWithURL:[NSURL URLWithString:_model.userInfo.wechat_headimgurl]];
        _nickNameLab.text = _model.userInfo.nike;
        _positionLab.text = _model.product_details.name;
        _addressLab.text = _model.product_details.address.address;
        _timeLab.hidden = YES;
        if (_type == 0) {
            if ([_model.statusTitle isEqualToString:@"待支付"]) {
                _timeLab.hidden = NO;
                NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ] - 86400000 withFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:self.model.create_time deadlineStr:nowStr];
                _secondsCountDown = 60 * 15 - ss1;
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
            }else if ([_model.statusTitle isEqualToString:@"待接单"]) {
                _timeLab.hidden = NO;
                if ([_model.c_model isEqualToString:@"1"]) {
                    if ([_model.product_details.c_longtime isEqualToString:@"0"]) {
                        _timeLab.text = [NSString stringWithFormat:@"劵码有效期:不限制"];
                    }else {
                        NSInteger day = [_model.product_details.c_longtime integerValue] / (3600 *24);
                        _timeLab.text = [NSString stringWithFormat:@"劵码有效期:%ld天",day];
                    }
                }else {
                    NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ] - 86400000 withFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.pay_time integerValue]];
                    NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:time deadlineStr:nowStr];
                    _secondsCountDown = 60 * 15 - ss1;
                    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
                }
            }else if ([_model.statusTitle isEqualToString:@"已接单"]) {
                _timeLab.hidden = NO;
                NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ] - 86400000 withFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *time = [YXDataTimeTool getTimeFromTimestamp:[self.model.take_time integerValue]];
                NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:time deadlineStr:nowStr];
                _secondsCountDown =  3600 - ss1;
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
                
            }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
                _timeLab.hidden = NO;
                NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ]  withFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.finish_time integerValue]];
                NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:time deadlineStr:nowStr];
                _secondsCountDown =  (3600 * 48) - ss1;
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
            }
        }else {
            
            if ([_model.statusTitle isEqualToString:@"已接单"]) {
                _timeLab.hidden = NO;
                NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ] - 86400000 withFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *time = [YXDataTimeTool getTimeFromTimestamp:[self.model.take_time integerValue]];
                NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:time deadlineStr:nowStr];
                _secondsCountDown =  3600 - ss1;
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
                
            }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
                _timeLab.hidden = NO;
                NSString *nowStr = [YXDataTimeTool timestampToString:[[YXDataTimeTool currentTimeStr] integerValue ]  withFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.finish_time integerValue]];
                NSInteger ss1 = [YXDataTimeTool getDateDifferenceWithNowDateStr:time deadlineStr:nowStr];
                _secondsCountDown =  (3600 * 48) - ss1;
                _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runTime) userInfo:nil repeats:YES];
            }

        }
        
        
    }else {
        [_userImgView sd_setImageWithURL:[NSURL URLWithString:_model.takeUserInfo.wechat_headimgurl]];
        _nickNameLab.text = _model.takeUserInfo.nike;
        _positionLab.text = _model.takeUserInfo.show_id;
        _addressLab.text = _model.receiver.region;
        _timeLab.hidden = NO;
        NSString *time = [YXDataTimeTool getTimeFromTimestamp:[_model.take_time integerValue]];
        _timeLab.text = [NSString stringWithFormat:@"接单时间：%@",time];
    }
    
    if (self.type == 0 || self.type == 2) {
        if (_indexPath.section == 0) {
            _phoneBtn.hidden = YES;
        }else {
            _phoneBtn.hidden = NO;
        }
    }else {
        if (_indexPath.section == 0) {
            _phoneBtn.hidden = NO;
        }else {
            _phoneBtn.hidden = YES;
        }
    }
    
    
}

-(void)runTime{
    
    _secondsCountDown--;
    // 重新计算 时/分/秒
    NSString *str_day = [NSString stringWithFormat:@"%02ld", _secondsCountDown / (3600 * 24)];
    NSString *str_hour = [NSString stringWithFormat:@"%02ld", _secondsCountDown / 3600];
    NSString *str_minute = [NSString stringWithFormat:@"%02ld", (_secondsCountDown % 3600) / 60];
    NSString *str_second = [NSString stringWithFormat:@"%02ld", _secondsCountDown % 60];
    
    if (self.type == 0) {
        if ([_model.statusTitle isEqualToString:@"待支付"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@分%@秒",str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"剩余时间:%@/15分钟",format_time];
        }else if ([_model.statusTitle isEqualToString:@"待接单"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@分%@秒",str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"剩余时间:%@/15分钟",format_time];
        }else if ([_model.statusTitle isEqualToString:@"已接单"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@分%@秒",str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"剩余时间:%@/60分钟",format_time];
        }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@时%@分%@秒",str_hour,str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"%@/48小时",format_time];
        }
    }else {
        if ([_model.statusTitle isEqualToString:@"已接单"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@分%@秒",str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"剩余时间:%@/60分钟",format_time];
        }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
            NSString *format_time = [NSString stringWithFormat:@"%@时%@分%@秒",str_hour,str_minute, str_second];
            _timeLab.text=[NSString stringWithFormat:@"%@/48小时",format_time];
        }
    }
  

    // 当倒计时结束时做需要的操作: 比如活动到期不能提交
    if(_secondsCountDown <= 0) {
        if (_type == 0) {
            if ([_model.statusTitle isEqualToString:@"待接单"]&&[_model.c_model isEqualToString:@"1"]) {
                _timeLab.text=[NSString stringWithFormat:@"劵码有效期:0天"];
            }else if ([_model.statusTitle isEqualToString:@"已接单"]) {
                _timeLab.text=[NSString stringWithFormat:@"剩余时间:0分0秒/60分钟"];
            }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
                _timeLab.text=[NSString stringWithFormat:@"0时0分0秒/48小时"];
            }else {
                _timeLab.text=[NSString stringWithFormat:@"剩余时间:0分0秒/15分钟"];
            }
            
        }else {
            if ([_model.statusTitle isEqualToString:@"已接单"]) {
                _timeLab.text=[NSString stringWithFormat:@"剩余时间:0分0秒/60分钟"];
            }else if ([_model.statusTitle isEqualToString:@"待确认"]) {
                _timeLab.text=[NSString stringWithFormat:@"0时0分0秒/48小时"];
            }
        }
        [_timer invalidate];
        _timer = nil;
        return;
    }
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self setUpLayout];
    }
    return self;
}

/**
 页面布局
 */
- (void)setUpLayout{

    [self.contentView addSubview:self.backView];
    [self.backView addSubview:self.userImgView];
    [self.backView addSubview:self.nickNameLab];
    [self.backView addSubview:self.positionLab];
    [self.backView addSubview:self.phoneBtn];
    [self.backView addSubview:self.msgBtn];
    [self.backView addSubview:self.lineView];
    [self.backView addSubview:self.bottomView];
    [self.bottomView addSubview:self.icon];
    [self.bottomView addSubview:self.addressLab];
    [self.bottomView addSubview:self.verticalView];
    [self.bottomView addSubview:self.timeLab];
    
    YXWeakSelf
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.contentView);
     }];
     
     [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
         make.top.equalTo(weakSelf.backView.mas_top).offset(20);
         make.left.equalTo(weakSelf.backView.mas_left).offset(15);
         make.size.mas_equalTo(CGSizeMake(48, 48));
     }];
     
     [self.nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
         make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
         make.top.equalTo(weakSelf.userImgView.mas_top).offset(0);
         make.size.mas_equalTo(CGSizeMake(200, 20));
     }];
    
    [self.positionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
        make.bottom.equalTo(weakSelf.userImgView.mas_bottom).offset(0);
        [weakSelf.positionLab sizeToFit];
    }];
    
    [self.phoneBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.userImgView.mas_centerY);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    [self.msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.phoneBtn.mas_centerY);
        make.right.equalTo(weakSelf.phoneBtn.mas_left).offset(-15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        make.top.equalTo(weakSelf.userImgView.mas_bottom).offset(20);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        make.height.equalTo(@0.5);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.lineView.mas_bottom);
        make.left.right.bottom.equalTo(weakSelf.backView);
    }];
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.left.equalTo(weakSelf.backView.mas_left).offset(10);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    [self.addressLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.left.equalTo(weakSelf.icon.mas_right).offset(10);
        make.width.equalTo(@50);
    }];
    
//    [self.locationBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(weakSelf.bottomView.mas_left).offset(15);
//        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
//        [weakSelf.locationBtn sizeToFit];
//    }];

    [self.verticalView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.left.equalTo(weakSelf.addressLab.mas_right).offset(15);
        make.size.mas_equalTo(CGSizeMake(1, 30));
    }];

    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bottomView.mas_centerY);
        make.left.equalTo(weakSelf.verticalView.mas_right).offset(15);
        make.right.equalTo(weakSelf.backView.mas_right).offset(-15);
        [weakSelf.timeLab sizeToFit];
    }];
}


#pragma mark - Lozy Loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
        _backView.layer.masksToBounds = YES;
        _backView.layer.cornerRadius = 4.0f;
    }
    return _backView;
}

- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.layer.masksToBounds = YES;
        _userImgView.layer.cornerRadius = 24.0;
        _userImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImgView.layer.borderWidth = 1.0f;
        _userImgView.userInteractionEnabled = YES;
        _userImgView.image = [UIImage imageNamed:@"mine_head_default"];
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userImgAction)];
//        [_userImgView addGestureRecognizer:tap];
    }
    return _userImgView;
}

- (UILabel *)nickNameLab {
    if (!_nickNameLab) {
        _nickNameLab = [[UILabel alloc] init];
        _nickNameLab.text = @"会飞的鱼";
        _nickNameLab.textColor = color_TextOne;
        _nickNameLab.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    return _nickNameLab;
}

- (UILabel *)positionLab {
    if (!_positionLab) {
        _positionLab = [[UILabel alloc] init];
        _positionLab.text = @"教师";
        _positionLab.textColor = color_TextTwo;
        _positionLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _positionLab;
}

- (UIButton *)phoneBtn {
    if (!_phoneBtn) {
        _phoneBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_phoneBtn setImage:[UIImage imageNamed:@"dianhua_333333"] forState:(UIControlStateNormal)];
        [_phoneBtn addTarget:self action:@selector(phoneAction) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _phoneBtn;
}

- (UIButton *)msgBtn {
    if (!_msgBtn) {
        _msgBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_msgBtn setImage:[UIImage imageNamed:@""] forState:(UIControlStateNormal)];
    }
    return _msgBtn;
}


- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

- (UIImageView *)icon {
    if (!_icon) {
        _icon = [UIImageView new];
        _icon.image = [UIImage imageNamed:@"weizhi"];
    }
    return _icon;
}
- (UILabel *)addressLab {
    if (!_addressLab) {
        _addressLab = [UILabel new];
        _addressLab.text = @"位置";
        _addressLab.textColor = color_TextOne;
        _addressLab.font = [UIFont systemFontOfSize:14.0f];
        _addressLab.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
        [_addressLab addGestureRecognizer:tap];
    }
    return _addressLab;
}

- (UIView *)verticalView {
    if (!_verticalView) {
        _verticalView = [UIView new];
        _verticalView.backgroundColor = color_LineColor;
    }
    return _verticalView;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [UILabel new];
        _timeLab.text = @"剩余时间：";
        _timeLab.textColor = color_TextOne;
        _timeLab.font = [UIFont systemFontOfSize:14.0f];
        _timeLab.hidden = YES;
        _timeLab.textAlignment = NSTextAlignmentCenter;
    }
    return _timeLab;
}

#pragma mark - Touche Even
// 点击地址
- (void)click {
    
    if (self.clickAddressBlock) {
        self.clickAddressBlock();
    }
}

// 点击拨打电话
- (void)phoneAction {
    if (_indexPath.section == 0) {
        if (_model.receiver.userphone.length > 0) {
            NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",_model.receiver.userphone];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }else {
            [YJProgressHUD showMessage:@"暂无联系电话！"];
        }
    }else {
        if (_model.takeUserInfo.us_account.length > 0) {
            NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",_model.takeUserInfo.us_account];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }else {
            [YJProgressHUD showMessage:@"暂无联系电话！"];
        }
    }
}

@end


