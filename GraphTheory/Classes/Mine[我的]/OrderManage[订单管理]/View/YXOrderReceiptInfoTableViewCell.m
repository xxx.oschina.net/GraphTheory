//
//  YXOrderReceiptInfoTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/25.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderReceiptInfoTableViewCell.h"
#import "YXUserOrderModel.h"
#import "YXOrderInfoModel.h"
@implementation YXOrderReceiptInfoTableViewCell

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    
    _nameLab.text = _model.receiver.username;
    _phoneLab.text = _model.receiver.userphone;
    _addressLab.text = [NSString stringWithFormat:@"%@%@%@%@",_model.receiver.province,_model.receiver.city,_model.receiver.region,_model.receiver.address];
    _priceLab.text = [NSString stringWithFormat:@"¥%@",_model.sales_price];
    _timeLab.text = _model.create_time;
    _stateLab.text = _model.statusTitle;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
