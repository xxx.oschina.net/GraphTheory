//
//  YXExpressTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/12.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXExpressTableViewCell.h"
#import "YXOrderInfoModel.h"
@interface YXExpressTableViewCell ()
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextField *expressNameTF;
@property (weak, nonatomic) IBOutlet UITextField *expressCodeTF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@end

@implementation YXExpressTableViewCell

- (void)setModel:(YXOrderInfoModel *)model {
    _model = model;
    
    if ([_model.status isEqualToString:@"15"]) {
        _expressNameTF.text = _model.expressInfo.express_name;
        _expressCodeTF.text = _model.expressInfo.express_no;
        _expressNameTF.enabled = NO;
        _expressCodeTF.enabled = NO;
        _saveBtn.hidden = YES;
    }else {
        _expressNameTF.enabled = YES;
        _expressCodeTF.enabled = YES;
        _saveBtn.hidden = NO;
    }

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.saveBtn.layer.masksToBounds = YES;
    self.saveBtn.layer.cornerRadius = 4.0;
    self.saveBtn.backgroundColor = APPTintColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)nameTFChange:(UITextField *)sender {
    if (self.clickNameBlock) {
        self.clickNameBlock(sender.text);
    }
}
- (IBAction)codeTFChange:(UITextField *)sender {
    if (self.clickCodeBlock) {
        self.clickCodeBlock(sender.text);
    }
}

- (IBAction)submitBtnAction:(UIButton *)sender {
    if (self.clickSubmitBlock) {
        self.clickSubmitBlock();
    }
}

@end
