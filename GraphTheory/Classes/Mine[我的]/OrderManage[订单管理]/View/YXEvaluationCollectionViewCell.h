//
//  YXEvaluationCollectionViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class HCSStarRatingView;
@interface YXEvaluationCollectionViewCell : UICollectionViewCell
@property (nonatomic ,copy)void(^clickStarBlock)(HCSStarRatingView *starView);
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starView1;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starView2;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starView3;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starView4;
@end

NS_ASSUME_NONNULL_END
