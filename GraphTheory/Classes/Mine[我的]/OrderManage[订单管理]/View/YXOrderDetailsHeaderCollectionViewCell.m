//
//  YXOrderDetailsHeaderCollectionViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/8/5.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXOrderDetailsHeaderCollectionViewCell.h"
#import "YXOrderInfoModel.h"
#import "YXUserOrderModel.h"
#import "YXEvaluationModel.h"
@interface YXOrderDetailsHeaderCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *userImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *typeLab;

@end

@implementation YXOrderDetailsHeaderCollectionViewCell

- (void)setType:(NSInteger)type {
    _type = type;
}

- (void)setModel:(YXUserOrderModelList *)model {
    _model = model;
    [_userImgView sd_setImageWithURL:[NSURL URLWithString:_model.userInfo.wechat_headimgurl]];
    _nameLab.text = _model.userInfo.nike;
    _typeLab.text = _model.product_details.name;
}
- (void)setElModel:(YXEvaluationModel *)elModel {
    _elModel = elModel;
    _starView.value = [_elModel.total_star floatValue];
    if (self.type == 1) {
        self.starView.userInteractionEnabled = NO;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.starView.tintColor = APPTintColor;
    self.userImgView.layer.masksToBounds = YES;
    self.userImgView.layer.cornerRadius = 25.0f;
    
}

- (IBAction)starClick:(HCSStarRatingView *)sender {
    if (self.clickStarBlock) {
        self.clickStarBlock(sender.value);
    }
}

- (IBAction)phoneClick:(UIButton *)sender {
    
    if ([YXUserInfoManager getUserInfo].mobile.length > 0) {
        NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"tel:%@",[YXUserInfoManager getUserInfo].mobile];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    }else {
        [YJProgressHUD showMessage:@"暂无联系电话！"];
    }

}


@end
