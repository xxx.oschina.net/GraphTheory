//
//  YXUserOrderViewModel.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXUserOrderViewModel.h"

@implementation YXUserOrderViewModel

#pragma mark - 订单状态列表
+ (void)queryOrderStatusCompletion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/orderStatus",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  子订单列表
+ (void)querySubOrderListWithPage:(NSString *)page
                      order_field:(NSString *)order_field
                       order_type:(NSString *)order_type
                     Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/subOrderList",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:page forKey:@"page"];
    [params setValue:@"10" forKey:@"limit"];
    [params setValue:order_field forKey:@"order_field"];
    if ([order_type isEqualToString:@"1,2"]) {
        [params setValue:order_type forKey:@"order_type"];
    }else if ([order_type isEqualToString:@"3"]) {
        [params setValue:order_type forKey:@"order_type"];
    }else {
        [params setValue:order_type forKey:@"status"];
    }
    
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  大订单详情
+ (void)queryOrderInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/orderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  子订单详情
+ (void)querySubOrderInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/subOrderInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  取消子订单
+ (void)queryCancelSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/cancelSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  删除子订单
+ (void)queryDeleteSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/deleteSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  确认订单操作
+ (void)queryCompleteSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/completeSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  订单发起异议
+ (void)queryComplainSubOrderWithId:(NSString *)Id
                            content:(NSString *)content
                       Completion:(void(^)(id responesObj))completion
                            failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/complainSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:content forKey:@"content"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark -  取消订单异议
+ (void)queryCancelComplainSubOrderWithId:(NSString *)Id
                               Completion:(void(^)(id responesObj))completion
                                  failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@order/cancelComplainSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

#pragma mark -  评价订单
+ (void)queryCommentSubOrderWithId:(NSString *)Id
                              type:(NSInteger )type
                             title:(NSString *)title
                           content:(NSString *)content
                        total_star:(NSString *)total_star
                      process_star:(NSString *)process_star
                        speed_star:(NSString *)speed_star
                     attitude_star:(NSString *)attitude_star
                   efficiency_star:(NSString *)efficiency_star
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError{
    
    NSString *urlStr;
    if (type == 0) {
        urlStr = [NSString stringWithFormat:@"%@order/commentSubOrder",kBaseURL];
    }else {
        urlStr = [NSString stringWithFormat:@"%@order/subOrderComment",kBaseURL];
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [params setValue:title forKey:@"title"];
    [params setValue:content forKey:@"content"];
    [params setValue:total_star forKey:@"total_star"];
    [params setValue:process_star forKey:@"process_star"];
    [params setValue:speed_star forKey:@"speed_star"];
    [params setValue:attitude_star forKey:@"attitude_star"];
    [params setValue:efficiency_star forKey:@"efficiency_star"];

    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark -  分享订单到小纸条
+ (void)queryShareSubOrderWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@order/shareSubOrder",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:Id forKey:@"id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
    
}

@end
