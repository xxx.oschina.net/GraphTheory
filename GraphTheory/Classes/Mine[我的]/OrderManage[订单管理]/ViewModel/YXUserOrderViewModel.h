//
//  YXUserOrderViewModel.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/7/3.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXUserOrderViewModel : DDBaseViewModel

/**
 订单状态列表

 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryOrderStatusCompletion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;

/**
 子订单列表

 @param page       分页数
 @param order_field    create_time
 @param order_type      订单状态
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)querySubOrderListWithPage:(NSString *)page
                      order_field:(NSString *)order_field
                       order_type:(NSString *)order_type
                     Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;
/**
 大订单详情

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryOrderInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                     failure:(void(^)(NSError *error))conError;

/**
 子订单详情

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)querySubOrderInfoWithId:(NSString *)Id
                  Completion:(void(^)(id responesObj))completion
                        failure:(void(^)(NSError *error))conError;


/**
 取消子订单

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryCancelSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 删除子订单

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryDeleteSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 确认订单操作

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryCompleteSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 订单发起异议

 @param Id       订单id
 @param content       内容
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryComplainSubOrderWithId:(NSString *)Id
                            content:(NSString *)content
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;

/**
 取消订单异议

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryCancelComplainSubOrderWithId:(NSString *)Id
                       Completion:(void(^)(id responesObj))completion
                          failure:(void(^)(NSError *error))conError;


/**
 评价订单

 @param Id       订单id
 @param type    0:评价订单   1：获取订单评价
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryCommentSubOrderWithId:(NSString *)Id
                              type:(NSInteger )type
                             title:(NSString *)title
                           content:(NSString *)content
                        total_star:(NSString *)total_star
                      process_star:(NSString *)process_star
                        speed_star:(NSString *)speed_star
                     attitude_star:(NSString *)attitude_star
                   efficiency_star:(NSString *)efficiency_star
                        Completion:(void(^)(id responesObj))completion
                           failure:(void(^)(NSError *error))conError;



/**
 分享订单到小纸条

 @param Id       订单id
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryShareSubOrderWithId:(NSString *)Id
                      Completion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError;




@end

NS_ASSUME_NONNULL_END
