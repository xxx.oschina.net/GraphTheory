//
//  YXMessageTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXMessageTableViewCell.h"
#import "YXMessageModel.h"
@implementation YXMessageTableViewCell

- (void)setModel:(YXMessageModel *)model {
    _model = model;
    
    if (_model.is_read == 0) {
        _pointLab.backgroundColor = [UIColor redColor];
    }else {
        _pointLab.backgroundColor = [UIColor blackColor];
    }
    _titleLab.text = _model.n_title;
    _subTitleLab.text = _model.n_time;
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.pointLab];
        [self.contentView addSubview:self.rightLab];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.subTitleLab];
        [self addLayout];
    }
    return self;
}
#pragma mark - Lazy loading
- (UILabel *)pointLab {
    if (!_pointLab) {
        _pointLab = [[UILabel alloc] init];
        _pointLab.backgroundColor = [UIColor redColor];
        _pointLab.layer.masksToBounds = YES;
        _pointLab.layer.cornerRadius = 1.0f;
    }
    return _pointLab;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLab.text = @"张三";
    }
    return _titleLab;
}
- (UILabel *)subTitleLab {
    if (!_subTitleLab) {
        _subTitleLab = [[UILabel alloc] init];
        _subTitleLab.font = [UIFont systemFontOfSize:14];
        _subTitleLab.textColor = [UIColor colorWithHexString:@"#999999"];
        _subTitleLab.text = @"06-23";
    }
    return _subTitleLab;
}
- (UILabel *)rightLab {
    if (!_rightLab) {
        _rightLab = [[UILabel alloc] init];
        _rightLab.font = [UIFont systemFontOfSize:14];
        _rightLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _rightLab.text = @"查看";
    }
    return _rightLab;
}

- (void)addLayout {
    
    YXWeakSelf
    [_pointLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(@15.0);
        make.size.mas_equalTo(CGSizeMake(2, 2));
    }];
    
    [_rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        [weakSelf.rightLab sizeToFit];
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.pointLab.mas_right).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(10);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.pointLab.mas_right).offset(15);
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(5);
        [weakSelf.subTitleLab sizeToFit];
    }];
    
 
}
@end
