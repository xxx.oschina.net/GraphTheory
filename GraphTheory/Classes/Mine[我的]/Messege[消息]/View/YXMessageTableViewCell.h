//
//  YXMessageTableViewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/14.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class YXMessageModel;
@interface YXMessageTableViewCell : UITableViewCell

@property (nonatomic ,strong) YXMessageModel *model;

@property (nonatomic ,strong) UILabel *pointLab;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *subTitleLab;
@property (nonatomic ,strong) UILabel *rightLab;

@end

NS_ASSUME_NONNULL_END
