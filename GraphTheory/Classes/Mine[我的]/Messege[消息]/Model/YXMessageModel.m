//
//  YXMessageModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/3.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMessageModel.h"
#import "NSObject+Coding.h"
#import "UIViewController+Nav.h"

@implementation MessageJumpModel

@end

@implementation YXMessageModel

+(NSDictionary *)mj_objectClassInArray{
    return @{@"jumpJson":[MessageJumpModel class]};
}
+ (void)jumpZDY:(NSDictionary *)parms{
    UIViewController* rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;

    if ([parms[@"path"] length] > 0) {
        Class class = NSClassFromString(parms[@"path"]);
        UIViewController *vc = [[class alloc]init];
        NSArray *properties = [NSObject getProperties:class];
        if (vc) {
            NSDictionary *tem = parms[@"parameter"];
            for (NSString *key in tem.allKeys) {
                if ([properties containsObject:key]) {
                    [vc setValue:tem[key] forKey:key];
                }
            }
            [rootVC.myNavigationController pushViewController:vc animated:YES];
        }
    }
}
@end

@implementation YXMessageInfoModel

@end
