//
//  YXMessageModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/3.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface MessageJumpModel :NSObject

@property (nonatomic , copy) NSDictionary              * iosPath;

@end

@interface YXMessageModel : DDBaseModel

@property (nonatomic , assign) NSInteger              softwareType;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * sendTime;
@property (nonatomic , copy) NSString              * senderId;
@property (nonatomic , assign) NSInteger              readState;
@property (nonatomic , assign) NSInteger              sendSize;
@property (nonatomic , copy) NSString              * messageId;
@property (nonatomic , copy) NSString              * contactPerson;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , assign) NSInteger              isCancel;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , assign) NSInteger              readSize;
@property (nonatomic , copy) NSString              * readTime;
@property (nonatomic , copy) NSString              * relatedDatald;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * userName;
@property (nonatomic , copy) NSString              * senderName;
@property (nonatomic , copy) NSString              * content;

@property (nonatomic , strong) MessageJumpModel              * jumpJson;


+ (void)jumpZDY:(NSDictionary *)parms ;


@property (nonatomic , copy) NSString              * infoid;
@property (nonatomic , assign) NSInteger             is_read;
@property (nonatomic , copy) NSString              * n_content;
@property (nonatomic , copy) NSString              * n_time;
@property (nonatomic , copy) NSString              * n_title;
@property (nonatomic , copy) NSString              * notice_id;
@end

@interface YXMessageInfoModel : DDBaseModel

@property (nonatomic , copy) NSString              * infoid;
@property (nonatomic , copy) NSString              * n_content;
@property (nonatomic , copy) NSString              * n_time;
@property (nonatomic , copy) NSString              * n_title;
@property (nonatomic , copy) NSString              * notice_id;

@end
NS_ASSUME_NONNULL_END
