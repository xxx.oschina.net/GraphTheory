//
//  YXMessageViewModel.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/2.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXMessageViewModel.h"

@implementation YXMessageViewModel


#pragma mark - 查询用户的消息
+ (void)queryUserMessageWithMessageType:(NSString *)messageType
                              PageIndex:(NSString *)pageIndex
                               pageSize:(NSString *)pageSize
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
    
}


+ (void)queryNoticeMsgCompletion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/noticeMsg",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];

    
}

+ (void)queryNoticeMsgInfoWithNotice_id:(NSString *)notice_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@center/noticeMsgInfo",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:notice_id forKey:@"notice_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


+ (void)queryReadNoticeMsgWithNotice_id:(NSString *)notice_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@center/noticeMsg",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:notice_id forKey:@"notice_id"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


#pragma mark - 查询用户未读消息数
+ (void)queryMessageNotReadWithMessageType:(NSString *)messageType
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/1_0/user/message/queryMessageNotRead",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:messageType forKey:@"messageType"];
    [params setValue:kAppType forKey:@"appType"];
    [NetWorkTools getWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 消息一键已读
+ (void)queryUnreadMessageCompletion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/1_0/user/message/queryUnreadMessage",kBaseURL];
    [NetWorkTools getWithUrl:urlStr parameters:nil success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}

#pragma mark - 修改消息状态为已读
+ (void)queryMessageSetIsReadWithMessageId:(NSString *)messageId
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError {
    NSString *urlStr = [NSString stringWithFormat:@"%@auth/1_0/user/message/setIsRead",kBaseURL];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:messageId forKey:@"messageId"];
    [NetWorkTools postWithUrl:urlStr parameters:params success:^(id responseObj) {
        if (completion) {
            completion(responseObj);
        }
    } failure:^(NSError *error) {
        if (conError) {
            conError(error);
        }
    }];
}


@end
