//
//  YXMessageViewModel.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/2.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "DDBaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YXMessageViewModel : DDBaseViewModel


/**
 查询用户的消息

 @param messageType 消息类型 1：活动通知；2：设备通知；3：物业通知; 4: 重要通知 5:店铺通知; 6:系统通知;7：订单通知；
 @param pageIndex   页码
 @param pageSize    条数
 @param completion  返回成功
 @param conError    返回失败
 */
+ (void)queryUserMessageWithMessageType:(NSString *)messageType
                              PageIndex:(NSString *)pageIndex
                               pageSize:(NSString *)pageSize
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;


+ (void)queryNoticeMsgCompletion:(void(^)(id responesObj))completion
                         failure:(void(^)(NSError *error))conError;


+ (void)queryNoticeMsgInfoWithNotice_id:(NSString *)notice_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;

+ (void)queryReadNoticeMsgWithNotice_id:(NSString *)notice_id
                             Completion:(void(^)(id responesObj))completion
                                failure:(void(^)(NSError *error))conError;


/**
查询用户未读消息数(点生活,生意精

@param messageType 消息类型 1：活动通知；2：设备通知；3：物业通知; 4: 重要通知 5:店铺通知; 6:系统通知;7：订单通知；
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryMessageNotReadWithMessageType:(NSString *)messageType
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError;

/**
消息一键已读

@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryUnreadMessageCompletion:(void(^)(id responesObj))completion
                             failure:(void(^)(NSError *error))conError;


/**
修改消息状态为已读

@param messageId    消息Id
@param completion  返回成功
@param conError    返回失败
*/
+ (void)queryMessageSetIsReadWithMessageId:(NSString *)messageId
                                Completion:(void(^)(id responesObj))completion
                                   failure:(void(^)(NSError *error))conError;




@end

NS_ASSUME_NONNULL_END
