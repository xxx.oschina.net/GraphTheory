//
//  YXMessageViewController.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/3/23.
//  Copyright © 2020 杨旭. All rights reserved.
//
#import "YXMessageViewController.h"
#import "YXWebViewController.h"
#import "YXMessageTableViewCell.h"

#import "YXMessageViewModel.h"
#import "YXMessageModel.h"
#import "UIScrollView+DREmptyDataSet.h"
@interface YXMessageViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic ,strong) NSMutableArray *dataArr;
@end
@implementation YXMessageViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];    
    self.page = 1;
    [self requestUnRead];
}

- (void)refresh {
    YXWeakSelf;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.page = 1;
        [weakSelf requestUnRead];
    }];
    self.tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        weakSelf.page ++ ;
        [weakSelf requestUnRead];
    }];
//    [self.tableView.mj_header beginRefreshing];

}
- (void)requestUnRead{
    
    YXWeakSelf
    [YXMessageViewModel queryNoticeMsgCompletion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            weakSelf.dataArr = [YXMessageModel mj_objectArrayWithKeyValuesArray:responesObj[@"data"][@"list"]];
        }else {
            [YJProgressHUD showMessage:responesObj[@"msg"]];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        [YJProgressHUD showError:REQUESTERR];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
    }];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"消息中心";
    _page = 1;
    [self loadSubViews];
}

- (void)loadSubViews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0,*)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
        }else{
            make.bottom.left.top.right.equalTo(self.view);
        }
    }];
    
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YXMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YXMessageTableViewCell"];
    if (!cell) {
        cell = [[YXMessageTableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"YXMessageTableViewCell"];
    }
    cell.model = self.dataArr[indexPath.row];
    return cell;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    DDWeakSelf
    YXMessageModel *model = self.dataArr[indexPath.row];
    [weakSelf queryReadNoticeMsgWithNotice_id:model.notice_id];
    [YXMessageViewModel queryNoticeMsgInfoWithNotice_id:model.notice_id Completion:^(id  _Nonnull responesObj) {
        if (REQUESTDATASUCCESS) {
            YXMessageInfoModel *infoModel = [YXMessageInfoModel mj_objectWithKeyValues:responesObj[@"data"]];
            YXWebViewController *webVC = [YXWebViewController new];
            webVC.titleStr = infoModel.n_title;
            webVC.url = infoModel.n_content;
            [weakSelf.navigationController pushViewController:webVC animated:YES];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

- (void)queryReadNoticeMsgWithNotice_id:(NSString *)notice_id {
    
    [YXMessageViewModel queryReadNoticeMsgWithNotice_id:notice_id Completion:^(id  _Nonnull responesObj) {
        
    } failure:^(NSError * _Nonnull error) {
        
    }];
}

#pragma mark - Lazy Loading
- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.tableFooterView = [[UIView alloc] init];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[YXMessageTableViewCell class] forCellReuseIdentifier:@"YXMessageTableViewCell"];
        [_tableView setupEmptyDataText:@"暂无数据" tapBlock:^{
        }];
    }
    return _tableView;
}
- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}



@end
