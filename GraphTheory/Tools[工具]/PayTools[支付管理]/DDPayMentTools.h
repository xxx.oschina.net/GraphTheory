//
//  DDPayMentTools.h
//  DDLife
//
//  Created by wanggang on 2019/8/8.
//  Copyright © 2019年 点都. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WXApi.h>
NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger,DDPayResultCode){
    DDPayResultCodeSuccess,// 成功
    DDPayResultCodeFailure,// 失败
    DDPayResultCodeCancel// 取消
};

typedef void(^DDPayCompleteCallBack)(DDPayResultCode errCode,NSString *errStr);

typedef void(^DDWebPayCompleteCallBack)(void);

@class DDPayMentModel;
@interface DDPayMentTools : NSObject <WXApiDelegate>

+ (instancetype)shareDDPayMentTools;

@property (nonatomic,copy) DDWebPayCompleteCallBack webcallBack;

/**
 处理跳转url，回到应用，需要在delegate中实现
 */
- (BOOL)dd_handleUrl:(NSURL *)url;


///**
// 选择支付方式
//
// @param model 支付参数
// @param vc 当前vc
// */
//- (void)slectPayMent:(DDRequestPayMentModel *)model currentVc:(UIViewController *)vc callBack:(DDPaySelectPayMentCallBack)callBack;

/**
 支付

 @param type 支付类型
 @param payMent 支付方式
 @param dic 支付参数
 */
- (void)payType:(NSInteger)type PayMent:(DDPayMentModel *)payMent parms:(NSDictionary *)dic callBack:(DDPayCompleteCallBack)callBack;

@end

NS_ASSUME_NONNULL_END
