//
//  DDPayMentTools.m
//  DDLife
//
//  Created by wanggang on 2019/8/8.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "DDPayMentTools.h"
#import "UIViewController+KNSemiModal.h"
#import <AlipaySDK/AlipaySDK.h>
#import <WXApi.h>
#import <CommonCrypto/CommonDigest.h>
#import <WXApiObject.h>
#import "DDPayMentModel.h"
#import <WebKit/WebKit.h>
#import <WechatOpenSDK/WXApi.h>
@interface DDPayMentTools ()<WXApiDelegate,WKNavigationDelegate,WKUIDelegate>

@property (strong, nonatomic) WKWebView *webView;

@property (strong, nonatomic) UIViewController *currutVC;

@property (nonatomic,copy) DDPayCompleteCallBack callBack;

@property (nonatomic,strong) DDPayMentModel *payMent;

@end

@implementation DDPayMentTools

+ (instancetype)shareDDPayMentTools{
    static DDPayMentTools *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}
- (BOOL)dd_handleUrl:(NSURL *)url{
    
    if ([url.host isEqualToString:@"pay"]) {// 微信
        return [WXApi handleOpenURL:url delegate:self];
    }
    else if ([url.host isEqualToString:@"safepay"]) {// 支付宝
        // 支付跳转支付宝钱包进行支付，处理支付结果(在app被杀模式下，通过这个方法获取支付结果）
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSString *resultStatus = resultDic[@"resultStatus"];
            NSString *errStr = resultDic[@"memo"];
            DDPayResultCode errorCode = DDPayResultCodeSuccess;
            switch (resultStatus.integerValue) {
                case 9000:// 成功
                    errorCode = DDPayResultCodeSuccess;
                    break;
                case 6001:// 取消
                    errorCode = DDPayResultCodeCancel;
                    break;
                default:
                    errorCode = DDPayResultCodeFailure;
                    break;
            }
            if ([DDPayMentTools shareDDPayMentTools].callBack) {
                [DDPayMentTools shareDDPayMentTools].callBack(errorCode,errStr);
            }
        }];
        
        // 授权跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            // 解析 auth code
            NSString *result = resultDic[@"result"];
            NSString *authCode = nil;
            if (result.length>0) {
                NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                for (NSString *subResult in resultArr) {
                    if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                        authCode = [subResult substringFromIndex:10];
                        break;
                    }
                }
            }
            NSLog(@"授权结果 authCode = %@", authCode?:@"");
        }];
        return YES;
    }
    else if ([url.absoluteString containsString:@"shop.ddsaas.com"]){
        if (self.webcallBack) {
            self.webcallBack();
        }
        return YES;
    }else{
        return NO;
    }
}
#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp {
    // 判断支付类型
    if([resp isKindOfClass:[PayResp class]]){
        //支付回调
        DDPayResultCode errorCode = DDPayResultCodeSuccess;
        NSString *errStr = resp.errStr;
        switch (resp.errCode) {
            case 0:
                errorCode = DDPayResultCodeSuccess;
                errStr = @"订单支付成功";
                break;
            case -1:
                errorCode = DDPayResultCodeFailure;
                errStr = resp.errStr;
                break;
            case -2:
                errorCode = DDPayResultCodeCancel;
                errStr = @"您取消了支付";
                break;
            default:
                errorCode = DDPayResultCodeFailure;
                errStr = resp.errStr;
                break;
        }
        if (self.callBack) {
            self.callBack(errorCode,errStr);
        }
    }
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *resp = (SendAuthResp *)resp;
    }else {
        NSLog(@"授权失败");
    }
}
//- (void)slectPayMent:(DDRequestPayMentModel *)model currentVc:(UIViewController *)vc callBack:(nonnull DDPaySelectPayMentCallBack)callBack{
//    DDPayPickerView *pay = [DDPayPickerView new];
//    pay.model = model;
//    pay.currutVC = vc;
//    DDWeakSelf
//    self.selectPaymentCallBack = callBack;
//    pay.callBack = ^(DDPayMentModel * _Nonnull payMent, NSString * _Nonnull passWord) {
//        weakSelf.payMent = payMent;
//        if (payMent.payType.integerValue == 18) {
//            [weakSelf isPayPassWord];
//        }else{
//            if (weakSelf.selectPaymentCallBack) {
//                weakSelf.selectPaymentCallBack(payMent, @"");
//            }
//        }
//
//    };
//    _currutVC = vc;
//
//    [vc presentSemiView:pay withOptions:@{
//                                          KNSemiModalOptionKeys.pushParentBack : @(NO),
//                                          KNSemiModalOptionKeys.parentAlpha : @(0.8)
//                                          }];
//}
- (void)payType:(NSInteger)type PayMent:(DDPayMentModel *)payMent parms:(NSDictionary *)dic callBack:(DDPayCompleteCallBack)callBack{
    _callBack = callBack;
    if (type == 1) {
        [self rouseToAliPayWithBody:dic[@"data"][@"sign"]];
    }else if (type == 2) {
        [self rouseToWeiXinWithBody:dic[@"data"]];
    }
    
    
}
/** 唤醒支付宝*/
- (void)rouseToAliPayWithBody:(NSString *)aLiPayBody {
    
    [[AlipaySDK defaultService]payOrder:aLiPayBody fromScheme:@"tulunshe" callback:^(NSDictionary *resultDic) {
        NSLog(@"%@",resultDic);
        if ([resultDic[@"resultStatus"]integerValue]==4000) {
            [YJProgressHUD showMessage:@"订单支付失败"];
            self.callBack(DDPayResultCodeFailure, @"支付失败");
        }else if ([resultDic[@"resultStatus"]integerValue]==9000){
            [YJProgressHUD showMessage:@"订单支付失败"];
            self.callBack(DDPayResultCodeFailure, @"支付成功");
        }else{
            [YJProgressHUD showMessage:@"订单支付失败"];
            self.callBack(DDPayResultCodeFailure, @"支付失败");
        }
    }];
    
}
/** 唤醒微信支付*/
- (void)rouseToWeiXinWithBody:(id)responseObj {
    
    [WXApi registerApp:WX_APP_ID universalLink:@"https://syj.ddsaas.cn"];

    // 判断手机是否安装微信
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"weixin://"]]) {
        PayReq *req = [[PayReq alloc]init];
        req.openID = [NSString stringWithFormat:@"%@",responseObj[@"appid"]];
        req.partnerId = [NSString stringWithFormat:@"%@",responseObj[@"partnerid"]];
        req.prepayId = [NSString stringWithFormat:@"%@",responseObj[@"prepayid"]];
        req.nonceStr =[NSString stringWithFormat:@"%@",responseObj[@"noncestr"]];
        req.package =[NSString stringWithFormat:@"%@",responseObj[@"package"]];
        req.timeStamp = [responseObj[@"timestamp"]intValue];
        req.sign = [NSString stringWithFormat:@"%@",responseObj[@"sign"]];
//        [WXApi sendReq:req];
        [WXApi sendReq:req completion:^(BOOL success) {
            
        }];
        
    }else {
        [YJProgressHUD showMessage:@"您未安装微信客户端,不能进行支付~"];
    }
    
}

/** 唤醒微信小程序支付*/
- (void)rouseToWeiXinLaunchWithBody:(id)responseObj {
    
    [WXApi registerApp:WX_APP_ID universalLink:@"https://syj.ddsaas.cn"];

    if ([WXApi isWXAppInstalled]) {
        WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
        launchMiniProgramReq.userName = @"gh_ebeae7738a6e";
        launchMiniProgramReq.path = [NSString stringWithFormat:@"pages/apppay/apppay?datas=%@",[responseObj mj_JSONString]];    ////拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
        launchMiniProgramReq.miniProgramType = WXMiniProgramTypeRelease; //拉起小程序的类型
        [WXApi sendReq:launchMiniProgramReq completion:nil];
        
    }else {
        KPOP(@"您还未安装微信~");
    }
    
}


#pragma mark   ============== WKWebview相关 回调及加载 ==============
- (void)webView:(WKWebView*)webView decidePolicyForNavigationAction:(WKNavigationAction*)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    if ([[navigationAction.request.URL absoluteString] rangeOfString:@"alipays"].length != 0) {
        BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:navigationAction.request.URL];
        //先判断是否能打开该url
        if (canOpen)
        {   //打开支付宝
            [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
        }else {
            KPOP(@"您的设备未安装支付宝");
        }
        decisionHandler(WKNavigationActionPolicyCancel);
        
        return;
        
    }
    decisionHandler(WKNavigationActionPolicyAllow);
    
}


- (void)loadWithUrlStr:(NSString*)urlStr
{
    if (urlStr.length > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURLRequest *webRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]
                                                        cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                    timeoutInterval:30];
            [self.webView loadRequest:webRequest];
        });
    }
}
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectZero];
        _webView.navigationDelegate = self;
    }
    return _webView;
}

@end
