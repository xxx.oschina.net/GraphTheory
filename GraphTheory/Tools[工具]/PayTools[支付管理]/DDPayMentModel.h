//
//  DDPayMentModel.h
//  DDLife
//
//  Created by wanggang on 2019/8/8.
//  Copyright © 2019年 点都. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DDPayMentModel : DDBaseModel

/** 主键id*/
@property (nonatomic ,copy) NSString *Id;
/** 支付名称*/
@property (nonatomic ,copy) NSString *text;
/** 支付类型*/
@property (nonatomic ,copy) NSString *pay_type;
/** 支付密码*/
@property (nonatomic ,copy) NSString *pay_pwd;
/** 是否选中*/
@property (nonatomic ,assign) BOOL selected;

@end


@interface DDRequestPayMentModel : NSObject

@property (strong, nonatomic) NSString *houserID;
@property (strong, nonatomic) NSString *wycompanyId;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *money;
@property (strong, nonatomic) NSString *title;


@end



NS_ASSUME_NONNULL_END
