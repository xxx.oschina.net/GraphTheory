//
//  YXCustomAlertActionView.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/13.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXCustomAlertActionView.h"
#import <IQTextView.h>
#import "UITextField+Extension.h"
#import <WebKit/WebKit.h>

// 定义alertView 宽
#define AlertW  300

// 定义各个栏目之间的距离
#define Spce 10.0

// 定义提示弹框的时间
#define k_AlertTypeNotButtonTime 1.8

#define SCREEN_WIDTH  (self.frame.size.width)
#define SCREEN_HEIGHT  (self.frame.size.height)

#define ColorHUI [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0]
@interface YXCustomAlertActionView ()<UITextFieldDelegate,WKNavigationDelegate,WKUIDelegate>


/** 弹窗类型*/
@property (nonatomic ,assign) AlertViewType type;
/** 弹窗视图*/
@property (nonatomic ,strong) UIView *alertView;
/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 消息*/
@property (nonatomic ,strong) UILabel *msgLab;
/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *textField;
/** 输入文本*/
@property (nonatomic ,strong) IQTextView *textView;
/** 不限制按钮*/
@property (nonatomic ,strong) UIButton *noLimitBtn;
/** 限制按钮*/
@property (nonatomic ,strong) UIButton *limitBtn;
/** 横线*/
@property (nonatomic ,strong) UIView *lineView;
/** 竖线*/
@property (nonatomic ,strong) UIView *verLineView;
/** 取消按钮*/
@property (nonatomic ,strong) UIButton *cancleBtn;
/** 确认*/
@property (nonatomic ,strong) NSString *sureTitle;
/** 取消*/
@property (nonatomic ,strong) NSString *cancleTitle;
/** 关闭按钮*/
@property (nonatomic ,strong) UIButton *closeBtn;
/** 选中按钮*/
@property (nonatomic ,strong) UIButton *selectBtn;
/** 单位*/
@property (nonatomic ,strong) UILabel *unitLab;


@property(strong,nonatomic) WKWebView *webView;

@end

@implementation YXCustomAlertActionView


- (void)setSureStr:(NSString *)sureStr {
    _sureStr = sureStr;
    [self.sureBtn setTitle:_sureStr forState:(UIControlStateNormal)];
}

- (instancetype)initWithFrame:(CGRect)frame ViewType:(AlertViewType)type Title:(NSString *)title Message:(NSString *)message sureBtn:(NSString *)sureTitle cancleBtn:(NSString *)cancleTitle ;
{
    self = [super initWithFrame:frame];
    if (self) {
        self.sureTitle = sureTitle;
        self.cancleTitle = cancleTitle;
        switch (type) {
            case AlertText:
            {
                
                if (message.length >= 12) {
                    self.alertView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, AlertW, 160)];
                    self.alertView.backgroundColor = [UIColor whiteColor];
                    self.alertView.layer.cornerRadius=10.0;
                    self.alertView.layer.masksToBounds=YES;
                    self.alertView.userInteractionEnabled=YES;
                    [self addSubview:self.alertView];
                    
                    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                    titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                    titleLab.text = title;
                    titleLab.font = [UIFont systemFontOfSize:16];
                    titleLab.textAlignment = NSTextAlignmentCenter;
                    [self.alertView addSubview:titleLab];
                    self.titleLab = titleLab;
                    
                    UILabel *messageLab = [[UILabel alloc]  initWithFrame:(CGRectMake(15, CGRectGetMaxY(titleLab.frame) + 20, self.alertView.frame.size.width - 30, 40))];
                    messageLab.text = message;
                    messageLab.textAlignment = NSTextAlignmentCenter;
                    messageLab.font = [UIFont systemFontOfSize:15.0];
                    messageLab.textColor = color_TextTwo;
                    messageLab.numberOfLines = 0;
                    [self.alertView addSubview:messageLab];
                    self.msgLab = messageLab;
                    
                    self.lineView = [[UIView alloc] init];
                    self.lineView.frame = self.msgLab?CGRectMake(0, CGRectGetMaxY(self.msgLab.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                    self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                    [self.alertView addSubview:self.lineView];
                }else {
                    self.alertView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, AlertW, 140)];
                    self.alertView.backgroundColor = [UIColor whiteColor];
                    self.alertView.layer.cornerRadius=10.0;
                    self.alertView.layer.masksToBounds=YES;
                    self.alertView.userInteractionEnabled=YES;
                    [self addSubview:self.alertView];
                    
                    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                    titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                    titleLab.text = title;
                    titleLab.font = [UIFont systemFontOfSize:16];
                    titleLab.textAlignment = NSTextAlignmentCenter;
                    [self.alertView addSubview:titleLab];
                    self.titleLab = titleLab;
                    
                    UILabel *messageLab = [[UILabel alloc]  initWithFrame:(CGRectMake(15, CGRectGetMaxY(titleLab.frame) + 20, self.alertView.frame.size.width - 30, 20))];
                    messageLab.text = message;
                    messageLab.textAlignment = NSTextAlignmentCenter;
                    messageLab.font = [UIFont systemFontOfSize:15.0];
                    messageLab.textColor = color_TextTwo;
                    messageLab.numberOfLines = 0;
                    [self.alertView addSubview:messageLab];
                    self.msgLab = messageLab;
                    
                    self.lineView = [[UIView alloc] init];
                    self.lineView.frame = self.msgLab?CGRectMake(0, CGRectGetMaxY(self.msgLab.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                    self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                    [self.alertView addSubview:self.lineView];
                }

                
                [self addBtnUI];
                
            }
                break;
            case AlertTextField:
            {
            
                self.alertView = [[UIView alloc]initWithFrame:CGRectMake((KWIDTH -AlertW)/2, (self.frame.size.width-200)/2, AlertW, 150)];
                self.alertView.backgroundColor = [UIColor whiteColor];
                self.alertView.layer.cornerRadius=10.0;
                self.alertView.layer.masksToBounds=YES;
                self.alertView.userInteractionEnabled=YES;
                [self addSubview:self.alertView];
                
                [self addObservers];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                titleLab.text = title;
                titleLab.font = [UIFont systemFontOfSize:16];
                titleLab.textAlignment = NSTextAlignmentCenter;
                [self.alertView addSubview:titleLab];
                self.titleLab = titleLab;
                
                LTLimitTextField *textField = [[LTLimitTextField alloc] initWithFrame:(CGRectMake(20, CGRectGetMaxY(titleLab.frame) + 20, self.alertView.width - 40, 30))];
                textField.placeholder = message;
//                textField.text = @"";
                textField.borderStyle = UITextBorderStyleRoundedRect;
                textField.font = [UIFont systemFontOfSize:15.0];
                textField.tintColor = APPTintColor;
                if ([title isEqualToString:@"增加库存"]) {
                    textField.keyboardType = UIKeyboardTypeNumberPad;
                    textField.textLimitType = LTTextLimitTypeLength;
                    textField.textLimitSize = 5;
                    [textField setFirstLetterWithIsZero:YES];
                }else if ([title isEqualToString:@"拒绝原因"]||[title isEqualToString:@"新增快捷回复"]) {
                    textField.textLimitType = LTTextLimitTypeLength;
                    textField.textLimitSize = 60;
                }
//                textField.keyboardType = UIKeyboardTypeDecimalPad;
                [textField becomeFirstResponder];
                [self.alertView addSubview:textField];
                self.textField = textField;

                self.lineView = [[UIView alloc] init];
                self.lineView.frame = self.textField?CGRectMake(0, CGRectGetMaxY(self.textField.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                [self.alertView addSubview:self.lineView];
                
                [self addBtnUI];

            }
                break;
            case AlertTextView:
            {
                self.alertView = [[UIView alloc]initWithFrame:CGRectMake((KWIDTH -AlertW)/2, (self.frame.size.width-200)/2, AlertW, 200)];
                //        self.alertView.mj_centerX = SCREEN_WIDTH/2. + 35*SCALE;
                self.alertView.backgroundColor = [UIColor whiteColor];
                self.alertView.layer.cornerRadius=4.0;
                self.alertView.layer.masksToBounds=YES;
                self.alertView.userInteractionEnabled=YES;
                [self addSubview:self.alertView];
                
                [self addObservers];

                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                titleLab.text = title;
                titleLab.font = [UIFont systemFontOfSize:16];
                titleLab.textAlignment = NSTextAlignmentCenter;
                [self.alertView addSubview:titleLab];
                self.titleLab = titleLab;

                
                self.textView = [[IQTextView alloc] initWithFrame:(CGRectMake(20, CGRectGetMaxY(titleLab.frame) + 20, self.alertView.width - 40, 80))];
                self.textView.placeholder = message;
                self.textView.font = [UIFont systemFontOfSize:14.0f];
                self.textView.layer.borderColor = color_BorderColor.CGColor;
                self.textView.layer.cornerRadius=4.0;
                self.textView.layer.masksToBounds=YES;
                self.textView.layer.borderWidth=1.0f;
                [self.textView becomeFirstResponder];
                [self.alertView addSubview:self.textView];
            
                self.lineView = [[UIView alloc] init];
                self.lineView.frame = self.textView?CGRectMake(0, CGRectGetMaxY(self.textView.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                [self.alertView addSubview:self.lineView];
                
                
                [self addBtnUI];

            }
                break;
                
            case AlertBatchTextField:
            {
                
                self.alertView = [[UIView alloc]initWithFrame:CGRectMake((KWIDTH -AlertW)/2, (self.frame.size.width-184)/2, AlertW, 184)];
                self.alertView.backgroundColor = [UIColor whiteColor];
                self.alertView.layer.cornerRadius=10.0;
                self.alertView.layer.masksToBounds=YES;
                self.alertView.userInteractionEnabled=YES;
                [self addSubview:self.alertView];
                
                [self addObservers];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                titleLab.text = title;
                titleLab.font = [UIFont systemFontOfSize:16];
                titleLab.textAlignment = NSTextAlignmentCenter;
                [self.alertView addSubview:titleLab];
                self.titleLab = titleLab;
                
                
                UIButton *discountBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
                discountBtn.frame = CGRectMake(10, CGRectGetMaxY(titleLab.frame) + 10, 80, 30);
                [discountBtn setTitle:@"打折" forState:(UIControlStateNormal)];
                [discountBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
                [discountBtn setImage:[UIImage imageNamed:@"advert_unSelect"] forState:(UIControlStateNormal)];
                [discountBtn setImage:[UIImage imageNamed:@"advert_Select"] forState:(UIControlStateSelected)];
                discountBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
                discountBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
                discountBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                discountBtn.tag = 1000;
                discountBtn.selected = YES;
                [discountBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                [self.alertView addSubview:discountBtn];
                self.selectBtn = discountBtn;

                
                UIButton *fullReductionBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
                fullReductionBtn.frame = CGRectMake(CGRectGetMaxX(discountBtn.frame) + 15, CGRectGetMaxY(titleLab.frame) + 10, 80, 30);
                [fullReductionBtn setTitle:@"减价" forState:(UIControlStateNormal)];
                [fullReductionBtn setTitleColor:color_TextThree forState:(UIControlStateNormal)];
                [fullReductionBtn setImage:[UIImage imageNamed:@"advert_unSelect"] forState:(UIControlStateNormal)];
                [fullReductionBtn setImage:[UIImage imageNamed:@"advert_Select"] forState:(UIControlStateSelected)];
                fullReductionBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
                fullReductionBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 5);
                fullReductionBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                fullReductionBtn.tag = 1001;
                [fullReductionBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                [self.alertView addSubview:fullReductionBtn];
                
                UIView *contentView = [[UIView alloc] initWithFrame:(CGRectMake(20, CGRectGetMaxY(discountBtn.frame) + 10, self.alertView.width - 40, 35))];
                contentView.backgroundColor = [UIColor whiteColor];
                contentView.layer.masksToBounds = YES;
                contentView.layer.cornerRadius = 4.0f;
                contentView.layer.borderColor = color_LineColor.CGColor;
                contentView.layer.borderWidth = 1.0f;
                [self.alertView addSubview:contentView];
                
                LTLimitTextField *textField = [[LTLimitTextField alloc] initWithFrame:(CGRectMake(15, 0, contentView.width - 80, 35))];
                textField.placeholder = @"请输入";
                textField.borderStyle = UITextBorderStyleNone;
                textField.font = [UIFont systemFontOfSize:15.0];
                textField.tintColor = APPTintColor;
                textField.keyboardType = UIKeyboardTypeDecimalPad;
                [textField becomeFirstResponder];
                textField.delegate = self;
                [contentView addSubview:textField];
                self.textField = textField;

                UILabel *unitLab = [[UILabel alloc] initWithFrame:(CGRectMake(CGRectGetMaxX(textField.frame), 0, 65, 35))];
                unitLab.textAlignment = NSTextAlignmentCenter;
                unitLab.textColor = color_TextTwo;
                unitLab.text = @"折";
                unitLab.font = [UIFont systemFontOfSize:14.0f];
                [contentView addSubview:unitLab];
                self.unitLab = unitLab;
                
                self.lineView = [[UIView alloc] init];
                self.lineView.frame = contentView?CGRectMake(0, CGRectGetMaxY(contentView.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                [self.alertView addSubview:self.lineView];
                
                [self addBtnUI];
        
                
            }
                break;
                
                
            case AlertTextLimit:
            {
                self.alertView = [[UIView alloc]initWithFrame:CGRectMake((KWIDTH -AlertW)/2, (self.frame.size.width-200)/2, AlertW, 150)];
                self.alertView.backgroundColor = [UIColor whiteColor];
                self.alertView.layer.cornerRadius=10.0;
                self.alertView.layer.masksToBounds=YES;
                self.alertView.userInteractionEnabled=YES;
                [self addSubview:self.alertView];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                 titleLab.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
                 titleLab.text = title;
                 titleLab.font = [UIFont systemFontOfSize:16];
                 titleLab.textAlignment = NSTextAlignmentCenter;
                 [self.alertView addSubview:titleLab];
                 self.titleLab = titleLab;
                 
                
                UIButton *discountBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
                discountBtn.frame = CGRectMake(30, CGRectGetMaxY(titleLab.frame) + 20, 105, 30);
                [discountBtn setTitle:@"不限制" forState:(UIControlStateNormal)];
                [discountBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
                [discountBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateSelected)];
                discountBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                discountBtn.backgroundColor = APPTintColor;
                discountBtn.layer.masksToBounds = YES;
                discountBtn.layer.cornerRadius = 4.0f;
                discountBtn.layer.borderColor = color_LineColor.CGColor;
                discountBtn.layer.borderWidth = 1.0f;
                discountBtn.tag = 1000;
                discountBtn.selected = YES;
                [discountBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                [self.alertView addSubview:discountBtn];
                self.noLimitBtn = discountBtn;
                self.selectBtn = self.noLimitBtn;
                
                UIButton *fullReductionBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
                fullReductionBtn.frame = CGRectMake(CGRectGetMaxX(discountBtn.frame) + 10, CGRectGetMaxY(titleLab.frame) + 20, 105, 30);
                [fullReductionBtn setTitle:@"开启限制" forState:(UIControlStateNormal)];
                [fullReductionBtn setTitleColor:color_TextOne forState:(UIControlStateNormal)];
                [fullReductionBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateSelected)];
                fullReductionBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                fullReductionBtn.layer.masksToBounds = YES;
                fullReductionBtn.layer.cornerRadius = 4.0f;
                fullReductionBtn.layer.borderColor = color_LineColor.CGColor;
                fullReductionBtn.layer.borderWidth = 1.0f;
                fullReductionBtn.tag = 1001;
                [fullReductionBtn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
                [self.alertView addSubview:fullReductionBtn];
                self.limitBtn = fullReductionBtn;
                
                
                self.lineView = [[UIView alloc] init];
                self.lineView.frame = discountBtn?CGRectMake(0, CGRectGetMaxY(discountBtn.frame)+2*Spce, AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame)+2*Spce, AlertW, 1);
                self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                [self.alertView addSubview:self.lineView];
                 
                
                [self addBtnUI];

            }
                break;
            case AlertTextWeb:
            {
                self.alertView = [[UIView alloc]initWithFrame:CGRectMake((KWIDTH -AlertW)/2, (self.frame.size.width-200)/2, AlertW, 400)];
                self.alertView.backgroundColor = [UIColor whiteColor];
                self.alertView.layer.cornerRadius=10.0;
                self.alertView.layer.masksToBounds=YES;
                self.alertView.userInteractionEnabled=YES;
                [self addSubview:self.alertView];
                
                UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.alertView.frame.size.width, 40)];
                 titleLab.backgroundColor = APPTintColor;
                 titleLab.text = title;
                titleLab.textColor = [UIColor whiteColor];
                 titleLab.font = [UIFont systemFontOfSize:16];
                 titleLab.textAlignment = NSTextAlignmentCenter;
                 [self.alertView addSubview:titleLab];
                 self.titleLab = titleLab;
                 
                UIView *contentView = [[UIView alloc] initWithFrame:(CGRectMake(0, CGRectGetMaxY(titleLab.frame), self.alertView.frame.size.width, 320))];
                contentView.backgroundColor = [UIColor whiteColor];
                [self.alertView addSubview:contentView];
                
                
                
                //js脚本
                NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
                //注入
                WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES]; WKUserContentController *wkUController = [[WKUserContentController alloc] init];
                [wkUController addUserScript:wkUScript];
                //配置对象
                WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
                wkWebConfig.userContentController = wkUController;
                // 创建设置对象
                WKPreferences *preference = [[WKPreferences alloc]init];
                // 设置字体大小(最小的字体大小)
                preference.minimumFontSize = 14;
                // 设置偏好设置对象
                wkWebConfig.preferences = preference;
                
                _webView = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0,self.alertView.frame.size.width , 320) configuration:wkWebConfig];
                _webView.scrollView.showsVerticalScrollIndicator = NO;
                _webView.scrollView.showsHorizontalScrollIndicator = NO;
                _webView.backgroundColor = [UIColor whiteColor];
                _webView.UIDelegate = self;
                _webView.navigationDelegate = self;
            
                NSString *url = @"图论社非常重视对用户（以下简称“您”或“用户”）隐私的保护，为了给您提供更准确、更有个性化的服务，请您仔细阅读如下声明。当您访问图论社网站或使用图论社提供的服务前，您需要同意隐私政策中具体个人信息的收集、使用、公布和以其它形式使用和披露您的个人信息等内容。但图论社会以高度的勤勉、审慎义务对待您的个人信息。除本隐私权政策另有规定外，在未征得您事先许可的情况下，图论社不会将这些信息对外披露或向第三方提供。如果您不同意隐私政策中的任何内容，请立即停止使用或访问图论社。本协议为图论社用户许可协议不可分割的组成部分。<br \/>\r\n一、图论社隐私政策的适用范围<br \/>\r\n1.1适用范围<br \/>\r\n1.1.1在您注册图论社时，您根据图论社要求提供的个人注册信息，包括但不限于姓名、地址、联系方式等（图论社应法律法规要求需公示的企业名称及相关工商注册信息除外）；<br \/>\r\n1.1.2在您使用图论社网络服务，或访问图论社平台网页时，图论社自动接收并记录您的浏览器和计算机上的信息，包括但不限于您的IP地址、浏览器的类型、使用的语言、访问日期和时间、软硬件特征信息及您需求的网页记录等数据；<br \/>\r\n1.1.3图论社通过合法途径从商业伙伴处取得的用户个人数据。<br \/>\r\n1.2您了解并同意，以下信息不适用本隐私权政策：<br \/>\r\n1.2.1您在使用图论社平台提供的搜索服务时输入的关键字信息；<br \/>\r\n1.2.2图论社收集到的您在图论社发布的有关信息数据，包括但不限于成交信息及评价详情；<br \/>\r\n1.2.3违反法律规定或违反图论社规则行为及图论社已对您采取的措施。<br \/>\r\n二、图论社收集信息的使用<br \/>\r\n2.1图论社利用从所有服务中收集的信息来提供、维护、保护和改进这些服务，同时开发新的服务为您带来更好的用户体验，并提高图论社的总体服务品质。图论社还会向您提供您感兴趣的信息，包括但不限于向您发出产品和服务信息，或者经您的事先同意，与图论社合作伙伴共享信息以便他们向您发送有关其产品和服务的信息。<br \/>\r\n2.2图论社不会向任何无关第三方（即不包括图论社的关联公司或合作公司）提供、出售、出租、分享或交易您的个人信息，除非事先得到您的许可，或该无关第三方和图论社（含图论社关联公司）单独或共同为您提供服务时，在该服务结束后，其将被禁止访问包括其以前能够访问的所有这些资料。<br \/>\r\n2.3图论社亦不允许任何第三方以任何手段收集、编辑、出售或者无偿传播您的个人信息。任何图论社平台用户如从事上述活动，一经发现，图论社有权立即终止与该用户的服务协议。<br \/>\r\n三、您完全理解并不可撤销地、免费地授予图论社及其关联公司下列权利<br \/>\r\n3.1图论社关联公司或合作公司允许图论社用户登录关联公司或合作公司并使用其服务，图论社用户在关联公司或合作公司的任何行为均需遵守该等平台服务协议的约定、平台公布的规则以及有关正确使用平台服务的说明和操作指引。为了实现上述功能，您同意图论社将您在图论社的注册信息、交易\/支付数据等信息和数据同步至关联公司或合作公司系统并允许其使用。<br \/>\r\n3.2如您以图论社关联公司或合作公司用户账号和密码登录图论社，为了实现向您提供同等服务的功能，您同意图论社将您在关联公司或合作公司账号项下的注册信息、交易\/支付数据等信息和数据同步至图论社系统并进行使用，并且您不会因此追究图论社以及图论社关联公司或合作公司的责任。<br \/>\r\n3.3为确保交易安全，允许图论社及其关联公司对用户信息进行数据分析，并允许图论社及其关联公司对上述分析结果进行商业利用。<br \/>\r\n3.4您在享受图论社提供的各项服务的同时，授权并同意接受图论社向您的电子邮件、手机、通信地址等发送商业信息，包括不限于最新的图论社产品信息、促销信息等。若您选择不接受图论社提供的各类信息服务，您可以按照图论社网提供的相应设置拒绝该类信息服务。<br \/>\r\n3.5用户在如下情况下，图论社会遵照您的意愿或法律的规定披露您的个人信息，由此引发的问题将由您个人承担：<br \/>\r\n3.5.1事先获得您的同意向第三方披露；<br \/>\r\n3.5.2为提供您所要求的产品和服务，而必须与第三方分享您的个人信息；<br \/>\r\n3.5.3根据有关的法律法规的规定，或者依据行政机关或司法机关的要求，向第三方或者行政、司法机构披露；<br \/>\r\n3.5.4如您是适格的知识产权投诉人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；<br \/>\r\n3.5.5如您出现违反中国有关法律、法规或者图论社服务协议或相关规则的情况，需要向第三方披露；<br \/>\r\n3.5.6在图论社上创建的某一交易中，如交易任何一方履行或部分履行了交易义务并提出信息披露请求的，图论社有权决定向该用户提供其交易对方的联络方式等必要信息，以促成交易的完成或纠纷的解决；<br \/>\r\n3.5.7其他图论社根据法律、法规或者图论社政策认为合适的披露。<br \/>\r\n四、信息存储和交换<br \/>\r\n4.1图论社收集的有关您的信息和资料将保存在图论社及（或）其关联公司的服务器上，这些信息和资料可能传送至您所在国家、地区或图论社收集信息和资料所在地的境外并在境外被访问、存储和展示。<br \/>\r\n五、Cookies的使用<br \/>\r\n5.1在您未拒绝接受cookies的情况下，图论社会在您的计算机上设定或取用cookies，以便您能登录或使用依赖于cookies的图论社服务或功能。图论社使用cookies可为您提供更加周到的个性化服务，包括推广服务。<br \/>\r\n5.2您有权选择接受或拒绝接受cookies。您可以通过修改浏览器设置的方式拒绝接受cookies。但如果您选择拒绝接受cookies，则您可能无法登录或使用依赖于cookies的图论社服务或功能。<br \/>\r\n5.3通过图论社所设cookies所取得的有关信息，将适用本政策。<br \/>\r\n六、图论社对信息的维护与保护<br \/>\r\n6.1图论社希望用户放心的使用图论社的产品和服务，为此图论社设置了安全程序保护您的信息不会被未经授权的人访问、窃取。但也请您妥善保管您的账户信息。图论社将通过向其它服务器备份、对用户密码进行加密等安全措施确保您的信息不丢失，不被滥用和变造。尽管有前述安全措施，但同时也请您注意在信息网络上不存在绝对完善的安全措施。<br \/>\r\n6.2在使用图论社服务进行网上交易时，您不可避免的要向交易对方或潜在的交易对方披露自己的个人信息，如联络方式或者邮政地址。请您妥善保护自己的个人信息，仅在必要的情形下向他人提供。如您发现自己的个人信息泄密，请您立即联络图论社客服，以便图论社采取相应措施。<br \/>\r\n七、合作伙伴<br \/>\r\n7.1图论社选择有信誉的第三方公司或网站作为图论社的合作伙伴为用户提供信息和服务，尽管图论社只选择有信誉的公司或网站作为图论社的合作伙伴，但是每个合作伙伴都会有与图论社不同的隐私条款，一旦您通过点击进入了合作伙伴的网站，图论社的隐私条款将不再生效，图论社建议您查看该合作伙伴网站的隐私条款，并了解该合作伙伴对于收集、使用、披露您的个人信息的规定。<br \/>\r\n八、关于隐私条款的变更<br \/>\r\n8.1图论社将根据法律、法规或政策的变更和修改，或图论社网站内容的变化和技术的更新，或其他图论社认为合理的原因，对本隐私政策进行修改并以网站公告、用户通知等合适的形式告知用户，若您不接受修订后的条款的，应立即停止使用本服务，若您继续使用本服务的，视为接受修订后的所有条款。<br \/>";
                
                    url = [NSString stringWithFormat:@"<div style=\"line-height: 26px\">%@",url];
                    [self.webView loadHTMLString:url baseURL:nil];
                [contentView addSubview:self.webView];
            
            
                
                self.lineView = [[UIView alloc] init];
                self.lineView.frame = contentView?CGRectMake(0, CGRectGetMaxY(contentView.frame), AlertW, 1):CGRectMake(0, CGRectGetMaxY(self.titleLab.frame), AlertW, 1);
                self.lineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
                [self.alertView addSubview:self.lineView];
                 
                
                [self addBtnUI];

            }
                break;
                
                
            default:
                break;
        }
    

        
        self.type = type;
        
    }
    return self;
}



- (void)addBtnUI {
    
    
    //两个按钮
    if (self.cancleTitle && self.sureTitle) {
        
        self.sureBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        self.sureBtn.frame = CGRectMake(0, CGRectGetMaxY(self.lineView.frame), (AlertW-1)/2, 40);
        [self.sureBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateNormal];
        [self.sureBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateSelected];
        [self.sureBtn setTitle:self.sureTitle forState:UIControlStateNormal];
        [self.sureBtn setTitleColor:APPTintColor forState:UIControlStateNormal];
        self.sureBtn.tag = 1;
        [self.sureBtn addTarget:self action:@selector(sureClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.sureBtn.bounds byRoundingCorners:UIRectCornerBottomLeft cornerRadii:CGSizeMake(5.0, 5.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.sureBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        self.sureBtn.layer.mask = maskLayer;
        
        [self.alertView addSubview:self.sureBtn];
    }
    
    if (self.cancleTitle && self.sureTitle) {
        self.verLineView = [[UIView alloc] init];
        self.verLineView.frame = CGRectMake(CGRectGetMaxX(self.sureBtn.frame), CGRectGetMaxY(self.lineView.frame), 1, 40);
        self.verLineView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.6];
        [self.alertView addSubview:self.verLineView];
    }
    
    if(self.sureTitle && self.cancleTitle){
        
        self.cancleBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        self.cancleBtn.frame = CGRectMake(CGRectGetMaxX(self.verLineView.frame), CGRectGetMaxY(self.lineView.frame), (AlertW-1)/2+1, 40);
        [self.cancleBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateNormal];
        [self.cancleBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateSelected];
        [self.cancleBtn setTitle:self.cancleTitle forState:UIControlStateNormal];
        [self.cancleBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.cancleBtn.tag = 2;
        [self.cancleBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.cancleBtn.bounds byRoundingCorners:UIRectCornerBottomRight cornerRadii:CGSizeMake(5.0, 5.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.cancleBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        self.cancleBtn.layer.mask = maskLayer;
        
        [self.alertView addSubview:self.cancleBtn];
        
    }
    
    //只有确定按钮
    if(self.sureTitle && !self.cancleTitle){
        
        self.sureBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        self.sureBtn.frame = CGRectMake(0, CGRectGetMaxY(self.lineView.frame), AlertW, 40);
        [self.sureBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateNormal];
        [self.sureBtn setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.2]] forState:UIControlStateSelected];
        [self.sureBtn setTitle:self.sureTitle forState:UIControlStateNormal];
        [self.sureBtn setTitleColor:APPTintColor forState:UIControlStateNormal];
        self.sureBtn.tag = 2;
        [self.sureBtn addTarget:self action:@selector(sureClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.sureBtn.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:CGSizeMake(5.0, 5.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.sureBtn.bounds;
        maskLayer.path = maskPath.CGPath;
        self.sureBtn.layer.mask = maskLayer;
        
        [self.alertView addSubview:self.sureBtn];
        
    }
    
    
}


#pragma mark - WKWebView Delegate
-(void)webView:(WKWebView*)webView didStartProvisionalNavigation:(WKNavigation*)navigation{
}
-(void)webView:(WKWebView*)webView didFinishNavigation:(WKNavigation*)navigation{
    //禁止用户选择
//    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:nil];
//    [webView evaluateJavaScript:@"document.activeElement.blur();" completionHandler:nil];
//    // 适当增大字体大小
//    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '300%'" completionHandler:nil];
    //修改字体颜色
    [webView evaluateJavaScript:@"document.getElementsByTagName('body')[0].style.webkitTextFillColor= '#444444'"completionHandler:nil];
}
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
}


#pragma mark - Pravite Method
- (void)addObservers{
    
    //监听当键盘将要出现时
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //监听当键将要退出时
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

#pragma mark - Event response
- (void)keyboardWillShow:(NSNotification *)notif{
    
    //获取键盘的高度
    NSDictionary *userInfo = [notif userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    CGFloat height = keyboardRect.size.height;
    CGFloat bgMaxY = CGRectGetMaxY(self.alertView.frame);
    CGFloat allH = height + bgMaxY;
    
    CGFloat subHeight = allH - (self.mj_h)+10;//10为缓冲距离
    
    //获取键盘动画时长
    CGFloat dutation = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //键盘遮挡才需上移
    if(subHeight>0){
        [UIView animateWithDuration:dutation animations:^{
            self.alertView.transform = CGAffineTransformMakeTranslation(0, - subHeight);
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif{
    //获取键盘的高度
    NSDictionary *userInfo = [notif userInfo];
    CGFloat dutation = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:dutation animations:^{
        self.alertView.transform = CGAffineTransformIdentity;
    }];
}

#pragma mark -- UITextFieldDelegate
//  输入保留到小数点后两位
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableString *futureString = [NSMutableString stringWithString:textField.text];
    [futureString insertString:string atIndex:range.location];
    
    NSInteger flag = 0;
    // 这个可以自定义,保留到小数点后两位,后几位都可以
    const NSInteger limited = self.selectBtn.tag == 1001?2:1;
    
    for (NSInteger i = futureString.length - 1; i >= 0; i--) {
        
        if ([futureString characterAtIndex:i] == '.') {
            // 如果大于了限制的就提示
            if (flag > limited) {
  
                return NO;
            }
            
            break;
        }
        
        flag++;
    }
    return YES;
}


#pragma mark - Touch Even
- (void)btnAction:(UIButton *)sender {

    
    if (self.type == AlertBatchTextField) {
        if (!sender.isSelected) {
            self.selectBtn.selected = !self.selectBtn.selected;
            sender.selected = !sender.selected;
            self.selectBtn = sender;
        }
       self.unitLab.text = sender.tag==1000?@"折":@"元";

    }else if (self.type == AlertTextLimit) {
        self.selectBtn = sender;
        if (sender == self.noLimitBtn) {
            self.noLimitBtn.backgroundColor = APPTintColor;
            self.noLimitBtn.layer.borderColor = APPTintColor.CGColor;
            self.limitBtn.backgroundColor = [UIColor whiteColor];
            self.limitBtn.layer.borderColor = color_LineColor.CGColor;
            self.noLimitBtn.selected = YES;
            self.limitBtn.selected = NO;
        }else {
            self.limitBtn.backgroundColor = APPTintColor;
            self.limitBtn.layer.borderColor = APPTintColor.CGColor;
            self.noLimitBtn.backgroundColor = [UIColor whiteColor];
            self.noLimitBtn.layer.borderColor = color_LineColor.CGColor;
            self.noLimitBtn.selected = NO;
            self.limitBtn.selected = YES;
        }
    }
    
  
}


- (void)sureClick:(UIButton *)sender{
    
    if (self.type == AlertText) {
        if (self.sureClick) {
            self.sureClick(self.textField.text);
        }
    }else if (self.type == AlertTextField) {
        
        if ([NSString isBlankString:self.textField.text]) {
            return [YJProgressHUD showMessage:@"请输入内容"];
        }
        if (self.sureClick) {
            self.sureClick(self.textField.text);
        }
    }else if (self.type == AlertTextView) {
        if (self.sureClick) {
            self.sureClick(self.textView.text);
        }
    }else if (self.type == AlertBatchTextField) {
        
        if ([NSString isBlankString:self.textField.text]||
            [self.textField.text isEqualToString:@"0"]) {
            return [YJProgressHUD showMessage:@"输入需大于0"];
        }
        
        NSString *content = @"";
        if (self.selectBtn.tag == 1000) {
            if ([self.textField.text floatValue] > 10.0) {
                return [YJProgressHUD showMessage:@"输入不能大于10"];
            }
            content = [NSString stringWithFormat:@"%.1f",[self.textField.text floatValue]];
            
        }else  {
            content = [NSString stringWithFormat:@"%.02f",[self.textField.text floatValue]];
        }
        if (self.batchsureBlock) {
            self.batchsureBlock(self.selectBtn.tag - 1000, content);
        }
        
    }else if (self.type == AlertTextLimit) {

        if (self.batchsureBlock) {
            self.batchsureBlock(self.selectBtn.tag - 1000,self.selectBtn.titleLabel.text);
        }
        
    }

    [UIView animateWithDuration:0.3 animations:^{
        [self removeFromSuperview];
    }];
}

- (void)cancelClick:(UIButton *)sender{
    
    if (self.cancelClick) {
        self.cancelClick();
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self removeFromSuperview];
    }];
    
}

-(void)showAnimation{
    
    self.backgroundColor = [UIColor clearColor];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    
    if (self.type == AlertText || self.type == AlertTextLimit) {
        self.alertView.layer.position = self.center;
    }
    self.alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.alertView.alpha = 0;
    YXWeakSelf
    [UIView animateWithDuration:0.2 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        weakSelf.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4f];
        weakSelf.alertView.transform = transform;
        weakSelf.alertView.alpha = 1;
        weakSelf.textField.text = nil;
    } completion:^(BOOL finished) {
        
    }];
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    
    [self.textField becomeFirstResponder];
    
    for (NSSet *set in event.allTouches) {
        UITouch *touch = (UITouch *)set;
        if (touch.view != self.alertView) {
            [UIView animateWithDuration:0.3 animations:^{
                [self removeFromSuperview];
            }];
        }
    }
}


- (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
@end
