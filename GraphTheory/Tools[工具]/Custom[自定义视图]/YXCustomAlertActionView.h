//
//  YXCustomAlertActionView.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/13.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,AlertViewType) {
    AlertText,          //提示文字
    AlertTextField,     //提示输入框
    AlertTextView,      //提示文本输入框
    AlertBatchTextField,//提示批量设置价格
    AlertTextLimit,     //提示每人限购
    AlertTextWeb,     //提示隐私协议
};

typedef void(^sureBlock)(NSString *string);
typedef void(^cancelBlock)(void);

@interface YXCustomAlertActionView : UIView


/** 确认按钮*/
@property (nonatomic ,strong) UIButton *sureBtn;

@property (nonatomic ,strong) NSString *sureStr;

//初始化设置样式
- (instancetype)initWithFrame:(CGRect)frame ViewType:(AlertViewType)type Title:(NSString *)title Message:(NSString *)message sureBtn:(NSString *)sureTitle cancleBtn:(NSString *)cancleTitle ;
/**
 弹出视图
 */
-(void)showAnimation;


/**
 点击确定按钮回调
 */
@property (nonatomic,copy)sureBlock sureClick;


/**
 点击取消按钮回调
 */
@property (nonatomic,copy)cancelBlock  cancelClick;


/**
 点击批量设置价格确定按钮回调
 */
@property (nonatomic,copy)void(^batchsureBlock)(NSInteger index,NSString *string);
@end

NS_ASSUME_NONNULL_END
