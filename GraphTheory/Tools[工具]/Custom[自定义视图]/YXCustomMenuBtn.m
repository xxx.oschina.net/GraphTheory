//
//  YXCustomMenuBtn.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/6.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXCustomMenuBtn.h"

@implementation YXCustomMenuBtn


/** 设置按钮本身相关属性 */
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.layer.borderColor = APPTintColor.CGColor;
//        self.layer.borderWidth = 1;
//        self.layer.cornerRadius = 10.0f;
//        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

/** 初始化上下两个Label */
- (YXCustomMenuBtn *)buttonWithAboveLabelTitle:(NSString *)aStr belowLabelTitle:(NSString *)bStr {
    
    UILabel *aboveL = [[UILabel alloc] init];
    aboveL.text = aStr;
    aboveL.font = [UIFont systemFontOfSize:12.0];
    aboveL.textColor = color_TextTwo;
    aboveL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:aboveL];
    self.aboveL = aboveL;
    UILabel *belowL = [[UILabel alloc] init];
    belowL.text = bStr;
    belowL.font = [UIFont systemFontOfSize:12.0];
    belowL.textColor = color_TextOne;
    belowL.textAlignment = NSTextAlignmentCenter;
    [self addSubview:belowL];
    self.belowL = belowL;
    return self;
}

/** 初始化上下两个LabelStr */
- (YXCustomMenuBtn *)buttonWithAboveLabelStr:(NSString *)aStr belowLabelStr:(NSString *)bStr PackageId:(NSString *)packageId {
    _aboveStr = aStr;
    _belowLStr = bStr;
    return self;
}

/** 布局两个子控件 */
- (void)layoutSubviews {
    [super layoutSubviews];
    self.aboveL.frame = CGRectMake(0, 10, self.width, 12);
    self.belowL.frame = CGRectMake(0, CGRectGetMaxY(self.aboveL.frame)+ 5, self.width, 12);
}


/////** 重写按钮选中状态方法 */
//- (void)setSelected:(BOOL)selected {
//    [super setSelected:selected];
//    self.backgroundColor = selected ? APPTintColor : [UIColor whiteColor];
//    self.aboveL.textColor = selected ? [UIColor whiteColor] : APPTintColor;
//    self.belowL.textColor = selected ? [UIColor whiteColor] : APPTintColor;
//}


@end
