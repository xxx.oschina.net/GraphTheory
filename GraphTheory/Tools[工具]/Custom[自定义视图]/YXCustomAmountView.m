//
//  YXCustomAmountView.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/6/5.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "YXCustomAmountView.h"
#import "YXCustomBtn.h"
//#import "ZLJChongDianYuEModel.h"

@interface YXCustomAmountView ()


/** 选择按钮*/
@property (nonatomic ,strong) UIButton *selectedBtn;
@end

@implementation YXCustomAmountView

#pragma mark - Touch Even
- (void)btnAction:(YXCustomBtn *)sender {
    
    if (sender!= self.selectedBtn) {
        self.selectedBtn.selected = NO;
        sender.selected = YES;
        self.selectedBtn = sender;
    }else{
        self.selectedBtn.selected = YES;
    }
    
    if (self.clickSelectAmountBtnBlock) {
        self.clickSelectAmountBtnBlock(sender.aboveStr, sender.belowLStr,sender.packageId);
    }
}

- (void)setupSubvewsWithArray:(NSMutableArray <ZLJChongDianYuEList *>*)listArray{
    
    
//    CGFloat btnHeight = KWIDTH==320?60:70;
//    NSMutableArray  *viewArray = [NSMutableArray array];
//    for (int i = 0; i < listArray.count; i ++) {
//        ZLJChongDianYuEList *listModel = listArray[i];
//        YXCustomBtn *btn = [YXCustomBtn buttonWithType:UIButtonTypeCustom];
//        NSString *amount = [NSString stringWithFormat:@"%0.2f%@",listModel.packageMoney,listModel.unit];
//        NSString *price =  [NSString stringWithFormat:@"售价%0.2f%@",listModel.number,listModel.unit];
//        NSString *kId = listModel.kId;
//        [btn buttonWithAboveLabelTitle:amount belowLabelTitle:price];
//        [btn buttonWithAboveLabelStr:[NSString stringWithFormat:@"%0.2f",listModel.packageMoney]
//                       belowLabelStr:[NSString stringWithFormat:@"%0.2f",listModel.number]
//                           PackageId:kId];
//        [self addSubview:btn];
//        [viewArray addObject:btn];
//        btn.sd_layout.heightIs(btnHeight);
//        [btn addTarget:self action:@selector(btnAction:) forControlEvents:(UIControlEventTouchUpInside)];
//        // 设置默认值
//        if (i==0) {
//            btn.selected = YES;
//            self.selectedBtn=btn;
//            [self btnAction:btn];
//        }
//    }
//
//    [self setupAutoWidthFlowItems:viewArray.copy
//             withPerRowItemsCount:3
//                   verticalMargin:15
//                 horizontalMargin:15
//                verticalEdgeInset:0
//              horizontalEdgeInset:15];
//
}

@end
