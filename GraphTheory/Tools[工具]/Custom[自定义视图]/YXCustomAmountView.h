//
//  YXCustomAmountView.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/6/5.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ZLJChongDianYuEList;
@interface YXCustomAmountView : UIView

/**
 点击选择充值金额回调
 @param amount          金额
 @param price           售价
 @param packageId       id
 */
@property (nonatomic ,copy)void(^clickSelectAmountBtnBlock)(NSString *amount,NSString *price,NSString *packageId);


// 刷新界面
- (void)setupSubvewsWithArray:(NSMutableArray <ZLJChongDianYuEList *>*)listArray;
@end

NS_ASSUME_NONNULL_END
