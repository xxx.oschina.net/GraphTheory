//
//  YXCustomBottomPopView.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/24.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXCustomBottomPopView : UIView

@property (nonatomic ,copy)void(^selectPickerViewBlock)(NSString *text ,NSInteger index);

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *rightBtnTitle;


- (instancetype)initWithFrame:(CGRect)frame midArry:(NSMutableArray *)midArry;

-(void)show;
-(void)close;




@end


