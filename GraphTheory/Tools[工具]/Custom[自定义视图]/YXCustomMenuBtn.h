//
//  YXCustomMenuBtn.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/12/6.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXCustomMenuBtn : UIButton

/** aStr上面的title bStr下面的title */
- (YXCustomMenuBtn *)buttonWithAboveLabelTitle:(NSString *)aStr belowLabelTitle:(NSString *)bStr;

/** aStr上面的str bStr下面的str */
- (YXCustomMenuBtn *)buttonWithAboveLabelStr:(NSString *)aStr belowLabelStr:(NSString *)bStr PackageId:(NSString *)packageId;

/** aboveLabel */
@property (nonatomic, weak) UILabel *aboveL;
/** belowLabel */
@property (nonatomic, weak) UILabel *belowL;
/** aboveStr */
@property (nonatomic, strong) NSString *aboveStr;
/** belowStr */
@property (nonatomic, strong) NSString *belowLStr;

@end

NS_ASSUME_NONNULL_END
