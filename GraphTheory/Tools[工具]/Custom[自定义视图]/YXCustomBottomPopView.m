//
//  YXCustomBottomPopView.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/7/24.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "YXCustomBottomPopView.h"

#define LTColor(r, g, b, a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define UIColorFromRGB(argbValue) [UIColor colorWithRed:((float)((argbValue & 0x00FF0000) >> 16))/255.0 green:((float)((argbValue & 0x0000FF00) >> 8))/255.0 blue:((float)(argbValue & 0x000000FF))/255.0 alpha:((float)((argbValue & 0xFF000000) >> 24))/255.0]

@interface YXCustomBottomPopView ()<UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    UIWindow *_window;
    UITapGestureRecognizer *_gesture;
    UIView *_view;
    UIView*_headView;
    
}

@property (strong, nonatomic) UIView * select;
@property (strong, nonatomic) UIPickerView * pickView;
@property (strong, nonatomic) UIView  * all;
@property (strong, nonatomic) UILabel * allText;
@property (strong, nonatomic) NSMutableArray * midArry;
@property (assign, nonatomic) NSString  *selectStr;
@property (assign, nonatomic) NSInteger index;
@end


@implementation YXCustomBottomPopView

- (instancetype)initWithFrame:(CGRect)frame midArry:(NSMutableArray *)midArry{
    
    self = [super initWithFrame:frame];
    if (self) {
        _midArry = midArry;
        _index = 0;
        [self.pickView reloadAllComponents];
    }
    return self;
}


#pragma mark 打开与关闭方法
-(void)show{
    
    [self setUI];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.frame = CGRectMake(0, kHEIGHT - 200, KWIDTH, 200);
    }];
    
}

-(void)close{
    //移除点击手势
    [_window removeGestureRecognizer:_gesture];
    _gesture = nil;
    [UIView animateWithDuration:0.2 animations:^{
        self.frame = CGRectMake(0, kHEIGHT, KWIDTH, 200);
    } completion:^(BOOL finished) {
        
        for(id subv in [self subviews])
        {
            [subv removeFromSuperview];
        }
        [_view removeFromSuperview];
    }];
}

- (void)setUI{
    
    self.frame = CGRectMake(0, kHEIGHT, KWIDTH, 200);
    
    UIView * topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    [topView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:topView];
    
    UIButton *leftBtn = [[UIButton alloc]init];
    leftBtn.frame = CGRectMake(15, 0, 40, 50);
    [leftBtn setTitle:@"取消" forState:UIControlStateNormal];
    [leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(leftbtnOnclick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:leftBtn];
    
    UIButton *rightBtn = [[UIButton alloc]init];
    rightBtn.frame = CGRectMake(KWIDTH - 55, 0, 40, 50);
    [rightBtn setTitle:@"确定" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(rightBtnOnclick) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:rightBtn];

    
    UILabel *titleLable = [[UILabel alloc]init];
    titleLable.frame = CGRectMake(60, 0, KWIDTH-120, 50);
    titleLable.text = _title;
    titleLable.textAlignment = NSTextAlignmentCenter;
    titleLable.font = [UIFont systemFontOfSize:18];
    titleLable.textColor = [UIColor blackColor];
    [topView addSubview:titleLable];
    
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49, KWIDTH, 1)];
    [lineView setBackgroundColor:color_LineColor];
    [topView addSubview:lineView];
    
    self.select = [[UIView alloc] initWithFrame:CGRectMake(0, 50, KWIDTH, 150)];
    [self addSubview:self.select];
    _pickView=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, KWIDTH, 150)];
    _pickView.delegate=self;
    _pickView.backgroundColor=[UIColor whiteColor];
    _pickView.userInteractionEnabled=YES;
    _pickView.dataSource=self;
    [ self.select addSubview:self.pickView];
    
    [self pickerView:self.pickView didSelectRow:0 inComponent:0];
    
    self.select.userInteractionEnabled=YES;
    
    _window = [UIApplication sharedApplication].keyWindow;
    _window.frame=CGRectMake(0, 0, KWIDTH, kHEIGHT);
    [_window addSubview:self];
    _gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(close)];
    _gesture.delegate=self;
    [_window addGestureRecognizer:_gesture];
    _view = [[UIView alloc]initWithFrame:_window.bounds];
    _view.backgroundColor = LTColor(0, 0, 0, 0.6);
    
    [_window addSubview:_view];
    [_view addSubview:self];
}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    NSLog(@"%@",[touch.view class]);
//
//    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]||[NSStringFromClass([touch.view class]) isEqualToString:@"UITableView"]) {//如果当前是tableView
//        return NO;
//    }
//    return YES;
//}

#pragma mark - UIPickView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

//行中有几列
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.midArry.count;
}
//列显示的数据
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger) row forComponent:(NSInteger)component {
    return self.midArry[row];
}

#pragma mark - delegate
// 选中某一组的某一行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (self.midArry.count > 0) {
        self.selectStr = self.midArry[row];
        self.index = row;
        NSLog(@"%@", self.selectStr);
    }

}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}


- (void)leftbtnOnclick{
    [self close];
}

- (void)rightBtnOnclick{
    
    [self close];
    if (self.selectPickerViewBlock) {
        self.selectPickerViewBlock(self.selectStr,self.index);
    }
    
}

@end
