//
//  YXPublicCollectionReusableView.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXPublicCollectionReusableView : UICollectionReusableView

@property (nonatomic ,strong) UIView *backView;

@property (nonatomic ,strong) UILabel *titleLab;

@end

NS_ASSUME_NONNULL_END
