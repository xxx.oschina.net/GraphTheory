//
//  YXPublicFooterView.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/15.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXPublicFooterView : UIView

@property (nonatomic ,copy)void(^clickAddBtnBlock)(UIButton *button);

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UIButton *addBtn;

@end

NS_ASSUME_NONNULL_END
