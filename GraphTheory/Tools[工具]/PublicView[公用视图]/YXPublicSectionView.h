//
//  YXPublicSectionView.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/11/28.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YXPublicSectionView : UIView

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UIButton *allBtn;

@property (nonatomic ,strong) UIView *lineView;

@end

@interface YXPublicSectionView1 : UIView

@property (nonatomic ,copy)void(^clickAddBtnBlock)(UIButton *button);

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UIButton *addBtn;

@property (nonatomic ,strong) UIView *lineView;

@end

@interface YXPublicSectionView2 : UIView

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *leftLab;

@property (nonatomic ,strong) UILabel *rihgtLab;

@property (nonatomic ,strong) UIView *lineView;

@end
