//
//  DDPublicCell.m
//  DDWGB
//
//  Created by 杨旭 on 2020/12/22.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import "DDPublicCell.h"
#import "UITextView+Placeholder.h"
//#import "DDFileUtils.h"
#import "YXPreviewManage.h"
@interface DDPublicCell ()<UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic,strong) UIImageView*addImgView;
@property (nonatomic,strong) UIImageView*imgView;
@property (nonatomic,strong) UIButton *deleteBtn;

@end

@implementation DDPublicCell

- (void)setIsAdaptive:(BOOL)isAdaptive {
    _isAdaptive = isAdaptive;
}

- (void)setType:(CellViewType)type {
    _type = type;
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.lineView];
    
    self.contentLab.hidden = YES;
    self.contentTF.hidden = YES;
    self.rightImg.hidden = YES;
    self.textView.hidden = YES;
    self.collectionView.hidden = YES;
    if (_type == CellViewTypeStateText) {
        self.contentLab.hidden = NO;
        [self.contentView addSubview:self.contentLab];
    }else if (_type == CellViewTypeStateInput) {
        self.contentTF.hidden = NO;
        [self.contentView addSubview:self.contentTF];
    }else if (_type == CellViewTypeStateNext) {
        self.rightImg.hidden = NO;
        self.contentLab.hidden = NO;
        [self.contentView addSubview:self.rightImg];
        [self.contentView addSubview:self.contentLab];
    }else if (_type == CellViewTypeStateTextView) {
        self.textView.hidden = NO;
        [self.contentView addSubview:self.textView];
    }else if (_type == CellViewTypeStateImage) {
        self.collectionView.hidden = NO;
        [self.contentView addSubview:self.collectionView];
    }
    
    [self addLayout];
}

- (void)setImageArr:(NSMutableArray *)imageArr {
    _imageArr = imageArr;
    [self.collectionView reloadData];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)addLayout {
    
    DDWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.width.equalTo(@70);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.right.bottom.equalTo(weakSelf.contentView);
        make.height.equalTo(@0.5);
    }];
    
    if (_type == CellViewTypeStateText) {
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.width.equalTo(weakSelf.contentView.mas_width).multipliedBy(0.7);
        }];
    }else if (_type == CellViewTypeStateInput) {
        [_contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
            make.left.equalTo(weakSelf.titleLab.mas_right).offset(15);
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.height.offset(40);
        }];
        
     }else if (_type == CellViewTypeStateNext) {
 
         [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
             make.right.equalTo(weakSelf.mas_right).offset(-15);
             make.centerY.equalTo(weakSelf.contentView.mas_centerY);
             make.size.mas_equalTo(CGSizeMake(7, 12));
         }];
         
         if (self.isAdaptive == YES) {
             [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.right.equalTo(weakSelf.rightImg.mas_left).offset(-10);
                 make.top.equalTo(weakSelf.mas_top).offset(15);
                 make.width.equalTo(weakSelf.contentView.mas_width).multipliedBy(0.7);
                 [weakSelf.contentLab sizeToFit];
             }];
         }else {
             [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
                 make.left.equalTo(weakSelf.titleLab.mas_right).offset(15);
                 make.right.equalTo(weakSelf.rightImg.mas_left).offset(-10);
                 make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
                 [weakSelf.contentLab sizeToFit];
             }];
         }
         
     }else if (_type == CellViewTypeStateTextView) {

         [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(15);
             make.left.equalTo(weakSelf.mas_left).offset(15);
             make.bottom.equalTo(weakSelf.mas_bottom).offset(0);
             make.right.equalTo(weakSelf.mas_right).offset(-15);
         }];
         
     }else if (_type == CellViewTypeStateImage) {
         
         [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.top.equalTo(weakSelf.mas_top).offset(45);
             make.left.equalTo(weakSelf.mas_left).offset(15);
             make.right.equalTo(weakSelf.mas_right).offset(-15);
             make.bottom.equalTo(weakSelf.mas_bottom).offset(0);
         }];
         
     }
    
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        
    }
    return _titleLab;
}
- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:14.0f];
        _contentLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _contentLab.textAlignment = NSTextAlignmentRight;
    }
    return _contentLab;
}

- (LTLimitTextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[LTLimitTextField alloc] init];
        _contentTF.font = [UIFont systemFontOfSize:14.0f];
//        _contentTF.textColor = HEX_COLOR(MAIN_TITLE);
        _contentTF.delegate = self;
        _contentTF.placeholder = @"请输入";
        _contentTF.textAlignment = NSTextAlignmentRight;
        [_contentTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contentTF;
}

- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:@"arrowIcon"];
    }
    return _rightImg;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [UITextView new];
//            _textView.textColor = HEX_COLOR(MAIN_TITLE);
        _textView.placeholder = @"请输入问题描述";
        _textView.font = [UIFont systemFontOfSize:14.0f];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.delegate = self;
    }
    return _textView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        //创建一个layout布局类
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        //设置布局方向为垂直流布局
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
//        CGRect collectionFrame = CGRectMake(0, 50,self.width,self.width/4.0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.collectionViewLayout=[UICollectionViewFlowLayout new];
        
        _collectionView.backgroundColor=[UIColor whiteColor];
        [_collectionView registerClass:[DDPhotoCell class] forCellWithReuseIdentifier:@"DDPhotoCell"];
        //设置代理
//        _collectionView.allowsSelection = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.scrollEnabled=NO;
    }
    return _collectionView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
//        _lineView.backgroundColor = HEX_COLOR(MAIN_LINE_BACK);
    }
    return _lineView;
}

#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    if (self.editTextBlock) {
        self.editTextBlock(sender.text);
    }
}
-(void)textFieldDidEndEditing:(UITextField *)sender{
    if (self.endEditTextBlock) {
        self.endEditTextBlock(sender.text);
    }
}
#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewChange) {
        self.textViewChange(textView.text);
    }
}

#pragma mark UICollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _imageArr.count == 0 ? 1 : _imageArr.count + 1;
}

#pragma mark UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.width-75)/4, (self.width-75)/4);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    DDPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DDPhotoCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    if (indexPath.item != 0) {
        cell.delectBtn.tag=indexPath.item+100;
        [cell.delectBtn addTarget:self action:@selector(delpics:) forControlEvents:UIControlEventTouchDown];
        if ([[self.imageArr firstObject] isKindOfClass:[UIImage class]]) {
            UIImage *image = self.imageArr[indexPath.item-1];
            [cell.backImgView setImage:image];
        }else if ([[self.imageArr firstObject] isKindOfClass:[NSString class]]) {
//            NSString *imageName = self.imageArr[indexPath.item-1];
//            NSString *path = TemFilePath(imageName);
//            NSData *imageData = [DDFileUtils readFile:path];
//            UIImage *image = [UIImage imageWithData:imageData];
//            [cell.backImgView setImage:image];
        }
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    // 添加图片
    if (indexPath.item == 0) {
        if (self.clickAddPhoto) {
            self.clickAddPhoto();
         }
    }else { // 预览图片
//        NSMutableArray *tempArr = [NSMutableArray array];
//        if ([[self.imageArr firstObject] isKindOfClass:[NSString class]]) {
//            for (NSString *imageName in self.imageArr) {
//                NSString *path = TemFilePath(imageName);
//                NSData *imageData = [DDFileUtils readFile:path];
//                UIImage *image = [UIImage imageWithData:imageData];
//                if (image) {
//                    [tempArr addObject:image];
//                }
//            }
//        }
//        [[YXPreviewManage sharePreviewManage]showPhotoWithImgArr:tempArr currentIndex:indexPath.item-1 currentView:nil];
    }
}

// 删除图片
-(void)delpics:(UIButton*)sender{
    if (self.clickDeletePhoto) {
        self.clickDeletePhoto(sender);
    }
}

@end



@interface DDPublicCollectionCell ()<UITextFieldDelegate,UITextViewDelegate>

@end

@implementation DDPublicCollectionCell


- (void)setType:(CellViewType)type {
    _type = type;
    [self.contentView addSubview:self.titleLab];
//    [self.contentView addSubview:self.lineView];

    self.contentLab.hidden = YES;
    self.contentTF.hidden = YES;
    self.rightImg.hidden = YES;
    self.textView.hidden = YES;
    self.collectionView.hidden = YES;
    self.imgPicker.hidden = YES;
    if (_type == CellViewTypeStateText) {
        self.titleLab.hidden = NO;
        self.contentLab.hidden = NO;
        [self.contentView addSubview:self.contentLab];
    }else if (_type == CellViewTypeStateInput) {
        self.titleLab.hidden = YES;
        self.contentTF.hidden = NO;
        [self.contentView addSubview:self.contentTF];
    }else if (_type == CellViewTypeStateNext) {
        self.titleLab.hidden = NO;
        self.rightImg.hidden = NO;
        self.contentLab.hidden = NO;
        [self.contentView addSubview:self.rightImg];
        [self.contentView addSubview:self.contentLab];
    }else if (_type == CellViewTypeStateSelect) {
        self.titleLab.hidden = NO;
        self.rightImg.hidden = NO;
        [self.contentView addSubview:self.rightImg];
    }else if (_type == CellViewTypeStateTextView) {
        self.titleLab.hidden = NO;
        self.textView.hidden = NO;
        [self.contentView addSubview:self.textView];
    }else if (_type == CellViewTypeStateImage) {
        self.titleLab.hidden = YES;
        self.imgPicker.hidden = NO;
        [self.contentView addSubview:self.imgPicker];
    }
    
    [self addLayout];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)addLayout {
    
    DDWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.contentView.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        if (weakSelf.type == CellViewTypeStateSelect) {
            make.right.equalTo(weakSelf.mas_right).offset(-60);
        }else {
            [weakSelf.titleLab sizeToFit];
        }
    }];
    
//    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(weakSelf.mas_left).offset(15);
//        make.right.bottom.equalTo(weakSelf.contentView);
//        make.height.equalTo(@0.5);
//    }];
    
    if (_type == CellViewTypeStateText) {
        [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.width.equalTo(weakSelf.contentView.mas_width).multipliedBy(0.7);
        }];
    }else if (_type == CellViewTypeStateInput) {
        [_contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.mas_centerY);
            make.left.equalTo(weakSelf.mas_left).offset(15);
            make.right.equalTo(weakSelf.mas_right).offset(-15);
            make.height.offset(40);
        }];
        
     }else if (_type == CellViewTypeStateNext) {
 
         [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
             make.right.equalTo(weakSelf.mas_right).offset(-15);
             make.centerY.equalTo(weakSelf.contentView.mas_centerY);
             make.size.mas_equalTo(CGSizeMake(7, 12));
         }];
        
         [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
             make.left.equalTo(weakSelf.titleLab.mas_right).offset(15);
             make.right.equalTo(weakSelf.rightImg.mas_left).offset(-10);
             make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
             [weakSelf.contentLab sizeToFit];
         }];
         
     }else if (_type == CellViewTypeStateSelect) {
         
         [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
             make.right.equalTo(weakSelf.mas_right).offset(-15);
             make.centerY.equalTo(weakSelf.contentView.mas_centerY);
             make.size.mas_equalTo(CGSizeMake(30, 30));
         }];
                  
     }else if (_type == CellViewTypeStateTextView) {

         [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
             make.top.equalTo(weakSelf.mas_top).offset(0);
             make.left.equalTo(weakSelf.mas_left).offset(15);
             make.bottom.equalTo(weakSelf.mas_bottom).offset(0);
             make.right.equalTo(weakSelf.mas_right).offset(-15);
         }];
         
     }else if (_type == CellViewTypeStateImage) {
         
         [_imgPicker mas_makeConstraints:^(MASConstraintMaker *make) {
             make.top.equalTo(weakSelf.mas_top).offset(15);
             make.left.equalTo(weakSelf.mas_left).offset(10);
             make.right.equalTo(weakSelf.mas_right).offset(-10);
             make.bottom.equalTo(weakSelf.mas_bottom).offset(-15);
         }];
     }
    
}

#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    if (self.editTextBlock) {
        self.editTextBlock(sender.text);
    }
}
-(void)textFieldDidEndEditing:(UITextField *)sender{
    if (self.endEditTextBlock) {
        self.endEditTextBlock(sender.text);
    }
}
#pragma mark - UITextView Delegate
- (void)textViewDidChange:(UITextView *)textView {
    if (self.textViewChange) {
        self.textViewChange(textView.text);
    }
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
    }
    return _titleLab;
}
- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:14.0f];
        _contentLab.textColor = [UIColor colorWithHexString:@"#333333"];
        _contentLab.textAlignment = NSTextAlignmentRight;
    }
    return _contentLab;
}

- (LTLimitTextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[LTLimitTextField alloc] init];
        _contentTF.font = [UIFont systemFontOfSize:14.0f];
//        _contentTF.textColor = HEX_COLOR(MAIN_TITLE);
        _contentTF.delegate = self;
        _contentTF.placeholder = @"请输入";
        _contentTF.textAlignment = NSTextAlignmentRight;
        [_contentTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contentTF;
}

- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:@"right_arrow_black"];
    }
    return _rightImg;
}

- (UITextView *)textView {
    if (!_textView) {
        _textView = [UITextView new];
//            _textView.textColor = HEX_COLOR(MAIN_TITLE);
        _textView.placeholder = @"请输入问题描述";
        _textView.font = [UIFont systemFontOfSize:14.0f];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.delegate = self;
    }
    return _textView;
}

- (LLImagePickerView *)imgPicker {
    if (!_imgPicker) {
       _imgPicker =  [[LLImagePickerView alloc]initWithFrame:CGRectMake(10, 15, SCREEN_WIDTH-20-20, 70)];
        _imgPicker.type = LLImageTypePhoto;
        _imgPicker.maxImageSelected = 1;
        _imgPicker.allowMultipleSelection = NO;
        //    pickerV.showDelete = NO;
        _imgPicker.allowPickingVideo = NO;
    }
    return _imgPicker;
}



@end


@implementation DDPhotoCell

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)setup {
    if (_indexPath.row == 0) {
        [self.contentView addSubview:self.addArrow];
        [self.addArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
    }else {
        [self.contentView addSubview:self.backImgView];
        [self.contentView addSubview:self.delectBtn];
        [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
        [self.delectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@0);
            make.right.equalTo(@0);
            make.size.mas_equalTo(CGSizeMake(20, 20));
        }];
    }
  
}


- (UIImageView *)addArrow {
    if (!_addArrow) {
        _addArrow = [UIImageView new];
        [_addArrow setImage:[UIImage imageNamed:@"xunjian_takePhoto"]];
    }
    return _addArrow;
}
- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = [UIImageView new];
    }
    return _backImgView;
}

- (UIButton *)delectBtn {
    if (!_delectBtn) {
        _delectBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_delectBtn setImage:[UIImage imageNamed:@"work_delete"] forState:UIControlStateNormal];
    }
    return _delectBtn;
}

@end
