//
//  DDPublicCell.h
//  DDWGB
//
//  Created by 杨旭 on 2020/12/22.
//  Copyright © 2020 DinDo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WZHPhotoBrowserView.h"
#import "LLImagePickerView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,CellViewType) {
    CellViewTypeStateText,      //文本标签
    CellViewTypeStateInput,     //输入框
    CellViewTypeStateNext,      //点击下一步
    CellViewTypeStateSelect,    //点击选择
    CellViewTypeStateTextView,  //文本输入框
    CellViewTypeStateImage,     //选择图片
};

@interface DDPublicCell : UITableViewCell

@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^endEditTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^textViewChange)(NSString *text);
// 添加图片
@property (nonatomic ,copy)void(^clickAddPhoto)(void);
// 删除图片
@property (nonatomic ,copy)void(^clickDeletePhoto)(UIButton *btn);

@property (nonatomic ,copy)void(^backHeight)(CGFloat height);

/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 内容*/
@property (nonatomic ,strong) UILabel *contentLab;
/** 右箭头*/
@property (nonatomic ,strong) UIImageView *rightImg;
/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *contentTF;
/** 文本框*/
@property (nonatomic ,strong) UITextView *textView;
/** 列表*/
@property (nonatomic ,strong)UICollectionView *collectionView;
/** 底部线条*/
@property (nonatomic ,strong) UIView *lineView;
/** 图片数组*/
@property (nonatomic ,strong) NSMutableArray*imageArr;

@property (nonatomic ,assign) BOOL isAdaptive;

@property (nonatomic, assign) CellViewType type;

@end


@interface DDPublicCollectionCell : UICollectionViewCell

@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^endEditTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^textViewChange)(NSString *text);

/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 内容*/
@property (nonatomic ,strong) UILabel *contentLab;
/** 右箭头*/
@property (nonatomic ,strong) UIImageView *rightImg;
/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *contentTF;
/** 文本框*/
@property (nonatomic ,strong) UITextView *textView;
/** 列表*/
@property (nonatomic ,strong) UICollectionView *collectionView;
/** 图片选择器*/
@property (strong, nonatomic)LLImagePickerView *imgPicker;

@property (nonatomic, assign) CellViewType type;



@end


@interface DDPhotoCell : UICollectionViewCell

@property (nonatomic ,strong) NSIndexPath *indexPath;

/**icon*/
@property (nonatomic ,strong) UIImageView *addArrow;

@property (nonatomic ,strong) UIImageView *backImgView;
/**删除按钮*/
@property (nonatomic ,strong) UIButton *delectBtn;


@end

NS_ASSUME_NONNULL_END
