//
//  YXPublicCell.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/10/27.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IQTextView.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXPublicCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *contentLab;
@property (nonatomic ,strong) UIView *lineView;


@property (nonatomic, assign) CGFloat leftCons;

@property (nonatomic, assign) CGFloat rightCons;

@end


@interface YXPublicCell1 : UITableViewCell


@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UILabel *contentLab;

@property (nonatomic ,strong) UIImageView *rightImg;

@property (nonatomic, assign) CGFloat rightCons;
@end





@interface YXPublicCell2 : UITableViewCell

@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^endEditTextBlock)(NSString *text);


/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *contentTF;

@property (nonatomic, assign) CGFloat leftCons;


@end


@interface YXPublicCell3 : UITableViewCell

/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 子标题*/
@property (nonatomic ,strong) UILabel *subTitleLab;
/** 右标题*/
@property (nonatomic ,strong) UILabel *rightLab;

@end


@interface YXPublicCell4 : UITableViewCell

@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);


/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *contentTF;
@property (nonatomic ,strong) UIButton *select;


@end

@interface YXPublicCell5 : UITableViewCell

@property (nonatomic ,copy) void(^swtichOnOrOff)(BOOL status);

/** 标题*/
@property (nonatomic ,strong) UILabel *titleLab;
/** 右边切换按钮*/
@property (nonatomic ,strong) UISwitch *isSwitch;

@end


@interface YXPublicCell6 : UITableViewCell


@property (nonatomic ,copy) void(^editTextBlock)(NSString *text);

@property (nonatomic ,copy) void(^endEditTextBlock)(NSString *text);

/** 输入框*/
@property (nonatomic ,strong) LTLimitTextField *contentTF;

@end


@interface YXPublicCell7 : UITableViewCell


@property (nonatomic ,strong) UIImageView *imgView;
@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *contentLab;
@property (nonatomic ,strong) UILabel *rightLab;
@property (nonatomic ,strong) UIView *lineView;


@end

@interface YXPublicCell8 : UITableViewCell

@property (nonatomic ,copy) void(^textViewChange)(NSString *text);

@property (nonatomic ,strong) IQTextView *textView;
@property (nonatomic ,strong) UIView *lineView;

@end

@interface YXPublicCell9 : UITableViewCell

@property (nonatomic ,copy) void(^editTextBlock)(NSString *text,NSInteger index);

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIView *leftView;
@property (nonatomic ,strong) UIView *rightView;
@property (nonatomic ,strong) LTLimitTextField *leftTF;
@property (nonatomic ,strong) LTLimitTextField *rightTF;


@end

@interface YXPublicCell10 : UITableViewCell

@property (nonatomic ,copy)void(^clickTimeViewBlock)(NSInteger index);

@property (nonatomic ,strong) NSString *title;

@property (nonatomic ,strong) NSString *start;

@property (nonatomic ,strong) NSString *end;



@end

NS_ASSUME_NONNULL_END
