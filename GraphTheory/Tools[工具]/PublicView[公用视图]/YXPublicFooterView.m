
//
//  YXPublicFooterView.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/15.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXPublicFooterView.h"

@implementation YXPublicFooterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:(CGRectZero)];
        _bgView.backgroundColor = color_LineColor;
    }
    return _bgView;
}

- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        _addBtn.frame = CGRectMake(15, 15, KWIDTH-30, 44);
        _addBtn.backgroundColor = [UIColor colorWithHexString:@"#FF6F24" alpha:0.1];
        [_addBtn setTitle:@"+ 添加商品" forState:(UIControlStateNormal)];
        [_addBtn setTitleColor:APPTintColor forState:(UIControlStateNormal)];
        _addBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_addBtn addTarget:self action:@selector(addBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _addBtn;
}

- (void)setup {
    
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.addBtn];
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.offset(0);
    }];
    
//    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(weakSelf.bgView);
//        make.left.equalTo(weakSelf.bgView.mas_left).offset(15);
//        make.size.mas_equalTo(CGSizeMake(KWIDTH-30, 44));
//    }];
//
    CAShapeLayer *border = [CAShapeLayer layer];
    //虚线的颜色
    border.strokeColor = APPTintColor.CGColor;
    //填充的颜色
    border.fillColor = [UIColor clearColor].CGColor;
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:_addBtn.bounds cornerRadius:4];
    //设置路径
    border.path = path.CGPath;
    
    border.frame = _addBtn.bounds;
    //虚线的宽度
    border.lineWidth = 1.f;
    //设置线条的样式
    //    border.lineCap = @"square";
    //虚线的间隔
    border.lineDashPattern = @[@4, @2];
    
    _addBtn.layer.cornerRadius = 4.f;
    _addBtn.layer.masksToBounds = YES;
    [_addBtn.layer addSublayer:border];
}

- (void)addBtnAction:(UIButton *)sender {
    if (self.clickAddBtnBlock) {
        self.clickAddBtnBlock(sender);
    }
}

@end
