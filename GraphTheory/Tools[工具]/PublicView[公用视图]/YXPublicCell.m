//
//  YXPublicCell.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/10/27.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "YXPublicCell.h"
//#import "WGPlaceholderTextView.h"

@interface YXPublicCell ()

@end

@implementation YXPublicCell


- (void)setLeftCons:(CGFloat)leftCons {
    _leftCons = leftCons;
    YXWeakSelf
    [_titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(weakSelf.leftCons);
        [weakSelf.titleLab sizeToFit];
    }];
    [self layoutIfNeeded];
}

- (void)setRightCons:(CGFloat)rightCons {
    _rightCons = rightCons;
    YXWeakSelf
    [_contentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(10);
        //        make.right.equalTo(weakSelf.contentView.mas_right).offset(-_rightCons);
        make.width.equalTo(weakSelf.contentView.mas_width).multipliedBy(0.7);        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.contentLab sizeToFit];
    }];
    [self layoutIfNeeded];
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setup];
    }
    return self;
}

- (void)setup {
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.contentLab];
    [self.contentView addSubview:self.lineView];
    [self addLayout];
}

- (void)addLayout {
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.offset(60);
    }];
    
    [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.width.equalTo(weakSelf.contentView.mas_width).multipliedBy(0.7);
        
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(@0.5);
    }];
    
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        
    }
    return _titleLab;
}
- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:14.0f];
        _contentLab.textColor = [UIColor colorWithHexString:@"#333333"];
    }
    return _contentLab;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

@end


@implementation YXPublicCell1


- (void)setRightCons:(CGFloat)rightCons {
    _rightCons = rightCons;
    YXWeakSelf
    [_contentLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-weakSelf.rightCons);
        [weakSelf.contentLab sizeToFit];
    }];
    [self layoutIfNeeded];
    
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setup];
        [self addLayout];
    }
    return self;
}

- (void)setup {
    
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.rightImg];
    [self.contentView addSubview:self.contentLab];
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (UIImageView *)rightImg {
    if (!_rightImg) {
        _rightImg = [UIImageView new];
        _rightImg.image = [UIImage imageNamed:@"advert_arrow_right"];
    }
    return _rightImg;
}

- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.text = @"请选择";
        _contentLab.font = [UIFont systemFontOfSize:14.0f];
        _contentLab.textColor = color_TextOne;
    }
    return _contentLab;
}

- (void)addLayout {
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_rightImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@-15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(7, 12));
    }];
    
    [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.rightImg.mas_left).offset(-10);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.contentLab sizeToFit];
    }];
}

@end

@interface YXPublicCell2 ()<UITextFieldDelegate>

@end

@implementation YXPublicCell2

- (void)setLeftCons:(CGFloat)leftCons {
    _leftCons = leftCons;
    YXWeakSelf
    [_titleLab mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(weakSelf.leftCons);
        [weakSelf.titleLab sizeToFit];
    }];
    [self layoutIfNeeded];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.contentTF];
    }
    return self;
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (LTLimitTextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[LTLimitTextField alloc] init];
        _contentTF.font = [UIFont systemFontOfSize:16];
        _contentTF.textColor = color_TextOne;
        _contentTF.delegate = self;
        _contentTF.placeholder = @"请输入";
        _contentTF.textAlignment = NSTextAlignmentRight;
        [_contentTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contentTF;
}


#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    if (sender.markedTextRange == nil) {
        if (self.editTextBlock) {
            self.editTextBlock(sender.text);
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)sender{
    if (self.endEditTextBlock) {
        self.endEditTextBlock(sender.text);
    }
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    YXWeakSelf
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [self.contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-20);
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(10);
        make.height.offset(40);
    }];
}


@end


@interface YXPublicCell3 ()

@end

@implementation YXPublicCell3


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.subTitleLab];
        [self.contentView addSubview:self.rightLab];
        [self addLayout];
        
    }
    return self;
}


#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"消费(视频广告)";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:14];
    }
    return _titleLab;
}

- (UILabel *)subTitleLab {
    if (!_subTitleLab) {
        _subTitleLab = [[UILabel alloc] init];
        _subTitleLab.text = @"2019-02-22 17:00";
        _subTitleLab.textColor = color_TextThree;
        _subTitleLab.font = [UIFont systemFontOfSize:14];
    }
    return _subTitleLab;
}

- (UILabel *)rightLab {
    if (!_rightLab) {
        _rightLab = [[UILabel alloc] init];
        _rightLab.text = @"80.00元";
        _rightLab.textColor = [UIColor redColor];
        _rightLab.font = [UIFont boldSystemFontOfSize:16];
    }
    return _rightLab;
}

#pragma mark - Layout
- (void)addLayout {
    
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(15);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        [weakSelf.subTitleLab sizeToFit];
    }];
    
    [_rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        [weakSelf.rightLab sizeToFit];
    }];
    
}



@end




@interface YXPublicCell4 ()<UITextFieldDelegate>

@end

@implementation YXPublicCell4


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.contentTF];
        [self.contentView addSubview:self.select];
    }
    return self;
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}
- (UIButton *)select{
    if (!_select) {
        _select = [UIButton buttonWithType:UIButtonTypeCustom];
        [_select setImage:[UIImage imageNamed:@"login_password_hide"] forState:(UIControlStateNormal)];
        [_select setImage:[UIImage imageNamed:@"login_password_show"] forState:(UIControlStateSelected)];
        [_select addTarget:self action:@selector(selectAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _select;
}
- (LTLimitTextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[LTLimitTextField alloc] init];
        _contentTF.font = [UIFont systemFontOfSize:16];
        _contentTF.textAlignment = NSTextAlignmentRight;
        _contentTF.secureTextEntry = YES;
        _contentTF.placeholder = @"请输入";
        _contentTF.delegate = self;
        [_contentTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contentTF;
}


#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    
    if (self.editTextBlock) {
        self.editTextBlock(sender.text);
    }
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    YXWeakSelf
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(70, 44));
    }];
    
    [self.contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.titleLab.mas_right).offset(10);
        make.right.equalTo(weakSelf.select.mas_left).offset(-10);
        make.height.offset(44);
    }];
    [self.select mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(44, 44));
        make.right.equalTo(self.contentView.mas_right).offset(-15);
    }];
}

- (void)selectAction:(UIButton *)sender {
    self.select.selected = ! self.select.selected;
    self.contentTF.secureTextEntry = !sender.selected;
}


@end


@interface YXPublicCell5 ()

@end

@implementation YXPublicCell5


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setup];
    }
    return self;
}

- (void)setup {
    [self.contentView addSubview:self.titleLab];
    [self.contentView addSubview:self.isSwitch];
    [self addLayout];
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:16];
        _titleLab.textColor = color_TextOne;
        
    }
    return _titleLab;
}
- (UISwitch *)isSwitch {
    if (!_isSwitch) {
        _isSwitch = [[UISwitch alloc] init];
        [_isSwitch setOnTintColor:APPTintColor];
        _isSwitch.on = YES;
        [_isSwitch addTarget:self action:@selector(valueChanged:) forControlEvents:(UIControlEventValueChanged)];
    }
    return _isSwitch;
}
- (void)addLayout {
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.centerY.equalTo(weakSelf.mas_centerY);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_isSwitch mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    
}

- (void)valueChanged:(UISwitch *)switc{
    if (self.swtichOnOrOff) {
        self.swtichOnOrOff(switc.on);
    }
}

@end


@interface YXPublicCell6 ()<UITextFieldDelegate>


@end

@implementation YXPublicCell6


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.contentTF];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (LTLimitTextField *)contentTF {
    if (!_contentTF) {
        _contentTF = [[LTLimitTextField alloc] init];
        _contentTF.font = [UIFont systemFontOfSize:16];
        _contentTF.textAlignment = NSTextAlignmentLeft;
        _contentTF.placeholder = @"请输入";
        _contentTF.delegate = self;
        [_contentTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _contentTF;
}

- (void)addLayout {
    
    YXWeakSelf
    [_contentTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.bottom.equalTo(weakSelf);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
    }];

}

#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    if (sender.markedTextRange == nil) {
        if (self.editTextBlock) {
            self.editTextBlock(sender.text);
        }
    }
}

-(void)textFieldDidEndEditing:(UITextField *)sender{
    if (self.endEditTextBlock) {
        self.endEditTextBlock(sender.text);
    }
}

@end



@implementation YXPublicCell7

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.imgView];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.contentLab];
        [self.contentView addSubview:self.rightLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (UIImageView *)imgView {
    if (!_imgView) {
        _imgView = [UIImageView new];
  
    }
    return _imgView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont boldSystemFontOfSize:14];
        _titleLab.textColor = [UIColor colorWithHexString:@"#333333"];
        
    }
    return _titleLab;
}
- (UILabel *)contentLab {
    if (!_contentLab) {
        _contentLab = [[UILabel alloc] init];
        _contentLab.font = [UIFont systemFontOfSize:14];
        _contentLab.textColor = [UIColor colorWithHexString:@"#999999"];
    }
    return _contentLab;
}
- (UILabel *)rightLab {
    if (!_rightLab) {
        _rightLab = [[UILabel alloc] init];
        _rightLab.font = [UIFont systemFontOfSize:14];
        _rightLab.textColor = [UIColor colorWithHexString:@"#999999"];
    }
    return _rightLab;
}


- (void)addLayout {
    
    YXWeakSelf
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
        [weakSelf.imgView sizeToFit];
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.imgView.mas_right).offset(15);
        make.top.equalTo(weakSelf.mas_top).offset(15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.height.offset(14);
    }];
    
    [_contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.imgView.mas_right).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.height.offset(14);
    }];
    
    [_rightLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.titleLab.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        [weakSelf.rightLab sizeToFit];
    }];
}

@end


@interface YXPublicCell8 ()<UITextViewDelegate>

@end


@implementation YXPublicCell8

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.textView];
        [self.contentView addSubview:self.lineView];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy loading
- (IQTextView *)textView {
    if (!_textView) {
        _textView = [IQTextView new];
            _textView.textColor = color_TextOne;
        _textView.placeholderTextColor = [UIColor colorWithHexString:@"#999999"];
        _textView.placeholder = @"请输入详细地址";
        _textView.font = [UIFont systemFontOfSize:14.0f];
        _textView.textAlignment = NSTextAlignmentLeft;
        _textView.delegate = self;
    }
    return _textView;
}
- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

- (void)addLayout {
    
    YXWeakSelf
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(10);
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-15);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.right.equalTo(weakSelf.mas_right);
        make.height.equalTo(@0.5);
    }];
}

#pragma mark - UITextView Delegate
- (void)textViewDidChange:(IQTextView *)textView {
    if (self.textViewChange) {
        self.textViewChange(textView.text);
    }
}


@end

@interface YXPublicCell9 ()<UITextFieldDelegate>

@end

@implementation YXPublicCell9

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.leftView];
        [self.leftView addSubview:self.leftTF];
        [self.contentView addSubview:self.rightView];
        [self.rightView addSubview:self.rightTF];
    }
    return self;
}

#pragma mark - Lazy loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (UIView *)leftView {
    if (!_leftView) {
        _leftView = [UIView new];
        _leftView.layer.masksToBounds = YES;
        _leftView.layer.cornerRadius = 4.0f;
        _leftView.layer.borderColor = color_LineColor.CGColor;
        _leftView.layer.borderWidth = 1.0f;
    }
    return _leftView;
}

- (UIView *)rightView {
    if (!_rightView) {
        _rightView = [UIView new];
        _rightView.layer.masksToBounds = YES;
        _rightView.layer.cornerRadius = 4.0f;
        _rightView.layer.borderColor = color_LineColor.CGColor;
        _rightView.layer.borderWidth = 1.0f;
    }
    return _rightView;
}
- (LTLimitTextField *)leftTF {
    if (!_leftTF) {
        _leftTF = [[LTLimitTextField alloc] init];
        _leftTF.font = [UIFont systemFontOfSize:15];
        _leftTF.textAlignment = NSTextAlignmentLeft;
        _leftTF.keyboardType = UIKeyboardTypeDecimalPad;
        _leftTF.placeholder = @"请输入";
        _leftTF.delegate = self;
        _leftTF.tag = 2000;
        [_leftTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _leftTF;
}
- (LTLimitTextField *)rightTF {
    if (!_rightTF) {
        _rightTF = [[LTLimitTextField alloc] init];
        _rightTF.font = [UIFont systemFontOfSize:15];
        _rightTF.textAlignment = NSTextAlignmentLeft;
        _rightTF.keyboardType = UIKeyboardTypeDecimalPad;
        _rightTF.placeholder = @"请输入";
        _rightTF.delegate = self;
        _rightTF.tag = 2001;
        [_rightTF addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    }
    return _rightTF;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    YXWeakSelf
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf);
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.height.equalTo(@44);
    }];
    
    [_leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(15);
        make.top.equalTo(weakSelf.titleLab.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(150, 32));
    }];
    
    [_rightView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-15);
        make.top.equalTo(weakSelf.titleLab.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(150, 32));
    }];
    
    
    [_leftTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.leftView.mas_left).offset(15);
        make.top.right.bottom.equalTo(weakSelf.leftView);
    }];
    
    [_rightTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.rightView.mas_left).offset(15);
        make.top.right.bottom.equalTo(weakSelf.rightView);
    }];

}


#pragma mark - UITextField Delegate
- (void)textFieldChanged:(UITextField *)sender {
    
    if (self.editTextBlock) {
        self.editTextBlock(sender.text, sender.tag-2000);
    }

    
}

@end


@interface YXPublicCell10 ()<UITextFieldDelegate>

@property (nonatomic ,strong) UILabel *titleLab;

@property (nonatomic ,strong) UIView *startView;
@property (nonatomic ,strong) UILabel *startLab;
@property (nonatomic ,strong) UIImageView *startImg;
@property (nonatomic ,strong) UIView *endView;
@property (nonatomic ,strong) UILabel *endLab;
@property (nonatomic ,strong) UIImageView *endImg;

@end

@implementation YXPublicCell10

- (void)setStart:(NSString *)start {
    _start = start;
    _startLab.text = [NSString stringWithFormat:@"%@",_start==nil?@"开始时间":_start];
}

- (void)setEnd:(NSString *)end {
    _end = end;
    _endLab.text = [NSString stringWithFormat:@"%@",_end==nil?@"结束时间":_end];

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.titleLab];
        [self.contentView addSubview:self.startView];
        [self.startView addSubview:self.startImg];
        [self.startView addSubview:self.startLab];
        [self.contentView addSubview:self.endView];
        [self.endView addSubview:self.endImg];
        [self.endView addSubview:self.endLab];
        [self addLayout];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"有效时间";
        _titleLab.font = [UIFont systemFontOfSize:14.0f];
        _titleLab.textColor = color_TextOne;
    }
    return _titleLab;
}

- (UIView *)startView {
    if (!_startView) {
        _startView = [[UIView alloc] init];
        _startView.backgroundColor = [UIColor whiteColor];
        _startView.layer.masksToBounds = YES;
        _startView.layer.cornerRadius = 4.0f;
        _startView.layer.borderColor = color_LineColor.CGColor;
        _startView.layer.borderWidth = 1.0f;
        _startView.tag =1000;
        _startView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(startViewAction)];
        [_startView addGestureRecognizer:tap];
        
    }
    return _startView;
}

- (UILabel *)startLab {
    if (!_startLab) {
        _startLab = [[UILabel alloc] init];
        _startLab.text = @"开始时间";
        _startLab.textColor = color_TextTwo;
        _startLab.font =[UIFont systemFontOfSize:12.0f];
    }
    return _startLab;
}

- (UIImageView *)startImg {
    if (!_startImg) {
        _startImg = [UIImageView new];
        _startImg.image = [UIImage imageNamed:@"screen_calendar"];
    }
    return _startImg;
}
- (UIView *)endView {
    if (!_endView) {
        _endView = [[UIView alloc] init];
        _endView.backgroundColor = [UIColor whiteColor];
        _endView.layer.masksToBounds = YES;
        _endView.layer.cornerRadius = 4.0f;
        _endView.layer.borderColor = color_LineColor.CGColor;
        _endView.layer.borderWidth = 1.0f;
        _endView.tag =1001;
        _endView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endViewAction)];
        [_endView addGestureRecognizer:tap];
    }
    return _endView;
}

- (UILabel *)endLab {
    if (!_endLab) {
        _endLab = [[UILabel alloc] init];
        _endLab.text = @"结束时间";
        _endLab.textColor = color_TextTwo;
        _endLab.font =[UIFont systemFontOfSize:12.0f];
    }
    return _endLab;
}
- (UIImageView *)endImg {
    if (!_endImg) {
        _endImg = [UIImageView new];
        _endImg.image = [UIImage imageNamed:@"screen_calendar"];
    }
    return _endImg;
}

#pragma mark - Layout
- (void)addLayout {
    
    YXWeakSelf
     [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@15);
        [weakSelf.titleLab sizeToFit];
    }];
    
    [_startView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(15);
        make.left.equalTo(@15.0);
        make.size.mas_equalTo(CGSizeMake(150, 32));
    }];
    
    [_startImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startView.mas_centerY);
        make.right.equalTo(weakSelf.startView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(18, 17));
    }];
    
    [_startLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.startView.mas_centerY);
        make.right.equalTo(weakSelf.startImg.mas_right).offset(-10);
        make.left.equalTo(weakSelf.startView.mas_left).offset(10);
        make.height.equalTo(@20.0);
    }];
    
    [_endView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleLab.mas_bottom).offset(15);
        make.right.equalTo(@-15.0);
        make.size.mas_equalTo(CGSizeMake(150, 32));
    }];
    
    [_endImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endView.mas_centerY);
        make.right.equalTo(weakSelf.endView.mas_right).offset(-10);
        make.size.mas_equalTo(CGSizeMake(18, 17));
    }];
    
    [_endLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.endView.mas_centerY);
        make.right.equalTo(weakSelf.endImg.mas_right).offset(-10);
        make.left.equalTo(weakSelf.endView.mas_left).offset(10);
        make.height.equalTo(@20.0);
    }];
}

#pragma mark - Touch Even
- (void)startViewAction {
    if (self.clickTimeViewBlock) {
        self.clickTimeViewBlock(0);
    }
}

- (void)endViewAction {
    if (self.clickTimeViewBlock) {
        self.clickTimeViewBlock(1);
    }
}

@end
