//
//  YXPublicCollectionReusableView.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/9/23.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXPublicCollectionReusableView.h"

@implementation YXPublicCollectionReusableView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
    }
    return _backView;
}
- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont systemFontOfSize:15.0f];
    }
    return _titleLab;
}

- (void)setup {
    
    [self addSubview:self.backView];
    [self.backView addSubview:self.titleLab];
    YXWeakSelf
    [_backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.right.equalTo(@-15);
        make.top.bottom.equalTo(self);
    }];
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.backView.mas_centerY);
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        [weakSelf.titleLab sizeToFit];
    }];
        
}




@end
