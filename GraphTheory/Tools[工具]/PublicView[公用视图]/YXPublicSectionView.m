//
//  YXPublicSectionView.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/11/28.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXPublicSectionView.h"

@implementation YXPublicSectionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:(CGRectZero)];
    }
    return _bgView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"全选";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont boldSystemFontOfSize:20.0f];
    }
    return _titleLab;
}


- (void)setup {

    [self addSubview:self.bgView];
    [self.bgView addSubview:self.titleLab];

    YXWeakSelf
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(KWIDTH, 44));
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.left.equalTo(weakSelf.bgView.mas_left).offset(15);
    }];
    
}
@end

@implementation YXPublicSectionView1

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:(CGRectZero)];
    }
    return _bgView;
}

- (UILabel *)titleLab {
    if (!_titleLab) {
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"适用商品(0)";
        _titleLab.textColor = color_TextOne;
        _titleLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _titleLab;
}

- (UIButton *)addBtn {
    if (!_addBtn) {
        _addBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [_addBtn setTitle:@"添加" forState:(UIControlStateNormal)];
        [_addBtn setTitleColor:color_GreenColor forState:(UIControlStateNormal)];
        _addBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [_addBtn addTarget:self action:@selector(addBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    }
    return _addBtn;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] initWithFrame:CGRectZero];
        _lineView.backgroundColor = color_LineColor;
    }
    return _lineView;
}

- (void)setup {
    
    
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.titleLab];
    [self.bgView addSubview:self.addBtn];
    [self.bgView addSubview:self.lineView];
    
    YXWeakSelf
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left).offset(0);
        make.size.mas_equalTo(CGSizeMake(KWIDTH, 44));
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.left.equalTo(weakSelf.bgView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(100, 44));
    }];
    
    [_addBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.right.equalTo(weakSelf.bgView.mas_right).offset(-15);
        make.size.mas_equalTo(CGSizeMake(44, 44));
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(@0.0);
        make.height.equalTo(@0.5);
    }];
    
}


#pragma mark - Touch Even
- (void)addBtnAction:(UIButton *)sender {
    
    if (self.clickAddBtnBlock) {
        self.clickAddBtnBlock(sender);
    }
}

@end

@implementation YXPublicSectionView2

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setup];
    }
    return self;
}

#pragma mark - Lazy Loading
- (UIView *)bgView {
    if (!_bgView) {
        _bgView = [[UIView alloc] initWithFrame:(CGRectZero)];
    }
    return _bgView;
}

- (UILabel *)leftLab {
    if (!_leftLab) {
        _leftLab = [[UILabel alloc] init];
        _leftLab.text = @"评论";
        _leftLab.textColor = color_TextOne;
        _leftLab.font = [UIFont boldSystemFontOfSize:14.0f];
    }
    return _leftLab;
}

- (UILabel *)rihgtLab {
    if (!_rihgtLab) {
        _rihgtLab = [[UILabel alloc] init];
        _rihgtLab.backgroundColor = [UIColor whiteColor];
        _rihgtLab.textColor = color_TextTwo;
        _rihgtLab.font = [UIFont systemFontOfSize:12.0f];
        _rihgtLab.textAlignment =NSTextAlignmentCenter;
        _rihgtLab.text = @"1";
        _rihgtLab.layer.masksToBounds = YES;
        _rihgtLab.layer.cornerRadius = 4;
        _rihgtLab.layer.borderWidth =1.0;
        _rihgtLab.layer.borderColor = color_LineColor.CGColor;
    }
    return _rihgtLab;
}

- (void)setup {
    
    [self addSubview:self.bgView];
    [self.bgView addSubview:self.leftLab];
    [self.bgView addSubview:self.rihgtLab];
    
    YXWeakSelf
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.left.equalTo(weakSelf.mas_left);
        make.size.mas_equalTo(CGSizeMake(KWIDTH, 44));
    }];
    
    [_leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.left.equalTo(weakSelf.bgView.mas_left).offset(15);
        [_leftLab sizeToFit];
    }];
    
    [_rihgtLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.bgView.mas_centerY);
        make.left.equalTo(weakSelf.leftLab.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(20, 15));
    }];
    
}

@end

