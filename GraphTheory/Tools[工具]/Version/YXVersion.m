//
//  YXVersion.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/12/25.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "YXVersion.h"

@implementation YXVersion

+ (void)appVersonUpdateWithState:(NSInteger)state {
        
    //获取手机程序的版本号
    __block NSString *old_version = BundleIdentifier;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setValue:APPStoreID forKey:@"id"];
    [NetWorkTools postWithUrl:@"https://itunes.apple.com/lookup" parameters:dict success:^(id responseObj) {
        NSArray *array = responseObj[@"results"];
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (array.count != 0) {// 先判断返回的数据是否为空
            NSString *appStoreVersion = array[0][@"version"];
            // yx_设置版本号
            old_version = [old_version stringByReplacingOccurrencesOfString:@"." withString:@""];
            if (old_version.length==2) {
                old_version  = [old_version stringByAppendingString:@"0"];
            }else if (old_version.length==1){
                old_version  = [old_version stringByAppendingString:@"00"];
            }
            appStoreVersion = [appStoreVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
            if (appStoreVersion.length==2) {
                appStoreVersion  = [appStoreVersion stringByAppendingString:@"0"];
            }else if (appStoreVersion.length==1){
                appStoreVersion  = [appStoreVersion stringByAppendingString:@"00"];
            }
            //yx_设置版本号
            if ((appStoreVersion.floatValue > old_version.floatValue)) {
                NSString  *appVersion = BundleIdentifier;
                NSString  *appStroeVersion = array[0][@"version"];
                NSString *msg = [NSString stringWithFormat:@"你当前的版本是V%@，发现新版本V%@，是否更新新版本？",appVersion,appStroeVersion];
                [defaults setInteger:floorf(appStoreVersion.floatValue) forKey:@"updateAPP"];
                UIAlertController *alertCon = [UIAlertController alertControllerWithTitle:@"检测到新版本"
                                                                                  message:msg
                                                                           preferredStyle:UIAlertControllerStyleAlert];
                if (state==0) {
                    UIAlertAction   *cancel = [UIAlertAction actionWithTitle:@"下次再说"
                                                                       style:UIAlertActionStyleCancel
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    [alertCon addAction:cancel];
                }
                UIAlertAction   *update = [UIAlertAction actionWithTitle:@"更新"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                    [self updateButtonClick];
                }];
                
                [alertCon addAction:update];
                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertCon animated:YES completion:nil];
            }
        }
        
    } failure:^(NSError *error) {
        
    }];
    
    
}



//更新APP
+ (void)updateButtonClick{
    NSString  * nsStringToOpen = [NSString  stringWithFormat: @"itms-apps://itunes.apple.com/app/viewContentsUserReviews/id%@",APPStoreID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:nsStringToOpen]];
}



@end
