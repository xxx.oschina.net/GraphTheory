//
//  YXVersion.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/12/25.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YXVersion : NSObject


/**
 版本更新

 @param state 0:非强制更新 1:强制更新
 */
+ (void)appVersonUpdateWithState:(NSInteger)state;

@end
