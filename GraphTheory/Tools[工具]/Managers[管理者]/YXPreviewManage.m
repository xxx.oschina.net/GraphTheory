//
//  YXPreviewManage.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXPreviewManage.h"
#import "LLImagePickerModel.h"
#import "YXFileModel.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "ZQPlayer.h"
#import "WGAddGoodsModel.h"
#import <YBImageBrowser.h>
#import <YBIBVideoData.h>

@interface YXPreviewManage ()<ZQPlayerDelegate>

@property (nonatomic ,strong) NSMutableArray *photos;

@property (nonatomic ,strong) ZQPlayer *player;

@end

@implementation YXPreviewManage

+ (YXPreviewManage *)sharePreviewManage{
    static YXPreviewManage *previewManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        previewManager = [[self alloc]init];
    });
    return previewManager;
}

- (ZQPlayer *)player{
    if (!_player) {
        _player = [ZQPlayer new];
        _player.delegate = self;
    }
    return _player;
}


- (void)showVideoWithUrl:(NSString *)url withController:(UIViewController *)controller {
    
    if (url.length > 0) {
        AVPlayerViewController *playVC = [AVPlayerViewController new];
        playVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:url]];
        [playVC.player play];
        playVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [controller presentViewController:playVC animated:YES completion:nil];
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:playVC];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nc animated:YES completion:nil];
//        YBIBVideoData *data = [YBIBVideoData new];
//        data.videoURL = [NSURL URLWithString:url];
//        YBImageBrowser *browser = [YBImageBrowser new];
//        browser.toolViewHandlers = nil;
//        browser.dataSourceArray = @[data];
//        browser.currentPage = 0;
//        browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
//        [browser show];
    }
}



- (void)showPhotoWithImgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentInde{
    
    
    _photos = [NSMutableArray array];
    
    for (id tem in imgArr) {
        if ([tem isKindOfClass:[UIImage class]]){
            YBIBImageData *data = [YBIBImageData new];
            data.image = ^UIImage * _Nullable{
                return tem;
            };
            [_photos addObject:data];
        }else if ([tem isKindOfClass:[WGGoodsImageModel class]]) {
            WGGoodsImageModel *temModel = (WGGoodsImageModel *)tem;
            YBIBImageData *data = [YBIBImageData new];
            
            if (temModel.original) {
                data.image = ^UIImage * _Nullable{
                    return temModel.original;
                };
            }else if (temModel.imagePath) {
                data.imageURL = [NSURL URLWithString:temModel.imagePath];
            }else if (temModel.thumbnail){
                data.thumbImage = temModel.thumbnail;
            }
            [_photos addObject:data];
            
        }else if ([tem isKindOfClass:[NSString class]]) {
            YBIBImageData *data = [YBIBImageData new];
            data.imageURL = [NSURL URLWithString:tem];
            [_photos addObject:data];
        }else if ([tem isKindOfClass:[LLImagePickerModel class]]) {
            LLImagePickerModel *temModel = (LLImagePickerModel *)tem;
            if (temModel.isVideo) {
                YBIBVideoData *data = [YBIBVideoData new];

                if (temModel.mediaURL) {
                    data.videoURL = temModel.mediaURL;
                }else {
                    data.videoPHAsset = temModel.asset;
                }
                data.thumbImage = temModel.image;
                [_photos addObject:data];
            }else if (temModel.imageUrlString) {
                YBIBImageData *data = [YBIBImageData new];
                data.imageURL = [NSURL URLWithString:temModel.imageUrlString];
                [_photos addObject:data];
            }
        }else if ([tem isKindOfClass:[YXUploadModel class]]) {
            YXUploadModel *temModel = (YXUploadModel *)tem;
            YBIBImageData *data = [YBIBImageData new];
            data.image = ^UIImage * _Nullable{
                return temModel.fileImg;
            };
            [_photos addObject:data];
        }
    }
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.toolViewHandlers = nil;
    browser.dataSourceArray = _photos;
    browser.currentPage = currentInde;
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}


- (void)showAudioWithUrl:(NSString *)url {
    
    if (url.length > 0) {
        self.player.playUrl = url;
        [self.player play];
    }
    
}


@end
