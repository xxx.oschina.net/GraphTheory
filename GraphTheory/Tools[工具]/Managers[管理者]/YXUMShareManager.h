//
//  YXUMShareManager.h
//  DDLife
//
//  Created by 杨旭 on 2019/8/20.
//  Copyright © 2019年 点都. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMShare/UMShare.h>

@interface YXUMShareManager : NSObject


/** 初始化单列类 */
+ (instancetype)shared;


/// 友盟分享，带UI
/// @param thumbURL 缩略图
/// @param title 标题
/// @param description 描述
/// @param webpageUrl 分享的url
- (void)shareWebPageThumbURL:(NSString *)thumbURL
                       title:(NSString *)title
                 description:(NSString *)description
                  webpageUrl:(NSString *)webpageUrl;
/**
 友盟分享
 
 @param platformType    分享类型
 @param thumbURL        缩略图
 @param title           标题
 @param description     描述
 @param webpageUrl      分享的url
 */
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                          thumbURL:(NSString *)thumbURL
                             title:(NSString *)title
                       description:(NSString *)description
                        webpageUrl:(NSString *)webpageUrl;
- (void)shareMiniProgramthumbURL:(id)thumbURL
                           title:(NSString *)title
                     description:(NSString *)description
                      webpageUrl:(NSString *)webpageUrl
                        userName:(NSString *)userName
                            path:(NSString *)path
                 miniProgramType:(NSInteger)miniProgramType
                     hdImageData:(NSData *)hdImageData
                        isHaveUI:(BOOL)isHaveUI;

/// 分享小程序
/// @param thumbURL 图片
/// @param title 标题
/// @param description 详情
/// @param webpageUrl 网页链接
/// @param userName 小程序名
/// @param path 路径
/// @param miniProgramType 环境
/// @param hdImageData 预览图
- (void)shareMiniProgramthumbURL:(id)thumbURL
                           title:(NSString *)title
                     description:(NSString *)description
                      webpageUrl:(NSString *)webpageUrl
                        userName:(NSString *)userName
                            path:(NSString *)path
                 miniProgramType:(NSInteger)miniProgramType
                     hdImageData:(NSData *)hdImageData;


@end


