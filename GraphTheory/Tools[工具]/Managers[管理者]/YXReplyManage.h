//
//  YXReplyManage.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXReplyManage : NSObject

+ (YXReplyManage *)shareReplyManage;


// 保存快捷回复信息
- (void)saveReplyList:(NSArray *)list;

// 查询快捷回复信息
- (NSMutableArray *)getReplyList;

@end

NS_ASSUME_NONNULL_END
