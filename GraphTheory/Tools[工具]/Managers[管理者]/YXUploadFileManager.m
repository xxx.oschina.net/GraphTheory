//
//  YXUploadFileManager.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/31.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "YXUploadFileManager.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <CoreMedia/CoreMedia.h>
#import <CoreServices/CoreServices.h>
#import "VideoEditVC.h"
#import "YXFileModel.h"
#define PHOTOCACHEPATH [NSTemporaryDirectory() stringByAppendingPathComponent:@"photoCache"]
#define VIDEOCACHEPATH [NSTemporaryDirectory() stringByAppendingPathComponent:@"videoCache"]
@interface YXUploadFileManager ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIVideoEditorControllerDelegate>

@property (nonatomic ,strong) UIImagePickerController *imagePicker;

@property (nonatomic ,strong) NSMutableArray *uploadArray;
@end

@implementation YXUploadFileManager

+ (YXUploadFileManager *)shareUploadManager {
    static YXUploadFileManager *uploadManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        uploadManager = [[self alloc]init];
    });
    return uploadManager;
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

- (NSMutableArray *)uploadArray {
    if (!_uploadArray) {
        _uploadArray = [NSMutableArray array];
    }
    return _uploadArray;
}


- (void)setDuration:(NSInteger)duration {
    _duration = duration;
}


- (void)actionPhotoWithBlock:(SelectFileBlock)block{
    
    YXWeakSelf
    UIAlertController *alertController  = \
    [UIAlertController alertControllerWithTitle:@""
                                        message:@"上传照片"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 判断是否iPad
    if ([self getIsIpad]) {
        alertController.popoverPresentationController.sourceView =  [UIApplication sharedApplication].keyWindow.rootViewController.view;
        alertController.popoverPresentationController.sourceRect = CGRectMake(0,0,1.0,1.0);
    }
    
    UIAlertAction *photoAction  = \
    [UIAlertAction actionWithTitle:@"从相册选择"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               weakSelf.selectFileBlock = block;
                            
                               NSLog(@"从相册选择");
                               weakSelf.imagePicker.sourceType    = UIImagePickerControllerSourceTypePhotoLibrary;
                               weakSelf.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                               weakSelf.imagePicker.allowsEditing = NO;
                               
                                weakSelf.imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
                                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:weakSelf.imagePicker animated:YES completion:nil];
                               
                           }];
    
    UIAlertAction *cameraAction = \
    [UIAlertAction actionWithTitle:@"拍照"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               weakSelf.selectFileBlock = block;

                               NSLog(@"拍照");
                               if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                   
                                   weakSelf.imagePicker.sourceType        = UIImagePickerControllerSourceTypeCamera;
                                   weakSelf.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
                                   weakSelf.imagePicker.cameraDevice      = UIImagePickerControllerCameraDeviceRear;
                                   weakSelf.imagePicker.allowsEditing     = NO;
                                   weakSelf.imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;

                                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:weakSelf.imagePicker animated:YES completion:nil];
                              
                               }
                           }];
    
    UIAlertAction *cancelAction = \
    [UIAlertAction actionWithTitle:@"取消"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               NSLog(@"取消");
                           }];
    
    [alertController addAction:photoAction];
    [alertController addAction:cameraAction];
    [alertController addAction:cancelAction];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    
}

- (void)actionVideoWithDuration:(NSInteger)duration Block:(SelectFileBlock)block {
    
    
    _duration = duration;
    
    YXWeakSelf
    UIAlertController *alertController = \
    [UIAlertController alertControllerWithTitle:@""
                                        message:@"上传视频"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    // 判断是否iPad
    if ([self getIsIpad]) {
        alertController.popoverPresentationController.sourceView =  [UIApplication sharedApplication].keyWindow.rootViewController.view;
        alertController.popoverPresentationController.sourceRect = CGRectMake(0,0,1.0,1.0);
    }
    
    UIAlertAction *photoAction = \
    [UIAlertAction actionWithTitle:@"从视频库选择"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               weakSelf.selectFileBlock = block;
                               
                               NSLog(@"从视频库选择");
                               weakSelf.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                               weakSelf.imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
                               weakSelf.imagePicker.allowsEditing = NO;
                               weakSelf.imagePicker.editing =NO;
                                weakSelf.imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
                                [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:weakSelf.imagePicker animated:YES completion:nil];
                           }];
    
    UIAlertAction *cameraAction = \
    [UIAlertAction actionWithTitle:@"录像"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               weakSelf.selectFileBlock = block;

                               NSLog(@"录像");
                               weakSelf.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                               weakSelf.imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
                               weakSelf.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
                               weakSelf.imagePicker.videoQuality = UIImagePickerControllerQualityType640x480;
                               weakSelf.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
                               weakSelf.imagePicker.allowsEditing = YES;
                               
                               // 设置录制视频最大时间
                               weakSelf.imagePicker.videoMaximumDuration = 15;
                                weakSelf.imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
                               [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:weakSelf.imagePicker animated:YES completion:nil];
                           }];
    
    UIAlertAction *cancelAction = \
    [UIAlertAction actionWithTitle:@"取消"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction * _Nonnull action) {
                               
                               NSLog(@"取消");
                           }];
    
    [alertController addAction:photoAction];
    [alertController addAction:cameraAction];
    [alertController addAction:cancelAction];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}



//将Image保存到缓存路径中
- (void)saveImage:(UIImage *)image toCachePath:(NSString *)path {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:PHOTOCACHEPATH]) {
        
        NSLog(@"路径不存在, 创建路径");
        [fileManager createDirectoryAtPath:PHOTOCACHEPATH
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    } else {
        
        NSLog(@"路径存在");
    }
    
    //[UIImagePNGRepresentation(image) writeToFile:path atomically:YES];
    [UIImageJPEGRepresentation(image, 1) writeToFile:path atomically:YES];
}

//将视频保存到缓存路径中
- (void)saveVideoFromPath:(NSString *)videoPath toCachePath:(NSString *)path {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:VIDEOCACHEPATH]) {
        
        NSLog(@"路径不存在, 创建路径");
        [fileManager createDirectoryAtPath:VIDEOCACHEPATH
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    } else {
        
        NSLog(@"路径存在");
    }
    
    NSError *error;
    [fileManager copyItemAtPath:videoPath toPath:path error:&error];
    if (error) {
        
        NSLog(@"文件保存到缓存失败");
    }
}

//从缓存路径获取照片
- (UIImage *)getImageFromPath:(NSString *)path {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        
        return [UIImage imageWithContentsOfFile:path];
    }
    
    return nil;
}

//以当前时间合成图片名称
- (NSString *)getImageNameBaseCurrentTime {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH-mm-ss"];
    
    return [[dateFormatter stringFromDate:[NSDate date]] stringByAppendingString:@".JPG"];
}

//以当前时间合成视频名称
- (NSString *)getVideoNameBaseCurrentTime {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH-mm-ss"];
    
    return [[dateFormatter stringFromDate:[NSDate date]] stringByAppendingString:@".MOV"];
}


//获取视频的第一帧截图, 返回UIImage
//需要导入AVFoundation.h
- (UIImage*) getVideoPreViewImageWithPath:(NSURL *)videoPath
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];
    
    AVAssetImageGenerator *gen         = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    
    CMTime time      = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error   = nil;
    
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img     = [[UIImage alloc] initWithCGImage:image];
    
    return img;
}


// 获取视频的时长
- (NSInteger)getVideoTimeByUrlString:(NSString*)urlString {
    
    NSURL*videoUrl = [NSURL URLWithString:urlString];
    
    AVURLAsset *avUrl = [AVURLAsset assetWithURL:videoUrl];
    
    CMTime time = [avUrl duration];
    
    int seconds = ceil(time.value/time.timescale);
    
    return seconds;
    
}

// 获取视频文件的大小
- (NSInteger)getVideoDataLengthByUrlString:(NSString*)urlString {
    
    NSInteger length = 0;
    
    NSURL*videoUrl = [NSURL URLWithString:urlString];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL:videoUrl];
    
    NSArray *arr = [asset tracksWithMediaType:AVMediaTypeVideo];// 项目中是明确媒体类型为视频，其他没试过
    
    for (AVAssetTrack *track in arr) {
        length = track.totalSampleDataLength;
        NSLog(@"视频文件大小---:%lldkb  %lld Mb",track.totalSampleDataLength/1024 ,track.totalSampleDataLength/(1024*1024));// 视频文件字节大小
    }
    return length;
    
}


#pragma mark - UIImagePickerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    [self.uploadArray removeAllObjects];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    //获取用户选择或拍摄的是照片还是视频
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
//        //获取编辑后的照片
//        NSLog(@"获取编辑后的好片");
//        UIImage *tempImage = info[UIImagePickerControllerEditedImage];
        
        //获取原图的照片
        NSLog(@"获取编辑后的照片");
        UIImage *tempImage = info[UIImagePickerControllerOriginalImage];
        
        //将照片存入相册
        if (tempImage) {
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                
                //将照片存入相册
                NSLog(@"将照片存入相册");
                UIImageWriteToSavedPhotosAlbum(tempImage, self, nil, nil);
            }
            
            //获取图片名称
            NSLog(@"获取图片名称");
            NSString *imageName = [self getImageNameBaseCurrentTime];
            NSLog(@"图片名称: %@", imageName);
            
            //将图片存入缓存
            NSLog(@"将图片写入缓存");
            [self saveImage:tempImage
                toCachePath:[PHOTOCACHEPATH stringByAppendingPathComponent:imageName]];
            
            //创建uploadModel
            NSLog(@"创建model");
            YXUploadModel *model = [[YXUploadModel alloc] init];
            
            model.path       = [NSURL URLWithString:[PHOTOCACHEPATH stringByAppendingPathComponent:imageName]];
            model.name       = imageName;
            model.type       = @"image";
            model.isUploaded = NO;
            model.fileImg = tempImage;
            
            //将模型存入待上传数组
            NSLog(@"将Model存入待上传数组");
            [self.uploadArray addObject:model];
            
            if (self.selectFileBlock) {
                self.selectFileBlock(self.uploadArray);
            }
        }
    }
    
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {

            //如果是拍摄的视频, 则把视频保存在系统多媒体库中
            NSLog(@"video path: %@", info[UIImagePickerControllerMediaURL]);
            
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library writeVideoAtPathToSavedPhotosAlbum:info[UIImagePickerControllerMediaURL] completionBlock:^(NSURL *assetURL, NSError *error) {
                
                if (!error) {
                    
                    NSLog(@"视频保存成功");
                } else {
                    
                    NSLog(@"视频保存失败");
                }
            }];
            
        }
        
        //获取视频的url
        NSURL *videoAssetURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        //获取视频时长
//        NSInteger videoTime = [self getVideoTimeByUrlString:videoAssetURL.absoluteString];
//        NSLog(@"%ld",videoTime);
        
        //获取视频大小
        NSInteger videoSize = [self getVideoDataLengthByUrlString:videoAssetURL.absoluteString];
        NSLog(@"%ld",videoSize);

        //获取视频图片
        UIImage *fileImg  =  [self getVideoPreViewImageWithPath:videoAssetURL];
        
        //生成视频名称
        NSString *mediaName = [self getVideoNameBaseCurrentTime];
        NSLog(@"mediaName: %@", mediaName);
        
        //将视频存入缓存
//        NSLog(@"将视频存入缓存");
        [self saveVideoFromPath:info[UIImagePickerControllerMediaURL] toCachePath:[VIDEOCACHEPATH stringByAppendingPathComponent:mediaName]];
        
        
        //创建uploadmodel
        YXUploadModel *model = [[YXUploadModel alloc] init];
//        model.path       = [NSURL URLWithString:[VIDEOCACHEPATH stringByAppendingPathComponent:mediaName]];
        model.path       = videoAssetURL;
        model.name       = mediaName;
        model.type       = @"moive";
        model.isUploaded = NO;
        model.fileImg   = fileImg;
        
        //将model存入待上传数组
        [self.uploadArray addObject:model];
        
        if (self.selectFileBlock) {
            self.selectFileBlock(self.uploadArray);
        }
    }
        
    //[picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//如果想要判断设备是ipad，要用如下方法
- (BOOL)getIsIpad
{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"]) {
        //iPhone
        return NO;
    }
    else if([deviceType isEqualToString:@"iPod touch"]) {
        //iPod Touch
        return NO;
    }
    else if([deviceType isEqualToString:@"iPad"]) {
        //iPad
        return YES;
    }
    return NO;
    
}

@end
