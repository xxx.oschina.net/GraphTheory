//
//  YXUMShareManager.m
//  DDLife
//
//  Created by 杨旭 on 2019/8/20.
//  Copyright © 2019年 点都. All rights reserved.
//

#import "YXUMShareManager.h"
#import <UMShare/UMShare.h>
#import <UShareUI/UShareUI.h>
//#import "UIView+DDPosition.h"
#import <WXApi.h>

@implementation YXUMShareManager

+ (id)shared {
    static  dispatch_once_t once;
    static   id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (void)shareWebPageThumbURL:(NSString *)thumbURL title:(NSString *)title description:(NSString *)description webpageUrl:(NSString *)webpageUrl{
    //    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine),@(UMSocialPlatformType_QQ),@(UMSocialPlatformType_Qzone)]];
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine)]];
    
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        [self shareWebPageToPlatformType:platformType thumbURL:thumbURL title:title description:description webpageUrl:webpageUrl completion:^{
            
        }];
    }];
}

//友盟分享
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                          thumbURL:(NSString *)thumbURL
                             title:(NSString *)title
                       description:(NSString *)description
                        webpageUrl:(NSString *)webpageUrl {
    [self shareWebPageToPlatformType:platformType
                            thumbURL:thumbURL
                               title:title
                         description:description
                          webpageUrl:webpageUrl
                          completion:^{
        
    }];
}
- (void)shareMiniProgramthumbURL:(id)thumbURL
                           title:(NSString *)title
                     description:(NSString *)description
                      webpageUrl:(NSString *)webpageUrl
                        userName:(NSString *)userName
                            path:(NSString *)path
                 miniProgramType:(NSInteger)miniProgramType
                     hdImageData:(NSData *)hdImageData
                        isHaveUI:(BOOL)isHaveUI{
    if (isHaveUI) {
        [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession)]];
        
        [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
            [self shareMiniProgramthumbURL:thumbURL title:title description:description webpageUrl:webpageUrl userName:userName path:path miniProgramType:miniProgramType hdImageData:hdImageData];
        }];
    }else{
        [self shareMiniProgramthumbURL:thumbURL title:title description:description webpageUrl:webpageUrl userName:userName path:path miniProgramType:miniProgramType hdImageData:hdImageData];
        
    }
    
}
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType
                          thumbURL:(NSString *)thumbURL
                             title:(NSString *)title
                       description:(NSString *)description
                        webpageUrl:(NSString *)webpageUrl
                        completion:(void(^)(void))block {
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    if (webpageUrl) {
        //创建网页内容对象
        UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:description thumImage:[UIImage imageNamed:@"appicon"]];
        //设置网页地址
        shareObject.webpageUrl = webpageUrl;
        //分享消息对象设置分享内容对象
        messageObject.shareObject = shareObject;
    }else if(thumbURL){
        UMShareObject *shareObject = [UMShareObject shareObjectWithTitle:title descr:description thumImage:thumbURL];
        messageObject.shareObject = shareObject;
    }
    messageObject.text = description;
    
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        block();
        if (error) {
            
        } else {
            
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                NSLog(@"response message is %@",resp.message);
                //第三方原始返回的数据
                NSLog(@"response originalResponse data is %@",resp.originalResponse);
            }else{
                NSLog(@"response data is %@",data);
            }
        }
    }];
}
- (void)shareMiniProgramthumbURL:(id)thumbURL
                           title:(NSString *)title
                     description:(NSString *)description
                      webpageUrl:(NSString *)webpageUrl
                        userName:(NSString *)userName
                            path:(NSString *)path
                 miniProgramType:(NSInteger)miniProgramType
                     hdImageData:(NSData *)hdImageData{
    WXMiniProgramObject *object = [WXMiniProgramObject object];
    object.webpageUrl = webpageUrl;
    object.userName = userName;
    object.path = path;
    object.hdImageData = hdImageData;
    object.withShareTicket = YES;
    object.miniProgramType = miniProgramType;
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = title;
    message.description = description;
    message.thumbData = nil;  //兼容旧版本节点的图片，小于32KB，新版本优先
    //使用WXMiniProgramObject的hdImageData属性
    message.mediaObject = object;
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = WXSceneSession;  //目前只支持会话
    [WXApi sendReq:req completion:^(BOOL success) {
        
    }];
}
@end
