//
//  YXAMapManager.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/20.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXAMapManager.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "YXCityModel.h"
@interface YXAMapManager ()<AMapSearchDelegate,AMapLocationManagerDelegate>
@property (nonatomic,strong) AMapLocationManager *locationManager;//定位
@property (nonatomic,strong) AMapSearchAPI *search;//搜索
@property (nonatomic,strong) YXCitysModel *cityModel;
@end

@implementation YXAMapManager

+ (instancetype)shareLocation {
    static YXAMapManager *mapManage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mapManage = [[self alloc] init] ;
    });
    return mapManage;
}


- (void)getLocationInfo:(void(^)(YXCitysModel *cityModel))selectCityBlock {

    _selectCityBlock = selectCityBlock;
    
    DDWeakSelf
    self.locationManager = [[AMapLocationManager alloc] init];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    [self.locationManager setLocationTimeout:2];
    [self.locationManager setReGeocodeTimeout:2];
    self.locationManager.delegate = self;
    //带逆地理的单次定位
    [self.locationManager requestLocationWithReGeocode:YES
                                       completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
                                           
                                           if (error){
                                               NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
                                               if (error.code == AMapLocationErrorLocateFailed){
                                                   return;
                                               }
                                           }
                                           if (regeocode){
                                               NSLog(@"reGeocode:%@", regeocode.city);
                                               weakSelf.cityModel.address = regeocode.formattedAddress;
                                               weakSelf.cityModel.cityName = regeocode.city;
                                               weakSelf.cityModel.cityId = regeocode.citycode;
                                               weakSelf.cityModel.POIName = regeocode.POIName;
                                               weakSelf.cityModel.lat = location.coordinate.latitude;
                                               weakSelf.cityModel.lng = location.coordinate.longitude;
                                               AMapWeatherSearchRequest *request = [[AMapWeatherSearchRequest alloc] init];
                                               request.city = regeocode.city;
                                               request.type = AMapWeatherTypeLive;
                                               [weakSelf.search AMapWeatherSearch:request];
                                           }
                                       }];
}

- (void)queryWeatherWithCityName:(NSString *)cityName selectCityBlock:(void(^)(YXCitysModel *cityModel))selectCityBlock {
    _selectCityBlock = selectCityBlock;
    AMapWeatherSearchRequest *request = [[AMapWeatherSearchRequest alloc] init];
    request.city = cityName;
    request.type = AMapWeatherTypeLive;
    [self.search AMapWeatherSearch:request];
}

- (void)onWeatherSearchDone:(AMapWeatherSearchRequest *)request response:(AMapWeatherSearchResponse *)response
{
    //解析response获取天气信息，具体解析见 Demo
    AMapLocalWeatherLive *live = [response.lives firstObject];
    self.cityModel.weather = [NSString stringWithFormat:@"%@%@°C",live.weather,live.temperature];
    if (self.selectCityBlock) {
        self.selectCityBlock(self.cityModel);
    }
    
}

- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error);
}
- (void)amapLocationManager:(AMapLocationManager *)manager doRequireLocationAuth:(CLLocationManager *)locationManager {
    [locationManager requestAlwaysAuthorization];
}

#pragma mark - Lazy Loading
- (AMapSearchAPI *)search {
    if (!_search) {
        _search = [[AMapSearchAPI alloc] init];
        _search.delegate = self;
    }
    return _search;
}
- (YXCitysModel *)cityModel {
    if (!_cityModel) {
        _cityModel = [[YXCitysModel alloc] init];
    }
    return _cityModel;
}

@end
