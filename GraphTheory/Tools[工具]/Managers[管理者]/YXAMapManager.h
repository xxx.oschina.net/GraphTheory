//
//  YXAMapManager.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/20.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class YXCitysModel;
typedef void (^selectCityBlock) (YXCitysModel *cityModel);
@interface YXAMapManager : NSObject

+ (instancetype)shareLocation;

@property (nonatomic, copy) selectCityBlock selectCityBlock;

// 查询定位城市信息
- (void)getLocationInfo:(void(^)(YXCitysModel *cityModel))selectCityBlock;

// 根据城市名称查询天气
- (void)queryWeatherWithCityName:(NSString *)cityName selectCityBlock:(void(^)(YXCitysModel *cityModel))selectCityBlock;

@end

NS_ASSUME_NONNULL_END
