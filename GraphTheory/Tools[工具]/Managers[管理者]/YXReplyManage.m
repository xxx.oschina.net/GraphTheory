//
//  YXReplyManage.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/4/4.
//  Copyright © 2020 杨旭. All rights reserved.
//

#import "YXReplyManage.h"

#define kReplyList  @"replyList"


@implementation YXReplyManage

+ (YXReplyManage *)shareReplyManage {
    static YXReplyManage *replyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        replyManager = [[self alloc]init];
    });
    return replyManager;
}

- (void)saveReplyList:(NSArray *)list {

    if (list.count > 0) {
        NSString *str = [list mj_JSONString];
        [[NSUserDefaults standardUserDefaults] setValue:str forKey:kReplyList];
    }else {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kReplyList];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSMutableArray *)getReplyList {
    
    NSMutableArray *tempArr = [NSMutableArray array];
    NSMutableArray *cacheArr = [NSMutableArray arrayWithObjects:@"亲，在的呢，请问有什么可以帮您的呢",[NSString stringWithFormat:@"您好~ 欢迎光临本店，请问您观看中哪些宝贝？我可以帮你介绍一下~ 我是客服“%@”~",[YXUserInfoManager getUserInfo].nike],@"亲~ 不好意思，现在忙于打包发货中，有事请留言，晚点给你回复，谢谢！",@"亲，感谢您购置我们的产品，合做愉快，欢迎下次光临。", nil];
    NSMutableArray *listArr = [NSMutableArray arrayWithObjects:tempArr,cacheArr, nil];
    if ([[NSUserDefaults standardUserDefaults]valueForKey:kReplyList]) {
        NSString *str = [[NSUserDefaults standardUserDefaults]valueForKey:kReplyList];
        if (str.length > 0) {
            listArr = [str mj_JSONObject];
        }else {
            listArr = listArr;
        }
    }
    return [listArr mutableCopy];
}


@end
