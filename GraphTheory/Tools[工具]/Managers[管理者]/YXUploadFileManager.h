//
//  YXUploadFileManager.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/31.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SelectFileBlock) (NSMutableArray *uploadArray);
@interface YXUploadFileManager : NSObject

+ (YXUploadFileManager *)shareUploadManager;

@property (nonatomic, copy) SelectFileBlock selectFileBlock;


/** 广告录制时长*/
@property (nonatomic, assign) NSInteger duration;

/** 选择图片*/
- (void)actionPhotoWithBlock:(SelectFileBlock)block;

/** 选择视频*/
- (void)actionVideoWithDuration:(NSInteger)duration Block:(SelectFileBlock)block;



@end

