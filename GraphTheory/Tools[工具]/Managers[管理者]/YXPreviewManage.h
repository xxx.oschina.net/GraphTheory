//
//  YXPreviewManage.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXPreviewManage : NSObject

+ (YXPreviewManage *)sharePreviewManage;


// 预览视频
- (void)showVideoWithUrl:(NSString *)url withController:(UIViewController *)controller;

// 预览音频
- (void)showAudioWithUrl:(NSString *)url;

// 预览图片
- (void)showPhotoWithImgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentInde;



@end

NS_ASSUME_NONNULL_END
