//
//  WGPopUpTool.h
//  BusinessFine
//
//  Created by wanggang on 2019/10/17.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WGPopUpTool : NSObject

+ (instancetype)sharedInstance;

- (void)popUpWithView:(UIView *)view subView:(UIView *)subView withAnimation:(BOOL)animation withCanTapBgCancel:(Boolean)canTapBgCancel;
- (void)removePopUpView;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
