//
//  NetWorkTools.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking.h>

@interface NetWorkTools : NSObject

+ (instancetype)sharedTools;
+ (void)pushLoginVC;

/**
 *  GET 网络请求
 *
 *  @param url          请求的网址
 *  @param parameters   请求需要的参数
 *  @param success      请求成功的回调
 *  @param conError     请求失败的回调
 */
+ (void)getWithUrl:(NSString *)url parameters:(id)parameters success:(void(^)(id responseObj))success failure:(void(^)(NSError *error))conError;

/**
 *  POST 网络请求
 *
 *  @param url          请求的网址
 *  @param parameters   请求需要的参数
 *  @param success      请求成功的回调
 *  @param conError     请求失败的回调
 */
+ (void)postWithUrl:(NSString *)url parameters:(id)parameters success:(void(^)(id responseObj))success failure:(void(^)(NSError *error))conError;

/**
 POST body请求

 @param url 请求的网址
 @param body 请求需要的参数
 @param success 请求成功的回调
 @param conError 请求成功的回调
 */
+ (void)postWithUrl:(NSString *)url body:(NSData *)body  success:(void(^)(id responseObj))success failure:(void(^)(NSError *error))conError;

/**
 上传图片
 
 @param url        请求上传的地址
 @param parameters 参数
 @param imageArray 图片数组
 @param success    成功回调
 @param conError   失败回调
 */
+ (void)uploadImagesPOSTWithUrl:(NSString *)url parameters:(id)parameters images:(NSArray *)imageArray success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError;



/**
 上传base64图片
 
 @param url        请求上传的地址
 @param parameters 参数
 @param images      图片
 @param success    成功回调
 @param conError   失败回调
 */
+ (void)uploadImagesBase64POSTWithUrl:(NSString *)url parameters:(id)parameters images:(UIImage *)images success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError;


/**
 上传base64视频
 
 @param url        请求上传的地址
 @param parameters 参数
 @param fileData    二进制流文件
 @param mimeType    文件格式
 @param success    成功回调
 @param conError   失败回调
 */
+ (void)uploadVideoBase64POSTWithUrl:(NSString *)url parameters:(id)parameters FileData:(NSData *)fileData  MimeType:(NSString *)mimeType success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError;



/**
 上传音频文件

 @param url         请求上传的地址
 @param parameters  参数
 @param fileData    二进制流文件
 @param fileName    文件名
 @param mimeType    文件格式
 @param success     成功回调
 @param conError    失败回调
 */
+ (void)uploadAudioPOSTWithUrl:(NSString *)url parameters:(id)parameters FileData:(NSData *)fileData FileName:(NSString *)fileName MimeType:(NSString *)mimeType success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError;
@end

