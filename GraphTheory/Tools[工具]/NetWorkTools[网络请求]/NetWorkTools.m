//
//  NetWorkTools.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/9/24.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "NetWorkTools.h"
#import "DDBaseNavigationController.h"
#import "WGLoginViewController.h"
#import "NSDictionary+DeleteNull.h"
#import "YXDataTimeTool.h"
#import <CYLTabBarController.h>
//#import "WGIMManage.h"
@implementation NetWorkTools

+ (instancetype)sharedTools {
    static NetWorkTools *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

+ (AFHTTPSessionManager *)sharedNetwork{

    static AFHTTPSessionManager *manager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{

        manager = [AFHTTPSessionManager manager];

        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        //过滤value为空的键值
//        [serializer setRemovesKeysWithNullValues:YES];

        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",
                                             @"text/plain",
                                             @"application/json",
                                             @"image/jpeg",
                                             @"image/png",
                                             @"application/octet-stream",
                                             @"text/json",
                                             @"text/javascript", nil];
        //JSON请求
        AFJSONRequestSerializer *request = [AFJSONRequestSerializer serializer];
        manager.requestSerializer = request;

        manager.requestSerializer.timeoutInterval = 10.f;

    });

    return manager;
}

/**
 *  GET 网络请求
 *
 *  @param url              请求的网址
 *  @param parameters       请求需要的参数
 *  @param success          请求成功的回调
 *  @param conError         请求失败的回调
 */
+ (void)getWithUrl:(NSString *)url parameters:(id)parameters success:(void(^)(id responseObj))success failure:(void(^)(NSError *error))conError {
    
    //创建一个网络请求管理对象
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    //设置可接受的数据类型
    manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",
                                                        @"text/plain",
                                                        @"application/json",
                                                        @"image/jpeg",
                                                        @"image/png",
                                                        @"application/octet-stream",
                                                        @"text/json",
                                                        @"text/javascript", nil];
    manager.requestSerializer.timeoutInterval = 10.f;
    
    
    // 公共参数
    if ([parameters objectForKey:@"mobile"]==nil) {
        [parameters setValue:[YXUserInfoManager getUserInfo].mobile forKey:@"mobile"];
    }
    if ([parameters objectForKey:@"userId"]==nil) {
        [parameters setValue:[YXUserInfoManager getUserInfo].user_id forKey:@"userId"];
    }
    if ([parameters objectForKey:@"userName"]==nil) {
        [parameters setValue:[YXUserInfoManager getUserInfo].name forKey:@"userName"];
    }
    [parameters setValue:[YXUserInfoManager getUserInfo].adminMobile forKey:@"phoneumber"];
    
    [parameters setValue:[YXUserInfoManager getUserInfo].token forKey:@"user_token"];
    
    YXWeakSelf
    //get请求头 的设置
    // 将token存到请求头中
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];
    NSString    *timeStamp = [YXDataTimeTool currentTimeStr];
    // 将requestTime存到请求头中
    [manager.requestSerializer setValue:timeStamp forHTTPHeaderField:@"requestTime"];
    
    //进行MD5转换
    NSString *signString = [NSString getSignStringFromDic:parameters];
    NSString *signKey = [NSString stringWithFormat:@"%@&key=%@",signString,@"FA16D075B8C3A51A9F8772E8C3EF1216"];
    NSString *signMd5 = [NSString getMD5StringFromStr:signKey];
    // 将sign存到请求头中
    [manager.requestSerializer setValue:signMd5 forHTTPHeaderField:@"sign"];

    //开始网络请求
    [manager GET:url parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //请求数据成功  回调数据
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //请求数据失败, 回调错误原因
//        NSHTTPURLResponse *responses = (NSHTTPURLResponse *)task.response;
//        if (responses.statusCode == 401) {
//            [YJProgressHUD showError:@"您的账号在别处登录，请重新登录"];
//            [weakSelf pushLoginVC];
//        }else {
            conError(error);
//        }
    }];
    
    
}



/**
 *  POST 网络请求
 *
 *  @param url            请求的网址
 *  @param parameters     请求需要的参数
 *  @param success        请求成功的回调
 *  @param conError       请求失败的回调
 */
+ (void)postWithUrl:(NSString *)url parameters:(id)parameters success:(void(^)(id responseObj))success failure:(void(^)(NSError *error))conError {
    
    //创建一个网络请求管理对象
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //设置可接受的数据类型
    manager.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"text/html",
                                                        @"text/plain",
                                                        @"application/json",
                                                        @"image/jpeg",
                                                        @"image/png",
                                                        @"application/octet-stream",
                                                        @"text/json",
                                                        @"text/javascript", nil];
    manager.requestSerializer.timeoutInterval = 10.f;

//    // 公共参数
//    if ([parameters objectForKey:@"mobile"]==nil) {
//        [parameters setValue:[YXUserInfoManager getUserInfo].mobile forKey:@"mobile"];
//    }
//    if ([parameters objectForKey:@"user_id"]==nil) {
//        [parameters setValue:[YXUserInfoManager getUserInfo].user_id forKey:@"user_id"];
//    }
//    if ([parameters objectForKey:@"userName"]==nil) {
//        [parameters setValue:[YXUserInfoManager getUserInfo].name forKey:@"userName"];
//    }
  
    [parameters setValue:[YXUserInfoManager getUserInfo].token forKey:@"user_token"];
    
    
    //post请求头 的设置
    // 将token存到请求头中
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];
    NSString    *timeStamp = [YXDataTimeTool currentTimeStr];
    // 将requestTime存到请求头中
    [manager.requestSerializer setValue:timeStamp forHTTPHeaderField:@"requestTime"];
    
    //进行MD5转换
    NSString *signString = [NSString getSignStringFromDic:parameters];
    NSString *signKey = [NSString stringWithFormat:@"%@&key=%@",signString,@"FA16D075B8C3A51A9F8772E8C3EF1216"];
    NSString *signMd5 = [NSString getMD5StringFromStr:signKey];
    // 将sign存到请求头中
    [manager.requestSerializer setValue:signMd5 forHTTPHeaderField:@"sign"];

    YXWeakSelf
    //开始网络请求
    [manager POST:url parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        responseObject = [NSDictionary changeType:responseObject];
        //请求数据成功  回调数据
        if ([responseObject[@"code"] integerValue] == -104 ||
            [responseObject[@"code"] integerValue] == -102) {
            [weakSelf pushLoginVC];
            [YJProgressHUD showError:responseObject[@"msg"]];
        }else {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            conError(error);
    }];
    
}

+ (void)postWithUrl:(NSString *)url body:(NSData *)body success:(void (^)(id))success failure:(void (^)(NSError *))conError{
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:nil error:nil];
    request.timeoutInterval= 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"user_token"];

    // 设置body
    [request setHTTPBody:body];
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",
                                                 @"text/plain",
                                                 @"application/json",
                                                 @"image/jpeg",
                                                 @"image/png",
                                                 @"application/octet-stream",
                                                 @"text/json",
                                                 @"text/javascript", nil];
    manager.responseSerializer = responseSerializer;
//    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];// 将token存到请求头中

    [[manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            responseObject = [NSDictionary changeType:responseObject];
            //请求数据成功  回调数据
            success(responseObject);
            
        } else {
            conError(error);
        }
    }] resume];
    
}

/**
 上传图片
 
 @param url        请求上传的地址
 @param parameters 参数
 @param imageArray 图片数组
 @param success    成功回调
 @param conError   失败回调
 */
+ (void)uploadImagesPOSTWithUrl:(NSString *)url parameters:(id)parameters images:(NSArray *)imageArray success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError {
    
    
    //向服务器提交图片
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/javascript",@"text/html", nil];
    
    manager.requestSerializer.timeoutInterval = 60.f;
    
    //post请求头 的设置
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];// 将token存到请求头中
    
    //显示进度
    [manager POST:url parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        //上传多张照片
        for (NSInteger i = 0; i < imageArray.count; i++) {
            
//            NSData *imageData = UIImagePNGRepresentation([imageArray objectAtIndex:i]);
            NSData *imageData = [imageArray[0] compressWithMaxLength:1024 * 1024 * 15];
            //根据当前时间设置上传的名字
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //设置日期格式
            formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            //上传的参数名
            NSString *name = [NSString stringWithFormat:@"%@%zi", [NSDate date], i+1];
            //上传文件名字fileName
            NSString *fileName = [NSString stringWithFormat:@"%@.jpeg", name];
            NSLog(@"-----文件名-----%@",fileName);
            [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpeg"];
        }
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"错误 %@", error.localizedDescription);
        if (conError) {
            conError(error);
        }
        
    }];
    
}


/**
 上传base64图片
 
 @param url        请求上传的地址
 @param parameters 参数
 @param images      图片
 @param success    成功回调
 @param conError   失败回调
 */
+ (void)uploadImagesBase64POSTWithUrl:(NSString *)url parameters:(id)parameters images:(UIImage *)images success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError {
    
    //向服务器提交图片
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    //    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/javascript",@"text/html", nil];
    manager.requestSerializer.timeoutInterval = 10.f;
    
    //post请求头 的设置
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"user_token"];// 将token存到请求头中
    
    NSData *imageData = UIImageJPEGRepresentation(images, 0.6);
    NSString *dataStr = [imageData base64EncodedStringWithOptions:0];
    [parameters setValue:dataStr forKey:@"base64_img"];

    [parameters setValue:[YXUserInfoManager getUserInfo].token forKey:@"user_token"];

    //显示进度
    [manager POST:url parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"错误 %@", error.localizedDescription);
        if (conError) {
            conError(error);
        }
    }];

}


/**
 上传base64视频
 
 @param url         请求上传的地址
 @param parameters  参数
 @param fileData    二进制流文件
 @param mimeType    文件格式
 @param success     成功回调
 @param conError    失败回调
 */
+ (void)uploadVideoBase64POSTWithUrl:(NSString *)url parameters:(id)parameters FileData:(NSData *)fileData  MimeType:(NSString *)mimeType success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError {
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/javascript",@"text/html", nil];
    manager.requestSerializer.timeoutInterval = 10.f;
    //post请求头 的设置
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"user_token"];// 将token存到请求头中
    
    NSString *dataStr = [fileData base64EncodedStringWithOptions:0];
    [parameters setValue:dataStr forKey:@"base64_video"];
    [parameters setValue:[YXUserInfoManager getUserInfo].token forKey:@"user_token"];
    [parameters setValue:mimeType forKey:@"mime_type"];

    [manager POST:url parameters:parameters headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"错误 %@", error.localizedDescription);
        if (conError) {
            conError(error);
        }
    }];
    
}



/**
 上传音频文件
 
 @param url         请求上传的地址
 @param parameters  参数
 @param fileData    二进制流文件
 @param fileName    文件名
 @param mimeType    文件格式
 @param success     成功回调
 @param conError    失败回调
 */
+ (void)uploadAudioPOSTWithUrl:(NSString *)url parameters:(id)parameters FileData:(NSData *)fileData FileName:(NSString *)fileName MimeType:(NSString *)mimeType success:(void(^)(id responesObj))success failure:(void(^)(NSError *error))conError {
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/html",@"text/javascript",@"text/html", nil];
    
    manager.requestSerializer.timeoutInterval = 10.f;
    
    //post请求头 的设置
    [manager.requestSerializer setValue:[YXUserInfoManager getUserInfo].token forHTTPHeaderField:@"token"];// 将token存到请求头中
    
    [manager POST:url parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //123.amr 约定啊
//        [formData appendPartWithFileData:amrData name:@"file" fileName:@"open.mp3" mimeType:@"audio/mp3"];
        [formData appendPartWithFileData:fileData name:@"file" fileName:fileName mimeType:mimeType];

    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (conError) {
            conError(error);
        }
    }];
 
}
- (void)cancelRequest
{
//    if ([manager.tasks count] > 0) {
//        NSLog(@"返回时取消网络请求");
//        [manager.tasks makeObjectsPerformSelector:@selector(cancel)];
//        //NSLog(@"tasks = %@",manager.tasks);
//    }
}

#pragma mark - 验证token过期跳转登录页面
+ (void)pushLoginVC {
    
    //清空用户信息
    [YXUserInfoManager cleanUserInfo];
    
    // 返回登录页面
    WGLoginViewController *vc = [[WGLoginViewController alloc] init];

    vc.isPresent = YES;
    DDBaseNavigationController *nav = [[DDBaseNavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[self cyl_tabBarController] presentViewController:nav animated:YES completion:nil];

}



@end
