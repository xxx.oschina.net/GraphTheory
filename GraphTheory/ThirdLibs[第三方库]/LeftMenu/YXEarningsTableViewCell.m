//
//  YXEarningsTableViewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/20.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXEarningsTableViewCell.h"

@implementation YXEarningsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.backgroundColor = APPTintColor;
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.cornerRadius = 4.0;
    self.backView.userInteractionEnabled = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
