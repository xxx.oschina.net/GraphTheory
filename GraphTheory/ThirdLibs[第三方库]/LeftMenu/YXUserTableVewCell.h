//
//  YXUserTableVewCell.h
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/20.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YXUserTableVewCell : UITableViewCell

/** 背景*/
@property (nonatomic ,strong) UIView *backView;
/** 用户头像*/
@property (nonatomic ,strong) UIImageView *userImgView;
/** 用户昵称*/
@property (nonatomic ,strong) UILabel *nickNameLab;
/** 用户等级*/
@property (nonatomic ,strong) UIView *levelView;
/** 用户等级*/
@property (nonatomic ,strong) UILabel *levelLab;

@end

NS_ASSUME_NONNULL_END
