//
//  YXUserTableVewCell.m
//  GraphTheory
//
//  Created by 杨旭 on 2021/6/20.
//  Copyright © 2021 杨旭. All rights reserved.
//

#import "YXUserTableVewCell.h"

@implementation YXUserTableVewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selected = UITableViewCellSelectionStyleNone;
        [self setUpLayout];
    }
    return self;
}

/**
 页面布局
 */
- (void)setUpLayout{

        
    [self addSubview:self.backView];
    [self.backView addSubview:self.userImgView];
    [self.backView addSubview:self.nickNameLab];
    [self.backView addSubview:self.levelView];
    [self.levelView addSubview:self.levelLab];

    
    YXWeakSelf
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(weakSelf);
        make.height.equalTo(@80);
    }];
    
    [self.userImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.backView.mas_top).offset(35);
        make.left.equalTo(weakSelf.backView.mas_left).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [self.nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.userImgView.mas_right).offset(15);
        make.top.equalTo(weakSelf.userImgView.mas_top).offset(0);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    
    [self.levelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.userImgView.mas_bottom).offset(-2);
        make.left.equalTo(self.userImgView.mas_right).offset(15);
    }];
    [self.levelLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.levelView).mas_offset(UIEdgeInsetsMake(2, 5, 2, 5));
    }];
    
}

#pragma mark - Lozy Loading
- (UIView *)backView {
    if (!_backView) {
        _backView = [UIView new];
        _backView.backgroundColor = [UIColor whiteColor];
    }
    return _backView;
}

- (UIImageView *)userImgView {
    if (!_userImgView) {
        _userImgView = [UIImageView new];
        _userImgView.layer.masksToBounds = YES;
        _userImgView.layer.cornerRadius = 20.0;
        _userImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _userImgView.layer.borderWidth = 1.0f;
        _userImgView.userInteractionEnabled = YES;
        _userImgView.image = [UIImage imageNamed:@"mine_head_default"];
    }
    return _userImgView;
}

- (UILabel *)nickNameLab {
    if (!_nickNameLab) {
        _nickNameLab = [[UILabel alloc] init];
        _nickNameLab.text = @"会飞的鱼";
        _nickNameLab.textColor = [UIColor blackColor];
        _nickNameLab.font = [UIFont systemFontOfSize:12.0f];
    }
    return _nickNameLab;
}
- (UIView *)levelView{
    if (!_levelView) {
        _levelView = [[UIView alloc]init];
//        _levelView.layer.borderColor = APPTintColor.CGColor;
//        _levelView.layer.borderWidth = 1.0f;
        _levelView.layer.masksToBounds = YES;
        _levelView.layer.cornerRadius = 4;
        _levelView.backgroundColor = APPTintColor;
    }
    return _levelView;
}
- (UILabel *)levelLab {
    if (!_levelLab) {
        _levelLab = [[UILabel alloc] init];
        _levelLab.text = @"普通会员";
        _levelLab.font = [UIFont systemFontOfSize:10.0f];
        _levelLab.textColor = [UIColor whiteColor];
        _levelLab.textAlignment = NSTextAlignmentCenter;
    }
    return _levelLab;
}



@end
