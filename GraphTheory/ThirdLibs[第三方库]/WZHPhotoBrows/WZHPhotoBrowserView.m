//
//  WZHPhotoBrowserView.m
//  PhotoBrowserTest
//
//  Created by 东亨 on 2017/10/11.
//  Copyright © 2017年 东亨. All rights reserved.
//

#import "WZHPhotoBrowserView.h"
#import "ImageItemCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "XLPhotoBrowser.h"
@interface WZHPhotoBrowserView()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,XLPhotoBrowserDelegate,XLPhotoBrowserDatasource>
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)CGFloat height;

@end
@implementation WZHPhotoBrowserView
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _width = self.bounds.size.width;
        _height = self.bounds.size.height;
        [self setupUI];
    }
    return self;
}

-(void)setupUI
{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    // 设置列的最小间距
        flowLayout.minimumInteritemSpacing = 10;
    // 设置最小行间距
    //        flowLayout.minimumLineSpacing = 5;
    // 设置布局的内边距
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    // 滚动方向
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.backgroundColor = [UIColor clearColor];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    [_collectionView registerNib:[UINib nibWithNibName:@"ImageItemCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ImageItemCollectionViewCell"];
    [self addSubview:_collectionView];
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self);
    }];
}
// 每个分区多少个item
- (NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return  CGSizeMake((_height<20?20:_height)-20, (_height<20?20:_height)-20);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImageItemCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageItemCollectionViewCell" forIndexPath:indexPath];
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:_dataArray[indexPath.row]]];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self toXLPhotoBrowser:indexPath.row];
}
-(void)toXLPhotoBrowser:(NSInteger)currentIndex
{
    //快速创建并进入浏览模式
    XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithCurrentImageIndex:currentIndex imageCount:_dataArray.count datasource:self];
    browser.browserStyle = XLPhotoBrowserStyleIndexLabel;
    // 设置长按手势弹出的地步ActionSheet数据,不实现此方法则没有长按手势
    [browser setActionSheetWithTitle:nil delegate:self cancelButtonTitle:nil deleteButtonTitle:nil otherButtonTitles:@"保存图片",nil];
}
#pragma mark    -   XLPhotoBrowserDatasource
- (NSURL *)photoBrowser:(XLPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{//URL图片
    return _dataArray[index];
}
#pragma mark    -   XLPhotoBrowserDelegate 长按ActionSheet点击事件
- (void)photoBrowser:(XLPhotoBrowser *)browser clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex
{
    switch (actionSheetindex) {
        case 0: // 保存
        {
            NSLog(@"保存");
            [browser saveCurrentShowImage];
        }
            break;
        default:
        {
            
        }
            break;
    }
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _height = frame.size.height;
    [_collectionView reloadData];
}
-(void)relodeData
{
    [_collectionView reloadData];
}

@end
