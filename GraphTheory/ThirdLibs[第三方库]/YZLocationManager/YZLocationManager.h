//
//  YZLocationManager.h
//  YZLocationManager
//
//  Created by 叶志强 on 2017/2/20.
//  Copyright © 2017年 CancerQ. All rights reserved.
//

/**
 * 推荐使用单例来创建该类
 *
 * 推荐使用单例来创建该类
 *
 * 推荐使用单例来创建该类
 *
 * 如果要使用后台定位的就必须使用单例创建方法 否则会被释放
 *
 * 在使用单例创建的时候要注意 使用block的时候是否被重新赋值，如果重新被赋值将使用最后一次赋值 例子：
 * YZLocationManager *manager = [YZLocationManager sharedLocationManager];
 *  [manager setYZLocationGeocderAddress:^(NSString *address, NSUInteger error) {   <--block1
 *       ..........
 *  }];
 *  [manager setYZLocationGeocderAddress:^(NSString *address, NSUInteger error) {   <--block2
 *       ..........
 *  }];
 * 最终将在block2才会回调
 * 使用 - (void)geoCodeSearchWithCoorinate:(CLLocationCoordinate2D)coordinate address:(void (^)(NSString *address, NSUInteger error))address 方法是也要注意 后面的block是和YZLocationGeocderAddress是使用的同一个block
 *
 * 推荐使用单例来创建该类
 *
 * 推荐使用单例来创建该类
 *
 * 推荐使用单例来创建该类
 *
 * 如果同一个页面多处要求同时定位，本类或许将不适用
*/

#import <Foundation/Foundation.h>
#import <BMKLocationkit/BMKLocationComponent.h>


@import CoreLocation;

typedef void (^YZLocationCoordinate)(BMKLocation *location, NSError *error);
typedef void (^YZLocationCLHeading)(CLHeading *heading, NSError *error);


@interface YZLocationManager : NSObject

//获取经纬度
@property (nonatomic, copy) YZLocationCoordinate location;
@property (nonatomic, copy) YZLocationCLHeading locationHeading;

//最近一次定位的经纬度
@property (nonatomic, strong) BMKLocation *lastCoordinate;
//通过单例创建
+ (YZLocationManager *)sharedLocationManager;

//开始定位
- (void)startLocationService;

//停止定位
- (void)stopLocationService;


@end
