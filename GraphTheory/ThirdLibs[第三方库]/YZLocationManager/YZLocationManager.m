//
//  YZLocationManager.m
//  YZLocationManager
//
//  Created by 叶志强 on 2017/2/20.
//  Copyright © 2017年 CancerQ. All rights reserved.
//

#import "YZLocationManager.h"
#import <BMKLocationkit/BMKLocationComponent.h>
#import <BMKLocationKit/BMKLocationAuth.h>

@interface YZLocationManager ()<CLLocationManagerDelegate,BMKLocationManagerDelegate,BMKLocationAuthDelegate>
@property (nonatomic, strong) BMKLocationManager *locationManager;
@property (nonatomic, strong) CLLocationManager *CLLocationCoordinate;


@end

//为iOS8定位
static CLLocationManager *clLocationManager;
@implementation YZLocationManager

#pragma mark - Lifecycle (生命周期)

+ (YZLocationManager *)sharedLocationManager{
    static YZLocationManager *LocationManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        LocationManager = [[self alloc]init];
    });
    return LocationManager;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [[BMKLocationAuth sharedInstance] checkPermisionWithKey:@"CWDLRxGNipQ2MK8Yrofd7Vl3vuFcX4GY" authDelegate:self];
        _CLLocationCoordinate = [[CLLocationManager alloc]init];
        [_CLLocationCoordinate requestWhenInUseAuthorization];

        [self startLocationService];

    }
    return self;
}

- (void)startLocationService{
    
    if (![self _checkCLAuthorizationStatus]) {
        return;
    }else{
        self.locationManager.delegate = nil;
        self.locationManager = nil;
        self.locationManager.coordinateType = BMKLocationCoordinateTypeBMK09LL;
//        [self.locationManager setLocatingWithReGeocode:YES];
//        [self.locationManager startUpdatingLocation];
//        [self.locationManager startUpdatingHeading];
    }
}

- (void)stopLocationService{
    self.locationManager.delegate = nil;
}

#pragma mark - Private (私有方法)
- (void)restartLocationUpdates{
    [self startLocationService];
}

//检测是否打开定位
- (BOOL)_checkCLAuthorizationStatus{
    if ( [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        //1.跳出弹出框 提示用户打开步骤
        //2.通过代码调到设置页面
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示"
                                                                       message:@"请打开定位以获取周围的店铺信息"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"好的"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            
                                                            NSURL   *openUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                            if ( [[UIApplication sharedApplication] canOpenURL:openUrl]) {
                                                                [[UIApplication sharedApplication] openURL:openUrl];
                                                            }
                                                        }];
        [alert addAction:action1];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alert animated:YES completion:nil];
        return NO;
    }
    return YES;

}
- (BMKLocationManager *)locationManager{
    if (!_locationManager) {
        DDWeakSelf
        //初始化实例
        _locationManager = [[BMKLocationManager alloc] init];
        //设置delegate
        _locationManager.delegate = self;
        //设置返回位置的坐标系类型
        _locationManager.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        //设置距离过滤参数
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        //设置预期精度参数
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //设置应用位置类型
        _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        //设置是否自动停止位置更新
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        //设置是否允许后台定位
        //    bmklocationManager.allowsBackgroundLocationUpdates = YES;
        //设置位置获取超时时间
        _locationManager.locationTimeout = 10;
        //设置获取地址信息超时时间
        _locationManager.reGeocodeTimeout = 10;
        [_locationManager requestLocationWithReGeocode:YES
                                        withNetworkState:YES
                                         completionBlock:^(BMKLocation * _Nullable location,
                                                           BMKLocationNetworkState state, NSError * _Nullable error) {
                                             if (error) {
//                                                 NSLog(error);
                                             }else{
                                                 weakSelf.lastCoordinate = location;
                                                 if (weakSelf.location) {
                                                     weakSelf.location(location, error);
                                                 }
                                             }
                                         }];

    }
    return _locationManager;
}
/* 定位失败**/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"定位错误");
}
- (void)BMKLocationManager:(BMKLocationManager * _Nonnull)manager
          didUpdateHeading:(CLHeading * _Nullable)heading{
    if (self.locationHeading) {
        self.locationHeading(heading, nil);
    }
}
- (void)BMKLocationManager:(BMKLocationManager * _Nonnull)manager didUpdateLocation:(BMKLocation * _Nullable)location orError:(NSError * _Nullable)error

{
    if (error) {
        //                                                 NSLog(error);
    }else{
        self.lastCoordinate = location;
        if (self.location) {
            self.location(location, error);
        }
    }
}



@end
