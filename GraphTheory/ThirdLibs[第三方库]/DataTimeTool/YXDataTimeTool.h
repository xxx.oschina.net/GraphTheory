//
//  YXDataTimeToo.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/10/24.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YXDataTimeTool : NSObject

/**
 计算两个时间间隔
 
 @param nowDateStr 当前时间
 @param deadlineStr 截止时间
 @return 返回字符串
 */
+ (NSInteger)getDateDifferenceWithNowDateStr:(NSString*)nowDateStr deadlineStr:(NSString*)deadlineStr;

/**
 计算两个时间间隔多少年
 
 @param nowDateStr 当前时间
 @param deadlineStr 截止时间
 @return 返回字符串
 */
+ (NSInteger)getYearsWithNowDateStr:(NSString*)nowDateStr deadlineStr:(NSString*)deadlineStr;


/**
 时间戳转string
 
 @param timestamp 时间戳
 @param format 时间格式
 @return 返回时间string
 */
+ (NSString *)timestampToString:(NSInteger)timestamp  withFormat:(NSString *)format;

/**
 将时间戳转换成时间

 @param time 时间戳
 @return 返回时间string
 */
+ (NSString *)getTimeFromTimestamp:(NSInteger)time;

//获取当前的时间
+(NSString*)getCurrentTimes;
//获取当前的时间自定义格式
+(NSString*)getCurrentTimesWithFormat:(NSString *)format;
// 字符串转时间戳
+ (NSString *)getTimeStrWithString:(NSString *)str;
// 字符串转自定义格式时间戳
+ (NSString *)getTimeStrWithString:(NSString *)str Format:(NSString *)format;
// 字符串转时间戳根据小时
+ (NSString *)getTimeStrWithString:(NSString *)str withHour:(NSString *)hour;
//获取当前时间戳字符串
+ (NSString *)currentTimeStr;

+ (BOOL)compareDate:(NSString*)sDate withDate:(NSString*)bDate;

@end

