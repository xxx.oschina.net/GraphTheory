//
//  NSMutableAttributedString+YXExtension.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "NSMutableAttributedString+YXExtension.h"

@implementation NSMutableAttributedString (YXExtension)

#pragma mark - 设置金额
+ (NSMutableAttributedString *)attributedPriceLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color {
    priceLab.text = [NSString stringWithFormat:@"¥ %@",text];
    NSMutableAttributedString *totalStr = [[NSMutableAttributedString alloc] initWithString:priceLab.text];
    [totalStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17],NSForegroundColorAttributeName:color} range:[priceLab.text rangeOfString:[NSString stringWithFormat:@"%@",text]]];
    return totalStr;
}

#pragma mark - 设置优惠券金额
+ (NSMutableAttributedString *)attributedCouponsLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color {
    
    priceLab.text = [NSString stringWithFormat:@"-¥%@",text];
    NSMutableAttributedString *totalStr = [[NSMutableAttributedString alloc] initWithString:priceLab.text];
    [totalStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15],NSForegroundColorAttributeName:color} range:[priceLab.text rangeOfString:[NSString stringWithFormat:@"%@",text]]];
    [totalStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12]range:NSMakeRange(1, 1)];
    return totalStr;
}

#pragma mark - 设置总计金额
+ (NSMutableAttributedString *)totalPriceLabText:(NSString *)text Lab:(UILabel *)lab {
    lab.text = [NSString stringWithFormat:@"总计：%@",text];
    NSMutableAttributedString *totalStr = [[NSMutableAttributedString alloc] initWithString:lab.text];
    [totalStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16],NSForegroundColorAttributeName:APPTintColor} range:[lab.text rangeOfString:[NSString stringWithFormat:@"%@",text]]];
    [totalStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12]range:NSMakeRange(3, 1)];
    [totalStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor]range:NSMakeRange(0,2)];

    return totalStr;
}

@end
