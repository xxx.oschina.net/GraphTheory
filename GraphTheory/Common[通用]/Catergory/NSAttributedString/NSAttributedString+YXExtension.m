//
//  NSAttributedString+YXExtension.m
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/4.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "NSAttributedString+YXExtension.h"
#import <NSAttributedString+YYText.h>

@implementation NSAttributedString (YXExtension)

+ (NSAttributedString *)attributedText:(NSString *)text HighlightText:(NSString *)highlightText {
    
    NSString *textString = [NSString stringWithFormat:@"%@%@", text, highlightText];;
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:textString];
    mutableAttributedString.yy_font = [UIFont systemFontOfSize:14.0f];
    mutableAttributedString.yy_color = color_TextOne;
    mutableAttributedString.yy_lineSpacing = 14.0f;
    
    NSRange fromUserRange = NSMakeRange(3, highlightText.length);
    YYTextHighlight *fromUserHighlight = [YYTextHighlight highlightWithBackgroundColor:[UIColor colorWithWhite:0.000 alpha:0.220]];
    [mutableAttributedString yy_setTextHighlight:fromUserHighlight range:fromUserRange];
    // 设置找回密码颜色
    [mutableAttributedString yy_setColor:color_TextOne range:NSMakeRange(3, highlightText.length)];
    return mutableAttributedString;
    
}

@end
