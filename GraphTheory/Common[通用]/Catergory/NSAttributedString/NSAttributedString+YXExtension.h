//
//  NSAttributedString+YXExtension.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/4.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (YXExtension)



/**
 设置找回密码文字

 @param     text 默认文字
 @param     highlightText 高亮文字
 @return    返回 NSAttributedString
 */
+ (NSAttributedString *)attributedText:(NSString *)text HighlightText:(NSString *)highlightText;

@end


