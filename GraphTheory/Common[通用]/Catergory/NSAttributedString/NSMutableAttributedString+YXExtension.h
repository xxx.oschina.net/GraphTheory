//
//  NSMutableAttributedString+YXExtension.h
//  DDBLifeShops
//
//  Created by 杨旭 on 2019/3/20.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (YXExtension)

/**
 设置金额
 
 @param priceLab  文本
 @param text 金额
 @return 返回 NSMutableAttributedString
 */
+ (NSMutableAttributedString *)attributedPriceLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color;


/**
 设置优惠券金额
 
 @param priceLab  文本
 @param text 金额
 @return 返回 NSMutableAttributedString
 */
+ (NSMutableAttributedString *)attributedCouponsLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color;


/**
 设置总金额

 @param text 金额
 @param lab 文本
 @return NSMutableAttributedString
 */
+ (NSMutableAttributedString *)totalPriceLabText:(NSString *)text Lab:(UILabel *)lab;

@end

NS_ASSUME_NONNULL_END
