//
//  NSString+YXCommon.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/10/29.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (YXCommon)

/**
 * 验证字符串是否为空
 */
+(BOOL)isBlankString:(NSString *)aStr;

/**
 * 验证提现密码是否包含字母和数字
 */
+(BOOL)checkPasseordNo:(NSString*)password;


/**
 * 只能为中文
 */
+ (BOOL)onlyInputChineseCharacters:(NSString*)string;


/**
 * 隐藏手机号中间四位数字
 */
+ (NSString *)numberSuitScanf:(NSString*)number;

/**
 * 验证身份证号码
 */
+(BOOL)checkIdentityCardNo:(NSString*)cardNo;

/**
 * 判断手机号码格式是否正确
 */
+ (BOOL)valiMobile:(NSString *)mobile;

/**
 * 用户名加密
 */
+ (NSString *)userNameEncryption:(NSString *)userName;

/**
 url 转字典
 */
+(NSDictionary *)dictionaryWithUrlString:(NSString *)urlStr;


/**
 json字符串转字典
 */
+ (NSDictionary *)convertjsonStringToDict:(NSString *)jsonString;


/**
 替换
 */
+ (NSString *)Replace :(NSString *)str :(NSString *)oldstr :(NSString *)newstr;

/**
 字典转签名str
 
 @param dic 参数
 @return 获取签名字符串
 */
+ (NSString *)getSignStringFromDic:(NSDictionary *)dic;


/**
 str转MD5Str
 
 @param str str
 @return md5str
 */
+ (NSString *)getMD5StringFromStr:(NSString *)str;

/**
 推送使用 字符串转 32bytes位 token

 @param string token字符串
 @return 返回32bytes
 */
+ (NSData *)dataFromHexString:(NSString *)string;


/**
 格式化价格
 
 @param string 价格
 @return 价格
 */
+ (NSString *)formatPriceString:(NSString *)string;



/**
 数组转json字符串

 @param array 数组
 @return 字符串
 */
+ (NSString *)arrayToJSONString:(NSArray *)array;



/**
 数组转可变字符串

 @param arr 数组
 @return 字符串
 */
+ (NSString *)stringWithArr:(NSArray *)arr;

/**
 * 获取文字高度
 */
+ (CGFloat)getStringHeightWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)width;

/**
 * 获取文字宽度
 */
+ (CGFloat)getStringWidthWithText:(NSString *)text font:(UIFont *)font viewHeight:(CGFloat)height;


+ (NSString *)arrayToString:(NSArray *)arr;
+ (NSString *)dicToString:(NSDictionary *)dic;


@end

