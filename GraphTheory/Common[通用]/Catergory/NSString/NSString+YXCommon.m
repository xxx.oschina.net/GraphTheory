//
//  NSString+YXCommon.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2018/10/29.
//  Copyright © 2018年 DianDu. All rights reserved.
//

#import "NSString+YXCommon.h"
#import <CommonCrypto/CommonDigest.h>
#import <sys/utsname.h>
@implementation NSString (YXCommon)

+(BOOL)isBlankString:(NSString *)aStr {
    
    if(!aStr){
        return YES;
    }
    
    if([aStr isKindOfClass:[NSNull class]]) {
        return YES;
    }
    
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSString *trimmedStr = [aStr stringByTrimmingCharactersInSet:set];
    
    if (!trimmedStr.length){
        return YES;
    }
    return NO;
    
}

+(BOOL)checkPasseordNo:(NSString*)password{
    //字母加数字
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isOk = [pred evaluateWithObject:password];
    if (isOk) {//如果是的话
        return YES;
    }
    return NO;
}


+ (BOOL)onlyInputChineseCharacters:(NSString*)string{
    NSString *zhString = @"[\u4e00-\u9fa5]+";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",zhString];
    BOOL inputString = [predicate evaluateWithObject:string];
    return inputString;
}

+ (NSString *)numberSuitScanf:(NSString*)number {
    //首先验证是不是手机号码
    NSString *pattern = @"^1+[3-9]+\\d{9}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    
    BOOL isOk = [pred evaluateWithObject:number];
    if (isOk) {//如果是手机号码的话
        NSString *numberString = [number stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        return numberString;
    }
    return number;
    
}

//验证身份证号码
+(BOOL)checkIdentityCardNo:(NSString*)cardNo
{
    if (cardNo.length != 18) {
        return  NO;
    }
    NSArray* codeArray = [NSArray arrayWithObjects:@"7",@"9",@"10",@"5",@"8",@"4",@"2",@"1",@"6",@"3",@"7",@"9",@"10",@"5",@"8",@"4",@"2", nil];
    NSDictionary* checkCodeDic = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"1",@"0",@"X",@"9",@"8",@"7",@"6",@"5",@"4",@"3",@"2", nil]  forKeys:[NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10", nil]];
    
    NSScanner* scan = [NSScanner scannerWithString:[cardNo substringToIndex:17]];
    
    int val;
    BOOL isNum = [scan scanInt:&val] && [scan isAtEnd];
    if (!isNum) {
        return NO;
    }
    int sumValue = 0;
    
    for (int i =0; i<17; i++) {
        sumValue+=[[cardNo substringWithRange:NSMakeRange(i , 1) ] intValue]* [[codeArray objectAtIndex:i] intValue];
    }
    
    NSString* strlast = [checkCodeDic objectForKey:[NSString stringWithFormat:@"%d",sumValue%11]];
    
    if ([strlast isEqualToString: [[cardNo substringWithRange:NSMakeRange(17, 1)]uppercaseString]]) {
        return YES;
    }
    return  NO;
}

//判断手机号码格式是否正确
+ (BOOL)valiMobile:(NSString *)mobile{
    mobile = [mobile stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (mobile.length == 11){
        NSString *str1 = [mobile substringToIndex:1];
        if ([str1 isEqualToString:@"1"]) {
            return YES;
        }
        return NO;
    }else{
        return NO;
    }
}

/**
 * 用户名加密
 */
+ (NSString *)userNameEncryption:(NSString *)userName {
    
    if (userName.length > 1) {
        //字符串结尾
        NSString *lastStr = [userName substringFromIndex:[userName length]-1];
        NSString *lastName = [NSString stringWithFormat:@"**%@",lastStr];
        return lastName;
    }
    
    return userName;
}

/**
 url 转字典
 */
+(NSDictionary *)dictionaryWithUrlString:(NSString *)urlStr
{
    if (urlStr && urlStr.length && [urlStr rangeOfString:@"?"].length == 1) {
        NSArray *array = [urlStr componentsSeparatedByString:@"?"];
        if (array && array.count == 2) {
            NSString *paramsStr = array[1];
            if (paramsStr.length) {
                NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
                NSArray *paramArray = [paramsStr componentsSeparatedByString:@"&"];
                for (NSString *param in paramArray) {
                    if (param && param.length) {
                        NSArray *parArr = [param componentsSeparatedByString:@"="];
                        if (parArr.count == 2) {
                            [paramsDict setObject:parArr[1] forKey:parArr[0]];
                        }
                    }
                }
                return paramsDict;
            }else{
                return nil;
            }
        }else{
            return nil;
        }
    }else{
        return nil;
    }
}


/**
 json字符串转字典
 */
+ (NSDictionary *)convertjsonStringToDict:(NSString *)jsonString {
    
    NSDictionary *retDict = nil;
    if ([jsonString isKindOfClass:[NSString class]]) {
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        retDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
        return  retDict;
    }else{
        return retDict;
    }
    
}

/**
 替换
 */
+(NSString *)Replace:(NSString *)str :(NSString *)oldstr :(NSString *)newstr
{
    if([str isEqual:nil] || [oldstr isEqual:nil] || [newstr isEqual:nil])
    {
        return  @"";
    }
    return [str stringByReplacingOccurrencesOfString:oldstr withString:newstr];
}


+ (NSString *)getSignStringFromDic:(NSDictionary *)dic
{
    
    NSMutableString *URL = [NSMutableString string];
    //获取字典的所有keys
    NSArray * keys = [dic allKeys];
    NSArray *sortKeys = [keys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    
    NSString *string;
    //拼接字符串
    for (int j = 0; j < sortKeys.count; j ++){
        if (j == 0){
            //拼接时不加&
            string = [NSString stringWithFormat:@"%@=%@", sortKeys[j], dic[sortKeys[j]]];
        }else{
            //拼接时加&
            string = [NSString stringWithFormat:@"&%@=%@", sortKeys[j], dic[sortKeys[j]]];
        }
        //拼接字符串
        [URL appendString:string];
        
    }
    
    return URL;
}


+ (NSString *)getMD5StringFromStr:(NSString *)str
{
    
    const char *cStr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++){
        [output appendFormat:@"%02x", digest[i]];
    }
    output = [output uppercaseString].copy;
    
    return output;
}

/**
 字符串转 32bytes位 token
 */
+ (NSData *)dataFromHexString:(NSString *)string {
    
    if (string.length > 0) {
        NSMutableData*apnsTokenMutableData = [[NSMutableData alloc]init];
        
        unsigned char whole_byte;
        
        char byte_chars[3] = {'\0','\0','\0'};
        
        int i;
        
        for(i=0; i < [string length]/2; i++) {
            
            byte_chars[0] = [string characterAtIndex:i*2];
            
            byte_chars[1] = [string characterAtIndex:i*2+1];
            
            whole_byte =strtol(byte_chars,NULL,16);
            
            [apnsTokenMutableData appendBytes:&whole_byte length:1];
            
        }
        
        NSData*apnsTokenData = [NSData dataWithData:apnsTokenMutableData];
        
        return  apnsTokenData;
    }
    return nil;
}

+ (NSString *)formatPriceString:(NSString *)string{
    NSMutableString *fomatString = [[NSMutableString alloc]initWithString:@"###,##0.00"];
    
    NSDecimalNumberHandler *hander = [[NSDecimalNumberHandler alloc]initWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO];
    
    NSDecimalNumber *decimal = [[NSDecimalNumber alloc]initWithString:string?:@"0"];
    decimal = [decimal decimalNumberByRoundingAccordingToBehavior:hander];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = kCFNumberFormatterNoStyle;
    [formatter setPositiveFormat:fomatString];
    return [formatter stringFromNumber:decimal];

}

+ (NSString *)arrayToJSONString:(NSArray *)array

{
    NSError *error = nil;
    //    NSMutableArray *muArray = [NSMutableArray array];
    //    for (NSString *userId in array) {
    //        [muArray addObject:[NSString stringWithFormat:@"\"%@\"", userId]];
    //    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    // 去除换行与空格
    NSString *jsonTemp = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString *jsonResult = [jsonTemp stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"json array is: %@", jsonResult);
    return jsonResult;
}


+ (NSString *)stringWithArr:(NSArray *)arr {
    
    if (arr.count>0) {
        NSMutableString *str = [NSMutableString string];
        for (int i = 0; i < arr.count; i++) {
            NSString *string =  [arr objectAtIndex:i];
            if (i == 0) {
                [str appendString:string];
            }else {
                [str appendString:[NSString stringWithFormat:@"  %@",string]];
            }
        }
        return str;
    }
    return @"";
}


+ (CGFloat)getStringHeightWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)width {
    // 设置文字属性 要和label的一致
    NSDictionary *attrs = @{NSFontAttributeName :font};
    CGSize maxSize = CGSizeMake(width, MAXFLOAT);
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    // 计算文字占据的宽高
    CGSize size = [text boundingRectWithSize:maxSize options:options attributes:attrs context:nil].size;
    
    // 当你是把获得的高度来布局控件的View的高度的时候.size转化为ceilf(size.height)。
    return  ceilf(size.height);
}

+ (CGFloat)getStringWidthWithText:(NSString *)text font:(UIFont *)font viewHeight:(CGFloat)height {
    // 设置文字属性 要和label的一致
    NSDictionary *attrs = @{NSFontAttributeName :font};
    CGSize maxSize = CGSizeMake(MAXFLOAT, height);
    
    NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    // 计算文字占据的宽高
    CGSize size = [text boundingRectWithSize:maxSize options:options attributes:attrs context:nil].size;
    
    // 当你是把获得的高度来布局控件的View的高度的时候.size转化为ceilf(size.height)。
    return  ceilf(size.width);
}

+ (NSString *)arrayToString:(NSArray *)arr {
    
    NSMutableString *string = [NSMutableString stringWithFormat:@"["];
    for (id obj in arr) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            [string appendString:[NSString dicToString:obj]];
        } else if ([obj isKindOfClass:[NSString class]]) {
            [string appendString:[NSString stringWithFormat:@"\"%@\"", obj]];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            [string appendString:[NSString arrayToString:obj]];
        } else {
            [string appendString:[NSString stringWithFormat:@"%@", obj]];
        }
        if (!([arr indexOfObject:obj] >= arr.count)) {
            [string appendString:@","];
        }
    }
    [string appendString:@"]"];
    
    return string;
}

//字典转字符串
+ (NSString *)dicToString:(NSDictionary *)dic{
    NSMutableString *string = [NSMutableString stringWithFormat:@"{"];
    for (NSString *keyStr in dic.allKeys) {
        [string appendString:[NSString stringWithFormat:@"\"%@\":",keyStr]];
        if ([[dic valueForKey:keyStr] isKindOfClass:[NSArray class]]) {
            [string appendString:[NSString arrayToString:[dic valueForKey:keyStr]]];
        }else if ([[dic valueForKey:keyStr] isKindOfClass:[NSDictionary class]]) {
            [string appendString:[NSString dicToString:[dic valueForKey:keyStr]]];
        }else if ([[dic valueForKey:keyStr] isKindOfClass:[NSString class]]) {
            [string appendString:[NSString stringWithFormat:@"\"%@\"",[dic valueForKey:keyStr]]];
        }else{
            [string appendString:[NSString stringWithFormat:@"%@",[dic valueForKey:keyStr]]];
        }
        if ([dic.allKeys indexOfObject:keyStr] < dic.allKeys.count - 1) {
            [string appendString:@","];
        }
    }
    [string appendString:@"}"];
    
    return string;
}

@end
