//
//  NSDictionary+DeleteNull.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/10/21.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (DeleteNull)

+(id)changeType:(id)myObj;

+ (NSData *)dictionary2JsonData:(NSDictionary *)dict;

+ (NSDictionary *)jsonSring2Dictionary:(NSString *)jsonString;

+ (NSDictionary *)jsonData2Dictionary:(NSData *)jsonData;
@end

NS_ASSUME_NONNULL_END
