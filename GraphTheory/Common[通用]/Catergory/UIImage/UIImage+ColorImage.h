//
//  UIImage+ColorImage.h
//  ZanXiaoQu
//
//  Created by mac on 2017/12/1.
//  Copyright © 2017年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorImage)

+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;

/**
 旋转图片

 @param aImage 原图
 @return 旋转后的图片
 */
+ (UIImage *)imageWithRightOrientation:(UIImage *)aImage;



/**
 获取视图的一张图片

 @param videoPath 视频地址
 @return 返回视频的第一帧截图
 */
+ (UIImage*) getVideoPreViewImageWithPath:(NSURL *)videoPath;

/**
 压缩图片大小

 @param maxLength 图片大小
 @return 压缩后的
 */
- (NSData *)compressWithMaxLength:(NSUInteger)maxLength;



+ (UIImage*) getThumImgOfImgIOWithData:(NSData*)data withMaxPixelSize:(int)maxPixelSize;

//修改图片尺寸 同比缩放

+ (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize;

/** 获取图片在内存中占用的空间大小 */
+ (UInt64) getMemorySizeWithImg:(UIImage*)img;
@end
