//
//  UIImage+ColorImage.m
//  ZanXiaoQu
//
//  Created by mac on 2017/12/1.
//  Copyright © 2017年 DianDu. All rights reserved.
//

#import "UIImage+ColorImage.h"
#import <AVFoundation/AVFoundation.h>
@implementation UIImage (ColorImage)

+ (UIImage *)imageWithColor:(UIColor *)color{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context,color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+ (UIImage *)imageWithRightOrientation:(UIImage *)aImage {  
    
    // No-op if the orientation is already correct  
    if (aImage.imageOrientation == UIImageOrientationUp)   
        return aImage;  
    
    // We need to calculate the proper transformation to make the image upright.  
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.  
    CGAffineTransform transform = CGAffineTransformIdentity;  
    
    switch (aImage.imageOrientation) {  
        case UIImageOrientationDown:  
        case UIImageOrientationDownMirrored:  
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);  
            transform = CGAffineTransformRotate(transform, M_PI);  
            break;  
            
        case UIImageOrientationLeft:  
        case UIImageOrientationLeftMirrored:  
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);  
            transform = CGAffineTransformRotate(transform, M_PI_2);  
            break;  
            
        case UIImageOrientationRight:  
        case UIImageOrientationRightMirrored:  
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);  
            transform = CGAffineTransformRotate(transform, -M_PI_2);  
            break;  
        default:  
            break;  
    }  
    
    switch (aImage.imageOrientation) {  
        case UIImageOrientationUpMirrored:  
        case UIImageOrientationDownMirrored:  
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);  
            transform = CGAffineTransformScale(transform, -1, 1);  
            break;  
            
        case UIImageOrientationLeftMirrored:  
        case UIImageOrientationRightMirrored:  
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);  
            transform = CGAffineTransformScale(transform, -1, 1);  
            break;  
        default:  
            break;  
    }  
    
    // Now we draw the underlying CGImage into a new context, applying the transform  
    // calculated above.  
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,  
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,  
                                             CGImageGetColorSpace(aImage.CGImage),  
                                             CGImageGetBitmapInfo(aImage.CGImage));  
    CGContextConcatCTM(ctx, transform);  
    switch (aImage.imageOrientation) {  
        case UIImageOrientationLeft:  
        case UIImageOrientationLeftMirrored:  
        case UIImageOrientationRight:  
        case UIImageOrientationRightMirrored:  
            // Grr...  
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);  
            break;  
            
        default:  
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);  
            break;  
    }  
    
    // And now we just create a new UIImage from the drawing context  
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);  
    UIImage *img = [UIImage imageWithCGImage:cgimg];  
    CGContextRelease(ctx);  
    CGImageRelease(cgimg);  
    return img;  
}

//获取视频的第一帧截图, 返回UIImage
//需要导入AVFoundation.h
+ (UIImage*) getVideoPreViewImageWithPath:(NSURL *)videoPath
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoPath options:nil];
    
    AVAssetImageGenerator *gen         = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    gen.appliesPreferredTrackTransform = YES;
    
    CMTime time      = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error   = nil;
    
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *img     = [[UIImage alloc] initWithCGImage:image];
    
    return img;
}
-(NSData *)compressWithMaxLength:(NSUInteger)maxLength{
    // Compress by quality
    CGFloat compression = 1;
    NSData *data = UIImageJPEGRepresentation(self, compression);
    //NSLog(@"Before compressing quality, image size = %ld KB",data.length/1024);
    if (data.length < maxLength) return data;
    
    CGFloat max = 1;
    CGFloat min = 0;
    for (int i = 0; i < 6; ++i) {
        compression = (max + min) / 2;
        data = UIImageJPEGRepresentation(self, compression);
        //NSLog(@"Compression = %.1f", compression);
        //NSLog(@"In compressing quality loop, image size = %ld KB", data.length / 1024);
        if (data.length < maxLength * 0.9) {
            min = compression;
        } else if (data.length > maxLength) {
            max = compression;
        } else {
            break;
        }
    }
    //NSLog(@"After compressing quality, image size = %ld KB", data.length / 1024);
    if (data.length < maxLength) return data;
    UIImage *resultImage = [UIImage imageWithData:data];
    // Compress by size
    NSUInteger lastDataLength = 0;
    while (data.length > maxLength && data.length != lastDataLength) {
        lastDataLength = data.length;
        CGFloat ratio = (CGFloat)maxLength / data.length;
        //NSLog(@"Ratio = %.1f", ratio);
        CGSize size = CGSizeMake((NSUInteger)(resultImage.size.width * sqrtf(ratio)),
                                 (NSUInteger)(resultImage.size.height * sqrtf(ratio))); // Use NSUInteger to prevent white blank
        UIGraphicsBeginImageContext(size);
        [resultImage drawInRect:CGRectMake(0, 0, size.width, size.height)];
        resultImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        data = UIImageJPEGRepresentation(resultImage, compression);
        //NSLog(@"In compressing size loop, image size = %ld KB", data.length / 1024);
    }
    //NSLog(@"After compressing size loop, image size = %ld KB", data.length / 1024);
    return data;
}



/** Image I/O 获取指定尺寸的图片，返回的结果Image 目标尺寸大小 <= 图片原始尺寸大小
 @param data ---- 图片Data
 @param maxPixelSize ---- 图片最大宽/高尺寸 ，设置后图片会根据最大宽/高 来等比例缩放图片
 
 @return 目标尺寸的图片Image  */
+ (UIImage*) getThumImgOfImgIOWithData:(NSData*)data withMaxPixelSize:(int)maxPixelSize
{
    UIImage *imgResult = nil;
    if(data == nil)         { return imgResult; }
    if(data.length <= 0)    { return imgResult; }
    if(maxPixelSize <= 0)   { return imgResult; }
    
    const float scale = [UIScreen mainScreen].scale;
    const int sizeTo = maxPixelSize * scale;
    CFDataRef dataRef = (__bridge CFDataRef)data;
    
    /* CGImageSource的键值说明
     kCGImageSourceCreateThumbnailWithTransform - 设置缩略图是否进行Transfrom变换
     kCGImageSourceCreateThumbnailFromImageAlways - 设置是否创建缩略图，无论原图像有没有包含缩略图，默认kCFBooleanFalse，影响 CGImageSourceCreateThumbnailAtIndex 方法
     kCGImageSourceCreateThumbnailFromImageIfAbsent - 设置是否创建缩略图，如果原图像有没有包含缩略图，则创建缩略图，默认kCFBooleanFalse，影响 CGImageSourceCreateThumbnailAtIndex 方法
     kCGImageSourceThumbnailMaxPixelSize - 设置缩略图的最大宽/高尺寸 需要设置为CFNumber值，设置后图片会根据最大宽/高 来等比例缩放图片
     kCGImageSourceShouldCache - 设置是否以解码的方式读取图片数据 默认为kCFBooleanTrue，如果设置为true，在读取数据时就进行解码 如果为false 则在渲染时才进行解码 */
    CFDictionaryRef dicOptionsRef = (__bridge CFDictionaryRef) @{
                                                                 (id)kCGImageSourceCreateThumbnailFromImageIfAbsent : @(YES),
                                                                 (id)kCGImageSourceThumbnailMaxPixelSize : @(sizeTo),
                                                                 (id)kCGImageSourceShouldCache : @(YES),
                                                                 };
    CGImageSourceRef src = CGImageSourceCreateWithData(dataRef, nil);
    CGImageRef thumImg = CGImageSourceCreateThumbnailAtIndex(src, 0, dicOptionsRef); //注意：如果设置 kCGImageSourceCreateThumbnailFromImageIfAbsent为 NO，那么 CGImageSourceCreateThumbnailAtIndex 会返回nil
    
    CFRelease(src); // 注意释放对象，否则会产生内存泄露
    
    imgResult = [UIImage imageWithCGImage:thumImg scale:scale orientation:UIImageOrientationUp];
    
    if(thumImg != nil){
        CFRelease(thumImg); // 注意释放对象，否则会产生内存泄露
    }
    
    return imgResult;
}

//修改图片尺寸 同比缩放

+ (UIImage *)thumbnailWithImageWithoutScale:(UIImage *)image size:(CGSize)asize

{
    
    UIImage *newimage;
    
    if (nil == image) {
        
        newimage = nil;
        
    }
    
    else{
        
        CGSize oldsize = image.size;
        
        CGRect rect;
        
        
        
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            
            
            
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            
            
            
            rect.size.height = asize.height;
            
            
            
            rect.origin.x = (asize.width - rect.size.width)/2;
            
            
            
            rect.origin.y = 0;
            
            
            
        }
        
        else{
            
            
            
            rect.size.width = asize.width;
            
            
            
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            
            
            
            rect.origin.x = 0;
            
            
            
            rect.origin.y = (asize.height - rect.size.height)/2;
            
            
            
        }
        
        
        
        UIGraphicsBeginImageContext(asize);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        
        
        
        [image drawInRect:rect];
        
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
    }
    
    return newimage;
    
}


/** 获取图片在内存中占用的空间大小 */
+ (UInt64) getMemorySizeWithImg:(UIImage*)img{
    UInt64 cgImageBytesPerRow = CGImageGetBytesPerRow(img.CGImage);
    UInt64 cgImageHeight = CGImageGetHeight(img.CGImage);
    UInt64 size  = cgImageHeight * cgImageBytesPerRow;
    NSLog(@"MemorySize:%lu Bytes",(unsigned long)size/1024);
    return size;
}


@end
