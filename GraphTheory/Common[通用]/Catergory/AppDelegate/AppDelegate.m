//
//  AppDelegate.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "AppDelegate.h"
#import "DDBaseTabBarController.h"
#import "DDBaseNavigationController.h"
#import "WGLoginViewController.h"
#import "AppDelegate+AppService.h"
#import "DDPayMentTools.h"
#import <IQKeyboardManager/IQKeyboardManager.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
        // Override point for customization after application launch.
    
        // 注册友盟
    [self registerUmeng:launchOptions];
    [self registerKeyboardManager];
    [self registerBugly];
    [self registerAMapManager];
//    [self requestIDFA];
    NSLog(@"token是多少：：%@", [YXUserInfoManager getUserInfo].token);
    NSLog(@"userId == %@",[YXUserInfoManager getUserInfo].user_id);
    [WXApi registerApp:WX_APP_ID universalLink:@"https://syj.ddsaas.cn"];
#if DEBUG
        //模拟器  -(void)injected {}
    [[NSBundle bundleWithPath:@"/Applications/InjectionIII.app/Contents/Resources/iOSInjection.bundle"] load];
#endif
    
    [self setRootViewController];

    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
     [WXApi handleOpenUniversalLink:userActivity delegate:[DDPayMentTools shareDDPayMentTools]];
    if (![userActivity.activityType isEqualToString:NSUserActivityTypeBrowsingWeb]) {
        return YES;
    }
    //读取url地址
    NSURL *jumpUrl = userActivity.webpageURL;
    // TODO: handle jump code
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)setRootViewController
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    if ([YXUserInfoManager isLoad]== NO) {
        WGLoginViewController *vc = [[WGLoginViewController alloc] init];
        DDBaseNavigationController *nav = [[DDBaseNavigationController alloc] initWithRootViewController:vc];
        self.window.rootViewController = nav;
    }else {
        DDBaseTabBarController *tabBar = [[DDBaseTabBarController alloc] init];
        self.window.rootViewController = tabBar;
    }
   
    [self.window makeKeyAndVisible];

}

@end
