//
//  AppDelegate+AppService.m
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//
#import "AppDelegate+AppService.h"
#import <IQKeyboardManager.h>
#import <UMCommon/UMCommon.h>
#import <UMPush/UMessage.h>
#import <UMShare/UMShare.h>
#import <Bugly/Bugly.h>
#import <AlipaySDK/AlipaySDK.h>
#import <CYLTabBarController.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import "DDPayMentTools.h"
#import "UIViewController+Nav.h"
#import "DDBaseTabBarController.h"
#import "DDBaseNavigationController.h"
#import "WGLoginViewController.h"
@implementation AppDelegate (AppService)
- (void)registerUmeng:(NSDictionary *)launchOptions{
    [UMConfigure setLogEnabled:NO];
    [UMConfigure initWithAppkey:UM_APP_KEY channel:@"App Store"];
    UMessageRegisterEntity * entity = [[UMessageRegisterEntity alloc] init];
    entity.types = UMessageAuthorizationOptionBadge|UMessageAuthorizationOptionSound|UMessageAuthorizationOptionAlert;
    if (@available(iOS 10.0, *)) {
        [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    } else {
        // Fallback on earlier versions
    }
    [UMessage registerForRemoteNotificationsWithLaunchOptions:launchOptions
                                                       Entity:entity
                                            completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                                if (granted) {
                                                    // 用户选择了接收Push消息
                                                }else{
                                                    // 用户拒绝接收Push消息
                                                }
                                            }];
//
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:WX_APP_ID appSecret:Wechat_APP_Secret redirectURL:nil];
//    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:QQ_APP_Id/*设置QQ平台的appID*/  appSecret:nil redirectURL:nil];
}
- (void)registerKeyboardManager{
//    [[UITextField appearance] setTintColor:[UIColor blackColor]];
    IQKeyboardManager* manager = [IQKeyboardManager sharedManager];
    manager.toolbarDoneBarButtonItemText =@"完成";
    manager.enable=YES;
//    manager.toolbarManageBehaviour =IQAutoToolbarBySubviews;
    manager.shouldResignOnTouchOutside=YES;
    manager.shouldToolbarUsesTextFieldTintColor=NO;
    manager.enableAutoToolbar=YES;
    manager.shouldShowToolbarPlaceholder = YES;
    [manager setLayoutIfNeededOnUpdate:NO];
}

- (void)registerBugly {
    [Bugly startWithAppId:Bugly_Id];
}

- (void)registerAMapManager {
    [AMapServices sharedServices].apiKey = AMAP_APP_KEY;
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    self.deviceToken = deviceToken;

    if (@available(iOS 13.0, *)) {
        // 获取deviceToken
        if (![deviceToken isKindOfClass:[NSData class]]) return;
        const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
        NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                              ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                              ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                              ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
        NSLog(@"deviceToken:%@",hexToken);
        NSData *token = [NSString dataFromHexString:hexToken];
        [UMessage registerDeviceToken:token];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:hexToken forKey:@"deviceToken"];
        [defaults setObject:token forKey:@"deviceTokenData"];
        [defaults synchronize];
    } else {
        
        [UMessage registerDeviceToken:deviceToken];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *deviceTokenStr =  [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<"withString: @""]
                                      stringByReplacingOccurrencesOfString: @">"withString: @""]
                                     stringByReplacingOccurrencesOfString: @" "withString: @""];
        [defaults setObject:deviceTokenStr forKey:@"deviceToken"];
        [defaults setObject:deviceToken forKey:@"deviceTokenData"];
        [defaults synchronize];
        
        NSLog(@"%@", deviceTokenStr);
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = deviceTokenStr;
    }
}
//iOS_10 以下
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    /** 必须加上，否则不会触发该方法 */
    if (userInfo) {
        completionHandler(UIBackgroundFetchResultNewData);
    }else {
        completionHandler(UIBackgroundFetchResultNoData);
    }
    [UMessage setAutoAlert:NO];
    if([[[UIDevice currentDevice] systemVersion]intValue] < 10){
        [UMessage didReceiveRemoteNotification:userInfo];
    }
    completionHandler(UIBackgroundFetchResultNewData);
}
#pragma mark 推送注册失败;
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}
#pragma mark -- iOS 10: App在前台获取到通知
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [UMessage setAutoAlert:NO];
        [UMessage didReceiveRemoteNotification:userInfo];//必须加这句代码 - 应用处于
    }else{
        //应用处于前台时的本地推送接受
    }
    completionHandler(UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert);
}
#pragma mark -- iOS 10: 点击通知进入App时触发
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // 添加跳转详情页
    [self JPushNotificationShowDetailViewController:response.notification.request.content.userInfo];
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        [UMessage didReceiveRemoteNotification:userInfo];//必须加这句代码
    }else{
        //应用处于前台时的本地推送接受
    }
    completionHandler();
}
/**
 *  最老的版本，最好也写上
 */
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        return [[DDPayMentTools shareDDPayMentTools] dd_handleUrl:url];
    }
    return result;
}
/**
 *  iOS 9.0 之前 会调用
 */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL result = [[DDPayMentTools shareDDPayMentTools] dd_handleUrl:url];
    if (!result) {
        return [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return result;
}
/**
 *  iOS 9.0 以上（包括iOS9.0）
 */
- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options{
    BOOL result = [[DDPayMentTools shareDDPayMentTools] dd_handleUrl:url];
    if (!result) {
        return [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    }
    return result;
}
#pragma mark -- 推送进入详情界面
/**
 1. 当APP为关闭状态时，点击通知栏消息跳转到指定的页面。
 2. 当APP在后台运行时，点击通知栏消息跳转到指定的页面。
 3. 当APP在前台运行时，不会有通知栏提醒，也就不会跳转到指定界面。
 */
- (void)JPushNotificationShowDetailViewController:(NSDictionary *)userInfo{
    [self receivePush:userInfo];
}
- (void)receivePush:(NSDictionary *)userInfo{
//    UIViewController* rootVC = [[UIApplication sharedApplication].delegate window].rootViewController;
//    NSDictionary *dic = userInfo[@"aps"][@"alert"];
//    NSLog(@"---%@",dic);
//    NSString *vcName;
//    NSMutableDictionary *vcTemp;
//    ///资金入账
//    if ([userInfo[@"biaoshi"] isEqualToString:@"zjrzsj"]) {
//        vcName = @"YXMoneyDetailViewController";
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"txcgsj"]) { ///提现成功
//        vcName = @"YXWithdrawRecordViewController";
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"cjdd"]) { ///订单-待发货
//        vcName = @"WGOrderViewController";
//        NSString *selectIndex = userInfo[@"selectIndex"];
//        vcTemp = [[NSMutableDictionary alloc]initWithDictionary:@{@"selectIndex":selectIndex}];
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"jdsj"]) { ///接单、退款-订单详情
//        vcName = @"WGOrderDetailViewController";
//        NSString *relationId = userInfo[@"orderNo"];
//        vcTemp = [[NSMutableDictionary alloc]initWithDictionary:@{@"orderNo":relationId}];
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"tksj"]) {
//        vcName = @"YXSalesAfterController";
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"yxhdsj"]) { ///营销活动
//        vcName = @"YXFlashSaleViewController";
//        vcTemp = [[NSMutableDictionary alloc]initWithDictionary:@{@"type":@"2"}];
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"rztgsj"]) { ///个人认证审核通过-APP首页
////        WGLoginViewController *vc = [[WGLoginViewController alloc] init];
////        DDBaseNavigationController *nav = [[DDBaseNavigationController alloc] initWithRootViewController:vc];
////        self.window.rootViewController = nav;
////        return;
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"rzjjsj"]) { ///个人认证审核拒绝-实名认证-审核不通过页面
//
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"wltgsj"]) { ///物料审核通过-投放广告页面
//        vcName = @"YXMaterialListViewController";
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"wljjsj"]) { ///物料审核不通过-选择物料页面
//        vcName = @"YXADMaterialViewController";
//    }else if ([userInfo[@"biaoshi"] isEqualToString:@"kcdyxjsj"]) {
//        vcName = @"ZLJAddGoodsVC";
//        NSString *goodsId = userInfo[@"goodsId"];
//        vcTemp = [[NSMutableDictionary alloc]initWithDictionary:@{@"goodsId":goodsId}];
//    }else{
//        [self cyl_tabBarController].selectedIndex = 1;
//        return;
//    }
//    if (vcName) {
//        Class viewController = NSClassFromString(vcName);
//        UIViewController *vc = [[viewController alloc]init];
//        for (NSString *temKey in vcTemp.allKeys) {
//            [vc setValue:vcTemp[temKey] forKey:temKey];
//        }
//        [rootVC.myNavigationController pushViewController:vc animated:NO];
//
//    }
}
- (void)applicationDidEnterBackground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {

}

@end
