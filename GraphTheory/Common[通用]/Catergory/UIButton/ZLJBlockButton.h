//
//  ZLJBlockButton.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^ButtonBlock)(UIButton *sender);

@interface ZLJBlockButton : UIButton

- (void)addTapBlock:(ButtonBlock)block;

@end

NS_ASSUME_NONNULL_END
