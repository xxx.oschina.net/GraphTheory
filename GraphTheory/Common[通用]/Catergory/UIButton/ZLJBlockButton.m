//
//  ZLJBlockButton.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "ZLJBlockButton.h"



@interface ZLJBlockButton ()

@property(nonatomic,copy)ButtonBlock block;

@end

@implementation ZLJBlockButton

- (void)addTapBlock:(ButtonBlock)block
{
    _block = block;
    [self addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)buttonAction:(UIButton *)button
{
    _block(button);
}

@end
