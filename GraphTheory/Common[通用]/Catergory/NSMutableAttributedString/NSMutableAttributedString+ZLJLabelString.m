//
//  NSMutableAttributedString+ZLJLabelString.m
//  ZanXiaoQu
//
//  Created by TomLong on 2019/5/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "NSMutableAttributedString+ZLJLabelString.h"

@implementation NSMutableAttributedString (ZLJLabelString)

+ (NSMutableAttributedString *)middelLabelTextWithString:(NSString *)string appendString:(NSString *)appendString{
    
    NSString    *duliString = appendString;
    NSRange     stringRange = {0, [string length]};
    NSRange     duliRange = {[string length],[duliString length]};
    NSString    *allString = [NSString stringWithFormat:@"%@%@",string,duliString];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:allString];
    [str addAttribute:NSForegroundColorAttributeName value:APPTintColor range:stringRange];
    [str addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:22.0f] range:stringRange];
    [str addAttribute:NSForegroundColorAttributeName value:APPTintColor range:duliRange];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:10.0f] range:duliRange];
    return str;
}

+ (NSMutableAttributedString *)attributedLabelTextWithString:(NSString *)string appendString:(NSString *)appendString {
    NSString    *duliString = appendString;
    NSRange     stringRange = {0, [string length]};
    NSRange     duliRange = {[string length],[duliString length]};
    NSString    *allString = [NSString stringWithFormat:@"%@%@",string,duliString];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:allString];
    [str addAttribute:NSForegroundColorAttributeName value:APPTintColor range:stringRange];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15.0f] range:stringRange];
    [str addAttribute:NSForegroundColorAttributeName value:color_TextOne range:duliRange];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15.0f] range:duliRange];
    return str;
}

#pragma mark - 设置商家信息必填项
+ (NSMutableAttributedString *)attributedPriceLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color {
    priceLab.text = [NSString stringWithFormat:@"%@*",text];
    NSMutableAttributedString *totalStr = [[NSMutableAttributedString alloc] initWithString:priceLab.text];
    [totalStr setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14],NSForegroundColorAttributeName:color} range:[priceLab.text rangeOfString:[NSString stringWithFormat:@"%@",@"*"]]];
    return totalStr;
}



@end
