//
//  NSMutableAttributedString+ZLJLabelString.h
//  ZanXiaoQu
//
//  Created by TomLong on 2019/5/31.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (ZLJLabelString)


+ (NSMutableAttributedString *)middelLabelTextWithString:(NSString *)string appendString:(NSString *)appendString;


+ (NSMutableAttributedString *)attributedLabelTextWithString:(NSString *)string appendString:(NSString *)appendString;

+ (NSMutableAttributedString *)attributedPriceLab:(UILabel *)priceLab Text:(NSString *)text Color:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
