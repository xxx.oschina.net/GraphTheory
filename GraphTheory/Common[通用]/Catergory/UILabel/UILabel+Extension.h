//
//  UILabel+Extension.h
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/8/23.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Extension)

/**
 *  按照自定义尺寸大小，对字体间距设置px像素
 */
+ (void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font withSpacing:(CGFloat)spacing ImageName:(nonnull NSString *)imageName ;



/**
 *  获得字符串的高度
 */
+ (CGFloat)heightForString:(NSString *)value fontSize:(CGFloat)fontSize andWidth:(CGFloat)width;



/**
 *  字符串转网络url链接
 */
- (void)setTextWithLinkAttribute:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
