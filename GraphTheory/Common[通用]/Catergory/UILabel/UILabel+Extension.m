//
//  UILabel+Extension.m
//  ZanXiaoQu
//
//  Created by 杨旭 on 2019/8/23.
//  Copyright © 2019年 DianDu. All rights reserved.
//

#import "UILabel+Extension.h"

@implementation UILabel (Extension)


+ (void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font withSpacing:(CGFloat)spacing ImageName:(nonnull NSString *)imageName{
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] init];
    if (imageName.length) {
        NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        //使用一张图片作为Attachment数据
        attachment.image = [UIImage imageNamed:imageName];
        //这里bounds的x值并不会产生影响
        attachment.bounds = CGRectMake(0, -2, 44, 14);
        NSAttributedString *imgStr = [NSAttributedString attributedStringWithAttachment:attachment];
        [attributeStr appendAttributedString:imgStr];
        [attributeStr appendAttributedString:[[NSAttributedString alloc]initWithString:@" "]];
    }
    if (str) {
        NSAttributedString *imgStr = [[NSAttributedString alloc]initWithString:str];
        [attributeStr appendAttributedString:imgStr];
        
    }
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    
    paraStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    
    paraStyle.alignment = NSTextAlignmentLeft;
    
    paraStyle.lineSpacing = spacing; //设置行间距
    
    paraStyle.hyphenationFactor = 1.0;
    
    paraStyle.firstLineHeadIndent = 0.0;
    
    paraStyle.paragraphSpacingBefore = 0.0;
    
    paraStyle.headIndent = 0;
    
    paraStyle.tailIndent = 0;
    
    //设置字间距 NSKernAttributeName:@1.5f
    
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.0f
                          };
    [attributeStr addAttributes:dic range:NSMakeRange(0, attributeStr.length)];
    label.attributedText = attributeStr;
}


//获得字符串的高度
+ (CGFloat)heightForString:(NSString *)value fontSize:(CGFloat)fontSize andWidth:(CGFloat)width{
    
    CGSize sizeToFit =[value boundingRectWithSize:CGSizeMake(width,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0]}context:nil].size;
    return sizeToFit.height + 15;
    
}

//字符串转网络url链接
- (void)setTextWithLinkAttribute:(NSString *)text {
    
    self.attributedText = [self subStr:text];
    
}


-(NSMutableAttributedString*)subStr:(NSString *)string

{
    
    NSError *error;
    
    //可以识别url的正则表达式
    
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                  
                                                                           options:NSRegularExpressionCaseInsensitive
                                  
                                                                             error:&error];
    
    NSArray *arrayOfAllMatches = [regex matchesInString:string options:0 range:NSMakeRange(0, [string length])];
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    NSMutableArray *rangeArr=[[NSMutableArray alloc]init];
    
    
    
    for (NSTextCheckingResult *match in arrayOfAllMatches)
        
    {
        
        NSString* substringForMatch;
        
        substringForMatch = [string substringWithRange:match.range];
        
        [arr addObject:substringForMatch];
        
        
        
    }
    
    NSString *subStr=string;
    
    for (NSString *str in arr) {
        
        [rangeArr addObject:[self rangesOfString:str inString:subStr]];
        
    }
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    
    NSMutableAttributedString *attributedText;
    
    attributedText=[[NSMutableAttributedString alloc]initWithString:subStr attributes:@{NSFontAttributeName :font}];
    
    
    
    for(NSValue *value in rangeArr)
        
    {
        
        NSInteger index=[rangeArr indexOfObject:value];
        
        NSRange range=[value rangeValue];
        
        [attributedText addAttribute:NSLinkAttributeName value:[NSURL URLWithString:[arr objectAtIndex:index]] range:range];
        
        [attributedText addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:range];
        
        
        
    }
    
    return attributedText;
    
    
    
    
    
}

//获取查找字符串在母串中的NSRange

- (NSValue *)rangesOfString:(NSString *)searchString inString:(NSString *)str {
    
    
    
    NSRange searchRange = NSMakeRange(0, [str length]);
    
    
    
    NSRange range;
    
    
    
    if ((range = [str rangeOfString:searchString options:0 range:searchRange]).location != NSNotFound) {
        
        searchRange = NSMakeRange(NSMaxRange(range), [str length] - NSMaxRange(range));
        
    }
    
    return [NSValue valueWithRange:range];
    
}


@end
