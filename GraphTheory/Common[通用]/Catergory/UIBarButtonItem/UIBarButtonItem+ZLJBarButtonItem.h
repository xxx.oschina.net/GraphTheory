//
//  UIBarButtonItem+ZLJBarButtonItem.h
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^BarButtonClickBlock)(UIButton *sender);


@interface UIBarButtonItem (ZLJBarButtonItem)

/**
 设置导航栏item

 @param title 标题
 @param tColor 标题颜色
 @param tFount 标题字体
 @return 设置导航栏item
 */
+ (instancetype)initItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount itemClickBlock:(BarButtonClickBlock)itemClickBlock;



/**
 设置导航栏右边的item

 @param title 标题
 @param tColor 标题颜色
 @param tFount 标题字体
 @return 返回的item不可编辑
 */
+ (instancetype)initRightItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount itemClickBlock:(BarButtonClickBlock)itemClickBlock;


/**
设置导航栏左边的item

@param title 标题
@param tColor 标题颜色
@param tFount 标题字体
@param imageStr  图片名称
@return 返回的item不可编辑
*/
+ (instancetype)initLeftItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount imageStr:(NSString *)imageStr itemClickBlock:(BarButtonClickBlock)itemClickBlock;

@end

NS_ASSUME_NONNULL_END
