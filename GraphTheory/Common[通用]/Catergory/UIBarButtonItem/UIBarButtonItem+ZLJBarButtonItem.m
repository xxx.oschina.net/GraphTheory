//
//  UIBarButtonItem+ZLJBarButtonItem.m
//  BusinessFine
//
//  Created by TomLong on 2019/9/19.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#import "UIBarButtonItem+ZLJBarButtonItem.h"
#import "ZLJBlockButton.h"

@interface UIBarButtonItem ()

@end


@implementation UIBarButtonItem (ZLJBarButtonItem)


+ (instancetype)initItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount itemClickBlock:(BarButtonClickBlock)itemClickBlock{
    
    ZLJBlockButton    *rightButton = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:(CGRectMake(0, 0, 60, 30))];
    [rightButton setTitle:title forState:UIControlStateNormal];
    [rightButton setTitle:@"完成" forState:UIControlStateSelected];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:tFount];
    [rightButton setTitleColor:tColor forState:UIControlStateNormal];
    UIBarButtonItem *editeItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTapBlock:^(UIButton * _Nonnull sender) {
        sender.selected = !sender.selected;
        if (itemClickBlock) {
            itemClickBlock(sender);
        }
    }];
    return editeItem;
}


+ (instancetype)initRightItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount itemClickBlock:(BarButtonClickBlock)itemClickBlock {
    
    ZLJBlockButton    *rightButton = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:(CGRectMake(0, 0, 60, 30))];
    [rightButton setTitle:title forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:tFount];
    [rightButton setTitleColor:tColor forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTapBlock:^(UIButton * _Nonnull sender) {
        if (itemClickBlock) {
            itemClickBlock(sender);
        }
    }];
    return rightItem;
}

+ (instancetype)initLeftItems:(NSString *)title titleColor:(UIColor *)tColor titleFount:(CGFloat)tFount imageStr:(NSString *)imageStr itemClickBlock:(BarButtonClickBlock)itemClickBlock {
    
    ZLJBlockButton    *rightButton = [ZLJBlockButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:(CGRectMake(0, 0, 20, 30))];
    [rightButton setTitle:title forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:tFount];
    [rightButton setTitleColor:tColor forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:imageStr] forState:(UIControlStateNormal)];
    [rightButton setImageEdgeInsets:(UIEdgeInsetsMake(0, 10, 0, -10))];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [rightButton addTapBlock:^(UIButton * _Nonnull sender) {
        if (itemClickBlock) {
            itemClickBlock(sender);
        }
    }];
    return rightItem;
}


@end
