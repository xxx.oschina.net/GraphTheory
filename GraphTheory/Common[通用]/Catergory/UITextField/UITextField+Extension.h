//
//  UITextField+Extension.h
//  BusinessFine
//
//  Created by 杨旭 on 2020/1/16.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Extension) <UITextFieldDelegate>

/**
 设置输入首字母不能为0

 @param isZero  YES 设置输入不能为0  
 */
- (void)setFirstLetterWithIsZero:(BOOL)isZero;

@end

NS_ASSUME_NONNULL_END
