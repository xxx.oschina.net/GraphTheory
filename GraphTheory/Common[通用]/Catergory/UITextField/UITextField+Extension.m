//
//  UITextField+Extension.m
//  BusinessFine
//
//  Created by 杨旭 on 2020/1/16.
//  Copyright © 2020年 杨旭. All rights reserved.
//

#import "UITextField+Extension.h"

@implementation UITextField (Extension)

/**
 设置输入首字母不能为0
 
 @param isZero  YES 设置输入不能为0
 */
- (void)setFirstLetterWithIsZero:(BOOL)isZero {
    if (isZero == YES) {
        self.delegate = self;
    }
}


//  输入首字母不能为0
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string

{
    if ([string length] > 0){
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.')//数据格式正确
        {
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if (single == '0'){
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }else {
                    return YES;
                }
            }
        }
        
    }else {
        return YES;
    }
    
    return YES;
}

@end
