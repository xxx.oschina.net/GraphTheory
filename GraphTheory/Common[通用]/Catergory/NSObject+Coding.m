//
//  NSObject+Coding.m
//  Utility
//
//  Created by 刘小壮 on 2015/4/9.
//
//

#import "NSObject+Coding.h"
#import <objc/runtime.h>

@implementation NSObject (Coding)

- (void)lxz_decodeWithCoder:(NSCoder *)decoder {
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    if (ivars) {
        for (int i = 0; i < count; i++) {
            Ivar ivar = ivars[i];
            const char *ivarName = ivar_getName(ivar);
            NSString *keyName = [[NSString alloc] initWithUTF8String:ivarName];
            id value = [decoder decodeObjectForKey:keyName];
            [self setValue:value forKeyPath:keyName];
        }
    }
    free(ivars);
}

- (void)lxz_encodeWithCoder:(NSCoder *)coder {
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    if (ivars) {
        for (int i = 0; i < count; i++) {
            Ivar ivar = ivars[i];
            const char *ivarName = ivar_getName(ivar);
            NSString *keyName = [[NSString alloc] initWithUTF8String:ivarName];
            id value = [self valueForKeyPath:keyName];
            [coder encodeObject:value forKey:keyName];
        }
    }
    free(ivars);
}
- (id)lxz_copyWithZone:(NSZone *)zone {
    NSObject *obj = [[[self class] allocWithZone:zone] init];
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    if (ivars) {
        for (int i = 0; i < count; i++) {
            Ivar ivar = ivars[i];
            const char *ivarName = ivar_getName(ivar);
            NSString *keyName = [[NSString alloc] initWithUTF8String:ivarName];
            id value = [self valueForKeyPath:keyName];
            
            if ([value respondsToSelector:@selector(copyWithZone:)]) {
                [obj setValue:[value copy] forKey:keyName];
            } else {
                [obj setValue:value forKey:keyName];
            }
        }
    }
    free(ivars);
    return obj;
}
+ (NSArray *)getProperties:(Class)cls{
    // 获取当前类的所有属性
    unsigned int count;// 记录属性个数
    objc_property_t *properties = class_copyPropertyList(cls, &count);
    // 遍历
    NSMutableArray *mArray = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        
        // An opaque type that represents an Objective-C declared property.
        // objc_property_t 属性类型
        objc_property_t property = properties[i];
        // 获取属性的名称 C语言字符串
        const char *cName = property_getName(property);
        // 转换为Objective C 字符串
        NSString *name = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
        [mArray addObject:name];
    }
    
    return mArray.copy;
}
@end





