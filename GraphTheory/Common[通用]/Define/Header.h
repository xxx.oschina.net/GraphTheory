//
//  Header.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#ifndef Header_h
#define Header_h

//          ------ 颜色配置 -------
//16进制颜色
#define HEX_COLOR(hexString)  [UIColor colorWithHexString:hexString]

//RGB颜色转换
#define ColorRGB(x,y,z,h)   [UIColor colorWithRed:x/255.0 green:y/255.0 blue:z/255.0 alpha:h]

//app主题色
//#define APPTintColor            HEX_COLOR(@"#FF6F24")
#define APPTintColor            HEX_COLOR(@"#F16622")


// 定义全局背景色
#define color_BgColor [UIColor colorWithHexString:@"#ffffff"]

// 定义部分页面背景色
#define color_BackColor [UIColor colorWithHexString:@"#f8f8f8"]

// 定义标题颜色
#define color_TextOne [UIColor colorWithHexString:@"#333333"]

// 定义副标题颜色
#define color_TextTwo [UIColor colorWithHexString:@"#666666"]

// 定义副标题颜色
#define color_TextThree [UIColor colorWithHexString:@"#999999"]

// 定义副标题颜色
#define color_TextFour [UIColor colorWithHexString:@"#eeeeee"]

// 定义边框颜色
#define color_BorderColor [UIColor colorWithHexString:@"#999999"]

// 定义分割线色值
#define color_LineColor [UIColor colorWithHexString:@"#f4f4f4"]

// 定义绿色颜色
#define color_GreenColor [UIColor colorWithHexString:@"#00be00"]

// 定义橘色
#define color_OrangeColor [UIColor colorWithHexString:@"#ff8a09"]

// 定义咖啡色
//#define color_CoffeeColor [UIColor colorWithHexString:@"#5F3100"]

// 定义红色baise
#define color_WhiteColor [UIColor colorWithHexString:@"#ffffff"]

// 定义红色
#define color_RedColor [UIColor colorWithHexString:@"#e41818"]

// 定义粉色
#define color_PinkColor [UIColor colorWithHexString:@"#FF6F24" alpha:0.06]

#define kNotificationImageUrl @"NotificationImageUrl" // 修改用户头像

#define kNotificationPutScreening @"NotificationPutScreening" // 投放筛选

#define kNotificationNoticeOrderBadge @"NotificationNoticeOrderBadge" // 更新通知订单角标

#define kNotificationNoticeMessageBadge @"NotificationNoticeMessageBadge" // 更新通知消息角标


#endif /* Header_h */
