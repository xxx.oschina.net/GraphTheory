//
//  WebUrl.h
//  BusinessFine
//
//  Created by 杨旭 on 2019/8/29.
//  Copyright © 2019年 杨旭. All rights reserved.
//

#ifndef WebUrl_h
#define WebUrl_h
//------ 网络请求接口 ------
#ifdef DEBUG //处于开发阶段
#define DevepMent 0  //1为开发接口， 2为正式接口
#else //处于开发阶段
#define DevepMent 0  //0为正式接口，永远不要改
#endif


/****开发接口*****/
#if DevepMent == 1
// 测试
#define kBaseURL @"http://app.itulun.com/api/"
/****正式接口*****/
#elif DevepMent == 2
//正式
#define kBaseURL @"http://app.itulun.com/api/“
/****打包接口*****/
#elif DevepMent == 0
//正式
#define kBaseURL @"http://app.itulun.com/api/"

#endif


/**
* 苹果商店APP_Id
*/
#define APPStoreID  @"1578154227"

/**
 * Bugly的APP_Id   
 */
#define Bugly_Id  @"e9f6367187"

/**
 微信key
 */
#define WX_APP_ID   @"wxdd816350394e8f81"
/**
 微信的APP_Secret
 */
#define Wechat_APP_Secret   @"c22fba85374903a57afa3c016cee5b20"

/**
 友盟key
 */
#define UM_APP_KEY   @"5dd4b3e90cafb2b1f50009f3"

/**
 百度地图key
 */
#define BMK_APP_KEY   @"vRCUhPKEGFByk6ir1uTXdDYHWdoOfvax"

/**
 高德地图key
 */
#define AMAP_APP_KEY   @"8ddf675fca558dc1a7b1e9c7ad3f864a"


#define PYSEARCH_SEARCH_HISTORY_CACHE_PATH [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"PYSearchhistories.plist"] 

#endif /* WebUrl_h */
